..
  *******************************************************************************
  Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

Lane (osi3::LogicalLane)
========================

A ``osiql::Lane`` is a wrapper of a ``osi3::LogicalLane``, which unlike a ``osi3::Lane`` stores its driving direction, successor lanes and predecessor lanes.

Direction & Side of Road
************************

The local coordinate system of a lane is dependent on its assigned reference line.
The driving direction following the reference line is called ``Direction::Downstream`` in osiql and corresponds to ``osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S``, while the reversed direction is called ``Direction::Upstream`` in osiql or ``osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_DECREASING_S`` in OSI.
The direction in osiql and OSI are defined equivalently and can thus be statically cast from one to other in both directions.
Additionally, osiql provides ``GetSideOfRoad`` functions that return ``Side::Right`` for lanes with driving direction ``Direction::Downstream`` and ``Side::Left`` for ``Direction::Upstream``.

Traversal
*********

OSI defines the successor and predecessor lane of a given lane based on that lane's reference line.
A lane connected at the start of the reference line is a predecessor, and a lane connected at the end of the reference line is a successor.
In some cases it may be desirable to query the successor and predecessor according to the driving direction of a given lane ss opposed to the direction of its reference line.
In osiql, the successor lanes can be retrieved using  ``Lane::GetConnectedLanes<Traversal::Forward>``.
The successor lanes as defined by OSI can still be accesed via ``Lane::GetConnectedLanes<Direction::Downstream>``.

Likewise, ``Lane::GetAdjacentLanes<Side, Traversal::Forward>`` returns the adjacent lane on the given side relative to the lane's driving direction while ``Lane::GetAdjacentLanes<Side, Direction::Downstream>`` does the same but according to what is left and right based on the definition of the reference line.

If no template non-type argument for a Traversal or Direction is provided, osiql defaults to ``Traversal::Forward``.

Lane Boundaries
***************

Each lane has a chain of left boundaries and right boundaries.
In osiql it is assumed that a lane's first boundary on either side starts at the same s-coordinate where the lane starts and that a lane's last boundary on either side ends at the same s-coordinate where the lane ends.
This is not required by the OSI standard, but failure to satisfy this assumption may result in incorrect point localizations.
For more on boundaries, see :doc:`/Wrappers/20_lines`.

Adjacent Lanes
**************

osiql adds a concept for roads, which are rows of adjacent lanes.
Every lane in osiql is part of a road.
This introduces a restriction that all lanes of the same road must begin and end at the same s-coordinates as their neighbors, meaning that osiql does not support lanes with multiple neighboring lanes on the same side.
Instead, each lane in osiql has at most one neighboring lane on each of its sides.
Currently, osiql does not support lanes having multiple overlapping adjacent lanes either, as would be the case with a bicycle lane on top of a car lane for instance.

The benefit of roads is that accessing a lane multiple lanes to the left or right is significantly more performant than in OSI and that roads provide many useful methods such as object aggregation and point conversion.
See :doc:`/Extensions/30_road` for more information on roads.

Code Examples
*************

Given a vehicle, return how many lanes there are to its left that share its current driving direction:

.. code-block:: cpp
  :linenos:

  Road& road{...};
  MovingObject& object{...};
  // The object may touch multiple roads, so a specific overlap has to be filtered for:
  std::optional<Overlap<Lane>> overlap{object.GetOverlap<Lane>(Matches<Road>{road})};
  assert(overlap.has_value());
  const Lane& lane{overlap.value().GetLane()};
  return lane.GetIndex(lane.GetSideOfRoad());
