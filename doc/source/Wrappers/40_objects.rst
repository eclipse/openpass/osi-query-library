..
  *******************************************************************************
  Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

Objects
=======

Moving objects
**************

osiql ignores the assigned positions of moving objects. Instead, when constructing a ``osiql::Query``, copies of all moving objects are created and their local positions are calculated. This is because there is a chance that a reference to a ``osi3::MovingObject`` may be invalidated (despawned or reallocated) between timesteps. The original assigned positions can still be accessed directly through the object's handle.

Stationary objects
******************

Static objects, which includes ``osi3::RoadMarking``, ``osiql::StaticObject``, ``osiql::TrafficLight`` & ``osi3::TrafficSign``, inherit their assigned positions from the OSI GroundTruth.

A ``osi3::TrafficLight`` is an individual light bulb, whereas an ``osiql::TrafficLight`` is a collection of light bulbs that have identical lane assignments to each other.