..
  *******************************************************************************
  Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

Lane (osi3::LogicalLane)
========================

A ``osiql::Lane`` is a wrapper of a ``osi3::LogicalLane``, which unlike a ``osi3::Lane`` stores its driving direction, successor lanes and predecessor lanes.

Direction & Side of Road
************************

The local coordinate system of a lane is dependent on its assigned reference line. The driving direction following the reference line is called ``Direction::Downstream`` in osiql and corresponds to ``osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S``, while the reversed direction is called ``Direction::Upstream`` in osiql or ``osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_DECREASING_S`` in OSI. More intuitively, osiql provides ``GetSideOfRoad`` functions that return ``Side::Right`` for lanes with driving direction ``Direction::Downstream`` and ``Side::Left`` for ``Direction::Upstream``.

Orientation
***********

In OSI, a successor of a logical lane is always at the end of the lane which is further along its assigned reference line, meaning a vehicle driving forwards on a lane on the left side of the road (or reference line) is driving from one lane onto its predecessor lane, which is rather unintuitive. In osiql, the method ``Lane::GetConnectedLane<Orientation::Forward>`` returns the successor in that lane's driving direction instead of the reference line's definition direction. Retrieving the OSI successor is still possible:

.. code-block:: cpp
  :linenos:

  const osiql::Lane& lane{ ... };
  // Successors in driving direction:
  const std::vector<osiql::Lane*>& successorLanes{lane.GetConnectedLanes<osiql::Orientation::Forward>()};
  // Successors in road direction:
  const std::vector<osiql::Lane*>& osiSuccessorLanes{lane.GetConnectedLanes(osiql::Direction::Downstream)};

Likewise, ``Lane::GetAdjacentLanes<Side, Orientation>`` returns the adjacent lane on the given side relative to the lane's driving direction in the given orientation, meaning ``GetAdjacentLanes<Side::Left, Orientation::Forward>() == GetAdjacentLanes<Side::Right, Orientation::Backward>()``. 