..
  *******************************************************************************
  Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

Point Chains
============

Reference Lines
***************

The reference line defines the coordinate system of all lanes that refer to it.
While this is not required in OSI, osiql assumes that all adjacent lanes of a lane refer to the same reference line.
Queries may fail if this is not the case.

If osiql is built with OSI 3.6.0 or later, a reference line can have different definitions for its coordinate systems.
If the polyline type of the reference line is not set, osiql will default to treating the reference line as one using t-axis yaws for its local coordinate system.
If a point on the polyline has no t-axis yaw, osiql will use an average of that point's prior and next edge's angles to interpolate a t-axis yaw for that point.
See the OSI documentation of a reference line for more information on the different st-coordinate system definitions.

Boundaries
**********

Boundaries of a lane are always defined in the same direction as the reference line of that lane (ergo its points are ordered by ascending s-coordinate).
Localizing points using just a boundary is possible in osiql.
However, since boundaries do not have access to their lane's reference line, they can not adhere to any set t-axis yaws.
The point will be localized as though the reference line had disabled t-axis yaws.

Lane Markings
*************

Lane markings are the actual visible lines that denote a lane boundary.
There is no direct link between a lane boundary and a lane marking in OSI, but osiql finds the nearest boundary to any marking and links the physical and logical representations together.
This is merely a best guess and may be prone to error or unintended associations.
