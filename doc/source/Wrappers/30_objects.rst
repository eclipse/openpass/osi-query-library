..
  *******************************************************************************
  Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

Objects
=======

osiql stores lazy-evaluated transformation matrices and separate matrices for translation, rotation, velocity, acceleration, spin and angular impulse for each object.
These allow somewhat efficient conversion between global coordinates and object coordinates.

These transformations can also be inverted:

.. code-block:: cpp
  :linenos:

  const MovingObject& object{...};
  const XYZ localOffset{...}; // Offset relative to the vehicle center

  const Matrix<4>& transform{object.GetTransformationMatrix()};
  const XYZ globalOffset{transform * localOffset};
  assert(Inv(transform) * globalOffset == localOffset);

Moving Objects
**************

osiql ignores the assigned positions of moving objects and stationary objects.
Instead, when constructing or updating a ``osiql::World``, the geometric intersections of all moving objects with any touched lane or road are computed.
These overlaps are used in place of an object's local center for most queries.
For instance, the distance between objects looks at the shortest distance between overlaps instead of their object centers.
However, the object's original assigned positions can still be accessed directly through its handle.

Vehicle
*******

A vehicle is a moving object with vehicle lights, a front axle, a rear axle and a role.
The position of the center of these axles can be retrieved using ``GetAxleOffset<Axle::Front>`` (relative to the object's bounding box center and orientation) or ``GetAxleXY<Axle::Rear>`` (relative to the world's coordinate origin)

Stationary Objects
******************

There are multiple types of stationary objects.

* A ``osiql::StationaryObject`` is a wrapper of a ``osi3::StationaryObject``.
* A ``osiql::RoadMarking`` is a wrapper of a ``osi3::RoadMarking``, which is essentially a traffic sign drawn onto a road.
* A ``osiql::TrafficSign`` is a wrapper of a ``osi3::TrafficSign``.
  It may contain supplementary signs.
  Support for these is currently limited within osiql and supplementary signs will need to be accesed via the wrapper's handle in most cases.
  ``osi3::RoadMarking``, ``osiql::StationaryObject``, ``osiql::TrafficLight`` and ``osi3::TrafficSign``, inherit their assigned positions from the OSI GroundTruth.
* A ``osi3::TrafficLight`` is an individual light bulb, whereas an ``osiql::TrafficLight`` is a collection of light bulbs that have identical lane assignments to each other.
