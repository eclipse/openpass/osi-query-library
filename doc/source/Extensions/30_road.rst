..
  *******************************************************************************
  Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

osiql::Road
===========

In OSI, the concept of roads does not exist, just relationships between individual lanes.
In osiql, however, a ``osiql::Road`` is defined as a set of adjacent lanes, similar to a LaneSection in OpenDRIVE or a LaneGroup in NDS.
This allows for iteration over lanes on a given side of the road, identification of the lane closest to a given point, performance of coordinate conversions like a Lane, and access to successors/predecessors of all adjacent lanes simultaneously.
One constraint imposed by osiql on roads is the existence of a right side, where zero or more lanes have a moving direction that increases in s-coordinate (referred to as Downstream in osiql), and a left side, where zero or more lanes have a moving direction that decreases in s-coordinates (referred to as Upstream in osiql).
This limitation facilitates the targeting of lanes by their index on a particular side of the road.

.. note::
  Currently, osiql does not support bidirectional lanes.
