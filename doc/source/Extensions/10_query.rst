..
  *******************************************************************************
  Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

Localization & coordinate conversion with osiql::Query
======================================================

The ``osiql::Query`` is the entry-point for any usage of osiql. It should be constructed only once for any ``osi3::GroundTruth`` and provides ways to convert global points to local coordinates and to calculate the intersections of geometry with the GroundTruth's lanes. It also includes more efficient getters (than by using OSI) for various object types given their id and has shortcuts to perform queries on the GroundTruth's host vehicle.

Any global point touching a road can be converted to a set of local points inside lanes using ``Query::FindLocalPoints``. The returned value is a vector in case of overlapping lanes. 

For more information on local points, see :doc:`/Types/10_points`.

For more information on localizing to a specific lane or road, see :doc:`/Wrappers/20_lane` or :doc:`/Extensions/30_road`.

For more information on the Query class, refer to the doxygen documentation.