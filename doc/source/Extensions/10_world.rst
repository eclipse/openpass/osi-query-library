..
  *******************************************************************************
  Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

osiql::World
============

The ``osiql::World`` is the entry-point for any usage of osiql.
It should be constructed only once for any ``osi3::GroundTruth`` and provides ways to convert global points to local coordinates and to calculate the intersections of geometry with the GroundTruth's lanes.
``osiql::World`` is a wrapper of a ``osi3::GroundTruth`` that stores all its wrapper objects in separate containers to allow fetching objects by their id more efficiently than OSI's linear time lookup.

These are the stored types:

* Lane Boundaries (osi3::LogicalLaneBoundary)
* Lane Markings (osi3::LaneBoundary)
* Lanes (osi3::LogicalLane);
* Reference Lines (osi3::ReferenceLine)
* Roads (No corresponding OSI object)
* Moving Objects (osi3::MovingObject)
* Road Markings (osi3::RoadMarking)
* Static Objects (osi3::StationaryObject)
* Light Bulbs (osi3::TrafficLight)
* Traffic Lights (Set of light bulbs)
* Traffic Signs (osi3::TrafficSign)

The ``osiql::World`` needs to be informed when its underlying handle has changed:

.. code-block:: cpp
  :linenos:

  osi3::GroundTruth groundTruth;
  // A world will hold a reference to the given ground truth.
  // The user is responsible for managing the handle's lifetime.
  World world{groundTruth};
  // When the moving objects of the underlying groundTruth change, the world needs to be updated:
  world.Update(groundTruth.moving_object());

See the doxygen documentation for all methods that ``osiql::World`` provides.

A world can be used to select objects by their id.

.. code-block:: cpp
  :linenos:

  const Lane* lane{world.GetLane(5)};
  const MovingObject* movingObject{world.GetMovingObject(10)};
  // GetRoadMarking, GetTrafficSign, etc.
  // To iterate over all objects a certain type:
  const auto& movingObjects{world.GetAll<MovingObject>()};
  for(const auto& item : movingObjects) {
    // ...
  }

Any global point touching a road can be converted to a set of local points inside lanes using ``World::Localize``.
The returned value is always a vector in case of overlapping lanes.

.. code-block:: cpp
  :linenos:

  std::vector<Point<>> points{world.Localize(XY{1, 2})};
  // Points with angles can also be localized:
  std::vector<Pose<Point<>>> poses{world.Localize(Pose<XY>{{1, 2}, M_PI_2})};
  // Points can easily be globalized:
  assert(!points.empty());
  points.front().GetXY();

For more information on local points, see :doc:`/Types/10_types`.

For more information on localizing to a specific lane or road, see :doc:`/Wrappers/10_lane` or :doc:`/Extensions/30_road`.

For more information on the World class, refer to the doxygen documentation.
