..
  *******************************************************************************
  Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

Vicinity queries with osiql::Stream
===================================

Streams can be constructed on-demand from a starting local point and are used to perform vicinity queries. A ``osiql::Stream`` is a tree traversing a local origin point's connected lanes or roads up to a certain distance. A stream can also look back peripherally into lanes merging into it. Once created, a stream can be searched either road-wide or lane-wide, which excludes any lane changes and any findings are returned with a distance from the starting point of the stream.

Streams will be heavily expanded upon and replaced with Nodes in osiql 1.2.0.
