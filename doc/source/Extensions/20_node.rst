..
  *******************************************************************************
  Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

osiql::Node
===========

Broadly speaking, a node is an element or vertex in a graph.
In osiql, these vertices are lanes or roads.
Each node stores its distance from an origin point, a pointer to the node's parent node and lazy-evaluated pointers to the node's connected nodes.
Nodes can be used for pathfinding or structured graph traversal (breadth-first, depth-first, shortest-distance, etc.).
Nodes are also used as the foundation of routes.
For more info on routes, see :doc:`/Extensions/40_route`.

A default node is a road node.
Nodes can only be heap-allocated.

.. code-block:: cpp
  :linenos:

  const Point<> origin{...};
  std::shared_ptr<const Node<>> root{Node<>::Create(origin)};
  // The shared pointer of a node can be retrieved from itself:
  const Node<>& node{*root};
  root = node.GetShared();

A chain of nodes can be constructed from any initial node.

.. code-block:: cpp
  :linenos:

  // Let's create a path that starts on a highway and stays on the highway for as long as possible.
  // If there is a fork in the road, we can stick with whichever road has the most lanes
  // as a good heuristic of what constitutes a highway as opposed to an exit.
  std::shared_ptr<const Node<>> root{...};
  std::vector<std::shared_ptr<const Node<>>> path{root->GetPath([](const Node<>& node) -> const Node<>*{
    const auto& children{node.GetChildren()};
    auto successor{std::max_element(children.begin(), children.end(), [](const auto& a, const auto& b) {
      return a->GetRoad().lanes.size() < b->GetRoad().lanes.size();
    })};
    return successor == children.end() ? nullptr : successor->get();
  })};

Alternatively, a specific destination can be searched for, to which the shortest path can be returned.

.. code-block:: cpp
  :linenos:

  std::shared_ptr<const Node<>> root{...};
  const Point<> destination{...};
  const Node<>* goal{root->FindClosestNode(destination)};
  std::vector<std::shared_ptr<const Node<>>> path{goal->GetPathFromDepth(root->depth)};

The objects assigned to a node can be retrieved and filtered using predicates.
Unlike when querying a road or lane for its assigned objects, the resulting list of object placement relations of a node query will have its result sorted by distance.

.. code-block:: cpp
  :linenos:

  const Node<>& node{...};
  // Returns all locations of vehicles on this node:
  node.FindAllWithin<MovingObject>(Is{MovingObject::Type::Vehicle});

  // Returns all locations of moving objects 50 to 100 units of distance from the origin
  // on any connected node while excluding one specific object:
  const MovingObject& self{...}
  node.FindAll<MovingObject>(Conjunction{GreaterEqual<Distance>::Than{50.0},
                                         Less<Distance>::Than{100.0},
                                         NotEqual<MovingObject>::To{self}});

If querying a single node is not sufficient, a set of nodes can be produced from an initial node.

.. code-block:: cpp
  :linenos:

  const Node<>& node{...};
  // Search forward from this node, visiting each node within
  // 300 units of distance from the node origin, ordered by ascending distance
  // and return only those nodes whose lane is an exit lane.
  node.GetNodesIf(Is{Lane::Type::Exit}, 300);
  // osiql provides a more complex overload of GetNodesIf where more behaviors can be user-defined,
  // such as what direction to search in, which nodes to visit or in what order they should be visited.
  // See the Doxygen documentation for more details.

osiql provides an efficient way to merge the results of multiple node queries using InsertAll or InsertAllWithin.

.. code-block:: cpp
  :linenos:

  std::vector<Node<>> nodes{...};
  std::vector<Location<MovingObject, Node<>>> output;
  for(auto& node : nodes) {
    node.InsertAllWithin<MovingObject>(std::back_inserter(output), Is{MovingObject::Type::Vehicle});
  }
