..
  *******************************************************************************
  Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

osiql::Route
============

A route in osiql is a chain of nodes with a local start and end point.
(Local points are UV or ST coordinates paired with a lane; see :doc:`/Types/10_types`.)
Nodes are roads or lanes paired with a distance from an origin (see :doc:`/Extensions/20_node`).
The distance is the signed total s-coordinate difference travelled from the origin along the direction of the route.
It may be negative if describing a traversal in the opposite of the route's direction.
Routes do not model any t-coordinate movement or offsets.
They are NOT related to a ``osi3::Route``, which was introduced in OSI 3.6.

Routes allow querying the distances of objects along a chain of nodes in an ordered fashion.
They can for instance be used to return all objects within a given distance range or to return the first n closest objects to a given target distance.

A route can be constructed from a ``osiql::World`` and will find the shortest path between the given points:

.. code-block:: cpp
  :linenos:

  using namespace osiql;
  World world{...};
  // From one start point to an end point:
  std::optional<Route<>> route = world.GetRoute(XY{0, 0}, XY{100, 100});
  // OR passing through a chain of points:
  std::vector<XY> points{XY{0, 0}, XY{100, 0}, XY{100, 100}};
  route = world.GetRoute(points.begin(), points.end());
  // NOTE: Local points are also valid

A route can also be constructed between two local points without access to a ``osiql::World``:

.. code-block:: cpp
  :linenos:

  using namespace osiql;
  Point<> a{...};
  Point<> b{...};
  std::optional<Route<>> routeForwards = a.GetRoute(b);
  std::optional<Route<Traversal::Backward>> routeBackwards = a.GetRoute<Traversal::Backward>(b);

If no path to a target is found, ``std::nullopt`` is returned instead of a route.
``GetRoute`` will find the shortest path in the driving direction of the first point's lane.
To search in the opposite direction, the template non-type parameter ``Traversal::Backward`` must be provided.
A route travelling both backwards and forwards is currently not supported.
Routes can consist of LaneNodes or RoadNodes depending on the route's second template type, which defaults to ``Road``, but can be replaced with ``Lane``, just like with Nodes.
These nodes can then be queried using the route's interface or be accessed directly to search outside the route's nodes as well.

Once constructed, a route can be used to find objects on it or to determine longitudinal distances (s-coordinate differences) along its path between objects or points.
