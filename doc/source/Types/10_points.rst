..
  *******************************************************************************
  Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

Point Types
===========

There are four categories of points in osiql:

* **Global point**: A set of cartesian XY(Z)-coordinates
* **Local point**: A pair of local st-coordinates and an entity they relate to
* **Pose**: A local point with an added relative angle
* **Vertex**: A local and global point in one

.. _points:
.. figure:: ./points.svg
  
``osiql::ReferenceLineCoordinates`` follow OSI's definition of a reference-line-based st-coordinate system, whereas a ``osiql::CenterlineCoordinates`` are similar, but their s-coordinate is relative to the start of a lane in its driving direction and their t-coordinate is relative to the respective lane's center line.
  