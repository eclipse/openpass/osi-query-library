..
  *******************************************************************************
  Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

Functors
========

In some cases, a user may want to perform a query that returns a filtered result.
Several functions in osiql accept function objects (functors) as predicates, enabling precise filtering of the data returned.

While these functors can be defined freely, osiql also provides a number of built-in, useful functors, including:

* ``osiql::Conjunction``: Require the satisfaction of multiple nested predicates
* ``osiql::Disjunction``: Require the satisfaction of one out of multiple nested predicates
* ``osiql::Is``: Require that the input fit one of the given enumeration values
* ``osiql::Get``: Transforms the input to a desired type
* ``osiql::Not``: Require the nested predicate to evaluate to false
* ``osiql::Matches``: Require a match with a given value
* ``osiql::Return``: Return a predefined value, such as ``Return<true>`` (often used as a default predicate)

For comparison tasks, the following binary built-in functors are available:

* ``osiql::Less``
* ``osiql::Greater``
* ``osiql::LessEqual``
* ``osiql::GreaterEqual``
* ``osiql::Equal``
* ``osiql::NotEqual``

And unary versions:

* ``osiql::Less::Than``
* ``osiql::Greater::Than``
* ``osiql::LessEqual::Than``
* ``osiql::GreaterEqual::Than``
* ``osiql::Equal::To``
* ``osiql::NotEqual::To``

Examples
********

.. code-block:: cpp
  :linenos:

  const TrafficSign trafficSign{...};
  // Return the first supplementary sign of this traffic sign that matches neither of the following types:
  trafficSign.GetSupplementarySign(Not{Is{SupplementarySign::Type::Accident, SupplementarySign::Type::Hazardous}});
  // Return the first supplementary sign with the following type and role:
  trafficSign.GetSupplementarySign(Conjunction{Is{SupplementarySign::Type::Weight}, Is{SupplementarySign::Role::Truck}});

.. note: Using ``osiql::Is`` allows querying in O(1) even with ``n`` enum values.

.. code-block:: cpp
  :linenos:

  const Lane& lane{...};
  const MovingObject& object{...};
  // Return the road overlap of this object with the same road as the given lane:
  object.GetOverlap<Road>(Matches<Road>{lane});

A use-case for custom comparators:

.. code-block:: cpp
  :linenos:

  const Lane& lane{...};
  // Return the three shortest vehicles on the lane:
  lane.Find<MovingObject>(3, Is{MovingObject::Type::Vehicle}, Less<Length>{});
  // Return the moving object with the greatest id among the first 100m of the lane:
  lane.Find<MovingObject>(1, Less<U>::Than{100}, Greater<Id>{});
  // Return the last two pedestrians on the lane:
  lane.Find<MovingObject>(2, Is{MovingObject::Type::Pedestrian}, Greater<U>{});

There are more functors for specific use-cases in osiql not listed above. 
It is usually beneficial to examine the inline documentation for examples on how they are used:

.. code-block:: cpp
  :linenos:

  const Node<>& node{...};
  const Point<>& target;
  // Find the nearest node by driving distance matching the target's road
  node.FindNode({}, NextNode<StableLess<Max<Distance>>, Node<>>{}, Matches<Road>{target});
  // Using breadth-first search
  node.FindNode({}, NextNode<StableLess<Depth>, Node<>>{}, Matches<Road>{target});
  // Using depth-first search
  node.FindNode({}, NextNode<StableGreater<Depth>, Node<>>{}, Matches<Road>{target});
