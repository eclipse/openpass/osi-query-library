..
  *******************************************************************************
  Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

Types
=====

osiql defines its own types for greater easy of use.

Coordinate Systems
******************

There are three fundamental coordinate type categories in osiql.

* **XY/XYZ**: Cartesian coordinates relative to an origin. Similar to ``osi3::Vector2d`` and ``osi3::Vector3d`` but with added support for arithmetic operators and other added utility methods.
* **ST**: Local coordinates relative to the reference line of a road (as defined by OSI's ``s_position()`` and ``t_position()`` fields)
* **UV**: Local coordinates relative to the center line of a lane in driving direction (adheres to the axis definitions of polyline type of the lane's reference line but might have inverted coordinates if the lane's driving direction opposes the reference line)

Cartesian coordinates can be used to express points in global coordinates or relative to a different origin.
osiql provides matrices to support transformation between different origins.
This can be used to get the lateral and longitudinal distance between two objects.

.. code-block:: cpp
  :linenos:

  const XYZ displacement{...};
  const Matrix<4> translation{TranslationMatrix(displacement)};
  assert(displacement + XYZ{1, 2, 3} == translation * XYZ{1, 2, 3});

Usually, transformations need to be applied based on the transformation of an object.
For that case, objects provide getters to retrieve their transformation matrices.
For more details, see :doc:`/Wrappers/30_objects`

Local coordinates are ambiguous, as the road they refer to is not specified.
This can be alleviated by associating coordinates with a lane:

* **Point<LaneType, CoordinateType>**: A pair of local coordinates (``ST`` or ``UV``) and a lane they relate to (``Lane`` or ``const Lane``)

Furthermore, coordinates or points can be extended to hold an angle using a Pose:

* **Pose<CoordinateType>**: An extension of a set of coordinates (``ST``, ``UV``, ``Point<LaneType, ST>``, ``Point<LaneType, UV>``, ``XY`` or ``XYZ``) by an angle relative to the primary axis of its coordinates

.. _points:
.. figure:: ./points.drawio.svg

Overlaps
********

An Overlap is the polygonal intersection area on either a Lane or a Road.
It allows retrieval of its global bounds in xy-coordinates as well as its local bounds in st-coordinates.
