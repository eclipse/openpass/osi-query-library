..
  *******************************************************************************
  Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

osiql Documentation
===================

osiql (Open Simulation Interface Query Library) is a wrapper of the `ASAM Open Simulation Interface (OSI) <https://github.com/OpenSimulationInterface/open-simulation-interface>`_ using modern C++17 features and idioms to provide a terse, object-oriented interface to OSI data in addition to offering various utility methods and features.
Specifically, it provides ways to convert between global and local road-based coordinate systems, offers means to generate random routes or the shortest route between two points and adds functions for querying an object's/point's surroundings.

For build instructions, see the repository's README.md.

Motivation
**********

1. Amortized Performance

OSI is a data interface used to standardize communication between different programs.
This might include programs without access to the same memory, forcing all data to be represented in an address-independent, serializable fashion.
Additionally, the OSI specification defines few validity constraints.
Objects may be stored without being sorted, which prevents efficient look-up based on indirect associations.
osiql reduces the degree of indirection by constructing wrappers of OSI objects that store direct references to associated objects.

2. Improved Usability

A large part of osiql adds ways to perform input-dependent queries, such as retrieving the left or right adjacent lane based on its driving direction.
Other quality of life improvements are the introduction of arithmetic operators for various OSI point types, a common interface to access their coordinates, and support for all osiql types to be printed to an output stream.

3. Features & Extensions

osiql introduces a range of features and extensions, including global and local point conversion, geometric intersection calculations for objects overlapping with roads and lanes, and the ability to group individual light bulbs into functional traffic lights.
It also supports pathfinding, road mapping, and travel distance calculations, among other capabilities.

Note that development of osiql began prior to the introduction of ``osi3::Route`` and currently provides its own version of a route.

.. toctree::
   :caption: Main osiql classes
   :glob:
   :maxdepth: 1

   Extensions/*

.. toctree::
   :caption: OSI-Wrapper-Objects
   :glob:
   :maxdepth: 1

   Wrappers/*

.. toctree::
   :caption: Additional Types & Abstractions
   :glob:
   :maxdepth: 1

   Types/*
