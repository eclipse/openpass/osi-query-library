/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <tuple>

#include <osi3/osi_groundtruth.pb.h>
#include <osi3/osi_version.pb.h>

#include "OsiQueryLibrary/Utility/Common.h"

namespace osiql {
using Version = std::tuple<uint32_t, uint32_t, uint32_t>;

constexpr Version GetVersion()
{
    return {2, 0, 0};
}

constexpr Version GetOSIVersion()
{
    // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers, cppcoreguidelines-pro-type-vararg, readability-magic-numbers)
    if constexpr (OSIQL_HAS_MEMBER(osi3::LogicalLane, traffic_rule()))
    {
        return {3, 7, 0};
    }
    else if constexpr (OSIQL_HAS_MEMBER(osi3::ReferenceLine_ReferenceLinePoint, has_t_axis_yaw()))
    {
        return {3, 6, 0};
    }
    else
    {
        return {3, 5, 0};
    }
    // NOLINTEND(cppcoreguidelines-avoid-magic-numbers, cppcoreguidelines-pro-type-vararg, readability-magic-numbers)
}

Version GetVersion(const osi3::GroundTruth &);
Version GetVersion(const osi3::InterfaceVersion &);
} // namespace osiql
