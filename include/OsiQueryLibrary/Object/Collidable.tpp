/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Collidable.h"

#include "OsiQueryLibrary/Object/VehicleData.h"
#include "OsiQueryLibrary/Types/Overlap.tpp"
#include "OsiQueryLibrary/Utility/Is.tpp"

namespace osiql {
template <typename Instance>
constexpr Collidable<Instance>::Collidable(Id id) :
    id{id}
{
}

template <typename Instance>
Collidable<Instance>::~Collidable()
{
    for (auto &overlap : GetOverlaps<Lane>())
    {
        overlap.GetLane().template GetOverlapping<Instance>().erase(static_cast<const Instance *>(this)->GetId());
    }
    for (auto &overlap : GetOverlaps<Road>())
    {
        overlap.GetRoad().template GetOverlapping<Instance>().erase(static_cast<const Instance *>(this)->GetId());
    }
}

template <typename Instance>
constexpr Id Collidable<Instance>::GetId() const
{
    return id;
}

template <typename Instance>
template <typename Type>
const std::vector<Overlap<Type>> &Collidable<Instance>::GetOverlaps() const
{
    if constexpr (std::is_same_v<Type, Lane>)
    {
        return laneOverlaps;
    }
    else if constexpr (std::is_same_v<Type, Road>)
    {
        return roadOverlaps;
    }
    else
    {
        static_assert(always_false<Type>, "Not supported");
    }
}

template <typename Instance>
template <typename Type>
std::vector<Overlap<Type>> &Collidable<Instance>::GetOverlaps()
{
    return const_cast<std::vector<Overlap<Type>> &>(std::as_const(*this).template GetOverlaps<Type>());
}

template <typename Instance>
template <typename Type, typename Pred>
const Overlap<Type> *Collidable<Instance>::GetOverlap(Pred &&pred) const
{
    const auto &overlaps{GetOverlaps<Type>()};
    const auto it{std::find_if(overlaps.begin(), overlaps.end(), std::forward<Pred>(pred))};
    return it == overlaps.end() ? nullptr : &*it;
}

template <typename Instance>
template <typename Type, typename Pred>
Overlap<Type> *Collidable<Instance>::GetOverlap(Pred &&pred)
{
    return const_cast<Overlap<Type> *>(std::as_const(*this).template GetOverlap<Type>(std::forward<Pred>(pred)));
}

template <typename Instance>
template <typename Type>
void Collidable<Instance>::SetOverlaps(std::vector<Overlap<Type>> &&overlaps)
{
    GetOverlaps<Type>() = std::move(overlaps);
    for (Overlap<Type> &overlap : GetOverlaps<Type>())
    {
        get<Type>(overlap).template GetOverlapping<Instance>().emplace(
            static_cast<const Instance *>(this)->GetId(),
            Aggregate<Overlap<Type> *, Instance *>{&overlap, static_cast<Instance *>(this)}
        );
    }
}

template <typename Instance>
template <typename It>
Interval<double> Collidable<Instance>::GetDeviation(It first, It pastLast) const
{
    Interval<double> result;
    for (auto lane{first}; lane != pastLast; ++lane)
    {
        const std::vector<Overlap<Road>> &overlaps{GetOverlaps<Road>()};
        const auto overlap{std::find_if(overlaps.begin(), overlaps.end(), Matches<Road>{*lane})};
        if (overlap != overlaps.end())
        {
            Interval<double> partialResult{(*lane)->GetV(overlap->localShape.begin(), overlap->localShape.end())};
            result.min = std::min(partialResult.min, result.min);
            result.max = std::max(partialResult.max, result.max);
        }
    }
    return result;
}

template <typename Instance>
template <typename LaneChain>
Interval<double> Collidable<Instance>::GetDeviation(const LaneChain &chain) const
{
    return GetDeviation(chain.begin(), chain.end());
}

template <typename Instance>
template <Traversal Toward, Traversal T>
std::deque<const Lane *> Collidable<Instance>::GetLaneChain(const Route<T> &route) const
{
    // TODO: Performance - Do this once per road overlap, not per lane overlap
    // (Requires a way to get intended driving direction from road. It might be best to store
    // street overlaps as a std::unordered_map<Overlap<Road>, std::vector<Overlap<Lane>>)
    for (auto &overlap : GetOverlaps<Lane>())
    {
        std::deque<const Lane *> chain{overlap.GetLane().template GetChain<Toward>(route)};
        if (!chain.empty())
        {
            return chain;
        }
    }
    return {};
}

template <typename Instance>
template <typename Target, typename LaneChain>
Interval<double> Collidable<Instance>::GetObstruction(const Target &target, const LaneChain &laneChain) const
{
    if constexpr (Are<Overlaps<Road>>::in<Target>)
    {
        const Interval<double> selfDeviations{GetDeviation(laneChain)};
        const Interval<double> targetDeviations{target.GetDeviation(laneChain)};
        return {targetDeviations.min - selfDeviations.max, targetDeviations.max - selfDeviations.min};
    }
    else if constexpr (OSIQL_HAS_MEMBER(Target, begin()) && OSIQL_HAS_MEMBER(Target, end()))
    {
        static_assert(always_false<Target>, "Currently not supported");
    }
    else if constexpr (Are<Lane, S, T>::in<Target>)
    {
        const Interval<double> selfDeviations{GetDeviation(laneChain)};
        const Interval<double> targetDeviations{target.GetDeviation(laneChain)};
        return {targetDeviations.min - selfDeviations.max, targetDeviations.max - selfDeviations.min};
    }
    else
    {
        static_assert(always_false<Target>, "Collidable::GetObstruction: First argument's type must be a collidable or local point.");
    }
}

template <typename Instance>
double Collidable<Instance>::GetMass() const
{
    const Instance *self{static_cast<const Instance *>(this)};
    if constexpr (std::is_same_v<Instance, MovingObject>)
    {
        for (const auto &userData : self->GetHandle().source_reference())
        {
            if (userData.has_type() && userData.type() == "osiql::HostVehicleData")
            {
                std::uintptr_t address{StringToAddress(userData.reference())};
                const auto *data{reinterpret_cast<const HostVehicleData *>(address)}; // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast, performance-no-int-to-ptr)
                return data->GetVehicleWeight();
            }
        }
    }
    constexpr const double density{1.0};
    double volume{self->GetLength() * self->GetWidth() * self->GetHeight()};
    return volume * density;
}
} // namespace osiql
