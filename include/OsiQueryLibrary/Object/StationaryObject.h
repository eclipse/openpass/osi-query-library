/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <iostream>

#include <osi3/osi_object.pb.h>

#include "Collidable.h"
#include "Object.h"
#include "StationaryObject.hpp"

namespace osiql {
//! \brief A stationary object that will never move.
struct StationaryObject : Object<osi3::StationaryObject>, Collidable<StationaryObject>
{
    template <typename Base, OSIQL_REQUIRES(std::is_constructible_v<Object<osi3::StationaryObject>, Base>)>
    constexpr StationaryObject(Base &&base) : // NOLINT(bugprone-forwarding-reference-overload)
        Object<osi3::StationaryObject>{std::forward<Base>(base)}, Collidable<StationaryObject>{Object<osi3::StationaryObject>::GetId()}
    {
    }

    using Collidable<StationaryObject>::GetId;
};

std::ostream &operator<<(std::ostream &, const StationaryObject &);

OSIQL_HAS(Assignment<StationaryObject>, GetStationaryObject);
OSIQL_HAS(Overlap<StationaryObject>, GetStationaryObject);
} // namespace osiql
