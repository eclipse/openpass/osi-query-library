/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "MovingObject.h"

#include <numeric>

#include "Collidable.tpp"
#include "Object.tpp"
#include "OsiQueryLibrary/Routing/Node.tpp"
#include "OsiQueryLibrary/Street/Lane.tpp"
#include "OsiQueryLibrary/Street/Road.tpp"
#include "OsiQueryLibrary/Types/Constants.h"
#include "OsiQueryLibrary/Types/Enum.tpp"

namespace osiql {
template <Traversal T>
bool MovingObject::IsMoving() const
{
    if constexpr (T == Traversal::Any)
    {
        return GetBase().has_velocity() && ((GetBase().velocity().has_x() && GetBase().velocity().x() != 0.0) ||
                                            (GetBase().velocity().has_y() && GetBase().velocity().y() != 0.0) ||
                                            (GetBase().velocity().has_z() && GetBase().velocity().z() != 0.0));
    }
    else if constexpr (T == Traversal::Backward)
    {
        const double angle{GetYaw() + halfPi};
        return GetVelocity<XY>().GetSide(XY{}, XY{std::cos(angle), std::sin(angle)}) == Side::Left;
    }
    else if constexpr (T == Traversal::Forward)
    {
        const double angle{GetYaw() + halfPi};
        return GetVelocity<XY>().GetSide(XY{}, XY{std::cos(angle), std::sin(angle)}) == Side::Right;
    }
    else
    {
        static_assert(always_false<decltype(T)>, "MovingObject::IsMoving only support orientations 'Any', 'Forward' and 'Backward'");
    }
}

template <typename Type>
Type MovingObject::GetVelocity() const
{
    if (GetBase().has_velocity())
    {
        return as<Type>(GetBase().velocity());
    }
    std::cerr << "Error: GetVelocity() - Moving object " << GetId() << " has no assigned velocity\n";
    return {};
}

template <typename Point>
Vector<Dimensions<Point>> MovingObject::GetVelocity(const Point &localPoint) const
{
    static_assert(Are<X, Y>::in<Point>);
    return GetGlobalMovementMatrix() * localPoint;
}

template <typename Point>
Vector<Dimensions<Point>> MovingObject::GetAngularMomentum(const Point &localPoint) const
{
    return GetSpinMatrix() * localPoint;
}

template <typename Point>
Point MovingObject::GetAcceleration() const
{
    static_assert(Are<X, Y>::in<Point>);
    if (GetBase().has_acceleration())
    {
        return Point{GetBase().acceleration()};
    }
    std::cerr << "Error: GetAcceleration() - Moving object " << GetId() << " has no assigned acceleration\n";
    return {};
}

template <typename Point>
Vector<Dimensions<Point>> MovingObject::GetAcceleration(const Point &localPoint) const
{
    static_assert(Are<X, Y>::in<Point>);
    return GetFullAccelerationMatrix() * localPoint;
}

template <typename Point>
Vector<Dimensions<Point>> MovingObject::GetAngularImpulse(const Point &localPoint) const
{
    return GetAngularAccelerationMatrix() * localPoint;
}

template <typename Vector>
Vector MovingObject::GetMomentum() const
{
    return GetMass() * GetVelocity<Vector>();
}

template <typename Vector>
Vector MovingObject::GetForce() const
{
    return GetMass() * GetAcceleration<Vector>();
}

template <Traversal T, typename Destination>
double MovingObject::GetDistanceTo(const Destination &destination) const
{
    if constexpr (T == Traversal::Any)
    {
        const double forwardDistance{GetDistanceTo<Traversal::Forward>(destination)};
        const double backwardDistance{GetDistanceTo<Traversal::Backward>(destination)};
        if (forwardDistance < 0.0 && backwardDistance < 0.0)
        {
            return 0.0;
        }
        return (forwardDistance <= std::max(backwardDistance, 0.0)) ? forwardDistance : -backwardDistance;
    }
    else
    {
        return std::transform_reduce(
            GetOverlaps<Road>().begin(),
            GetOverlaps<Road>().end(),
            std::numeric_limits<double>::max(),
            [](double a, double b) { return std::min(a, b); },
            [&](const Overlap<Road> &roadOverlap) {
                const std::vector<Overlap<Lane>> &laneOverlaps{GetOverlaps<Lane>()};
                assert(!laneOverlaps.empty());
                const auto laneOverlap{std::find_if(laneOverlaps.begin(), laneOverlaps.end(), Matches<Road>{roadOverlap})};
                assert(laneOverlap != laneOverlaps.end());
                auto root{Node<T>::Create(roadOverlap, laneOverlap->GetLane())};
                if (const Node<T> *leaf{root->FindClosestNode(destination)}; leaf)
                {
                    return GetDistanceBetween(root->GetDistance(roadOverlap), leaf->GetDistance(destination));
                }
                return std::numeric_limits<double>::max();
            }
        );
    }
}

template <auto Towards>
Distances MovingObject::GetBoundaryDistances(const Lane &lane) const
{
    if constexpr (std::is_same_v<decltype(Towards), Traversal>)
    {
        return GetBoundaryDistances(lane.GetDirection(Towards), lane);
    }
    else if constexpr (std::is_same_v<decltype(Towards), Direction>)
    {
        const auto &overlaps{GetOverlaps<Road>()};
        if (const auto overlap{std::find_if(overlaps.begin(), overlaps.end(), Matches<Road>{lane})}; overlap != overlaps.end())
        {
            return {-lane.GetBoundaries<Side::Left, Towards>().template GetLateralDistances<Towards>(overlap->localShape).max, lane.GetBoundaries<Side::Right, Towards>().template GetLateralDistances<Towards>(overlap->localShape).min};
        }
        throw std::runtime_error("Getting the lateral distance of an object to a lane's boundaries whose road it does not lie on is no longer supported");
    }
    else
    {
        static_assert(always_false<decltype(Towards)>, "Not supported");
    }
}

template <typename Towards>
Distances MovingObject::GetBoundaryDistances(Towards t, const Lane &lane) const
{
    if constexpr (std::is_same_v<Towards, Traversal>)
    {
        return GetBoundaryDistances(lane.GetDirection(t), lane);
    }
    else if constexpr (std::is_same_v<Towards, Direction>)
    {
        return IsInverse(t) ? GetBoundaryDistances<!Default<Towards>>(lane) : GetBoundaryDistances<Default<Towards>>(lane);
    }
    else
    {
        static_assert(always_false<Towards>, "Not supported");
    }
}

namespace detail {
constexpr std::array<std::string_view, Size<MovingObject::Type>> movingObjectTypeToString{
    "Unknown",
    "Other",
    "Vehicle",
    "Pedestrian",
    "Animal",
};
} // namespace detail
} // namespace osiql
