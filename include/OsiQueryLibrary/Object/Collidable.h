/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Base class of MovingObject and StationaryObject which adds logic for storing
//! overlaps with roads and lanes

#include <deque>
#include <vector>

#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/Road.h"
#include "OsiQueryLibrary/Types/Overlap.h"

namespace osiql {
template <Traversal, typename>
struct Route;

//! \brief An object that stores how it overlaps with lanes and roads
template <typename Instance>
struct Collidable
{
    //! \brief Constructs a collidable object from its id
    constexpr Collidable(Id);

    Collidable(const Collidable &) = default;
    Collidable &operator=(const Collidable &) = default;
    Collidable(Collidable &&) = default;
    Collidable &operator=(Collidable &&) = default;

    ~Collidable();

    //! Returns the id of this object
    //!
    //! \return Id The id of this object
    constexpr Id GetId() const;

    //! Returns the collection of overlaps of this object with any object of the given type (excluding self)
    //!
    //! \tparam LaneOrRoad Type of the overlapping objects. Either Lane or Road
    //! \return An unsorted, possibly empty container of overlaps.
    template <typename LaneOrRoad>
    const std::vector<Overlap<LaneOrRoad>> &GetOverlaps() const;

    //! Returns the collection of overlaps of this object with any object of the given type (excluding self)
    //!
    //! \tparam LaneOrRoad Type of the overlapping objects. Either Lane or Road
    //! \return An unsorted, possibly empty container of overlaps.
    template <typename LaneOrRoad>
    std::vector<Overlap<LaneOrRoad>> &GetOverlaps();

    //! Returns the first overlap that satisfies the given predicate
    //!
    //! \tparam LaneOrRoad Lane or Road
    //! \tparam Pred Unary invocable receiving an overlap and returning a bool
    //! \param Pred
    //! \return Overlap satisfying the predicate or a nullptr if no such overlap exists
    template <typename LaneOrRoad, typename Pred>
    const Overlap<LaneOrRoad> *GetOverlap(Pred &&) const;

    //! Returns the first overlap that satisfies the given predicate
    //!
    //! \tparam LaneOrRoad Lane or Road
    //! \tparam Pred Unary invocable receiving an overlap and returning a bool
    //! \param Pred
    //! \return Overlap satisfying the predicate or a nullptr if no such overlap exists
    template <typename LaneOrRoad, typename Pred>
    Overlap<LaneOrRoad> *GetOverlap(Pred &&);

    //! Replaces the assigned overlaps of this object with overlaps of the given type.
    //!
    //! \tparam LaneOrRoad Type of the overlapping objects. Either Lane or Road
    //! \param overlaps An unsorted, possibly empty container of overlaps.
    template <typename LaneOrRoad>
    void SetOverlaps(std::vector<Overlap<LaneOrRoad>> &&overlaps);

    //! Returns the minimum and maximum lateral distance between this object and
    //! and any lane's centerline whose road this object touches. If this object touches no roads
    //! a default-constructed interval is returned, which holds the largest possible value
    //! as its minimum and the smallest possible value as its maximum.
    //!
    //! \tparam It Iterator pointing to a const Lane*
    //! \param firstLane Iterator pointing to the first element in the chain
    //! \param pastLastLane Iterator pointing past the last element in the chain
    //! \return Min and max t-coordinate difference. Positive values mean the point
    //! is to the left of the corresponding centerline (in driving direction).
    template <typename It>
    Interval<double> GetDeviation(It firstLane, It pastLastLane) const;

    //! Returns the minimum and maximum lateral distance between this object and
    //! and any lane's centerline whose road this object touches. If this object touches no roads
    //! a default-constructed interval is returned, which holds the largest possible value
    //! as its minimum and the smallest possible value as its maximum.
    //!
    //! \tparam LaneChain Iterable range of const Lane*
    //! \return Min and max t-coordinate difference. Positive values mean the point
    //! is to the left of the corresponding centerline (in driving direction).
    template <typename LaneChain>
    Interval<double> GetDeviation(const LaneChain &) const;

    //! Returns the lane chain of the first lane on the given route that this object overlaps with.
    //!
    //! \tparam Toward The direction towards which the lane chain is constructed
    //! \tparam T Direction of the route
    //! \param Route Iterable range of objects that hold a road accessible via osiql::get<Road>
    //! \return Empty if this object doesn't overlap with the given route
    template <Traversal Toward = Traversal::Any, Traversal T = Traversal::Forward>
    std::deque<const Lane *> GetLaneChain(const Route<T, Road> &) const;

    //! Returns the signed lateral movement this object would have to move leftward relative to the reference line
    //! of the nearby lane of the given lane chain in order to pass by the given target entity without collision.
    //! The min value is the distance to pass by the target's right side, the max is that to pass by its left side.
    //!
    //! \tparam Target Either an object of the same type as the caller, an iterable collection of road points,
    //! or a single point convertible to a road point.
    //! \param target Object, iterable collection of road point or a road point convertible type
    //! \param laneChain
    //! \return Minimum and maximum distance to barely avoid the target
    template <typename Target, typename LaneChain>
    Interval<double> GetObstruction(const Target &, const LaneChain &) const;

    //! Returns the weight of this object in kilograms.
    //!
    //! \return Weight of this object. If this object is a moving object with a source reference
    //! of type "osiql::HostVehicleData", the curb weight of this object's osi3::HostVehicleData
    //! will be returned. Otherwise, the volume of this object's local bounding box is returned.
    double GetMass() const;

private:
    std::vector<Overlap<Road>> roadOverlaps;
    std::vector<Overlap<Lane>> laneOverlaps;

    //! Id of this object
    //! \note Copied to allow identification after handle deletion.
    Id id;
};
} // namespace osiql
