/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Vehicle.h"

#include "MovingObject.tpp"
#include "OsiQueryLibrary/Street/Road.tpp"

namespace osiql {
template <Axle A>
bool Vehicle::HasAxle() const
{
    if constexpr (A == Axle::Front)
    {
        return GetHandle().has_vehicle_attributes() && GetHandle().vehicle_attributes().has_bbcenter_to_front();
    }
    else // Axle::Back
    {
        return GetHandle().has_vehicle_attributes() && GetHandle().vehicle_attributes().has_bbcenter_to_rear();
    }
}

template <Axle A, typename Vector>
Vector Vehicle::GetAxleOffset() const
{
    assert(GetHandle().has_vehicle_attributes());
    if constexpr (A == Axle::Front)
    {
        assert(GetHandle().vehicle_attributes().has_bbcenter_to_front());
        return GetHandle().vehicle_attributes().bbcenter_to_front();
    }
    else // A == Axle::Back
    {
        assert(GetHandle().vehicle_attributes().has_bbcenter_to_rear());
        return GetHandle().vehicle_attributes().bbcenter_to_rear();
    }
}

template <Axle A>
XY Vehicle::GetAxleXY() const
{
    return GetTransformationMatrix() * GetAxleOffset<A>();
}

template <Axle A>
std::vector<Point<const Lane>> Vehicle::GetLocalAxlePoints() const
{
    const XY axle{GetAxleXY<A>()};
    std::vector<Point<const Lane>> result;
    result.reserve(positions.size());
    for (const auto &overlap : GetOverlaps<Road>())
    {
        const ST axleST{overlap.GetReferenceLine().Localize(axle)};
        for (const Lane *lane : overlap.GetRoad().GetLanesContaining(axleST))
        {
            result.emplace_back(axleST, *lane);
        }
    }
    return result;
}

namespace detail {
constexpr std::array<std::string_view, Size<Axle>> axleToString{"Front", "Rear"};
constexpr std::array<std::string_view, Size<Vehicle::BrakeLightState>> brakeLightStateToString{
    "Unknown",
    "Other",
    "Off",
    "Normal",
    "Strong",
};
constexpr std::array<std::string_view, Size<Vehicle::GenericLightState>> genericLightStateToString{
    "Unknown",
    "Other",
    "Off",
    "On",
    "Flashing blue",
    "Flashing blue & red",
    "Flashing amber",
};
constexpr std::array<std::string_view, Size<Vehicle::IndicatorState>> indicatorStateToString{
    "Unknown",
    "Other",
    "Off",
    "Left",
    "Right",
    "Warning",
};
constexpr std::array<std::string_view, Size<Vehicle::VehicleType>> vehicleTypeToString{
    "Unknown",
    "Other",
    "Small car",
    "Compact car",
    "Medium car",
    "Luxury car",
    "Delivery van",
    "Heavy Truck",
    "Semi-trailer",
    "Trailer",
    "Motorbike",
    "Bicycle",
    "Bus",
    "Tram",
    "Train",
    "Wheelchair",
    "Semi-tractor",
};
constexpr std::array<std::string_view, Size<Vehicle::Role>> vehicleRoleToString{
    "Unknown",
    "Other",
    "Civil",
    "Ambulance",
    "Fire",
    "Police",
    "Public transport",
    "Road assistance",
    "Garbage collection",
    "Road construction",
    "Military",
};
} // namespace detail
} // namespace osiql
