/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Derived LaneAssignable which additionally provides distance and obstruction getters

#include "OsiQueryLibrary/Component/LaneAssignable.h"
#include "OsiQueryLibrary/Component/Locatable.h"
#include "OsiQueryLibrary/Types/Enum.h" // Traversal
#include "OsiQueryLibrary/Types/Interval.h"

namespace osiql {
template <typename, typename>
struct Point;
template <Traversal, typename>
struct Route;
//! An Object is an entity with a global shape that can perform distance queries from its position
//!
//! \tparam Handle Underlying OSI class that this object is a wrapper of.
template <typename Handle>
struct Object : LaneAssignable<Locatable<Handle>>
{
    using LaneAssignable<Locatable<Handle>>::LaneAssignable;

    //! Returns the smallest and largest signed Euclidean distance from this object to the chain of points specified
    //! by the given iterators. The first value represents how far the leftmost point of this object is to the right
    //! of the nearest edge (relative to that edge's orientation) while the second value represents the same for the
    //! rightmost point.
    //!
    //! \tparam It
    //! \param first
    //! \param pastLast
    //! \return Interval<double>
    template <typename It>
    Interval<double> GetSignedDistancesTo(It first, It pastLast) const;

    //! Returns the shortest driving distance along the road network in the specified direction
    //! such that the s-coordinates of the boundaries of this and the given object touch/overlap.
    //! A negative result indicates that the objects already overlap along the road and
    //! represents the inverse amount this object would need to drive backwards to cease intersecting.
    //!
    //! \tparam Traversal Forward, Backward or Any (Both)
    //! \tparam U Object type (MovingObject, Object, TrafficSign, TrafficLight, RoadMarking)
    //! \param other the object to which the shortest collision distance gets determined
    //! \return double
    template <Traversal = Traversal::Any, typename OtherHandle = Handle>
    double GetDistanceTo(const Object<OtherHandle> &other) const;

    //! Returns the shortest driving distance along the road network such that the center of this object
    //! shares the same s-coordinate as the given location.
    //!
    //! \tparam Traversal Forward, Backward or Any (Both)
    //! \param destination destination to which the distance is measured
    //! \return double positive distance or max if no route was found
    template <Traversal = Traversal::Any>
    double GetDistanceTo(const Point<const Lane, ST> &destination) const;

    //! Returns a pair of distances along the given route. The first is the distance this object needs to travel along
    //! this route to be right behind the earliest of the given points along the route. The second is the distance for
    //! this object to be just ahead of the furthest among the given points along the route.
    //!
    //! \tparam O Forward or Backward
    //! \tparam Scope Lane or Road. Scope of the route along which the obstruction is measured
    //! \param points Points of which the smallest and largest obstruction is returned
    //! \param route Chain of roads connecting this object and the given points
    //! \return Pair of distances where the smaller is the distance to collision and
    //! the larger is the distance to fully overtake all given points.
    template <Traversal T, typename Scope = Road>
    [[deprecated("Use Route::GetDistance instead")]] Interval<double> GetLongitudinalObstruction(
        const std::vector<Point<const Lane, ST>> &points,
        const Route<T, Scope> &route
    ) const;

    //! Returns whether the object is on any road of the given route.
    //!
    //! \tparam Route Iterable range of objects that have a Road accessible via osiql::get<Road>
    //! \return bool True only if the object is on any road of the given route.
    template <typename Route>
    bool IsOnRoute(const Route &) const;

    //! Returns whether the object is on a given range of roads
    //!
    //! \tparam It Iterator to a type T satisfying Is<Road>::in<T>
    //! \param first Iterator to the first element of the range
    //! \param pastLast Iterator past the last element of the range
    //! \return bool True only if the object is on any road of the given range of roads
    template <typename It>
    bool IsOnRoute(It first, It pastLast) const;
};
} // namespace osiql
