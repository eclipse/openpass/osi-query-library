/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "VehicleData.h"

#include "OsiQueryLibrary/Routing/Route.tpp"
#include "OsiQueryLibrary/Utility/Compare.tpp"
#include "OsiQueryLibrary/World.tpp"
#include "Vehicle.tpp"

namespace osiql {
template <typename Destination>
bool VehicleData::SetDestination(Destination &&destination)
{
    if (!HasRoute())
    {
        std::set<std::shared_ptr<Node<>>, Less<Road>> nodes;
        for (const auto &overlap : vehicle.GetOverlaps<Lane>())
        {
            nodes.insert(std::make_shared<Node<>>(overlap));
        }
        std::vector<std::shared_ptr<Route<>>> routes;
        routes.reserve(nodes.size());
        for (const auto &node : nodes)
        {
            if (const Node<> *goal{node->FindNode(destination)}; goal)
            {
                Point<> origin{ST{node->GetS(0.0), std::numeric_limits<double>::quiet_NaN()}, node->GetLane()};
                Point<> destination{ST{extract(destination, goal->GetDirection()), std::numeric_limits<double>::quiet_NaN()}, goal->GetLane()};
                routes.push_back(std::make_shared<Route<>>(origin, destination, goal->GetPathFromDepth(0)));
            }
        }
        if (!routes.empty())
        {
            route = *std::min_element(routes.begin(), routes.end(), Less<Length>{});
            return true;
        }
    }
    else if (const auto *goal{route->front()->FindNode(std::forward<Destination>(destination))}; goal)
    {
        const auto distance{goal->GetDistance(std::forward<Destination>(destination))};
        const double s{extract(goal->GetS(distance), !goal->GetDirection())};
        route = std::make_shared<Route<>>(
            route->origin,
            Point<const Lane>{ST{s, std::numeric_limits<double>::quiet_NaN()}, goal->GetLane()},
            goal->GetPathFromDepth(route->front()->depth)
        );
        return true;
    }
    return false;
}
} // namespace osiql
