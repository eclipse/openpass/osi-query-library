/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Derived Object which defines properties shared by osi3::TrafficSign::MainSign and osi3::TrafficSign::SupplementarySign

#include <iostream>

#include "OsiQueryLibrary/Trait/Size.h"
#include "OsiQueryLibrary/Types/Value.h"
#include "OsiQueryLibrary/Utility/Common.h"
#include "OsiQueryLibrary/Utility/Get.tpp"

namespace osiql {
//! \brief The variability of a traffic sign, meaning whether it can be moved or whether its display can be altered.
enum class Variability : std::underlying_type_t<osi3::TrafficSign_Variability>
{
    Unknown = osi3::TrafficSign_Variability_VARIABILITY_UNKNOWN,
    Other = osi3::TrafficSign_Variability_VARIABILITY_OTHER,
    Fixed = osi3::TrafficSign_Variability_VARIABILITY_FIXED,
    Variable = osi3::TrafficSign_Variability_VARIABILITY_VARIABLE,
    Movable = 4,          // osi3::TrafficSign_Variability_VARIABILITY_MOVABLE, // new in OSI 3.6.0
    Mutable = 5,          // osi3::TrafficSign_Variability_VARIABILITY_MUTABLE, // new in OSI 3.6.0
    MovableAndMutable = 6 // osi3::TrafficSign_Variability_VARIABILITY_MOVABLE_AND_MUTABLE // new in OSI 3.6.0
};
OSIQL_GET(Variability, GetVariability(), variability);

//! CRTP base class of the template type which provides the shared getters GetValue & GetVariability
//!
//! \tparam Type TrafficSign or SupplementarySign
template <typename Type>
struct CommonSign
{
    //! Returns the value coupled with its associated unit represented on this traffic sign.
    //! If no value is present on the sign, the default value (NaN with Unit::None) is returned.
    //!
    //! \return Value
    Value GetValue() const;

    //! Returns the variability of this traffic sign, meaning whether it can be moved or its display can be altered.
    //!
    //! \return Variability
    Variability GetVariability() const;
};

std::ostream &operator<<(std::ostream &, Variability);
} // namespace osiql
OSIQL_SET_SIZE(osiql::Variability, 7)
