/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Derived Object which additionally provides supplementary-traffic-sign-specific getters

#include <type_traits>

#include <osi3/osi_trafficsign.pb.h>

#include "CommonSign.h"
#include "OsiQueryLibrary/Component/Identifiable.h" // Wrapper
#include "OsiQueryLibrary/Trait/Size.h"
#include "OsiQueryLibrary/Utility/Get.tpp"

namespace osiql {
struct SupplementarySign : CommonSign<SupplementarySign>, Wrapper<osi3::TrafficSign_SupplementarySign>
{
    using Wrapper::Wrapper;

    SupplementarySign(const osi3::TrafficSign_SupplementarySign &);

    enum class Actor : std::underlying_type_t<osi3::TrafficSign_SupplementarySign_Classification_Actor>
    {
        Unknown = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_UNKNOWN,
        Other = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_OTHER,
        NoActor = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_NO_ACTOR,
        AgriculturalVehicles = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_AGRICULTURAL_VEHICLES,
        Bicycles = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_BICYCLES,
        Buses = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_BUSES,
        Campers = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CAMPERS,
        Caravans = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CARAVANS,
        Cars = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CARS,
        CarsWithCaravans = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CARS_WITH_CARAVANS,
        CarsWithTrailers = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CARS_WITH_TRAILERS,
        Cattle = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CATTLE,
        Children = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CHILDREN,
        ConstructionVehicles = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CONSTRUCTION_VEHICLES,
        DeliveryVehicles = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_DELIVERY_VEHICLES,
        DisabledPersons = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_DISABLED_PERSONS,
        Ebikes = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_EBIKES,
        ElectricVehicles = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_ELECTRIC_VEHICLES,
        EmergencyVehicles = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_EMERGENCY_VEHICLES,
        FerryUsers = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_FERRY_USERS,
        ForestryVehicles = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_FORESTRY_VEHICLES,
        HazardousGoodsVehicles = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_HAZARDOUS_GOODS_VEHICLES,
        HorseCarriages = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_HORSE_CARRIAGES,
        HorseRiders = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_HORSE_RIDERS,
        InlineSkaters = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_INLINE_SKATERS,
        MedicalVehicles = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_MEDICAL_VEHICLES,
        MilitaryVehicles = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_MILITARY_VEHICLES,
        Mopeds = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_MOPEDS,
        Motorcycles = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_MOTORCYCLES,
        MotorizedMultitrackVehicles = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_MOTORIZED_MULTITRACK_VEHICLES,
        OperationalAndUtilityVehicles = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_OPERATIONAL_AND_UTILITY_VEHICLES,
        Pedestrians = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_PEDESTRIANS,
        PublicTransportVehicles = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_PUBLIC_TRANSPORT_VEHICLES,
        RailroadTraffic = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_RAILROAD_TRAFFIC,
        Residents = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_RESIDENTS,
        SlurryTransport = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_SLURRY_TRANSPORT,
        Taxis = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TAXIS,
        Tractors = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRACTORS,
        Trailers = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRAILERS,
        Trams = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRAMS,
        Trucks = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRUCKS,
        TrucksWithSemitrailers = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRUCKS_WITH_SEMITRAILERS,
        TrucksWithTrailers = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRUCKS_WITH_TRAILERS,
        VehiclesWithGreenBadges = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_VEHICLES_WITH_GREEN_BADGES,
        VehiclesWithRedBadges = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_VEHICLES_WITH_RED_BADGES,
        VehiclesWithYellowBadges = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_VEHICLES_WITH_YELLOW_BADGES,
        WaterPollutantVehicles = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_WATER_POLLUTANT_VEHICLES,
        WinterSportspeople = osi3::TrafficSign_SupplementarySign_Classification_Actor_ACTOR_WINTER_SPORTSPEOPLE
    };
    Actor GetActor() const;

    enum class Type : std::underlying_type_t<osi3::TrafficSign_SupplementarySign_Classification_Type>
    {
        Unknown = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_UNKNOWN,
        Other = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_OTHER,
        NoSign = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_NO_SIGN,
        ValidForDistance = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_VALID_FOR_DISTANCE,
        ValidInDistance = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_VALID_IN_DISTANCE,
        TimeRange = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_TIME_RANGE,
        Weight = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_WEIGHT,
        Rain = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_RAIN,
        Fog = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_FOG,
        Snow = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_SNOW,
        SnowRain = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_SNOW_RAIN,
        LeftArrow = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_LEFT_ARROW,
        RightArrow = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_RIGHT_ARROW,
        LeftBendArrow = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_LEFT_BEND_ARROW,
        RightBendArrow = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_RIGHT_BEND_ARROW,
        Truck = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_TRUCK,
        TractorsMayBePassed = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_TRACTORS_MAY_BE_PASSED,
        Hazardous = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_HAZARDOUS,
        Trailer = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_TRAILER,
        Night = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_NIGHT,
        Zone = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_ZONE,
        Stop4Way = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_STOP_4_WAY,
        Motorcylce = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_MOTORCYCLE,
        MotorcycleAllowed = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_MOTORCYCLE_ALLOWED,
        Car = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_CAR,
        StopIn = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_STOP_IN,
        Time = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_TIME,
        PriorityRoadBottomLeftFourWay = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_BOTTOM_LEFT_FOUR_WAY,
        PriorityRoadTopLeftFourWay = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_TOP_LEFT_FOUR_WAY,
        PriorityRoadBottomRightFourWay = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_BOTTOM_RIGHT_FOUR_WAY,
        Arrow = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_ARROW,
        PriorityRoadTopRightFourWay = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_TOP_RIGHT_FOUR_WAY,
        PriorityRoadBottomLeftThreeWayStraight = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_BOTTOM_LEFT_THREE_WAY_STRAIGHT,
        PriorityRoadBottomLeftThreeWaySideways = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_BOTTOM_LEFT_THREE_WAY_SIDEWAYS,
        PriorityRoadTopLeftThreeWayStraight = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_TOP_LEFT_THREE_WAY_STRAIGHT,
        PriorityRoadBottomRightThreeWayStraight = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_BOTTOM_RIGHT_THREE_WAY_STRAIGHT,
        PriorityRoadBottomRightThreeWaySideway = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_BOTTOM_RIGHT_THREE_WAY_SIDEWAY,
        PriorityRoadTopRightThreeWayStraight = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_TOP_RIGHT_THREE_WAY_STRAIGHT,
        NoWaitingSideStripes = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_NO_WAITING_SIDE_STRIPES,
        Space = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_SPACE,
        Accident = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_ACCIDENT,
        Text = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_TEXT,
        ParkingConstraint = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_PARKING_CONSTRAINT,
        ParkingDiscTimeRestriction = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_PARKING_DISC_TIME_RESTRICTION,
        Wet = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_WET,
        Except = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_EXCEPT,
        ConstraintedTo = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_CONSTRAINED_TO,
        Services = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_SERVICES,
        RollingHighwayInformation = osi3::TrafficSign_SupplementarySign_Classification_Type_TYPE_ROLLING_HIGHWAY_INFORMATION,
    };
    Type GetType() const;
};

OSIQL_GET(SupplementarySign, GetSupplementarySign(), supplementarySign);
OSIQL_GET(SupplementarySign::Actor, GetActor(), actor, GetSupplementarySign(), supplementarySign);

std::ostream &operator<<(std::ostream &, SupplementarySign::Type);
std::ostream &operator<<(std::ostream &, const SupplementarySign &);
} // namespace osiql
OSIQL_SET_SIZE(osiql::SupplementarySign::Actor, 48)
OSIQL_SET_SIZE(osiql::SupplementarySign::Type, 49)
