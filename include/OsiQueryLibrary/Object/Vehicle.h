/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "MovingObject.h"
#include "OsiQueryLibrary/Trait/Size.h"
#include "OsiQueryLibrary/Utility/Get.tpp"

//! \file
//! \brief Derived MovingObject with additional getters for vehicle lights, steering wheel angle and other data

namespace osiql {
//! \brief A vehicle's axle
enum class Axle
{
    Front,
    Rear
};
OSIQL_GET(Axle, GetAxle(), axle);

//! \brief A moving object that contains vehicle specific information, e.g. vehicle type, brake-light states, etc.
struct Vehicle : MovingObject
{
    using MovingObject::MovingObject;

    //! Returns whether the given axle is present and defined in this vehicle
    //!
    //! \tparam Axle Front or Rear
    //! \return bool
    template <Axle>
    bool HasAxle() const;

    //! Returns the distance from the vehicle's center to the specified axle before any transformations are applied,
    //! meaning the x-coordinate represents the offset in the direction the vehicle is facing and the y-coordinate
    //! represents the offset towards its left side.
    //!
    //! \tparam Axle Front or Rear
    //! \tparam Vector Type constructible from const osi3::Vector3d&
    //! \return Vector Local offset relative to the center and rotation of this object.
    template <Axle, typename Vector = XY>
    Vector GetAxleOffset() const;

    //! Returns the global coordinates of the specified axle of this vehicle.
    //!
    //! \tparam Axle Front or Rear
    //! \return XY
    template <Axle>
    XY GetAxleXY() const;

    //! Returns the position of the specified axle localized to each road this vehicle touches.
    //!
    //! \tparam Axle Front or Rear
    //! \return Localizations of the axle point
    template <Axle>
    std::vector<Point<const Lane, ST>> GetLocalAxlePoints() const;

    //! \brief State of the brake light. Can be statically cast to the corresponding OSI light state
    enum class BrakeLightState : std::underlying_type_t<osi3::MovingObject_VehicleClassification_LightState_BrakeLightState>
    {
        Unknown = osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_UNKNOWN,
        Other = osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_OTHER,
        Off = osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_OFF,
        Normal = osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_NORMAL,
        Strong = osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_STRONG
    };
    //! Returns the state of this vehicle's brake lights
    //!
    //! \return BrakeLightState
    BrakeLightState GetBrakeLightState() const;

    //! \brief State of a vehicle light. Can be statically cast to the corresponding OSI light state
    enum class GenericLightState : std::underlying_type_t<osi3::MovingObject_VehicleClassification_LightState_GenericLightState>
    {
        Unknown = osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_UNKNOWN,
        Other = osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OTHER,
        Off = osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF,
        On = osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_ON,
        FlashingBlue = osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_FLASHING_BLUE,
        FlashingBlueAndRed = osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_FLASHING_BLUE_AND_RED,
        FlashingAmber = osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_FLASHING_AMBER,
    };
    //! Returns the state of this vehicle's front fog light
    //!
    //! \return GenericLightState
    GenericLightState GetFrontFogLightState() const;

    //! Returns the state of this vehicle's rear fog light
    //!
    //! \return GenericLightState
    GenericLightState GetRearFogLightState() const;

    //! Returns the state of this vehicle's head light
    //!
    //! \return GenericLightState
    GenericLightState GetHeadLightState() const;

    //! Returns the state of this vehicle's high beam light
    //!
    //! \return GenericLightState
    GenericLightState GetHighBeamState() const;

    //! Returns the state of this vehicle's reversing light
    //!
    //! \return GenericLightState
    GenericLightState GetReversingLightState() const;

    //! Returns the state of this vehicle's license plate illumination
    //!
    //! \return GenericLightState
    GenericLightState GetRearLicensePlateIlluminationState() const;

    //! Returns the state of this vehicle's emergency light, which is present in
    //! ambulances, police cars, etc.
    //!
    //! \return GenericLightState
    GenericLightState GetEmergencyVehicleIlluminationState() const;

    //! Returns the state of this vehicle's service light, which is present in snow
    //! removal vehicles, garbage trucks, towing trucks, etc.
    //!
    //! \return GenericLightState
    GenericLightState GetServiceVehicleIlluminationState() const;

    //! \brief State of a vehicle's turn or hazard warning indicator light. Can be statically cast to the corresponding OSI light state
    enum class IndicatorState : std::underlying_type_t<osi3::MovingObject_VehicleClassification_LightState_IndicatorState>
    {
        Unknown = osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_UNKNOWN,
        Other = osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_OTHER,
        Off = osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_OFF,
        Left = osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_LEFT,
        Right = osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_RIGHT,
        Warning = osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_WARNING
    };
    //! Returns the state of this vehicle's turn or hazard warning indicator light
    //!
    //! \return IndicatorState
    IndicatorState GetIndicatorState() const;

    //! \brief The perceived role of a vehicle, ergo the role it appears to have in the eyes of other traffic participants.
    //! Can be statically cast to the corresponding OSI role
    enum class Role : std::underlying_type_t<osi3::MovingObject_VehicleClassification_Role>
    {
        Unknown = osi3::MovingObject_VehicleClassification_Role_ROLE_UNKNOWN,
        Other = osi3::MovingObject_VehicleClassification_Role_ROLE_OTHER,
        Civil = osi3::MovingObject_VehicleClassification_Role_ROLE_CIVIL,
        Ambulance = osi3::MovingObject_VehicleClassification_Role_ROLE_AMBULANCE,
        Fire = osi3::MovingObject_VehicleClassification_Role_ROLE_FIRE,
        Police = osi3::MovingObject_VehicleClassification_Role_ROLE_POLICE,
        PublicTransport = osi3::MovingObject_VehicleClassification_Role_ROLE_PUBLIC_TRANSPORT,
        RoadAssistance = osi3::MovingObject_VehicleClassification_Role_ROLE_ROAD_ASSISTANCE,
        GarbageCollection = osi3::MovingObject_VehicleClassification_Role_ROLE_GARBAGE_COLLECTION,
        RoadConstruction = osi3::MovingObject_VehicleClassification_Role_ROLE_ROAD_CONSTRUCTION,
        Military = osi3::MovingObject_VehicleClassification_Role_ROLE_MILITARY,
    };
    //! Returns the perceived role of this vehicle
    //!
    //! \return Role
    Role GetRole() const;

    //! \brief Type of a vehicle. Can be statically cast to the corresponding OSI type
    enum class VehicleType : std::underlying_type_t<osi3::MovingObject_VehicleClassification_Type>
    {
        Unknown = osi3::MovingObject_VehicleClassification_Type_TYPE_UNKNOWN,
        Other = osi3::MovingObject_VehicleClassification_Type_TYPE_OTHER,
        SmallCar = osi3::MovingObject_VehicleClassification_Type_TYPE_SMALL_CAR,
        CompactCar = osi3::MovingObject_VehicleClassification_Type_TYPE_COMPACT_CAR,
        MediumCar = osi3::MovingObject_VehicleClassification_Type_TYPE_MEDIUM_CAR,
        LuxuryCar = osi3::MovingObject_VehicleClassification_Type_TYPE_LUXURY_CAR,
        DeliveryVan = osi3::MovingObject_VehicleClassification_Type_TYPE_DELIVERY_VAN,
        HeavyTruck = osi3::MovingObject_VehicleClassification_Type_TYPE_HEAVY_TRUCK,
        SemiTrailer = osi3::MovingObject_VehicleClassification_Type_TYPE_SEMITRAILER,
        Trailer = osi3::MovingObject_VehicleClassification_Type_TYPE_TRAILER,
        Motorbike = osi3::MovingObject_VehicleClassification_Type_TYPE_MOTORBIKE,
        Bicycle = osi3::MovingObject_VehicleClassification_Type_TYPE_BICYCLE,
        Bus = osi3::MovingObject_VehicleClassification_Type_TYPE_BUS,
        Tram = osi3::MovingObject_VehicleClassification_Type_TYPE_TRAM,
        Train = osi3::MovingObject_VehicleClassification_Type_TYPE_TRAIN,
        Wheelchair = osi3::MovingObject_VehicleClassification_Type_TYPE_WHEELCHAIR,
        SemiTractor = osi3::MovingObject_VehicleClassification_Type_TYPE_SEMITRACTOR
    };
    //! Returns the type of this vehicle
    //!
    //! \return VehicleType
    VehicleType GetVehicleType() const;

    //! Steering wheel angle relative to that of this vehicle.
    //!
    //! \return double counter-clockwise ascending angle in radians within [-π, π]
    double GetSteeringWheelAngle() const;

    //! \brief Trailer of this vehicle, which is nullptr if the vehicle has no attached trailer
    MovingObject *trailer{nullptr};
};

OSIQL_GET(Vehicle, GetVehicle(), vehicle, GetObject(), object);
OSIQL_GET(Vehicle::BrakeLightState, GetBrakeLightState(), brakeLightState);
OSIQL_GET(Vehicle::GenericLightState, GetGenericLightState(), genericLightState);
OSIQL_GET(Vehicle::IndicatorState, GetIndicatorState(), indicatorState);
OSIQL_GET(Vehicle::Role, GetRole(), role);
OSIQL_GET(Vehicle::VehicleType, GetVehicleType(), vehicleType);

std::ostream &operator<<(std::ostream &, Axle);
std::ostream &operator<<(std::ostream &, typename Vehicle::BrakeLightState);
std::ostream &operator<<(std::ostream &, typename Vehicle::GenericLightState);
std::ostream &operator<<(std::ostream &, typename Vehicle::IndicatorState);
std::ostream &operator<<(std::ostream &, typename Vehicle::Role);
std::ostream &operator<<(std::ostream &, typename Vehicle::VehicleType);
} // namespace osiql
OSIQL_SET_SIZE(osiql::Axle, 2)
OSIQL_SET_SIZE(osiql::Vehicle::BrakeLightState, 5)
OSIQL_SET_SIZE(osiql::Vehicle::GenericLightState, 7)
OSIQL_SET_SIZE(osiql::Vehicle::IndicatorState, 6)
OSIQL_SET_SIZE(osiql::Vehicle::Role, 11)
OSIQL_SET_SIZE(osiql::Vehicle::VehicleType, 17)
