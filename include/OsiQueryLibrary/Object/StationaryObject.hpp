/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! Forward declaration of a static object

#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Has.h"

namespace osiql {
struct StationaryObject;
OSIQL_GET(StationaryObject, GetStationaryObject(), stationaryObject, GetObject(), object);
OSIQL_HAS(StationaryObject, GetStationaryObject);
} // namespace osiql
