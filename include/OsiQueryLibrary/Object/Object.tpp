/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Object.h"

#include "MovingObject.h"
#include "OsiQueryLibrary/Component/LaneAssignable.tpp"
#include "OsiQueryLibrary/Component/Locatable.tpp"
#include "OsiQueryLibrary/Point/Point.tpp"
#include "OsiQueryLibrary/Routing/Node.h"
#include "OsiQueryLibrary/Routing/Route.h"
#include "OsiQueryLibrary/Street/Lane.tpp"
#include "OsiQueryLibrary/Types/Enum.tpp"
#include "OsiQueryLibrary/Types/Interval.tpp"

namespace osiql {
template <typename Handle>
template <typename It>
Interval<double> Object<Handle>::GetSignedDistancesTo(It first, It pastLast) const
{
    assert(first != pastLast);
    std::vector<double> distances(this->GetShape().size());
    std::transform(this->GetShape().begin(), this->GetShape().end(), distances.begin(), [a = first, z = pastLast](const XY &point) {
        return point.GetSignedDistanceToChain(a, z);
    });
    const auto [min, max]{std::minmax_element(distances.begin(), distances.end())};
    return {*min, *max};
}

template <typename Handle>
template <Traversal T, typename OtherHandle>
double Object<Handle>::GetDistanceTo(const Object<OtherHandle> &other) const
{
    if constexpr (T == Traversal::Any)
    { // TODO: Interlaced bidirectional search
        const double forwardDistance{GetDistanceTo<Traversal::Forward>(other)};
        const double backwardDistance{GetDistanceTo<Traversal::Backward>(other)};
        if (forwardDistance < 0.0 && backwardDistance < 0.0)
        {
            return 0.0;
        }
        return (forwardDistance <= std::max(backwardDistance, 0.0)) ? forwardDistance : -backwardDistance;
    }
    else // TODO: Make all object types collidable so that this implementation can be removed
    {
        double result{std::numeric_limits<double>::max()};
        const double range{this->GetSize().SquaredLength()};
        const double otherRange{other.GetSize().SquaredLength()};
        for (const auto &position : this->positions)
        {
            const auto rearStartPoint{this->GetFurthestPoint(get<Lane>(position), !T)};
            const auto frontStartPoint{this->GetFurthestPoint(get<Lane>(position), T)};
            const double startOffset{rearStartPoint.GetDistanceTo(frontStartPoint, range, T)};
            const Node<T> origin{rearStartPoint};
            for (const auto &destination : other.positions)
            {
                const auto rearEndPoint{other.GetFurthestPoint(get<Lane>(destination), !T)};
                if (const Node<T> *goal{origin.FindClosestNode(rearEndPoint)}; goal)
                {
                    const auto frontEndPoint{other.GetFurthestPoint(get<Lane>(destination), T)};
                    const double endOffset{rearEndPoint.GetDistanceTo(frontEndPoint, otherRange * otherRange, T)};
                    const double totalOffset{startOffset + endOffset};
                    const double distance{goal->GetDistance(rearEndPoint)};
                    if (distance <= totalOffset)
                    {
                        return distance - totalOffset;
                    }
                    result = std::min(result, distance - totalOffset);
                }
            }
        }
        return result;
    }
}

template <typename Handle>
template <Traversal T>
double Object<Handle>::GetDistanceTo(const Point<const Lane> &destination) const
{
    if constexpr (T == Traversal::Any)
    {
        const double forwardDistance{GetDistanceTo<Traversal::Forward>(destination)};
        const double backwardDistance{GetDistanceTo<Traversal::Backward>(destination)};
        return (forwardDistance <= backwardDistance) ? forwardDistance : -backwardDistance;
    }
    else
    {
        double result{std::numeric_limits<double>::max()};
        const double range{this->GetSize().SquaredLength()};
        for (const auto &origin : this->positions)
        {
            const auto rearStartPoint{this->GetFurthestPoint(get<Lane>(origin), !T)};
            const auto frontStartPoint{this->GetFurthestPoint(get<Lane>(origin), T)};
            const double startOffset{rearStartPoint.GetDistanceTo(frontStartPoint, range, T)};
            Node<T> start{rearStartPoint};
            if (const auto *goal{start.FindClosestNode(destination)}; goal)
            {
                const double distance{goal->GetDistance(destination)};
                if (distance <= startOffset)
                {
                    return 0.0;
                }
                result = std::min(result, distance - startOffset);
            }
        }
        return result;
    }
}

template <typename Handle>
template <Traversal T, typename Scope>
Interval<double> Object<Handle>::GetLongitudinalObstruction(const std::vector<Point<const Lane>> &points, const Route<T, Scope> &route) const
{
    // Iterator to the route point whose lane's road contains this object
    const auto it{std::find_if(this->positions.begin(), this->positions.end(), [&](const auto &position) {
        return std::find_if(route.begin(), route.end(), Matches<Road>{position}) != route.end();
    })};
    if (it == this->positions.end())
    {
        std::cerr << "GetObstruction: Object is not on the given route. Returning pair of NaNs.\n";
        return {std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};
    }
    const std::vector<Point<const Lane>> shape{this->GetLocalShape(get<Lane>(*it))};
    std::vector<double> selfDistances(shape.size());
    std::transform(shape.begin(), shape.end(), selfDistances.begin(), [&](const Point<const Lane> &point) {
        return route.GetDistance(point);
    });
    std::vector<double> targetDistances(points.size());
    std::transform(points.begin(), points.end(), targetDistances.begin(), [&](const Point<const Lane> &point) {
        return route.GetDistance(point);
    });
    const auto [minA, maxA]{std::minmax_element(selfDistances.begin(), selfDistances.end())};
    const auto [minB, maxB]{std::minmax_element(selfDistances.begin(), selfDistances.end())};
    return {*minB - *maxA, *maxB - *minA};
}

template <typename Handle>
template <typename Route>
bool Object<Handle>::IsOnRoute(const Route &route) const
{
    return IsOnRoute(route.begin(), route.end());
}

template <typename Handle>
template <typename It>
bool Object<Handle>::IsOnRoute(It first, It pastLast) const
{
    return std::any_of(first, pastLast, [&](const auto &waypoint) { // clang-format off
        return std::any_of(this->positions.begin(), this->positions.end(), Matches<Road>{waypoint});
    }); // clang-format on
}
} // namespace osiql
