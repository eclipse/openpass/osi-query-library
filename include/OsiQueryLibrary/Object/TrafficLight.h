/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Derived Object which consists of one or more light bulbs (osi3::TrafficLight)

#include <osi3/osi_trafficlight.pb.h>

#include "Object.h"
#include "OsiQueryLibrary/Types/Common.h"
#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Has.h"

namespace osiql {
//! \brief Class that represents common traffic light types(e.g. three lights, one-light bicycle, etc.) and state(off, green, etc).
struct TrafficLight : Object<osi3::TrafficLight>
{
    static constexpr std::string_view name{"Traffic Light"};
    //! Constructs a TrafficLight from a set of bulbs
    //!
    //! \param bulbs
    TrafficLight(std::vector<const osi3::TrafficLight *> &&bulbs);

    // TODO: Type and State

    //! \brief The individual light bulbs that make up a traffic light
    std::vector<const osi3::TrafficLight *> bulbs;
};

OSIQL_GET(TrafficLight, GetTrafficLight(), trafficLight, GetObject(), object);

std::ostream &operator<<(std::ostream &, const TrafficLight &);

template <typename Type>
struct Has<TrafficLight, Type> : detail::Has<TrafficLight, Type>
{
    using detail::Has<TrafficLight, Type>::Has;

    constexpr const TrafficLight &GetTrafficLight() const
    {
        return Has<TrafficLight, Type>::template Get<TrafficLight>();
    }

    template <typename _ = Type, OSIQL_REQUIRES(!is_const<_>)>
    constexpr TrafficLight &GetTrafficLight()
    {
        return Has<TrafficLight, Type>::template Get<TrafficLight>();
    }

    constexpr const std::vector<const osi3::TrafficLight *> &GetLightBulbs() const
    {
        return GetTrafficLight().bulbs;
    }

    template <typename _ = Type, OSIQL_REQUIRES(!is_const<_>)>
    constexpr std::vector<const osi3::TrafficLight *> &GetLightBulbs()
    {
        return GetTrafficLight().bulbs;
    }
};

template <typename Type>
struct Has<Assignment<TrafficLight>, Type> : detail::Has<Assignment<TrafficLight>, Type>
{
    using detail::Has<Assignment<TrafficLight>, Type>::Has;

    constexpr const TrafficLight &GetTrafficLight() const
    {
        return Has<Assignment<TrafficLight>, Type>::template Get<TrafficLight>();
    }

    template <typename _ = Type, OSIQL_REQUIRES(!is_const<_>)>
    constexpr TrafficLight &GetTrafficLight()
    {
        return Has<Assignment<TrafficLight>, Type>::template Get<TrafficLight>();
    }

    constexpr const std::vector<const osi3::TrafficLight *> &GetLightBulbs() const
    {
        return GetTrafficLight().bulbs;
    }

    template <typename _ = Type, OSIQL_REQUIRES(!is_const<_>)>
    constexpr std::vector<const osi3::TrafficLight *> &GetLightBulbs()
    {
        return GetTrafficLight().bulbs;
    }
};
} // namespace osiql
