/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Derived Object which additionally provides road-marking-specific getters

#include <osi3/osi_roadmarking.pb.h>

#include "Object.h"
#include "OsiQueryLibrary/Trait/Size.h"
#include "OsiQueryLibrary/Types/Value.h"
#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Has.h"
#include "TrafficSign.h"

namespace osiql {
//! \brief A RoadMarking is a traffic sign mapped onto the surface of a road
struct RoadMarking : Object<osi3::RoadMarking>
{
    static constexpr std::string_view name{"Road Marking"};

    using Object<osi3::RoadMarking>::Object;

    //! \brief Type of the RoadMarking
    enum class Type : std::underlying_type_t<osi3::RoadMarking::Classification::Type>
    {
        Undefined = osi3::RoadMarking_Classification_Type_TYPE_UNKNOWN,
        Other = osi3::RoadMarking_Classification_Type_TYPE_OTHER,
        PaintedTrafficSign = osi3::RoadMarking_Classification_Type_TYPE_PAINTED_TRAFFIC_SIGN,
        SymbolicTrafficSign = osi3::RoadMarking_Classification_Type_TYPE_SYMBOLIC_TRAFFIC_SIGN,
        TextualTrafficSign = osi3::RoadMarking_Classification_Type_TYPE_TEXTUAL_TRAFFIC_SIGN,
        GenericSymbol = osi3::RoadMarking_Classification_Type_TYPE_GENERIC_SYMBOL,
        GenericLine = osi3::RoadMarking_Classification_Type_TYPE_GENERIC_LINE,
        GenericText = osi3::RoadMarking_Classification_Type_TYPE_GENERIC_TEXT
    };
    //! Returns the type of this road marking.
    //!
    //! \return Type
    Type GetType() const;

    //! Returns the type of traffic sign this road marking represents if this road marking has the type
    //! PaintedTrafficSign, SymbolicTrafficSign or TextualTrafficSign. Otherwise, Type::Undefined is returned.
    //!
    //! \return TrafficSign::Type
    TrafficSign::Type GetSignType() const;

    //! Returns the value coupled with its associated unit represented by this road marking.
    //! If no value is present, the default value (NaN with Unit::None) is returned.
    //!
    //! \return Value
    Value GetValue() const;
};
OSIQL_GET(RoadMarking, GetRoadMarking(), roadMarking, GetObject(), object);
OSIQL_GET(RoadMarking::Type, GetType(), type, GetRoadMarking(), roadMarking);

std::ostream &operator<<(std::ostream &, RoadMarking::Type);
std::ostream &operator<<(std::ostream &, const RoadMarking &);

OSIQL_HAS(RoadMarking, GetRoadMarking);
OSIQL_HAS(Assignment<RoadMarking>, GetRoadMarking);
} // namespace osiql
OSIQL_SET_SIZE(osiql::RoadMarking::Type, 8)

namespace osiql::detail {
constexpr std::array<std::string_view, Size<RoadMarking::Type>> roadMarkingTypeToString{
    "Undefined",
    "Other",
    "Painted traffic sign",
    "Symbolic traffic sign",
    "Textual traffic sign",
    "Generic symbol",
    "Generic line",
    "Generic text",
};
}
