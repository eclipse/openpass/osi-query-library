/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <iostream>

#include <osi3/osi_object.pb.h>

#include "Collidable.h"
#include "Object.h"

namespace osiql {
//! \brief A stationary object that will never move.
struct StaticObject : Object<osi3::StationaryObject>, Collidable<StaticObject>
{
    using Object<osi3::StationaryObject>::Object;
};

std::ostream &operator<<(std::ostream &, const StaticObject &);
} // namespace osiql
