/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once
#include <osi3/osi_object.pb.h>

#include "Collidable.h"
#include "MovingObject.hpp"
#include "Object.h"
#include "OsiQueryLibrary/Trait/Size.h" // OSIQL_SET_SIZE
#include "OsiQueryLibrary/Types/Enum.h" // Traversal

namespace osiql {
struct Lane;

//! \brief A pair of relative distances to the left and right
struct Distances
{
    double left;
    double right;
};

std::ostream &operator<<(std::ostream &, const Distances &);

//! \brief A MovingObject is an osiql::Object that has velocity and acceleration and can generate routes from its location
struct MovingObject : Object<osi3::MovingObject>, Collidable<MovingObject>
{
    static constexpr std::string_view name{"Moving Object"};

    template <typename Base, OSIQL_REQUIRES(std::is_constructible_v<Object<osi3::MovingObject>, Base>)>
    constexpr MovingObject(Base &&base) : // NOLINT(bugprone-forwarding-reference-overload)
        Object<osi3::MovingObject>{std::forward<Base>(base)}, Collidable<MovingObject>{Object<osi3::MovingObject>::GetId()}
    {
    }

    using Collidable<MovingObject>::GetId;

    //! Replaces the handle of this object
    //!
    //! \return This object after having replaced its handle
    MovingObject &operator=(const osi3::MovingObject &);

    //! Returns whether this object is moving in the specified traversal direction
    //!
    template <Traversal T = Traversal::Any>
    bool IsMoving() const;

    //! Returns the global directional velocity of this object or a zero vector if not assigned
    //!
    //! \return Global point type. Either XY or XYZ
    template <typename Vector = XYZ>
    Vector GetVelocity() const;

    //! Returns the change in position of the given local object point after traveling one second
    //! going only by the object's current velocity and angular velocity.
    //!
    //! \param localPoint Point relative to this object's origin
    //! \return Point type with an x- & y-coordinate, optionally with a z-coordinate
    template <typename Point = XY>
    Vector<Dimensions<Point>> GetVelocity(const Point &localPoint) const;

    //! Returns the spin, aka angular velocity of this object. Each component represents the change of
    //! rotation per second around the corresponding axis parallel passing through the object's origin.
    //!
    //! \return Rotation Counter-clockwise ascending change in angle around each axis in radians per second
    Rotation GetAngularVelocity() const;

    //! Returns the change in roll (rotation around x-axis in OSI) per second of this object.
    //!
    //! \return double counter-clockwise ascending angle in radians per second
    double GetRollVelocity() const;

    //! Returns the change in pitch (rotation around y-axis in OSI) per second of this object.
    //!
    //! \return double counter-clockwise ascending angle in radians per second
    double GetPitchVelocity() const;

    //! Returns the change in yaw (rotation around z-axis in OSI) per second of this object.
    //!
    //! \return double counter-clockwise ascending angle in radians per second
    double GetYawVelocity() const;

    //! Returns a pseudovector representing the product of the object's rotational inertia
    //! and rotational velocity (in radians/sec) at the given point relative to the object
    //! center. In layman's terms, this is the distance the point would travel per second
    //! based on its spinning speed if it were not forced to rotate around the object center.
    //!
    //! \param localPoint Point relative to this object's origin
    //! \return XYZ Current spin direction and intensity.
    template <typename Point = XY>
    Vector<Dimensions<Point>> GetAngularMomentum(const Point &localPoint) const;

    //! Returns the directional acceleration of this object or a zero vector if not assigned.
    //!
    //! \return Vector Global point type. Either XY or XYZ
    template <typename Vector = XY>
    Vector GetAcceleration() const;

    //! Returns the spin angular acceleration of this object. Each component represents the change in change of
    //! rotation per second around the corresponding axis parallel passing through the object's origin.
    //!
    //! \return Rotation Counter-clockwise ascending change in angle around each axis in radians per second squared
    Rotation GetAngularAcceleration() const;

    //! Returns this object's change in change in roll (rotation around x-axis in OSI) per second.
    //!
    //! \return double counter-clockwise ascending angle in radians per second within [-π, π]
    double GetRollAcceleration() const;

    //! Returns this object's change in change in pitch (rotation around y-axis in OSI) per second.
    //!
    //! \return double counter-clockwise ascending angle in radians per second within [-π, π]
    double GetPitchAcceleration() const;

    //! Returns this object's change in change in yaw (rotation around z-axis in OSI) per second.
    //!
    //! \return double counter-clockwise ascending angle in radians per second within [-π, π]
    double GetYawAcceleration() const;

    //! Returns a pseudovector representing the product of the object's rotational inertia
    //! and rotational acceleration (in radians/sec) at the given point relative to the object
    //! center. In layman's terms, this is the distance the point would travel additionally per second
    //! based on its spinning acceleration if it were not forced to rotate around the object center.
    //!
    //! \param localPoint Point relative to this object's origin prior to transformation
    //! \return Global vector representing this object's current spin acceleration direction and intensity.
    template <typename Point = XY>
    Vector<Dimensions<Point>> GetAngularImpulse(const Point &localPoint) const;

    //! Returns the global change in velocity of the given local object point after one second
    //! going only by the object's current translational acceleration and angular acceleration.
    //!
    //! \param localPoint Point relative to this object's origin prior to transformation
    //! \return Acceleration after transformation (based on global coordinates rather than local coordinates),
    //! which includes a z-coordinate only if the input did as well.
    template <typename Point = XY>
    Vector<Dimensions<Point>> GetAcceleration(const Point &localPoint) const;

    //! \brief Type of a moving object, describing whether it's an animal, pedestrian, vehicle or something else
    enum class Type : std::underlying_type_t<osi3::MovingObject_Type>
    {
        Unknown = osi3::MovingObject_Type_TYPE_UNKNOWN,
        Other = osi3::MovingObject_Type_TYPE_OTHER,
        Vehicle = osi3::MovingObject_Type_TYPE_VEHICLE,
        Pedestrian = osi3::MovingObject_Type_TYPE_PEDESTRIAN,
        Animal = osi3::MovingObject_Type_TYPE_ANIMAL
    };

    //! Returns the type of moving object this object is, which may affect which lanes it moves on,
    //! in what manner it navigates them, and how it is perceived by other objects.
    //!
    //! \return Type
    Type GetType() const;

    //! Returns this object's angular velocity as an antimetric matrix. Multiplying this matrix with a local
    //! point will return a vector representing the rotational force that currently applies to said local point.
    //!
    //! \return 3x3 antimetric matrix
    const Matrix<3> &GetSpinMatrix() const;

    //! Returns a transformation matrix that converts a local point relative to this object's origin to where it will
    //! be in one second with the current linear velocity (ignoring spin & acceleration) in global xyz-coordinates.
    //!
    //! \return 4x4 translation matrix
    const Matrix<4> &GetVelocityMatrix() const;

    //! Returns the combined spin and velocity matrix.
    //!
    //! \return 4x4 translation matrix
    const Matrix<4> &GetLocalMovementMatrix() const;

    //! Returns the combined spin, rotation and velocity matrix.
    //!
    //! \return 4x4 translation matrix
    const Matrix<4> &GetGlobalMovementMatrix() const;

    //! Returns the combined spin and rotation matrix
    //!
    //! \return 3x3 rotation matrix combined with an antimetric spin matrix
    const Matrix<3> &GetSpinAndRotationMatrix() const;

    //! Returns a transformation matrix that converts a local point relative to this object's origin
    //! to where it will be in one second with the current linear acceleration (ignoring spin, velocity &
    //! angular velocity) in global xyz-coordinates.
    //!
    //! \return 4x4 translation matrix
    const Matrix<4> &GetAccelerationMatrix() const;

    //! Returns this object's angular acceleration as an antimetric matrix.
    //! Multiplying it with a local point will return a vector representing
    //! the rotational acceleration that currently applies to said local point.
    //!
    //! \return 3x3 antimetric matrix
    const Matrix<3> &GetAngularAccelerationMatrix() const;

    //! Returns the combined translational and rotational acceleration matrices.
    //!
    //! \return 4x4 transformation matrix
    const Matrix<4> &GetFullAccelerationMatrix() const;

    //! Returns the momentum of this object, which is its mass multiplied with its velocity.
    //!
    //! \note Does not include angular velocity (spin)
    //! \tparam Vector Type of the returned vector. For instance XY or XYZ
    //! \return Vector Multi-dimensional vector representing this object's momentum
    template <typename Vector = XY>
    Vector GetMomentum() const;

    //! Returns the force of this object, which is its mass multiplied with its acceleration.
    //!
    //! \note Does not include angular acceleration (twirl)
    //! \tparam Vector Type of the returned vector. For instance XY or XYZ
    //! \return Vector Multi-dimensional vector representing this object's force
    template <typename Vector = XY>
    Vector GetForce() const;

    template <Traversal = Traversal::Any, typename Destination = Point<const Lane, ST>>
    double GetDistanceTo(const Destination &) const;

    //! Returns the lateral distance from this object to the left and right boundary of the given lane.
    //! Throws if this object does not lie on the same road as the given lane.
    //!
    //! \tparam Towards Direction or Traversal
    //! \param Lane The lane for which to compute the boundary distances
    //! \return Distances A pair of signed left and right distances
    template <auto Towards = Traversal::Forward>
    Distances GetBoundaryDistances(const Lane &) const;

    //! Returns the shortest lateral distance from this object to the left and right boundary of
    //! the given lane. Throws if this object does not lie on the same road as the given lane.
    //!
    //! \tparam Towards Direction or Traversal
    //! \param Towards What constitutes the forward direction. Left and right are relative to that direction
    //! \param lane The lane for which to compute the boundary distances
    //! \return Distances A pair of signed left and right distances
    template <typename Towards>
    Distances GetBoundaryDistances(Towards, const Lane &) const;

    //! Returns the difference in t-coordinate between this object and the given lane's left and right boundary.
    //! Note: The implementation only checks the t-coordinate differences at the object's greatest and smallest
    //! s-coordinate.
    //!
    //! \param Lane
    //! \param Traversal Forward or Backward
    //! \return Distances
    Distances GetApproximateBoundsDistancesToBoundaries(const Lane &, Traversal = Traversal::Forward) const;

protected:
    void OutdateRotation();

    void OutdateSpin();

    void OutdateVelocity();

private:
    mutable std::optional<Matrix<3>> spinMatrix;
    mutable std::optional<Matrix<3>> spinAndRotationMatrix;

    mutable std::optional<Matrix<4>> velocityMatrix;
    mutable std::optional<Matrix<4>> localMovementMatrix;
    mutable std::optional<Matrix<4>> globalMovementMatrix;

    mutable std::optional<Matrix<4>> accelerationMatrix;
    mutable std::optional<Matrix<3>> angularAccelerationMatrix;
    mutable std::optional<Matrix<4>> fullAccelerationMatrix;
};

OSIQL_GET(MovingObject::Type, GetType(), type, GetMovingObject());

std::ostream &operator<<(std::ostream &, const MovingObject &);
std::ostream &operator<<(std::ostream &, MovingObject::Type);

OSIQL_HAS(Assignment<MovingObject>, GetMovingObject);

template <typename Position = XY, OSIQL_REQUIRES(Are<X, Y>::in<Position>)>
constexpr void SetPosition(osi3::Vector3d &output, Position &&input)
{
    output.set_x(X{}(input));
    output.set_y(Y{}(input));
    output.set_z(Z{}(input));
}

template <typename Position = XY, OSIQL_REQUIRES(Are<X, Y>::in<Position>)>
constexpr void SetPosition(osi3::BaseMoving &output, Position &&input)
{
    SetPosition(*output.mutable_position(), std::forward<Position>(input));
}

template <typename Position = XY, OSIQL_REQUIRES(Are<X, Y>::in<Position>)>
constexpr void SetPosition(osi3::MovingObject &object, Position &&position)
{
    SetPosition(*object.mutable_base(), std::forward<Position>(position));
}

void SetDimensions(osi3::Dimension3d &output, double length, double width, double height = 0.0);  // NOLINT(bugprone-easily-swappable-parameters)
void SetDimensions(osi3::BaseMoving &output, double length, double width, double height = 0.0);   // NOLINT(bugprone-easily-swappable-parameters)
void SetDimensions(osi3::MovingObject &output, double length, double width, double height = 0.0); // NOLINT(bugprone-easily-swappable-parameters)
} // namespace osiql
OSIQL_SET_SIZE(osiql::MovingObject::Type, 5)
