/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Wrapper of osi3::HostVehicleData representing a vehicle alongside its route

#include <osi3/osi_hostvehicledata.pb.h>

#include "OsiQueryLibrary/Routing/Route.h"
#include "OsiQueryLibrary/Utility/Get.tpp"

namespace osiql {
struct Vehicle;

//! Wrapper of an osi3::HostVehicleData. Currently does not support the container osi3::Route, but
//! can hold a separate osiql::Route instead.
//!
struct VehicleData
{
    VehicleData(const Vehicle &);
    VehicleData(const Vehicle &, std::shared_ptr<Route<>> &);

    //! Refreshes the touched node(s) and distances of this vehicle on its assigned route.
    //! Throws an error if this vehicle has no assigned route.
    //!
    void UpdateOnRoute();

    //! Returns whether this vehicle has an assigned route.
    //! This route is separate from the host vehicle data's OSI route.
    //! To world the OSI route, use GetHandle().has_route().
    //!
    //! \return Whether this vehicle holds a route
    bool HasRoute() const;

    //! Returns whether this vehicle has an assigned route and is currently somewhere on that route.
    //!
    //! \return Whether this object overlaps with any of the roads of its assigned route.
    bool IsOnRoute() const;

    //! Returns the route assigned to this vehicle. This route is separate
    //! from the intended OSI route stored inside the OSI host vehicle data,
    //! which can be accessed via GetHandle().route().
    //! \note Throws an error if this vehicle has no assigned route.
    //!
    //! \return The route assigned to this vehicle
    const Route<> &GetRoute() const;

    //! Assigns the route of this vehicle
    //!
    void SetRoute(std::shared_ptr<Route<>>);

    //! Assigns the route of this vehicle. If this vehicle already has an assigned route
    //! its destination is replaced with the
    //!
    //! \tparam Destination
    //! \return true
    //! \return false
    template <typename Destination>
    bool SetDestination(Destination &&);

    Interval<ConstIterator<Route<>>> GetTouchedNodes() const;

    //! Returns an iterator to the earliest node this vehicle touches along its assigned route.
    //! \note Throws an error if this vehicle has no assigned route
    //!
    //! \return Iterator to the last touched node. If there is no such node,
    //! an iterator past the last node of the route is returned.
    ConstIterator<Route<>> GetFirstNodeIterator() const;

    //! Returns an iterator to the furthest node this vehicle touches along its assigned route.
    //! \note Throws an error if this vehicle has no assigned route
    //!
    //! \return Iterator to the last touched node. If there is no such node,
    //! an iterator past the last node of the route is returned.
    ConstIterator<Route<>> GetLastNodeIterator() const;

    //! Returns the furthest node along this vehicle's route that his vehicle overlaps with.
    //! \note Throws an error if the vehicle does not lie on their route or has no assigned route.
    //!
    //! \return Last node of this vehicle's route that this vehicle overlaps with
    const Node<> &GetFirstTouchedNode() const;

    //! Returns the furthest node along this vehicle's route that his vehicle overlaps with.
    //! \note Throws an error if the vehicle does not lie on their route or has no assigned route.
    //!
    //! \return Last node of this vehicle's route that this vehicle overlaps with
    const Node<> &GetLastTouchedNode() const;

    const Vehicle &vehicle; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)
    Interval<double> distance;

protected:
    std::shared_ptr<Route<>> route{nullptr};
    Interval<std::ptrdiff_t> nodes;
};
OSIQL_GET(VehicleData, GetVehicleData(), vehicleData);

enum class OperatingState : std::underlying_type_t<osi3::HostVehicleData_VehicleBasics_OperatingState>
{
    Unknown = osi3::HostVehicleData_VehicleBasics_OperatingState_OPERATING_STATE_UNKNOWN,             // 0
    Other = osi3::HostVehicleData_VehicleBasics_OperatingState_OPERATING_STATE_OTHER,                 // 1
    Sleep = osi3::HostVehicleData_VehicleBasics_OperatingState_OPERATING_STATE_SLEEP,                 // 2
    Standby = osi3::HostVehicleData_VehicleBasics_OperatingState_OPERATING_STATE_STANDBY,             // 3
    Boarding = osi3::HostVehicleData_VehicleBasics_OperatingState_OPERATING_STATE_BOARDING,           // 4
    Entertainment = osi3::HostVehicleData_VehicleBasics_OperatingState_OPERATING_STATE_ENTERTAINMENT, // 5
    Driving = osi3::HostVehicleData_VehicleBasics_OperatingState_OPERATING_STATE_DRIVING,             // 6
    Diagnostic = osi3::HostVehicleData_VehicleBasics_OperatingState_OPERATING_STATE_DIAGNOSTIC,       // 7
};
OSIQL_GET(OperatingState, GetOperatingState(), operatingState);

struct SteeringWheel : Wrapper<osi3::VehicleSteeringWheel>
{
    using Wrapper<osi3::VehicleSteeringWheel>::Wrapper;

    //! Returns the angle of the steering wheel relative to its center position.
    //!
    //! \return Steering wheel angle. Positive if left of center, negative if right of center.
    double GetAngle() const;

    //! Returns the change in angle of the steering wheel per second.
    //!
    //! \return Rotational velocity of the steering wheel. Positive if rotating counter-clockwise,
    //! negative if rotating clockwise.
    double GetAngularSpeed() const;

    //! Returns the force the steering wheel applies on the hands of a driver
    //!
    //! \return Positive if the wheel would turn to the left, negative if to the right.
    double GetTorque() const;
};

struct HostVehicleData : VehicleData, Identifiable<osi3::HostVehicleData>
{
    HostVehicleData(const osi3::HostVehicleData &, const Vehicle &);
    HostVehicleData(const osi3::HostVehicleData &, const Vehicle &, std::shared_ptr<Route<>> &);

    //! Returns the acceleration pedal position of this vehicle
    //!
    //! \return Position of the acceleration pedal from unpressed to fully pressed in the range [0, 1]
    double GetAccelerationPedalPosition() const;

    //! Returns the brake pedal position of this vehicle
    //!
    //! \return Position of the brake pedal from unpressed to fully pressed in the range [0, 1]
    double GetBrakePedalPosition() const;

    //! Returns the clutch pedal position of this vehicle
    //!
    //! \return Position of the clutch pedal from unpressed to fully pressed in the range [0, 1]
    double GetClutchPedalPosition() const;

    SteeringWheel GetSteeringWheel() const;

    //! Returns the total mass of this vehicle
    //!
    //! \return Curb weight of the vehicle in kg
    double GetVehicleWeight() const;

    //! Returns the operating state of this vehicle
    //!
    //! \return The state of the vehicle
    OperatingState GetVehicleState() const;
};
OSIQL_GET(HostVehicleData, GetHostVehicleData(), hostVehicleData);
} // namespace osiql
