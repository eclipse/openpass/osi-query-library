/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Enum.h"

#include "Enum/Direction.tpp"
#include "Enum/Side.tpp"
#include "Enum/Traversal.tpp"

namespace osiql {
namespace detail {
constexpr std::array<std::array<Direction, 3>, 5> combinedDirectionAndOrientation{
    //                       Traversal::Any, Traversal::Forward, Traversal::Backward
    std::array<Direction, 3>{Direction::Unknown, Direction::Unknown, Direction::Unknown},           // Direction::Unknown
    std::array<Direction, 3>{Direction::Other, Direction::Other, Direction::Other},                 // Direction::Other
    std::array<Direction, 3>{Direction::Downstream, Direction::Downstream, Direction::Upstream},    // Direction::Downstream
    std::array<Direction, 3>{Direction::Upstream, Direction::Upstream, Direction::Downstream},      // Direction::Upstream
    std::array<Direction, 3>{Direction::Bidirectional, Direction::Downstream, Direction::Upstream}, // Direction::Bidirectional
};

constexpr std::array<std::array<Direction, 3>, 6> sideToDirection{
    //                       Traversal::Any, Traversal::Forward, Traversal::Backward
    std::array<Direction, 3>{Direction::Upstream, Direction::Upstream, Direction::Downstream},   // Side::Left
    std::array<Direction, 3>{Direction::Downstream, Direction::Downstream, Direction::Upstream}, // Side::Right
    std::array<Direction, 3>{Direction::Upstream, Direction::Upstream, Direction::Downstream},   // Side::Both
    std::array<Direction, 3>{Direction::Downstream, Direction::Downstream, Direction::Upstream}, // Side::None
    std::array<Direction, 3>{Direction::Unknown, Direction::Unknown, Direction::Unknown},        // Side::Other
    std::array<Direction, 3>{Direction::Other, Direction::Other, Direction::Other},              // Side::Undefined
};

constexpr std::array<std::array<Side, 3>, 5> directionToSide{
    //                  Traversal::Any, Traversal::Forward, Traversal::Backward
    std::array<Side, 3>{Side::Undefined, Side::Undefined, Side::Undefined}, // Direction::Unknown
    std::array<Side, 3>{Side::Other, Side::Other, Side::Other},             // Direction::Other
    std::array<Side, 3>{Side::Right, Side::Right, Side::Left},              // Direction::Downstream
    std::array<Side, 3>{Side::Left, Side::Left, Side::Right},               // Direction::Upstream
    std::array<Side, 3>{Side::Both, Side::Right, Side::Left},               // Direction::Bidirectional
};

constexpr std::array<std::array<Traversal, 5>, 6> directionToOrientation{
    // Direction:              Unknown,          Other,            Downstream,             Upstream,              Bidirectional
    std::array<Traversal, 5>{Traversal::Any, Traversal::Any, Traversal::Backward, Traversal::Forward, Traversal::Any}, // Side::Left
    std::array<Traversal, 5>{Traversal::Any, Traversal::Any, Traversal::Forward, Traversal::Backward, Traversal::Any}, // Side::Right
    std::array<Traversal, 5>{Traversal::Any, Traversal::Any, Traversal::Forward, Traversal::Backward, Traversal::Any}, // Side::Both
    std::array<Traversal, 5>{Traversal::Any, Traversal::Any, Traversal::Backward, Traversal::Forward, Traversal::Any}, // Side::None
    std::array<Traversal, 5>{Traversal::Any, Traversal::Any, Traversal::Any, Traversal::Any, Traversal::Any},          // Side::Other
    std::array<Traversal, 5>{Traversal::Any, Traversal::Any, Traversal::Any, Traversal::Any, Traversal::Any},          // Side::Undefined
};

constexpr std::array<std::array<Traversal, 5>, 5> combinedDirectionToOrientation{
    // Direction:              Unknown,          Other,            Downstream,             Upstream,              Bidirectional
    std::array<Traversal, 5>{Traversal::Any, Traversal::Any, Traversal::Any, Traversal::Any, Traversal::Any},          // Direction::Unknown
    std::array<Traversal, 5>{Traversal::Any, Traversal::Any, Traversal::Any, Traversal::Any, Traversal::Any},          // Direction::Other
    std::array<Traversal, 5>{Traversal::Any, Traversal::Any, Traversal::Forward, Traversal::Backward, Traversal::Any}, // Direction::Downstream
    std::array<Traversal, 5>{Traversal::Any, Traversal::Any, Traversal::Backward, Traversal::Forward, Traversal::Any}, // Direction::Upstream
    std::array<Traversal, 5>{Traversal::Any, Traversal::Any, Traversal::Any, Traversal::Any, Traversal::Any},          // Direction::Bidirectional
};

constexpr std::array<std::string_view, Size<Selection>> selectionToString{
    "First",
    "Last",
    "Each",
};
} // namespace detail

constexpr Direction operator*(Direction direction, Traversal toward)
{
    return detail::combinedDirectionAndOrientation[static_cast<size_t>(direction)][static_cast<size_t>(toward)]; // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
}

constexpr Direction operator*(Traversal toward, Direction direction)
{
    return direction * toward;
}

constexpr Traversal operator*(Direction a, Direction b)
{
    return detail::combinedDirectionToOrientation[static_cast<size_t>(a)][static_cast<size_t>(b)]; // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
}

constexpr Direction GetDirection(Side side, Traversal toward)
{
    return detail::sideToDirection[static_cast<size_t>(side)][static_cast<size_t>(toward)]; // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
}

constexpr Side GetSide(Direction direction, Traversal toward)
{
    return detail::directionToSide[static_cast<size_t>(direction)][static_cast<size_t>(toward)]; // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
}

constexpr Traversal GetOrientation(Side side, Direction direction)
{
    return detail::directionToOrientation[static_cast<size_t>(side)][static_cast<size_t>(direction)]; // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
}

template <typename Enum>
constexpr bool IsInverse(Enum value)
{
    if constexpr (std::is_same_v<Enum, Direction>)
    {
        return value == Direction::Upstream;
    }
    else if constexpr (std::is_same_v<Enum, Traversal>)
    {
        return value == Traversal::Backward;
    }
    else if constexpr (std::is_same_v<Enum, Side>)
    {
        return value == Side::Right || value == Side::None;
    }
    else
    {
        static_assert(always_false<Enum>, "Type not supported");
    }
}

static_assert(Direction::Downstream == Default<Direction>);
static_assert(Direction::Upstream == !Default<Direction>);
static_assert(Side::Left == Default<Side>);
static_assert(Side::Right == !Default<Side>);
static_assert(GetDirection(Default<Side>) == !Default<Direction>);
static_assert(GetDirection(!Default<Side>) == Default<Direction>);
static_assert(GetDirection(Side::Both) == !Default<Direction>);
static_assert(GetDirection(Side::None) == Default<Direction>);
static_assert(GetSide(Direction::Downstream) == Side::Right);
static_assert(GetSide(Direction::Upstream) == Side::Left);
} // namespace osiql
