/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <boost/math/constants/constants.hpp>

namespace osiql {
namespace constants {
constexpr double twoPi{boost::math::constants::two_pi<double>()};
constexpr double pi{boost::math::constants::pi<double>()};
constexpr double halfPi{boost::math::constants::half_pi<double>()};
constexpr double quarterPi{boost::math::constants::pi<double>() / 4.0};
constexpr double threeQuarterPi{boost::math::constants::three_quarters_pi<double>()};
} // namespace constants
using namespace constants;
} // namespace osiql
