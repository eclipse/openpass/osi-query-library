/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Location.h"

#include "OsiQueryLibrary/Types/Enum/Traversal.tpp"
#include "OsiQueryLibrary/Utility/Pick.tpp"

namespace osiql {
template <typename Object, typename Node>
template <auto Toward>
constexpr double Location<Object, Node>::GetDistance() const
{
    if constexpr (std::is_same_v<decltype(Toward), Traversal>)
    {
        static_assert((Toward == Traversal::Forward) || (Toward == Traversal::Backward));
        if constexpr (IsInverse(Toward))
        {
            return GetEndDistance();
        }
        else
        {
            return GetStartDistance();
        }
    }
    else
    {
        static_assert((Toward == Direction::Downstream) || (Toward == Direction::Upstream));
        return this->GetNode().GetDirection() == Toward ? GetStartDistance() : GetEndDistance();
    }
}

template <typename Object, typename Node>
template <typename Toward>
constexpr double Location<Object, Node>::GetDistance(Toward toward) const
{
    return IsInverse(toward) ? GetDistance<!Default<Toward>>() : GetDistance<Default<Toward>>();
}

template <typename Object, typename Node>
constexpr double Location<Object, Node>::GetStartDistance() const
{
    return Has<double>::template Get<double>();
}

template <typename Object, typename Node>
constexpr double Location<Object, Node>::GetEndDistance() const
{
    if constexpr (OSIQL_HAS_MEMBER(Placement<Object>, GetLength()))
    {
        return GetStartDistance() + get<Placement<Object>>(this).GetLength();
    }
    else
    {
        return GetStartDistance();
    }
}

template <typename Object, typename Node>
std::ostream &operator<<(std::ostream &os, const Location<Object, Node> &location)
{
    return os << '{' << raw_t<decltype(location)>::name << " | Distance: " << get<Distance>(location) << ", Placement: " << get<Placement<Object>>(location) << ", Node: " << get<Node>(location) << '}';
}
} // namespace osiql
