/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Utility/Get.tpp"

namespace osiql {
struct Distance
{
};
OSIQL_SET_RETURN_TYPE(Distance, double)
OSIQL_GET(Distance, distance, GetDistance());
} // namespace osiql
