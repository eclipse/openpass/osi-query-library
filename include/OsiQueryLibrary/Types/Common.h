/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Interval from a minimum up to and including a maximum value

#include <iostream>

namespace osiql {
constexpr double MAX_SEARCH_DISTANCE{10000.0};

//! \brief Tolerance for rounding errors
constexpr double EPSILON{1e-6};
} // namespace osiql
