/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Wrapper of a osi3::Timestamp with templatized getters & aggregated setters

#include <chrono>

#include <osi3/osi_common.pb.h>

#include "OsiQueryLibrary/Utility/Common.h"

namespace osiql {
namespace detail {
template <typename>
struct is_duration
{
    static constexpr bool value = false;
};

template <typename Underlying, typename Ratio>
struct is_duration<std::chrono::duration<Underlying, Ratio>>
{
    static constexpr bool value = true;
};
} // namespace detail

template <typename Type>
constexpr bool is_duration = detail::is_duration<Type>::value;

template <typename Handle = osi3::Timestamp>
struct Timestamp
{
    static_assert(std::is_same_v<std::remove_const_t<std::remove_reference_t<Handle>>, osi3::Timestamp>);

    constexpr Timestamp(Handle &&);

    //! Returns the seconds of this timestamp
    //!
    //! \return Seconds of this timestamp rounded down
    std::chrono::seconds GetSeconds() const;

    //! Returns the nanoseconds of this timestamp
    //!
    //! \return Nanoseconds of this timestamp rounded down
    std::chrono::nanoseconds GetNanoseconds() const;

    template <typename Ratio = std::milli>
    std::chrono::duration<int64_t, Ratio> GetTime() const;

    template <typename Ratio, typename _ = Handle, OSIQL_REQUIRES(!is_const<_>)>
    void SetTime(std::chrono::duration<int64_t, Ratio> time)
    {
        const auto seconds{std::chrono::duration_cast<std::chrono::seconds>(time)};
        handle.set_seconds(seconds.count());
        const auto nanos{std::chrono::duration_cast<std::chrono::nanoseconds>(time - seconds)};
        handle.set_nanos(nanos.count());
    }

    Handle handle; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)
};

template <typename Ratio = std::milli>
std::chrono::duration<int64_t, Ratio> GetTime(const osi3::Timestamp &);
} // namespace osiql

namespace osiql {
template <typename Handle>
constexpr Timestamp<Handle>::Timestamp(Handle &&handle) :
    handle{std::forward<Handle>(handle)}
{
}

template <typename Handle>
std::chrono::seconds Timestamp<Handle>::GetSeconds() const
{
    return handle.has_seconds() ? std::chrono::seconds{handle.seconds()} : std::chrono::seconds{};
}

template <typename Handle>
std::chrono::nanoseconds Timestamp<Handle>::GetNanoseconds() const
{
    return handle.has_nanos() ? std::chrono::nanoseconds{handle.nanos()} : std::chrono::nanoseconds{};
}

template <typename Handle>
template <typename Ratio>
std::chrono::duration<int64_t, Ratio> Timestamp<Handle>::GetTime() const
{
    return std::chrono::duration_cast<std::chrono::duration<int64_t, Ratio>>(GetSeconds() + GetNanoseconds());
}

template <typename Ratio>
std::chrono::duration<int64_t, Ratio> GetTime(const osi3::Timestamp &timestamp)
{
    return Timestamp<const osi3::Timestamp &>{timestamp}.GetTime<Ratio>();
}

template <typename Ratio>
void SetTime(osi3::Timestamp &timestamp, std::chrono::duration<int64_t, Ratio> time)
{
    Timestamp<osi3::Timestamp &>{timestamp}.SetTime<Ratio>(time);
}
} // namespace osiql
