/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Component/Component.h"
#include "OsiQueryLibrary/Utility/Common.h"
#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Is.tpp"

namespace osiql { // clang-format off
struct Roll : Component<Roll> {};
OSIQL_SET_RETURN_TYPE(Roll, double)
OSIQL_GET(Roll, GetRoll(), roll(), roll, orientation());

struct Pitch : Component<Pitch> {};
OSIQL_SET_RETURN_TYPE(Pitch, double)
OSIQL_GET(Pitch, GetPitch(), pitch(), pitch, orientation());

struct Yaw : Component<Yaw> {};
OSIQL_SET_RETURN_TYPE(Yaw, double)
OSIQL_GET(Yaw, GetYaw(), yaw(), yaw, orientation()); // clang-format on

struct Rotation
{
    constexpr Rotation() = default;
    constexpr Rotation(double roll, double pitch, double yaw);

    template <typename Type, OSIQL_REQUIRES(Are<Roll, Pitch, Yaw>::in<Type>)>
    constexpr Rotation(Type &&rotation) :
        roll{Roll{}(rotation)}, pitch{Pitch{}(rotation)}, yaw{Yaw{}(rotation)}
    {
    }

    double roll{0.0}, pitch{0.0}, yaw{0.0};
};
OSIQL_GET(Rotation, GetRotation(), rotation);

constexpr Rotation operator*(const Rotation &, double);
constexpr Rotation operator*(double, const Rotation &);
} // namespace osiql

namespace osiql {
constexpr Rotation::Rotation(double roll, double pitch, double yaw) : // NOLINT(bugprone-easily-swappable-parameters)
    roll{roll}, pitch{pitch}, yaw{yaw}
{
}

constexpr Rotation operator*(const Rotation &r, double factor)
{
    return {r.roll * factor, r.pitch * factor, r.yaw * factor};
}

constexpr Rotation operator*(double factor, const Rotation &r)
{
    return r * factor;
}
} // namespace osiql
