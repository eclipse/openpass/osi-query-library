/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Overlap.h"

#include "LocalBounds.tpp"
#include "OsiQueryLibrary/Point/Coordinates.h"
#include "OsiQueryLibrary/Point/Vector.h"
#include "OsiQueryLibrary/Utility/Get.tpp"

namespace osiql {
template <typename LaneOrRoad>
constexpr Overlap<LaneOrRoad>::Overlap(Shape &&shape, LaneOrRoad &overlapable, std::vector<ST> &&localShape) :
    LocalBounds{
        {extract<Direction::Downstream>(*std::min_element(localShape.begin(), localShape.end(), Extract<Direction::Downstream>::less)),
         extract<Direction::Downstream>(*std::max_element(localShape.begin(), localShape.end(), Extract<Direction::Downstream>::less))},
        {extract<Side::Left>(*std::min_element(localShape.begin(), localShape.end(), Extract<Side::Left>::less)),
         extract<Side::Left>(*std::max_element(localShape.begin(), localShape.end(), Extract<Side::Left>::less))}, //
    },
    Aggregate<LaneOrRoad *, Shape, Bounds>{&overlapable, std::move(shape), Bounds{}},
    localShape{std::move(localShape)}
{
    boost::geometry::envelope(this->GetShape(), this->GetBounds());
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const Overlap<T> &overlap)
{
    os << "{Overlap with " << T::name << ' ' << get<T>(overlap).GetId() << ", [";
    auto it{overlap.GetShape().begin()};
    os << *it;
    for (++it; it != overlap.GetShape().end(); ++it)
    {
        os << ", " << *it;
    }
    return os << "]}";
}
} // namespace osiql
