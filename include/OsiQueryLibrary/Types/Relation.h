/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Has.h"

namespace osiql {

//! An association between two types paired with a distance
//! denoting where that association occurs
//!
//! \tparam T
//! \tparam U
template <typename T, typename U>
struct Relation : Aggregate<const T *, const U *>
{
    Relation(Aggregate<const T *, const U *>, double);

    template <typename A, typename B, REQUIRES(std::is_constructible_v<Aggregate<const T *, const U *>, const A *, const B *>)>
    Relation(const A &a, const B &b, double distance) :
        Aggregate<const T *, const U *>{&a, &b}, distance{distance}
    {
    }

    double distance;
};

template <typename T1, typename U1, typename T2, typename U2>
constexpr bool operator<(const Relation<T1, U1> &, const Relation<T2, U2> &);

template <typename T1, typename U1, typename T2, typename U2>
constexpr bool operator==(const Relation<T1, U1> &, const Relation<T2, U2> &);

template <typename T1, typename U1, typename T2, typename U2>
constexpr bool operator>(const Relation<T1, U1> &, const Relation<T2, U2> &);
} // namespace osiql

namespace osiql {
template <typename T, typename U>
Relation<T, U>::Relation(Aggregate<const T *, const U *> aggregate, double distance) :
    Aggregate<const T *, const U *>{aggregate}, distance{distance}
{
}

template <typename T1, typename U1, typename T2, typename U2>
constexpr bool operator<(const Relation<T1, U1> &a, const Relation<T2, U2> &b)
{
    return a.distance < b.distance;
}

template <typename T1, typename U1, typename T2, typename U2>
constexpr bool operator==(const Relation<T1, U1> &a, const Relation<T2, U2> &b)
{
    return (a.template Get<T1>() == b.template Get<T2>()) && (a.template Get<U1>() == b.template Get<U2>());
}

template <typename T1, typename U1, typename T2, typename U2>
constexpr bool operator>(const Relation<T1, U1> &a, const Relation<T2, U2> &b)
{
    return a.distance < b.distance;
}
} // namespace osiql
