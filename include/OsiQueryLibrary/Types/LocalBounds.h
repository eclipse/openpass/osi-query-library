/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Local bounds of an object

#include <limits>

#include "Common.h"
#include "Enum/Direction.h"
#include "Interval.tpp"
#include "OsiQueryLibrary/Point/Coordinates.h"
#include "OsiQueryLibrary/Trait/Id.h"
#include "OsiQueryLibrary/Utility/Extract.h"
#include "OsiQueryLibrary/Utility/XYZ.h" // Dimensions

namespace osiql {
struct Lane;
//! \brief s,t-coordinate bounds
struct LocalBounds
{
    //! Constructs fully defined LocalBounds from its components
    //!
    //! \param s Interval of s-coordinates
    //! \param t Interval of t-coordinates
    constexpr LocalBounds( // clang-format off
        Interval<double> s = Interval{std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity()},
        Interval<double> t = Interval{std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity()}
    ); // clang-format on

    //! Minimally widens the minimum and maximum s- and t-coordinates of these bounds
    //! such that the given object's s- and t-coordinate lies inside them.
    //!
    //! \tparam Type Object from which its s-coordinate range and t-coordinate range can be read
    template <typename Type>
    void Add(const Type &);

    //! \brief Smallest and largest s-coordinate touched by the bounds
    Interval<double> s;

    //! \brief Smallest and largest t-coordinate touched by the bounds
    Interval<double> t;
};

template <typename A, typename B, OSIQL_REQUIRES(Are<S, Not<Id>>::in<A, B> && (Is<Not<Z>>::in<A> || Is<Not<Z>>::in<B>))>
constexpr bool operator==(const A &a, const B &b)
{
    return std::abs(extract<Direction::Downstream>(a) - extract<Direction::Downstream>(b)) <= EPSILON // clang-format off
        && std::abs(extract<Direction::Upstream>(a) - extract<Direction::Upstream>(b)) <= EPSILON; // clang-format on
}

template <typename T, typename U, OSIQL_REQUIRES(Are<S, Not<Id>>::in<T, U>)>
constexpr bool operator!=(const T &t, const U &u)
{
    return !(t == u);
}

std::ostream &operator<<(std::ostream &, const LocalBounds &);
} // namespace osiql
