/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Interval.h"

#include <algorithm>

namespace osiql {
template <typename Type>
constexpr Interval<Type>::Interval(Type min, Type max) : // NOLINT(bugprone-easily-swappable-parameters)
    min{min}, max{max}
{
}

template <typename Type>
template <typename OtherType>
constexpr Interval<Type>::Interval(const OtherType &u) :
    min{u}, max{u}
{
}

template <typename Type>
constexpr Type Interval<Type>::begin() const
{
    return min;
}

template <typename Type>
constexpr Type Interval<Type>::end() const
{
    return max;
}

template <typename T>
constexpr bool Interval<T>::empty() const
{
    return min == max;
}

constexpr double GetDistanceBetween(const Interval<double> &a, const Interval<double> &b)
{
    return std::max({0.0, a.min - b.max, b.min - a.max});
}

constexpr double GetDistanceBetween(const Interval<double> &a, double b)
{
    return std::max({0.0, a.min - b, b - a.max});
}

constexpr double GetDistanceBetween(double a, const Interval<double> &b)
{
    return std::max({0.0, a - b.max, b.min - a});
}

constexpr double GetDistanceBetween(double a, double b)
{
    return std::abs(b - a);
}

template <typename Type>
constexpr bool operator==(const Interval<Type> &lhs, const Interval<Type> &rhs)
{
    return lhs.min == rhs.min && lhs.max == rhs.max;
}

template <typename Type>
std::ostream &operator<<(std::ostream &os, const Interval<Type> &interval)
{
    return os << '[' << interval.min << ", " << interval.max << ']';
}
} // namespace osiql
