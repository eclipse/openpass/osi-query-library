/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! A Location is the placement of an object paired with a distance and a node.

#include "Distance.h"
#include "OsiQueryLibrary/Trait/Placement.h"
#include "OsiQueryLibrary/Types/Enum/Traversal.h"
#include "OsiQueryLibrary/Utility/Aggregate.h"
#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Pick.tpp" // Min<>/Max<>

namespace osiql {
template <typename Object, typename Node>
struct Location : Aggregate<double, const Placement<Object> *, const Node *>
{
    using NodeType = Node;
    using ObjectType = Object;
    using PlacementType = Placement<Object>;

    static constexpr const std::string_view name{"Location"};

    using Aggregate<double, const Placement<Object> *, const Node *>::Aggregate;

    //! Returns the distance from the origin of this location's node
    //! of the earliest encountered s-coordinate of this location based on
    //! the given direction.
    //!
    //! \tparam Toward Direction or Traversal
    //! \return constexpr Smallest s-coordinate distance of this location
    template <auto Toward = Traversal::Forward>
    constexpr double GetDistance() const;

    //! Returns the distance from the origin of this location's node
    //! of the earliest encountered s-coordinate of this location based on
    //! the given direction.
    //!
    //! \tparam Toward Direction or Traversal
    //! \return constexpr Smallest s-coordinate distance of this location
    template <typename Toward = Traversal>
    constexpr double GetDistance(Toward) const;

    //! Returns the smallest s-coordinate distance of this location
    //! from the its node's origin.
    //!
    //! \return constexpr Smallest s-coordinate distance of this location
    constexpr double GetStartDistance() const;

    //! Returns the greatest s-coordinate distance of this location
    //! from the its node's origin.
    //!
    //! \return constexpr Greatest s-coordinate distance of this location
    constexpr double GetEndDistance() const;
};
} // namespace osiql
