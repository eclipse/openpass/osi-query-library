/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <iostream>

#include "Bounds.h"
#include "LocalBounds.h"
#include "OsiQueryLibrary/Utility/Aggregate.h"
#include "Shape.h"

namespace osiql {
struct Lane;
struct Road;

template <typename LaneOrRoad>
struct Overlap : LocalBounds, Aggregate<LaneOrRoad *, Shape, Bounds>
{
    using Type = LaneOrRoad;
    static_assert(std::is_same_v<std::remove_const_t<Type>, Lane> || std::is_same_v<std::remove_const_t<Type>, Road>);

    constexpr Overlap(Shape &&, LaneOrRoad &, std::vector<ST> &&localShape);

    std::vector<ST> localShape;
};

template <typename T, typename StoredType>
struct Has<Overlap<T>, StoredType> : detail::Has<Overlap<T>, StoredType>
{
    using detail::Has<Overlap<T>, StoredType>::Has;

    constexpr const Overlap<T> &GetOverlap() const
    {
        return Has<Overlap<T>, StoredType>::template Get<Overlap<T>>();
    }

    template <typename U = StoredType, OSIQL_REQUIRES(!is_const<U>)>
    constexpr Overlap<T> &GetOverlap()
    {
        return Has<Overlap<T>, StoredType>::template Get<Overlap<T>>();
    }

    constexpr const Shape &GetShape() const
    {
        return GetOverlap().GetShape();
    }

    template <typename U = StoredType, OSIQL_REQUIRES(!is_const<U>)>
    constexpr Shape &GetShape()
    {
        return GetOverlap().GetShape();
    }

    constexpr const Bounds &GetBounds() const
    {
        return GetOverlap().GetBounds();
    }

    template <typename U = StoredType, OSIQL_REQUIRES(!is_const<U>)>
    constexpr Bounds &GetBounds()
    {
        return GetOverlap().GetBounds();
    }

    constexpr const std::vector<ST> &GetLocalShape() const
    {
        return GetOverlap().localShape;
    }

    constexpr std::vector<ST> &GetLocalShape()
    {
        return GetOverlap().localShape;
    }
};

template <typename Type>
using Overlaps = std::vector<Overlap<Type>>;

template <typename T>
std::ostream &operator<<(std::ostream &, const Overlap<T> &);
} // namespace osiql
