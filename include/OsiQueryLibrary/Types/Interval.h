/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <iostream>
#include <limits>

#include "OsiQueryLibrary/Utility/Common.h"

namespace osiql {
//! A range of the given template type with a minimum and maximum value
//!
//! \tparam Type Any type, usually a double
template <typename Type>
struct Interval
{
    template <typename _ = Type, OSIQL_REQUIRES(std::is_fundamental_v<_>)>
    constexpr Interval() :
        min{std::numeric_limits<_>::max()}, max{std::numeric_limits<_>::lowest()}
    {
    }

    constexpr Interval(Type min, Type max);

    template <typename OtherType>
    constexpr Interval(const OtherType &);

    constexpr Type begin() const;

    constexpr Type end() const;

    constexpr bool empty() const;

    Type min;
    Type max;
};

template <typename Type>
Interval(Type &&, Type &&) -> Interval<Type>;

constexpr double GetDistanceBetween(const Interval<double> &, const Interval<double> &);
constexpr double GetDistanceBetween(const Interval<double> &, double);
constexpr double GetDistanceBetween(double, const Interval<double> &);
constexpr double GetDistanceBetween(double, double);

template <typename Type>
constexpr bool operator==(const Interval<Type> &, const Interval<Type> &);

template <typename Type>
std::ostream &operator<<(std::ostream &, const Interval<Type> &);
} // namespace osiql
