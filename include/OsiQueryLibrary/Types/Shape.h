/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Defines an alias for a simple polygon and registers it as a boost::geometry::ring

#include <iostream>
#include <vector>

#include <boost/geometry/geometries/register/ring.hpp>
#include <boost/geometry/geometries/ring.hpp>

#include "OsiQueryLibrary/Trait/BoostGeometry.h" // So Shape can be used as a boost::geometry::ring
#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Has.h"

namespace osiql {
// Open, counter-clockwise simple polygon
using Shape = std::vector<XY>;

std::ostream &operator<<(std::ostream &, const Shape &);

OSIQL_GET(Shape, GetShape(), shape, GetGlobalShape(), globalShape, overlap);
OSIQL_HAS(Shape, GetShape);
} // namespace osiql

BOOST_GEOMETRY_REGISTER_RING(osiql::Shape)

namespace boost {
namespace geometry {
template <>
struct point_order<osiql::Shape>
{
    static const order_selector value = counterclockwise;
};

template <>
struct closure<osiql::Shape>
{
    static const closure_selector value = open;
};
} // namespace geometry
} // namespace boost
