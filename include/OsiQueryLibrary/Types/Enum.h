/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Forward declares enumerations while providing interoperability

#include <type_traits> // std::underlying_type_t

#include "Enum/Direction.h"
#include "Enum/Side.h"
#include "Enum/Traversal.h"
#include "OsiQueryLibrary/Utility/Get.tpp"

namespace osiql {
enum class Selection : char
{
    First,
    Last,
    Each
};
OSIQL_GET(Selection, GetSelection(), selection);

constexpr Direction operator*(Direction, Traversal);

constexpr Direction operator*(Traversal, Direction);

constexpr Traversal operator*(Direction, Direction);

//! Returns the driving direction of lanes on the given side of a road.
//!
//! \return Direction
constexpr Direction GetDirection(Side, Traversal = Traversal::Forward);

//! Returns what side of the road a lane with the given driving direction is on.
//!
//! \return constexpr Side
constexpr Side GetSide(Direction, Traversal = Traversal::Forward);

//! Returns the orientation of the given direction on the given side of the road
//!
//! \return Traversal
constexpr Traversal GetOrientation(Side, Direction);

template <typename Enum>
constexpr bool IsInverse(Enum);
} // namespace osiql
OSIQL_SET_SIZE(osiql::Selection, 3)
