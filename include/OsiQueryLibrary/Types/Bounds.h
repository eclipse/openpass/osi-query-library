/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <iostream>

#include <boost/geometry/geometries/box.hpp> // boost::geometry::model::box

#include "OsiQueryLibrary/Point/XY.tpp"
#include "OsiQueryLibrary/Trait/BoostGeometry.h" // So that XY can be used inside boost::box
#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Has.h"

namespace osiql {
// axis-aligned rectangle used as a bounding box of lane segments in a osiql::World's RTree
using Bounds = boost::geometry::model::box<XY>;

//! Returns the shortest path between the given bounds.
//!
//! \return XY Shortest path, which is a zero-length vector if the bounds intersect
XY GetDistanceBetween(const Bounds &, const Bounds &);

XY GetDistanceBetween(const Bounds &, const XY &);

XY GetDistanceBetween(const XY &, const Bounds &);

std::ostream &operator<<(std::ostream &, const Bounds &);

template <>
struct Get<Bounds>
{
    using result_type = Bounds;

    OSIQL_GET_OPERATOR(Bounds, GetBounds(), bounds, GetGlobalBounds(), globalBounds, overlap)
};

OSIQL_HAS(Bounds, GetBounds);
} // namespace osiql
