/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Aliases for standard library container types with custom comparators

#include <type_traits> // is_transparent
#include <unordered_map>
#include <unordered_set>

#include <google/protobuf/repeated_field.h>

#include "OsiQueryLibrary/Trait/Id.h"
#include "OsiQueryLibrary/Utility/Compare.h"

namespace osiql {
template <typename Type>
using Container = google::protobuf::RepeatedPtrField<Type>;

//! A std::set of shared pointers of the given type
//!
//! \tparam Type Pointer of a type that supports the less-than operator (<) and has the member function Id GetId() const
template <typename Type>
using Set = std::set<Type, Less<Id>>;

//! \brief Generic hash
template <typename Type>
using HashMap = std::unordered_map<Id, Type, Get<Id>, Equal<Id>>;

template <typename Type, typename Hash = Get<Type>, typename Comp = Equal<Type>>
using HashSet = std::unordered_set<Type, Hash, Comp>;
} // namespace osiql
