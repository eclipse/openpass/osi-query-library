/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "LocalBounds.h"

#include "Bounds.h"
#include "Interval.h"
#include "OsiQueryLibrary/Utility/Extract.tpp"
#include "OsiQueryLibrary/Utility/XYZ.h"

namespace osiql {
constexpr LocalBounds::LocalBounds(Interval<double> s, Interval<double> t) : // NOLINT(bugprone-easily-swappable-parameters)
    s{s}, t{t}
{
}
} // namespace osiql
