/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Alias and output operator for geometrical types (box & polygon)

#include <cmath> // sqrt & math constants
#include <iostream>
#include <optional>

#include "Constants.h"
#include "OsiQueryLibrary/Point/XY.h"

namespace osiql {
//! \brief Infinite 2D line defined by two points
struct Line
{
    XY start;
    XY end;

    //! Returns the intersection of this line with the given line or a nullopt if they are parallel.
    //!
    //! \return std::optional<XY>
    std::optional<XY> GetIntersection(const Line &) const;

    double GetRatio(const XY &) const;
};

std::ostream &
operator<<(std::ostream &, const Line &);

//! Returns the equivalent to the given angle within the range (-pi, pi]
//!
//! \param angle
//! \return double
constexpr double WrapAngle(double angle) noexcept;
} // namespace osiql

namespace osiql {
constexpr double WrapAngle(double angle) noexcept
{
    double result{std::fmod(angle, twoPi)};
    if (result > pi)
        return result - twoPi;
    if (result <= -pi)
        return result + twoPi;
    return result;
}
} // namespace osiql
