/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Traversal.h"

#include <array>
#include <string_view>

#include "OsiQueryLibrary/Types/Enum.h"
#include "OsiQueryLibrary/Types/Enum/Traversal.h"

namespace osiql {
namespace detail {
constexpr std::array<std::string_view, Size<Traversal>> orientationToString{"Any", "Forward", "Backward"};

constexpr std::array<Traversal, 3> invertedOrientation{
    Traversal::Any,
    Traversal::Backward,
    Traversal::Forward,
};

constexpr std::array<std::array<Traversal, 3>, 3> combinedOrientation{
    std::array<Traversal, 3>{Traversal::Any, Traversal::Forward, Traversal::Backward},
    std::array<Traversal, 3>{Traversal::Forward, Traversal::Forward, Traversal::Backward},
    std::array<Traversal, 3>{Traversal::Backward, Traversal::Backward, Traversal::Forward},
};
} // namespace detail

constexpr Traversal operator!(Traversal traversal)
{
    return detail::invertedOrientation[static_cast<size_t>(traversal)]; // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
}

constexpr Traversal operator*(Traversal a, Traversal b)
{
    return detail::combinedOrientation[static_cast<size_t>(a)][static_cast<size_t>(b)]; // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
}
} // namespace osiql
