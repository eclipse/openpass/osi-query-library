/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Utility/Common.h"

namespace osiql {
namespace trait {
template <typename Type>
struct Default
{
    static_assert(always_false<Type>, "Missing specialization");
};
} // namespace trait

template <typename Type>
constexpr Type Default = trait::Default<Type>::value;
} // namespace osiql
