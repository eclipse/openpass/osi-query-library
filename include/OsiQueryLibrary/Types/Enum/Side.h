/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Side of a road, which is the same as the side of a reference line

#include <iostream>

#include "Default.h"
#include "OsiQueryLibrary/Trait/Size.h"
#include "OsiQueryLibrary/Utility/Get.tpp"

namespace osiql {
//! \brief Describes what side of the road a lane is on. If a lane's driving direction is Downstream, its Side is right.
//! If the direction is Upstream, the Side is left. Side is used instead of Direction for lateral traversal queries such
//! as Lane::GetAdjacentLanes<Side>().
//!
enum class Side : char
{
    Left,
    Right,
    Both,
    None,
    Other, //!< Side is not static but situational/conditional
    Undefined
};
OSIQL_GET(Side, GetSide(), side, GetSideOfRoad(), sideOfRoad, GetLane(), lane);

constexpr Side operator!(Side);

std::ostream &operator<<(std::ostream &, Side);

namespace trait {
template <>
struct Default<Side>
{
    static constexpr Side value = Side::Left;
};
} // namespace trait
} // namespace osiql
OSIQL_SET_SIZE(osiql::Side, 6)
