/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Traversal direction relative to a reference line

#include <iostream>

#include <osi3/osi_logicallane.pb.h>

#include "Default.h"
#include "OsiQueryLibrary/Trait/Size.h"
#include "OsiQueryLibrary/Utility/Get.tpp"

namespace osiql {
//! \brief A wrapper of osi3::LogicalLane::MoveDirection that describes the driving direction of a lane
//! relative to the order in which the points of its boundaries are defined. It is also used to specify
//! how queries ought to traverse road geometry. If Direction is Downstream, it follows the boundary in
//! the order it was defined, ergo with a steadily increasing s-coordinate. If Direction is Upstream,
//! the boundary is traversed in reverse, thus with a decreasing s-coordinate.
enum class Direction : std::underlying_type_t<osi3::LogicalLane::MoveDirection>
{
    Unknown = osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_UNKNOWN,
    Other = osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_OTHER,
    Downstream = osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S,
    Upstream = osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_DECREASING_S,
    Bidirectional = osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_BOTH_ALLOWED,
};
OSIQL_GET(Direction, GetDirection(), direction);

//! Inverts the given direction
//!
//! \return Direction
constexpr Direction operator!(Direction);

std::ostream &operator<<(std::ostream &, Direction);

namespace trait {
template <>
struct Default<Direction>
{
    static constexpr Direction value = Direction::Downstream;
};
} // namespace trait
} // namespace osiql
OSIQL_SET_SIZE(osiql::Direction, 5)
