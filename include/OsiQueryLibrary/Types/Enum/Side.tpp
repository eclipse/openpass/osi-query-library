/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Side.h"

#include <array>
#include <string_view>

#include "OsiQueryLibrary/Types/Enum.h"

namespace osiql {
namespace detail {
constexpr std::array<Side, Size<Side>> sideToOppositeSide{Side::Right, Side::Left, Side::None, Side::Both, Side::Other, Side::Undefined};

constexpr std::array<std::string_view, Size<Side>> sideToString{"Left", "Right", "Both", "None", "Other", "Undefined"};
} // namespace detail

constexpr Side operator!(Side side)
{
    return detail::sideToOppositeSide[static_cast<size_t>(side)]; // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
}
} // namespace osiql
