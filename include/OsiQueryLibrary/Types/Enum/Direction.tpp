/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Direction.h"

#include <array>
#include <string_view>

#include "OsiQueryLibrary/Types/Enum.h"

namespace osiql {
namespace detail {
constexpr std::array<Direction, 5> inverseDirection{
    Direction::Unknown,       // From Direction::Unknown
    Direction::Other,         // From Direction::Other
    Direction::Upstream,      // From Direction::Downstream
    Direction::Downstream,    // From Direction::Upstream
    Direction::Bidirectional, // From Direction::Bidirectional
};

constexpr std::array<std::string_view, Size<Direction>> directionToString{
    "Unknown",
    "Other",
    "Downstream",
    "Upstream",
    "Bidirectional",
};
} // namespace detail

constexpr Direction operator!(Direction direction)
{
    return detail::inverseDirection[static_cast<size_t>(direction)]; // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
}
} // namespace osiql
