/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Traversal direction relative to the a direction relative to a reference line

#include <iostream>

#include "Default.h"
#include "OsiQueryLibrary/Trait/Size.h"
#include "OsiQueryLibrary/Utility/Get.tpp"

namespace osiql {
//! \brief Traversal is used to tell queries whether to respect (Forward),
//! oppose (Backward) or ignore (Any) the driving direction of traversed lanes.
//!
enum class Traversal : char
{
    Any,
    Forward,
    Backward
};
OSIQL_GET(Traversal, GetTraversal(), traversal);

//! Inverts the given orientation
//!
//! \return Traversal
constexpr Traversal operator!(Traversal);

//! Returns the combined orientation
//!
//! \return constexpr Traversal
constexpr Traversal operator*(Traversal, Traversal);

std::ostream &operator<<(std::ostream &, Traversal);

namespace trait {
template <>
struct Default<Traversal>
{
    static constexpr Traversal value = Traversal::Forward;
};
} // namespace trait
} // namespace osiql
OSIQL_SET_SIZE(osiql::Traversal, 3)
