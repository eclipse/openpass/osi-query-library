/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief File including all others

#include "OsiQueryLibrary/Component/Component.h"
#include "OsiQueryLibrary/Component/Identifiable.tpp"
#include "OsiQueryLibrary/Component/Iterable.tpp"
#include "OsiQueryLibrary/Component/LaneAssignable.tpp"
#include "OsiQueryLibrary/Component/Locatable.tpp"
#include "OsiQueryLibrary/Component/Overlapable.tpp"
#include "OsiQueryLibrary/Component/Points.tpp"
//
#include "OsiQueryLibrary/Object/Collidable.tpp"
#include "OsiQueryLibrary/Object/CommonSign.tpp"
#include "OsiQueryLibrary/Object/MovingObject.tpp"
#include "OsiQueryLibrary/Object/Object.tpp"
#include "OsiQueryLibrary/Object/RoadMarking.h"
#include "OsiQueryLibrary/Object/StationaryObject.h"
#include "OsiQueryLibrary/Object/SupplementarySign.tpp"
#include "OsiQueryLibrary/Object/TrafficLight.h"
#include "OsiQueryLibrary/Object/TrafficSign.tpp"
#include "OsiQueryLibrary/Object/Vehicle.tpp"
#include "OsiQueryLibrary/Object/VehicleData.tpp"
//
#include "OsiQueryLibrary/Point/Anchor.h"
#include "OsiQueryLibrary/Point/Assignment.tpp"
#include "OsiQueryLibrary/Point/Coordinates.tpp"
#include "OsiQueryLibrary/Point/Point.tpp"
#include "OsiQueryLibrary/Point/Pose.tpp"
#include "OsiQueryLibrary/Point/StatePoint.tpp"
#include "OsiQueryLibrary/Point/Vector.tpp"
#include "OsiQueryLibrary/Point/XY.tpp"
#include "OsiQueryLibrary/Point/XYZ.tpp"
//
#include "OsiQueryLibrary/Routing/Node.tpp"
#include "OsiQueryLibrary/Routing/Route.tpp"
#include "OsiQueryLibrary/Routing/Utilities.tpp"
//
#include "OsiQueryLibrary/Street/Lane/Adjoinable.tpp"
#include "OsiQueryLibrary/Street/Lane/BoundaryEnclosure.tpp"
#include "OsiQueryLibrary/Street/Lane/Connectable.tpp"
#include "OsiQueryLibrary/Street/Lane/Orientable.tpp"
//
#include "OsiQueryLibrary/Street/Road/BoundaryChains.tpp"
#include "OsiQueryLibrary/Street/Road/Lanes.tpp"
//
#include "OsiQueryLibrary/Street/Boundary.tpp"
#include "OsiQueryLibrary/Street/BoundaryChain.tpp"
#include "OsiQueryLibrary/Street/Lane.tpp"
#include "OsiQueryLibrary/Street/LaneMarking.tpp"
#include "OsiQueryLibrary/Street/ReferenceLine.tpp"
#include "OsiQueryLibrary/Street/Road.tpp"
//
#include "OsiQueryLibrary/Trait/Base.h"
#include "OsiQueryLibrary/Trait/BoostGeometry.h"
#include "OsiQueryLibrary/Trait/Collection.tpp"
#include "OsiQueryLibrary/Trait/Handle.h"
#include "OsiQueryLibrary/Trait/Id.h"
#include "OsiQueryLibrary/Trait/Localization.h"
#include "OsiQueryLibrary/Trait/Placement.h"
#include "OsiQueryLibrary/Trait/Position.h"
#include "OsiQueryLibrary/Trait/Size.h"
//
#include "OsiQueryLibrary/Types/Enum/Default.h"
#include "OsiQueryLibrary/Types/Enum/Direction.tpp"
#include "OsiQueryLibrary/Types/Enum/Side.tpp"
#include "OsiQueryLibrary/Types/Enum/Traversal.tpp"
//
#include "OsiQueryLibrary/Types/Bounds.h"
#include "OsiQueryLibrary/Types/Common.h"
#include "OsiQueryLibrary/Types/Constants.h"
#include "OsiQueryLibrary/Types/Container.h"
#include "OsiQueryLibrary/Types/Enum.tpp"
#include "OsiQueryLibrary/Types/Geometry.h"
#include "OsiQueryLibrary/Types/Interval.tpp"
#include "OsiQueryLibrary/Types/LocalBounds.tpp"
#include "OsiQueryLibrary/Types/Location.tpp"
#include "OsiQueryLibrary/Types/Matrix.h"
#include "OsiQueryLibrary/Types/Overlap.tpp"
#include "OsiQueryLibrary/Types/Rotation.h"
#include "OsiQueryLibrary/Types/Shape.h"
#include "OsiQueryLibrary/Types/Timestamp.h"
#include "OsiQueryLibrary/Types/Value.h"
//
#include "OsiQueryLibrary/Utility/Aggregate.h"
#include "OsiQueryLibrary/Utility/Common.tpp"
#include "OsiQueryLibrary/Utility/Compare.tpp"
#include "OsiQueryLibrary/Utility/Expression.tpp"
#include "OsiQueryLibrary/Utility/Extract.tpp"
#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Has.h"
#include "OsiQueryLibrary/Utility/Is.tpp"
#include "OsiQueryLibrary/Utility/Length.tpp"
#include "OsiQueryLibrary/Utility/Pick.tpp"
#include "OsiQueryLibrary/Utility/XYZ.h"
//
#include "OsiQueryLibrary/GroundTruth.tpp"
#include "OsiQueryLibrary/NumberGenerator.h"
#include "OsiQueryLibrary/SensorView.h"
#include "OsiQueryLibrary/Version.h"
#include "OsiQueryLibrary/World.tpp"
