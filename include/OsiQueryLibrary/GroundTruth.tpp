/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/GroundTruth.h"

#include "OsiQueryLibrary/Component/LaneAssignable.tpp"
#include "OsiQueryLibrary/Object/MovingObject.tpp"
#include "OsiQueryLibrary/Trait/Collection.tpp"

namespace osiql {
template <typename Type>
constexpr const Collection<Type> &GroundTruth::GetAll() const
{
    if constexpr (std::is_same_v<Type, Lane>)
    {
        return lanes;
    }
    else if constexpr (std::is_same_v<Type, Boundary>)
    {
        return laneBoundaries;
    }
    else if constexpr (std::is_same_v<Type, LaneMarking>)
    {
        return laneMarkings;
    }
    else if constexpr (std::is_same_v<Type, MovingObject>)
    {
        return movingObjects;
    }
    else if constexpr (std::is_same_v<Type, ReferenceLine>)
    {
        return referenceLines;
    }
    else if constexpr (std::is_same_v<Type, Road>)
    {
        return roads;
    }
    else if constexpr (std::is_same_v<Type, RoadMarking>)
    {
        return roadMarkings;
    }
    else if constexpr (std::is_same_v<Type, StationaryObject>)
    {
        return stationaryObjects;
    }
    else if constexpr (std::is_same_v<Type, TrafficLight>)
    {
        return trafficLights;
    }
    else if constexpr (std::is_same_v<Type, TrafficSign>)
    {
        return trafficSigns;
    }
}

template <typename Type>
constexpr Collection<Type> &GroundTruth::GetAll()
{
    return const_cast<Collection<Type> &>(std::as_const(*this).GetAll<Type>());
}

template <typename Type>
const Type *GroundTruth::Get(Id id) const
{
    return osiql::Find<Type>(GetAll<Type>(), id);
}

template <typename Type>
Type &GroundTruth::Find(Id id)
{
    return *osiql::Find<Type>(GetAll<Type>(), id);
}

template <typename Type>
Collection<Type> GroundTruth::FetchAll(const Container<typename Type::Handle> &container)
{
    if constexpr (std::is_same_v<Type, Lane>)
    {
        static_assert(always_false<Type>, "Not supported");
    }
    else if constexpr (OSIQL_HAS_MEMBER(Collection<Type>, reserve(size_t{})))
    {
        Collection<Type> result;
        result.reserve(static_cast<size_t>(container.size()));
        for (const typename Type::Handle &entity : container)
        {
            Emplace<Type>(result, entity);
        }
        return result;
    }
    else
    {
        Collection<Type> result;
        for (const typename Type::Handle &entity : container)
        {
            Emplace<Type>(result, entity);
        }
        return result;
    }
}

template <typename Type>
void GroundTruth::LinkToLanes(std::vector<Type> &entities)
{
    for (Type &entity : entities)
    {
        for (const osi3::LogicalLaneAssignment &assignment : entity.GetLaneAssignments())
        {
            Lane &lane{Find<Lane>(assignment.assigned_lane_id().value())};
            lane.GetAll<Type>().emplace_back(entity, assignment);
            entity.positions.emplace_back(lane, assignment);
        }
    }
}
} // namespace osiql
