/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief File including all others

#include <osi3/osi_groundtruth.pb.h>
#include <osi3/osi_trafficcommand.pb.h>

#include "OsiQueryLibrary/GroundTruth.h"
#include "OsiQueryLibrary/NumberGenerator.h"
#include "OsiQueryLibrary/Object/MovingObject.h"
#include "OsiQueryLibrary/Object/StationaryObject.h"
#include "OsiQueryLibrary/Object/Vehicle.h"
#include "OsiQueryLibrary/Point/Point.h"
#include "OsiQueryLibrary/Street/Boundary.h"
#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/Road.h"
#include "OsiQueryLibrary/Trait/Localization.h"
#include "OsiQueryLibrary/Types/Overlap.h"

namespace osiql {
template <typename Type>
using RTree = boost::geometry::index::rtree<Type *, boost::geometry::index::quadratic<8, 4>, osiql::Get<Bounds>>;

//! \brief A World offers location world methods, xy- <-> st-coordinate conversion and bidirectional road to lane to object mappings.
struct World : GroundTruth
{
    //! Initialize the world with a fully defined ground truth.
    World(const osi3::GroundTruth &);

    //! Updates (includes adding and removing) all moving objects given the
    //! collection of object handles. Recomputes all lane & road overlaps
    //!
    //! \param allObjects Container of osi3::MovingObject sorted by id
    void Update(const Container<osi3::MovingObject> &allObjects);

    //! Returns the lane with the given id or nullptr if none exists
    //!
    //! \return const Lane*
    const Lane *GetLane(Id) const;

    //! Returns the lane boundary with the given id or nullptr if none exists
    //!
    //! \return const Boundary*
    const Boundary *GetBoundary(Id) const;

    //! Returns the reference line with the given id or nullptr if none exists
    //!
    //! \return const ReferenceLine*
    const ReferenceLine *GetReferenceLine(Id) const;

    //! Returns the moving object with the given id or nullptr if none exists
    //!
    //! \return const MovingObject*
    const MovingObject *GetMovingObject(Id) const;

    //! Returns the vehicle with the given id or nullptr if none exists
    //!
    //! \return const Vehicle*
    const Vehicle *GetVehicle(Id) const;

    //! Returns the road marking with the given id or nullptr if none exists
    //!
    //! \return const RoadMarking*
    const RoadMarking *GetRoadMarking(Id) const;

    //! Returns the stationary object with the given id or nullptr if none exists
    //!
    //! \return const Object*
    const StationaryObject *GetStationaryObject(Id) const;

    //! Returns the traffic light with the given id or nullptr if none exists
    //!
    //! \return const TrafficLight*
    const TrafficLight *GetTrafficLight(Id) const;

    //! Returns the traffic sign with the given id or nullptr if none exists
    //!
    //! \return const TrafficSign*
    const TrafficSign *GetTrafficSign(Id) const;

    //! Calculates the local st-coordinates representing a given point for each lane covering the point.
    //!
    //! \tparam GlobalPoint XY or Pose<XY>
    //! \param GlobalPoint global XY-coordinate pair, optionally with a global angle
    //! \return Local representation of the input for each road it touches.
    template <typename GlobalPoint>
    std::vector<Localization<GlobalPoint>> Localize(const GlobalPoint &) const;

    //! Returns the shortest route from the origin to the destination.
    //!
    //! \tparam T Forward or Backward
    //! \tparam Origin The starting point or an iterator pointing to the start of a chain of points
    //! \tparam Destination The destination of the returned route or an iterator to the end of a chain of points
    //! \return Route Shortest route from the origin to the destination or nullopt if no path exists
    template <Traversal T = Traversal::Forward, typename Scope = Road, typename Origin = Point<const Lane>, typename Destination = Point<const Lane>>
    std::optional<Route<T, Scope>> GetRoute(const Origin &, const Destination &) const;

    //! Returns the shortest route passing through all points of the given chain of points.
    //!
    //! \tparam T Forward or Backward
    //! \param action The TrafficAction containing the ordered set of global points
    //! \return Route Shortest route through all points or nullopt if no path exists
    template <Traversal T = Traversal::Forward, typename Scope = Road>
    std::optional<Route<T, Scope>> GetRoute(const osi3::TrafficAction::FollowPathAction &action) const;

    //! Returns a random route from the host vehicle up until a given maximum distance, either in driving direction
    //! if given a positive distance or against driving direction if given a negative distance.
    //!
    //! \tparam Traversal Direction of the route relative to the driving direction of the traversed lanes
    //! \param maxDistance [0, ∞)
    //! \return Route A route from the given origin. If the origin does not lie on any road, a nullopt is returned.
    template <Traversal T = Traversal::Forward, typename Scope = Road, typename Origin = Point<const Lane>, typename RNG = void>
    std::optional<Route<T, Scope>> GetRandomRoute(const Origin &, RNG &&, double maxDistance = MAX_SEARCH_DISTANCE) const;

    //! Returns a custom route from the given origin to the given destination using the given successor selector.
    //!
    //! \tparam T Forward or Backward
    //! \tparam Origin Global or local point
    //! \tparam Selector Invocable with signature const Node<T, Scope>*(const Node<T, Scope>&).
    //! The route ends once nullptr is returned
    //! \return A route from the given origin. If the origin does not lie on any road, a nullopt is returned.
    template <Traversal T, typename Scope = Road, typename Origin = void, typename Selector = void>
    std::optional<Route<T, Scope>> GetCustomRoute(const Origin &, Selector &&) const;

    //! Returns the relative bounds (minimum and maximum s and t values) of the intersections
    //! of the given global bounding box with any road geometry it touches in the world.
    //! Note: If an object intersects the same piece of geometry in two places, only one intersection will be returned.
    //!
    //! \tparam Type Either Lane or Road
    //! \param Shape Open simply polygon consisting of global xy-coordinates
    //! \param Bounds axis-aligned rectangular bounding box of global points
    //! \return List of overlaps with objects of the given type
    template <typename Type = Lane>
    std::vector<Overlap<const Type>> FindOverlapping(const Shape &, const Bounds &) const;

    //! Returns the relative bounds (minimum and maximum s and t values) of the intersections
    //! of the given global bounding box with any road geometry it touches in the world.
    //! Note: If an object intersects the same piece of geometry in two places, only one intersection will be returned.
    //!
    //! \tparam Type Either Lane or Road
    //! \tparam U Object containing a Shape and Bounds
    //! \return List of overlaps with objects of the given type
    template <typename Type = Lane, typename Object = void, OSIQL_REQUIRES(Are<Shape, Bounds>::in<Object>)>
    std::vector<Overlap<const Type>> FindOverlapping(const Object &entity) const
    {
        return FindOverlapping<Type>(get<Shape>(entity), get<Bounds>(entity));
    }

    //! Debug output listing the state of the world.
    //!
    //! \return std::ostream&
    friend std::ostream &operator<<(std::ostream &, const World &);

private:
    template <typename Type = Lane>
    std::vector<Overlap<Type>> InternalFindOverlapping(const Shape &, const Bounds &);

    //! Returns an unsorted vector of lanes containing the given global point.
    //!
    //! \param XY global xy-coordinate pair
    //! \return std::vector<const Lane*> Empty if no lane contains the given point
    std::vector<const Lane *> GetLanesContaining(const XY &) const;

    //! Updates the global shape, local positions and lane intersection bounds of the given object.
    //!
    //! \param object
    template <typename ObjectType>
    void UpdateObject(ObjectType &object);

    template <typename Type>
    constexpr const RTree<Type> &GetRTree() const;

    template <typename Type>
    constexpr RTree<Type> &GetRTree();

private:
    template <Traversal T, typename Scope, typename Origin, typename Destination>
    std::optional<Route<T, Scope>> GetRouteBetweenGlobalPoints(const Origin &, const Destination &) const;

    template <Traversal T, typename Scope, typename Origin, typename Destination>
    std::optional<Route<T, Scope>> GetRouteFromGlobalPointToLocalPoint(const Origin &, const Destination &) const;

    template <Traversal T, typename Scope, typename Origin, typename Destination>
    std::optional<Route<T, Scope>> GetRouteFromLocalPointToGlobalPoint(const Origin &, const Destination &) const;

    template <Traversal T, typename Scope, typename Origin, typename Destination>
    std::optional<Route<T, Scope>> GetRouteFromLocalPointToLocalPoint(const Origin &, const Destination &) const;

    template <Traversal T, typename Scope, typename PointIterator>
    std::optional<Route<T, Scope>> GetRouteFromRangeOfGlobalPoints(PointIterator first, PointIterator pastLast) const;

    template <Traversal T, typename Scope, typename PointIterator>
    std::optional<Route<T, Scope>> GetRouteFromRangeOfLocalPoints(PointIterator first, PointIterator pastLast) const;

public:
    RTree<Lane> laneRTree;
    RTree<Road> roadRTree;
    RTree<MovingObject> movingObjectRTree;
    RTree<StationaryObject> stationaryObjectRTree;
    RTree<TrafficLight> trafficLightRTree;
    RTree<TrafficSign> trafficSignRTree;

    std::vector<Id> priorMovingObjectIds;
};

//! Returns the world associated with the given ground truth
//!
//! \note For the World to be returned, its address must be stored in the source_reference of
//! GroundTruth's environmental conditions as an external reference with the key "osiql::World"
//! and the value being the serialized address of said world
//! \return nullptr if not found
World *GetWorld(const osi3::GroundTruth &);

//! Returns the world associated with the given ground truth
//!
//! \note For the World to be returned, its address must be stored in the source_reference of
//! GroundTruth's environmental conditions as an external reference with the key "std::shared_ptr<osiql::World>"
//! and the value being the serialized address of said world
//! \return nullptr if not found
std::shared_ptr<World> GetSharedWorld(const osi3::GroundTruth &);

namespace detail {
//! Returns a container of the intersection points of two polygons
//!
//! \tparam PolygonA
//! \tparam PolygonB
//! \return const std::vector<XY>
template <typename PolygonA, typename PolygonB>
std::vector<XY> GetIntersections(const PolygonA &, const PolygonB &);

//! Returns whether two polygons touch
//!
//! \tparam PolygonA
//! \tparam PolygonB
//! \return const std::vector<XY>
template <typename PolygonA, typename PolygonB>
bool HasIntersections(const PolygonA &, const PolygonB &);
} // namespace detail

std::ostream &operator<<(std::ostream &, const World &);
} // namespace osiql
