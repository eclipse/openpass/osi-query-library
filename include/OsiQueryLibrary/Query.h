/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief File including all others

#include <osi3/osi_groundtruth.pb.h>
#include <osi3/osi_trafficcommand.pb.h>

#include "OsiQueryLibrary/NumberGenerator.h"
#include "OsiQueryLibrary/Object/MovingObject.h"
#include "OsiQueryLibrary/Object/StaticObject.h"
#include "OsiQueryLibrary/Object/Vehicle.h"
#include "OsiQueryLibrary/Point/Point.h"
#include "OsiQueryLibrary/Street/Boundary.h"
#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/Road.h"
#include "OsiQueryLibrary/Trait/Localization.h"
#include "OsiQueryLibrary/Types/Overlap.tpp"
#include "OsiQueryLibrary/World.h"

namespace osiql {
template <typename T>
using RTree = boost::geometry::index::rtree<T *, boost::geometry::index::quadratic<8, 4>, osiql::Get<Bounds>>;

//! \brief A Query offers location query methods, xy- <-> st-coordinate conversion and bidirectional road to lane to object mappings.
class Query
{
public:
    //! Initialize the query with a fully defined ground truth.
    //!
    //! \param groundTruth  ground truth with static world data
    Query(const osi3::GroundTruth &groundTruth);

    //! Updates (includes adding and removing) all moving objects given the
    //! collection of object handles. Recomputes all lane & road overlaps
    //!
    //! \param allObjects Container of osi3::MovingObject sorted by id
    void Update(const Container<osi3::MovingObject> &allObjects);

    //! Returns the world this query is querying
    //!
    //! \return const World&
    const World &GetWorld() const;

    //! Returns the lane with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const Lane*
    const Lane *GetLane(Id id) const;

    //! Returns the lane boundary with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const Boundary*
    const Boundary *GetBoundary(Id id) const;

    //! Returns the reference line with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const ReferenceLine*
    const ReferenceLine *GetReferenceLine(Id id) const;

    //! Returns the moving object with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const MovingObject*
    const MovingObject *GetMovingObject(Id id) const;

    //! Returns the road marking with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const RoadMarking*
    const RoadMarking *GetRoadMarking(Id id) const;

    //! Returns the stationary object with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const Object*
    const StaticObject *GetStaticObject(Id id) const;

    //! Returns the traffic light with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const TrafficLight*
    const TrafficLight *GetTrafficLight(Id id) const;

    //! Returns the traffic sign with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const TrafficSign*
    const TrafficSign *GetTrafficSign(Id id) const;

    //! Returns the host vehicle of the simulation (aka the ego agent).
    //!
    //! \return const Vehicle&
    const Vehicle &GetHostVehicle() const;

    //! Calculates the local st-coordinates representing a given point for each lane covering the point.
    //!
    //! \tparam GlobalPoint XY or Pose<XY>
    //! \param GlobalPoint global XY-coordinate pair, optionally with a global angle
    //! \return Local representation of the input for each road it touches.
    template <typename GlobalPoint>
    std::vector<Localization<GlobalPoint>> Localize(const GlobalPoint &) const;

    //! Returns the shortest route from the origin to the destination.
    //!
    //! \tparam T Forward or Backward
    //! \tparam Origin The starting point or an iterator pointing to the start of a chain of points
    //! \tparam Destination The destination of the returned route or an iterator to the end of a chain of points
    //! \return Route Shortest route from the origin to the destination or nullopt if no path exists
    template <Traversal T = Traversal::Forward, typename Origin = Point<const Lane>, typename Destination = Point<const Lane>>
    std::optional<Route<T>> GetRoute(const Origin &, const Destination &) const;

    //! Returns the shortest route passing through all points of the given chain of points.
    //!
    //! \tparam T Forward or Backward
    //! \param action The TrafficAction containing the ordered set of global points
    //! \return Route Shortest route through all points or nullopt if no path exists
    template <Traversal T = Traversal::Forward>
    std::optional<Route<T>> GetRoute(const osi3::TrafficAction::FollowPathAction &action) const;

    //! Returns a random route from the host vehicle up until a given maximum distance, either in driving direction
    //! if given a positive distance or against driving direction if given a negative distance.
    //!
    //! \tparam Traversal Direction of the route relative to the driving direction of the traversed lanes
    //! \param maxDistance [0, ∞)
    //! \return Route A route from the given origin. If the origin does not lie on any road, a nullopt is returned.
    template <Traversal T = Traversal::Forward, typename Origin = Point<const Lane>, typename RNG = void>
    std::optional<Route<T>> GetRandomRoute(const Origin &, RNG &&, double maxDistance = MAX_SEARCH_DISTANCE) const;

    //! Returns a custom route from the given origin to the given destination using the given successor selector.
    //!
    //! \tparam T Forward or Backward
    //! \tparam Origin Global or local point
    //! \tparam Selector Callable taking a const Node<T>& and returning a const Node<T>*.
    //! The route ends once nullptr is returned
    //! \return A route from the given origin. If the origin does not lie on any road, a nullopt is returned.
    template <Traversal T, typename Origin, typename Selector>
    std::optional<Route<T>> GetCustomRoute(const Origin &, Selector &&) const;

    //! Returns the relative bounds (minimum and maximum s and t values) of the intersections
    //! of the given global bounding box with any road geometry it touches in the world.
    //! Note: If an object intersects the same piece of geometry in two places, only one intersection will be returned.
    //!
    //! \tparam T Either Lane or Road
    //! \param Shape Open simply polygon consisting of global xy-coordinates
    //! \param Bounds axis-aligned rectangular bounding box of global points
    //! \return List of overlaps with objects of the given type
    template <typename T = Lane>
    std::vector<Overlap<const T>> FindOverlapping(const Shape &, const Bounds &) const;

    //! Returns the relative bounds (minimum and maximum s and t values) of the intersections
    //! of the given global bounding box with any road geometry it touches in the world.
    //! Note: If an object intersects the same piece of geometry in two places, only one intersection will be returned.
    //!
    //! \tparam T Either Lane or Road
    //! \tparam U Object containing a Shape and Bounds
    //! \return List of overlaps with objects of the given type
    template <typename T = Lane, typename U = void, REQUIRES(Are<Shape, Bounds>::template in<U>)>
    std::vector<Overlap<const T>> FindOverlapping(const U &u) const
    {
        return FindOverlapping<T>(get<Shape>(u), get<Bounds>(u));
    }

    //! Debug output listing the state of the world.
    //!
    //! \param os
    //! \param query
    //! \return std::ostream&
    friend std::ostream &operator<<(std::ostream &os, const Query &query);

    //! \brief The query's random number generator
    mutable NumberGenerator rng;

private:
    template <typename T = Lane>
    std::vector<Overlap<T>> InternalFindOverlapping(const Shape &, const Bounds &bounds);

    //! Returns an unsorted vector of lanes containing the given global point.
    //!
    //! \param XY global xy-coordinate pair
    //! \return std::vector<const Lane*> Empty if no lane contains the given point
    std::vector<const Lane *> GetLanesContaining(const XY &) const;

    //! Updates the global shape, local positions and lane intersection bounds of the given object.
    //!
    //! \param object
    template <typename ObjectType>
    void UpdateObject(ObjectType &object);

    template <typename T>
    constexpr const RTree<T> &GetRTree() const;

    template <typename T>
    constexpr RTree<T> &GetRTree();

    std::unique_ptr<World> world;

    RTree<Lane> lanes;
    RTree<Road> roads;
    RTree<MovingObject> movingObjects;
    RTree<StaticObject> staticObjects;
    RTree<TrafficLight> trafficLights;
    RTree<TrafficSign> trafficSigns;

    std::vector<Id> priorMovingObjectIds;
};

namespace detail {
//! Returns a container of the intersection points of two polygons
//!
//! \param polygonA
//! \param polygonB
//! \return const std::vector<XY>
template <typename T, typename U>
std::vector<XY> GetIntersections(const T &polygonA, const U &polygonB);

//! Returns whether two polygons touch
//!
//! \param polygonA
//! \param polygonB
//! \return const std::vector<XY>
template <typename T, typename U>
bool HasIntersections(const T &polygonA, const U &polygonB);
} // namespace detail

std::ostream &operator<<(std::ostream &os, const Query &query);
} // namespace osiql
