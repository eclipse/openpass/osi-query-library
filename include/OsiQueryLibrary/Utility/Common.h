/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Common type traits and macros

#include <cstddef>
#include <functional> // std::reference_wrapper
#include <iostream>
#include <memory>
#include <optional>
#include <sstream>
#include <type_traits>
#include <utility>

namespace osiql {
#define OSIQL_COMMA ,

template <class... T>
constexpr bool always_false = false;

template <typename T>
using raw_t = typename std::remove_const_t<std::remove_pointer_t<std::remove_const_t<std::remove_reference_t<T>>>>;

template <typename T, typename F>
constexpr auto has_member(F &&func) -> decltype(func(std::declval<T>()), true) // NOLINT(readability-identifier-naming)
{
    return true;
}

template <typename>
constexpr bool has_member(...) // NOLINT(readability-identifier-naming)
{
    return false;
}

#define OSIQL_HAS_MEMBER(T, ...) has_member<T>([](auto &&_) -> decltype(_.__VA_ARGS__) {})

#define OSIQL_HAS_STATIC_MEMBER(T, ...) has_member<T>([](auto &&_) -> decltype(decltype(_)::__VA_ARGS__) {})

#define OSIQL_REQUIRES(...) std::enable_if_t<__VA_ARGS__, bool> = true

template <typename T>
using Const = std::add_const_t<T>;

template <typename T>
using value_type = std::conditional_t<std::is_const_v<T>, Const<typename T::value_type>, typename T::value_type>;

template <typename T>
using reference = std::add_lvalue_reference_t<value_type<T>>;

template <typename T>
using const_reference = std::add_lvalue_reference_t<Const<value_type<T>>>;

template <typename T>
using difference_type = typename T::difference_type;

namespace trait { // clang-format off
template <typename T>
struct is_smart_pointer : std::false_type {}; // NOLINT(readability-identifier-naming)

template <typename T>
struct is_smart_pointer<std::shared_ptr<T>> : std::true_type {};

template <typename T>
struct is_smart_pointer<std::unique_ptr<T>> : std::true_type {};

template<typename T>
struct is_reference_wrapper : std::false_type {}; // NOLINT(readability-identifier-naming)

template<typename T>
struct is_reference_wrapper<std::reference_wrapper<T>> : std::true_type{};
// clang-format on
} // namespace trait

template <typename T>
constexpr bool is_smart_pointer = trait::is_smart_pointer<std::remove_const_t<T>>::value; // NOLINT(readability-identifier-naming)

template <typename T>
constexpr bool is_pointer = is_smart_pointer<std::remove_const_t<T>> || std::is_pointer_v<T>; // NOLINT(readability-identifier-naming)

template <typename Type>
constexpr bool is_reference_wrapper = trait::is_reference_wrapper<std::remove_const_t<Type>>::value; // NOLINT(readability-identifier-naming)
namespace detail {
template <typename It>
struct is_reverse_iterator : std::false_type // NOLINT(readability-identifier-naming)
{
};

template <typename It>
struct is_reverse_iterator<std::reverse_iterator<It>> : std::true_type
{
};
} // namespace detail

template <typename It>
constexpr bool is_reverse_iterator = detail::is_reverse_iterator<std::remove_const_t<It>>::value;

namespace trait {
template <typename Type>
struct ReturnType
{
    using type = Type;
};
} // namespace trait

template <typename Type>
using ReturnType = typename trait::ReturnType<Type>::type;

#define OSIQL_SET_RETURN_TYPE(TYPE, ...) \
    namespace trait {                    \
    template <>                          \
    struct ReturnType<TYPE>              \
    {                                    \
        using type = __VA_ARGS__;        \
    };                                   \
    }

template <typename Transform, typename... Input>
using Transformations = std::vector<std::remove_reference_t<std::invoke_result_t<Transform, Input...>>>;

template <typename Type, typename Input>
constexpr decltype(auto) as(Input &&input) // NOLINT(readability-identifier-naming)
{
    if constexpr (std::is_same_v<Type, Input> || (sizeof(Type) == sizeof(Input)))
    {
        return std::forward<Input>(input);
    }
    else
    {
        return Type{input};
    }
}

template <auto Start, auto End, class F>
constexpr void constexpr_for(F &&function) // NOLINT(readability-identifier-naming)
{
    if constexpr (Start < End)
    {
        function(std::integral_constant<decltype(Start), Start>());
        constexpr_for<Start + 1, End>(function);
    }
}

template <typename It, typename Func, typename Comp = std::less<>>
It TernaryIteratorSearch(It min, It max, Func &&func, Comp comp = std::less{})
{
    while (min < max)
    {
        const auto jump{std::distance(min, max) / 3};
        auto a{min + jump};
        auto b{max - jump};
        if (comp(func(a), func(b)))
        {
            max = std::prev(b);
        }
        else
        {
            min = std::next(a);
        }
    }
    return min;
}

namespace detail {
template <size_t Skip, size_t N, size_t I, size_t... Is>
constexpr auto skip_sequence_helper(std::index_sequence<I, Is...>) // NOLINT(readability-identifier-naming)
{
    if constexpr (N == 0 && Skip == I)
    {
        return std::index_sequence<Is...>{};
    }
    else if constexpr (N == 0)
    {
        return std::index_sequence<Is..., I>{};
    }
    else if constexpr (Skip == I)
    {
        return skip_sequence_helper<Skip, N - 1>(std::index_sequence<Is...>{});
    }
    else
    {
        return skip_sequence_helper<Skip, N - 1>(std::index_sequence<Is..., I>{});
    }
}
} // namespace detail

template <size_t Skip, size_t To>
constexpr auto make_skip_sequence() // NOLINT(readability-identifier-naming)
{
    return detail::skip_sequence_helper<Skip, To - 1>(std::make_index_sequence<To>());
}

namespace detail {
template <typename T>
struct is_const // NOLINT(readability-identifier-naming)
{
    static constexpr bool value = false;
};

template <typename T>
struct is_const<const T>
{
    static constexpr bool value = true;
};

template <typename T>
struct is_const<T &>
{
    static constexpr bool value = is_const<T>::value;
};

template <typename T>
struct is_const<T *>
{
    static constexpr bool value = is_const<T>::value;
};

template <typename T>
struct is_const<std::reference_wrapper<T>>
{
    static constexpr bool value = is_const<T>::value;
};

template <typename T>
struct is_const<std::shared_ptr<T>>
{
    static constexpr bool value = is_const<T>::value;
};

template <typename T>
struct is_const<std::unique_ptr<T>>
{
    static constexpr bool value = is_const<T>::value;
};

template <typename T>
struct is_const<std::optional<T>>
{
    static constexpr bool value = is_const<T>::value;
};

template <size_t I, typename T, typename... Ts>
struct nth // NOLINT(readability-identifier-naming)
{
    using type = nth<I - 1, Ts...>;
};

template <typename T, typename... Ts>
struct nth<0, T, Ts...>
{
    using type = T;
};
} // namespace detail

template <typename T>
static constexpr bool is_const = detail::is_const<T>::value; // NOLINT(readability-identifier-naming)

template <size_t I, typename... Ts>
using nth = typename detail::nth<I, Ts...>::type;

template <size_t I, typename T, typename... Ts>
constexpr decltype(auto) at(T &&arg, Ts &&...args)
{
    if constexpr (I > 0)
    {
        return at<I - 1>(std::forward<Ts>(args)...);
    }
    else
    {
        return std::forward<T>(arg);
    }
}

template <typename T>
std::ostream &operator<<(std::ostream &, const std::vector<T> &);

inline uintptr_t StringToAddress(const std::string &string)
{
    std::istringstream stream{string};
    uintptr_t result; // NOLINT(cppcoreguidelines-init-variables)
    stream >> result;
    return result;
}

template <typename Address>
std::string AddressToString(const Address *address)
{
    return std::to_string(reinterpret_cast<uintptr_t>(address)); // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast)
}
} // namespace osiql
