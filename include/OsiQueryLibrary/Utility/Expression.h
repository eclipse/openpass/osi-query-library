/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Aggregate.h"

namespace osiql {
//! Function object that returns whether an input
//! satisfies all of this object's predicates
//!
//! \tparam Predicate Invocable with signature bool(const auto&&) const
template <typename... Predicate>
struct Conjunction : Aggregate<Predicate...>
{
    using Aggregate<Predicate...>::Aggregate;

    //! Returns whether each predicate satisfies the given input.
    //! Terminates as soon as a predicate returns false.
    //!
    //! \tparam Value
    //! \return Whether each predicate satisfies the given input.
    template <typename Value>
    constexpr bool operator()(Value &&) const;
};

//! Function object that returns whether an input
//! satisfies any of this object's predicates
//!
//! \tparam Predicate Invocable with signature bool(const auto&&) const
template <typename... Predicate>
struct Disjunction : Aggregate<Predicate...>
{
    using Aggregate<Predicate...>::Aggregate;

    //! Returns whether any predicate satisfies the given input.
    //! Terminates as soon as a predicate returns true.
    //!
    //! \tparam Value
    //! \return Whether any predicate satisfies the given input.
    template <typename Value>
    constexpr bool operator()(Value &&) const;
};
} // namespace osiql
