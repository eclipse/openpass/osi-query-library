/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#ifndef OSIQL_INCLUDE_IS
#error Please include Is.h indirectly via Is.tpp
#endif
#undef OSIQL_INCLUDE_IS

//! \file
//! \brief Compile-time member checking & enum value look-up tables

#include <array>

#include "Common.h"
#define OSIQL_INCLUDE_GET
#include "Get.h"
#include "OsiQueryLibrary/Trait/Size.h"

namespace osiql {
//! Function object that generates a look-up table from enum values.
//! Calling this functor with another object will return whether that object
//! has one of the enum values that this functor was constructed with.
//!
//! \tparam Enum Enumeration type for which trait::Size<Enum>::value has been specialized.
template <typename Type>
class Is
{
    template <typename>
    struct InTuple : std::false_type
    {
    };

    template <typename... Types>
    struct InTuple<std::tuple<Types...>> : std::disjunction<std::is_same<std::remove_const_t<std::remove_reference_t<Type>>, Types>...>
    {
    };

public:
    template <typename... T>
    static constexpr bool in = (!std::is_void_v<decltype(get<Type>(std::declval<std::add_const_t<T>>()))> && ...);

    template <typename Tuple>
    static constexpr bool in_tuple = InTuple<Tuple>::value; // NOLINT(readability-identifier-naming)

    template <typename... Base>
    static constexpr bool derived = (std::is_base_of_v<Base, Type> || ...);

    //! Constructs a look-up table from the given enum values, where
    //! all indices hold false expect for the given indices, which hold true.
    constexpr Is(std::initializer_list<Type>);

    //! Constructs a look-up table from the given enum values where all
    //! inidices hold false except for the given indicies, which hold true.
    //!
    //! \tparam It Iterator pointing to an enum value
    template <typename It, OSIQL_REQUIRES(!std::is_same_v<Type, It>)>
    constexpr Is(It begin, It end)
    {
        for (; begin != end; ++begin)
        {
            mapping[static_cast<size_t>(*begin)] = true;
        }
    }

    //! Returns whether the given object has one of the types this functor was constructed with
    //!
    //! \tparam T Type where get<Enum>(t) returns Enum
    //! \return The mapping value of the given object's enum index to this functor's mapping
    template <typename T>
    constexpr bool operator()(const T &) const;

    std::array<bool, Size<Type>> mapping{};
};

template <typename... Types>
struct Are
{
    template <typename... T>
    static constexpr bool in = (Is<Types>::template in<T...> && ...);
};
} // namespace osiql
