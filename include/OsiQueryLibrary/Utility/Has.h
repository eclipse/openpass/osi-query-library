/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//!
//! \file
//! \brief Defines the class "Has" that is a wrapper of a single member. This
//! allows the specialization of specific wrappers such as Has<Lane> so that
//! any type that inherits from Has<Lane> (such as Overlap<Lane>,  Node<Lane>
//! & Point<Lane>) also simultaneously provides getters and setters related to
//! that member.

#include <optional>

#include "Common.h"
#include "Get.tpp"
#include "Is.tpp"
#include "OsiQueryLibrary/Trait/Comparable.h"

namespace osiql {
#define OSIQL_HAS(TYPE, GETTER)                                    \
    template <typename Type>                                       \
    struct Has<TYPE, Type> : detail::Has<TYPE, Type>               \
    {                                                              \
        using detail::Has<TYPE, Type>::Has;                        \
                                                                   \
        constexpr const TYPE &GETTER() const                       \
        {                                                          \
            return Has<TYPE, Type>::template Get<TYPE>();          \
        }                                                          \
                                                                   \
        template <typename T = Type, OSIQL_REQUIRES(!is_const<T>)> \
        constexpr TYPE &GETTER()                                   \
        {                                                          \
            return Has<TYPE, Type>::template Get<TYPE>();          \
        }                                                          \
    }

namespace detail {
//! Wrapper of a type that can return its member using Get<Type>()
//!
//! @tparam Type The decayed type of the stored member. Specified in order to
//! specialization of this class for sets of types that shared the same decayed type
//! @tparam StoredType The exact type of the stored member
template <typename Type, typename StoredType = Type>
class Has
{
    ReturnType<StoredType> member;

public:
    //! Constructs a wrapper from its member
    //! \tparam Input Type convertible to the StoredType of this wrapper
    //! \param input The data this wrapper shall hold
    template <typename Input = ReturnType<StoredType>, OSIQL_REQUIRES(Is<Type>::template in<Input>)>
    constexpr Has(Input &&input = {}) :
        member{std::forward<Input>(input)}
    {
    }

    //! Returns the member held by this wrapper if it matches the requested type.
    //!
    //! \tparam T Requested type, which may be a decayed version of the stored member.
    //! \return The member held by this wrapper if matching the requested type, otherwise void
    template <typename T>
    constexpr decltype(auto) Get() const
    {
        if constexpr (std::is_fundamental_v<T> || std::is_enum_v<T>)
        {
            return osiql::get<T>(member);
        }
        else
        {
            return (osiql::get<T>(member));
        }
    }

    //! Returns the member held by this wrapper if it matches the requested type.
    //!
    //! \tparam T Requested type, which may be a decayed version of the stored member.
    //! \return The member held by this wrapper if matching the requested type, otherwise void
    template <typename T, typename U = StoredType, OSIQL_REQUIRES(!osiql::is_const<U>)>
    constexpr decltype(auto) Get()
    {
        if constexpr (std::is_fundamental_v<T> || std::is_enum_v<T>)
        {
            return osiql::get<T>(member);
        }
        else
        {
            return (osiql::get<T>(member));
        }
    }
};
} // namespace detail

template <typename T, typename StoredType = T>
struct Has : detail::Has<T, StoredType>
{
    using detail::Has<T, StoredType>::Has;
};

#define OSIQL_HAS_COMPARATOR(OPERATOR, NAME)                                        \
    template <typename A, typename B, OSIQL_REQUIRES(is_##NAME##_comparable<A, B>)> \
    constexpr bool operator OPERATOR(const Has<A> &a, const Has<B> &b)              \
    {                                                                               \
        return a.template Get<A>() OPERATOR b.template Get<B>();                    \
    }

OSIQL_HAS_COMPARATOR(==, equal)
OSIQL_HAS_COMPARATOR(!=, not_equal)
OSIQL_HAS_COMPARATOR(<, less)
OSIQL_HAS_COMPARATOR(<=, less_or_equal)
OSIQL_HAS_COMPARATOR(>, greater)
OSIQL_HAS_COMPARATOR(>=, greater_or_equal)

#define OSIQL_SPECIALIZE_HAS(TYPE, STORED_TYPE)                          \
    template <typename TYPE>                                             \
    struct Has<STORED_TYPE, STORED_TYPE> : Has<raw_t<TYPE>, STORED_TYPE> \
    {                                                                    \
        using Has<raw_t<TYPE>, STORED_TYPE>::Has;                        \
    }

OSIQL_SPECIALIZE_HAS(T, const T);
OSIQL_SPECIALIZE_HAS(T, T *);
OSIQL_SPECIALIZE_HAS(T, const T *);
OSIQL_SPECIALIZE_HAS(T, T &);
OSIQL_SPECIALIZE_HAS(T, const T &);
OSIQL_SPECIALIZE_HAS(T, std::reference_wrapper<T>);
OSIQL_SPECIALIZE_HAS(T, std::optional<T>);
} // namespace osiql
