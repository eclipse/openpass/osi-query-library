/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Utility/Common.h"
#include "OsiQueryLibrary/Utility/Has.h"

namespace osiql {
struct Lane;
struct Road;
enum class Side : char;

template <typename Type>
struct Has<Lane, Type> : detail::Has<Lane, Type>
{
    using detail::Has<Lane, Type>::Has;

    constexpr const Lane &GetLane() const
    { // NOTE: clang-diagnostic does not find this definition if separated from its declaration
        return detail::Has<Lane, Type>::template Get<Lane>();
    }

    template <typename _ = Type, OSIQL_REQUIRES(!is_const<_>)>
    constexpr Lane &GetLane()
    {
        return const_cast<Lane &>(std::as_const(*this).GetLane());
    }

    constexpr const Road &GetRoad() const;

    template <typename _ = Type, OSIQL_REQUIRES(!is_const<_>)>
    constexpr Road &GetRoad()
    {
        return const_cast<Road &>(std::as_const(*this).GetRoad());
    }

    constexpr Side GetSideOfRoad() const;
};
} // namespace osiql
