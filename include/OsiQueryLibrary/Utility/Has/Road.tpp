/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Road.h"

#include "OsiQueryLibrary/Street/Road.h"

namespace osiql {
template <typename Type>
constexpr const ReferenceLine &Has<Road, Type>::GetReferenceLine() const
{
    return GetRoad().GetReferenceLine();
}
} // namespace osiql
