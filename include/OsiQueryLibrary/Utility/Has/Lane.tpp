/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Lane.h"

#include "OsiQueryLibrary/Street/Lane.h"

namespace osiql {
template <typename Type>
constexpr const Road &Has<Lane, Type>::GetRoad() const
{
    return GetLane().GetRoad();
}

template <typename Type>
constexpr Side Has<Lane, Type>::GetSideOfRoad() const
{
    return GetLane().GetSideOfRoad();
}
} // namespace osiql
