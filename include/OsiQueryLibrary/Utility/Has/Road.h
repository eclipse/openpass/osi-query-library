/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Utility/Common.h"
#include "OsiQueryLibrary/Utility/Has.h"

namespace osiql {
struct ReferenceLine;
struct Road;

template <typename Type>
struct Has<Road, Type> : detail::Has<Road, Type>
{
    using detail::Has<Road, Type>::Has;

    constexpr const Road &GetRoad() const
    { // NOTE: clang-diagnostic does not find this definition if separated from its declaration
        return detail::Has<Road, Type>::template Get<Road>();
    }

    template <typename _ = Type, OSIQL_REQUIRES(!is_const<_>)>
    constexpr Road &GetRoad()
    {
        return const_cast<Road &>(std::as_const(*this).GetRoad());
    }

    constexpr const ReferenceLine &GetReferenceLine() const;

    template <typename _ = Type, OSIQL_REQUIRES(!is_const<_>)>
    constexpr ReferenceLine &GetReferenceLine()
    {
        return const_cast<ReferenceLine &>(std::as_const(*this).GetReferenceLine());
    }
};
} // namespace osiql
