/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Get.tpp"

namespace osiql {
//! \brief A functor used to extract the length of an object
struct Length
{
    //! Returns the difference between an object's maximum and minimum s-coordinate
    //!
    //! \tparam T Type for which the overload double extract<Direction>(const T&) is defined
    //! \param object Entity of type T
    //! \return double Longitudinal length of the given entity
    template <typename T>
    constexpr double operator()(const T &) const;
};
OSIQL_SET_RETURN_TYPE(Length, double)
OSIQL_GET(Length, GetLength(), length);
} // namespace osiql
