/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Common functions for specific objects, mostly primitive getters

#include <osi3/osi_referenceline.pb.h>

#include "Get.tpp"
#include "Is.tpp"
#include "OsiQueryLibrary/Component/Component.h"

namespace osiql {
#define OSIQL_GET_COMPONENT(COMPONENT, IDENTIFIER)                                                                                               \
    OSIQL_SET_RETURN_TYPE(COMPONENT, double)                                                                                                     \
    template <>                                                                                                                                  \
    struct Get<COMPONENT>                                                                                                                        \
    {                                                                                                                                            \
        template <typename Type>                                                                                                                 \
        constexpr decltype(auto) operator()(Type &&_) const                                                                                      \
        {                                                                                                                                        \
            if constexpr (std::is_same_v<ReturnType<COMPONENT>, raw_t<Type>>) { return; }                                                        \
            else if constexpr (has_member<Type>([](auto &&_) -> decltype(_.IDENTIFIER()) {})) { return _.IDENTIFIER(); }                         \
            else if constexpr (has_member<Type>([](auto &&_) -> decltype(_.IDENTIFIER) {})) { return _.IDENTIFIER; }                             \
            else if constexpr (has_member<Type>([](auto &&_) -> decltype(_.position()) {})) { return get<COMPONENT>(_.position()); }             \
            else if constexpr (has_member<Type>([](auto &&_) -> decltype(_.world_position()) {})) { return get<COMPONENT>(_.world_position()); } \
            else { return default_get<COMPONENT>(std ::forward<Type>(_)); }                                                                      \
        }                                                                                                                                        \
    }
// clang-format off

struct X : Component<X>{};
OSIQL_GET_COMPONENT(X, x);

struct Y : Component<Y>{};
OSIQL_GET_COMPONENT(Y, y);

struct Z : Component<Z>{};
OSIQL_GET_COMPONENT(Z, z);
// clang-format on

template <typename Type>
constexpr size_t Dimensions = Is<X>::in<Type> ? Is<Y>::in<Type> ? Is<Z>::in<Type> ? 3 : 2 : 1 : 0; // NOLINT(readability-identifier-naming)

template <size_t I, typename Vector>
constexpr decltype(auto) At(const Vector &vector)
{
    if constexpr (I == 0)
    {
        return X{}(vector);
    }
    else if constexpr (I == 1)
    {
        return Y{}(vector);
    }
    else if constexpr (I == 2)
    {
        return Z{}(vector);
    }
    else
    {
        return 0.0;
    }
}

std::ostream &operator<<(std::ostream &, const osi3::ReferenceLine::ReferenceLinePoint &);
} // namespace osiql
