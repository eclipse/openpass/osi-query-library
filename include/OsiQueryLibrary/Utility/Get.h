/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#ifndef OSIQL_INCLUDE_GET
#error Please include Get.h indirectly via Get.tpp
#endif
#undef OSIQL_INCLUDE_GET

#include <functional>  // std::equal_to
#include <type_traits> // std::true_type

#include "Common.h"  // OSIQL_HAS_MEMBER
#include "Compare.h" // Equal

namespace osiql {
template <auto Value>
struct Return
{
    template <typename... Args>
    constexpr decltype(Value) operator()(Args &&...) const;
};

template <typename Type>
struct Not
{
    constexpr Not(Type &&);

    template <typename... Input>
    constexpr bool operator()(Input &&...) const;

    Type predicate;
};

template <typename Type>
Not(Type &) -> Not<Type &>;

template <typename Type>
Not(Type &&) -> Not<Type>;

template <typename Type>
struct Get
{
    using transparent_key_equal = Equal<Type>;

    template <typename Input>
    constexpr decltype(auto) operator()(Input &&) const;
};

#define OSIQL_VARIADIC(_1, _2, _3, _4, _5, _6, _7, FUNC, ...) FUNC
#define OSIQL_GET_IMPL(...) OSIQL_VARIADIC(__VA_ARGS__, OSIQL_GET_IMPL6, OSIQL_GET_IMPL5, OSIQL_GET_IMPL4, OSIQL_GET_IMPL3, OSIQL_GET_IMPL2, OSIQL_GET_IMPL1)(__VA_ARGS__)
#define OSIQL_GET_IMPL1(TYPE, A)             \
    if constexpr (OSIQL_HAS_MEMBER(Type, A)) \
    {                                        \
        return get<TYPE>(_.A);               \
    }
#define OSIQL_GET_IMPL2(TYPE, A, B) OSIQL_GET_IMPL1(TYPE, A) else OSIQL_GET_IMPL1(TYPE, B)
#define OSIQL_GET_IMPL3(TYPE, A, B, C) OSIQL_GET_IMPL1(TYPE, A) else OSIQL_GET_IMPL2(TYPE, B, C)
#define OSIQL_GET_IMPL4(TYPE, A, B, C, D) OSIQL_GET_IMPL1(TYPE, A) else OSIQL_GET_IMPL3(TYPE, B, C, D)
#define OSIQL_GET_IMPL5(TYPE, A, B, C, D, E) OSIQL_GET_IMPL1(TYPE, A) else OSIQL_GET_IMPL4(TYPE, B, C, D, E)
#define OSIQL_GET_IMPL6(TYPE, A, B, C, D, E, F) OSIQL_GET_IMPL1(TYPE, A) else OSIQL_GET_IMPL5(TYPE, B, C, D, E, F)

#define OSIQL_GET(TYPE, ...)                  \
    template <>                               \
    struct Get<TYPE>                          \
    {                                         \
        OSIQL_GET_OPERATOR(TYPE, __VA_ARGS__) \
    }

#define OSIQL_GET_OPERATOR(TYPE, ...)                        \
    template <typename Type>                                 \
    constexpr decltype(auto) operator()(Type &&_) const      \
    {                                                        \
        OSIQL_GET_IMPL(TYPE, __VA_ARGS__)                    \
        else                                                 \
        {                                                    \
            return default_get<TYPE>(std::forward<Type>(_)); \
        }                                                    \
    }

template <typename Type, typename Input>
constexpr decltype(auto) default_get(Input &&); // NOLINT(readability-identifier-naming)

template <typename Type, typename Input>
constexpr decltype(auto) get(Input &&); // NOLINT(readability-identifier-naming)

template <typename Type, typename Comp = std::equal_to<Type>>
struct Matches
{
    using is_transparent = std::true_type;

    template <typename Input>
    constexpr Matches(const Input &);

    template <typename Input>
    constexpr bool operator()(const Input &) const;

private:
    const Type &type; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)
};

struct Angle
{
};
OSIQL_SET_RETURN_TYPE(Angle, double)
OSIQL_GET(Angle, GetAngle(), angle, GetYaw(), yaw(), yaw, orientation());
} // namespace osiql
