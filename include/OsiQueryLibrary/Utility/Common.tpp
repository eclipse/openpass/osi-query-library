/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Common.h"

namespace osiql {
template <typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &list)
{
    os << '[';
    if (!list.empty())
    {
        os << list.front();
        for (auto item{std::next(list.begin())}; item != list.end(); ++item)
        {
            os << ", " << get<raw_t<T>>(*item);
        }
    }
    return os << ']';
}
} // namespace osiql
