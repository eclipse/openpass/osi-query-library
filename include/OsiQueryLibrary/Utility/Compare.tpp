/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Compare.h"

#include "Common.h"
#include "Get.tpp"
#include "Is.tpp"

namespace osiql {
template <typename Type, typename Comparator, typename Fallback>
template <typename A, typename B>
constexpr bool StableCompare<Type, Comparator, Fallback>::operator()(A &&a, B &&b) const
{
    if constexpr (OSIQL_HAS_MEMBER(Type, operator()(std::declval<A>())) && OSIQL_HAS_MEMBER(Type, operator()(std::declval<B>())))
    {
        if (Type{}(std::forward<A>(a)) == Type{}(std::forward<B>(b)))
        {
            return Fallback{}(std::forward<A>(a), std::forward<B>(b));
        }
        return Comparator{}(Type{}(std::forward<A>(a)), Type{}(std::forward<B>(b)));
    }
    else
    {
        return StableCompare<Get<Type>, Comparator, Fallback>{}(std::forward<A>(a), std::forward<B>(b));
    }
}

template <typename Type, typename Comparator>
template <typename A, typename B>
constexpr bool Compare<Type, Comparator>::operator()(A &&a, B &&b) const
{
    if constexpr (!Is<Type>::template in<A, B>)
    {
        if constexpr (OSIQL_HAS_MEMBER(Type, operator()(std::declval<A>(), std::declval<B>())))
        {
            return Type{}(std::forward<A>(a), std::forward<B>(b));
        }
        else
        {
            static_assert(always_false<A, B>, "Comparison not supported");
        }
    }
    else
    {
        return Comparator{}(get<Type>(std::forward<A>(a)), get<Type>(std::forward<B>(b)));
    }
}
template <typename T, typename Comparator>
template <typename A, typename B>
constexpr bool CompareEquality<T, Comparator>::operator()(A &&a, B &&b) const
{
    if constexpr (std::is_void_v<decltype(get<T>(a))> || std::is_void_v<decltype(get<T>(b))>)
    {
        if constexpr (OSIQL_HAS_MEMBER(T, operator()(std::declval<A>(), std::declval<B>())))
        {
            return T{}(std::forward<A>(a), std::forward<B>(b));
        }
        else
        {
            static_assert(always_false<A, B>, "Comparison not supported");
        }
    }
    else
    {
        return Comparator{}(get<T>(std::forward<A>(a)), get<T>(std::forward<B>(b)));
    }
}

template <typename T, typename Comparator>
template <typename B>
template <typename A>
constexpr bool Compare<T, Comparator>::Than<B>::operator()(A &&a) const
{
    return Compare<T, Comparator>{}(std::forward<A>(a), b);
}

template <typename T, typename Comparator>
template <typename B>
template <typename A>
constexpr bool CompareEquality<T, Comparator>::To<B>::operator()(A &&a) const
{
    return CompareEquality<T, Comparator>{}(std::forward<A>(a), b);
}
} // namespace osiql
