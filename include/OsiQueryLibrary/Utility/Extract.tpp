/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Extract.h"

#include <osi3/osi_referenceline.pb.h>

#include "Common.h"
#include "Get.tpp"
#include "OsiQueryLibrary/Types/Enum.tpp" // IsInverse

namespace osiql {
template <auto Value>
template <typename T>
constexpr double Extract<Value>::operator()(const T &t) const // NOLINT(readability-function-cognitive-complexity)
{
    if constexpr (std::is_fundamental_v<T>)
    {
        return t;
    }
    else if constexpr (is_pointer<T>)
    {
        return extract<Value>(*t);
    }
    else if constexpr (std::is_same_v<decltype(Value), Side> && OSIQL_HAS_MEMBER(T, template GetT<Value>()))
    {
        return t.template GetT<Value>();
    }
    else if constexpr (std::is_same_v<decltype(Value), Side> && OSIQL_HAS_MEMBER(T, GetT()))
    {
        return t.GetT();
    }
    else if constexpr (std::is_same_v<decltype(Value), Side> && OSIQL_HAS_MEMBER(T, t))
    {
        return extract<Value>(t.t);
    }
    else if constexpr (!std::is_same_v<decltype(Value), Side> && OSIQL_HAS_MEMBER(T, template GetS<Value>()))
    {
        return t.template GetS<Value>();
    }
    else if constexpr (!std::is_same_v<decltype(Value), Side> && OSIQL_HAS_MEMBER(T, GetS()))
    {
        return t.GetS();
    }
    else if constexpr (!std::is_same_v<decltype(Value), Side> && OSIQL_HAS_MEMBER(T, s))
    {
        return extract<Value>(t.s);
    }
    else if constexpr (!std::is_same_v<decltype(Value), Side> && OSIQL_HAS_MEMBER(T, GetDirection(Value)))
    {
        return extract(t, t.GetDirection(Value));
    }
    else if constexpr (std::is_same_v<decltype(Value), Traversal> && OSIQL_HAS_MEMBER(T, GetLane()))
    {
        return extract(t, t.GetLane().template GetDirection(Value));
    }
    else if constexpr (std::is_same_v<decltype(Value), Traversal> && OSIQL_HAS_MEMBER(T, GetStartDistance()) && OSIQL_HAS_MEMBER(T, GetEndDistance()))
    {
        if constexpr (Value == Traversal::Backward)
        {
            return t.GetEndDistance();
        }
        else
        {
            return t.GetStartDistance();
        }
    }
    else if constexpr (!std::is_same_v<decltype(Value), Side> && OSIQL_HAS_MEMBER(T, has_s_position()))
    {
        return t.has_s_position() ? t.s_position() : .0;
    }
    else if constexpr (std::is_same_v<decltype(Value), Side> && OSIQL_HAS_MEMBER(T, has_t_position()))
    {
        return t.has_t_position() ? t.t_position() : .0;
    }
    else if constexpr (std::is_same_v<decltype(Value), Side> && std::is_same_v<std::remove_const_t<T>, osi3::ReferenceLine::ReferenceLinePoint>)
    {
        std::ignore = t;
        return 0.0;
    }
    else if constexpr (!IsInverse(Value) && OSIQL_HAS_MEMBER(T, start_s()))
    {
        return t.start_s();
    }
    else if constexpr (IsInverse(Value) && OSIQL_HAS_MEMBER(T, end_s()))
    {
        return t.end_s();
    }
    else if constexpr (!IsInverse(Value) && OSIQL_HAS_MEMBER(T, min))
    {
        return extract<Value>(t.min);
    }
    else if constexpr (IsInverse(Value) && OSIQL_HAS_MEMBER(T, max))
    {
        return extract<Value>(t.max);
    }
    else if constexpr (OSIQL_HAS_MEMBER(T, first) && OSIQL_HAS_MEMBER(T, second))
    {
        return extract<Value>(t.second);
    }
    else if constexpr (OSIQL_HAS_MEMBER(T, GetHandle()))
    {
        return extract<Value>(t.GetHandle());
    }
    else if constexpr (!IsInverse(Value) && OSIQL_HAS_MEMBER(T, front()))
    {
        return extract<Value>(t.front());
    }
    else if constexpr (IsInverse(Value) && OSIQL_HAS_MEMBER(T, back()))
    {
        return extract<Value>(t.back());
    }
    else if constexpr (OSIQL_HAS_MEMBER(T, overlap))
    {
        return extract<Value>(t.overlap);
    }
    else if constexpr (OSIQL_HAS_MEMBER(T, GetOverlap()))
    {
        return extract<Value>(t.GetOverlap());
    }
    else if constexpr (OSIQL_HAS_MEMBER(T, GetLane()))
    {
        return extract<Value>(t.GetLane());
    }
    else
    {
        static_assert(always_false<T>, "Extract not supported for given type");
    }
}

template <auto Value, typename Type>
constexpr double extract(const Type &input)
{
    return Extract<Value>{}(input);
}

template <typename Type, typename Orientation>
constexpr double extract(const Type &input, Orientation value)
{
    return IsInverse(value) ? extract<!Default<Orientation>>(input) : extract<Default<Orientation>>(input);
}

template <typename Enum, typename Container, typename Contained>
constexpr bool Contains(const Container &out, const Contained &in, double epsilon)
{
    return extract<Default<Enum>>(in) - extract<Default<Enum>>(out) >= -epsilon // clang-format off
        && extract<!Default<Enum>>(out) - extract<!Default<Enum>>(in) >= -epsilon; // clang-format on
}

template <auto Value>
template <typename T, typename U>
constexpr bool Extract<Value>::Less::operator()(const T &t, const U &u) const
{
    if constexpr (IsInverse(Value))
    {
        return extract<Value>(t) > extract<Value>(u);
    }
    else
    {
        return extract<Value>(t) < extract<Value>(u);
    }
}

template <typename A, typename B, typename T>
constexpr bool less(const A &a, const B &b, T &&t) // NOLINT(readability-identifier-naming)
{
    if constexpr (std::is_enum_v<T>)
    {
        return IsInverse(t) ? Extract<!Default<T>>::less(a, b) : Extract<Default<T>>::less(a, b);
    }
    else if constexpr (OSIQL_HAS_MEMBER(T, operator()(std::declval<A>(), std::declval<B>())))
    {
        return t(a) < t(b);
    }
    else
    {
        static_assert(always_false<T>, "Not supported");
    }
}

template <auto Value>
template <typename T, typename U>
constexpr bool Extract<Value>::Greater::operator()(const T &t, const U &u) const
{
    if constexpr (IsInverse(Value))
    {
        return extract<Value>(u) > extract<Value>(t);
    }
    else
    {
        return extract<Value>(t) > extract<Value>(u);
    }
}

template <typename A, typename B, typename T>
constexpr bool greater(const A &a, const B &b, T &&t) // NOLINT(readability-identifier-naming)
{
    if constexpr (std::is_enum_v<T>)
    {
        return IsInverse(t) ? Extract<!Default<T>>::greater(a, b) : Extract<Default<T>>::greater(a, b);
    }
    else if constexpr (OSIQL_HAS_MEMBER(T, operator()(std::declval<A>(), std::declval<B>())))
    {
        return t(a) > t(b);
    }
    else
    {
        static_assert(always_false<T>, "Not supported");
    }
}

template <auto Value>
template <typename A, typename B>
constexpr bool Extract<Value>::LessEqual::operator()(const A &a, const B &b) const
{
    return !Extract<Value>::greater(a, b);
}

template <typename A, typename B, typename T>
constexpr bool lessEqual(const A &a, const B &b, T &&t) // NOLINT(readability-identifier-naming)
{
    return !greater(a, b, std::forward<T>(t));
}

template <auto Value>
template <typename A, typename B>
constexpr bool Extract<Value>::GreaterEqual::operator()(const A &a, const B &b) const
{
    return !Extract<Value>::less(a, b);
}

template <typename A, typename B, typename T>
constexpr bool greaterEqual(const A &a, const B &b, T &&t) // NOLINT(readability-identifier-naming)
{
    return !less(a, b, std::forward<T>(t));
}

template <auto Value, typename A, typename B>
constexpr double min(const A &a, const B &b)
{
    if constexpr (IsInverse(Value))
    {
        return std::max(extract<Value>(a), extract<Value>(b));
    }
    else
    {
        return std::min(extract<Value>(a), extract<Value>(b));
    }
}

template <typename Value, typename A, typename B>
constexpr double min(const A &a, const B &b, Value value)
{
    return IsInverse(value) ? min<!Default<Value>>(a, b) : min<Default<Value>>(a, b);
}

template <auto Value, typename T, typename U>
constexpr double max(const T &a, const U &b)
{
    if constexpr (IsInverse(Value))
    {
        return std::min(extract<Value>(a), extract<Value>(b));
    }
    else
    {
        return std::max(extract<Value>(a), extract<Value>(b));
    }
}

template <typename Value, typename A, typename B>
constexpr double max(const A &a, const B &b, Value value)
{
    return IsInverse(value) ? max<!Default<Value>>(a, b) : max<Default<Value>>(a, b);
}

template <auto Value, typename T, typename U>
constexpr double GetStartDifference(const T &start, const U &goal)
{
    if constexpr (IsInverse(Value))
    {
        return extract<Value>(start) - extract<Value>(goal);
    }
    else
    {
        return extract<Value>(goal) - extract<Value>(start);
    }
}

template <typename From, typename To, typename Orientation>
constexpr double GetStartDifference(const From &start, const To &goal, Orientation value)
{
    return IsInverse(value) ? GetStartDifference<!Default<Orientation>>(start, goal) : GetStartDifference<Default<Orientation>>(start, goal);
}

template <auto Value, typename From, typename To>
constexpr double GetEndDifference(const From &start, const To &goal)
{
    if constexpr (IsInverse(Value))
    {
        return extract<!Value>(start) - extract<!Value>(goal);
    }
    else
    {
        return extract<!Value>(goal) - extract<!Value>(start);
    }
}

template <typename From, typename To, typename Orientation>
constexpr double GetEndDifference(const From &start, const To &goal, Orientation value)
{
    return IsInverse(value) ? GetEndDifference<!Default<Orientation>>(start, goal) : GetEndDifference<Default<Orientation>>(start, goal);
}

template <auto Value, typename From, typename To>
constexpr double Difference(const From &start, const To &goal)
{
    if constexpr (IsInverse(Value))
    {
        return extract<!Value>(start) - extract<Value>(goal);
    }
    else
    {
        return extract<Value>(goal) - extract<!Value>(start);
    }
}

template <typename From, typename To, typename Orientation>
constexpr double Difference(const From &start, const To &goal, Orientation value)
{
    return IsInverse(value) ? Difference<!Default<Orientation>>(start, goal) : Difference<Default<Orientation>>(start, goal);
}

template <auto Value, typename T>
constexpr double Increase(const T &t, double amount)
{
    if constexpr (IsInverse(Value))
    {
        return extract<Value>(t) - amount;
    }
    else
    {
        return extract<Value>(t) + amount;
    }
}

template <typename Type, typename Orientation>
constexpr double Increase(const Type &input, double amount, Orientation value)
{
    return IsInverse(value) ? Increase<!Default<Orientation>>(input, amount) : Increase<Default<Orientation>>(input, amount);
}
} // namespace osiql
