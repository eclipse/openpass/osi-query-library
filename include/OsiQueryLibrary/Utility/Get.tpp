/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#define OSIQL_INCLUDE_GET
#include "Get.h"

#include <cassert>

#include "Common.h"
#include "Compare.tpp"

#define OSIQL_INCLUDE_IS
#include "Is.h"

namespace osiql {
template <auto Value>
template <typename... Args>
constexpr decltype(Value) Return<Value>::operator()(Args &&...) const
{
    return Value;
}

template <typename Type>
constexpr Not<Type>::Not(Type &&predicate) :
    predicate{std::forward<Type>(predicate)}
{
}

template <typename Type>
template <typename... Input>
constexpr bool Not<Type>::operator()(Input &&...input) const
{
    return !predicate(std::forward<Input>(input)...);
}

template <typename Type, typename Input>
constexpr decltype(auto) default_get(Input &&input)
{
    if constexpr (is_reference_wrapper<Type>)
    {
        return get<typename Type::type>(std::forward<Input>(input));
    }
    else if constexpr (is_pointer<Type>)
    {
        if constexpr (Is<raw_t<Type>>::template in<Input>)
        {
            return &get<raw_t<Type>>(std::forward<Input>(input));
        }
        else
        {
            return;
        }
    }
    else if constexpr (std::is_same_v<ReturnType<Type>, std::remove_const_t<std::remove_reference_t<Input>>>)
    {
        if constexpr (std::is_fundamental_v<ReturnType<Type>> || std::is_enum_v<ReturnType<Type>>)
        {
            return ReturnType<Type>{input};
        }
        else
        {
            return std::forward<Input>(input);
        }
    }
    else if constexpr (std::is_base_of_v<ReturnType<Type>, std::remove_const_t<std::remove_reference_t<Input>>>)
    {
        return std::forward<Input>(input);
    }
    else if constexpr (is_pointer<std::remove_reference_t<Input>>)
    { // Pointer
        return get<Type>(*input);
    }
    else if constexpr (OSIQL_HAS_MEMBER(Input, first) && OSIQL_HAS_MEMBER(Input, second))
    { // std::pair
        if constexpr (Is<Type>::template in<decltype(std::declval<Input>().first)>)
        {
            return get<Type>(input.first);
        }
        else
        {
            return get<Type>(input.second);
        }
    }
    else if constexpr (OSIQL_HAS_MEMBER(Input, get()))
    { // std::reference_wrapper or smart pointer
        return get<Type>(input.get());
    }
    else if constexpr (OSIQL_HAS_MEMBER(Input, has_value()) && OSIQL_HAS_MEMBER(Input, value()))
    { // std::optional
        assert(input.has_value());
        return get<Type>(input.value());
    }
    else if constexpr (OSIQL_HAS_MEMBER(Input, template Get<Type>()))
    {
        return input.template Get<Type>();
    }
    else
    {
        return;
    }
}

template <typename Type>
template <typename Input>
constexpr decltype(auto) Get<Type>::operator()(Input &&input) const
{
    return default_get<Type>(std::forward<Input>(input));
}

template <typename Type, typename Input>
constexpr decltype(auto) get(Input &&input)
{
    return Get<Type>{}(std::forward<Input>(input));
}

template <typename Type, typename Comp>
template <typename Input>
constexpr Matches<Type, Comp>::Matches(const Input &input) :
    type{get<Type>(input)}
{
}

template <typename Type, typename Comp>
template <typename Input>
constexpr bool Matches<Type, Comp>::operator()(const Input &input) const
{
    return Comp{}(type, get<Type>(input));
}

} // namespace osiql
