/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <tuple>

#include "Common.h"
#include "Has.h"

namespace osiql {
//! A type that holds one or more other types and can return any requested type T that it holds using Get<T>()
//!
//! \tparam Ts... The types as they are stored in this aggregate
template <typename... Ts>
struct Aggregate : Has<Ts>...
{
    static constexpr size_t size = sizeof...(Ts);

    Aggregate(Ts &&...input) :
        Has<Ts>{std::forward<Ts>(input)}...
    {
    }

    template <typename T>
    constexpr decltype(auto) Get() const
    {
        return Return<T, Ts...>();
    }

    template <size_t I>
    constexpr decltype(auto) Get() const
    {
        using T = decltype(std::get<I>(std::declval<std::tuple<Ts...>>()));
        return Has<T>::template Get<T>();
    }

private:
    template <typename T, typename Type, typename... Types>
    constexpr decltype(auto) Return() const
    {
        if constexpr (std::is_void_v<decltype(std::declval<Has<Type>>().template Get<T>())>)
        {
            if constexpr (sizeof...(Types) == 0)
            {
                return;
            }
            else
            {
                return Return<T, Types...>();
            }
        }
        else
        {
            return Has<Type>::template Get<T>();
        }
    }
};

template <>
struct Aggregate<>
{
};

template <typename Type>
constexpr bool is_aggregate = OSIQL_HAS_MEMBER(Type, template Get<0>); // NOLINT(readability-identifier-naming)

#define OSIQL_AGGREGATE_COMPARATOR(OPERATOR, NAME)                                           \
    namespace detail {                                                                       \
    template <size_t N = 0, typename A = void, typename B = void>                            \
    constexpr decltype(auto) aggregate_comparator_##NAME(const A &a, const B &b)             \
    {                                                                                        \
        if constexpr (is_aggregate<A>)                                                       \
        {                                                                                    \
            if constexpr (N >= A::size)                                                      \
            {                                                                                \
                if constexpr (N >= B::size)                                                  \
                {                                                                            \
                    return;                                                                  \
                }                                                                            \
                else if constexpr (is_##NAME##_comparable<A, decltype(b.template Get<N>())>) \
                {                                                                            \
                    return a OPERATOR b.template Get<N>();                                   \
                }                                                                            \
                else                                                                         \
                {                                                                            \
                    return aggregate_comparator_##NAME<N + 1>(a, b);                         \
                }                                                                            \
            }                                                                                \
            else if constexpr (is_##NAME##_comparable<decltype(a.template Get<N>()), B>)     \
            {                                                                                \
                return a.template Get<N>() OPERATOR b;                                       \
            }                                                                                \
            else                                                                             \
            {                                                                                \
                return aggregate_comparator_##NAME<N + 1>(a, b);                             \
            }                                                                                \
        }                                                                                    \
        else /*if constexpr(is_aggregate<B>)*/                                               \
        {                                                                                    \
            if constexpr (N >= B::size)                                                      \
            {                                                                                \
                return;                                                                      \
            }                                                                                \
            else if constexpr (is_##NAME##_comparable<A, decltype(b.template Get<N>())>)     \
            {                                                                                \
                return a OPERATOR b.template Get<N>();                                       \
            }                                                                                \
            else                                                                             \
            {                                                                                \
                return aggregate_comparator_##NAME<N + 1>(a, b);                             \
            }                                                                                \
        }                                                                                    \
    }                                                                                        \
    }                                                                                        \
    template <typename A, typename B, OSIQL_REQUIRES(is_aggregate<A> || is_aggregate<B>)>    \
    constexpr bool operator OPERATOR(const A &a, const B &b)                                 \
    {                                                                                        \
        return detail::aggregate_comparator_##NAME(a, b);                                    \
    }

OSIQL_AGGREGATE_COMPARATOR(<, less)
OSIQL_AGGREGATE_COMPARATOR(<=, less_or_equal)
OSIQL_AGGREGATE_COMPARATOR(==, equal)
OSIQL_AGGREGATE_COMPARATOR(!=, not_equal)
OSIQL_AGGREGATE_COMPARATOR(>, greater)
OSIQL_AGGREGATE_COMPARATOR(>=, greater_or_equal)

template <typename T, typename... Ts>
std::ostream &operator<<(std::ostream &os, const Aggregate<T, Ts...> &aggregate)
{
    os << '{' << get<raw_t<T>>(aggregate);
    ((os << ", " << get<raw_t<Ts>>(aggregate)), ...);
    return os << '}';
}
} // namespace osiql
