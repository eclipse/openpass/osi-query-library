/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <functional>

#include "Common.h"

namespace osiql {
template <typename Type, typename Comparator, typename Fallback = std::less<>>
struct StableCompare
{
    template <typename A, typename B>
    constexpr bool operator()(A &&, B &&) const;
};

template <typename Type, typename Fallback = std::less<>>
using StableLess = StableCompare<Type, std::less<>, Fallback>;

template <typename Type, typename Fallback = std::less<>>
using StableGreater = StableCompare<Type, std::greater<>, Fallback>;

template <typename Type, typename Fallback = std::less<>>
using StableLessEqual = StableCompare<Type, std::less_equal<>, Fallback>;

template <typename Type, typename Fallback = std::less<>>
using StableGreaterEqual = StableCompare<Type, std::greater_equal<>, Fallback>;

template <typename Type, typename Comparator>
struct Compare
{
    using is_transparent = std::true_type;

    template <typename A, typename B>
    constexpr bool operator()(A &&, B &&) const;

    template <typename B = ReturnType<Type>>
    struct Than
    {
        template <typename A>
        constexpr bool operator()(A &&) const;

        B b;
    };

#ifdef __clang__ // GCC does not support deduction guides for nested template classes
    template <typename Input>
    Than(Input &) -> Than<Input &>;

    template <typename Input>
    Than(Input &&) -> Than<Input>;
#endif
};

template <typename Type, typename Comparator>
struct CompareEquality
{
    using is_transparent = std::true_type;

    template <typename A, typename B>
    constexpr bool operator()(A &&, B &&) const;

    template <typename B = ReturnType<Type>>
    struct To
    {
        template <typename A>
        constexpr bool operator()(A &&) const;

        B b;
    };

#ifdef __clang__ // GCC does not support deduction guides for nested template classes
    template <typename Input>
    To(Input &) -> To<Input &>;

    template <typename Input>
    To(Input &&) -> To<Input>;
#endif
};

template <typename T>
using Less = Compare<T, std::less<>>;

template <typename T>
using Greater = Compare<T, std::greater<>>;

template <typename T>
using LessEqual = Compare<T, std::less_equal<>>;

template <typename T>
using GreaterEqual = Compare<T, std::greater_equal<>>;

template <typename T>
using Equal = CompareEquality<T, std::equal_to<>>;

template <typename T>
using NotEqual = CompareEquality<T, std::not_equal_to<>>;
} // namespace osiql
