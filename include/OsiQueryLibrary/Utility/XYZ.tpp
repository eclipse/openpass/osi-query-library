/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "XYZ.h"

#include "Common.h"
#include "Is.h"

namespace osiql {
template <size_t I, typename T>
constexpr decltype(auto) At(const T &t)
{
    if constexpr (I == 0)
    {
        return X{}(t);
    }
    else if constexpr (I == 1)
    {
        return Y{}(t);
    }
    else if constexpr (I == 2)
    {
        return Z{}(t);
    }
    else
    {
        return 0.0;
    }
}

template <typename T>
constexpr size_t Dimensions()
{
    return Is<X>::in<T> ? Is<Y>::in<T> ? Is<Z>::in<T> ? 3 : 2 : 1 : 0;
}

template <typename T>
constexpr size_t Dimensions(const T &)
{
    return Dimensions<T>();
}
} // namespace osiql
