/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#define OSIQL_INCLUDE_PICK
#include "Pick.h"

#include "Get.tpp"

namespace osiql {
struct Distance;

OSIQL_SET_RETURN_TYPE(Min<Distance>, double)
OSIQL_GET(Min<Distance>, distance, GetMinDistance(), GetStartDistance(), GetDistance());

OSIQL_SET_RETURN_TYPE(Max<Distance>, double)
OSIQL_GET(Max<Distance>, distance, GetMaxDistance(), GetEndDistance(), GetDistance());

template <typename Comparator>
template <typename Type>
constexpr decltype(auto) Pick<Comparator>::operator()(Type &&a, Type &&b) const
{
    return Comparator{}(std::forward<Type>(a), std::forward<Type>(b)) ? std::forward<Type>(a) : std::forward<Type>(b);
}

template <typename Type, typename After, typename Before>
template <typename Input>
constexpr bool Between<Type, After, Before>::operator()(Input &&input) const
{
    decltype(auto) value{get<Type>(std::forward<Input>(input))};
    return After{}(value, min) && Before{}(value, max);
}
} // namespace osiql
