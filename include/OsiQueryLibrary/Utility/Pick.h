/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#ifndef OSIQL_INCLUDE_PICK
#error Please include Pick.h indirectly via Pick.tpp
#endif
#undef OSIQL_INCLUDE_PICK

#include "Common.h"
#include "Compare.h"

namespace osiql {
template <typename Comparator>
struct Pick
{
    //! Tests the comparator on both arguments and returns the first if it returned true.
    //! Otherwise the latter element is returned.
    //!
    //! \tparam Type Type of the arguments
    //! \return One of the passed arguments
    template <typename Type>
    constexpr decltype(auto) operator()(Type &&, Type &&) const;

    //! Picks the extractor value out of the given argument
    //! that is selected by this class's comparator.
    //!
    //! \tparam Type Arbitrary type that contains one or more extractor values
    //! \return The extractor value selected from the given input
    // template <typename Type>
    // constexpr decltype(auto) operator()(Type &&) const;
};

template <typename Type>
using Min = Pick<Less<Type>>;

template <typename Type>
using Max = Pick<Greater<Type>>;

template <typename Type,                            //
          typename After = GreaterEqual<Max<Type>>, //
          typename Before = Less<Min<Type>>>
struct Between
{
    ReturnType<Type> min{};
    ReturnType<Type> max{};

    template <typename Input>
    constexpr bool operator()(Input &&) const;
};
} // namespace osiql
