/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Length.h"

#include "Extract.tpp"
#include "XYZ.h"

namespace osiql {
template <typename T>
constexpr double Length::operator()(const T &t) const
{
    if constexpr (is_pointer<T>)
    {
        return Length{}(*t);
    }
    else if constexpr (Dimensions<T> > 1)
    {
        return sqrt(SquaredLength(t));
    }
    else if constexpr (OSIQL_HAS_MEMBER(T, Length()))
    {
        return t.Length();
    }
    else if constexpr (OSIQL_HAS_MEMBER(T, GetLength()))
    {
        return t.GetLength();
    }
    else if constexpr (OSIQL_HAS_MEMBER(T, length))
    {
        return t.length;
    }
    else
    {
        return extract<Direction::Upstream>(t) - extract<Direction::Downstream>(t);
    }
}

template <typename T>
constexpr double SquaredLength(const T &t)
{
    if constexpr (Dimensions<T> == 2)
    {
        return X{}(t)*X{}(t) + Y{}(t)*Y{}(t);
    }
    else if constexpr (Dimensions<T> == 3)
    {
        return X{}(t)*X{}(t) + Y{}(t)*Y{}(t) + Z{}(t)*Z{}(t);
    }
    else if constexpr (OSIQL_HAS_MEMBER(T, GetLength()))
    {
        constexpr const double length{t.GetLength()};
        return length * length;
    }
    else if constexpr (OSIQL_HAS_MEMBER(T, Length()))
    {
        constexpr const double length{t.Length()};
        return length * length;
    }
    else if constexpr (OSIQL_HAS_MEMBER(T, length))
    {
        return t.length * t.length;
    }
    else if constexpr (is_pointer<T>)
    {
        return SquaredLength(*t);
    }
    else
    {
        constexpr const double length{Length{}(t)};
        return length * length;
    }
}
} // namespace osiql
