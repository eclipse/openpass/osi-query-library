/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Expression.h"

namespace osiql {
template <typename... Predicate>
template <typename Value>
constexpr bool Conjunction<Predicate...>::operator()(Value &&value) const
{
    return (Has<Predicate>::template Get<Predicate>()(value) && ...);
}

template <typename... Predicate>
template <typename Value>
constexpr bool Disjunction<Predicate...>::operator()(Value &&value) const
{
    return (Has<Predicate>::template Get<Predicate>()(value) || ...);
}
} // namespace osiql
