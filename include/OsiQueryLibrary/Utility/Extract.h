/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Types/Enum.h"

namespace osiql {
template <auto Value = Direction::Downstream>
struct Extract
{
    template <typename T>
    constexpr double operator()(const T &) const;

    struct Less
    {
        template <typename T, typename U>
        constexpr bool operator()(const T &, const U &) const;
    };
    static constexpr Less less{};

    struct Greater
    {
        template <typename T, typename U>
        constexpr bool operator()(const T &, const U &) const;
    };
    static constexpr Greater greater{};

    struct LessEqual
    {
        template <typename T, typename U>
        constexpr bool operator()(const T &, const U &) const;
    };
    static constexpr LessEqual lessEqual{};

    struct GreaterEqual
    {
        template <typename T, typename U>
        constexpr bool operator()(const T &, const U &) const;
    };
    static constexpr GreaterEqual greaterEqual{};
};

template <auto Value, typename T>
constexpr double extract(const T &);

template <typename T, typename Enum>
constexpr double extract(const T &, Enum);

template <typename T, typename U, typename Enum>
constexpr bool less(const T &, const U &, Enum);

template <typename T, typename U, typename Enum>
constexpr bool greater(const T &, const U &, Enum);

template <typename T, typename U, typename Enum>
constexpr bool lessEqual(const T &, const U &, Enum);

template <typename T, typename U, typename Enum>
constexpr bool greaterEqual(const T &, const U &, Enum);

template <typename Enum, typename Container, typename Content>
constexpr bool Contains(const Container &, const Content &, double epsilon = 0.0);

template <auto Value, typename T, typename U>
constexpr double min(const T &, const U &);

template <typename Value, typename T, typename U>
constexpr double min(const T &, const U &, Value);

template <auto Value, typename T, typename U>
constexpr double max(const T &, const U &);

template <typename Value, typename T, typename U>
constexpr double max(const T &, const U &, Value);

template <auto Value, typename From, typename To>
constexpr double GetStartDifference(const From &, const To &);

template <typename From, typename To, typename Enum>
constexpr double GetStartDifference(const From &, const To &, Enum);

template <auto Value, typename From, typename To>
constexpr double GetEndDifference(const From &, const To &);

template <typename From, typename To, typename Enum>
constexpr double GetEndDifference(const From &, const To &, Enum);

template <auto Value, typename From, typename To>
constexpr double Difference(const From &, const To &);

template <typename From, typename U, typename Enum>
constexpr double Difference(const From &, const U &, Enum);

template <auto Value, typename T>
constexpr double Increase(const T &, double amount);

template <typename T, typename Enum>
constexpr double Increase(const T &, double a, Enum);
} // namespace osiql
