/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#define OSIQL_INCLUDE_IS
#include "Is.h"

namespace osiql {
template <typename Enum>
constexpr Is<Enum>::Is(std::initializer_list<Enum> values)
{
    for (Enum value : values)
    {
        mapping[static_cast<size_t>(value)] = true;
    }
}

template <typename Enum>
template <typename T>
constexpr bool Is<Enum>::operator()(const T &t) const
{
    return mapping[static_cast<size_t>(get<Enum>(t))];
}

template <typename Type>
struct Get<Not<Type>>
{
    template <typename Input>
    constexpr decltype(auto) operator()(Input &&) const
    {
        if constexpr (Is<Type>::template in<Input>)
        {
            return;
        }
        else
        {
            return true;
        }
    }
};
} // namespace osiql
