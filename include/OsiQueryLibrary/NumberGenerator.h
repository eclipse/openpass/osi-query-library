/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Default number generator

#include <random>

namespace osiql {
namespace detail {
static inline std::random_device::result_type GetDefaultSeed()
{
    std::random_device device;
    return device();
}
} // namespace detail

//! \brief Seedable invocable that returns a random double within [0,1) when invoked
class NumberGenerator
{
public:
    //! Constructs a NumberGenerator from a given seed. Uses a default seed if none is provided.
    //!
    //! \param seed
    NumberGenerator(std::random_device::result_type seed = detail::GetDefaultSeed()) :
        generator{seed}, distribution{0.0, 1.0} {};

    //! Implicit modifying cast from NumberGenerator to a random double within [0,1)
    //!
    //! \return double
    inline double operator()() noexcept
    {
        return distribution(generator);
    }

private:
    std::mt19937 generator;
    std::uniform_real_distribution<> distribution;
};
} // namespace osiql
