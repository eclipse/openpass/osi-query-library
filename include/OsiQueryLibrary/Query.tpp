/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Query.h"

#include <boost/iterator/function_output_iterator.hpp>

#include "OsiQueryLibrary/Object/MovingObject.tpp"
#include "OsiQueryLibrary/Object/Vehicle.tpp"
#include "OsiQueryLibrary/Street/Boundary.tpp"
#include "OsiQueryLibrary/Street/Lane.tpp"
#include "OsiQueryLibrary/Street/Road.tpp"
#include "OsiQueryLibrary/Trait/BoostGeometry.h"
#include "OsiQueryLibrary/World.tpp"

namespace osiql {
template <typename ObjectType>
void Query::UpdateObject(ObjectType &object)
{
    object.UpdateShapeAndBounds();
    object.template SetOverlaps<Road>(InternalFindOverlapping<Road>(object.shape, object.bounds));
    // TODO: Performance - Stop computing object centers
    object.positions.clear();
    for (auto &overlap : object.template GetOverlaps<Road>())
    {
        object.positions.push_back(overlap.GetRoad().Localize(Pose<XY>{object.GetXY(), object.GetYaw()}));
    }
    // TODO: Performance - Get lane overlaps from road overlaps
    object.template SetOverlaps<Lane>(InternalFindOverlapping<Lane>(object.shape, object.bounds));
}

template <typename GlobalPoint>
std::vector<Localization<GlobalPoint>> Query::Localize(const GlobalPoint &point) const
{
    std::vector<Localization<GlobalPoint>> result;
    const auto inserter = [&](const Road *road) {
        auto localization{road->GetReferenceLine().Localize(point)};
        for (const Lane *closestLane : road->GetLanesContaining(localization))
        {
            result.emplace_back(localization, *closestLane);
        }
    };
    roads.query(boost::geometry::index::intersects(point), boost::make_function_output_iterator(inserter));
    return result;
}

template <Traversal T, typename Origin, typename Destination>
std::optional<Route<T>> Query::GetRoute(const Origin &origin, const Destination &destination) const
{
    static_assert(T != Traversal::Any);
    if constexpr (Dimensions<Origin>() > 1 && Dimensions<Destination>() > 1)
    {
        const auto origins{Localize(origin)};
        if (origins.empty())
        {
            return std::nullopt;
        }
        const auto destinations{Localize(destination)};

        std::vector<std::shared_ptr<const Node<T>>> roots;
        std::transform(origins.begin(), origins.end(), std::back_inserter(roots), [](const Point<const Lane> &point) {
            return Node<T>::Create(point);
        });
        struct Data
        {
            const Point<const Lane> *origin;
            const Point<const Lane> *destination;
            const Node<T> *goal;
            double distance;
        };
        std::vector<Data> paths;
        paths.reserve(origins.size() * destinations.size());
        for (const auto &destination : destinations)
        {
            auto root{roots.begin()};
            for (auto origin{origins.begin()}; root != roots.end(); ++root, ++origin)
            {
                if (const Node<T> *goal{(*root)->FindClosestNode(destination)}; goal != nullptr)
                {
                    if (const double distance{goal->GetDistance(destination)}; distance >= -EPSILON)
                    {
                        paths.emplace_back(Data{&*origin, &destination, goal, distance});
                    }
                }
            }
        }
        if (paths.empty())
        {
            return std::nullopt;
        }
        const auto &path{*std::min_element(paths.begin(), paths.end(), Less<Distance>{})};
        return Route<T>{*path.origin, *path.destination, path.goal->GetPathFromDepth(0)};
    }
    else if constexpr (Dimensions<Origin>() > 1)
    {
        const auto origins{Localize(origin)};
        if (origins.empty())
        {
            return std::nullopt;
        }
        std::vector<std::optional<Route<T>>> routes;
        routes.reserve(origins.size());
        std::transform(origins.begin(), origins.end(), std::back_inserter(routes), [&destination](const auto &origin) {
            return Route<T>{origin, destination};
        });
        const auto route{std::min_element(routes.begin(), routes.end(), [](const auto &a, const auto &b) {
            return !b.has_value() || (a.has_value() && a.value().GetLength() < b.value().GetLength());
        })};
        if (!route->has_value())
        {
            return std::nullopt;
        }
        return std::move(*route);
    }
    else if constexpr (Dimensions<Destination>() > 1)
    {
        const auto destinations{Localize(destination)};
        if (destinations.empty())
        {
            std::cout << "GetRoute: Destination does not lie on any road. Returning the starting point\n";
            return {origin};
        }
        Node<T> root{origin};
        double shortestDistance{std::numeric_limits<double>::max()};
        const Node<T> *closestGoal;
        const auto *preferredDestination{&destinations.front()};
        for (const auto &localDestination : destinations)
        {
            if (const auto *goal{root.FindClosestNode(localDestination)}; goal)
            {
                const double distance{goal->GetDistance(localDestination)};
                if (distance < shortestDistance)
                {
                    shortestDistance = distance;
                    closestGoal = goal;
                    preferredDestination = &localDestination;
                }
            }
        }
        if (!closestGoal)
        {
            return std::nullopt;
        }
        return Route<T>{origin, *preferredDestination, std::move(root), closestGoal->GetPathFromDepth(0)};
    }
    else if constexpr (Are<S, Lane>::template in<Origin, Destination>)
    {
        Route<T> result{origin, destination};
        if (result.destination != destination)
        {
            return std::nullopt;
        }
        return result;
    }
    else if constexpr (std::is_same_v<Origin, Destination> && Dimensions<decltype(*std::declval<Origin>())>() > 1)
    {
        if (origin == destination)
        {
            return std::nullopt;
        }
        const auto origins{Localize(*origin)};
        if (origins.empty())
        {
            return std::nullopt;
        }
        struct NodeData
        {
            std::shared_ptr<const Node<T>> root{nullptr};
            const Node<T> *leaf{nullptr};
            double distance{std::numeric_limits<double>::max()};
            const Point<const Lane> *origin{nullptr};
            const Point<const Lane> *destination{nullptr};
        };
        std::vector<NodeData> nodes;
        nodes.reserve(origins.size());
        std::transform(origins.begin(), origins.end(), std::back_inserter(nodes), [](const Point<const Lane> &point) {
            std::shared_ptr<const Node<T>> root{Node<T>::Create(point)};
            return NodeData{root, root.get(), 0.0, &point, &point};
        });

        std::vector<NodeData> nextNodes;
        for (auto checkpoint{std::next(origin)}; checkpoint != destination; ++checkpoint)
        {
            const auto destinations{Localize(*checkpoint)};
            if (destinations.empty())
            {
                return std::nullopt;
            }
            nextNodes.reserve(destinations.size());
            std::transform(destinations.begin(), destinations.end(), std::back_inserter(nextNodes), [&](const Point<const Lane> &goal) {
                return std::transform_reduce(
                    nodes.begin(), nodes.end(), NodeData{},
                    [](const auto &a, const auto &b) { return std::min(a, b, Less<Distance>{}); },
                    [&goal](const auto &data) {
                        if (const Node<T> *nextNode{data.leaf->FindClosestNode(goal)}; nextNode != nullptr)
                        {
                            const double distance{nextNode->GetDistance(goal)};
                            return data.distance <= distance ? NodeData{data.root, nextNode, distance, data.origin, data.destination} : NodeData{};
                        }
                        return NodeData{};
                    }
                );
            });
            nextNodes.erase(
                std::remove_if(nextNodes.begin(), nextNodes.end(), [](const auto &node) { return node.root == nullptr; }),
                nextNodes.end()
            );
            if (nextNodes.empty())
            {
                return std::nullopt;
            }
            std::swap(nodes, nextNodes);
            nextNodes.clear();
        }
        const auto &path{*std::min_element(nodes.begin(), nodes.end(), Less<Distance>{})};
        return Route<T>{*path.origin, *path.destination, path.leaf->GetPathFromDepth(0)};
    }
    else if constexpr (std::is_same_v<Origin, Destination> && Is<S>::template in<decltype(*std::declval<Origin>)>())
    {
        if (origin == destination)
        {
            return std::nullopt;
        }
        const auto root{Node<T>::Create(*origin)};
        const Node<T> *pathEnd{root.get()};
        for (auto waypoint{std::next(origin)}; waypoint != destination; ++waypoint)
        {
            const Node<T> *newEnd{pathEnd->FindClosestNode(*waypoint)};
            if (!newEnd)
            {
                std::cout << "GetRoute<" << T << ">: Skipping waypoint " << *waypoint << ", as no route to it exists\n";
                continue;
            }
            pathEnd = newEnd;
        }
        return Route<T>{*origin, *std::prev(destination), *root, pathEnd->GetPathFromDepth(0)};
    }
    else if constexpr (std::is_same_v<Destination, osi3::TrafficAction::AcquireGlobalPositionAction>)
    {
        if (!destination.has_position() || !destination.position().has_x() || !destination.position().has_y())
        {
            std::cerr << "GetRoute< " << T << ">: AcquireGlobalPositionAction has no assigned position.\n";
            return std::nullopt;
        }
        return GetRoute<T>(origin, destination.position());
    }
    else
    {
        static_assert(always_false<Origin, Destination>, "Not supported");
    }
}

template <Traversal T, typename Origin, typename RNG>
std::optional<Route<T>> Query::GetRandomRoute(const Origin &origin, RNG &&rng, double maxDistance) const
{
    if constexpr (Dimensions<Origin>() > 1)
    {
        const auto localizations{Localize(origin)};
        if (localizations.empty())
        {
            std::cout << "GetRandomRoute<" << T << ">: Origin " << origin << " does not lie on any road\n";
            return std::nullopt;
        }
        return localizations.front().template GetRandomRoute<T>(std::forward<RNG>(rng), maxDistance);
    }
    else if constexpr (HAS_MEMBER(Origin, template GetRandomRoute<T>(rng, maxDistance)))
    {
        return origin.template GetRandomRoute<T>(std::forward<RNG>(rng), maxDistance);
    }
    else
    {
        static_assert(always_false<Origin>, "Origin type not supported");
    }
}

template <Traversal T, typename Origin, typename Selector>
std::optional<Route<T>> Query::GetCustomRoute(const Origin &origin, Selector &&selector) const
{
    if constexpr (Dimensions<Origin>() > 1)
    {
        const auto localizations{Localize(origin)};
        if (localizations.empty())
        {
            std::cout << "GetCustomRoute<" << T << ">: Origin " << origin << " does not lie on any road\n";
            return std::nullopt;
        }
        return localizations.front().template GetCustomRoute<T>(std::forward<Selector>(selector));
    }
    else if constexpr (HAS_MEMBER(Origin, template GetCustomRoute<T>(std::forward<Selector>(selector))))
    {
        return origin.template GetCustomRoute<T>(std::forward<Selector>(selector));
    }
    else
    {
        static_assert(always_false<Origin>, "Origin type not supported");
    }
}

template <Traversal T>
std::optional<Route<T>> Query::GetRoute(const osi3::TrafficAction::FollowPathAction &action) const
{
    static_assert(T != Traversal::Any);
    if (action.path_point().empty())
    {
        return std::nullopt;
    };
    if (std::any_of(action.path_point().begin(), action.path_point().end(), [](const osi3::StatePoint &point) {
            return !point.has_position();
        }))
    {
        std::cerr << "osiql::GetRoute: Not all points in the FollowPathAction have positions. Returning an empty route.\n";
        return std::nullopt;
    }
    return GetRoute<T>(action.path_point().begin(), action.path_point().end());
}

template <typename T>
std::vector<Overlap<const T>> Query::FindOverlapping(const Shape &shape, const Bounds &bounds) const
{
    std::vector<Overlap<const T>> result;
    const auto inserter = [&result, &shape](const T *object) {
        if constexpr (std::is_base_of_v<Lane, T> || std::is_base_of_v<Road, T>)
        {
            Shape intersections{detail::GetIntersections(get<Shape>(object), shape)};
            if (intersections.size() > 2)
            {
                result.emplace_back(std::move(intersections), *object, get<Road>(object).GetReferenceLine().Localize(intersections.begin(), intersections.end()));
            }
        }
        else
        {
            Shape intersections{detail::GetIntersections(get<Shape>(object), shape)};
            if (intersections.size() > 2)
            {
                result.emplace_back(std::move(intersections), *object);
            }
        }
    };
    GetRTree<T>().query(boost::geometry::index::intersects(bounds), boost::make_function_output_iterator(inserter));
    return result;
}

template <typename T>
std::vector<Overlap<T>> Query::InternalFindOverlapping(const Shape &shape, const Bounds &bounds)
{
    std::vector<Overlap<T>> result;
    const auto inserter = [&result, &shape](T *object) {
        if constexpr (std::is_base_of_v<Overlapable<T>, T>)
        {
            Shape intersections{detail::GetIntersections(get<Shape>(object), shape)};
            if (intersections.size() > 2)
            {
                result.emplace_back(std::move(intersections), *object, get<Road>(object).GetReferenceLine().Localize(intersections.begin(), intersections.end()));
            }
        }
        else
        {
            Shape intersections{detail::GetIntersections(get<Shape>(object), shape)};
            if (intersections.size() > 2)
            {
                result.emplace_back(std::move(intersections), *object);
            }
        }
    };
    GetRTree<T>().query(boost::geometry::index::intersects(bounds), boost::make_function_output_iterator(inserter));
    return result;
}

template <typename T>
constexpr const RTree<T> &Query::GetRTree() const
{
    if constexpr (std::is_same_v<T, Lane>)
    {
        return lanes;
    }
    else if constexpr (std::is_same_v<T, Road>)
    {
        return roads;
    }
    else if constexpr (std::is_same_v<T, MovingObject>)
    {
        return movingObjects;
    }
    else if constexpr (std::is_same_v<T, StaticObject>)
    {
        return staticObjects;
    }
    else if constexpr (std::is_same_v<T, TrafficLight>)
    {
        return trafficLights;
    }
    else if constexpr (std::is_same_v<T, TrafficSign>)
    {
        return trafficSigns;
    }
    else
    {
        static_assert(always_false<T>, "Query maintains no RTree of the given type");
    }
}

template <typename T>
constexpr RTree<T> &Query::GetRTree()
{
    return const_cast<RTree<T> &>(std::as_const(*this).GetRTree<T>());
}

namespace detail {
template <typename T, typename U>
std::vector<XY> GetIntersections(const T &polygonA, const U &polygonB)
{
    std::vector<XY> intersections{};
    intersections.reserve(8u);

    std::vector<XY> edgesA(polygonA.size());
    for (size_t i{1}; i < polygonA.size(); ++i)
    {
        edgesA[i - 1] = polygonA[i] - polygonA[i - 1];
    }
    edgesA.back() = polygonA.front() - polygonA.back();

    std::vector<XY> edgesB(polygonB.size());
    for (size_t i{1}; i < polygonB.size(); ++i)
    {
        edgesB[i - 1] = polygonB[i] - polygonB[i - 1];
    }
    edgesB.back() = polygonB.front() - polygonB.back();

    for (size_t i{0}; i < edgesB.size(); ++i)
    {
        for (size_t k{0}; k < edgesA.size(); ++k)
        {
            const double det{edgesB[i].Cross(edgesA[k])};
            const double lambda{(edgesA[k].Cross(polygonB[i]) + polygonA[k].Cross(edgesA[k])) / det};
            const double kappa{(edgesB[i].Cross(polygonB[i]) + polygonA[k].Cross(edgesB[i])) / det};
            if (lambda >= 0.0 && lambda <= 1.0 && kappa >= 0.0 && kappa <= 1.0)
            {
                const double x{polygonB[i].x + lambda * edgesB[i].x};
                const double y{polygonB[i].y + lambda * edgesB[i].y};
                if (intersections.empty() || intersections.back().x != x || intersections.back().y != y)
                {
                    intersections.emplace_back(x, y);
                }
            }
        }
    }

    std::copy_if(polygonA.begin(), polygonA.end(), std::back_inserter(intersections), [&](const XY &a) {
        return (std::find(intersections.begin(), intersections.end(), a) == intersections.end()) && a.IsWithin(polygonB);
    });
    std::copy_if(polygonB.begin(), polygonB.end(), std::back_inserter(intersections), [&](const XY &b) {
        return (std::find(intersections.begin(), intersections.end(), b) == intersections.end()) && b.IsWithin(polygonA);
    });
    return intersections;
}

template <typename T, typename U>
bool HasIntersections(const T &polygonA, const U &polygonB)
{
    std::vector<XY> edgesA(polygonA.size());
    for (size_t i{1}; i < polygonA.size(); ++i)
    {
        edgesA[i - 1] = polygonA[i] - polygonA[i - 1];
    }
    edgesA.back() = polygonA.front() - polygonA.back();

    std::vector<XY> edgesB(polygonB.size());
    for (size_t i{1}; i < polygonB.size(); ++i)
    {
        edgesB[i - 1] = polygonB[i] - polygonB[i - 1];
    }
    edgesB.back() = polygonB.front() - polygonB.back();

    for (size_t i{0}; i < edgesB.size(); ++i)
    {
        for (size_t k{0}; k < edgesA.size(); ++k)
        {
            const double det{edgesB[i].Cross(edgesA[k])};
            const double lambda{(edgesA[k].Cross(polygonB[i]) + polygonA[k].Cross(edgesA[k])) / det};
            const double kappa{(edgesB[i].Cross(polygonB[i]) + polygonA[k].Cross(edgesB[i])) / det};
            if (lambda >= 0.0 && lambda <= 1.0 && kappa >= 0.0 && kappa <= 1.0)
            {
                return true;
            }
        }
    }

    return std::any_of(polygonA.begin(), polygonA.end(), [&](const XY &a) { return a.IsWithin(polygonB); }) // clang-format off
        || std::any_of(polygonB.begin(), polygonB.end(), [&](const XY &b) { return b.IsWithin(polygonA); }); // clang-format on
}
} // namespace detail
} // namespace osiql
