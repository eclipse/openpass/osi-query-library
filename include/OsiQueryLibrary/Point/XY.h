/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Two-dimensional global point that additionally supports queries like what side of a line it is on

#include "OsiQueryLibrary/Trait/Size.h"
#include "OsiQueryLibrary/Types/Enum/Side.h"
#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/XYZ.h"
#include "Vector.h"

namespace osiql {
struct ST;

enum class Winding : bool
{
    Clockwise,
    CounterClockwise
};
OSIQL_GET(Winding, GetWinding(), winding);

//! \brief An x & y coordinate used to represent a planar vector or global point.
//! Provides transformation methods, conversions and containment queries.
template <>
struct Vector<2>
{
    static constexpr size_t size = 2;

    //! Constructs a 2d vector from an x- & y-component
    //!
    constexpr Vector(double x = 0.0, double y = 0.0);

    //! Constructs a 2d vector from the given object
    //!
    //! \tparam Type Type that has an x- & y- component
    template <typename Type, OSIQL_REQUIRES(Are<X, Y>::in<Type>)>
    constexpr Vector(const Type &vector) :
        x{X{}(vector)}, y{Y{}(vector)}
    {
    }

    //! Returns whether this vector has a non-zero component
    //!
    //! \return bool
    constexpr bool HasLength() const noexcept;

    //! Adds the coordinate components of the right-hand side to those of the left-hand side
    //!
    //! \tparam Type Type with an x- & y-component
    template <typename Type>
    constexpr void operator+=(const Type &) noexcept;

    //! Subtracts the coordinate components of the right-hand side from those of the left-hand side
    //!
    //! \tparam Type Type with an x- & y-component
    template <typename Type>
    constexpr void operator-=(const Type &) noexcept;

    //! Multiplies each component of the left-hand side by the given factor
    //!
    //! \param factor
    constexpr void operator*=(double factor) noexcept;

    //! Divides each component of the left-hand side by the given divisor
    //!
    //! \param divisor
    constexpr void operator/=(double divisor);

    //! Computes the dot product between this point and the given one, which is the chain product
    //! of their Euclidean magnitudes and the cosine of the angle between them.
    //!
    //! \return double
    template <typename Type>
    constexpr double Dot(const Type &) const noexcept;

    //! Computes the cross product between this point and the given one.
    //!
    //! \return double
    template <typename Type>
    constexpr double Cross(const Type &) const noexcept;

    //! Returns the squared length of this vector.
    //!
    //! \return double
    constexpr double SquaredLength() const noexcept;

    //! Returns the numerically robust length of this vector.
    //!
    //! \return double
    double Length() const;

    //! Returns the angle of this vector relative to the x-axis.
    //!
    //! \return double counter-clockwise ascending angle in radians within [-π, π]
    double Angle() const noexcept;

    //! Returns this point rotated counter-clockwise by the given radians.
    //!
    //! \param angle
    //! \return XY
    XY Rotate(double angle) const noexcept;

    //! Returns this vector projected onto a line passing through the coordinate origin
    //! with the given angle relative to the x-axis.
    //!
    //! \param angle counter-clockwise angle in radians relative to the x-axis
    //! \return XY
    XY Projection(double angle) const;

    //! Returns a projection of this point onto the line defined by the given points.
    //!
    //! \tparam Start Point on the line
    //! \tparam End Different point on the line
    //! \return XY Point on the line defined by the given points
    template <typename Start, typename End>
    XY Projection(const Start &, const End &) const;

    //! Returns the signed length of the projection of this point onto a lane passing
    //! through the coordinate vector with the given angle relative to the x-axis.
    //!
    //! \param angle
    //! \return double
    double ProjectionLength(double angle) const;

    //! Returns the component-wise multiplication of this vector with a given one
    //!
    //! \tparam Type Type with an x- & y-component
    //! \return constexpr XY
    template <typename Type>
    constexpr XY Times(const Type &) const;

    //! Return what side of the line defined by the given points this point lies on.
    //! Returns Side::None if the points are collinear
    //!
    //! \tparam Start Point on the line
    //! \tparam End Different point on the line
    //! \return Side
    template <typename Start, typename End>
    Side GetSide(const Start &, const End &) const;

    //! Returns whether this XY is inside the given simple polygon, which does not need to be convex.
    //! The implementation checks for an uneven number of hits with the polygon's edges from a ray cast to
    //! this point. Bounds checking is not performed. A point on the edge is treated as inside the polygon.
    //!
    //! \tparam Shape Open span of points representing a simply polygon
    //! in either clockwise or counter-clockwise orientation.
    //! \return bool Whether this vector is not outside the given polygon
    template <typename Shape>
    constexpr bool IsWithin(const Shape &) const;

    //! Returns whether this XY is inside the given simple polygon, which does not need to be convex.
    //! The implementation checks for an uneven number of hits with the polygon's edges from a ray cast to
    //! this point. Bounds checking is not performed. A point on the edge is treated as inside the polygon.
    //!
    //! \tparam It Iterator to a vertex of a simple polygon
    //! \return bool Whether the polygon represented by the range of vertices overlaps with this point
    template <typename It>
    constexpr bool IsWithin(It, It) const;

    //! Receives a range of points and returns an iterator to the point at the end of the edge closest to this point.
    //!
    //! \tparam It
    //! \param it
    //! \param pastLast
    //! \param distance Optional output parameter for the signed distance from the point
    //! \return It
    template <typename It>
    It GetEndOfClosestEdge(It it, It pastLast, double *distance = nullptr) const;

    //! Returns the shortest vector from the line segment defined by the given points to this point.
    //!
    //! \tparam A Global point with an x- & y-coordinate
    //! \tparam B Global point with an x- & y-coordinate
    //! \param a Start of the line segment
    //! \param b End of the line segment
    //! \param squaredEpsilon Minimum squared length of the line segment from a to b. If the squared length
    //! is smaller than this value, the path from a to this point is returned instead.
    //! \return XY
    template <typename A, typename B>
    XY GetPathTo(const A &a, const B &b, double squaredEpsilon = 0.0) const;

    //! Returns the length of the shortest path to the edge defined by the given points.
    //! If this point is on the right side of the edge, the returned distance is negative.
    //!
    //! \param a
    //! \param b
    //! \return double
    double GetSignedDistanceTo(const XY &a, const XY &b) const;

    //! Finds the nearest edge in the given range of points and returns the signed distance of this point to it.
    //! If this point is on the right of said edge, the returned distance is negative.
    //!
    //! \tparam It
    //! \param first
    //! \param pastLast
    //! \return double
    template <typename It>
    double GetSignedDistanceToChain(It first, It pastLast) const;

    //! Converts this point's x- & y-coordinates to s- & t-coordinates relative to the line segment defined by the given vertices.
    //!
    //! \tparam A Type containing both ST coordinates and XY coordinates
    //! \tparam B Type containing both ST coordinates and XY coordinates
    //! \return Point
    template <typename A, typename B>
    ST GetST(const A &, const B &) const;

    //! Returns this vector's clockwise or counter-clockwise normal, which is
    //! this vector rotated 90° in the given winding direction.
    //!
    //! \tparam Winding Clockwise or CounterClockwise
    //! \return constexpr XY The normal of the input
    template <Winding>
    constexpr XY GetPerpendicular() const;

    double x;
    double y;

private:
    template <int PreviousHemisphere, typename It>
    constexpr bool IsWithinHelper(It first, It pastLast) const;

    template <int PreviousRegion, typename It>
    constexpr bool IsWithin(It first, It pastLast) const;

    template <bool State, int PreviousRegion, int CurrentHemisphere, typename It>
    constexpr bool IsWithin(It prev, It current, It pastLast) const;

    template <bool State, int PreviousRegion, int CurrentHemisphere, int NextHemisphere, typename It>
    constexpr bool IsWithinHelper(It prev, It current, It pastLast) const;

    template <bool State, int CurrentHemisphere, int NextRegion, typename PointA, typename PointB, typename PointC, typename It>
    constexpr bool IsWithin(const PointA &a, const PointB &b, const PointC &c, It current, It pastLast) const;

    template <bool State, int PreviousRegion, int CurrentHemisphere, int NextRegion, typename It>
    constexpr bool IsWithin(It prev, It current, It pastLast) const;
};

//! Returns the given vector's clockwise or counter-clockwise normal, which is
//! said vector rotated 90° in the given winding direction.
//!
//! \tparam Winding Clockwise or CounterClockwise
//! \param const XY& Global xy-coordinates whose normal will be returned
//! \return constexpr XY The normal of the input
template <Winding>
constexpr XY GetPerpendicular(const XY &);

void PrintTo(const XY &point, std::ostream *os);

std::ostream &operator<<(std::ostream &, Winding);

template <typename Type, OSIQL_REQUIRES(Dimensions<Type> == 2)>
std::ostream &operator<<(std::ostream &os, const Type &vector)
{
    return os << '(' << X{}(vector) << ", " << Y{}(vector) << ')';
}
} // namespace osiql
OSIQL_SET_SIZE(osiql::Winding, 2)
