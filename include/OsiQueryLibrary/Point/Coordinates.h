/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Base class of any local point, containing its s-coordinate and t-coordinate

#include <iostream>
#include <type_traits> // enable_if

#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Is.tpp" // Are

namespace osiql { // NOLINTBEGIN(bugprone-macro-parentheses)
// get<Lane>(Lane) should return the input, but get<X>(double) should not.
// This macro expands to a specialization of Get for components that only
// returns the desired output if the input contains (instead of is) the output.
#define OSIQL_GET_COORDINATE(COORDINATE, IDENTIFIER)                                                                                                                        \
    OSIQL_SET_RETURN_TYPE(COORDINATE, double)                                                                                                                               \
    template <>                                                                                                                                                             \
    struct Get<COORDINATE>                                                                                                                                                  \
    {                                                                                                                                                                       \
        template <typename Type>                                                                                                                                            \
        constexpr decltype(auto) operator()(Type &&_) const                                                                                                                 \
        {                                                                                                                                                                   \
            if constexpr (std::is_same_v<ReturnType<COORDINATE>, raw_t<Type>>) { return; }                                                                                  \
            else if constexpr (has_member<Type>([](auto &&_) -> decltype(_.Get##COORDINATE()) {})) { return get<ReturnType<COORDINATE>>(_.Get##COORDINATE()); }             \
            else if constexpr (has_member<Type>([](auto &&_) -> decltype(_.IDENTIFIER##_position()) {})) { return get<ReturnType<COORDINATE>>(_.IDENTIFIER##_position()); } \
            else if constexpr (has_member<Type>([](auto &&_) -> decltype(_.IDENTIFIER) {})) { return get<ReturnType<COORDINATE>>(_.IDENTIFIER); }                           \
            else { return default_get<COORDINATE>(std ::forward<Type>(_)); }                                                                                                \
        }                                                                                                                                                                   \
    }
// NOLINTEND(bugprone-macro-parentheses)

//! \brief s-coordinate as defined by OSI. The signed distance from the start of a reference line along the trajectory of
//! that reference line. See https://opensimulationinterface.github.io/open-simulation-interface/structosi3_1_1ReferenceLine.html#:~:text=Rules%20on%20the%20S%20position
struct S
{
};
OSIQL_GET_COORDINATE(S, s);

//! \brief t-coordinate as defined by OSI. The signed distance from the point on a reference line with the same
//! t-coordinate. See https://opensimulationinterface.github.io/open-simulation-interface/structosi3_1_1ReferenceLine.html#:~:text=.-,Adding%20T%20coordinates,-To%20describe%20points
struct T
{
};
OSIQL_GET_COORDINATE(T, t);

//! Coordinate pair relative to an unspecified reference line of a road
struct ST
{
    //! Constructs local st-coordinates from its components
    //!
    //! \param s Signed distance along a reference line from the start of said line
    //! \param t Signed distance from a point on a reference line with the same t-coordinate
    //! to the described point. A point to the right of the reference line has a negative t-coordinate
    constexpr ST(double s, double t);

    //! Constructs a local st-coordinate point from a point
    //!
    //! \tparam Type Type containing an st-coordinate pair
    //! \param t Point with an s- & t-coordinate
    template <typename Type, OSIQL_REQUIRES(Are<S, T>::in<Type>)>
    constexpr ST(const Type &type) :
        s{get<S>(type)}, t{get<T>(type)}
    {
    }

    //! \brief Signed distance along the road's reference from the start of the reference line
    double s;

    //! \brief Signed distance to the nearest point on a reference line. Negative if on the right side
    double t;
};
OSIQL_GET(ST, GetST(), st);

template <typename Type>
constexpr bool operator==(const ST &, const Type &);

template <typename Type>
constexpr bool operator!=(const ST &, const Type &);

std::ostream &operator<<(std::ostream &, const ST &);

struct U
{
};
OSIQL_GET_COORDINATE(U, u);

struct V
{
};
OSIQL_GET_COORDINATE(V, v);

//! \brief Pair of coordinates relative to a lane's centerline instead of st-coordinates,
//! which are relative to a road's reference line
struct UV
{
    //! Constructs local st-coordinates from its components
    //!
    //! \param u Signed distance from the start of a lane in its driving direction
    //! \param v Signed distance along the t-axis of a lane's reference line from the centerline
    //! of said lane Negative if to the right of the centerline (in driving direction).
    constexpr UV(double u, double v);

    //! \brief Signed distance from the start of a lane in its driving direction.
    double u;

    //! \brief Signed distance along the t-axis of a lane's reference line from the centerline
    //! of said lane. Negative if to the right of the centerline (in driving direction).
    double v;

    //! Constructs a local uv-coordinate point from a point
    //!
    //! \tparam Type Type containing an uv-coordinate pair
    //! \param t Point with an u- & v-coordinate
    template <typename Type, OSIQL_REQUIRES(Are<U, V>::in<Type>)>
    constexpr UV(const Type &type) :
        u{get<U>(type)}, v{get<V>(type)}
    {
    }
};
OSIQL_GET(UV, GetUV(), uv);

constexpr bool operator==(const UV &, const UV &);
constexpr bool operator!=(const UV &, const UV &);

std::ostream &operator<<(std::ostream &, const UV &);
} // namespace osiql
