/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Wrapper of a osi3::StatePoint with templatized getters & aggregated setters

#include <osi3/osi_common.pb.h>

#include "OsiQueryLibrary/Point/XY.h"       // XY
#include "OsiQueryLibrary/Types/Rotation.h" // Roll, Pitch, Yaw
#include "OsiQueryLibrary/Types/Timestamp.h"
#include "OsiQueryLibrary/Utility/XYZ.h" // X, Y, Z

namespace osiql {
template <typename Handle = osi3::StatePoint>
struct StatePoint
{
    static_assert(std::is_same_v<std::remove_const_t<std::remove_reference_t<Handle>>, osi3::StatePoint>);

    constexpr StatePoint(Handle &&);

    double GetX() const;

    template <typename Value, OSIQL_REQUIRES(Is<double>::in<Value> && !is_const<Handle>)>
    void SetX(Value &&value)
    {
        handle.mutable_position()->set_x(get<double>(std::forward<Value>(value)));
    }

    double GetY() const;

    template <typename Value, OSIQL_REQUIRES(Is<double>::in<Value> && !is_const<Handle>)>
    void SetY(Value &&value)
    {
        handle.mutable_position()->set_y(get<double>(std::forward<Value>(value)));
    }

    double GetZ() const;

    template <typename Value, OSIQL_REQUIRES(Is<double>::in<Value> && !is_const<Handle>)>
    void SetZ(Value &&value)
    {
        handle.mutable_position()->set_z(get<double>(std::forward<Value>(value)));
    }

    constexpr const osi3::Vector3d &GetPosition() const;

    template <typename H = Handle, OSIQL_REQUIRES(!is_const<H>)>
    constexpr osi3::Vector3d &GetPosition()
    {
        assert(handle.has_position());
        return *handle.mutable_position();
    }

    template <typename Position, OSIQL_REQUIRES(Are<X, Y>::in<Position> && !is_const<Handle>)>
    void SetPosition(Position &&position)
    {
        SetX(X{}(position));
        SetX(Y{}(position));
        SetZ(Z{}(position));
    }

    double GetRoll() const;

    template <typename Value, OSIQL_REQUIRES(Is<double>::in<Value> && !is_const<Handle>)>
    void SetRoll(Value &&value)
    {
        handle.mutable_orientation()->set_roll(get<double>(std::forward<Value>(value)));
    }

    double GetPitch() const;

    template <typename Value, OSIQL_REQUIRES(Is<double>::in<Value> && !is_const<Handle>)>
    void SetPitch(Value &&value)
    {
        handle.mutable_orientation()->set_pitch(get<double>(std::forward<Value>(value)));
    }

    double GetYaw() const;

    template <typename Value, OSIQL_REQUIRES(Is<double>::in<Value> && !is_const<Handle>)>
    void SetYaw(Value &&value)
    {
        handle.mutable_orientation()->set_yaw(get<double>(std::forward<Value>(value)));
    }

    template <typename Rotation = const osi3::Orientation3d &, OSIQL_REQUIRES(Are<Roll, Pitch, Yaw>::in<Rotation>)>
    constexpr Rotation GetOrientation() const
    {
        assert(handle.has_orientation());
        return *handle.orientation();
    }

    template <typename Rotation = osi3::Orientation3d &, typename H = Handle, OSIQL_REQUIRES(!is_const<H> && Are<Roll, Pitch, Yaw>::in<Rotation>)>
    constexpr Rotation GetOrientation()
    {
        assert(handle.has_orientation());
        return *handle.mutable_orientation();
    }

    template <typename Orientation = Rotation, OSIQL_REQUIRES(Are<Roll, Pitch, Yaw>::in<Orientation>)>
    void SetOrientation(Orientation &&orientation)
    {
        SetRoll(Roll{}(orientation));
        SetPitch(Pitch{}(orientation));
        SetYaw(Yaw{}(orientation));
    }

    Timestamp<const osi3::Timestamp &> GetTimestamp() const;

    template <typename H = Handle, OSIQL_REQUIRES(!is_const<H>)>
    Timestamp<osi3::Timestamp &> GetTimestamp()
    {
        assert(handle.has_timestamp());
        return *handle.mutable_timestamp();
    }

    template <typename Ratio = std::milli>
    std::chrono::duration<int64_t, Ratio> GetTime() const;

    Handle handle;
};

template <typename Ratio = std::milli>
std::chrono::duration<int64_t, Ratio> GetTime(const osi3::StatePoint &);
} // namespace osiql
