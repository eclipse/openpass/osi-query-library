/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Coordinates paired with a lane, thus expressing an unambiguous point

#include <deque>

#include "Coordinates.h"
#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Types/Common.h"
#include "OsiQueryLibrary/Types/Enum/Traversal.h"
#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Has.h"

namespace osiql {
struct Road;
template <Traversal, typename>
struct Route;
template <size_t>
struct Vector;

template <typename LaneType = const Lane, typename Coordinates = ST>
struct Point : Coordinates, Has<std::reference_wrapper<LaneType>>
{
    //! \brief Constructs a point from coordinates and a lane
    constexpr Point(Coordinates &&, LaneType &);

    //! \brief Constructs a point from coordinates and a lane
    constexpr Point(const Coordinates &, LaneType &);

    //! \brief Constructs a point from a lane and coordinates
    constexpr Point(LaneType &, Coordinates &&);

    //! \brief Constructs a point from a lane and coordinates
    constexpr Point(LaneType &, const Coordinates &);

    //! Returns the s-coordinate of this point
    //!
    //! \return double
    constexpr double GetS() const;

    //! Returns the t-coordinate of this point
    //!
    //! \return double
    constexpr double GetT() const;

    //! Returns the s- & t-coordinates corresponding to the u- & v-coordinates of this point.
    //!
    //! \return st-coordinate pair if this point is in uv-coordinates, otherwise a reference to this point
    constexpr decltype(auto) GetST() const;

    //! Returns the longitudinal distance from the start of this point's lane
    //! in driving direction to this point's s-coordinate.
    //!
    //! \return Signed distance in driving direction of this point from the start of its lane
    constexpr double GetU() const;

    //! Returns the lateral distance from the point on the centerline of this point's lane with the same
    //! s-coordinate to this point. If this point is to the right of the centerline (in road definition direction)
    //! the returned distance will be negative.
    //!
    //! \return Signed distance from the corresponding centerline point to this point
    constexpr double GetV() const;

    //! Returns the uv-coordinates matching this point's st-coordinates
    //!
    //! \return uv-coordinate pair
    constexpr decltype(auto) GetUV() const;

    //! Returns the global position of this point
    //!
    //! \return XY
    constexpr XY GetXY() const;

    // TODO: Returned route should be optional

    //! Returns a random route from this point. It will end at the given distance
    //! or sooner if there are no further connected roads.
    //!
    //! \tparam T Direction of the route relative to the driving direction of the traversed lanes
    //! \tparam RNG Nullary invocable returning a number within [0, 1). You can use World::rng
    //! \param maxDistance If negative, the random route will travel backwards
    //! (relative to the driving direction of this point's lane)
    //! \return A random route generated using the given number generator
    template <Traversal T = Traversal::Forward, typename Scope = Road, typename RNG = void>
    std::optional<Route<T, Scope>> GetRandomRoute(RNG &&, double maxDistance = MAX_SEARCH_DISTANCE) const;

    //! Returns a custom route from this point. It ends at the given distance
    //! or sooner if there are no further connected roads.
    //!
    //! \tparam T Direction of the route relative to the driving direction of the traversed lanes
    //! \tparam Selector Unary invocable receiving a const Node<O>& and returning a const Node<O>*
    //! \return A custom route created using the given selector
    template <Traversal T = Traversal::Forward, typename Scope = Road, typename Selector = void>
    std::optional<Route<T, Scope>> GetCustomRoute(Selector &&) const;

    //! Returns a route from this point to the given destination
    //! \tparam LocalPoint Coordinates associated with a lane
    //! \tparam T Forward or Backward
    //! \param destination Reachable destination where the route ends.
    //! \return
    template <Traversal T = Traversal::Forward, typename Scope = Road, typename LocalPoint = Point<const Lane, ST>>
    std::optional<Route<T, Scope>> GetRoute(const LocalPoint &destination) const;

    //! Returns the distance to the given destination or infinity if beyond the given maximum search range
    //!
    //! \tparam Enum Downstream or Upstream
    //! \param Destination double or a local point
    //! \param maxRange Search will terminate after surpassing that distance
    //! \return double
    template <Traversal = Traversal::Forward, typename Destination = double>
    double GetDistanceTo(const Destination &, double maxRange = MAX_SEARCH_DISTANCE) const;

    //! Returns the distance to the given destination or infinity if beyond the given maximum search range
    //!
    //! \param Destination double or a local point
    //! \param Enum Downstream or Upstream
    //! \param maxRange Search will terminate after surpassing that distance
    //! \return double Shortest longitudinal distance along all reference lines from this point
    //! to the earliest encountered point of the destination.
    template <typename Destination>
    double GetDistanceTo(const Destination &, Traversal, double maxRange = MAX_SEARCH_DISTANCE) const;

    //! Returns the distance to the given destination or infinity if beyond the given maximum search range
    //!
    //! \param Destination double or a local point
    //! \param Traversal Forward or Backward
    //! \param maxRange Search will terminate after surpassing that distance
    //! \return double Shortest longitudinal distance along all reference lines from this point
    //! to the earliest encountered point of the destination.
    template <typename Destination>
    double GetDistanceTo(const Destination &, double maxRange, Traversal) const;

    //! Returns the distance from the centerline of the lane in the given lane chain that is part
    //! of the same road as this point to this point's t-coordinate. If no such lane exists, NaN is returned.
    //!
    //! \return double Negative if this point is to the right of the target lane's centerline
    //! relative to the direction of its road's reference line.
    template <typename LaneChain>
    double GetDeviation(const LaneChain &) const;

    //! Returns the lateral distance from this point to the given point according to the given lane chain's centerline.
    //!
    //! \tparam OtherLaneType Lane or const Lane
    //! \param target Target point to which the obstruction is computed
    //! \param chain Chain of lanes that form a continuous centerline
    //! \return double Signed distance where left relative to the centerline is positive. Returns Nan if one of
    //! points does not lie on the given chain.
    template <typename OtherLaneType, typename Type = Coordinates, OSIQL_REQUIRES(std::is_same_v<Type, ST>)>
    double GetObstruction(const Point<OtherLaneType, ST> &target, const std::deque<const Lane *> &chain) const
    {
        const auto here{std::find_if(chain.begin(), chain.end(), Matches<Road>{*this})};
        if (here == chain.end())
        {
            return std::numeric_limits<double>::quiet_NaN();
        }
        const auto there{std::find_if(chain.begin(), chain.end(), Matches<Road>{target})};
        if (there == chain.end())
        {
            return std::numeric_limits<double>::quiet_NaN();
        }
        return (*there)->GetV(target) - (*here)->GetV(*this);
    } // TODO: Add support for UV-point as well

    template <typename Type = LaneType, typename U = Coordinates, OSIQL_REQUIRES(std::is_same_v<Type, const Lane>)>
    Point(const Point<Lane, Coordinates> &point) :
        Coordinates{point}, Has<std::reference_wrapper<const Lane>>{point.GetLane()}
    {
    }
};

template <typename LaneType, typename Coordinates, typename U>
bool operator==(const Point<LaneType, Coordinates> &, const U &);

template <typename LaneType, typename Coordinates, typename U>
bool operator!=(const Point<LaneType, Coordinates> &, const U &);

template <typename LaneType, typename Coordinates>
std::ostream &operator<<(std::ostream &, const Point<LaneType, Coordinates> &);
} // namespace osiql
