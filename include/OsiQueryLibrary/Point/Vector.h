/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Global point that supports various operators

#include <cmath>   // sqrt & math constants
#include <cstddef> // size_t

#include "OsiQueryLibrary/Types/Common.h"
#include "OsiQueryLibrary/Utility/Common.h"
#include "OsiQueryLibrary/Utility/Length.h"
#include "OsiQueryLibrary/Utility/XYZ.h"

namespace osiql {
template <size_t I>
struct Vector
{
    static constexpr size_t size = I;
};

using XY = Vector<2>;
using XYZ = Vector<3>;

//! Returns the given vector scaled down by its magnitude such that its length equals 1.
//! If the magnitude is near zero, a zero vector is returned instead.
//!
//! \tparam Type Any type representing an xy-coordinate pair or xyz-coordinate triplet.
//! \param v Instance of a point or vector
//! \param epsilon If the squared length of the input vector is smaller than this value,
//! a zero vector is returned.
//! \return constexpr Type
template <typename Type, OSIQL_REQUIRES(Are<X, Y>::in<Type>)>
constexpr Vector<Dimensions<Type>> Norm(const Type &vector, double epsilon = EPSILON)
{
    const double length{SquaredLength(vector)};
    return (length < epsilon) ? Type{} : (vector / sqrt(length));
}
} // namespace osiql
