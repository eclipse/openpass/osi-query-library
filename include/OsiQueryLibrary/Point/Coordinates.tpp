/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Coordinates.h"

#include "OsiQueryLibrary/Types/Common.h"
#include "OsiQueryLibrary/Types/Enum.h"
#include "OsiQueryLibrary/Utility/Extract.tpp"
#include "OsiQueryLibrary/Utility/Is.tpp"

namespace osiql {
constexpr ST::ST(double s, double t) : // NOLINT(bugprone-easily-swappable-parameters)
    s{s}, t{t}
{
}

template <typename Coordinates>
constexpr bool operator==(const ST &a, const Coordinates &b)
{
    return std::abs(extract<Direction::Downstream>(a) - extract<Direction::Downstream>(b)) <= EPSILON // clang-format off
        && std::abs(extract<Side::Left>(a) - extract<Side::Left>(b)) <= EPSILON; // clang-format on
}

template <typename Coordinates>
constexpr bool operator!=(const ST &a, const Coordinates &b)
{
    return !(a == b);
}

constexpr UV::UV(double u, double v) : // NOLINT(bugprone-easily-swappable-parameters)
    u{u}, v{v}
{
}

constexpr bool operator==(const UV &a, const UV &b)
{
    return std::abs(b.u - a.u) <= EPSILON // clang-format off
        && std::abs(b.v - a.v) <= EPSILON; // clang-format on
}

constexpr bool operator!=(const UV &a, const UV &b)
{
    return !(a == b);
}
} // namespace osiql
