/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Vector.h"

#include "OsiQueryLibrary/Utility/Length.tpp"
#include "OsiQueryLibrary/Utility/XYZ.h"
#include "XY.h"
#include "XYZ.h"

namespace osiql {
namespace math {
template <typename A, typename B, size_t... Is>
constexpr bool Equals(const A &a, const B &b, std::index_sequence<Is...>)
{
    return ((std::abs(At<Is>(a) - At<Is>(b)) < EPSILON) && ...);
}

template <typename A, typename B, size_t... Is>
constexpr Vector<sizeof...(Is)> Add(const A &a, const B &b, std::index_sequence<Is...>)
{
    return {(At<Is>(a) + At<Is>(b))...};
}

template <typename A, typename B, size_t... Is>
constexpr Vector<sizeof...(Is)> Subtract(const A &a, const B &b, std::index_sequence<Is...>)
{
    return {(At<Is>(a) - At<Is>(b))...};
}

template <typename Point, size_t... Is>
constexpr Vector<sizeof...(Is)> Invert(const Point &point, std::index_sequence<Is...>)
{
    return {-At<Is>(point)...};
}

template <typename Point, typename Type, size_t... Is>
constexpr Vector<sizeof...(Is)> MultiplyPointWithScalar(const Point &point, const Type &factor, std::index_sequence<Is...>)
{
    return {(At<Is>(point) * factor)...};
}

template <typename Point, typename Type, size_t... Is>
constexpr Vector<sizeof...(Is)> Divide(const Point &point, const Type &divisor, std::index_sequence<Is...>)
{
    return {(At<Is>(point) / divisor)...};
}
} // namespace math

template <typename A, typename B, OSIQL_REQUIRES(Are<X, Y>::in<A, B>)>
constexpr bool operator==(const A &a, const B &b)
{
    return math::Equals(a, b, std::make_index_sequence<std::min(Dimensions<A>, Dimensions<B>)>{});
}

template <typename A, typename B, OSIQL_REQUIRES(Are<X, Y>::in<A, B>)>
constexpr bool operator!=(const A &a, const B &b)
{
    return !(a == b);
}

template <typename A, typename B, OSIQL_REQUIRES(Are<X, Y>::in<A, B>)>
constexpr Vector<std::max(Dimensions<A>, Dimensions<B>)> operator+(const A &a, const B &b)
{
    return math::Add(a, b, std::make_index_sequence<std::max(Dimensions<A>, Dimensions<B>)>{});
}

template <typename Type, OSIQL_REQUIRES(Are<X, Y>::in<Type>)>
constexpr Vector<Dimensions<Type>> operator-(const Type &vector)
{
    return math::Invert(vector, std::make_index_sequence<Dimensions<Type>>{});
}

template <typename A, typename B, OSIQL_REQUIRES(Are<X, Y>::in<A, B>)>
constexpr Vector<std::max(Dimensions<A>, Dimensions<B>)> operator-(const A &a, const B &b)
{
    return math::Subtract(a, b, std::make_index_sequence<std::max(Dimensions<A>, Dimensions<B>)>{});
}

template <typename Type, OSIQL_REQUIRES(Are<X, Y>::in<Type>)>
constexpr Vector<Dimensions<Type>> operator*(const Type &vector, double factor)
{
    return math::MultiplyPointWithScalar(vector, factor, std::make_index_sequence<Dimensions<Type>>{});
}

template <typename Type, OSIQL_REQUIRES(Are<X, Y>::in<Type>)>
constexpr Vector<Dimensions<Type>> operator*(double factor, const Type &vector)
{
    return vector * factor;
}

template <typename Type, OSIQL_REQUIRES(Are<X, Y>::in<Type>)>
constexpr Vector<Dimensions<Type>> operator/(const Type &vector, double divisor)
{
    return math::Divide(vector, divisor, std::make_index_sequence<Dimensions<Type>>{});
}
} // namespace osiql
