/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Point.h"

#include "Coordinates.tpp"
#include "OsiQueryLibrary/Routing/Route.h"
#include "OsiQueryLibrary/Street/Lane.tpp"
#include "OsiQueryLibrary/Street/Road.h"
#include "OsiQueryLibrary/Trait/Id.h"
#include "OsiQueryLibrary/Types/Enum/Traversal.tpp"
#include "OsiQueryLibrary/Utility/Extract.tpp"
#include "OsiQueryLibrary/Utility/Length.tpp"

namespace osiql {
template <typename LaneType, typename Coordinates>
constexpr Point<LaneType, Coordinates>::Point(Coordinates &&coords, LaneType &lane) :
    Coordinates{std::forward<Coordinates>(coords)}, Has<std::reference_wrapper<LaneType>>{lane}
{
}

template <typename LaneType, typename Coordinates>
constexpr Point<LaneType, Coordinates>::Point(const Coordinates &coords, LaneType &lane) :
    Coordinates{coords}, Has<std::reference_wrapper<LaneType>>{lane}
{
}

template <typename LaneType, typename Coordinates>
constexpr Point<LaneType, Coordinates>::Point(LaneType &lane, Coordinates &&coords) :
    Coordinates{coords}, Has<std::reference_wrapper<LaneType>>{lane}
{
}

template <typename LaneType, typename Coordinates>
constexpr Point<LaneType, Coordinates>::Point(LaneType &lane, const Coordinates &coords) :
    Coordinates{coords}, Has<std::reference_wrapper<LaneType>>{lane}
{
}

template <typename LaneType, typename Coordinates>
constexpr XY Point<LaneType, Coordinates>::GetXY() const
{
    return this->GetRoad().GetReferenceLine().GetXY(*this);
}

template <typename LaneType, typename Coordinates>
constexpr double Point<LaneType, Coordinates>::GetS() const
{
    if constexpr (Is<S>::in<Coordinates>)
    {
        return this->s;
    }
    else
    {
        return this->GetLane().GetS(*this);
    }
}

template <typename LaneType, typename Coordinates>
constexpr double Point<LaneType, Coordinates>::GetT() const
{
    if constexpr (Is<T>::in<Coordinates>)
    {
        return this->t;
    }
    else
    {
        return this->GetST().t;
    }
}

template <typename LaneType, typename Coordinates>
constexpr decltype(auto) Point<LaneType, Coordinates>::GetST() const
{
    if constexpr (Are<S, T>::in<Coordinates>)
    {
        return (*this);
    }
    else
    {
        return this->GetLane().GetST(*this);
    }
}

template <typename LaneType, typename Coordinates>
constexpr double Point<LaneType, Coordinates>::GetU() const
{
    if constexpr (Is<U>::in<Coordinates>)
    {
        return this->u;
    }
    else
    {
        return this->GetLane().GetDistanceFromStart(*this);
    }
}

template <typename LaneType, typename Coordinates>
constexpr double Point<LaneType, Coordinates>::GetV() const
{
    if constexpr (Is<V>::in<Coordinates>)
    {
        return this->v;
    }
    else
    {
        return this->GetLane().GetV(*this);
    }
}

template <typename LaneType, typename Coordinates>
constexpr decltype(auto) Point<LaneType, Coordinates>::GetUV() const
{
    if constexpr (Are<U, V>::in<Coordinates>)
    {
        return (*this);
    }
    else
    {
        return this->GetLane().GetUV(*this);
    }
}

template <typename LaneType, typename Coordinates>
template <Traversal T, typename Scope, typename LocalPoint>
std::optional<Route<T, Scope>> Point<LaneType, Coordinates>::GetRoute(const LocalPoint &destination) const
{
    const auto root{Node<T, Scope>::Create(*this)};
    if (const auto *leaf{root->FindClosestNode(destination)}; leaf)
    {
        return Route<T, Scope>{*this, destination, leaf->GetPathFromDepth(0)};
    }
    return std::nullopt;
}

template <typename LaneType, typename Coordinates>
template <Traversal T, typename Scope, typename RNG>
std::optional<Route<T, Scope>> Point<LaneType, Coordinates>::GetRandomRoute(RNG &&rng, double maxDistance) const
{
    const auto &self{*static_cast<const Point<LaneType, Coordinates> *>(this)};
    auto path{Node<T, Scope>::Create(self)->GetRandomPath(std::forward<RNG>(rng), maxDistance)};
    const osiql::Point<const Lane> destination{ST{path.back()->GetClampedS(maxDistance), std::numeric_limits<double>::quiet_NaN()}, path.back()->GetLane()};
    return Route<T, Scope>{self, destination, std::move(path)};
}

template <typename LaneType, typename Coordinates>
template <Traversal T, typename Scope, typename Selector>
std::optional<Route<T, Scope>> Point<LaneType, Coordinates>::GetCustomRoute(Selector &&selector) const
{
    const auto &self{*static_cast<const Point<LaneType, Coordinates> *>(this)};
    auto path{Node<T, Scope>::Create(self)->GetPath(std::forward<Selector>(selector))};
    const osiql::Point<const Lane> destination{ST{path.back()->GetS(path.back()->distance + path.back()->GetLength()), std::numeric_limits<double>::quiet_NaN()}, path.back()->GetLane()};
    return Route<T, Scope>{self, destination, std::move(path)};
}

template <typename LaneType, typename Coordinates>
template <Traversal T, typename Destination>
double Point<LaneType, Coordinates>::GetDistanceTo(const Destination &destination, double maxRange) const
{
    if constexpr (std::is_fundamental_v<Destination>)
    {
        std::ignore = maxRange;
        const auto &self{*static_cast<const Point<LaneType, Coordinates> *>(this)};
        return Difference(self, destination, this->GetLane().GetDirection(T));
    }
    else
    {
        const auto &self{*static_cast<const Point<LaneType, Coordinates> *>(this)};
        const auto root{Node<T>::Create(self)};
        const Node<T> *goal{root->FindClosestNode(destination)};
        return goal ? goal->GetDistance(destination) : std::numeric_limits<double>::max();
    }
}

template <typename LaneType, typename Coordinates>
template <typename Destination>
double Point<LaneType, Coordinates>::GetDistanceTo(const Destination &destination, Traversal traversal, double maxRange) const
{
    return IsInverse(traversal) ? GetDistanceTo<!Default<Traversal>>(destination, maxRange) //
                                : GetDistanceTo<Default<Traversal>>(destination, maxRange);
}

template <typename LaneType, typename Coordinates>
template <typename Destination>
double Point<LaneType, Coordinates>::GetDistanceTo(const Destination &destination, double maxRange, Traversal traversal) const
{
    return GetDistanceTo(destination, traversal, maxRange);
}

template <typename LaneType, typename Coordinates>
template <typename LaneChain>
double Point<LaneType, Coordinates>::GetDeviation(const LaneChain &chain) const
{
    const auto &self{*static_cast<const Point<LaneType, Coordinates> *>(this)};
    if (const auto it{std::find_if(chain.begin(), chain.end(), Matches<Road>(self))}; it != chain.end())
    {
        return (*it)->GetV(self);
    }
    return std::numeric_limits<double>::quiet_NaN();
}

template <typename LaneType, typename Coordinates, typename OtherPoint>
bool operator==(const Point<LaneType, Coordinates> &a, const OtherPoint &b)
{
    if constexpr (std::is_same_v<OtherPoint, ST>)
    {
        return b == a;
    }
    else
    {
        return get<Road>(a) == get<Road>(b) && //
               get<S>(a) == get<S>(b) &&       //
               get<T>(a) == get<T>(b);
    }
}

template <typename Type, typename Coordinates, typename U>
bool operator!=(const Point<Type, Coordinates> &a, const U &b)
{
    return !(a == b);
}

template <typename Type, typename Coordinates>
std::ostream &operator<<(std::ostream &os, const Point<Type, Coordinates> &point)
{
    return os << "[Lane " << get<Lane>(point).GetId() << " (" << get<Lane>(point).index << "), " << static_cast<const Coordinates &>(point) << ']';
}
} // namespace osiql
