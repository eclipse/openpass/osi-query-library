/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Derived point that additionally stores an angle to the entity it is assigned to.

#include <iostream>

#include "OsiQueryLibrary/Utility/Common.h"

namespace osiql {
struct UV;
struct Lane;
template <typename, typename>
struct Point;
struct ST;

//! \brief A Pose is an instance of Coordinates that additionally stores an angle relative to the entity it is assigned to.
template <typename Coordinates = ST>
struct Pose : Coordinates
{
    //! Constructs a pose from a point and an angle
    //!
    //! \param Coordinates
    //! \param angle
    constexpr Pose(const Coordinates &, double angle);

    //! Constructs a pose from another pose and supplementary information needed to
    //! to construct this pose's base class
    //!
    //! \param otherPose Pose with a base that can partially construct this pose's base
    //! \param arg Argument that completes the construction of this pose's base alongside the given pose
    template <typename Type, typename Arg, OSIQL_REQUIRES(std::is_constructible_v<Coordinates, Type, Arg>)>
    constexpr Pose(const Pose<Type> &otherPose, Arg &&arg) :
        Coordinates{otherPose, std::forward<Arg>(arg)}, angle{otherPose.angle}
    {
    }

    //! \brief counter-clockwise ascending angle in radians within [-π, π]
    double angle;
};

using RoadPose [[deprecated]] = Pose<Point<const Lane, ST>>;

using LanePose [[deprecated]] = Pose<Point<const Lane, UV>>;

template <typename Type>
std::ostream &operator<<(std::ostream &, const Pose<Type> &);
} // namespace osiql
