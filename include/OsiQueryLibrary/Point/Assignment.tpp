/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Assignment.h"

#include "OsiQueryLibrary/Component/Identifiable.tpp"
#include "OsiQueryLibrary/Point/Coordinates.h"

namespace osiql {
template <typename Type>
Assignment<Type>::Assignment(Type &entity, const osi3::LogicalLaneAssignment &handle) :
    Wrapper{handle}, Has<std::reference_wrapper<Type>>{entity}
{
}

template <typename Type>
double Assignment<Type>::GetS() const
{
    return GetHandle().s_position();
}

template <typename Type>
double Assignment<Type>::GetT() const
{
    return GetHandle().t_position();
}

template <typename Type>
double Assignment<Type>::GetLocalYaw() const
{
    return GetHandle().has_angle_to_lane() ? GetHandle().angle_to_lane() : .0;
}

template <typename A, typename B>
bool operator<(const Assignment<A> &lhs, const Assignment<B> &rhs)
{
    // Can't use std::tie because some getters return values
    if (lhs.template Get<A>().GetId() != rhs.template Get<B>().GetId())
    {
        return lhs.template Get<A>().GetId() < rhs.template Get<B>().GetId();
    }
    if (get<S>(lhs) != get<S>(rhs))
    {
        return get<S>(lhs) < get<S>(rhs);
    }
    if (get<T>(lhs) != get<T>(rhs))
    {
        return get<T>(lhs) < get<T>(rhs);
    }
    return false;
}

template <typename Type>
std::ostream &operator<<(std::ostream &os, const Assignment<Type> &a)
{
    return os << "[(" << get<S>(a) << ", " << get<T>(a) << "), " << a.template Get<Type>() << ']';
}
} // namespace osiql
