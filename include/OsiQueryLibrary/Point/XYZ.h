/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Three-dimensional global point that additionally supports construction from OSI point types

#include <iostream>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/register/point.hpp>

#include "OsiQueryLibrary/Utility/XYZ.h"
#include "Vector.h"

namespace osiql {
template <>
struct Vector<3>
{
    static constexpr size_t size = 3;

    //! Constructs a XYZ from its components
    //!
    //! \param x
    //! \param y
    //! \param z
    constexpr Vector(double x = 0.0, double y = 0.0, double z = 0.0) :
        x{x}, y{y}, z{z}
    {
    }

    //! Constructs a XYZ from a 2d point and a z-component
    //!
    //! \param point
    //! \param z
    template <typename Type, OSIQL_REQUIRES(Are<X, Y, Not<Z>>::in<Type>)>
    constexpr Vector(const Type &xy, double z) :
        x{X{}(xy)}, y{Y{}(xy)}, z{z}
    {
    }

    //! Constructs a XYZ from a 3d point
    //!
    //! \param point
    template <typename Type, OSIQL_REQUIRES(Are<X, Y>::in<Type>)>
    explicit constexpr Vector(const Type &xyz) :
        x{X{}(xyz)}, y{Y{}(xyz)}, z{Z{}(xyz)}
    {
    }

    //! Returns whether this vector has a non-zero component
    //!
    //! \return bool
    constexpr bool HasLength() const noexcept;

    //! Adds the coordinate components of the right-hand side to those of the left-hand side
    //!
    //! \tparam Type Type with an x-, y- & z-component
    template <typename Type>
    constexpr void operator+=(const Type &) noexcept;

    //! Subtracts the coordinate components of the right-hand side from those of the left-hand side
    //!
    //! \tparam Type Type with an x-, y- & z-component
    template <typename Type>
    constexpr void operator-=(const Type &) noexcept;

    //! Multiplies each component of the left-hand side by the given factor
    //!
    //! \param factor
    constexpr void operator*=(double factor) noexcept;

    //! Divides each component of the left-hand side by the given divisor
    //!
    //! \param scale
    constexpr void operator/=(double divisor);

    //! Computes the dot product between this point and the given one, which is the chain product
    //! of their Euclidean magnitudes and the cosine of the angle between them.
    //!
    //! \return double
    template <typename Type>
    constexpr double Dot(const Type &) const noexcept;

    //! Computes the cross product between this point and the given one.
    //!
    //! \return XYZ
    template <typename Type>
    constexpr XYZ Cross(const Type &) const noexcept;

    //! Returns the squared length of this vector.
    //!
    //! \return double
    constexpr double SquaredLength() const noexcept;

    //! Returns the length of this vector.
    //!
    //! \return double
    double Length() const;

    //! Returns the component-wise multiplication of this vector with a given one
    //!
    //! \tparam Type Type with an x-, y- & z-component
    //! \return XYZ
    template <typename Type>
    constexpr XYZ Times(const Type &) const;

    double x;
    double y;
    double z;
};

void PrintTo(const XYZ &point, std::ostream *os);

template <typename Type, OSIQL_REQUIRES(Dimensions<Type> == 3)>
std::ostream &operator<<(std::ostream &os, const Type &xyz)
{
    return os << '(' << X{}(xyz) << ", " << Y{}(xyz) << ", " << Z{}(xyz) << ')';
}
} // namespace osiql

// Makes XYZ usable as a boost point type
BOOST_GEOMETRY_REGISTER_POINT_3D(osiql::XYZ, double, cs::cartesian, x, y, z)
