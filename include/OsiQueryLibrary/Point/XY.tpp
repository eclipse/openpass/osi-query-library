/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "XY.h"

#include "Coordinates.tpp"
#include "OsiQueryLibrary/Utility/XYZ.h"
#include "Vector.tpp"

namespace osiql {
constexpr XY::Vector(double x, double y) : // NOLINT(bugprone-easily-swappable-parameters)
    x{x}, y{y}
{
}

constexpr bool XY::HasLength() const noexcept
{
    return (x != 0.0) || (y != 0.0);
}

template <typename Type>
constexpr void XY::operator+=(const Type &vector) noexcept
{
    x += X{}(vector);
    y += Y{}(vector);
}

template <typename Type>
constexpr void XY::operator-=(const Type &vector) noexcept
{
    x -= X{}(vector);
    y -= Y{}(vector);
}

constexpr void XY::operator*=(double factor) noexcept
{
    x *= factor;
    y *= factor;
}

constexpr void XY::operator/=(double divisor)
{
    assert(divisor != 0.0);
    x /= divisor;
    y /= divisor;
}

template <typename Type>
constexpr double XY::Dot(const Type &value) const noexcept
{
    return x * X{}(value) + y * Y{}(value);
}

constexpr double XY::SquaredLength() const noexcept
{
    return this->Dot(*this);
}

template <typename Type>
constexpr double XY::Cross(const Type &vector) const noexcept
{
    return x * Y{}(vector)-y * X{}(vector);
}

template <typename Start, typename End>
XY XY::Projection(const Start &a, const End &b) const
{
    const XY ab{as<XY>(b) - as<XY>(a)};
    return as<XY>(a) + (ab.Dot(as<XY>(*this) - as<XY>(a)) * ab / ab.SquaredLength());
}

template <typename Type>
constexpr XY XY::Times(const Type &vector) const
{
    return {x * X{}(vector), y * Y{}(vector)};
}

template <typename Start, typename End>
Side XY::GetSide(const Start &a, const End &b) const
{
    const double determinant{(as<XY>(b) - as<XY>(a)).Cross(as<XY>(*this) - as<XY>(a))};
    if (determinant > 0.0)
    {
        return Side::Left;
    }
    if (determinant < 0.0)
    {
        return Side::Right;
    }
    return Side::None;
}

template <typename A, typename B>
XY XY::GetPathTo(const A &a, const B &b, double epsilon) const
{
    static_assert(Are<X, Y>::in<A, B>);
    const XY ab{X{}(b)-X{}(a), Y{}(b)-Y{}(a)};
    const XY ac{x - X{}(a), y - Y{}(a)};
    if (const double length{ab.SquaredLength()}; length > epsilon)
    {
        const double ratio{std::max(0.0, std::min(1.0, ac.Dot(ab) / length))};
        const XY projection{a * (1.0 - ratio) + b * ratio};
        return *this - projection;
    }
    return ac;
}

template <typename It>
It XY::GetEndOfClosestEdge(It it, It pastLast, double *distance) const
{
    // Note: C++20 ranges would be nice here
    if (it == pastLast)
    {
        return pastLast;
    }
    ++it;
    It result{it};
    double min{std::numeric_limits<double>::max()};
    while (it != pastLast)
    {
        const double tempDistance{this->GetPathTo(*std::prev(it), *it).SquaredLength()};
        if (tempDistance < min)
        {
            min = tempDistance;
            result = it;
        }
        ++it;
    }
    if (distance)
    {
        *distance = this->GetSide(*std::prev(result), *result) == Side::Left ? -std::sqrt(min) : std::sqrt(min);
    }
    return result;
}

template <typename It>
double XY::GetSignedDistanceToChain(It first, It pastLast) const
{
    if (first == pastLast)
    {
        throw std::runtime_error("XY::GetSignedDistanceToChain: Chain of points may not be empty");
    }
    double result{std::numeric_limits<double>::quiet_NaN()};
    GetEndOfClosestEdge(first, pastLast, &result);
    return result;
}

template <typename A, typename B>
ST XY::GetST(const A &a, const B &b) const
{
    // Project the point onto the line through start and end
    // t is the distance of the point to its projection
    const XY path{static_cast<XY>(b) - static_cast<XY>(a)};
    if (!path.HasLength())
    {
        throw std::runtime_error("Unable to return coordinates relative to a line segment of length 0");
    }
    const XY projection{Projection(a, b)};
    double t{GetSide(a, b) == Side::Right ? -(*this - projection).Length() : (*this - projection).Length()};
    // Calculate the s-coordinate
    const double length{extract<Direction::Downstream>(b) - extract<Direction::Downstream>(a)};
    double ratio{(projection - a).Length() / path.Length()};
    // Flip sign of ratio if the projection begins before the line segment
    if (X{}(a) != X{}(b))
    {
        if ((projection.x < X{}(a)) == (X{}(a) < X{}(b)))
        {
            ratio = -ratio;
        }
    }
    else if ((projection.y < Y{}(a)) == (Y{}(a) < Y{}(b)))
    {
        ratio = -ratio;
    }
    const double s{extract<Direction::Downstream>(a) + ratio * length};
    // Offset the t-coordinate
    if (ratio >= 1.0)
    {
        t += extract<Side::Left>(b);
    }
    else if (ratio <= 0.0)
    {
        t += extract<Side::Left>(a);
    }
    else
    {
        t += extract<Side::Left>(a) * (1.0 - ratio) + extract<Side::Left>(b) * ratio;
    }
    return {s, t};
}

template <typename Shape>
constexpr bool XY::IsWithin(const Shape &polygon) const
{
    return IsWithin(polygon.begin(), polygon.end());
}

namespace detail {
struct Region
{ // NOLINTBEGIN(readability-identifier-naming)
    static constexpr int None{0};
    static constexpr int Above{1 << 0};
    static constexpr int Below{1 << 1};
    static constexpr int Left{1 << 2};
    static constexpr int Right{1 << 3};
    static constexpr int NotAbove{Below | Left | Right};
    static constexpr int NotBelow{Above | Left | Right};
    static constexpr int NotLeft{Above | Below | Right};
    static constexpr int NotRight{Above | Below | Left};
    static constexpr int LeftAndRight{Left | Right};
    static constexpr int AboveAndBelow{Above | Below};
    static constexpr int TopLeft{Above | Left};
    static constexpr int TopRight{Above | Right};
    static constexpr int BottomLeft{Below | Left};
    static constexpr int BottomRight{Below | Right};
    static constexpr int All{TopLeft | BottomRight};
}; // NOLINTEND(readability-identifier-naming)
} // namespace detail

template <typename It>
constexpr bool XY::IsWithin(It first, It pastLast) const
{
    if (std::distance(first, pastLast) < 3)
    {
        return false;
    }
    const auto prev{std::prev(pastLast)};
    if (y - Y{}(*prev) > EPSILON)
    {
        return IsWithinHelper<detail::Region::Below>(first, pastLast);
    }
    if (Y{}(*prev) - y > EPSILON)
    {
        return IsWithinHelper<detail::Region::Above>(first, pastLast);
    }
    return IsWithinHelper<detail::Region::None>(first, pastLast);
}

template <int PreviousHemisphere, typename It>
constexpr bool XY::IsWithinHelper(It first, It pastLast) const
{
    const auto prev{std::prev(pastLast)};
    if (x - X{}(*prev) > EPSILON)
    {
        return IsWithin<PreviousHemisphere | detail::Region::Left>(first, pastLast);
    }
    if (X{}(*prev) - x > EPSILON)
    {
        return IsWithin<PreviousHemisphere | detail::Region::Right>(first, pastLast);
    }
    return IsWithin<PreviousHemisphere>(first, pastLast);
}

template <int PreviousRegion, typename It>
constexpr bool XY::IsWithin(It first, It pastLast) const
{
    if constexpr (PreviousRegion & detail::Region::AboveAndBelow)
    {
        return IsWithin<false, PreviousRegion, PreviousRegion & detail::Region::AboveAndBelow>(std::prev(pastLast), first, pastLast);
    }
    else
    {
        for (auto it{std::make_reverse_iterator(pastLast)}; it != std::make_reverse_iterator(first); ++it)
        {
            if (y - Y{}(*it) > EPSILON)
            {
                return IsWithin<false, PreviousRegion, detail::Region::Below>(std::prev(pastLast), first, pastLast);
            }
            if (Y{}(*it) - y > EPSILON)
            {
                return IsWithin<false, PreviousRegion, detail::Region::Above>(std::prev(pastLast), first, pastLast);
            }
        }
        // The input is a straight horizontal line of points (possibly out of order)
        return false;
    }
}

template <bool State, int PreviousRegion, int CurrentHemisphere, typename It>
constexpr bool XY::IsWithin(It prev, It current, It pastLast) const
{
    if (current == pastLast)
    {
        return State;
    }
    if (y - Y{}(*current) > EPSILON)
    {
        return IsWithinHelper<State, PreviousRegion, CurrentHemisphere, detail::Region::Below>(prev, current, pastLast);
    }
    if (Y{}(*current) - y > EPSILON)
    {
        return IsWithinHelper<State, PreviousRegion, CurrentHemisphere, detail::Region::Above>(prev, current, pastLast);
    }
    return IsWithinHelper<State, PreviousRegion, CurrentHemisphere, detail::Region::None>(prev, current, pastLast);
}

template <bool State, int PreviousRegion, int CurrentHemisphere, int NextHemisphere, typename It>
constexpr bool XY::IsWithinHelper(It prev, It current, It pastLast) const
{
    if (x - X{}(*current) > EPSILON)
    {
        return IsWithin<State, PreviousRegion, CurrentHemisphere, NextHemisphere | detail::Region::Left>(prev, current, pastLast);
    }
    if (X{}(*current) - x > EPSILON)
    {
        return IsWithin<State, PreviousRegion, CurrentHemisphere, NextHemisphere | detail::Region::Right>(prev, current, pastLast);
    }
    return IsWithin<State, PreviousRegion, CurrentHemisphere, NextHemisphere>(prev, current, pastLast);
}

template <bool State, int CurrentHemisphere, int NextRegion, typename PointA, typename PointB, typename PointC, typename It>
constexpr bool XY::IsWithin(const PointA &a, const PointB &b, const PointC &c, It current, It pastLast) const
{
    const XY ab{b - a};
    const XY ac{c - a};
    const double d{ac.x * ab.y}; // NOLINT(readability-identifier-length)
    const double e{ab.x * ac.y}; // NOLINT(readability-identifier-length)
    if (d <= e)
    {
        if (e - d < EPSILON)
        {
            return true;
        }
        return IsWithin<!State, NextRegion, ~CurrentHemisphere & detail::Region::AboveAndBelow>(current, std::next(current), pastLast);
    }
    return IsWithin<State, NextRegion, ~CurrentHemisphere & detail::Region::AboveAndBelow>(current, std::next(current), pastLast);
}

template <bool State, int PreviousRegion, int CurrentHemisphere, int NextRegion, typename It>
constexpr bool XY::IsWithin([[maybe_unused]] It prev,[[maybe_unused]] It current,[[maybe_unused]] It pastLast) const
{
    // clang-format off
    if constexpr ((NextRegion == detail::Region::None)
         || ((PreviousRegion | NextRegion) == detail::Region::LeftAndRight)
         || ((PreviousRegion | NextRegion) == detail::Region::AboveAndBelow))
    { // clang-format on
        return true;
    }
    else if constexpr ((PreviousRegion | NextRegion) == detail::Region::NotRight)
    {
        return IsWithin<!State, NextRegion, ~CurrentHemisphere & detail::Region::AboveAndBelow>(current, std::next(current), pastLast);
    }
    else if constexpr ((PreviousRegion | NextRegion) == detail::Region::All)
    {
        if constexpr (PreviousRegion == detail::Region::TopLeft)
        {
            return IsWithin<State, CurrentHemisphere, NextRegion>(*prev, *current, *this, current, pastLast);
        }
        else if constexpr (PreviousRegion == detail::Region::TopRight)
        {
            return IsWithin<State, CurrentHemisphere, NextRegion>(*current, *this, *prev, current, pastLast);
        }
        else if constexpr (PreviousRegion == detail::Region::BottomLeft)
        {
            return IsWithin<State, CurrentHemisphere, NextRegion>(*prev, *this, *current, current, pastLast);
        }
        else // BottomRight to TopLeft
        {
            return IsWithin<State, CurrentHemisphere, NextRegion>(*current, *prev, *this, current, pastLast);
        }
    }
    else if constexpr ((CurrentHemisphere | (NextRegion & detail::Region::AboveAndBelow)) == detail::Region::AboveAndBelow)
    {
        return IsWithin < PreviousRegion == detail::Region::Left ? !State : State, NextRegion, NextRegion & detail::Region::AboveAndBelow > (current, std::next(current), pastLast);
    }
    else
    {
        return IsWithin<State, NextRegion, CurrentHemisphere>(current, std::next(current), pastLast);
    }
}

template <Winding W>
constexpr XY XY::GetPerpendicular() const
{
    if constexpr (W == Winding::Clockwise)
    {
        return {y, -x};
    }
    else
    {
        return {-y, x};
    }
}

template <Winding W>
constexpr XY GetPerpendicular(const XY &point)
{
    return point.template GetPerpendicular<W>();
}

namespace detail {
constexpr std::array<std::string_view, Size<Winding>> windingToString{
    "Clockwise", "Counter-clockwise" //
};
} // namespace detail
} // namespace osiql
