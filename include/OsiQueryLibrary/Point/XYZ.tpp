/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "XYZ.h"

#include "OsiQueryLibrary/Utility/Length.tpp"
#include "OsiQueryLibrary/Utility/XYZ.h"
#include "Vector.tpp"

namespace osiql {
constexpr bool XYZ::HasLength() const noexcept
{
    return (x != 0.0) || (y != 0.0) || (z != 0.0);
}

template <typename Type>
constexpr void XYZ::operator+=(const Type &vector) noexcept
{
    x += X{}(vector);
    y += Y{}(vector);
    z += Z{}(vector);
}

template <typename Type>
constexpr void XYZ::operator-=(const Type &vector) noexcept
{
    x -= X{}(vector);
    y -= Y{}(vector);
    z -= Z{}(vector);
}

constexpr void XYZ::operator*=(double factor) noexcept
{
    x *= factor;
    y *= factor;
    z *= factor;
}

constexpr void XYZ::operator/=(double divisor)
{
    assert(divisor != 0.0);
    x /= divisor;
    y /= divisor;
    z /= divisor;
}

template <typename Type>
constexpr double XYZ::Dot(const Type &value) const noexcept
{
    return x * X{}(value) + y * Y{}(value) + z * Z{}(value);
}

template <typename Type>
constexpr XYZ XYZ::Cross(const Type &value) const noexcept
{
    return {
        y * Z{}(value)-z * Y{}(value),
        z * X{}(value)-x * Z{}(value),
        x * Y{}(value)-y * X{}(value) //
    };
}

template <typename Type>
constexpr XYZ XYZ::Times(const Type &value) const
{
    return {x * X{}(value), y * Y{}(value), z * Z{}(value)};
}

constexpr double XYZ::SquaredLength() const noexcept
{
    return this->Dot(*this);
}
} // namespace osiql
