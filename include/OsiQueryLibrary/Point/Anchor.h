/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "XY.h"

namespace osiql {
//! \brief Pre-defined global x,y-coordinate point used by Locatable::GetXY
//! to receive the position of certain points relative to the locatable's dimensions.
struct Anchor
{
    static constexpr XY Center{0.0, 0.0};          //!< The center of an object
    static constexpr XY Front{0.5, 0.0};           //!< The middle of the front bumper of a vehicle or equivalent for other objects
    static constexpr XY Left{0.0, 0.5};            //!< The middle of the left side of an object
    static constexpr XY Right{-Left};              //!< The middle of the right side of an object
    static constexpr XY Rear{-Front};              //!< The middle of the rear bumper of a vehicle or equivalent for other objects
    static constexpr XY FrontLeft{Front + Left};   //!< The left end of the front bumper of a vehicle or equivalent for other objects
    static constexpr XY FrontRight{Front + Right}; //!< The right end of the front bumper of a vehicle or equivalent for other objects
    static constexpr XY RearLeft{Rear + Left};     //!< The left end of the rear bumper of a vehicle or equivalent for other objects
    static constexpr XY RearRight{Rear + Right};   //!< The right end of the rear bumper of a vehicle or equivalent for other objects

    Anchor() = delete;
};
} // namespace osiql
