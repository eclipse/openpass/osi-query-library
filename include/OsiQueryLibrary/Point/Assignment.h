/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Component/Identifiable.h"
#include "OsiQueryLibrary/Utility/Has.h"

namespace osiql {
struct Lane;
struct Road;
//! Wrapper of a osi3::LogicalLaneAssignment linked to an entity of an arbitrary type
//!
//! \tparam Type Arbitrary, but is usually either a osiql::Lane or a derived osiql::LaneAssignable
template <typename Entity>
struct Assignment : Wrapper<const osi3::LogicalLaneAssignment>, Has<std::reference_wrapper<Entity>>
{
    using Type = Entity;
    //! Constructs an assignment of an entity to a road-relative pose
    //!
    //! \param entity
    //! \param handle
    Assignment(Type &entity, const osi3::LogicalLaneAssignment &handle);

    //! Returns the s-coordinate of this logical lane assignment
    //!
    //! \return double
    double GetS() const;

    //! Returns the t-coordinate of this logical lane assignment
    //!
    //! \return double
    double GetT() const;

    //! Returns the yaw of the assignment relative to the angle of the edge of the
    //! reference line from which this assignment's point is laterally projected.
    //!
    //! \return double counter-clockwise ascending angle in radians within [-π, π]
    double GetLocalYaw() const;
};

template <typename A, typename B>
bool operator<(const Assignment<A> &, const Assignment<B> &);

template <typename Type>
std::ostream &operator<<(std::ostream &, const Assignment<Type> &);
} // namespace osiql
