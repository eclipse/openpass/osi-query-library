/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "StatePoint.h"

namespace osiql {
template <typename Handle>
constexpr StatePoint<Handle>::StatePoint(Handle &&handle) :
    handle{std::forward<Handle>(handle)}
{
}

template <typename Handle>
double StatePoint<Handle>::GetX() const
{
    return (handle.has_position() && handle.position().has_x()) ? handle.position().x() : 0.0;
}

template <typename Handle>
double StatePoint<Handle>::GetY() const
{
    return (handle.has_position() && handle.position().has_y()) ? handle.position().y() : 0.0;
}

template <typename Handle>
double StatePoint<Handle>::GetZ() const
{
    return (handle.has_position() && handle.position().has_z()) ? handle.position().z() : 0.0;
}

template <typename Handle>
constexpr const osi3::Vector3d &StatePoint<Handle>::GetPosition() const
{
    assert(handle.has_position());
    return handle.position();
}

template <typename Handle>
double StatePoint<Handle>::GetRoll() const
{
    if (handle.has_orientation() && handle.orientation().has_roll())
    {
        return handle.orientation().roll();
    }
    return 0.0;
}

template <typename Handle>
double StatePoint<Handle>::GetPitch() const
{
    if (handle.has_orientation() && handle.orientation().has_pitch())
    {
        return handle.orientation().pitch();
    }
    return 0.0;
}

template <typename Handle>
double StatePoint<Handle>::GetYaw() const
{
    if (handle.has_orientation() && handle.orientation().has_yaw())
    {
        return handle.orientation().yaw();
    }
    return 0.0;
}

template <typename Handle>
Timestamp<const osi3::Timestamp &> StatePoint<Handle>::GetTimestamp() const
{
    assert(handle.has_timestamp());
    return handle.timestamp();
}

template <typename Handle>
template <typename Ratio>
std::chrono::duration<int64_t, Ratio> StatePoint<Handle>::GetTime() const
{
    return GetTimestamp().template GetTime<Ratio>();
}

template <typename Ratio>
std::chrono::duration<int64_t, Ratio> GetTime(const osi3::StatePoint &point)
{
    return StatePoint<const osi3::StatePoint &>{point}.template GetTime<Ratio>();
}
} // namespace osiql
