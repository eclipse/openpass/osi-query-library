/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Pose.h"

namespace osiql {
template <typename Coordinates>
constexpr Pose<Coordinates>::Pose(const Coordinates &coords, double angle) :
    Coordinates{coords}, angle{angle}
{
}

template <typename Coordinates>
std::ostream &operator<<(std::ostream &os, const Pose<Coordinates> &pose)
{
    return os << "[Pose " << static_cast<const Coordinates &>(pose) << ", " << pose.angle << ']';
}
} // namespace osiql
