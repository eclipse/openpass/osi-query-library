/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Base of most osi-object wrappers, which holds the handle to said object,
//! provides a getter for its id, and supports comparison operators via that id.

#include <osi3/osi_object.pb.h>

#include "OsiQueryLibrary/Trait/Handle.h"
#include "OsiQueryLibrary/Trait/Id.h"
#include "OsiQueryLibrary/Utility/Common.h"

namespace osiql {
//!  A wrapper of an osi3 object
//!
//! \tparam Type The underlying handle, ergo said osi3 object
template <typename Type>
struct Wrapper
{
    //! \brief The OSI object that this object wraps around
    using Handle = Type;

    //! Constructs a wrapper from the given osi3 handle
    //!
    //! \param Type Underlying osi3 handle
    Wrapper(const Type &);

    //! Returns the underlying OSI object handle of this object.
    //!
    //! \return const Handle&
    const Handle &GetHandle() const;

protected:
    //! \brief Underlying OSI handle of this object
    osiql::Handle<Type> handle;

public:
    //! Returns the underlying OSI object handle of this object.
    //!
    //! \return Handle&
    template <typename _ = Type, OSIQL_REQUIRES(std::is_same_v<osiql::Handle<Type>, std::reference_wrapper<_>>)>
    Handle &GetHandle()
    {
        return handle;
    }
};

//! An Identifiable is a wrapper for osi3 objects that have a unique osi3::Identifier.
//! It offers a public handle to the underlying osi3 object
//!
//! \tparam Type
template <typename Type>
struct Identifiable : Wrapper<Type>
{
    using Wrapper<Type>::Wrapper;

    //! Returns the id of this object. The id is unique among all osi3 objects of any type
    //!
    //! \return Id
    Id GetId() const;
};
} // namespace osiql
