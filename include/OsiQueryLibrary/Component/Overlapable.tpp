/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Overlapable.h"

namespace osiql {
template <typename Instance>
template <typename Type>
const HashMap<Aggregate<Overlap<Instance> *, Type *>> &Overlapable<Instance>::GetOverlapping() const
{
    if constexpr (std::is_same_v<MovingObject, Type>)
    {
        return movingObjects;
    }
    else if constexpr (std::is_same_v<StationaryObject, Type>)
    {
        return stationaryObjects;
    }
    else
    {
        static_assert(always_false<Type>, "Type not supported. Must be MovingObject or StationaryObject");
    }
}

template <typename Instance>
template <typename Type>
HashMap<Aggregate<Overlap<Instance> *, Type *>> &Overlapable<Instance>::GetOverlapping()
{
    return const_cast<HashMap<Aggregate<Overlap<Instance> *, Type *>> &>(std::as_const(*this).template GetOverlapping<Type>());
}
} // namespace osiql
