/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Is.tpp"

namespace osiql {
template <typename Type>
struct Component
{
    template <typename Composite>
    constexpr decltype(auto) operator()(Composite &&) const;
};
} // namespace osiql

namespace osiql {
template <typename Type>
template <typename Composite>
constexpr decltype(auto) Component<Type>::operator()(Composite &&t) const
{
    if constexpr (Is<Type>::template in<Composite>)
    {
        return get<Type>(t);
    }
    else
    {
        return 0.0;
    }
}
} // namespace osiql
