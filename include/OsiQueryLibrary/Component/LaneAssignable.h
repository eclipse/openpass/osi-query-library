/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Base class for objects that store their assigned positions on or relative to specified lanes.

#include <osi3/osi_object.pb.h>
#include <osi3/osi_trafficsign.pb.h>

#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Trait/Position.h"
#include "OsiQueryLibrary/Types/Container.h"

namespace osiql {
//! A LaneAssignable is an object that can be assigned to a lane, which is either a
//! MovingObject, StationaryObject, RoadMarking, TrafficLight or TrafficSign.
//! MovingObjects store their assignments as a Pose while the other objects use Assignment<Lane>.
//!
//! \tparam Instance Base class of this type
template <typename Instance>
struct LaneAssignable : Instance
{
    using Instance::Instance;

    //! Returns the position assigned to the lane with the given id. Throws an exception if no such lane exists.
    //!
    //! \param id Id of the lane of the returned position.
    //! \return const Position<Instance::Handle>::type&
    const Position<Instance> &GetLocalPosition(Id id) const;

    //! Returns the local yaw of this object relative to that of the given lane at the object's position. If this object
    //! has no assigned position on the given lane, NaN is returned.
    //!
    //! \param lane Lane to which this object is assigned
    //! \return double counter-clockwise ascending angle in radians within [-π, π] relative to th lane's reference line
    //! in definition direction (which may not be the same as the lane's driving direction)
    double GetLocalYaw(const Lane &lane) const; // TODO: Backward

    //! Returns the internal OSI representation of this object's lane assignments.
    //!
    //! \return const Container<osi3::LogicalLaneAssignment>&
    const Container<osi3::LogicalLaneAssignment> &GetLaneAssignments() const;

    //! \brief Road positions of the center of this object. Only one position is stored per touched road.
    std::vector<Position<Instance>> positions;
};
} // namespace osiql
