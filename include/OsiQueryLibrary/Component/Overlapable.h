/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Base class of a lane/road responsible for storing static and moving objects that overlap with it

#include "OsiQueryLibrary/Object/MovingObject.hpp"
#include "OsiQueryLibrary/Object/StationaryObject.hpp"
#include "OsiQueryLibrary/Types/Container.h"
#include "OsiQueryLibrary/Types/Overlap.h"
#include "OsiQueryLibrary/Utility/Aggregate.h"

namespace osiql {
//! CRTP base class of a road or lane that stores its overlaps with static objects or moving objects
//!
//! \tparam Instance The type inheriting from this class (Lane or Road)
template <typename Instance>
struct Overlapable
{
    template <typename Type>
    const HashMap<Aggregate<Overlap<Instance> *, Type *>> &GetOverlapping() const;

    template <typename Type>
    HashMap<Aggregate<Overlap<Instance> *, Type *>> &GetOverlapping();

private:
    //! \brief Container of all moving objects touching this object,
    //! including the local bounds of their intersection
    HashMap<Aggregate<Overlap<Instance> *, MovingObject *>> movingObjects;

    //! \brief Container of all static objects touching this object,
    //! including the local bounds of their intersection
    HashMap<Aggregate<Overlap<Instance> *, StationaryObject *>> stationaryObjects;
};
} // namespace osiql
