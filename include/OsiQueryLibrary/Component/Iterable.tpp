/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Iterable.h"

#include "OsiQueryLibrary/Types/Interval.tpp"
#include "OsiQueryLibrary/Utility/Extract.tpp"

namespace osiql {
template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr ConstIterator<Type, Value> Iterable<Type, From, Enum>::begin() const
{
    if constexpr (IsInverse(Value))
    {
        return std::make_reverse_iterator(end<!Value>());
    }
    else
    {
        return static_cast<const From *>(this)->begin();
    }
}

template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr ConstIterator<Type, Value> Iterable<Type, From, Enum>::cbegin() const
{
    return begin<Value>();
}

template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr ConstIterator<Type, Value> Iterable<Type, From, Enum>::end() const
{
    if constexpr (IsInverse(Value))
    {
        return std::make_reverse_iterator(begin<!Value>());
    }
    else
    {
        return static_cast<const From *>(this)->end();
    }
}

template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr ConstIterator<Type, Value> Iterable<Type, From, Enum>::cend() const
{
    return end<Value>();
}

template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr ConstIterator<Type, !Value> Iterable<Type, From, Enum>::rbegin() const
{
    return begin<!Value>();
}

template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr ConstIterator<Type, !Value> Iterable<Type, From, Enum>::crbegin() const
{
    return rbegin<Value>();
}

template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr ConstIterator<Type, !Value> Iterable<Type, From, Enum>::rend() const
{
    return end<!Value>();
}

template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr ConstIterator<Type, !Value> Iterable<Type, From, Enum>::crend() const
{
    return rend<Value>();
}

template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr ConstIterator<Type, Value> Iterable<Type, From, Enum>::begin(double s) const
{
    auto it{end<Value>(s)};
    return it != begin<Value>() ? --it : it;
}

template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr ConstIterator<Type, Value> Iterable<Type, From, Enum>::end(double s) const
{
    return std::upper_bound(begin<Value>(), end<Value>(), s, Extract<Value>::lessEqual);
}

template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr ConstIterator<Type, !Value> Iterable<Type, From, Enum>::rbegin(double s) const
{
    return begin<!Value>(s);
}

template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr ConstIterator<Type, !Value> Iterable<Type, From, Enum>::rend(double s) const
{
    return end<!Value>(s);
}

template <typename Type, typename From, typename Enum>
template <typename OtherType>
constexpr bool Iterable<Type, From, Enum>::Contains(const OtherType &u) const
{
    return greaterEqual<Default<Enum>>(u, front()) && greaterEqual<!Default<Enum>>(u, back());
}

template <typename Type, typename From, typename Enum>
template <typename OtherType>
constexpr bool Iterable<Type, From, Enum>::Overlaps(const OtherType &u) const
{
    return less<Default<Enum>>(u, back()) && less<!Default<Enum>>(u, front());
}

template <typename Type, typename From, typename Enum>
template <Enum Value, typename Pred>
ConstIterator<Type, Value> Iterable<Type, From, Enum>::TraverseUntil(Pred &&pred) const
{
    return osiql::TraverseUntil(begin<Value>(), end<Value>(), std::forward<Pred>(pred));
}

template <typename Type, typename From, typename Enum>
template <Enum Value, typename Pred>
ConstIterator<Type, Value> Iterable<Type, From, Enum>::TraverseWhile(Pred &&pred) const
{
    return osiql::TraverseWhile(begin<Value>(), end<Value>(), std::forward<Pred>(pred));
}

template <typename Type, typename From, typename Enum>
template <Enum Value, typename Pred>
Interval<ConstIterator<Type, Value>> Iterable<Type, From, Enum>::GetMatchingRange(Pred &&pred) const
{
    return osiql::GetMatchingRange(begin<Value>(), end<Value>(), std::forward<Pred>(pred));
}

template <typename Type, typename From, typename Enum>
template <Enum Value, typename Condition, typename Terminator>
Interval<ConstIterator<Type, Value>> Iterable<Type, From, Enum>::GetMatchingRange(Condition &&condition, Terminator &&terminator) const
{
    auto first{TraverseUntil<Value>(std::forward<Condition>(condition))};
    return {first, osiql::TraverseWhile(first, end<Value>(), std::forward<Terminator>(terminator))};
}

template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr const_reference<Type> Iterable<Type, From, Enum>::front() const
{
    return *begin<Value>();
}

template <typename Type, typename From, typename Enum>
constexpr const_reference<Type> Iterable<Type, From, Enum>::front(Enum value) const
{
    return IsInverse(value) ? front<!Default<Enum>>() : front<Default<Enum>>();
}

template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr const_reference<Type> Iterable<Type, From, Enum>::back() const
{
    return *std::prev(end<Value>());
}

template <typename Type, typename From, typename Enum>
constexpr const_reference<Type> Iterable<Type, From, Enum>::back(Enum value) const
{
    return IsInverse(value) ? back<!Default<Enum>>() : back<Default<Enum>>();
}

template <typename Type, typename From, typename Enum>
constexpr size_t Iterable<Type, From, Enum>::size() const
{
    return static_cast<size_t>(std::distance(begin<Default<Enum>>(), end<Default<Enum>>()));
}

template <typename Type, typename From, typename Enum>
constexpr bool Iterable<Type, From, Enum>::empty() const
{
    return begin<Default<Enum>>() == end<Default<Enum>>();
}

template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr const_reference<Type> Iterable<Type, From, Enum>::operator[](size_t i) const
{
    return *std::next(begin<Value>(), static_cast<difference_type<ConstIterator<Type, Value>>>(i));
}

template <typename Type, typename From, typename Enum>
template <Enum Value>
constexpr const_reference<Type> Iterable<Type, From, Enum>::at(size_t i) const
{
    return *std::next(begin<Value>(), static_cast<difference_type<ConstIterator<Type, Value>>>(i));
}

template <typename Container>
constexpr size_t size(const Container &container)
{
    return static_cast<size_t>(std::distance(begin(container), end(container)));
}

template <auto Value, typename Container>
constexpr ConstIterator<Container, Value> begin(const Container &container)
{
    if constexpr (IsInverse(Value))
    {
        return container.rbegin();
    }
    else
    {
        return container.begin();
    }
}

template <auto Value, typename Container>
constexpr ConstIterator<Container, Value> end(const Container &container)
{
    if constexpr (IsInverse(Value))
    {
        return container.crend();
    }
    else
    {
        return container.cend();
    }
}

template <auto Value, typename Container>
constexpr const_reference<Container> front(const Container &container)
{
    return *begin<Value>(container);
}

template <typename Enum, typename Container>
constexpr const_reference<Container> front(Enum value, const Container &container)
{
    return IsInverse(value) ? *begin<!Default<Enum>>(container) : *begin<Default<Enum>>(container);
}

template <auto Value, typename Type>
constexpr const_reference<Type> back(const Type &container)
{
    return *std::prev(end<Value>(container));
}

template <typename Enum, typename Container>
constexpr const_reference<Container> back(Enum value, const Container &container)
{
    return IsInverse(value) ? *begin<Default<Enum>>(container) : *begin<!Default<Enum>>(container);
}

template <typename It, typename Pred>
It TraverseUntil(It first, It pastLast, Pred &&pred)
{
    return std::find_if(first, pastLast, std::forward<Pred>(pred));
}

template <typename It, typename Pred>
It TraverseWhile(It first, It pastLast, Pred &&pred)
{
    return std::find_if(first, pastLast, [&pred](const auto &t) { return !pred(t); });
}

template <typename It, typename Pred>
Interval<It> GetMatchingRange(It first, It pastLast, Pred &&pred)
{
    const auto it{TraverseUntil(first, pastLast, std::forward<Pred>(pred))};
    return {it, osiql::TraverseWhile(it, pastLast, std::forward<Pred>(pred))};
}

template <typename It, typename AsSoonAs, typename AsLongAs>
Interval<It> GetMatchingRange(It first, It pastLast, AsSoonAs &&asSoonAs, AsLongAs &&asLongAs)
{
    const auto it{TraverseUntil(first, pastLast, std::forward<AsSoonAs>(asSoonAs))};
    return {it, osiql::TraverseWhile(it, pastLast, std::forward<AsLongAs>(asLongAs))};
}
} // namespace osiql
