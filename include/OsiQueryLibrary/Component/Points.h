/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Iterable.h"
#include "OsiQueryLibrary/Point/XY.h"
#include "OsiQueryLibrary/Types/Geometry.h"
#include "OsiQueryLibrary/Utility/XYZ.h"

namespace osiql {
template <typename>
struct Pose;

//! Points are an Iterable wrapper of an ordered container of points. Inheriting classes must publicly define:
//! Type::const_iterator begin() const and Type::const_iterator end() const. Public using declarations for
//! Iterable<Type, From>::begin and end are highly recommended.
//!
//! \tparam Type Container
template <typename Type, typename From>
struct Points : Iterable<Type, From>
{
    using Iterable<Type, From>::begin;
    using Iterable<Type, From>::end;

    //! Returns an iterator to the start of the nearest edge to the given point
    //!
    //! \tparam D Downstream or Upstream
    //! \tparam GlobalPoint xy-coordinate pair
    //! \return ConstIterator<Type, D>
    template <Direction D = Direction::Downstream, typename GlobalPoint = void>
    constexpr ConstIterator<Type, D> begin(const GlobalPoint &) const;

    //! Returns an iterator to the point past the start of the nearest edge to the given point
    //!
    //! \tparam D Downstream or Upstream
    //! \tparam GlobalPoint xy-coordinate pair
    //! \return ConstIterator<Type, D>
    template <Direction D = Direction::Downstream, typename GlobalPoint = void>
    constexpr ConstIterator<Type, D> end(const GlobalPoint &) const;

    //! Returns an iterator to the end of the nearest edge. If distance is at
    //! the end of one and the start of another edge, the latter is returned.
    //!
    //! \tparam D Downstream or Upstream
    //! \param s s-coordinate
    //! \return ConstIterator<Type, D>
    template <Direction D = Direction::Downstream>
    constexpr ConstIterator<Type, D> GetNearestEdge(double s) const;
    template <Direction D = Direction::Downstream>
    constexpr Iterator<Type, D> GetNearestEdge(double s);

    //! Computes and returns the sum of Euclidean distances between all neighboring points in this point chain.
    //!
    //! \return double
    double GetLength() const;

    //! Returns the interpolated point on this chain of points with the given s-coordinate
    //!
    //! \param s s-coordinate representing the distance from the start of a reference line along that reference line
    //! \return XY xy-coordinate pair representing a global point in world coordinates
    XY GetXY(double s) const;

    //! Returns the global point of any given s- & t-coordinate relative to this Points. This implementation is independent from
    //! other points comprising a lane and thus may produce results deviating from OpenDRIVE specification.
    //! The point is interpolated perpendicular to the (possibly extended) points and is thus ambiguous near the start/end of line segments.
    //!
    //! \return XY
    XY GetXY(const ST &) const;

    //! Returns a st-coordinate representation of the given xy-coordinates localized to this chain of points.
    //! If the input contains a global angle, the angle will be localized as well as a counter-clockwise
    //! ascending angle in radians within [-π, π] from the x-axis.
    //!
    //! \return Either ST or Pose<ST>
    template <typename GlobalPoint>
    decltype(auto) Localize(const GlobalPoint &) const;

    //! Returns the interpolated t-coordinate of the point on the line with the given s-coordinate. If
    //! the s-coordinate is outside the chain of points, the t-coordinate of the nearest point is returned
    //!
    //! \param s s-coordinate representing the distance from the start of a reference line along that reference line
    //! \return double Negative if to the right of the nearest edge, otherwise positive
    double GetT(double s) const;

    //! Returns the global angle of the point set's edge at the given value in the template direction.
    //! If the value hits a corner, the angle of the latter edge in the template direction is returned.
    //!
    //! \tparam Direction Downstream or Upstream
    //! \param s s-coordinate
    //! \return double counter-clockwise ascending angle in radians within [-π, π] from the x-axis
    template <Direction = Direction::Downstream>
    double GetAngle(double s) const;

    //! Returns the global angle of the point set's edge at the given value in the given direction.
    //! If the value hits a corner, the angle of the latter edge in the given direction is returned.
    //!
    //! \param s s-coordinate
    //! \param Direction Downstream or Upstream
    //! \return double counter-clockwise ascending angle in radians within [-π, π] from the x-axis
    double GetAngle(double s, Direction) const;

    //! Treats this chain of points as though it were a smooth curve and
    //! returns the change in angle per unit of length at the given s-coordinate.
    //!
    //! \tparam Direction Downstream or Upstream
    //! \param s s-coordinate at which the curvature will be measured
    //! \param epsilon Rounding error tolerance. The result is interpolated by the change in angle
    //! of the closest vertex before and after the given s-coordinate. Any value closer to 0 than
    //! the given epsilon will be replaced by 0.
    //! \return double
    template <Direction = Direction::Downstream>
    double GetCurvature(double s, double epsilon = 0.0) const;

    //! Treats this chain of points as though it were a smooth curve and
    //! returns the change in angle per unit of length at the given s-coordinate.
    //!
    //! \param s s-coordinate at which the curvature will be measured
    //! \param Direction Downstream or Upstream
    //! \param epsilon Rounding error tolerance. The result is interpolated by the change in angle
    //! of the closest vertex before and after the given s-coordinate. Any value closer to 0 than
    //! the given epsilon will be replaced by 0.
    //! \return double
    double GetCurvature(double s, Direction, double epsilon = 0.0) const;
};

template <typename Type, typename From>
std::ostream &operator<<(std::ostream &, const Points<Type, From> &line);
} // namespace osiql
