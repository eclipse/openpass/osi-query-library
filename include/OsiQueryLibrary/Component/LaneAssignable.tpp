/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "LaneAssignable.h"

#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/Road.h"

namespace osiql {
template <typename Type>
const Position<Type> &LaneAssignable<Type>::GetLocalPosition(const Id id) const
{
    const auto it{std::find_if(positions.begin(), positions.end(), [&id](const Position<Type> &position) {
        return position.entity.GetId() == id;
    })};
    if (it == positions.end())
    {
        std::ostringstream stream;
        stream << "Object has no assigned position to a lane with the given id.\n";
        throw std::runtime_error(stream.str());
    }
    return *it;
}

template <typename Type>
double LaneAssignable<Type>::GetLocalYaw(const Lane &lane) const
{
    const auto it{std::find_if(positions.begin(), positions.end(), Matches<Road>{lane})};
    return it != positions.end() ? it->GetLocalYaw() : std::numeric_limits<double>::quiet_NaN();
}

template <typename Type>
const Container<osi3::LogicalLaneAssignment> &LaneAssignable<Type>::GetLaneAssignments() const
{
    if constexpr (std::is_same_v<typename Type::Handle, osi3::MovingObject>)
    {
        return Type::GetHandle().moving_object_classification().logical_lane_assignment();
    }
    else if constexpr (std::is_same_v<typename Type::Handle, osi3::TrafficSign>)
    {
        return Type::GetHandle().main_sign().classification().logical_lane_assignment();
    }
    else
    {
        return Type::GetHandle().classification().logical_lane_assignment();
    }
}
} // namespace osiql
