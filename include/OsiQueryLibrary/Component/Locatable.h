/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Base class for objects that have a shape and global position

#include <algorithm>
#include <limits>

#include <osi3/osi_common.pb.h>
#include <osi3/osi_object.pb.h>
#include <osi3/osi_trafficsign.pb.h>

#include "Identifiable.h"
#include "OsiQueryLibrary/Trait/Base.h"
#include "OsiQueryLibrary/Types/Bounds.h"
#include "OsiQueryLibrary/Types/Enum/Traversal.h"
#include "OsiQueryLibrary/Types/Matrix.h"
#include "OsiQueryLibrary/Types/Rotation.h"
#include "OsiQueryLibrary/Types/Shape.h"
#include "OsiQueryLibrary/Utility/Aggregate.h"

namespace osiql {
template <typename, typename>
struct Point;
template <size_t>
struct Vector;
//! A Locatable is a wrapper of an OSI object that possesses a base, which allows it to returns its
//! local shape and size, as well as its global position and rotation. Additionally, a Locatable
//! stores its global shape and bounds.
//!
//! \tparam Handle
template <typename Handle>
struct Locatable : Identifiable<Handle>, Aggregate<Shape, Bounds>
{
    //! Constructs an object from its handle and computes its global shape and bounds
    //!
    //! \param Handle Underlying object that the object is a wrapper of
    Locatable(const Handle &);

    //! Returns the global x-coordinate of this object's bounding box center or NaN if not assigned
    //!
    //! \return double
    double GetX() const;

    //! Returns the global y-coordinate of this object's bounding box center or NaN if not assigned
    //!
    //! \return double
    double GetY() const;

    //! Returns the global z-coordinate of this object's bounding box center or NaN if not assigned
    //!
    //! \return double
    double GetZ() const;

    //! Returns the global xy-coordinates of this object's bounding box center
    //!
    //! \return XY
    XY GetXY() const;

    //! Returns the global xyz-coordinates of this object's bounding box center
    //!
    //! \return XYZ
    XYZ GetXYZ() const;

    //! Returns the global xy-coordinates of the point at the given local point.
    //!
    //! \param localPoint Local object-relative coordinates. In OSI, x represents
    //! forwards in the direction the object is facing and y represents leftwards
    //! \return XY
    template <typename RelativeCoordinates = XY, OSIQL_REQUIRES(Are<X, Y>::in<RelativeCoordinates>)>
    XY GetXY(const RelativeCoordinates &localPoint) const
    {
        return GetTransformationMatrix() * localPoint;
    }

    //! Returns the global xyz-coordinates of the point at the given local point.
    //!
    //! \param localPoint Local object-relative coordinates. In OSI, x represents
    //! forwards in the direction the object is facing, y represents leftwards and z upwards
    //! \return XYZ
    template <typename RelativeCoordinates = XYZ, OSIQL_REQUIRES(Are<X, Y, Z>::in<RelativeCoordinates>)>
    XYZ GetXYZ(const RelativeCoordinates &localPoint) const
    {
        return GetTransformationMatrix() * localPoint;
    }

    //! Returns the width of this object's axis-aligned bounding box prior to any transformation.
    //!
    //! \return double
    double GetWidth() const;

    //! Returns the length of this object's axis-aligned bounding box prior to any transformation.
    //!
    //! \return double
    double GetLength() const;

    //! Returns the height of this object's axis-aligned bounding box prior to any transformation
    //! or NaN if not assigned.
    //!
    //! \return double
    double GetHeight() const;

    //! Returns a vector whose x-coordinate represents this object's assigned length
    //! and whose y-coordinate represents this object's assigned width. The returned
    //! coordinates are NaN if not assigned.
    //!
    //! \return XY
    XY GetSize() const;

    //! Returns the global roll of this object, which is its sideway tilt.
    //! In OSI, this is the rotation around the x-axis.
    //!
    //! \return double counter-clockwise ascending angle in radians within [-π, π]
    double GetRoll() const;

    //! Returns the global pitch of this object, which is its up-/downward tilt, like on an incline.
    //! In OSI, this is the rotation around the y-axis.
    //!
    //! \return double counter-clockwise ascending angle in radians within [-π, π]
    double GetPitch() const;

    //! Returns the global yaw of this object, which is its rotation on a ground surface.
    //! In OSI, this is the rotation around the z-axis.
    //!
    //! \return double counter-clockwise ascending angle in radians within [-π, π]
    double GetYaw() const;

    //! Returns the rotation of this object. Each component returns the the change
    //! in angle around that component's corresponding axis.
    //!
    //! \return Rotation
    Rotation GetRotation() const;

    //! Returns this object's global shape localized to the st-coordinate system defined by the reference
    //! line of the road of the given lane. Each of the resulting points refers to the given lane.
    //!
    //! \param Lane The lane to which this object shall be localized
    //! \return std::vector<Point<const Lane, ST>>
    std::vector<Point<const Lane, ST>> GetLocalShape(const Lane &) const;

    //! Returns this object's underlying OSI base
    //!
    //! \return Either osi3::BaseMoving or osi3::BaseStationary
    const Base<Handle> &GetBase() const;

    //! Returns a transformation matrix representing the rotation of this object.
    //!
    //! \return const Matrix<3>&
    const Matrix<3> &GetRotationMatrix() const;

    //! Returns a transformation matrix representing the translation of this object.
    //!
    //! \return const Matrix<4>&
    const Matrix<4> &GetTranslationMatrix() const;

    //! Returns the transformation matrix of this object
    //!
    //! \return const Matrix<4>&
    const Matrix<4> &GetTransformationMatrix() const;

    //! Returns the point relative to the given lane of the corner of this
    //! object furthest along said lane in the given orientation.
    //!
    //! \param lane
    //! \param Traversal Forward or Backward
    //! \return Point<const Lane, ST>
    Point<const Lane, ST> GetFurthestPoint(const Lane &, Traversal = Traversal::Forward) const;

    //! Updates the global shape and bounds of this object
    //!
    void UpdateShapeAndBounds();

protected:
    //! \brief Resets the translation matrix of this object so that
    //! it gets recomputed the next time it is requested.
    void OutdateTranslation();

    //! \brief Resets the rotation matrix of this object so that
    //! it gets recomputed the next time it is requested.
    void OutdateRotation();

private:
    mutable std::optional<Matrix<3>> rotationMatrix;
    mutable std::optional<Matrix<4>> translationMatrix;
    mutable std::optional<Matrix<4>> transformationMatrix;
};
} // namespace osiql
