/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Roads are defined in one direction but can be driven on in the opposite direction. The Iterable header
//! provides direction-based iterator abstractions to allow for uniform traversal no matter the direction.

#include "OsiQueryLibrary/Types/Enum.tpp" // IsInverse, operator!
#include "OsiQueryLibrary/Types/Interval.h"
#include "OsiQueryLibrary/Utility/Common.h"
#include "OsiQueryLibrary/Utility/Extract.h"

namespace osiql {
template <typename>
struct Interval;

template <typename Container, auto Value = Traversal::Forward>
using ConstIterator = std::conditional_t<IsInverse(Value), typename Container::const_reverse_iterator, typename Container::const_iterator>;

template <typename Container, auto Value = Traversal::Forward>
using Iterator = std::conditional_t<IsInverse(Value), typename Container::reverse_iterator, typename Container::iterator>;

//! A CRTP base class that provides accessor methods to a random-access-iterable container or span of elements
//! that can be ordered by s-coordinate. It provides osiql::Direction-based begin, end, at, front and back methods.
//! Inheriting classes must publicly define: Type::const_iterator begin() const and Type::const_iterator end() const.
//! Public using declarations for Iterable<Type, From>::begin and Iterable<Type, From>::end are highly recommended.
//!
//! \tparam Type The underlying iterable container of the derived type
//! \tparam From The type from which the actual iterable is accessed. It inherits from this Iterable class
template <typename Type, typename From, typename Enum = Direction>
struct Iterable
{
    using const_iterator = typename Type::const_iterator;
    using const_reverse_iterator = typename Type::const_reverse_iterator;
    using iterator = typename Type::iterator;
    using reverse_iterator = typename Type::reverse_iterator;

    //! Returns an iterator to the first element of this span
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \return ConstIterator<Type, Value>
    template <Enum Value>
    constexpr ConstIterator<Type, Value> begin() const;

    //! Returns an iterator to the first element of this span
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \return ConstIterator<Type, Value>
    template <Enum Value = Default<Enum>>
    constexpr ConstIterator<Type, Value> cbegin() const;

    //! Returns an iterator to the first element of this span
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \return Iterator<Type, Value>
    template <Enum Value, OSIQL_REQUIRES(!std::is_const_v<value_type<Type>>)>
    constexpr Iterator<Type, Value> begin()
    {
        if constexpr (IsInverse(Value))
        {
            return std::make_reverse_iterator(end<!Value>());
        }
        else
        {
            return static_cast<From *>(this)->begin();
        }
    }

    //! Returns an iterator past the last element of this span in the given direction
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \return ConstIterator<Type, Value>
    template <Enum Value>
    constexpr ConstIterator<Type, Value> end() const;

    //! Returns an iterator past the last element of this span in the given direction
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \return ConstIterator<Type, Value>
    template <Enum Value = Default<Enum>>
    constexpr ConstIterator<Type, Value> cend() const;

    //! Returns an iterator past the last element of this span in the given direction
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \return Iterator<Type, Value>
    template <Enum Value, OSIQL_REQUIRES(!std::is_const_v<value_type<Type>>)>
    constexpr Iterator<Type, Value> end()
    {
        if constexpr (IsInverse(Value))
        {
            return std::make_reverse_iterator(begin<!Value>());
        }
        else
        {
            return static_cast<From *>(this)->end();
        }
    }

    //! Returns an iterator to the first element of this span in the opposite of the given direction
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \return ConstIterator<Type, !Value>
    template <Enum Value = Default<Enum>>
    constexpr ConstIterator<Type, !Value> rbegin() const;

    //! Returns an iterator to the first element of this span in the opposite of the given direction
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \return ConstIterator<Type, !Value>
    template <Enum Value = Default<Enum>>
    constexpr ConstIterator<Type, !Value> crbegin() const;

    //! Returns an iterator to the first element of this span in the opposite of the given direction
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \return Iterator<Type, !Value>
    template <Enum Value = Default<Enum>, OSIQL_REQUIRES(!std::is_const_v<value_type<Type>>)>
    constexpr Iterator<Type, !Value> rbegin()
    {
        return begin<!Value>();
    }

    //! Returns an iterator past the last element of this span in the opposite of the given direction
    //!
    //! \tparam Value The opposite of the direction in which the span should be traversed
    //! \return ConstIterator<Type, !Value>
    template <Enum Value = Default<Enum>>
    constexpr ConstIterator<Type, !Value> rend() const;

    //! Returns an iterator past the last element of this span in the opposite of the given direction
    //!
    //! \tparam Value The opposite of the direction in which the span should be traversed
    //! \return ConstIterator<Type, !Value>
    template <Enum Value = Default<Enum>>
    constexpr ConstIterator<Type, !Value> crend() const;

    template <Enum Value = Default<Enum>, OSIQL_REQUIRES(!std::is_const_v<value_type<Type>>)>
    constexpr Iterator<Type, !Value> rend()
    {
        return end<!Value>();
    }

    //! Returns an iterator to the last element of this span in the given direction
    //! with a starting s-coordinate less or equal to the given value. If all elements
    //! start after the given value, an iterator to the span's first element is returned.
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \param s Value before or at which the returned iterator's element should begin
    //! \return ConstIterator<Type, Value>
    template <Enum Value = Default<Enum>>
    constexpr ConstIterator<Type, Value> begin(double s) const;

    //! Returns an iterator to the last element of this span in the given direction
    //! with a starting s-coordinate less or equal to the given value. If all elements
    //! start after the given value, an iterator to the span's first element is returned.
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \param s Value before or at which the returned iterator's element should begin
    //! \return ConstIterator<Type, Value>
    template <Enum Value = Default<Enum>>
    constexpr ConstIterator<Type, Value> cbegin(double s) const;

    //! Returns an iterator to the last element of this span in the given direction
    //! with a starting s-coordinate less or equal to the given value. If all elements
    //! start after the given value, an iterator to the span's first element is returned.
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \param s Value before or at which the returned iterator's element should begin
    //! \return Iterator<Type, Value>
    template <Enum Value = Default<Enum>, OSIQL_REQUIRES(!std::is_const_v<value_type<Type>>)>
    constexpr Iterator<Type, Value> begin(double s)
    {
        auto it{end<Value>(s)};
        return it != begin<Value>() ? --it : it;
    }

    //! Returns an iterator past the last element of this span in the given direction
    //! with a starting s-coordinate less or equal to the given value. If all elements
    //! start after the given value, an iterator to the span's first element is returned.
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \param s Value after which the returned iterator's element should begin
    //! \return
    template <Enum Value = Default<Enum>>
    constexpr ConstIterator<Type, Value> end(double s) const;

    template <Enum Value = Default<Enum>>
    constexpr ConstIterator<Type, Value> cend(double s) const;

    template <Enum Value = Default<Enum>, OSIQL_REQUIRES(!std::is_const_v<value_type<Type>>)>
    constexpr Iterator<Type, Value> end(double s)
    {
        return std::upper_bound(begin<Value>(), end<Value>(), s, Extract<Value>::lessEqual);
    }

    //! Returns an iterator to the last element of this span in the opposite of the given direction
    //! with a starting s-coordinate less or equal to the given value. If all elements
    //! start after the given value, an iterator to the span's first element is returned.
    //!
    //! \tparam Value The opposite of the direction in which the span should be traversed
    //! \param x Value before or at which the returned iterator's element should begin
    //! \return ConstIterator<!Value>
    template <Enum Value = Default<Enum>>
    constexpr ConstIterator<Type, !Value> rbegin(double s) const;

    template <Enum Value = Default<Enum>>
    constexpr ConstIterator<Type, !Value> crbegin(double s) const;

    template <Enum Value = Default<Enum>, OSIQL_REQUIRES(!std::is_const_v<value_type<Type>>)>
    constexpr Iterator<Type, !Value> rbegin(double s)
    {
        return begin<!Value>(s);
    }

    //! Returns an iterator past the last element of this span in the opposite of the given direction
    //! with a starting s-coordinate less or equal to the given value. If all elements
    //! start after the given value, an iterator to the span's first element is returned.
    //!
    //! \tparam Value The opposite of the direction in which the span should be traversed
    //! \param x Value after which the returned iterator's element should begin
    //! \return ConstIterator<!Value>
    template <Enum Value = Default<Enum>>
    constexpr ConstIterator<Type, !Value> rend(double s) const;

    template <Enum Value = Default<Enum>>
    constexpr ConstIterator<Type, !Value> crend(double s) const;

    template <Enum Value = Default<Enum>, OSIQL_REQUIRES(!std::is_const_v<value_type<Type>>)>
    constexpr Iterator<Type, !Value> rend(double s)
    {
        return const_cast<Iterator<Type, !Value>>(std::as_const(*this).rend(s));
    }

    //! Returns the first element of this span in the given direction.
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \return const_reference<Type>
    template <Enum Value = Default<Enum>>
    constexpr const_reference<Type> front() const;

    template <Enum Value = Default<Enum>, OSIQL_REQUIRES(!std::is_const_v<value_type<Type>>)>
    constexpr reference<Type> front()
    {
        return const_cast<reference<Type>>(std::as_const(*this).front());
    }

    //! Returns the first element of this span in the given direction
    //!
    //! \return const_reference<Type>
    constexpr const_reference<Type> front(Enum) const;

    template <OSIQL_REQUIRES(!std::is_const_v<value_type<Type>>)>
    constexpr reference<Type> front(Enum value)
    {
        return const_cast<reference<Type>>(std::as_const(*this).front(value));
    }

    //! Returns the last element of this span in the given direction
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \return const_reference<Type>
    template <Enum Value = Default<Enum>>
    constexpr const_reference<Type> back() const;

    template <Enum Value = Default<Enum>, OSIQL_REQUIRES(!std::is_const_v<value_type<Type>>)>
    constexpr reference<Type> back()
    {
        return const_cast<reference<Type>>(std::as_const(*this).back());
    }

    //! Returns the last element of this span in the given direction
    //!
    //! \return const_reference<Type>
    constexpr const_reference<Type> back(Enum) const;

    template <OSIQL_REQUIRES(!std::is_const_v<value_type<Type>>)>
    constexpr reference<Type> back(Enum value)
    {
        return const_cast<reference<Type>>(std::as_const(*this).back(value));
    }

    //! Returns the number of elements in this span
    //!
    //! \return size_t
    constexpr size_t size() const;

    //! Returns whether this object contains no elements
    //!
    //! \return bool
    constexpr bool empty() const;

    //! Returns the (n-1)-th element in the given direction of this span
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \param size_t Index
    //! \return Reference to element at the given index
    template <Enum Value = Default<Enum>>
    constexpr const_reference<Type> operator[](size_t) const;

    //! Returns the (n-1)-th element in the given direction of this span
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \param size_t Index
    //! \return Reference to element at the given index
    template <Enum Value = Default<Enum>, OSIQL_REQUIRES(!std::is_const_v<value_type<Type>>)>
    constexpr reference<Type> operator[](size_t i)
    {
        return const_cast<reference<Type>>(std::as_const(*this)[i]);
    }

    //! Returns the (n-1)-th element in the given direction of this span
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \param size_t Index
    //! \return Reference to element at the given index
    template <Enum Value = Default<Enum>>
    constexpr const_reference<Type> at(size_t) const;

    //! Returns the (n-1)-th element in the given direction of this span
    //!
    //! \tparam Value The direction in which the span should be traversed
    //! \param size_t Index
    //! \return Reference to element at the given index
    template <Enum Value = Default<Enum>, OSIQL_REQUIRES(!std::is_const_v<value_type<Type>>)>
    constexpr reference<Type> at(size_t i)
    {
        return const_cast<reference<Type>>(std::as_const(*this).at(i));
    }

    //! Returns whether the s-coordinate interval of the given element is a subset of this span's s-coordinate interval
    //!
    //! \tparam U Type representing an s-coordinate interval
    //! \return bool Whether this object has a starting s-coordinate not greater than the given target
    //! and an ending s-coordinate not smaller than the target
    template <typename U>
    constexpr bool Contains(const U &) const;

    //! Returns whether the intersection of this span's s-coordinate interval and the given element is not empty.
    //!
    //! \tparam U Type representing an s-coordinate interval
    //! \return bool Whether the s-coordinate interval intersection is not empty
    template <typename U>
    constexpr bool Overlaps(const U &) const;

    //! Iterates over this object's elements while calling the given predicate and returns
    //! an iterator to the first element that satisfies that predicate.
    //!
    //! \tparam Value The direction in which the range of elements is traversed
    //! \tparam Pred Unary invocable receiving an element and returning a bool
    //! \return Iterator to the first element that satisfies the predicate
    //! or the end of the range if there is none.
    template <Enum Value = Default<Enum>, typename Pred = void>
    ConstIterator<Type, Value> TraverseUntil(Pred &&) const;

    //! Iterates over this object's elements while calling the given predicate and returns
    //! an iterator to the first element that does not satisfy the predicate.
    //!
    //! \tparam Value The direction in which the range of elements is traversed
    //! \tparam Pred Unary invocable receiving an element and returning a bool
    //! \return Iterator to the first element that does not satisfy the predicate
    //! or the end of the range if there is none.
    template <Enum Value = Default<Enum>, typename Pred = void>
    ConstIterator<Type, Value> TraverseWhile(Pred &&) const;

    //! Iterates over this object's elements while calling the given predicate and returns a
    //! pair of iterators representing the first range of elements that satisfies the predicate.
    //!
    //! \tparam Value The direction in which the range of elements is traversed
    //! \tparam Pred Unary invocable receiving an element and returning a bool
    //! \return Iterator to the first element that does not satisfy the predicate
    //! or the end of the range if there is none.
    template <Enum Value = Default<Enum>, typename Pred = void>
    Interval<ConstIterator<Type, Value>> GetMatchingRange(Pred &&) const;

    template <Enum Value = Default<Enum>, typename Condition = void, typename Terminator = void>
    Interval<ConstIterator<Type, Value>> GetMatchingRange(Condition &&, Terminator &&) const;
};

template <typename Container>
constexpr size_t size(const Container &);

template <auto Value, typename Container>
constexpr ConstIterator<Container, Value> begin(const Container &);

template <auto Value, typename Container, OSIQL_REQUIRES(!std::is_const_v<Container>)>
constexpr Iterator<Container, Value> begin(Container &iterable)
{
    if constexpr (IsInverse(Value))
    {
        return iterable.rbegin();
    }
    else
    {
        return iterable.begin();
    }
}

template <auto Value, typename Container>
constexpr ConstIterator<Container, Value> end(const Container &);

template <auto Value, typename Container, OSIQL_REQUIRES(!std::is_const_v<Container>)>
constexpr Iterator<Container, Value> end(Container &iterable)
{
    if constexpr (IsInverse(Value))
    {
        return iterable.rend();
    }
    else
    {
        return iterable.end();
    }
}

template <auto Value, typename Container>
constexpr const_reference<Container> front(const Container &);

template <auto Value, typename Container, OSIQL_REQUIRES(!std::is_const_v<Container>)>
constexpr reference<Container> front(Container &iterable)
{
    return *begin<Value>(iterable);
}

template <typename Enum, typename Container>
constexpr const_reference<Container> front(Enum value, const Container &);

template <typename Enum, typename Container, OSIQL_REQUIRES(!std::is_const_v<Container>)>
constexpr reference<Container> front(Enum value, Container &iterable)
{
    return IsInverse(value) ? *begin<!Default<Enum>>(iterable) : *begin<Default<Enum>>(iterable);
}

template <auto Value, typename Container>
constexpr const_reference<Container> back(const Container &);

template <auto Value, typename Container, OSIQL_REQUIRES(!std::is_const_v<Container>)>
constexpr reference<Container> back(Container &iterable)
{
    return *std::prev(end<Value>(iterable));
}

template <typename Enum, typename Container>
constexpr const_reference<Container> back(Enum value, const Container &);

template <typename Enum, typename Container, OSIQL_REQUIRES(!std::is_const_v<Container>)>
constexpr reference<Container> back(Enum value, Container &iterable)
{
    return IsInverse(value) ? *begin<Default<Enum>>(iterable) : *begin<!Default<Enum>>(iterable);
}

template <typename It, typename Pred>
It TraverseUntil(It first, It pastLast, Pred &&);

template <typename It, typename Pred>
It TraverseWhile(It first, It pastLast, Pred &&);

template <typename It, typename Pred>
Interval<It> GetMatchingRange(It first, It pastLast, Pred &&);

template <typename It, typename Until, typename While>
Interval<It> GetMatchingRange(It first, It pastLast, Until &&, While &&);
} // namespace osiql
