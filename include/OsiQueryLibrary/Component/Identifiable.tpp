/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Identifiable.h"

#include <osi3/osi_sensorview.pb.h>

namespace osiql {
namespace detail {
template <typename Type>
Handle<Type> ConvertHandle(const Type &rawHandle)
{
    if constexpr (std::is_constructible_v<Handle<Type>, const Type &>)
    {
        return rawHandle;
    }
    else
    {
        return &rawHandle;
    }
}
} // namespace detail
template <typename Type>
Wrapper<Type>::Wrapper(const Type &handle) :
    handle(detail::ConvertHandle(handle))
{
}

template <typename Type>
const Type &Wrapper<Type>::GetHandle() const
{
    if constexpr (is_pointer<decltype(handle)>)
    {
        return *handle;
    }
    else
    {
        return handle;
    }
}

template <typename Type>
Id Identifiable<Type>::GetId() const
{
    if constexpr (std::is_same_v<Type, osi3::SensorView>)
    {
        return this->GetHandle().sensor_id().value();
    }
    else if constexpr (std::is_same_v<Type, osi3::HostVehicleData>)
    {
        return this->GetHandle().has_host_vehicle_id() ? this->GetHandle().host_vehicle_id().value() : UNDEFINED_ID;
    }
    else
    {
        return this->GetHandle().has_id() ? this->GetHandle().id().value() : UNDEFINED_ID;
    }
}
} // namespace osiql
