/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Points.h"

#include "Iterable.tpp"
#include "OsiQueryLibrary/Point/Pose.h"
#include "OsiQueryLibrary/Point/XY.tpp"
#include "OsiQueryLibrary/Types/Constants.h"
#include "OsiQueryLibrary/Utility/Extract.tpp"

namespace osiql {
template <typename Type, typename From>
template <Direction D, typename GlobalPoint>
constexpr ConstIterator<Type, D> Points<Type, From>::begin(const GlobalPoint &point) const
{
    return this->empty() ? begin<D>() : std::prev(end<D>(point));
}

template <typename Type, typename From>
template <Direction D, typename GlobalPoint>
constexpr ConstIterator<Type, D> Points<Type, From>::end(const GlobalPoint &point) const
{
    if (this->size() < 2)
    {
        return end<D>();
    } // clang-format off
    return TernaryIteratorSearch(std::next(begin<D>()), std::prev(end<D>()), [point = XY{point}](auto it) {
        return point.GetPathTo(as<XY>(*std::prev(it)), as<XY>(*it)).SquaredLength();
    }, std::less{}); // clang-format on
}

template <typename Type, typename From>
template <Direction D>
constexpr ConstIterator<Type, D> Points<Type, From>::GetNearestEdge(double s) const
{
    if (this->size() < 2)
    {
        throw std::runtime_error("GetNearestEdge: Chain with fewer than 2 points has no edges.");
    }
    auto it{end<D>(s)};
    return (it == begin<D>()) ? ++it : ((it == end<D>()) ? --it : it);
}

template <typename Type, typename From>
template <Direction D>
constexpr Iterator<Type, D> Points<Type, From>::GetNearestEdge(double s)
{
    return const_cast<Iterator<Type, D>>(std::as_const(*this).GetNearestEdge(s));
}

template <typename Type, typename From>
double Points<Type, From>::GetLength() const
{
    double length{0.0};
    if (!this->empty())
    {
        for (auto it{std::next(begin())}; it != end(); ++it)
        {
            length += (*it - *std::prev(it)).Length();
        }
    }
    return length;
}

template <typename Type, typename From>
template <Direction D>
double Points<Type, From>::GetAngle(double s) const
{
    if constexpr (D == Direction::Upstream)
    {
        return WrapAngle(pi + GetAngle<!D>(s));
    }
    else
    {
        auto it{GetNearestEdge<D>(s)};
        const XY post{Norm(std::next(it) == end<D>() ? (*it - *std::prev(it)) : (*std::next(it) - *it))};
        const XY edge{Norm(*it - *std::prev(it))};
        --it;
        const XY pre{Norm(it == begin<D>() ? (*std::next(it) - *it) : (*it - *std::prev(it)))};

        const double startS{get<S>(*it)};
        const double endS{get<S>(*std::next(it))};
        const double ratio{(s - startS) / (endS - startS)};

        const double startAngle{(pre + edge).Angle()};
        const double endAngle{(edge + post).Angle()};
        if (startAngle - endAngle > pi)
        {
            return std::fmod(startAngle * (1.0 - ratio) + (endAngle + twoPi) * ratio, twoPi);
        }
        if (endAngle - startAngle > pi)
        {
            return std::fmod((startAngle + twoPi) * (1.0 - ratio) + endAngle * ratio, twoPi);
        }
        return startAngle * (1.0 - ratio) + endAngle * ratio;
    }
}

template <typename Type, typename From>
double Points<Type, From>::GetAngle(double s, Direction direction) const
{
    return direction == Direction::Upstream ? GetAngle<Direction::Upstream>(s) : GetAngle<Direction::Downstream>(s);
}

template <typename Type, typename From>
template <Direction D>
double Points<Type, From>::GetCurvature(double s, double epsilon) const // NOLINT(bugprone-easily-swappable-parameters)
{
    const auto edgeEnd{GetNearestEdge<D>(s)};
    const auto edgeStart{std::prev(edgeEnd)};
    if (Extract<D>::less(s, *edgeStart) || Extract<D>::greater(s, *edgeEnd))
    {
        return std::numeric_limits<double>::quiet_NaN();
    }
    const XY edge{*edgeEnd - *edgeStart};
    const double halfEdgeLength{edge.Length() * 0.5};
    if (halfEdgeLength <= epsilon)
    {
        return 0.0;
    }
    double startCurvature{0.0};
    if (edgeStart != begin<D>())
    {
        const XY priorEdge{*edgeStart - *std::prev(edgeStart)};
        const double deltaAngle{std::atan2(priorEdge.Cross(edge), priorEdge.Dot(edge))};
        const double length{halfEdgeLength + priorEdge.Length() * 0.5};
        startCurvature = deltaAngle / length;
    }

    double endCurvature{0.0};
    if (std::next(edgeEnd) != end<D>())
    {
        const XY nextEdge{*std::next(edgeEnd) - *edgeEnd};
        const double deltaAngle{std::atan2(edge.Cross(nextEdge), edge.Dot(nextEdge))};
        const double length{halfEdgeLength + nextEdge.Length() * 0.5};
        endCurvature = deltaAngle / length;
    }

    const double startS{get<S>(*edgeStart)};
    const double endS{get<S>(*edgeEnd)};
    if (std::abs(endS - startS) < epsilon)
    {
        return 0.0;
    }
    const double ratio{(s - startS) / (endS - startS)};
    return startCurvature * (1.0 - ratio) + endCurvature * ratio;
}

template <typename Type, typename From>
double Points<Type, From>::GetCurvature(double s, Direction direction, double epsilon) const
{
    return direction == Direction::Upstream ? GetCurvature<Direction::Upstream>(s, epsilon) : GetCurvature<Direction::Downstream>(s, epsilon);
}

template <typename Type, typename From>
double Points<Type, From>::GetT(double s) const
{
    if (this->size() < 2)
    {
        if (!this->empty() && get<S>(this->front()) == s)
        {
            return extract<Side::Left>(this->front());
        }
        throw std::runtime_error("Points::GetT: Chain of points has fewer than two points. Unable to determine t-coordinate at given s-coordinate");
    }
    const ConstIterator<Type> it{begin(s)};
    if (std::next(it) == end<Direction::Downstream>() || get<S>(*it) == get<S>(*std::next(it)))
    {
        return extract<Side::Left>(*it);
    }
    const double ratio{(s - get<S>(*it)) / (get<S>(*std::next(it)) - get<S>(*it))};
    return extract<Side::Left>(*it) * (1.0 - ratio) + extract<Side::Left>(*std::next(it)) * ratio;
}

template <typename Type, typename From>
XY Points<Type, From>::GetXY(double s) const
{
    const auto it{GetNearestEdge(s)};
    const double prevS{get<S>(*std::prev(it))};
    const double currS{get<S>(*it)};
    const double ratio{(s - prevS) / (currS - prevS)};
    return XY{*std::prev(it)} * (1.0 - ratio) + XY{*it} * ratio;
}

template <typename Type, typename From>
XY Points<Type, From>::GetXY(const ST &coordinates) const
{
    const auto it{GetNearestEdge(coordinates.s)};
    const XY edgeStart{*std::prev(it)};
    const XY edgeEnd{*it};
    const XY edge{edgeEnd - edgeStart};
    const XY normal{Norm(XY{-edge.y, edge.x})};

    const double startDistance{get<S>(*std::prev(it))};
    const double endDistance{get<S>(*it)};
    const double ratio{(coordinates.s - startDistance) / (endDistance - startDistance)};
    const XY pointOnEdge{edgeStart * (1.0 - ratio) + edgeEnd * ratio};

    return pointOnEdge + normal * coordinates.t;
}

template <typename Type, typename From>
template <typename GlobalPoint>
decltype(auto) Points<Type, From>::Localize(const GlobalPoint &input) const
{
    const auto it{this->end(input)};
    if constexpr (Is<Angle>::in<GlobalPoint>)
    {
        return Pose<ST>{
            input.GetST(*std::prev(it), *it),
            WrapAngle(get<Angle>(input) - (XY{*it} - XY{*std::prev(it)}).Angle()) //
        };
    }
    else if constexpr (std::is_same_v<GlobalPoint, XY>)
    {
        return input.GetST(*std::prev(it), *it);
    }
    {
        return Localize(XY{input});
    }
}

template <typename Type, typename From>
std::ostream &operator<<(std::ostream &os, const Points<Type, From> &line)
{
    if (!line.empty())
    {
        os << line.front();
        for (auto it{std::next(static_cast<const From &>(line).begin())}; it != static_cast<const From &>(line).end(); ++it)
        {
            os << ", " << *it;
        }
    }
    return os;
}
} // namespace osiql
