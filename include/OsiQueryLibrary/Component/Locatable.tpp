/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Locatable.h"

#include "Identifiable.tpp"
#include "OsiQueryLibrary/Point/Point.h"
#include "OsiQueryLibrary/Point/XY.tpp"
#include "OsiQueryLibrary/Point/XYZ.tpp"
#include "OsiQueryLibrary/Trait/BoostGeometry.h"
#include "OsiQueryLibrary/Types/LocalBounds.tpp"
#include "OsiQueryLibrary/Utility/Compare.h"
#include "OsiQueryLibrary/Utility/XYZ.h"

namespace osiql {
template <typename Handle>
Locatable<Handle>::Locatable(const Handle &handle) :
    Identifiable<Handle>{handle}, Aggregate<Shape, Bounds>{Shape{}, Bounds{}}
{
    UpdateShapeAndBounds();
}

template <typename Handle>
double Locatable<Handle>::GetX() const
{
    return (GetBase().has_position() && GetBase().position().has_x()) ? GetBase().position().x() : .0;
}

template <typename Handle>
double Locatable<Handle>::GetY() const
{
    return (GetBase().has_position() && GetBase().position().has_y()) ? GetBase().position().y() : .0;
}

template <typename Handle>
double Locatable<Handle>::GetZ() const
{
    return (GetBase().has_position() && GetBase().position().has_z()) ? GetBase().position().z() : .0;
}

template <typename Handle>
XY Locatable<Handle>::GetXY() const
{
    return {GetX(), GetY()};
}

template <typename Handle>
XYZ Locatable<Handle>::GetXYZ() const
{
    return {GetX(), GetY(), GetZ()};
}

template <typename Handle>
double Locatable<Handle>::GetWidth() const
{
    if (GetBase().has_dimension() && GetBase().dimension().has_width())
    {
        return GetBase().dimension().width();
    }
    const auto [min, max]{std::minmax_element(GetBase().base_polygon().begin(), GetBase().base_polygon().end(), Less<Y>{})};
    return max->y() - min->y();
}

template <typename Handle>
double Locatable<Handle>::GetLength() const
{
    if (GetBase().has_dimension() && GetBase().dimension().has_length())
    {
        return GetBase().dimension().length();
    }
    const auto [min, max]{std::minmax_element(GetBase().base_polygon().begin(), GetBase().base_polygon().end(), Less<X>{})};
    return max->x() - min->x();
}

template <typename Handle>
double Locatable<Handle>::GetHeight() const
{
    return (GetBase().has_dimension() && GetBase().dimension().has_height()) ? GetBase().dimension().height() : .0;
}

template <typename Handle>
XY Locatable<Handle>::GetSize() const
{
    return {GetLength(), GetWidth()};
}

template <typename Handle>
Rotation Locatable<Handle>::GetRotation() const
{
    if (GetBase().has_orientation())
    {
        return {GetBase().orientation().has_roll() ? GetBase().orientation().roll() : 0.0,   //
                GetBase().orientation().has_pitch() ? GetBase().orientation().pitch() : 0.0, //
                GetBase().orientation().has_yaw() ? GetBase().orientation().yaw() : 0.0};
    }
    return {0.0, 0.0, 0.0};
}

template <typename Handle>
double Locatable<Handle>::GetPitch() const
{
    return (GetBase().has_orientation() && GetBase().orientation().has_pitch()) ? GetBase().orientation().pitch() : 0.0;
}

template <typename Handle>
double Locatable<Handle>::GetRoll() const
{
    return (GetBase().has_orientation() && GetBase().orientation().has_roll()) ? GetBase().orientation().roll() : 0.0;
}

template <typename Handle>
double Locatable<Handle>::GetYaw() const
{
    return (GetBase().has_orientation() && GetBase().orientation().has_yaw()) ? GetBase().orientation().yaw() : 0.0;
}

template <typename Handle>
void Locatable<Handle>::UpdateShapeAndBounds()
{
    const Matrix<4> &transform{GetTransformationMatrix()};
    GetShape().clear();
    if (!GetBase().base_polygon().empty())
    {
        GetShape().reserve(static_cast<size_t>(GetBase().base_polygon().size()));
        for (const auto &point : GetBase().base_polygon())
        {
            GetShape().emplace_back(transform * point);
        }
    }
    else // No base polygon provided: Use width, length and center to create a rectangular shape
    {
        const double halfLength{GetLength() * 0.5};
        const double halfWidth{GetWidth() * 0.5};
        // Counter-clockwise winding so that external distances to it are positive:
        GetShape().emplace_back(transform * XYZ{-halfLength, -halfWidth, 0.0}); // Rear right
        GetShape().emplace_back(transform * XYZ{halfLength, -halfWidth, 0.0});  // Front right
        GetShape().emplace_back(transform * XYZ{halfLength, halfWidth, 0.0});   // Front left
        GetShape().emplace_back(transform * XYZ{-halfLength, halfWidth, 0.0});  // Rear left
    }
    boost::geometry::envelope(GetShape(), GetBounds());
}

template <typename Handle>
std::vector<Point<const Lane>> Locatable<Handle>::GetLocalShape(const Lane &lane) const
{
    std::vector<Point<const Lane>> localShape;
    localShape.reserve(GetShape().size());
    std::transform(GetShape().begin(), GetShape().end(), std::back_inserter(localShape), [&](const XY &point) {
        return lane.Localize(point);
    });
    return localShape;
}

template <typename Handle>
const Base<Handle> &Locatable<Handle>::GetBase() const
{
    if constexpr (std::is_same_v<Handle, osi3::TrafficSign>)
    {
        assert(this->GetHandle().main_sign().has_base());
        return this->GetHandle().main_sign().base();
    }
    else
    {
        assert(this->GetHandle().has_base());
        return this->GetHandle().base();
    }
}

template <typename Handle>
const Matrix<3> &Locatable<Handle>::GetRotationMatrix() const
{
    if (!rotationMatrix.has_value())
    {
        rotationMatrix = RotationMatrix(GetRotation());
    }
    return rotationMatrix.value();
}

template <typename Handle>
const Matrix<4> &Locatable<Handle>::GetTranslationMatrix() const
{
    if (!translationMatrix.has_value())
    {
        const auto &contour{GetBase().base_polygon()};
        XYZ displacement{GetXY()};
        // Double check that the object's center is (0, 0) and move it there if it is not.
        // NOTE: This can be removed in the future (or be made debug-only)
        if (!contour.empty())
        {
            const auto [minX, maxX]{std::minmax_element(contour.begin(), contour.end(), Less<X>{})};
            const auto [minY, maxY]{std::minmax_element(contour.begin(), contour.end(), Less<Y>{})};
            const XYZ offset{X{}(*minX) + X{}(*maxX), Y{}(*minY) + Y{}(*maxY), 0.0};
            if (offset.HasLength())
            {
                std::cerr << "Bounding box of shape lies on " << offset << " instead of the required (0, 0). Adjusting transformation matrix.\n";
            }
            displacement -= offset;
        }
        translationMatrix = TranslationMatrix(displacement);
    }
    return translationMatrix.value();
}

template <typename Handle>
const Matrix<4> &Locatable<Handle>::GetTransformationMatrix() const
{
    if (!transformationMatrix.has_value())
    {
        transformationMatrix = GetTranslationMatrix() * GetRotationMatrix();
    }
    return transformationMatrix.value();
}

template <typename Handle>
Point<const Lane> Locatable<Handle>::GetFurthestPoint(const Lane &lane, Traversal traversal) const
{
    const auto localShape{GetLocalShape(lane)};
    if (lane.GetDirection(traversal) == Direction::Upstream)
    {
        return *std::max_element(localShape.begin(), localShape.end(), Extract<Direction::Upstream>::less);
    }
    return *std::max_element(localShape.begin(), localShape.end(), Extract<Direction::Downstream>::less);
}

template <typename Handle>
void Locatable<Handle>::OutdateTranslation()
{
    translationMatrix = std::nullopt;
    transformationMatrix = std::nullopt;
    GetShape().clear();
}

template <typename Handle>
void Locatable<Handle>::OutdateRotation()
{
    rotationMatrix = std::nullopt;
    transformationMatrix = std::nullopt;
    GetShape().clear();
}
} // namespace osiql
