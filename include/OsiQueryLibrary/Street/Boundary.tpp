/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Street/Boundary.h"

#include "OsiQueryLibrary/Component/Identifiable.tpp"
#include "OsiQueryLibrary/Component/Iterable.tpp"
#include "OsiQueryLibrary/Point/Coordinates.h"
#include "OsiQueryLibrary/Types/Interval.tpp"

namespace osiql {
namespace {
template <Side Increasing>
Side GetPassingRule(const Boundary &boundary)
{
    if (boundary.GetHandle().has_passing_rule())
    {
        switch (boundary.GetHandle().passing_rule())
        {
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_INCREASING_T:
            return Increasing;
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_DECREASING_T:
            return !Increasing;
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_NONE_ALLOWED:
            return Side::None;
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_BOTH_ALLOWED:
            return Side::Both;
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_OTHER:
            return Side::Other;
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_UNKNOWN:
        default:
            return Side::Undefined;
        }
    }
    else
    {
        return Side::Undefined;
    }
}
} // namespace

template <Direction D>
Side Boundary::GetPassingRule() const
{
    return osiql::GetPassingRule<GetSide(D)>(*this);
}

template <Direction D>
double Boundary::GetLateralDistance(const ST &coords) const
{
    return Difference<D>(GetT(coords.s), coords.t);
}

template <Direction D, typename Coordinates>
Interval<double> Boundary::GetLateralDistances(const Coordinates &coords) const
{
    return GetLateralDistances(coords.begin(), coords.end());
}

template <typename Coordinates>
Interval<double> Boundary::GetLateralDistances(Direction direction, const Coordinates &coords) const
{
    return IsInverse(direction) ? GetLateralDistances<!Default<Direction>>(coords) : GetLateralDistances<Default<Direction>>(coords);
}

template <Direction D, typename It>
Interval<double> Boundary::GetLateralDistances(It begin, It end) const
{
    std::vector<double> distances;
    distances.reserve(static_cast<size_t>(std::distance(begin, end)));
    std::transform(begin, end, std::back_inserter(distances), [this](const ST &coords) {
        return GetLateralDistance<D>(coords);
    });
    const auto [min, max]{std::minmax_element(distances.begin(), distances.end())};
    return {*min, *max};
}

template <typename It>
Interval<double> Boundary::GetLateralDistances(Direction direction, It begin, It end) const
{
    return IsInverse(direction) ? GetLateralDistances<!Default<Direction>>(begin, end) : GetLateralDistances<Default<Direction>>(begin, end);
}
} // namespace osiql
