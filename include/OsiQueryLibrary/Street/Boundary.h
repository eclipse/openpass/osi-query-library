/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Wrapper of a osi3::LogicalLaneBoundary

#include <osi3/osi_logicallane.pb.h>

#include "OsiQueryLibrary/Component/Identifiable.h"
#include "OsiQueryLibrary/Street/LaneMarking.h"
#include "OsiQueryLibrary/Types/Interval.h"

namespace osiql {
//! A Boundary is a wrapper of a osi3::LogicalLaneBoundary (while osiql::LaneMarking is a wrapper of a osi3::LaneBoundary).
//! Its points hold global xy- and local st-coordinate information.
struct Boundary : Identifiable<osi3::LogicalLaneBoundary>, Points<Container<osi3::LogicalLaneBoundary::LogicalBoundaryPoint>, Boundary>
{
    using Identifiable::Identifiable;

    //! \brief Alias for the point type of the underlying OSI object
    using Point = osi3::LogicalLaneBoundary::LogicalBoundaryPoint;
    using Base = Points<Container<Point>, Boundary>;

    using Base::begin;
    ConstIterator<Container<Point>> begin() const;

    using Base::end;
    ConstIterator<Container<Point>> end() const;

    //! Returns to which side an object may pass over this boundary in the given direction.
    //!
    //! \tparam Direction
    //! \return Side
    template <Direction = Direction::Downstream>
    Side GetPassingRule() const;

    //! Returns to which side an object may pass over this boundary in the given direction.
    //!
    //! \param Direction
    //! \return Side Left or Right
    Side GetPassingRule(Direction) const;

    //! Returns the signed difference in t-coordinate from the point on this boundary with the closest s-coordinate
    //! to the given point. The distance is negative if the given point is to the right of the boundary.
    //!
    //! \tparam Direction Downstream or Upstream. Affects what is considered left and right
    //! \param ST st-coordinate pair
    //! \return signed t-coordinate difference from the point to the nearest boundary of this chain
    template <Direction = Direction::Downstream>
    double GetLateralDistance(const ST &) const;

    //! Returns the signed difference in t-coordinate from the point on this boundary with the closest s-coordinate
    //! to the given point. The distance is negative if the given point is to the right of the boundary.
    //!
    //! \param Direction Downstream or Upstream. Affects what is considered left and right
    //! \param ST st-coordinate pair
    //! \return signed t-coordinate difference from the point to the nearest boundary of this chain
    double GetLateralDistance(Direction, const ST &) const;

    //! Returns the smallest and largest signed differences in t-coordinate from the point
    //! on this boundary with the closest s-coordinate to each given point.
    //! The distance is negative if the given point is to the right of the boundary.
    //!
    //! \tparam Direction Downstream or Upstream. Affects what is considered left and right
    //! \tparam Coordinates Iterable container of st-coordinate pairs
    //! \param Coordinates If empty, a default-constructed interval is returned
    //! \return signed t-coordinate difference from the point to the nearest boundary of this chain
    template <Direction = Direction::Downstream, typename Coordinates = std::vector<ST>>
    Interval<double> GetLateralDistances(const Coordinates &) const;

    //! Returns the smallest and largest signed differences in t-coordinate from the point
    //! on this boundary with the closest s-coordinate to each given point.
    //! The distance is negative if the given point is to the right of the boundary.
    //!
    //! \tparam Coordinates Iterable container of st-coordinate pairs
    //! \param Direction Downstream or Upstream. Affects what is considered left and right
    //! \param Coordinates If empty, a default-constructed interval is returned
    //! \return signed t-coordinate difference from the point to the nearest boundary of this chain
    template <typename Coordinates = std::vector<ST>>
    Interval<double> GetLateralDistances(Direction, const Coordinates &) const;

    //! Returns the smallest and largest signed differences in t-coordinate from the point
    //! on this boundary with the closest s-coordinate to each given point.
    //! The distance is negative if the given point is to the right of the boundary.
    //!
    //! \tparam Direction Downstream or Upstream. Affects what is considered left and right
    //! \tparam It Iterator pointing to an st-coordinate pair
    //! \param begin Iterator to the first st-coordinate pair
    //! \param end Iterator directly past the last st-coordinate pair
    //! \return signed t-coordinate difference from the point to the nearest boundary of this chain
    template <Direction = Direction::Downstream, typename It = void>
    Interval<double> GetLateralDistances(It begin, It end) const;

    //! Returns the smallest and largest signed differences in t-coordinate from the point
    //! on this boundary with the closest s-coordinate to each given point.
    //! The distance is negative if the given point is to the right of the boundary.
    //!
    //! \tparam It Iterator pointing to an st-coordinate pair
    //! \param Direction Downstream or Upstream. Affects what is considered left and right
    //! \param begin Iterator to the first st-coordinate pair
    //! \param end Iterator directly past the last st-coordinate pair
    //! \return signed t-coordinate difference from the point to the nearest boundary of this chain
    template <typename It>
    Interval<double> GetLateralDistances(Direction, It begin, It end) const;

    //! \brief Set of lane markings sorted right to left (guaranteed by OSI)
    std::vector<const LaneMarking *> markings;
};

std::ostream &operator<<(std::ostream &, const Boundary::Point &);
std::ostream &operator<<(std::ostream &, const Boundary &);
} // namespace osiql
