/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Adjoinable.h"
#include "OsiQueryLibrary/Street/BoundaryChain.h"
#include "OsiQueryLibrary/Types/Bounds.h"
#include "OsiQueryLibrary/Types/Enum/Direction.h"
#include "OsiQueryLibrary/Types/Enum/Side.h"
#include "OsiQueryLibrary/Types/Enum/Traversal.h"
#include "OsiQueryLibrary/Types/Shape.h"
#include "OsiQueryLibrary/Utility/Aggregate.h"

namespace osiql {
struct ST;
template <size_t>
struct Vector;

namespace detail {
//! \brief A boundary enclosure is a component of a lane that defines getters to its
//! boundary points and logical partitions. These members are not public because
//! what constitutes the left and right boundary depends on the lane's intended
//! driving direction and the orientation in which it is being traversed.
template <typename Lane>
struct BoundaryEnclosure : Adjoinable<Lane>, Aggregate<Shape, Bounds>
{
    template <typename Param, OSIQL_REQUIRES(std::is_constructible_v<Adjoinable<Lane>, Param &&>)>
    BoundaryEnclosure(Param &&argument) :
        Adjoinable<Lane>{std::forward<Param>(argument)}, Aggregate<Shape, Bounds>{Shape{}, Bounds{}}
    {
    }

    //! Returns the lane boundaries on the given side of this lane
    //!
    //! \tparam Side Left or Right
    //! \tparam RelativeTo Direction or Traversal
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <Side, auto RelativeTo = Traversal::Forward>
    const BoundaryChain &GetBoundaries() const;

    //! Returns the lane boundaries on the given side of this lane
    //!
    //! \tparam Side Left or Right
    //! \tparam RelativeTo Direction or Traversal
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <Side, auto RelativeTo = Traversal::Forward>
    BoundaryChain &GetBoundaries();

    //! Returns the lane boundaries on the given side of this lane
    //!
    //! \tparam Side Left or Right
    //! \tparam RelativeTo Direction or Traversal
    //! \param RelativeTo The examined side is relative to the given direction/orientation
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <Side, typename RelativeTo>
    const BoundaryChain &GetBoundaries(RelativeTo) const;

    //! Returns the lane boundaries on the given side of this lane
    //!
    //! \tparam Side Left or Right
    //! \tparam RelativeTo Direction or Traversal
    //! \param RelativeTo The examined side is relative to the given direction/orientation
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <Side, typename RelativeTo>
    BoundaryChain &GetBoundaries(RelativeTo);

    //! Returns the lane boundaries on the given side of this lane
    //!
    //! \tparam RelativeTo Direction or Traversal
    //! \param Side Left or Right
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <auto RelativeTo = Traversal::Forward>
    const BoundaryChain &GetBoundaries(Side) const;

    //! Returns the lane boundaries on the given side of this lane
    //!
    //! \tparam RelativeTo Direction or Traversal
    //! \param Side Left or Right
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <auto RelativeTo = Traversal::Forward>
    BoundaryChain &GetBoundaries(Side);

    //! Returns the lane boundaries on the given side of this lane
    //!
    //! \tparam RelativeTo Direction or Traversal
    //! \param Side Left or Right
    //! \param RelativeTo The examined side is relative to the given direction/orientation
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <typename RelativeTo>
    const BoundaryChain &GetBoundaries(Side, RelativeTo) const;

    //! Returns the lane boundaries on the given side of this lane
    //!
    //! \param Side Left or Right
    //! \tparam RelativeTo Direction or Traversal
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <typename RelativeTo>
    BoundaryChain &GetBoundaries(Side, RelativeTo);

    //! Returns an iterator to the lane boundary on the given side of this lane closest to the given s-coordinate.
    //! If there are two equally close boundaries, the latter of the two in the given direction/orientation is returned.
    //!
    //! \tparam Side Left or Right
    //! \tparam Direction Downstream or Upstream
    //! \param s s-coordinate
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <Side, Direction D = Direction::Downstream>
    ConstIterator<std::vector<Boundary *>, D> FindBoundary(double s) const;

    //! Returns an iterator to the lane boundary on the given side of this lane closest to the given s-coordinate.
    //! If there are two equally close boundaries, the latter of the two in the given direction/orientation is returned.
    //!
    //! \tparam Side Left or Right
    //! \tparam Direction Downstream or Upstream
    //! \param s s-coordinate
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <Side, Direction D = Direction::Downstream>
    Iterator<std::vector<Boundary *>, D> FindBoundary(double s);

    //! Returns an iterator to the lane boundary on the given side of this lane closest to the given s-coordinate.
    //! If there are two equally close boundaries, the latter of the two in the given direction/orientation is returned.
    //!
    //! \tparam Direction Downstream or Upstream
    //! \param Side Left or Right
    //! \param s s-coordinate
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <Direction D = Direction::Downstream>
    ConstIterator<std::vector<Boundary *>, D> FindBoundary(Side, double s) const;

    //! Returns an iterator to the lane boundary on the given side of this lane closest to the given s-coordinate.
    //! If there are two equally close boundaries, the latter of the two in the given direction/orientation is returned.
    //!
    //! \tparam Direction Downstream or Upstream
    //! \param Side Left or Right
    //! \param s s-coordinate
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <Direction D = Direction::Downstream>
    Iterator<std::vector<Boundary *>, D> FindBoundary(Side, double s);

    //! Returns the lane boundary on the given side of this lane closest to the given s-coordinate. If there
    //! are two equally close boundaries, the latter of the two in the given direction/orientation is returned.
    //!
    //! \tparam Side Left or Right
    //! \tparam RelativeTo Direction or Traversal
    //! \param s s-coordinate closest to the returned boundary
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <Side, auto RelativeTo = Traversal::Forward>
    const Boundary &GetBoundary(double s) const;

    //! Returns the lane boundary on the given side of this lane closest to the given s-coordinate. If there
    //! are two equally close boundaries, the latter of the two in the given direction/orientation is returned.
    //!
    //! \tparam Side Left or Right
    //! \tparam RelativeTo Direction or Traversal
    //! \param s s-coordinate closest to the returned boundary
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <Side, auto RelativeTo = Traversal::Forward>
    Boundary &GetBoundary(double s);

    //! Returns the lane boundary on the given side of this lane closest to the given s-coordinate. If there
    //! are two equally close boundaries, the latter of the two in the given direction/orientation is returned.
    //!
    //! \tparam RelativeTo Direction or Traversal
    //! \param Side Left or Right
    //! \param s s-coordinate closest to the returned boundary
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <auto RelativeTo = Traversal::Forward>
    const Boundary &GetBoundary(Side, double s) const;

    //! Returns the lane boundary on the given side of this lane closest to the given s-coordinate. If there
    //! are two equally close boundaries, the latter of the two in the given direction/orientation is returned.
    //!
    //! \tparam RelativeTo Direction or Traversal
    //! \param Side Left or Right
    //! \param s s-coordinate closest to the returned boundary
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <auto RelativeTo = Traversal::Forward>
    Boundary &GetBoundary(Side, double s);

    //! Returns the lane boundary on the given side of this lane closest to the given s-coordinate. If there
    //! are two equally close boundaries, the latter of the two in the given direction/orientation is returned.
    //!
    //! \tparam Side Left or Right
    //! \tparam RelativeTo Direction or Traversal
    //! \param s s-coordinate closest to the returned boundary
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <Side, typename RelativeTo = Traversal>
    const Boundary &GetBoundary(RelativeTo, double s) const;

    //! Returns the lane boundary on the given side of this lane closest to the given s-coordinate. If there
    //! are two equally close boundaries, the latter of the two in the given direction/orientation is returned.
    //!
    //! \tparam Side Left or Right
    //! \tparam RelativeTo Direction or Traversal
    //! \param s s-coordinate closest to the returned boundary
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <Side, typename RelativeTo = Traversal>
    Boundary &GetBoundary(RelativeTo, double s);

    //! Returns the lane boundary on the given side of this lane closest to the given s-coordinate. If there
    //! are two equally close boundaries, the latter of the two in the given direction/orientation is returned.
    //!
    //! \tparam RelativeTo Direction or Traversal
    //! \param Side Left or Right
    //! \param s s-coordinate closest to the returned boundary
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <typename RelativeTo = Traversal>
    const Boundary &GetBoundary(Side, RelativeTo, double s) const;

    //! Returns the lane boundary on the given side of this lane closest to the given s-coordinate. If there
    //! are two equally close boundaries, the latter of the two in the given direction/orientation is returned.
    //!
    //! \tparam RelativeTo Direction or Traversal
    //! \param Side Left or Right
    //! \param s s-coordinate closest to the returned boundary
    //! \return Boundaries ordered from lowest to highest s-coordinate
    template <typename RelativeTo = Traversal>
    Boundary &GetBoundary(Side, RelativeTo, double s);

    //! Returns whether the given point is inside the given lane
    //!
    //! \return Whether the point is on the edges of or inside this lane.
    template <typename LocalOrGlobalPoint>
    bool Contains(const LocalOrGlobalPoint &) const;

    //! Returns the global angle of this lane's centerline at the given s-coordinate.
    //!
    //! \param s
    //! \param Traversal Forward or Backward
    //! \return double
    template <auto Towards = Traversal::Forward>
    double GetAngle(double s) const;

    //! Returns the global angle of this lane's centerline at the given s-coordinate.
    //!
    //! \tparam Towards Traversal or Downstream
    //! \param Towards The traversal direction/orientation
    //! \param s
    //! \return double
    template <typename Towards>
    double GetAngle(Towards, double s) const;

    //! Returns the average of the curvatures of this lane's left and right boundary at the given s-coordinate.
    //!
    //! \param s
    //! \param Traversal Forward or Backward
    //! \return double
    template <auto Towards = Traversal::Forward>
    double GetCurvature(double s) const;

    //! Returns the average of the curvatures of this lane's left and right boundary at the given s-coordinate.
    //!
    //! \tparam Direction Downstream or Upstream
    //! \param s
    //! \return double
    template <typename Towards>
    double GetCurvature(Towards, double s) const;

    //! Returns the t-coordinate of this lane's centerline at the given s-coordinate. If there is no centerline
    //! at the given s-coordinate, the t-coordinate of the nearest point on the centerline is returned instead.
    //!
    //! \param s s-coordinate
    //! \return Positive if to the left of this lane's reference line, otherwise negative
    double GetCenterlineT(double s) const;

    //! Converts the given st-coordinate pair relative to this lane's reference line
    //! to a uv-coordinate pair relative to this lane's centerline.
    //!
    //! \param ST st-coordinate pair where s represents the signed distance
    //! from the start of this lane's reference line along said reference line, while t represents
    //! the signed lateral distance from the point on the reference line with the same s-coordinate.
    //! \return uv-coordinate pair where u represents the signed distance from the start
    //! of this lane's centerline in driving direction along said centerline, while v represents
    //! the signed lateral distance from the point on the centerline with the same u-coordinate.
    UV GetUV(const ST &) const;

    //! Converts the given uv-coordinate pair relative to this lane's centerline
    //! to a st-coordinate pair relative to this lane's reference line.
    //!
    //! \param UV uv-coordinate pair where u represents the signed distance from the start
    //! of this lane's centerline in driving direction along said centerline, while v represents
    //! the signed lateral distance from the point on the centerline with the same u-coordinate.
    //! \return st-coordinate pair where s represents the signed distance
    //! from the start of this lane's reference line along said reference line, while t represents
    //! the signed lateral distance from the point on the reference line with the same s-coordinate.
    ST GetST(const UV &) const;

    //! Returns the signed distance from this lane's centerline to the given point.
    //!
    //! \param ST st-coordinate pair
    //! \return double Positive if the point is to the left of the centerline (in driving direction)
    template <auto Value = Traversal::Forward>
    double GetV(const ST &) const;

    //! Returns the signed minimum and maximum distance between the given range of st-coordinate pairs
    //! and this lane's centerline.
    //!
    //! \tparam It Iterator to an object that holds an s-coordinate accessible via extract<Direction::Downstream>
    //! \param first Iterator to the first point
    //! \param pastLast Iterator past the last point
    //! \return Interval with a minimum and maximum v-coordinate
    template <typename It>
    Interval<double> GetV(It first, It pastLast) const;

    //! Computes and stores the global shape and bounds of this lane.
    void UpdateShapeAndBounds();
};
} // namespace detail
} // namespace osiql
