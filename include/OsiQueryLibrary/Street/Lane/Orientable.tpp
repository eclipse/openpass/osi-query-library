/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Orientable.h"

#include "OsiQueryLibrary/Component/Identifiable.tpp"
#include "OsiQueryLibrary/Utility/Extract.tpp"

namespace osiql::detail {
template <auto Value, typename Type>
double Orientable::GetDistanceFromStart(const Type &object) const
{
    if constexpr (std::is_same_v<decltype(Value), Direction>)
    {
        return GetStartDifference<Value>(*this, object);
    }
    else if constexpr (std::is_same_v<decltype(Value), Traversal>)
    {
        return GetStartDifference(*this, object, GetDirection(Value));
    }
    else
    {
        static_assert(always_false<decltype(Value)>, "Not supported");
    }
}

template <typename Enum, typename Type>
double Orientable::GetDistanceFromStart(const Type &object, Enum value) const
{
    if constexpr (std::is_same_v<Enum, Direction>)
    {
        return IsInverse(value) ? GetDistanceFromStart<!Default<Enum>>(object) : GetDistanceFromStart<Default<Enum>>(object);
    }
    else if constexpr (std::is_same_v<Enum, Traversal>)
    {
        return GetStartDifference(*this, object, GetDirection(value));
    }
    else
    {
        static_assert(always_false<Enum>, "Not supported");
    }
}

template <auto Value, typename Type>
double Orientable::GetDistanceToEnd(const Type &object) const
{
    if constexpr (std::is_same_v<decltype(Value), Direction>)
    {
        return GetEndDifference<Value>(object, *this);
    }
    else if constexpr (std::is_same_v<decltype(Value), Traversal>)
    {
        return GetDistanceToEnd(object, GetDirection(Value));
    }
    else
    {
        static_assert(always_false<decltype(Value)>, "Not supported");
    }
}

template <typename Enum, typename Type>
double Orientable::GetDistanceToEnd(const Type &object, Enum value) const
{
    if constexpr (std::is_same_v<Enum, Direction>)
    {
    }
    else if constexpr (std::is_same_v<Enum, Traversal>)
    {
        return GetEndDifference(object, *this, GetDirection(value));
    }
    else
    {
        static_assert(always_false<Enum>, "Not supported");
    }
}
} // namespace osiql::detail
