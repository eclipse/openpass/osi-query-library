/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <deque>

#include "Orientable.h"
#include "OsiQueryLibrary/Types/Common.h"
#include "OsiQueryLibrary/Types/Enum/Direction.h"
#include "OsiQueryLibrary/Types/Enum/Traversal.h"

namespace osiql {
template <Traversal, typename>
struct Route;

struct Road;
namespace detail {
template <typename Lane>
struct Connectable : Orientable
{
    using Orientable::Orientable;

    //! Returns the id of this lane's road.
    //!
    //! \return std::string_view
    std::string_view GetRoadId() const;

    //! Returns a container of all lanes connected to the end of this one
    //! in the driving direction/orientation
    //!
    //! \tparam Toward Direction or Traversal
    //! \tparam Pred Invocable with the signature bool(const Lane*)
    //! \return const std::vector<Lane*>& without predicates, otherwise std::vector<const Lane*>
    template <auto Toward, typename... Pred>
    decltype(auto) GetConnectedLanes(Pred &&...) const;

    //! Returns a container of all lanes connected to the end of this one
    //! in the driving direction/orientation
    //!
    //! \tparam Toward Direction or Traversal
    //! \tparam Pred Invocable with the signature bool(const Lane*)
    //! \return std::vector<Lane*>& without predicates, otherwise std::vector<Lane*>
    template <auto Toward, typename... Pred>
    decltype(auto) GetConnectedLanes(Pred &&...);

    //! Returns a container of all lanes connected to the end of this one
    //! in the driving direction/orientation
    //!
    //! \tparam Toward Direction or Traversal
    //! \param Toward Forward, Backward, Downstream or Upstream
    //! \tparam Pred Invocable with the signature bool(const Lane*)
    //! \return std::vector<Lane*>& without predicates, otherwise std::vector<Lane*>
    template <typename Toward, typename... Pred>
    decltype(auto) GetConnectedLanes(Toward, Pred &&...) const;

    //! Returns a container of all lanes connected to the end of this one
    //! in the driving direction/orientation
    //!
    //! \tparam Toward Direction or Traversal
    //! \param Toward Forward, Backward, Downstream or Upstream
    //! \tparam Pred Invocable with the signature bool(const Lane*)
    //! \return std::vector<Lane*>& without predicates, otherwise std::vector<Lane*>
    template <typename Toward, typename... Pred>
    decltype(auto) GetConnectedLanes(Toward, Pred &&...);

    //! Returns a lane connected to the end of this one in the given direction
    //! that satisfies the given predicates or nullptr if no such lane exists.
    //!
    //! \tparam Toward Direction or Traversal
    //! \tparam Pred Predicate taking a const Lane& and returning a bool
    //! \param Pred The first lane satisfying this predicate gets returned.
    //! \return Lane satisfying the given predicates or nullptr if there are none
    template <auto Toward, typename... Pred>
    const Lane *GetConnectedLane(Pred &&...) const;

    //! Returns a lane connected to the end of this one in the given direction
    //! that satisfies the given predicates or nullptr if no such lane exists.
    //!
    //! \tparam Toward Direction or Traversal
    //! \tparam Pred Predicate taking a const Lane& and returning a bool
    //! \param Pred The first lane satisfying this predicate gets returned.
    //! \return Lane satisfying the given predicates or nullptr if there are none
    template <auto Toward, typename... Pred>
    Lane *GetConnectedLane(Pred &&...);

    //! Returns a lane connected to the end of this one in the given direction
    //! that satisfies the given predicates or nullptr if no such lane exists.
    //!
    //! \tparam Toward Direction or Traversal
    //! \tparam Pred Predicate taking a const Lane& and returning a bool
    //! \param Toward Forward, Backward, Downstream or Upstream
    //! \param Pred The first lane satisfying this predicate gets returned.
    //! \return Lane satisfying the given predicates or nullptr if there are none
    template <typename Toward, typename... Pred>
    const Lane *GetConnectedLane(Toward, Pred &&...) const;

    //! Returns the lane connected to the end of this one in the given direction
    //! that satisfies the given predicates or nullptr if no such lane exists.
    //!
    //! \tparam Toward Direction or Traversal
    //! \tparam Pred Predicate taking a const Lane& and returning a bool
    //! \param Toward Forward, Backward, Downstream or Upstream
    //! \param Pred The first lane satisfying this predicate gets returned.
    //! \return Lane satisfying the given predicate or nullptr if there are none
    template <typename Toward, typename... Pred>
    Lane *GetConnectedLane(Toward, Pred &&...);

    //! \brief Returns the chain of lanes matching the given chain of roads
    //! or a subset thereof based on the given orientation.
    //!
    //! \tparam Toward Traversal (Forward, Backward or Any) or Direction (Downstream, Upstream or Both).
    //! Restricts in what direction the lane chain is constructed.
    //! \tparam Roads Iterable range of elements holding a road that can be accessed via osiql::get<Road>
    //! \param Roads Container with member methods begin() & end()
    //! \return Chain of lanes; empty if this lane lies on none of the given roads
    template <auto Toward = Traversal::Any, Traversal T = Traversal::Forward>
    std::deque<const Lane *> GetChain(const Route<T, Road> &) const;

    //! \brief Returns the chain of lanes matching the given chain of roads
    //! or a subset thereof based on the given orientation.
    //!
    //! \tparam Toward Traversal (Forward, Backward or Any) or Direction (Downstream, Upstream or Both).
    //! \param Toward Restricts in what direction the lane chain is constructed.
    //! \tparam Roads Iterable range of elements holding a road that can be accessed via osiql::get<Road>
    //! \param Roads Container with member methods begin() & end()
    //! \return Chain of lanes; empty if this lane lies on none of the given roads
    template <typename Toward = Traversal, Traversal T = Traversal::Forward>
    std::deque<const Lane *> GetChain(Toward, const Route<T, Road> &) const;

private:
    std::vector<Lane *> outgoingLanes;
    std::vector<Lane *> incomingLanes;
};
} // namespace detail
} // namespace osiql
