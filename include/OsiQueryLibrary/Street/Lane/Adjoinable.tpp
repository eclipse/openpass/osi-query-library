/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Adjoinable.h"

#include "Connectable.tpp"
#include "OsiQueryLibrary/Component/Iterable.tpp"
#include "OsiQueryLibrary/Street/Road.h"
#include "OsiQueryLibrary/Types/Enum.tpp"
#include "OsiQueryLibrary/Utility/Has/Lane.tpp"

namespace osiql::detail {
template <typename Lane>
void Adjoinable<Lane>::SetRoad(Road &road)
{
    this->road = &road;
}

template <typename Lane>
Side Adjoinable<Lane>::GetSideOfRoad() const
{
    return GetSide(this->GetDirection());
}

template <typename Lane>
template <Side S, auto Toward>
const Lane *Adjoinable<Lane>::GetAdjacentLane(int offset) const
{
    if constexpr (std::is_same_v<decltype(Toward), Direction>)
    {
        if constexpr ((IsInverse(S)) != (IsInverse(Toward)))
        {
            return index < offset ? nullptr : GetRoad().lanes[index - offset];
        }
        else
        {
            return index + offset >= GetRoad().lanes.size() ? nullptr : GetRoad().lanes[index + offset];
        }
    }
    else if constexpr (std::is_same_v<decltype(Toward), Traversal>)
    {
        return GetAdjacentLane<S>(this->GetDirection(Toward), offset);
    }
    else
    {
        static_assert(always_false<decltype(Toward)>, "Must be Direction or Traversal");
    }
}

template <typename Lane>
template <Side S, auto Toward>
Lane *Adjoinable<Lane>::GetAdjacentLane(int offset)
{
    return const_cast<Lane *>(std::as_const(*this).template GetAdjacentLane<S, Toward>(offset));
}

template <typename Lane>
template <Side S, typename Toward>
const Lane *Adjoinable<Lane>::GetAdjacentLane(Toward toward, int offset) const
{
    return toward == !Default<Toward> ? GetAdjacentLane<S, !Default<Toward>>(offset) : GetAdjacentLane<S, Default<Toward>>(offset);
}

template <typename Lane>
template <Side S, typename Toward>
Lane *Adjoinable<Lane>::GetAdjacentLane(Toward toward, int offset)
{
    return const_cast<Lane *>(std::as_const(*this).template GetAdjacentLane<S>(toward, offset));
}

template <typename Lane>
template <auto Toward>
const Lane *Adjoinable<Lane>::GetAdjacentLane(Side side, int offset) const
{
    return IsInverse(side) ? GetAdjacentLane<!Default<Side>, Toward>(offset) : GetAdjacentLane<Default<Side>, Toward>(offset);
}

template <typename Lane>
template <auto Toward>
Lane *Adjoinable<Lane>::GetAdjacentLane(Side side, int offset)
{
    return const_cast<Lane *>(std::as_const(*this).template GetAdjacentLane<Toward>(side, offset));
}

template <typename Lane>
template <typename Toward>
Lane *Adjoinable<Lane>::GetAdjacentLane(Side side, Toward toward, int offset)
{
    return const_cast<Lane *>(std::as_const(*this).template GetAdjacentLane(side, toward, offset));
}

template <typename Lane>
template <Side S>
std::ptrdiff_t Adjoinable<Lane>::GetIndex() const
{
    if constexpr (S == Side::Left)
    {
        return static_cast<std::ptrdiff_t>(index) - static_cast<std::ptrdiff_t>(GetRoad().lanes.template size<Side::Right>());
    }
    else if constexpr (S == Side::Right)
    {
        return static_cast<std::ptrdiff_t>(GetRoad().lanes.template size<Side::Right>()) - static_cast<std::ptrdiff_t>(index + 1);
    }
    else
    {
        return GetIndex(GetSideOfRoad());
    }
}

template <typename Lane>
std::ptrdiff_t Adjoinable<Lane>::GetIndex(Side side) const
{
    return IsInverse(side) ? GetIndex<!Default<Side>>() : GetIndex<Default<Side>>();
}

template <typename Lane>
template <Side S>
ConstIterator<std::vector<Lane *>, S> Adjoinable<Lane>::GetIterator() const
{
    if constexpr (IsInverse(S))
    {
        return std::make_reverse_iterator(std::next(GetIterator<!S>()));
    }
    else
    {
        return std::next(GetRoad().lanes.begin(), index);
    }
}

template <typename Lane>
template <Side S, typename Predicate>
ConstIterator<std::vector<Lane *>, S> Adjoinable<Lane>::FindLaneIterator(Predicate &&pred) const
{
    auto it{std::next(GetIterator<S>())};
    for (; it != GetRoad().lanes.template end<S>(); ++it)
    {
        if (pred(**it))
        {
            return it;
        }
    }
    return it;
}
} // namespace osiql::detail
