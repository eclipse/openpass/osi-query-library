/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Component of a lane that allows it to access its driving direction and associated distances

#include <osi3/osi_logicallane.pb.h>

#include "OsiQueryLibrary/Component/Identifiable.h"
#include "OsiQueryLibrary/Point/Coordinates.h"
#include "OsiQueryLibrary/Types/Enum/Direction.h"
#include "OsiQueryLibrary/Types/Enum/Side.h"
#include "OsiQueryLibrary/Types/Enum/Traversal.h"

namespace osiql {
namespace detail {
struct Orientable : Identifiable<osi3::LogicalLane>
{
    using Identifiable<osi3::LogicalLane>::Identifiable;

    //! Returns the driving direction of traversing this lane in the given orientation. Downstream means
    //! traversal in increasing s-coordinate whereas Upstream means traversal in decreasing s-coordinate.
    //!
    //! \param Traversal Forward or Backward
    //! \return Direction
    Direction GetDirection(Traversal = Traversal::Forward) const;

    //! Returns what orientation the given direction corresponds to relative to this lane's direction.
    //!
    //! \param direction
    //! \return Traversal
    Traversal GetOrientation(Direction) const;

    //! Returns what side of the road this lane is on.
    //!
    //! \return Side Left if this lane's driving direction is Upstream, otherwise Right.
    Side GetSideOfRoad() const;

    //! Returns the s-coordinate corresponding to the centerline coordinates' distance
    //! from the start of this lane in its driving direction.
    //!
    //! \param UV Coordinates relative to this lane's centerline containing
    //! the distance from the start of this lane in its driving direction.
    //! \return double
    double GetS(const UV &) const;

    //! Returns the u-coordinate corresponding to the first encountered s-coordinate
    //! of the given input in the given traversal direction.
    //!
    //! \tparam Toward Direction or Traversal
    //! \tparam T The type of which the u-coordinate is determined.
    //! If a double, the value is treated as a s-coordinate
    //! \param Type Point, s-coordinate or s-coordinate range
    //! \return u-coordinate of the input
    template <auto Toward = Traversal::Forward, typename Type = double>
    double GetDistanceFromStart(const Type &) const;

    //! Returns the u-coordinate corresponding to the first encountered s-coordinate
    //! of the given input in the given traversal direction.
    //!
    //! \tparam Toward Direction or Traversal
    //! \tparam Type The type of which the u-coordinate is determined.
    //! If a double, the value is treated as a s-coordinate
    //! \param Type Point, s-coordinate or s-coordinate range
    //! \tparam Toward Instance of the traversal direction
    //! \return u-coordinate of the input
    template <typename Toward, typename Type = double>
    double GetDistanceFromStart(const Type &, Toward) const;

    template <auto Value = Traversal::Forward, typename Type = double>
    double GetDistanceToEnd(const Type &) const;

    template <typename Enum, typename Type>
    double GetDistanceToEnd(const Type &, Enum) const;

    //! Returns the s-coordinate difference from the start of this lane to its end
    //!
    //! \return Positive distance
    double GetLength() const;
};
} // namespace detail
} // namespace osiql
