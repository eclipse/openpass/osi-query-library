/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Connectable.h"

#include "Orientable.tpp"
#include "OsiQueryLibrary/Routing/Route.h"

namespace osiql::detail {
template <typename Lane>
std::string_view Connectable<Lane>::GetRoadId() const
{
    if (GetHandle().source_reference().empty() || GetHandle().source_reference(0).identifier().empty())
    {
        return UNDEFINED_ROAD_ID;
    }
    return GetHandle().source_reference(0).identifier(0);
}

template <typename Lane>
template <auto Toward, typename... Pred>
decltype(auto) Connectable<Lane>::GetConnectedLanes(Pred &&...pred) const
{
    if constexpr (std::is_same_v<decltype(Toward), Direction>)
    {
        static_assert(Toward != Direction::Bidirectional, "Not supported");
        return GetConnectedLanes(GetDirection() * Toward, std::forward<Pred>(pred)...);
    }
    else if constexpr (std::is_same_v<decltype(Toward), Traversal>)
    {
        static_assert(Toward != Traversal::Any, "Not supported");
        if constexpr (IsInverse(Toward))
        {
            return (incomingLanes);
        }
        else
        {
            return (outgoingLanes);
        }
    }
    else
    {
        static_assert(always_false<decltype(Toward)>, "First template parameter must be a Direction or Traversal");
    }
}

template <typename Lane>
template <auto Toward, typename... Pred>
decltype(auto) Connectable<Lane>::GetConnectedLanes(Pred &&...pred)
{
    if constexpr (std::is_same_v<decltype(Toward), Direction>)
    {
        return GetConnectedLanes(GetDirection() * Toward, std::forward<Pred>(pred)...);
    }
    else if constexpr (std::is_same_v<decltype(Toward), Traversal>)
    {
        if constexpr (IsInverse(Toward))
        {
            return (incomingLanes);
        }
        else
        {
            return (outgoingLanes);
        }
    }
    else
    {
        static_assert(always_false<decltype(Toward)>, "First template parameter must be a Direction or Traversal");
    }
}

template <typename Lane>
template <typename Toward, typename... Pred>
decltype(auto) Connectable<Lane>::GetConnectedLanes(Toward toward, Pred &&...pred) const
{
    return IsInverse(toward) ? GetConnectedLanes<!Default<Toward>>(std::forward<Pred>(pred)...) : GetConnectedLanes<Default<Toward>>(std::forward<Pred>(pred)...);
}

template <typename Lane>
template <typename Toward, typename... Pred>
decltype(auto) Connectable<Lane>::GetConnectedLanes(Toward toward, Pred &&...pred)
{
    return IsInverse(toward) ? GetConnectedLanes<!Default<Toward>>(std::forward<Pred>(pred)...) : GetConnectedLanes<Default<Toward>>(std::forward<Pred>(pred)...);
}

template <typename Lane>
template <auto Toward, typename... Pred>
const Lane *Connectable<Lane>::GetConnectedLane(Pred &&...pred) const
{
    const auto &lanes{GetConnectedLanes<Toward>()};
    if constexpr (sizeof...(Pred) == 0)
    {
        return lanes.empty() ? nullptr : lanes.front();
    }
    else if constexpr (sizeof...(Pred) == 1)
    {
        const auto it{std::find_if(lanes.begin(), lanes.end(), std::forward<Pred>(pred)...)};
        return it == lanes.end() ? nullptr : *it;
    }
    else
    {
        const auto it{std::find_if(lanes.begin(), lanes.end(), [&](const Lane *lane) {
            return (pred(lane) && ...);
        })};
        return it == lanes.end() ? nullptr : *it;
    }
}

template <typename Lane>
template <auto Toward, typename... Pred>
Lane *Connectable<Lane>::GetConnectedLane(Pred &&...pred)
{
    return const_cast<Lane *>(std::as_const(*this).template GetConnectedLane<Toward>(std::forward<Pred>(pred)...));
}

template <typename Lane>
template <typename Toward, typename... Pred>
const Lane *Connectable<Lane>::GetConnectedLane(Toward toward, Pred &&...pred) const
{
    return IsInverse(toward) ? GetConnectedLane<!Default<Toward>>(std::forward<Pred>(pred)...)
                             : GetConnectedLane<Default<Toward>>(std::forward<Pred>(pred)...);
}

template <typename Lane>
template <typename Toward, typename... Pred>
Lane *Connectable<Lane>::GetConnectedLane(Toward toward, Pred &&...pred)
{
    return IsInverse(toward) ? GetConnectedLane<!Default<Toward>>(std::forward<Pred>(pred)...)
                             : GetConnectedLane<Default<Toward>>(std::forward<Pred>(pred)...);
}

namespace detail {
template <Traversal Toward, Traversal T, typename It>
void ConstructChain(std::deque<const Lane *> &chain, It node, const Route<T> &route)
{
    if constexpr (Toward == Traversal::Any)
    {
        ConstructChain<Traversal::Forward>(chain, node, route);
        ConstructChain<Traversal::Backward>(chain, node, route);
    }
    else if constexpr (Toward == Traversal::Backward && !osiql::is_reverse_iterator<It>)
    {
        return ConstructChain<Traversal::Backward>(chain, std::prev(std::make_reverse_iterator(node)), route);
    }
    else
    {
        while (++node != osiql::end<Toward>(route))
        {
            const Lane *nextLane{back<Toward>(chain)->template GetConnectedLane<Toward * T>(Matches<Road>{*node})};
            if (!nextLane)
            {
                break;
            }
            *std::conditional_t<Toward == Traversal::Backward, std::front_insert_iterator<std::deque<const Lane *>>, std::back_insert_iterator<std::deque<const Lane *>>>{chain} = nextLane;
        }
    }
}
} // namespace detail

template <typename Lane>
template <auto Toward, Traversal T>
std::deque<const Lane *> Connectable<Lane>::GetChain(const Route<T> &route) const
{
    if constexpr (std::is_same_v<decltype(Toward), Direction>)
    {
        return GetChain(GetDirection() * Toward, route);
    }
    else if constexpr (std::is_same_v<decltype(Toward), Traversal>)
    {
        const Lane *self{static_cast<const Lane *>(this)};
        std::deque<const Lane *> chain{self};
        if (auto node{route.TraverseUntil(Matches<Road>{self})}; node != route.end())
        {
            detail::ConstructChain<Toward>(chain, node, route);
        }
        return chain;
    }
    else
    {
        static_assert(always_false<decltype(Toward)>, "Toward must be Direction or Traversal");
    }
}

template <typename Lane>
template <typename Toward, Traversal T>
std::deque<const Lane *> Connectable<Lane>::GetChain(Toward toward, const Route<T> &route) const
{
    if constexpr (std::is_same_v<Toward, Direction>)
    {
        if (toward == Direction::Bidirectional)
        {
            return GetChain<Traversal::Any>(route);
        }
        return IsInverse(toward) ? GetChain<Direction::Upstream>(route) : GetChain<Direction::Downstream>(route);
    }
    else if constexpr (std::is_same_v<Toward, Traversal>)
    {
        if (toward == Traversal::Any)
        {
            return GetChain<Traversal::Any>(route);
        }
        return IsInverse(toward) ? GetChain<Traversal::Backward>(route) : GetChain<Traversal::Forward>(route);
    }
    else
    {
        static_assert(always_false<Toward>, "Toward must be Direction or Traversal");
    }
}
} // namespace osiql::detail
