/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "BoundaryEnclosure.h"

#include <numeric>

#include "Adjoinable.tpp"
#include "OsiQueryLibrary/Point/XY.tpp"
#include "OsiQueryLibrary/Street/BoundaryChain.tpp"

namespace osiql::detail {
template <typename Lane>
template <Side S, auto RelativeTo>
const BoundaryChain &BoundaryEnclosure<Lane>::GetBoundaries() const
{
    if constexpr (std::is_same_v<Direction, decltype(RelativeTo)>)
    {
        if constexpr (RelativeTo == Direction::Upstream)
        {
            return GetBoundaries<!S, Direction::Downstream>();
        }
        else if constexpr (IsInverse(S))
        {
            return static_cast<const Lane *>(this)->GetRoad().boundaries[static_cast<const Lane *>(this)->index];
        }
        else
        {
            return static_cast<const Lane *>(this)->GetRoad().boundaries[static_cast<const Lane *>(this)->index + 1];
        }
    }
    else if constexpr (std::is_same_v<Traversal, decltype(RelativeTo)>)
    {
        return GetBoundaries<S>(this->GetDirection(RelativeTo));
    }
    else
    {
        static_assert(always_false<decltype(RelativeTo)>, "Not supported");
    }
}

template <typename Lane>
template <Side S, auto RelativeTo>
BoundaryChain &BoundaryEnclosure<Lane>::GetBoundaries()
{
    return const_cast<BoundaryChain &>(std::as_const(*this).template GetBoundaries<S, RelativeTo>());
}

template <typename Lane>
template <Side S, typename RelativeTo>
const BoundaryChain &BoundaryEnclosure<Lane>::GetBoundaries(RelativeTo value) const
{
    if constexpr (std::is_same_v<RelativeTo, Direction>)
    {
        return IsInverse(value) ? GetBoundaries<S, Direction::Upstream>() : GetBoundaries<S, Direction::Downstream>();
    }
    else if constexpr (std::is_same_v<RelativeTo, Traversal>)
    {
        return IsInverse(value) ? GetBoundaries<S, Traversal::Backward>() : GetBoundaries<S, Traversal::Forward>();
    }
    else
    {
        static_assert(always_false<RelativeTo>, "Not supported");
    }
}

template <typename Lane>
template <Side S, typename RelativeTo>
BoundaryChain &BoundaryEnclosure<Lane>::GetBoundaries(RelativeTo value)
{
    return const_cast<BoundaryChain &>(std::as_const(*this).template GetBoundaries<S>(value));
}

template <typename Lane>
template <auto RelativeTo>
const BoundaryChain &BoundaryEnclosure<Lane>::GetBoundaries(Side side) const
{
    return IsInverse(side) ? GetBoundaries<!Default<Side>, RelativeTo>() : GetBoundaries<Default<Side>, RelativeTo>();
}

template <typename Lane>
template <auto RelativeTo>
BoundaryChain &BoundaryEnclosure<Lane>::GetBoundaries(Side side)
{
    return const_cast<BoundaryChain &>(std::as_const(*this).template GetBoundaries<RelativeTo>(side));
}

template <typename Lane>
template <typename RelativeTo>
const BoundaryChain &BoundaryEnclosure<Lane>::GetBoundaries(Side side, RelativeTo value) const
{
    return IsInverse(value) ? GetBoundaries<!Default<RelativeTo>>(side) : GetBoundaries<Default<RelativeTo>>(side);
}

template <typename Lane>
template <typename RelativeTo>
BoundaryChain &BoundaryEnclosure<Lane>::GetBoundaries(Side side, RelativeTo value)
{
    return const_cast<BoundaryChain &>(std::as_const(*this).GetBoundaries(side, value));
}

template <typename Lane>
template <Side S, Direction D>
ConstIterator<std::vector<Boundary *>, D> BoundaryEnclosure<Lane>::FindBoundary(double s) const
{
    return GetBoundaries<S, D>().template FindBoundary<D>(s);
}

template <typename Lane>
template <Side S, Direction D>
Iterator<std::vector<Boundary *>, D> BoundaryEnclosure<Lane>::FindBoundary(double s)
{
    return GetBoundaries<S, D>().template FindBoundary<D>(s);
}

template <typename Lane>
template <Direction D>
ConstIterator<std::vector<Boundary *>, D> BoundaryEnclosure<Lane>::FindBoundary(Side side, double s) const
{
    return IsInverse(side) ? FindBoundary<!Default<Side>, D>(s) : FindBoundary<Default<Side>, D>(s);
}

template <typename Lane>
template <Direction D>
Iterator<std::vector<Boundary *>, D> BoundaryEnclosure<Lane>::FindBoundary(Side side, double s)
{
    return IsInverse(side) ? FindBoundary<!Default<Side>, D>(s) : FindBoundary<Default<Side>, D>(s);
}

template <typename Lane>
template <Side S, auto RelativeTo>
const Boundary &BoundaryEnclosure<Lane>::GetBoundary(double s) const
{
    if constexpr (std::is_same_v<decltype(RelativeTo), Traversal>)
    {
        return GetBoundary<S>(this->GetDirection(RelativeTo), s);
    }
    else if constexpr (std::is_same_v<decltype(RelativeTo), Direction>)
    {
        return GetBoundaries<S, RelativeTo>().template GetBoundary<RelativeTo>(s);
    }
    else
    {
        static_assert(always_false<decltype(RelativeTo)>, "Not supported");
    }
}

template <typename Lane>
template <Side S, auto RelativeTo>
Boundary &BoundaryEnclosure<Lane>::GetBoundary(double s)
{
    return const_cast<Boundary &>(std::as_const(*this).template GetBoundary<S, RelativeTo>(s));
}

template <typename Lane>
template <auto X>
const Boundary &BoundaryEnclosure<Lane>::GetBoundary(Side side, double s) const
{
    return IsInverse(side) ? GetBoundary<!Default<Side>, X>(s) : GetBoundary<Default<Side>, X>(s);
}

template <typename Lane>
template <auto RelativeTo>
Boundary &BoundaryEnclosure<Lane>::GetBoundary(Side side, double s)
{
    return IsInverse(side) ? GetBoundary<!Default<Side>, RelativeTo>(s) : GetBoundary<Default<Side>, RelativeTo>(s);
}

template <typename Lane>
template <Side S, typename RelativeTo>
const Boundary &BoundaryEnclosure<Lane>::GetBoundary(RelativeTo value, double s) const
{
    return IsInverse(value) ? GetBoundary<S, !Default<RelativeTo>>(s) : GetBoundary<S, Default<RelativeTo>>(s);
}

template <typename Lane>
template <Side S, typename RelativeTo>
Boundary &BoundaryEnclosure<Lane>::GetBoundary(RelativeTo value, double s)
{
    return IsInverse(value) ? GetBoundary<S, !Default<RelativeTo>>(s) : GetBoundary<S, Default<RelativeTo>>(s);
}

template <typename Lane>
template <typename RelativeTo>
const Boundary &BoundaryEnclosure<Lane>::GetBoundary(Side side, RelativeTo value, double s) const
{
    return IsInverse(side) ? GetBoundary<!Default<Side>>(value, s) : GetBoundary<Default<Side>>(value, s);
}

template <typename Lane>
template <typename RelativeTo>
Boundary &BoundaryEnclosure<Lane>::GetBoundary(Side side, RelativeTo value, double s)
{
    return IsInverse(value) ? GetBoundary<!Default<RelativeTo>>(side, s) : GetBoundary<Default<RelativeTo>>(side, s);
}

template <typename Lane>
template <typename Type>
bool BoundaryEnclosure<Lane>::Contains(const Type &point) const
{
    if constexpr (std::is_fundamental_v<Type>)
    {
        return osiql::Contains<Direction>(*this, point);
    }
    else if constexpr (Are<S, Type>::template in<Type>)
    {
        if (!Contains<Direction>(*this, point))
        {
            return false;
        }
        const double s{extract<Direction::Downstream>(point)};
        return GetBoundaries<Side::Right>().FindBoundary(s)->GetT(s) <= s // clang-format off
            && GetBoundaries<Side::Left>().FindBoundary(s)->GetT(s) >= s; // clang-format on
    }
    else if constexpr (Dimensions<Type>)
    {
        return point.IsWithin(this->GetShape());
    }
}

template <typename Lane>
template <auto Towards>
double BoundaryEnclosure<Lane>::GetAngle(double s) const
{
    if constexpr (std::is_same_v<decltype(Towards), Traversal>)
    {
        return GetAngle(this->GetDirection(Towards), s);
    }
    else if constexpr (std::is_same_v<decltype(Towards), Direction>)
    {
        const double leftLaneAngle{GetBoundary<Side::Left, Towards>(s).template GetAngle<Towards>(s)};
        const double rightLaneAngle{GetBoundary<Side::Right, Towards>(s).template GetAngle<Towards>(s)};
        return 0.5 * leftLaneAngle + 0.5 * rightLaneAngle; // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    }
    else
    {
        static_assert(always_false<decltype(Towards)>, "Not supported");
    }
}

template <typename Lane>
template <typename Towards>
double BoundaryEnclosure<Lane>::GetAngle(Towards t, double s) const
{
    return IsInverse(t) ? GetAngle<!Default<Towards>>(s) : GetAngle<Default<Towards>>(s);
}

template <typename Lane>
template <auto Towards>
double BoundaryEnclosure<Lane>::GetCurvature(double s) const
{
    if constexpr (std::is_same_v<decltype(Towards), Traversal>)
    {
        return GetCurvature(this->GetDirection(Towards), s);
    }
    else if constexpr (std::is_same_v<decltype(Towards), Direction>)
    {
        const double leftCurvature{GetBoundary<Side::Left, Towards>(s).template GetCurvature<Towards>(s)};
        const double rightCurvature{GetBoundary<Side::Right, Towards>(s).template GetCurvature<Towards>(s)};
        return 0.5 * leftCurvature + 0.5 * rightCurvature; // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    }
    else
    {
        static_assert(always_false<decltype(Towards)>, "Not supported");
    }
}

template <typename Lane>
template <typename Orientation>
double BoundaryEnclosure<Lane>::GetCurvature(Orientation toward, double s) const
{
    return IsInverse(toward) ? GetCurvature<!Default<Orientation>>(s) : GetCurvature<Default<Orientation>>(s);
}

template <typename Lane>
double BoundaryEnclosure<Lane>::GetCenterlineT(double s) const
{
    const double leftT{GetBoundary<Side::Left>(s).GetT(s)};
    const double rightT{GetBoundary<Side::Right>(s).GetT(s)};
    return 0.5 * leftT + 0.5 * rightT; // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
}

template <typename Lane>
UV BoundaryEnclosure<Lane>::GetUV(const ST &coordinates) const
{
    const double u{this->GetDistanceFromStart(coordinates)};
    const double centerlineT{GetCenterlineT(coordinates.s)};
    return {u, Difference(centerlineT, coordinates, !this->GetSideOfRoad())};
}

template <typename Lane>
ST BoundaryEnclosure<Lane>::GetST(const UV &coordinates) const
{
    const double s{this->GetS(coordinates)};
    return {s, Increase(GetCenterlineT(s), coordinates.v, !this->GetSideOfRoad())};
}

template <typename Lane>
template <auto Value>
double BoundaryEnclosure<Lane>::GetV(const ST &point) const
{
    if constexpr (std::is_same_v<decltype(Value), Traversal>)
    {
        if constexpr (IsInverse(Value))
        {
            return Difference(point.t, GetCenterlineT(point.s), !this->GetSideOfRoad());
        }
        else
        {
            return Difference(point.t, GetCenterlineT(point.s), this->GetSideOfRoad());
        }
    }
    else if constexpr (std::is_same_v<decltype(Value), Direction>)
    {
        return Difference(point.t, GetCenterlineT(point.s), this->GetDirection() == Value ? this->GetSideOfRoad() : !this->GetSideOfRoad());
    }
    else
    {
        static_assert(always_false<decltype(Value)>, "Not supported");
    }
}

template <typename Lane>
template <typename It>
Interval<double> BoundaryEnclosure<Lane>::GetV(It first, It pastLast) const
{
    std::vector<double> distances{};
    distances.reserve(static_cast<size_t>(std::distance(first, pastLast)));
    if (IsInverse(this->GetSideOfRoad())) // NOLINT(bugprone-branch-clone)
    {
        std::transform(first, pastLast, std::back_inserter(distances), [this](const auto &point) {
            return Difference<Side::Right>(extract<Side::Left>(point), this->GetCenterlineT(extract<Direction::Downstream>(point)));
        });
    }
    else
    {
        std::transform(first, pastLast, std::back_inserter(distances), [this](const auto &point) {
            return Difference<Side::Left>(extract<Side::Left>(point), this->GetCenterlineT(extract<Direction::Downstream>(point)));
        });
    }
    const auto [min, max]{std::minmax_element(distances.begin(), distances.end())};
    return {*min, *max};
}

template <typename Lane>
void BoundaryEnclosure<Lane>::UpdateShapeAndBounds()
{
    // NOTE: clang-diagnostic does not find the declaration of GetShape() & GetBounds() without "this->"
    assert(this->GetShape().empty());
    const auto rightPoints{GetBoundaries<Side::Right, Direction::Downstream>().GetPoints()};
    const auto leftPoints{GetBoundaries<Side::Left, Direction::Downstream>().GetPoints()};
    // Drop a point if the boundaries merge together at the start or end of the lane:
    auto beginRight{leftPoints.front() == rightPoints.front() ? std::next(rightPoints.cbegin()) : rightPoints.cbegin()};
    auto beginLeft{leftPoints.back() == rightPoints.back() ? std::next(leftPoints.crbegin()) : leftPoints.crbegin()};
    this->GetShape().reserve(static_cast<size_t>(std::distance(beginRight, rightPoints.end()) + std::distance(beginLeft, leftPoints.rend())));
    this->GetShape().insert(this->GetShape().end(), std::make_move_iterator(beginRight), std::make_move_iterator(rightPoints.end()));
    this->GetShape().insert(this->GetShape().end(), std::make_move_iterator(beginLeft), std::make_move_iterator(leftPoints.rend()));
    boost::geometry::envelope(this->GetShape(), this->GetBounds());
}
} // namespace osiql::detail
