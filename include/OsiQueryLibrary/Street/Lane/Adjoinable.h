/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Connectable.h"
#include "OsiQueryLibrary/Component/Iterable.h"
#include "OsiQueryLibrary/Types/Enum.h"
#include "OsiQueryLibrary/Utility/Has.h"

namespace osiql {
struct Road;
namespace detail {
template <typename Lane>
struct Adjoinable : Connectable<Lane>
{
    using Connectable<Lane>::Connectable;

    //! Returns the road this lane is a part of.
    //!
    //! \return const Road&
    constexpr const Road &GetRoad() const;

    //! Returns the road this lane is a part of.
    //!
    //! \return Road&
    constexpr Road &GetRoad();

    //! Assigns the road that this lane is a part of.
    //!
    //! \param road The road that this lane is a part of.
    void SetRoad(Road &road);

    //! Returns the side of the road this lane is on.
    //!
    //! \return Left or Right
    Side GetSideOfRoad() const;

    //! Returns the lane that is the given number of lane changes
    //! away from this lane on the given side relative to the given direction.
    //!
    //! \tparam Side Left or Right relative to the given direction
    //! \tparam Toward Direction or Traversal
    //! \param offset Number of lane changes in the given direction
    //! \return The lane that is the given number of lane changes away from this lane or nullptr if no such lane exists.
    template <Side, auto Toward = Traversal::Forward>
    const Lane *GetAdjacentLane(int offset = 1) const;

    //! Returns the lane that is the given number of lane changes
    //! away from this lane on the given side relative to the given direction.
    //!
    //! \tparam Side Left or Right relative to the given direction
    //! \tparam Toward Direction or Traversal
    //! \param offset Number of lane changes in the given direction
    //! \return The lane that is the given number of lane changes away from this lane or nullptr if no such lane exists.
    template <Side, auto Toward = Traversal::Forward>
    Lane *GetAdjacentLane(int offset = 1);

    //! Returns the lane that is the given number of lane changes
    //! away from this lane on the given side relative to the given direction.
    //!
    //! \tparam Side Left or Right relative to the given direction
    //! \tparam Toward Direction or Traversal
    //! \param Toward The direction the given side is relative to
    //! \param offset Number of lane changes in the given direction
    //! \return The lane that is the given number of lane changes away from this lane or nullptr if no such lane exists.
    template <Side, typename Toward>
    const Lane *GetAdjacentLane(Toward, int offset = 1) const;

    //! Returns the lane that is the given number of lane changes
    //! away from this lane on the given side relative to the given direction.
    //!
    //! \tparam Side Left or Right relative to the given direction
    //! \tparam Toward Direction or Traversal
    //! \param Toward The direction the given side is relative to
    //! \param offset Number of lane changes in the given direction
    //! \return The lane that is the given number of lane changes away from this lane or nullptr if no such lane exists.
    template <Side, typename Toward>
    Lane *GetAdjacentLane(Toward, int offset = 1);

    //! Returns the lane that is the given number of lane changes
    //! away from this lane on the given side relative to the given direction.
    //!
    //! \tparam Side Left or Right relative to the given direction
    //! \tparam Toward Direction or Traversal
    //! \param Side The side towards which the lane changes are performed
    //! \param offset Number of lane changes in the given direction
    //! \return The lane that is the given number of lane changes away from this lane or nullptr if no such lane exists.
    template <auto Toward = Traversal::Forward>
    const Lane *GetAdjacentLane(Side, int offset = 1) const;

    //! Returns the lane that is the given number of lane changes
    //! away from this lane on the given side relative to the given direction.
    //!
    //! \tparam Side Left or Right relative to the given direction
    //! \tparam Toward Direction or Traversal
    //! \param Side The side towards which the lane changes are performed
    //! \param offset Number of lane changes in the given direction
    //! \return The lane that is the given number of lane changes away from this lane or nullptr if no such lane exists.
    template <auto Toward = Traversal::Forward>
    Lane *GetAdjacentLane(Side, int offset = 1);

    //! Returns the lane that is the given number of lane changes
    //! away from this lane on the given side relative to the given direction.
    //!
    //! \tparam Side Left or Right relative to the given direction
    //! \tparam Toward Direction or Traversal
    //! \param Side The side towards which the lane changes are performed
    //! \param Toward The direction the given side is relative to
    //! \param offset Number of lane changes in the given direction
    //! \return The lane that is the given number of lane changes away from this lane or nullptr if no such lane exists.
    template <typename Toward, OSIQL_REQUIRES(std::is_enum_v<Toward>)>
    const Lane *GetAdjacentLane(Side side, Toward toward, int offset = 1) const
    {
        return IsInverse(side) ? GetAdjacentLane<!Default<Side>>(toward, offset) : GetAdjacentLane<Default<Side>>(toward, offset);
    }

    //! Returns the lane that is the given number of lane changes
    //! away from this lane on the given side relative to the given direction.
    //!
    //! \tparam Side Left or Right relative to the given direction
    //! \tparam Toward Direction or Traversal
    //! \param Side The side towards which the lane changes are performed
    //! \param Toward The direction the given side is relative to
    //! \param offset Number of lane changes in the given direction
    //! \return The lane that is the given number of lane changes away from this lane or nullptr if no such lane exists.
    template <typename Toward>
    Lane *GetAdjacentLane(Side, Toward, int offset = 1);

    //! Returns how many lanes to the left of this lane are on the same side of the road as this lane.
    //!
    //! \tparam Side Left or Right
    //! \return Number of lanes to the left of this lane on the same side of the road as this lane.
    template <Side = Side::Both>
    std::ptrdiff_t GetIndex() const;

    //! Returns how many lanes are between this lane and the nearest lane on the other side of this lane's road
    //!
    //! \param Side Left or Right
    //! \return std::ptrdiff_t
    std::ptrdiff_t GetIndex(Side) const;

    //! Returns an iterator that increments towards the given direction
    //! (relative to Direction::Downstream) pointing to this lane.
    //!
    //! \tparam S Left or Right
    //! \return Iterator pointing to this lane
    template <Side S>
    ConstIterator<std::vector<Lane *>, S> GetIterator() const;

    //! Returns an iterator to an adjacent lane on the side of this lane
    //! (relative to Direction::Downstream) that satisfies the given predicate.
    //!
    //! \tparam S Left or Right
    //! \tparam Predicate Unary invocable with signature bool(const Lane&)
    //! \return Iterator pointing past the last adjacent lane if no lane satisfies the predicate
    template <Side S, typename Predicate>
    ConstIterator<std::vector<Lane *>, S> FindLaneIterator(Predicate &&) const;

    //! \brief Number of lanes between this lane and the other side of the road.
    size_t index{0};

private:
    Road *road{nullptr};
};

template <typename Lane>
constexpr const Road &Adjoinable<Lane>::GetRoad() const
{
    assert(road != nullptr);
    return *road;
}

template <typename Lane>
constexpr Road &Adjoinable<Lane>::GetRoad()
{
    assert(road != nullptr);
    return *road;
}
} // namespace detail
} // namespace osiql

#include "OsiQueryLibrary/Utility/Has/Lane.h"
