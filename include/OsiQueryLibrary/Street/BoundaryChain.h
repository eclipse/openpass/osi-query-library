/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Component/Iterable.h"
#include "OsiQueryLibrary/Street/Boundary.h"

namespace osiql {
//! \brief A contiguous chain of boundaries in which any successor boundary
//! has the same first point as the last point of the previous boundary.
struct BoundaryChain : Iterable<std::vector<Boundary *>, BoundaryChain>
{
    using Base = Iterable<std::vector<Boundary *>, BoundaryChain>;

    using Base::begin;
    ConstIterator<std::vector<Boundary *>> begin() const;
    Iterator<std::vector<Boundary *>> begin();

    using Base::end;
    ConstIterator<std::vector<Boundary *>> end() const;
    Iterator<std::vector<Boundary *>> end();

    //! Combines and converts all points of the boundaries in this chain to a single chain of xy-coordinates.
    //! The first point of every boundary aside from the first is skipped, as it is identical
    //! to the last point of the previous boundary.
    //!
    //! \return Vector of global points
    std::vector<XY> GetPoints() const;

    //! Returns an iterator to the lane boundary closest to the given s-coordinate.
    //! \note In case of two equally close boundaries, the latter in the given direction is returned.
    //!
    //! \tparam D Downstream or Upstream
    //! \param s s-coordinate closest to the returned boundary
    //! \return Iterator to the closest boundary
    template <Direction D = Direction::Downstream>
    ConstIterator<std::vector<Boundary *>, D> FindBoundary(double s) const;

    //! Returns an iterator to the lane boundary closest to the given s-coordinate.
    //! \note In case of two equally close boundaries, the latter in the given direction is returned.
    //!
    //! \tparam D Downstream or Upstream
    //! \param s s-coordinate closest to the returned boundary
    //! \return Iterator to the closest boundary
    template <Direction D = Direction::Downstream>
    Iterator<std::vector<Boundary *>, D> FindBoundary(double s);

    //! Returns the lane boundary closest to the given s-coordinate.
    //! \note In case of two equally close boundaries, the latter in the given direction is returned.
    //!
    //! \tparam Direction Downstream or Upstream
    //! \param s s-coordinate closest to the returned boundary
    //! \return Boundary closest to the given s-coordinate
    template <Direction = Direction::Downstream>
    const Boundary &GetBoundary(double s) const;

    //! Returns the lane boundary closest to the given s-coordinate.
    //! \note In case of two equally close boundaries, the latter in the given direction is returned.
    //!
    //! \tparam Direction Downstream or Upstream
    //! \param s s-coordinate closest to the returned boundary
    //! \return Boundary closest to the given s-coordinate
    template <Direction = Direction::Downstream>
    Boundary &GetBoundary(double s);

    //! Returns the lane boundary closest to the given s-coordinate.
    //! \note In case of two equally close boundaries, the latter in the given direction is returned.
    //!
    //! \param Direction Downstream or Upstream
    //! \param s s-coordinate closest to the returned boundary
    //! \return Boundary closest to the given s-coordinate
    const Boundary &GetBoundary(Direction, double s) const;

    //! Returns the lane boundary closest to the given s-coordinate.
    //! \note In case of two equally close boundaries, the latter in the given direction is returned.
    //!
    //! \tparam Direction Downstream or Upstream
    //! \param s s-coordinate closest to the returned boundary
    //! \return Boundary closest to the given s-coordinate
    Boundary &GetBoundary(Direction, double s);

    //! Returns the signed difference in t-coordinate from the given point to the nearest point on this
    //! boundary chain. The distance is negative if the given point is to the right of the boundary chain.
    //!
    //! \tparam Direction Downstream or Upstream. Affects what is considered left and right
    //! \param ST st-coordinate pair
    //! \return Signed t-coordinate difference from the point to the nearest boundary of this chain
    template <Direction = Direction::Downstream>
    double GetLateralDistance(const ST &) const;

    //! Returns the signed difference in t-coordinate from the given point to the nearest point on this
    //! boundary chain. The distance is negative if the given point is to the right of the boundary chain.
    //!
    //! \param Direction Downstream or Upstream. Affects what is considered left and right
    //! \param ST st-coordinate pair
    //! \return Signed t-coordinate difference from the point to the nearest boundary of this chain
    double GetLateralDistance(Direction, const ST &) const;

    //! Returns the signed difference in t-coordinate from the given point to the nearest point on this
    //! boundary chain. The distance is negative if the given point is to the right of the boundary chain.
    //!
    //! \tparam Direction Downstream or Upstream. Affects what is considered left and right
    //! \tparam Coordinates Iterable container of st-coordinate pairs
    //! \param Coordinates If empty, a default-constructed interval is returned
    //! \return Signed t-coordinate difference from the point to the nearest boundary of this chain
    template <Direction = Direction::Downstream, typename Coordinates = std::vector<ST>>
    Interval<double> GetLateralDistances(const Coordinates &) const;

    //! Returns the signed difference in t-coordinate from the given point to the nearest point on this
    //! boundary chain. The distance is negative if the given point is to the right of the boundary chain.
    //!
    //! \tparam Coordinates Iterable container of st-coordinate pairs
    //! \param Direction Downstream or Upstream. Affects what is considered left and right
    //! \param Coordinates If empty, a default-constructed interval is returned
    //! \return Signed t-coordinate difference from the point to the nearest boundary of this chain
    template <typename Coordinates = std::vector<ST>>
    Interval<double> GetLateralDistances(Direction, const Coordinates &) const;

    //! Returns the signed difference in t-coordinate from the given point to the nearest point on this
    //! boundary chain. The distance is negative if the given point is to the right of the boundary chain.
    //!
    //! \tparam Direction Downstream or Upstream. Affects what is considered left and right
    //! \tparam It Iterator pointing to an st-coordinate pair
    //! \param begin Iterator to the first st-coordinate pair
    //! \param end Iterator directly past the last st-coordinate pair
    //! \return Signed t-coordinate difference from the point to the nearest boundary of this chain
    template <Direction = Direction::Downstream, typename It = void>
    Interval<double> GetLateralDistances(It begin, It end) const;

    //! Returns the signed difference in t-coordinate from the given point to the nearest point on this
    //! boundary chain. The distance is negative if the given point is to the right of the boundary chain.
    //!
    //! \tparam It Iterator pointing to an st-coordinate pair
    //! \param Direction Downstream or Upstream. Affects what is considered left and right
    //! \param begin Iterator to the first st-coordinate pair
    //! \param end Iterator directly past the last st-coordinate pair
    //! \return Signed t-coordinate difference from the point to the nearest boundary of this chain
    template <typename It>
    Interval<double> GetLateralDistances(Direction, It begin, It end) const;

    //! \brief chain of boundaries with ascending s-coordinates
    std::vector<Boundary *> container;
};

std::ostream &operator<<(std::ostream &, const BoundaryChain &);
} // namespace osiql
