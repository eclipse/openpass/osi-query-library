/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Road.h"

#include "OsiQueryLibrary/Component/Overlapable.tpp"
#include "OsiQueryLibrary/Point/Point.tpp"
#include "OsiQueryLibrary/Point/Pose.tpp"
#include "OsiQueryLibrary/Trait/Localization.h"
#include "OsiQueryLibrary/Types/Enum.tpp"
#include "OsiQueryLibrary/Types/Overlap.tpp"
#include "OsiQueryLibrary/Utility/Has/Road.tpp"
#include "ReferenceLine.tpp"
#include "Road/BoundaryChains.tpp"
#include "Road/Lanes.tpp"

namespace osiql {
template <Direction D>
constexpr double Road::GetS() const
{
    if constexpr (D == Direction::Upstream)
    {
        return extract<D>(this->lanes.front());
    }
    else
    {
        return extract<D>(this->lanes.back());
    }
}

template <typename Object, Side Si, Selection Se, typename Pred, typename Transform>
std::vector<std::invoke_result_t<Transform, const Placement<Object> &>> Road::GetAll(Pred &&pred, Transform &&transform) const
{
    std::vector<std::invoke_result_t<Transform, const Placement<Object> &>> result;
    InsertAll<Object, Si, Se>(std::back_inserter(result), std::forward<Pred>(pred), std::forward<Transform>(transform));
    return result;
}

template <typename Object, Side Si, Selection Se, typename Pred, typename Transform, typename OutputIt>
OutputIt Road::InsertAll(OutputIt output, Pred &&pred, Transform &&transform) const
{
    if constexpr (Se == Selection::Each)
    {
        std::for_each(lanes.first<Si>(), lanes.end<Si>(), [&](const Lane *lane) {
            output = lane->InsertAll<Object>(
                output,
                std::forward<Pred>(pred),
                std::forward<Transform>(transform)
            );
        });
    }
    else if constexpr (Se == Selection::First)
    {
        std::set<Id> pool;
        std::for_each(lanes.first<Si>(), lanes.end<Si>(), [&](const Lane *lane) {
            output = lane->InsertAll<Object>(
                output,
                [&](const auto &entry) { return pool.emplace(get<Id>(entry)).second && pred(entry); },
                std::forward<Transform>(transform)
            );
        });
    }
    else if constexpr (Se == Selection::Last)
    {
        std::set<Id> pool;
        std::for_each(lanes.rbegin<Si>(), lanes.first<!Si>(), [&](const Lane *lane) {
            output = lane->InsertAll<Object>(
                output,
                [&](const auto &entry) { return pool.emplace(get<Id>(entry)).second && pred(entry); },
                std::forward<Transform>(transform)
            );
        });
    }
    return output;
}

template <typename Object, Side Si, Selection Se, typename Pred, typename Comp, typename Transform>
std::vector<std::invoke_result_t<Transform, const Placement<Object> &>> Road::Find(size_t n, Pred &&pred, Comp &&comp, Transform &&transform) const
{
    std::vector<std::invoke_result_t<Transform, const Placement<Object> &>> result;
    Insert<Object, Si, Se>(n, std::forward<Pred>(pred), std::forward<Comp>(comp), std::forward<Transform>(transform));
    return result;
}

template <typename Object, Side Si, Selection Se, typename Pred, typename Comp, typename Transform, typename OutputIt>
OutputIt Road::Insert(OutputIt output, size_t n, Pred &&pred, Comp &&comp, Transform &&transform) const
{
    decltype(auto) all{GetAll<Object, Si, Se>(std::forward<Pred>(pred))};
    if (all.size() > n)
    {
        const auto pastLast{std::next(all.begin(), static_cast<ptrdiff_t>(n))};
        std::partial_sort(all.begin(), pastLast, all.end(), std::forward<Comp>(comp));
        for (auto item{all.begin()}; item != pastLast; ++item)
        {
            *(output++) = transform(get<Placement<Object>>(*item));
        }
    }
    else
    {
        std::sort(all.begin(), all.end(), std::forward<Comp>(comp));
        for (const auto &item : all)
        {
            *(output++) = transform(get<Placement<Object>>(*item));
        }
    }
    return output;
}

template <Side S, auto O>
const std::vector<Lane *> &Road::GetConnectedLanes() const
{
    static_assert(std::is_same_v<decltype(O), Direction> || std::is_same_v<decltype(O), Traversal>);
    static_assert(S != Side::Both);
    if constexpr (S == Side::Right)
    {
        if constexpr (IsInverse(O))
        {
            return (downstreamPredecessors);
        }
        else
        {
            return (downstreamSuccessors);
        }
    }
    else if constexpr (std::is_same_v<decltype(O), Direction>)
    {
        if constexpr (IsInverse(O))
        {
            return (upstreamSuccessors);
        }
        else
        {
            return (upstreamPredecessors);
        }
    }
    else // if constexpr(std::is_same_v<decltype(O), Traversal>)
    {
        if constexpr (IsInverse(O))
        {
            return (upstreamPredecessors);
        }
        else
        {
            return (upstreamSuccessors);
        }
    }
}

template <Side S, auto O>
std::vector<Lane *> &Road::GetConnectedLanes()
{
    return const_cast<std::vector<Lane *> &>(std::as_const(*this).GetConnectedLanes<S, O>());
}

template <Side S, auto Orientation, typename... Pred>
std::vector<const Lane *> Road::GetConnectedLanes(Pred &&...pred) const
{
    static_assert(std::is_same_v<decltype(Orientation), Direction> || std::is_same_v<decltype(Orientation), Traversal>);
    static_assert(S != Side::Both);
    std::vector<const Lane *> result;
    decltype(auto) all{GetConnectedLanes<S, Orientation>()};
    if constexpr (sizeof...(Pred) > 1)
    {
        // Note: Perfect capture of just the predicates requires C++20
        std::copy_if(all.begin(), all.end(), std::back_inserter(result), [&](const Lane *lane) {
            return (pred(lane) && ...);
        });
    }
    else
    {
        std::copy_if(all.begin(), all.end(), std::back_inserter(result), std::forward<Pred>(pred)...);
    }
    return result;
}

template <Side S, typename Orientation, typename... Pred>
decltype(auto) Road::GetConnectedLanes(Orientation orientation, Pred &&...pred) const
{
    return IsInverse(orientation) ? GetConnectedLanes<S, !Default<Orientation>>(std::forward<Pred>(pred)...) : GetConnectedLanes<S, Default<Orientation>>(std::forward<Pred>(pred)...);
}

template <auto Orientation, typename... Pred>
decltype(auto) Road::GetConnectedLanes(Side side, Pred &&...pred) const
{
    return IsInverse(side) ? GetConnectedLanes<!Default<Side>, Orientation>(std::forward<Pred>(pred)...) : GetConnectedLanes<Default<Side>, Orientation>(std::forward<Pred>(pred)...);
}

template <typename Orientation, typename... Pred>
decltype(auto) Road::GetConnectedLanes(Side side, Orientation orientation, Pred &&...pred) const
{
    return IsInverse(side) ? GetConnectedLanes<!Default<Side>>(orientation, std::forward<Pred>(pred)...) : GetConnectedLanes<Default<Side>>(orientation, std::forward<Pred>(pred)...);
}

template <Side S>
Interval<ConstIterator<std::vector<Lane *>, S>> Road::GetLanesContaining(const ST &coords) const
{
    if (lanes.empty<S>() || !Contains<Direction>(*this, coords, EPSILON))
    {
        return {lanes.end<S>()};
    }
    // Iterator to the first lane whose boundary further towards the given side lies past the coordinates
    const auto firstLane{std::upper_bound(lanes.first<S>(), lanes.end<S>(), coords, [](const ST &coords, const Lane *lane) {
        return Difference<S>(coords.t, lane->GetBoundary<S, Direction::Downstream>(coords.s).GetT(coords.s)) >= -EPSILON;
    })};
    // If firstLane is the very first lane, its earlier boundary might already lie past the coordinates.
    // If so, return an empty range:
    if (firstLane == lanes.first<S>() && Difference<S>(coords.t, (*firstLane)->template GetBoundary<!S, Direction::Downstream>(coords.s).GetT(coords.s)) >= EPSILON)
    {
        return {lanes.end<S>()};
    }
    // If firstLane is the very last lane or doesn't exist, return a range from it to the end of all lanes
    if (firstLane >= std::prev(lanes.end<S>()))
    {
        return {firstLane, lanes.end<S>()};
    }
    // If the point is (practically) on the boundary, return a range with this and the next lane
    if (Difference<S>(coords.t, (*firstLane)->template GetBoundary<S, Direction::Downstream>(coords.s).GetT(coords.s)) <= EPSILON)
    {
        return {firstLane, std::next(firstLane, 2)};
    }
    // Return a range with only this lane
    return {firstLane, std::next(firstLane)};
}

template <Side S>
const Lane *Road::GetClosestLane(const ST &point) const
{
    if (lanes.empty<S>())
    {
        return nullptr;
    }
    return *std::upper_bound(lanes.first<S>(), std::prev(lanes.end<S>()), point, [](const ST &point, const Lane *lane) {
        return Extract<GetDirection(!S)>::less(point.t, lane->GetBoundary<S>(point.s).GetT(point.s));
    });
}

template <Side S>
Lane *Road::GetClosestLane(const ST &point)
{
    return const_cast<Lane *>(const_cast<const Road *>(this)->GetClosestLane<S>(point));
}

template <Side S, typename GlobalPoint>
Localization<GlobalPoint> Road::Localize(const GlobalPoint &globalPoint) const
{
    if constexpr (OSIQL_HAS_MEMBER(GlobalPoint, begin()) && OSIQL_HAS_MEMBER(GlobalPoint, end()))
    {
        Localization<GlobalPoint> result;
        result.reserve(globalPoint.size());
        std::transform(globalPoint.begin(), globalPoint.end(), std::back_inserter(result), [&](const auto &point) {
            return Localize<S>(point);
        });
        return result;
    }
    else if constexpr (S == Side::Both)
    {
        const auto coordinates{GetReferenceLine().Localize(globalPoint)};
        const Lane *lane{GetClosestLane(coordinates)};
        return {coordinates, *lane};
    }
    else
    {
        auto coordinates{GetReferenceLine().Localize(globalPoint)};
        const Lane *lane{GetClosestLane<S>(coordinates)};
        if (!lane)
        {
            std::ostringstream stream;
            stream << "Road::Localize<" << S << ">: No lanes on the given side of this road exist.\n";
            throw std::runtime_error(stream.str());
        }
        return {coordinates, *lane};
    }
}

template <Side S, typename GlobalPoint>
Localized<GlobalPoint> Road::Localize(const GlobalPoint &globalPoint)
{
    if constexpr (OSIQL_HAS_MEMBER(GlobalPoint, begin()) && OSIQL_HAS_MEMBER(GlobalPoint, end()))
    {
        Localization<GlobalPoint> result;
        result.reserve(globalPoint.size());
        std::transform(globalPoint.begin(), globalPoint.end(), std::back_inserter(result), [&](const auto &point) {
            return Localize<S>(point);
        });
        return result;
    }
    else if constexpr (S == Side::Both)
    {
        const auto localization{GetReferenceLine().Localize(globalPoint)};
        Lane *lane{GetClosestLane(localization)};
        return {localization, *lane};
    }
    else
    {
        const auto localization{GetReferenceLine().Localize(globalPoint)};
        Lane *lane{GetClosestLane<S>(localization)};
        if (!lane)
        {
            std::ostringstream stream;
            stream << "Road::Localize<" << S << ">: No lanes on the given side of this road exist.\n";
            throw std::runtime_error(stream.str());
        }
        return {localization, *lane};
    }
}

template <typename GlobalPoint>
Localization<GlobalPoint> Road::Localize(const GlobalPoint &point, Side side) const
{
    if (side == Side::Both)
    {
        return Localize<Side::Both>(point);
    }
    return IsInverse(side) ? Localize<!Default<Side>>(point) : Localize<Default<Side>>(point);
}

template <typename GlobalPoint>
Localized<GlobalPoint> Road::Localize(const GlobalPoint &point, Side side)
{
    if (side == Side::Both)
    {
        return Localize<Side::Both>(point);
    }
    return IsInverse(side) ? Localize<!Default<Side>>(point) : Localize<Default<Side>>(point);
}

template <Side S>
double Road::GetDistanceTo(const XY &globalPoint) const
{
    const ST coordinates{GetReferenceLine().Localize(globalPoint)};
    const Lane *lane{GetClosestLane<S>(coordinates)};
    return lane ? lane->GetDistanceTo(globalPoint) : std::numeric_limits<double>::quiet_NaN();
}

template <typename Collection>
std::vector<const Road *> Road::GetRoads(const Collection &lanes)
{
    std::set<Id> pool;
    std::vector<const Road *> result;
    for (const auto &item : lanes)
    {
        if (pool.insert(get<Road>(item).GetId()).second)
        {
            result.push_back(&get<Road>(item));
        }
    }
    return result;
}
} // namespace osiql
