/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Street/LaneMarking.h"

namespace osiql::detail {
constexpr std::array<std::string_view, Size<LaneMarking::Vertex::Dash>> laneMarkingDashToString{
    "Unknown",
    "Other",
    "Start",
    "Continue",
    "End",
    "Gap",
};
constexpr std::array<std::string_view, Size<LaneMarking::Type>> laneMarkingTypeToString{
    "Unknown",
    "Other",
    "No Line",
    "Solid Line",
    "Dashed Line",
    "Botts Dots",
    "Road Edge",
    "Snow Edge",
    "Grass Edge",
    "Gravel Edge",
    "Soil Edge",
    "Guard Rail",
    "Curb",
    "Structure",
    "Barrier",
    "Sound Barrier",
};
constexpr std::array<std::string_view, Size<LaneMarking::Color>> laneMarkingColorToString{
    "Unknown",
    "Other",
    "None",
    "White",
    "Yellow",
    "Red",
    "Blue",
    "Green",
    "Violet",
    "Orange",
};
} // namespace osiql::detail
