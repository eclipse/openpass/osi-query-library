/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Full set of adjacent lanes

#include "OsiQueryLibrary/Component/Overlapable.h"
#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Has/Road.tpp"
#include "Road/RoadBase.h"

namespace osiql {
OSIQL_GET(Road, GetRoad(), road, GetLane(), lane);
OSIQL_GET(Overlaps<Road>, template GetOverlaps<Road>(), roadOverlaps, overlaps);

//! \brief An OpenDRIVE road, which is a collection of adjacent lanes that share the same start
//! and end s-coordinates along a shared reference line. Bidirectional lanes are not yet supported.
struct Road : RoadBase, Overlapable<Road>
{
    static constexpr const std::string_view name = "Road";

    Road(const ReferenceLine &);

    //! Returns the id of this road's rightmost lane
    //!
    //! \return Id
    Id GetId() const;

    //! Returns the OpenDRIVE name of this road, which is not guaranteed to be unique.
    //!
    //! \return std::string_view
    std::string_view GetOpenDriveId() const;

    //! Returns the earliest s-coordinate encountered in the given driving direction
    //!
    //! \tparam Direction Downstream or Upstream
    //! \return double
    template <Direction>
    constexpr double GetS() const;

    //! Returns an interval of iterators to adjacent lanes on the given side of the road that contain the given
    //! local coordinates. If no such lane exists, an interval from lanes.end<S>() to lanes.end<S>() is returned.
    //!
    //! \tparam S Left, Right or Both
    //! \return std::vector<const Lane *>
    template <Side S = Side::Both>
    Interval<ConstIterator<std::vector<Lane *>, S>> GetLanesContaining(const ST &) const;

    //! Returns the lane closest to the given point on the given side. If the is closest to multiple lanes, the one
    //! with the smaller starting s-coordinate is returned. If all candidates have the same starting s-coordinate,
    //! the one closest to the other side of the road is returned. If no such lane exists, returns nullptr.
    //!
    //! \tparam Side Left or Right
    //! \param ST s- & t-coordinate
    //! \return const Lane*
    template <Side = Side::Both>
    const Lane *GetClosestLane(const ST &) const;

    //! Returns the lane closest to the given point on the given side. If the is closest to multiple lanes, the one
    //! with the smaller starting s-coordinate is returned. If all candidates have the same starting s-coordinate,
    //! the one closest to the other side of the road is returned. If no such lane exists, returns nullptr.
    //!
    //! \tparam Side Left or Right
    //! \param ST s- & t-coordinate
    //! \return Lane*
    template <Side = Side::Both>
    Lane *GetClosestLane(const ST &);

    //! Returns the lane closest to the given point on the given side. If the is closest to multiple lanes, the one
    //! with the smaller starting s-coordinate is returned. If all candidates have the same starting s-coordinate,
    //! the one closest to the other side of the road is returned. If no such lane exists, returns nullptr.
    //!
    //! \param ST s- & t-coordinate
    //! \param sideOfRoad Left or Right
    //! \return const Lane*
    const Lane *GetClosestLane(const ST &point, Side sideOfRoad) const;

    //! Returns the lane closest to the given point on the given side. If the is closest to multiple lanes, the one
    //! with the smaller starting s-coordinate is returned. If all candidates have the same starting s-coordinate,
    //! the one closest to the other side of the road is returned. If no such lane exists, returns nullptr.
    //!
    //! \param ST s- & t-coordinate
    //! \param sideOfRoad Left or Right
    //! \return Lane*
    Lane *GetClosestLane(const ST &, Side sideOfRoad);

    //! Converts the given container of lanes to a container of unique roads containing said lanes.
    //!
    //! \tparam Iterable collection of types with an extractable lane
    //! \return std::vector<const Road *>
    template <typename Collection>
    static std::vector<const Road *> GetRoads(const Collection &);

    //! Returns all connected lanes at the end of the lanes on the given side of this road in the given direction.
    //!
    //! \tparam Side The side of the road that the connected lanes should be restricted to. (Left or Right)
    //! \tparam Orientation Either a Traversal direction relative to the driving direction on that side
    //! of the road or a Direction relative to the road's reference line that specifies at which end the
    //! returned lanes should be connected to this road.
    //! \return Lanes connected to the end of the road on the given side in the given direction
    template <Side, auto Orientation>
    const std::vector<Lane *> &GetConnectedLanes() const;

    //! Returns all connected lanes at the end of the lanes on the given side of this road in the given direction.
    //!
    //! \tparam Side The side of the road that the connected lanes should be restricted to. (Left or Right)
    //! \tparam Orientation Either a Traversal direction relative to the driving direction on that side
    //! of the road or a Direction relative to the road's reference line that specifies at which end the
    //! returned lanes should be connected to this road.
    //! \return std::vector<Lane*> if Side is Both, otherwise std::vector<Lane*>&
    template <Side, auto Orientation>
    std::vector<Lane *> &GetConnectedLanes();

    //! Returns all connected lanes at the end of the lanes on the given side
    //! of this road in the given direction that satisfy the given predicates.
    //!
    //! \tparam Side The side of the road that the connected lanes should be restricted to
    //! \tparam Orientation Either a Traversal direction relative to the driving direction on that side
    //! of the road or a Direction relative to the road's reference line that specifies at which end the
    //! returned lanes should be connected to this road.
    //! \tparam Pred Invocable with the signature bool(const Lane*)
    template <Side, auto Orientation, typename... Pred>
    std::vector<const Lane *> GetConnectedLanes(Pred &&...) const;

    //! Returns all connected lanes at the end of the lanes on the given side
    //! of this road in the given direction.
    //!
    //! \tparam Side What side of this road the returned lanes are connected to. (Either Left or Right)
    //! \param Orientation What end of the road the lanes are connected to.
    //! (Of type Traversal or Direction)
    //! \tparam Pred Invocable with the signature bool(const Lane*)
    //! \return std::vector<const Lane*> if one or more predicates are passed,
    //! otherwise const std::vector<Lane*>&
    template <Side, typename Orientation, typename... Pred>
    decltype(auto) GetConnectedLanes(Orientation, Pred &&...) const;

    //! Returns all connected lanes at the end of the lanes on the given side
    //! of this road in the given direction that satisfy the given predicates.
    //!
    //! \tparam Orientation What end of the road the lanes are connected to.
    //! (Of type Traversal or Direction)
    //! \param Side What side of this road the returned lanes are connected to. (Either Left or Right)
    //! \tparam Pred Invocable with the signature bool(const Lane*)
    //! \return std::vector<const Lane*> if one or more predicates are passed,
    //! otherwise const std::vector<Lane*>&
    template <auto Orientation, typename... Pred>
    decltype(auto) GetConnectedLanes(Side, Pred &&...) const;

    //! Returns all connected lanes at the end of the lanes on
    //! the given side of this road in the given orientation.
    //!
    //! \tparam Orientation What end of the road the lanes are connected to. (Of type Traversal or Direction)
    //! \param Side What side of this road the returned lanes are connected to. (Either Left or Right)
    //! \tparam Pred Invocable with the signature bool(const Lane*)
    //! \return std::vector<const Lane*> if one or more predicates are passed,
    //! otherwise const std::vector<Lane*>&
    template <typename Orientation, typename... Pred>
    decltype(auto) GetConnectedLanes(Side, Orientation, Pred &&...) const;

    //! Returns the transformed selected placements of the given object type assigned to
    //! any lane on the given side(s) of this road that satisfy the given predicate.
    //!
    //! \tparam Object Type of the object whose placements will be selected and transformed
    //! \tparam Side What side of the road to restrict the visited lanes to. Either Left, Right or Both
    //! \tparam Selection Determines which assigments are returned in cases of
    //! one object being assigned to multiple lanes of this road. Either
    //! First (only the innermost lane or if side is Both the rightmost),
    //! Last (only the outermost lane or if side is Both the left most) or
    //! Each (all assignments)
    //! \tparam Pred Invocable with the signature bool(const Placement<Object>&).
    //! Any placement for which the predicate returns true will be passed to the transformer
    //! and added to the returned container.
    //! \tparam Transform Invocable taking a const Placement<Object>& and returning
    //! the desired output type.
    //! \return Object placements pertaining to lanes of this road
    template <typename Object,                     //
              Side = Side::Both,                   //
              Selection = Selection::Each,         //
              typename Pred = osiql::Return<true>, //
              typename Transform = osiql::Get<Placement<Object> *>>
    std::vector<std::invoke_result_t<Transform, const Placement<Object> &>> GetAll(Pred && = {}, Transform && = {}) const;

    //! Inserts all transformed selected placements of the given object type on the given side of this road
    //! that satisfy the given predicate akin to std::transform.
    //!
    //! \tparam Object Type of the objects whose placements will be inserted
    //! \tparam Side What side of the road to restrict the visited lanes to. Either Left, Right or Both
    //! \tparam Selection Determines which assigments are returned in cases of
    //! one object being assigned to multiple lanes of this road. Either
    //! First (only the innermost lane or if side is Both the rightmost),
    //! Last (only the outermost lane or if side is Both the left most) or
    //! Each (all assignments)
    //! \tparam Pred Invocable with the signature bool(const Placement<Object>&).
    //! Any placement for which the predicate returns true will be passed to the transformer
    //! and added to the returned container.
    //! \tparam Transform Invocable taking a const Placement<Object>& and returning
    //! the desired output type.
    //! \tparam OutputIt Iterator pointing to the next place of insertion for a transformed placement.
    //! The iterator is incremented after an insertion and must guarantee the validity of any future insertion,
    //! like a std::back_inserter of a std::vector for instance.
    //! \return Iterator pointing past the last place of insertion
    template <typename Object,                                      //
              Side = Side::Both,                                    //
              Selection = Selection::Each,                          //
              typename Pred = osiql::Return<true>,                  //
              typename Transform = osiql::Get<Placement<Object> *>, //
              typename OutputIt = void>
    OutputIt InsertAll(OutputIt, Pred && = {}, Transform && = {}) const;

    template <typename Object,                     //
              Side = Side::Both,                   //
              Selection = Selection::Each,         //
              typename Pred = osiql::Return<true>, //
              typename Comp = Less<S>,             //
              typename Transform = osiql::Get<Placement<Object> *>>
    std::vector<std::invoke_result_t<Transform, const Placement<Object> &>> Find(size_t n, Pred && = {}, Comp && = {}, Transform && = {}) const;

    template <typename Object,                                      //
              Side = Side::Both,                                    //
              Selection = Selection::Each,                          //
              typename Pred = osiql::Return<true>,                  //
              typename Comp = Less<S>,                              //
              typename Transform = osiql::Get<Placement<Object> *>, //
              typename OutputIt = void>
    OutputIt Insert(OutputIt, size_t n, Pred && = {}, Comp && = {}, Transform && = {}) const;

    //! Returns a local point of the lane of this road closest to the given global point
    //!
    //! \tparam Side Left, Right or Both. Restricts the closest lane to be among those on the given side of this road
    //! \tparam GlobalPoint xy-coordinates, optionally with a global yaw angle, or an iterable range thereof.
    //! \param GlobalPoint Global point corresponding to the returned local point
    //! \return Point<const Lane, ST>
    template <Side = Side::Both, typename GlobalPoint = XY>
    Localization<GlobalPoint> Localize(const GlobalPoint &) const;

    //! Returns a local point of the lane of this road closest to the given global point
    //!
    //! \tparam Side Left, Right or Both. Restricts the closest lane to be among those on the given side of this road
    //! \tparam GlobalPoint xy-coordinates, optionally with a global yaw angle, or an iterable range thereof
    //! \param GlobalPoint Global point corresponding to the returned local point
    //! \return Local point of the lane of this road closest to the given global point
    template <Side = Side::Both, typename GlobalPoint = XY>
    Localized<GlobalPoint> Localize(const GlobalPoint &);

    //! Returns a local point of the lane in the given direction closest to or containing the given global point.
    //! Throws if this road does not have a lane in the given direction
    //!
    //! \tparam Scope Lane or Road
    //! \tparam GlobalPoint xy-coordinates, optionally with a global yaw angle, or an iterable range thereof
    //! \param GlobalPoint Global point corresponding to the returned local point
    //! \param sideOfRoad Left, Right or Both
    //! \return Local point of the lane of this road closest to the given global point
    template <typename GlobalPoint>
    Localization<GlobalPoint> Localize(const GlobalPoint &, Side sideOfRoad) const;

    //! Returns a local point of the lane in the given direction closest to or containing the given global point.
    //! Throws if this road does not have a lane in the given direction
    //!
    //! \tparam GlobalPoint GlobalPoint xy-coordinates, optionally with a global yaw angle, or an iterable range thereof
    //! \param GlobalPoint Global point corresponding to the returned local point
    //! \param sideOfRoad Left, Right or Both
    //! \return Local point of the lane of this road closest to the given global point
    template <typename GlobalPoint>
    Localized<GlobalPoint> Localize(const GlobalPoint &, Side sideOfRoad);

    //! Returns the shortest distance between the closest lane of this road in the given driving direction and
    //! the given global point. If one such lane contains the given point, 0.0 is returned.
    //!
    //! \tparam Side
    //! \param XY
    //! \return double
    template <Side>
    double GetDistanceTo(const XY &) const;

    //! Returns the shortest distance between the closest lane of this road in the given driving direction and
    //! the given global point. If one such lane contains the given point, 0.0 is returned.
    //!
    //! \param XY
    //! \param sideOfRoad
    //! \return double
    double GetDistanceTo(const XY &, Side sideOfRoad) const;

    //! Returns the shortest distance between the closest lane of this road in the given driving direction and
    //! the given global point. If one such lane contains the given point, 0.0 is returned.
    //!
    //! \param XY
    //! \return double
    double GetDistanceTo(const XY &) const;

    //! Returns the length of this road's reference line
    //!
    //! \return double
    double GetLength() const;

    void UpdateShapeAndBounds();

    //! \brief Lanes of this road from right to left
    Lanes lanes;

    //! \brief The boundaries from right to left of all lanes on this road
    BoundaryChains boundaries;

private:
    std::vector<Lane *> downstreamPredecessors;
    std::vector<Lane *> upstreamPredecessors;
    std::vector<Lane *> downstreamSuccessors;
    std::vector<Lane *> upstreamSuccessors;
};
} // namespace osiql
