/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Street/BoundaryChain.h"

#include "OsiQueryLibrary/Component/Iterable.tpp"
#include "OsiQueryLibrary/Street/Boundary.tpp"

namespace osiql {
template <Direction D>
ConstIterator<std::vector<Boundary *>, D> BoundaryChain::FindBoundary(double s) const
{
    return std::upper_bound(begin<D>(), std::prev(end<D>()), s, [](double s, const Boundary *boundary) {
        return Extract<D>::less(s, extract<!D>(boundary->front<!D>()));
    });
}

template <Direction D>
Iterator<std::vector<Boundary *>, D> BoundaryChain::FindBoundary(double s)
{
    return std::upper_bound(begin<D>(), std::prev(end<D>()), s, [](double s, const Boundary *boundary) {
        return less<D>(s, extract<!D>(boundary->front<!D>()));
    });
}

template <Direction D>
const Boundary &BoundaryChain::GetBoundary(double s) const
{
    return **FindBoundary<D>(s);
}

template <Direction D>
Boundary &BoundaryChain::GetBoundary(double s)
{
    return **FindBoundary<D>(s);
}

template <Direction D>
double BoundaryChain::GetLateralDistance(const ST &coords) const
{
    return GetBoundary<D>(coords.s).GetLateralDistance(coords);
}

template <Direction D, typename Coordinates>
Interval<double> BoundaryChain::GetLateralDistances(const Coordinates &coords) const
{
    return GetLateralDistances<D>(coords.begin(), coords.end());
}

template <typename Coordinates>
Interval<double> BoundaryChain::GetLateralDistances(Direction direction, const Coordinates &coords) const
{
    return IsInverse(direction) ? GetLateralDistances<!Default<Direction>>(coords) : GetLateralDistances<Default<Direction>>(coords);
}

template <Direction D, typename It>
Interval<double> BoundaryChain::GetLateralDistances(It begin, It end) const
{
    std::vector<double> distances;
    distances.reserve(static_cast<size_t>(std::distance(begin, end)));
    std::transform(begin, end, std::back_inserter(distances), [this](const ST &coords) {
        return GetLateralDistance<D>(coords);
    });
    const auto [min, max]{std::minmax_element(distances.begin(), distances.end())};
    return {*min, *max};
}

template <typename It>
Interval<double> BoundaryChain::GetLateralDistances(Direction direction, It begin, It end) const
{
    return IsInverse(direction) ? GetLateralDistance<!Default<Direction>>(begin, end) : GetLateralDistance<Default<Direction>>(begin, end);
}
} // namespace osiql
