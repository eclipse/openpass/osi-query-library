/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Street/Road/Lanes.h"

#include "OsiQueryLibrary/Component/Iterable.tpp"
#include "OsiQueryLibrary/Street/Lane.h"

namespace osiql {
template <Side S>
size_t Lanes::size() const
{
    if constexpr (S == Side::Left)
    {
        return size() - size<!S>();
    }
    else if constexpr (S == Side::Right)
    {
        return static_cast<size_t>(center);
    }
    else
    {
        return size();
    }
}

template <Side S>
ConstIterator<std::vector<Lane *>, S> Lanes::first() const
{
    if constexpr (IsInverse(S))
    {
        return std::make_reverse_iterator(first<!S>());
    }
    else if constexpr (S == Side::Left)
    {
        return std::next(begin(), center);
    }
    else
    {
        return begin();
    }
}

template <Side S>
Iterator<std::vector<Lane *>, S> Lanes::first()
{
    if constexpr (IsInverse(S))
    {
        return std::make_reverse_iterator(first<!S>());
    }
    else if constexpr (S == Side::Left)
    {
        return std::next(begin(), center);
    }
    else
    {
        return begin();
    }
}

template <Side S>
const Lane *Lanes::on(size_t i) const
{
    return *std::next(first<S>(), static_cast<std::ptrdiff_t>(i));
}

template <Side S>
Lane *Lanes::on(size_t i)
{
    return *std::next(first<S>(), static_cast<std::ptrdiff_t>(i));
}

template <Side S>
bool Lanes::empty() const
{
    return first<S>() == end<S>();
}

template <typename Type, Direction D>
std::vector<const Placement<Type> *> Lanes::GetAll() const
{
    std::vector<const Placement<Type> *> result;
    for (const Lane *lane : container)
    {
        const Collection<Placement<Type>> &placements{lane->template GetAll<Type>()};
        for (const Placement<Type> &placement : placements)
        {
            result.push_back(&placement);
        }
    }
    std::sort(result.begin(), result.end(), Extract<D>::less);
    return result;
}

template <typename Type>
std::vector<const Placement<Type> *> Lanes::GetAll(Direction direction) const
{
    return direction == Direction::Upstream ? GetAll<Type, Direction::Upstream>() : GetAll<Type>();
}

template <typename Type, Direction D, typename Pred>
std::vector<const Placement<Type> *> Lanes::GetAll(Pred &&pred) const
{
    std::vector<const Placement<Type> *> result;
    for (const Lane *lane : *this)
    {
        std::vector<const Placement<Type> *> placements{lane->template GetAll<Type>(std::forward<Pred>(pred))};
        result.insert(result.end(), std::make_move_iterator(placements.begin()), std::make_move_iterator(placements.end()));
    }
    std::sort(result.begin(), result.end(), Extract<D>::less);
    return result;
}

template <typename Type, typename Pred>
std::vector<const Placement<Type> *> Lanes::GetAll(Direction direction, Pred &&pred) const
{
    return direction == Direction::Upstream ? GetAll<Direction::Upstream>(std::forward<Pred>(pred)) : GetAll(std::forward<Pred>(pred));
}

template <typename Type, Side S, Traversal T>
std::vector<const Placement<Type> *> Lanes::GetAll() const
{
    std::vector<const Placement<Type> *> result;
    for (auto lane{first<S>()}; lane != end<S>(); ++lane)
    {
        const Collection<Placement<Type>> &placements{(*lane)->template GetAll<Type>()};
        for (const Placement<Type> &placement : placements)
        {
            result.push_back(&placement);
        }
    }
    std::sort(result.begin(), result.end(), Extract<GetDirection(S, T)>::less);
    return result;
}

template <typename Type, Traversal T>
std::vector<const Placement<Type> *> Lanes::GetAll(Side side) const
{
    return IsInverse(side) ? GetAll<Type, !Default<Side>, T>() : GetAll<Type, Default<Side>, T>();
}

template <typename Type, Side S>
std::vector<const Placement<Type> *> Lanes::GetAll(Traversal toward) const
{
    return toward == Traversal::Backward ? GetAll<Type, S, Traversal::Backward>() : GetAll<Type, S>();
}

template <typename Type>
std::vector<const Placement<Type> *> Lanes::GetAll(Side side, Traversal toward) const
{
    return IsInverse(side) ? GetAll<Type, !Default<Side>>(toward) : GetAll<Type, Default<Side>>(toward);
}

template <typename Type, Side S, Traversal T, typename Pred>
std::vector<const Placement<Type> *> Lanes::GetAll(Pred &&pred) const
{
    std::vector<const Placement<Type> *> result;
    for (auto lane{first<S>()}; lane != end<S>(); ++lane)
    {
        std::vector<const Placement<Type> *> placements{lane->template GetAll<Type>(std::forward<Pred>(pred))};
        result.insert(result.end(), std::make_move_iterator(placements.begin()), std::make_move_iterator(placements.end()));
    }
    std::sort(result.begin(), result.end(), Extract<GetDirection(S, T)>::less);
    return result;
}

template <typename Type, Traversal T, typename Pred>
std::vector<const Placement<Type> *> Lanes::GetAll(Side side, Pred &&pred) const
{
    return IsInverse(side) ? GetAll<Type, !Default<Side>, T>(std::forward<Pred>(pred)) : GetAll<Type, Default<Side>, T>(std::forward<Pred>(pred));
}

template <typename Type, Side S, typename Pred>
std::vector<const Placement<Type> *> Lanes::GetAll(Traversal toward, Pred &&pred) const
{
    return toward == Traversal::Backward ? GetAll<Type, S, Traversal::Backward>(std::forward<Pred>(pred)) : GetAll<Type, S>(std::forward<Pred>(pred));
}

template <typename Type, typename Pred>
std::vector<const Placement<Type> *> Lanes::GetAll(Side side, Traversal toward, Pred &&pred) const
{
    return IsInverse(side) ? GetAll<Type, !Default<Side>>(toward, std::forward<Pred>(pred)) : GetAll<Type, Default<Side>>(toward, std::forward<Pred>(pred));
}
} // namespace osiql
