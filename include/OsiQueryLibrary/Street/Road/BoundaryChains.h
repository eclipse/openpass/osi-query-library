/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Component/Iterable.h"
#include "OsiQueryLibrary/Street/BoundaryChain.h"

namespace osiql {
//! \brief A group of boundary chains with the same minimum and maximum s-coordinate
//! sorted from right to left. The boundary chains may touch, but never cross each other.
struct BoundaryChains : Iterable<std::vector<BoundaryChain>, BoundaryChains, Side>
{
    using Base = Iterable<std::vector<BoundaryChain>, BoundaryChains, Side>;

    using Base::begin;

    //! Returns an iterator pointing to the rightmost boundary chain.
    //!
    //! \return Bidirectional forward iterator
    ConstIterator<std::vector<BoundaryChain>> begin() const;

    //! Returns an iterator pointing to the rightmost boundary chain.
    //!
    //! \return Bidirectional forward iterator
    Iterator<std::vector<BoundaryChain>> begin();

    using Base::end;

    //! Returns an iterator directly past the leftmost boundary chain.
    //!
    //! \return Bidirectional forward iterator
    ConstIterator<std::vector<BoundaryChain>> end() const;

    //! Returns an iterator directly past the leftmost boundary chain.
    //!
    //! \return Bidirectional forward iterator
    Iterator<std::vector<BoundaryChain>> end();

    using Base::size;

    //! Returns the number of boundary chains on the given side of the road.
    //! Includes the boundary chain between both sides.
    //!
    //! \tparam S Left or Right
    //! \return Number of boundaries on the given side.
    template <Side>
    size_t size() const;

    //! Returns an iterator to the boundary separating the two sides of the road
    //!
    //! \tparam S Left or Right
    //! \return ConstIterator<std::vector<BoundaryChain>, S>
    template <Side S>
    ConstIterator<std::vector<BoundaryChain>, S> first() const; // NOLINT(readability-identifier-naming)

    //! Returns an iterator to the boundary separating the two sides of the road
    //!
    //! \tparam S Left or Right
    //! \return ConstIterator<std::vector<BoundaryChain>, S>
    template <Side S>
    Iterator<std::vector<BoundaryChain>, S> first(); // NOLINT(readability-identifier-naming)

    //! Returns the n-th boundary on the given side of the road starting from the boundary shared between the left
    //! and right side of the road. Similar to 'at' except it skips boundaries on the other side of the road.
    //!
    //! \tparam S Left or Right
    //! \param i Number of lanes between the returned boundary chain and the boundary
    //! chain shared between the left and right side of the road
    //! \return Boundary chain
    template <Side S>
    const BoundaryChain &on(std::ptrdiff_t i) const; // NOLINT(readability-identifier-naming)

    //! Returns the n-th boundary on the given side of the road starting from the boundary shared between the left
    //! and right side of the road. Similar to 'at' except it skips boundaries on the other side of the road.
    //!
    //! \param Side Left or Right
    //! \param i Number of lanes between the returned boundary chain and the boundary
    //! chain shared between the left and right side of the road
    //! \return Boundary chain
    const BoundaryChain &on(Side, std::ptrdiff_t) const; // NOLINT(readability-identifier-naming)

    //! Returns the n-th boundary on the given side of the road starting from the boundary shared between the left
    //! and right side of the road. Similar to 'at' except it skips boundaries on the other side of the road.
    //!
    //! \tparam Side Left or Right
    //! \param i Number of lanes between the returned boundary chain and the boundary
    //! chain shared between the left and right side of the road
    //! \return Boundary chain
    template <Side S>
    BoundaryChain &on(std::ptrdiff_t); // NOLINT(readability-identifier-naming)

    //! Returns the n-th boundary on the given side of the road starting from the boundary shared between the left
    //! and right side of the road. Similar to 'at' except it skips boundaries on the other side of the road.
    //!
    //! \param Side Left or Right
    //! \param i Number of lanes between the returned boundary chain and the boundary
    //! chain shared between the left and right side of the road
    //! \return Boundary chain
    BoundaryChain &on(Side, std::ptrdiff_t); // NOLINT(readability-identifier-naming)

    //! \brief Boundary chains of this road from right to left relative to the reference line's direction
    std::vector<BoundaryChain> container;

    //! \brief Index of the left boundary chain of the leftmost lane on the right side of the road
    std::ptrdiff_t center{0};
};
} // namespace osiql
