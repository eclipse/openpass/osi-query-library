/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Component/Iterable.h"
#include "OsiQueryLibrary/Trait/Placement.h"

namespace osiql {
struct Lane;
//! \brief A chain of adjacent lanes sharing the same reference line
//! from right to left relative to their reference line
struct Lanes : Iterable<std::vector<Lane *>, Lanes, Side>
{
    using Base = Iterable<std::vector<Lane *>, Lanes, Side>;

    using Base::begin;
    ConstIterator<std::vector<Lane *>> begin() const;
    Iterator<std::vector<Lane *>> begin();

    using Base::end;
    ConstIterator<std::vector<Lane *>> end() const;
    Iterator<std::vector<Lane *>> end();

    using Base::size;
    template <Side>
    size_t size() const;
    size_t size(Side) const;

    //! Returns an iterator to the first lane on the given side closest to the other side of the road.
    //! Similar to 'begin' except it skips lanes on the other side of the road.
    //!
    //! \tparam S Left or Right
    //! \return Iterator pointing to the first lane on this side of the road
    template <Side S>
    ConstIterator<std::vector<Lane *>, S> first() const; // NOLINT(readability-identifier-naming)

    //! Returns an iterator to the first lane on the given side closest to the other side of the road.
    //! Similar to 'begin' except it skips lanes on the other side of the road.
    //!
    //! \tparam S Left or Right
    //! \return Iterator pointing to the first lane on this side of the road
    template <Side S>
    Iterator<std::vector<Lane *>, S> first(); // NOLINT(readability-identifier-naming)

    //! Returns the nth lane on the given side of the road. Similar to 'at' except it skips lanes
    //! on the other side of the road.
    //!
    //! \tparam S Left or Right
    //! \return const Lane&
    template <Side S>
    const Lane *on(size_t) const; // NOLINT(readability-identifier-naming)

    template <Side S>
    Lane *on(size_t); // NOLINT(readability-identifier-naming)

    const Lane *on(Side, size_t) const; // NOLINT(readability-identifier-naming)

    Lane *on(Side, size_t); // NOLINT(readability-identifier-naming)

    using Base::empty;
    template <Side>
    bool empty() const;

    //! Returns a collection of all placements of objects of the given type.
    //!
    //! \tparam T MovingObject, RoadMarking, StationaryObject, TrafficLight or TrafficSign
    //! \tparam Direction The returned collection will be sorted by distance in this direction.
    //! \return std::vector<const Placement<T>*> Sorted by ascending distance
    template <typename Type, Direction = Direction::Downstream>
    std::vector<const Placement<Type> *> GetAll() const;

    template <typename Type>
    std::vector<const Placement<Type> *> GetAll(Direction) const;

    //! Returns a collection of all placements of objects of the given type satisfying the given predicate.
    //!
    //! \tparam Type MovingObject, RoadMarking, StationaryObject, TrafficLight or TrafficSign
    //! \tparam Direction The returned collection will be sorted by distance in this direction.
    //! \tparam Pred Invocable receiving a const Placement<Type>& and returning a bool
    //! \return std::vector<const Placement<Type>*> Sorted by ascending distance
    template <typename Type, Direction = Direction::Downstream, typename Pred = void>
    std::vector<const Placement<Type> *> GetAll(Pred &&) const;

    template <typename Type, typename Pred>
    std::vector<const Placement<Type> *> GetAll(Direction, Pred &&) const;

    //! Returns a collection of all placements of objects of the given type on the given side of the road.
    //!
    //! \tparam Type MovingObject, RoadMarking, StationaryObject, TrafficLight or TrafficSign
    //! \tparam Side Left or Right
    //! \tparam Traversal Forward or Backward
    //! \return std::vector<const Placement<Type> *> Sorted by ascending distance from the start of this lane
    //! row in the given orientation.
    template <typename Type, Side, Traversal = Traversal::Forward>
    std::vector<const Placement<Type> *> GetAll() const;

    template <typename Type, Traversal = Traversal::Forward>
    std::vector<const Placement<Type> *> GetAll(Side) const;

    template <typename Type, Side>
    std::vector<const Placement<Type> *> GetAll(Traversal) const;

    template <typename Type>
    std::vector<const Placement<Type> *> GetAll(Side, Traversal) const;

    //! Returns a collection of all placements of objects of the given type on the given side of the road
    //! satisfying the given predicate.
    //!
    //! \tparam Type MovingObject, RoadMarking, StationaryObject, TrafficLight or TrafficSign
    //! \tparam Side Left or Right
    //! \tparam Traversal Forward or Backward
    //! \tparam Pred Invocable receiving a const Placement<Type>& and returning a bool
    //! \return std::vector<const Placement<Type>*> Sorted by ascending distance from the start of this lane
    //! row in the given orientation.
    template <typename Type, Side, Traversal = Traversal::Forward, typename Pred = void>
    std::vector<const Placement<Type> *> GetAll(Pred &&) const;

    template <typename Type, Traversal = Traversal::Forward, typename Pred = void>
    std::vector<const Placement<Type> *> GetAll(Side, Pred &&) const;

    template <typename Type, Side, typename Pred>
    std::vector<const Placement<Type> *> GetAll(Traversal, Pred &&) const;

    template <typename Type, typename Pred>
    std::vector<const Placement<Type> *> GetAll(Side, Traversal, Pred &&) const;

    // TODO: std::vector<const Placement<T> *> GetAllInRange(double startS, double endS) const;

    //! \brief The set of lanes in this row
    std::vector<Lane *> container;

    //! \brief Number of lanes from the rightmost lane on the right side of the road
    //! to the rightmost lane on the left side of the road
    std::ptrdiff_t center{0};
};
} // namespace osiql
