/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Street/Road/BoundaryChains.h"

#include "OsiQueryLibrary/Component/Iterable.tpp"

namespace osiql {
template <Side S>
size_t BoundaryChains::size() const
{
    if constexpr (IsInverse(S))
    {
        return 1 + size() - size<!S>();
    }
    else
    {
        return 1 + static_cast<size_t>(center);
    }
}

template <Side S>
ConstIterator<std::vector<BoundaryChain>, S> BoundaryChains::first() const
{
    if constexpr (S == Side::Right)
    {
        return std::make_reverse_iterator(std::next(first<!S>()));
    }
    else if constexpr (S == Side::Left)
    {
        return std::next(begin<S>(), center);
    }
    else
    {
        return begin<S>();
    }
}

template <Side S>
Iterator<std::vector<BoundaryChain>, S> BoundaryChains::first()
{
    if constexpr (S == Side::Right)
    {
        return std::make_reverse_iterator(std::next(first<!S>()));
    }
    else if constexpr (S == Side::Left)
    {
        return std::next(begin<S>(), center);
    }
    else
    {
        return begin<S>();
    }
}

template <Side S>
const BoundaryChain &BoundaryChains::on(std::ptrdiff_t i) const
{
    return *std::next(first<S>(), i);
}

template <Side S>
BoundaryChain &BoundaryChains::on(std::ptrdiff_t i)
{
    return const_cast<BoundaryChain &>(std::as_const(*this).on<S>(i));
}
} // namespace osiql
