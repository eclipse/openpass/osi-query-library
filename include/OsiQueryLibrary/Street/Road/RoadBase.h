/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! Base class of Road. Needs to exist in order to allow a road
//! to inherit from Overlapable<Road>, which instantiates Has<Road>

#include <string_view>
#include <vector>

#include "BoundaryChains.h"
#include "Lanes.h"
#include "OsiQueryLibrary/Street/ReferenceLine.h"
#include "OsiQueryLibrary/Trait/Localization.h"
#include "OsiQueryLibrary/Trait/Placement.h"
#include "OsiQueryLibrary/Types/Bounds.h"
#include "OsiQueryLibrary/Types/Enum.h" // Traversal, Selection & Side
#include "OsiQueryLibrary/Types/Shape.h"
#include "OsiQueryLibrary/Utility/Aggregate.h"
#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Has/Road.h"

namespace osiql {
struct Lane;
template <typename, typename>
struct Point;

struct RoadBase : Aggregate<const ReferenceLine *, Shape, Bounds>
{
    using Aggregate<const ReferenceLine *, Shape, Bounds>::Aggregate;
};

std::ostream &operator<<(std::ostream &, const Road &);
} // namespace osiql

#include "OsiQueryLibrary/Utility/Has/Road.h"
