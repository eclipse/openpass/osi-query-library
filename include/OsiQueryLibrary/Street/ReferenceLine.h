/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Wrapper of a osi3::ReferenceLine

#include <iostream>
#include <optional>

#include <osi3/osi_referenceline.pb.h>

#include "OsiQueryLibrary/Component/Identifiable.h"
#include "OsiQueryLibrary/Component/Points.h"
#include "OsiQueryLibrary/Point/Coordinates.h"
#include "OsiQueryLibrary/Point/Pose.h"
#include "OsiQueryLibrary/Types/Container.h"
#include "OsiQueryLibrary/Utility/Common.h"
#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Has.h"
#include "OsiQueryLibrary/Version.h"

namespace osiql {
//! \brief osi3::ReferenceLine wrapper. A reference line is the center line of a road. Local points on the road are expressed
//! relative to the position of this reference line. Lanes to its right travel downstream and have negative relative ids.
//! Local points on them have negative t-coordinates. Lanes to its left travel upstream and have positive relative ids.
//! Local points on the have positive t-coordinates.
struct ReferenceLine : Identifiable<osi3::ReferenceLine>, Points<Container<osi3::ReferenceLine::ReferenceLinePoint>, ReferenceLine>
{
    //! \brief Alias for the point type of the underlying OSI object
    using Point = osi3::ReferenceLine::ReferenceLinePoint;

    using Base = Points<Container<Point>, ReferenceLine>;

    //! Constructs a wrapper of a given OSI reference line
    //!
    //! \param handle OSI reference line
    ReferenceLine(const osi3::ReferenceLine &handle);

    using Base::begin;
    ConstIterator<Container<Point>> begin() const;

    using Base::end;
    ConstIterator<Container<Point>> end() const;

    //! Returns an iterator to the start of the edge whose section contains the given point
    //!
    //! \tparam D Downstream or Upstream
    //! \param point
    //! \return ConstIterator<Container<Point>, D>
    template <Direction D = Direction::Downstream>
    ConstIterator<Container<Point>, D> begin(const XY &point) const;

    //! Returns an iterator to the point past the start of the edge whose section contains the given point
    //!
    //! \tparam D Downstream or Upstream
    //! \param point
    //! \return ConstIterator<Container<Point>, D>
    template <Direction D = Direction::Downstream>
    ConstIterator<Container<Point>, D> end(const XY &point) const;

    //! \brief Reference line type describing how its coordinate system is defined
    enum class Type : int
    {
        Polyline = 0,     //! Perpendicular t-axis
        PolylineWithTAxis //! Custom t-axis yaw for each point (requires OSI 3.6+)
    };

    //! Returns whether this reference line has an assigned type.
    //!
    //! \return Whether this reference line has an assigned type. Always false prior to OSI 3.6
    constexpr bool HasType() const;

    //! Returns the type of this reference line, which describes how its coordinate system is defined.
    //!
    //! \param fallback Fallback value to be returned if this reference line has no type.
    //! \return Type The type of this reference line or the fallback value if it has no type.
    constexpr Type GetType(Type fallback = Type::Polyline) const;

    //! Returns the local st-coordinate representation of the given global point
    //! according to the coordinate system defined by this reference line.
    //!
    //! \param XY Global xy-coordinate pair
    //! \return ST Local st-coordinate pair
    template <typename Input, OSIQL_REQUIRES(Are<X, Y, Not<Angle>>::in<Input>)>
    ST Localize(const Input &xy) const
    {
        if constexpr (GetOSIVersion() >= Version{3, 6, 0}) // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
        {
            return GetType(Type::PolylineWithTAxis) == Type::Polyline ? Base::Localize(xy) : LocalizeUsingTAxes(xy);
        }
        else // OSI 3.5
        {
            return Base::Localize(xy);
        }
    }

    //! Returns the local st-pose representation of the given global point and angle
    //! according to the coordinate system defined by this reference line.
    //!
    //! \param XY Global xy-coordinate pair with a counter-clockwise ascending angle
    //! \return ST Local st-coordinate pair with a counter-clockwise ascending angle between [-π, π]
    template <typename Point, OSIQL_REQUIRES(Are<X, Y, Angle>::in<Point>)>
    Pose<ST> Localize(const Point &pose) const
    {
        auto coords{Localize(XY{pose})};
        // TODO: Performance - Avoid s-coordinate lookup by reusing localization iterators
        return Pose<ST>{coords, WrapAngle(get<Angle>(pose) - GetAngle(coords.s))};
    }

    template <typename It>
    decltype(auto) Localize(It first, It pastLast) const;

    //! Returns the global xy-coordinate representation of the given local point
    //! according to the coordinate system defined by this reference line.
    //!
    //! \param const ST& Local st-coordinate pair
    //! \return XY Global xy-coordinate pair
    XY GetXY(const ST &) const;

private:
    //! Returns the direction vector describing the longitudinal axis at the point
    //! on this reference line with the given index
    //!
    //! \tparam Direction Downstream or Upstream Determines the iterator type
    //! \param point Forward or reverse iterator to a point on this reference line
    //! \return const XY&
    template <Direction D = Direction::Downstream>
    const XY &GetLongitudinalAxis(ConstIterator<Container<Point>, D> point) const;

    mutable std::vector<std::unique_ptr<XY>> longitudinalAxes;

    const std::optional<XY> &GetLongitudinalAxisIntersection(size_t) const;

    ST LocalizeUsingTAxes(const XY &) const;

    //! \brief Lazy-evaluated intersection of the start and end point's longitudinal axis for each edge.
    //! Used for point localization/globalization in OSI 3.6 and up.
    mutable std::vector<std::unique_ptr<std::optional<XY>>> intersections;
};

std::ostream &operator<<(std::ostream &, const ReferenceLine &);

OSIQL_GET(ReferenceLine, GetReferenceLine(), referenceLine, GetRoad(), road);
OSIQL_HAS(ReferenceLine, GetReferenceLine);
} // namespace osiql
