/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Wrapper of an OSI logical lane

#include <type_traits>
#include <vector>

#include <osi3/osi_logicallane.pb.h>

#include "OsiQueryLibrary/Component/Overlapable.h"
#include "OsiQueryLibrary/Point/Assignment.h"
#include "OsiQueryLibrary/Street/Lane/BoundaryEnclosure.h"
#include "OsiQueryLibrary/Trait/Placement.h"
#include "OsiQueryLibrary/Trait/Size.h" // OSIQL_SET_SIZE
#include "OsiQueryLibrary/Types/Enum.h" // Traversal & Selection
#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Has.h"
// To specialize Get<Lane>, Has<Lane> must be fully defined for clang-diagnostic checks to be satisfied:
#include "OsiQueryLibrary/Utility/Has/Lane.tpp"

namespace osiql {
template <typename>
struct Pose;
struct RoadMarking;
template <typename, typename>
struct Point;
struct Road;
struct TrafficLight;
struct TrafficSign;

OSIQL_GET(Lane, GetLane(), lane);
OSIQL_GET(Overlaps<Lane>, template GetOverlaps<Lane>(), laneOverlaps, overlaps);

//! \brief A wrapper for a osi3::LogicalLane that offers multiple utility methods and access to assigned objects
struct Lane : public detail::BoundaryEnclosure<Lane>, public Overlapable<Lane>
{
    static constexpr const std::string_view name = "Lane";

    using detail::BoundaryEnclosure<Lane>::BoundaryEnclosure;

    //! \brief The type of a lane
    enum class Type : std::underlying_type_t<osi3::LogicalLane_Type>
    {
        Undefined = osi3::LogicalLane_Type_TYPE_UNKNOWN,             // 0
        Other = osi3::LogicalLane_Type_TYPE_OTHER,                   // 1
        Normal = osi3::LogicalLane_Type_TYPE_NORMAL,                 // 2
        Biking = osi3::LogicalLane_Type_TYPE_BIKING,                 // 3
        Sidewalk = osi3::LogicalLane_Type_TYPE_SIDEWALK,             // 4
        Parking = osi3::LogicalLane_Type_TYPE_PARKING,               // 5
        Stop = osi3::LogicalLane_Type_TYPE_STOP,                     // 6
        Restricted = osi3::LogicalLane_Type_TYPE_RESTRICTED,         // 7
        Border = osi3::LogicalLane_Type_TYPE_BORDER,                 // 8
        Shoulder = osi3::LogicalLane_Type_TYPE_SHOULDER,             // 9
        Exit = osi3::LogicalLane_Type_TYPE_EXIT,                     // 10
        Entry = osi3::LogicalLane_Type_TYPE_ENTRY,                   // 11
        OnRamp = osi3::LogicalLane_Type_TYPE_ONRAMP,                 // 12
        OffRamp = osi3::LogicalLane_Type_TYPE_OFFRAMP,               // 13
        ConnectingRamp = osi3::LogicalLane_Type_TYPE_CONNECTINGRAMP, // 14
        Median = osi3::LogicalLane_Type_TYPE_MEDIAN,                 // 15
        Curb = osi3::LogicalLane_Type_TYPE_CURB,                     // 16
        Rail = osi3::LogicalLane_Type_TYPE_RAIL,                     // 17
        Tram = osi3::LogicalLane_Type_TYPE_TRAM                      // 18
    };

    //! Returns the type of this lane, which designates the intended kinds of vehicles and manners of navigation on them.
    //!
    //! \return Type
    Type GetType() const;

    //! Returns the street name of this lane
    //!
    //! \return "UNDEFINED_STREET_NAME" if not assigned or if using OSI 3.5.0
    std::string_view GetStreetName() const;

    //! Returns an unsorted container of transformed placements of objects of the
    //! specified type assigned to this lane that satisfy the given predicate.
    //!
    //! \tparam Object Type of the returned objects
    //! \tparam Pred Invocable with the signature bool(const Placement<Object>&).
    //! \tparam Transform Invocable receiving a const Placement<Object>& and returning
    //! a value to be included in the returned container.
    //! \return Object placements assigned to this lane satisfying the given predicate.
    //! If no custom predicate or transformer is passed, a reference to a container is returned.
    template <typename Object, typename Pred = osiql::Return<true>, typename Transform = osiql::Get<Placement<Object> *>>
    constexpr decltype(auto) GetAll(Pred && = {}, Transform && = {}) const;

    //! Returns an unsorted container of placements of objects of the specified type assigned to this lane.
    //!
    //! \tparam Object Type of the returned objects
    //! \return Object placements assigned to this lane satisfying the given predicate.
    //! If no custom predicate or transformer is passed, a reference to a container is returned.
    template <typename Object>
    constexpr decltype(auto) GetAll();

    //! Inserts all transformed object placements that satisfy the given predicate to the given output iterator,
    //! akin to std::transform.
    //!
    //! \tparam Object Type of the returned objects
    //! \tparam Pred Invocable with the signature bool(const Placement<Object>&).
    //! \tparam OutputIt Iterator pointing to the next place of insertion. The iterator is incremented after an insertion
    //! and must continue pointing to a writable address, like a std::back_insert_iterator (std::back_inserter(std::vector))
    //! \return Iterator pointer past the last insertion
    template <typename Object, typename Pred = osiql::Return<true>, typename Transform = osiql::Get<Placement<Object> *>, typename OutputIt = void>
    OutputIt InsertAll(OutputIt, Pred && = {}, Transform && = {}) const;

    //! Returns a collection of transformed object placements assigned to this lane that satisfy
    //! the given predicate.
    //!
    //! \tparam Type Type of the object whose transformed placements will be returned.
    //! \tparam Predicate Invocable with the signature bool(const Placement<Type>&).
    //! Placements not satisfying this predicate are ignored.
    //! \tparam Comp Invocable with the signature bool(const Placement<Type>*, const Placement<Type>*)
    //! or bool(const Placement<Type>&, const Placement<Type>&) if the Predicate is of type Return<true>
    //! Used to sort the placements in order to return the first n matching items.
    //! \tparam Transform Invocable taking a const Placement<Type>& and returning a value that will
    //! be inserted into the returned collection.
    //! \param n Maximum number of objects to find
    //! \return Collection of transformed object placements assigned to this lane that satisfy the given predicate
    template <typename Type,                            //
              typename Predicate = osiql::Return<true>, //
              typename Comp = Less<U>,                  //
              typename Transform = osiql::Get<Placement<Type> *>>
    constexpr Transformations<Transform, Placement<Type> &&> Find(size_t n = 1, Predicate && = {}, Comp && = {}, Transform && = {}) const;

    //! Inserts the sorted transformed object placements satisfying the given predicate to the given output iterator
    //!
    //! \tparam Type Type of the object whose transformed placements will be returned.
    //! \tparam Predicate Invocable with the signature bool(const Placement<Type>&, const Placement<Type>&).
    //! Used to sort the placements in order to return the first n matching items.
    //! \tparam Comp Invocable with the signature bool(const Placement<Type>*, const Placement<Type>*).
    //! Used to sort the placements in order to return the first n matching items.
    //! \tparam Transform Invocable taking a const Placement<Type>& and returning a value that will
    //! be inserted into the returned collection.
    //! \tparam OutputIt Iterator pointing to a transformed object placement
    //! \param n Maximum number of transformed object placements to insert
    //! \return Iterator pointing past the last inserted transformed object placement
    template <typename Type,                                      //
              typename Predicate = osiql::Return<true>,           //
              typename Comp = Less<U>,                            //
              typename Transform = osiql::Get<Placement<Type> *>, //
              typename OutputIt = void>
    constexpr OutputIt Insert(OutputIt, size_t n = 1, Predicate && = {}, Comp && = {}, Transform && = {}) const;

    //! Returns the width of the lane at the given relative s-coordinate. If it lies outside this lane's length,
    //! connected lanes are traversed and the width of the first found lane at that distance is returned.
    //! Returns zero if no connected lane exists at that distance.
    //!
    //! \param s s-coordinate
    //! \return double
    double GetWidth(double s) const;

    //! Returns the shortest distance between the given global point and this lane.
    //! If the point lies inside the lane, returns 0.0.
    //!
    //! \param XY global xy-coordinates
    //! \return double
    double GetDistanceTo(const XY &) const;

    //! Returns an s,t-coordinate pair relative to this lane describing the given global point.
    //!
    //! \param point
    //! \return Point<const Lane, ST>
    Point<const Lane, ST> Localize(const XY &point) const;

    //! Returns an s,t-coordinate pair with an angle relative to this lane describing the given global point and yaw.
    //!
    //! \return Pose<Point<const Lane, ST>>
    Pose<Point<const Lane, ST>> Localize(const Pose<XY> &) const;

    //! Returns the side in regards to the given orientation on this lane and the number of lane changes from this lane
    //! to the given lane, assuming both are on the same road. If there is no lane change, the returned side is always Left.
    //!
    //! \param Lane Target lane assumed to be on the same road as this lane. Still works on lanes of different roads,
    //! \param Traversal If Forward, then the returned side is relative to this lane's driving direction
    //! but the result is only useful if the layout of the other road's adjacent lanes is the same as this one's.
    //! \return std::pair<Side, size_t>
    std::pair<Side, size_t> GetLaneChangesTo(const Lane &, Traversal = Traversal::Forward) const;

    //! Returns the openDRIVE id of this lane. Its signedness represents whether it is on the right
    //! side of the road and its magnitude corresponds to the number of lanes between this lane's
    //! right boundary (in driving direction) and the reference line of this lane's road.
    //!
    //! \return int
    int GetOpenDriveId() const;

    // NOTE: osiql does not currently use osi3::LogicalLaneAssignments for MovingObjects

    //! \brief Assignments of road markings to this lane
    std::vector<Assignment<RoadMarking>> markings;

    //! \brief Assignments of static objects to this lane
    std::vector<Assignment<StationaryObject>> objects;

    //! \brief Assignments of traffic lights to this lane
    std::vector<Assignment<TrafficLight>> lights;

    //! \brief Assignments of traffic signs to this lane
    std::vector<Assignment<TrafficSign>> signs;

private:
    friend std::ostream &operator<<(std::ostream &, const Lane &);
};

std::ostream &operator<<(std::ostream &, Lane::Type);

std::ostream &operator<<(std::ostream &, const Lane &);

OSIQL_GET(Lane::Type, GetType(), type, GetLane(), lane);
} // namespace osiql
OSIQL_SET_SIZE(osiql::Lane::Type, 19)
