/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Street/ReferenceLine.h"

#include "OsiQueryLibrary/Component/Identifiable.tpp"
#include "OsiQueryLibrary/Component/Points.tpp"
#include "OsiQueryLibrary/Point/Coordinates.tpp"
#include "OsiQueryLibrary/Point/Pose.tpp"

namespace osiql {
template <typename It>
decltype(auto) ReferenceLine::Localize(It first, It pastLast) const
{
    std::vector<std::decay_t<decltype(std::declval<ReferenceLine>().Localize(*std::declval<It>()))>> result;
    const size_t size{static_cast<size_t>(std::distance(first, pastLast))};
    if (size)
    {
        result.reserve(size);
        std::transform(first, pastLast, std::back_inserter(result), [this](const auto &point) {
            return this->Localize(point);
        });
    }
    return result;
}

namespace detail {
template <typename Input>
constexpr bool HasType(const Input &referenceLine)
{
    if constexpr (GetOSIVersion() >= Version{3, 6, 0}) // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    {
        return referenceLine.GetHandle().has_type();
    }
    else
    {
        return false;
    }
}

template <typename Input>
constexpr typename ReferenceLine::Type GetType(const Input &referenceLine)
{
    if constexpr (GetOSIVersion() >= Version{3, 6, 0}) // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    {
        return static_cast<typename Input::Type>(referenceLine.GetHandle().type());
    }
    else
    {
        return ReferenceLine::Type::Polyline;
    }
}

template <typename Point>
constexpr inline bool HasTAxisYaw(const Point &input)
{
    if constexpr (GetOSIVersion() >= Version{3, 6, 0}) // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    {
        return input.has_t_axis_yaw();
    }
    else
    {
        return false;
    }
}

template <typename Point>
inline double GetTAxisYaw(const Point &input)
{
    if constexpr (GetOSIVersion() >= Version{3, 6, 0}) // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    {
        return input.t_axis_yaw();
    }
    else
    {
        return std::numeric_limits<double>::quiet_NaN();
    }
}
} // namespace detail

constexpr bool ReferenceLine::HasType() const
{
    return detail::HasType(*this);
}

constexpr ReferenceLine::Type ReferenceLine::GetType(ReferenceLine::Type fallback) const
{
    if constexpr (GetOSIVersion() >= Version{3, 6, 0}) // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    {
        return HasType() ? detail::GetType(*this) : fallback;
    }
    else
    {
        return fallback;
    }
}

template <Direction D>
const XY &ReferenceLine::GetLongitudinalAxis(ConstIterator<Container<typename ReferenceLine::Point>, D> it) const
{
    if constexpr (D == Direction::Downstream)
    {
        std::unique_ptr<XY> &result{*std::next(osiql::begin<D>(longitudinalAxes), std::distance(begin<D>(), it))};
        if (!result)
        {
            if (detail::HasTAxisYaw(*it))
            {
                result = std::make_unique<XY>(XY{std::cos(detail::GetTAxisYaw(*it)), std::sin(detail::GetTAxisYaw(*it))});
            }
            // Set t-axis to the normalized counter-clockwise normal of the s-axis
            else if (it == begin<D>())
            {
                result = std::make_unique<XY>(GetPerpendicular<Winding::CounterClockwise>(Norm(as<XY>(*std::next(it)) - as<XY>(*it))));
            }
            else if (std::next(it) == end<D>())
            {
                result = std::make_unique<XY>(GetPerpendicular<Winding::CounterClockwise>(Norm(XY{*it} - XY{*std::prev(it)})));
            }
            else
            {
                result = std::make_unique<XY>(GetPerpendicular<Winding::CounterClockwise>(Norm(Norm(XY{*it} - XY{*std::prev(it)}) + Norm(XY{*std::next(it)} - XY{*it}))));
            }
        }
        return *result;
    }
    else if constexpr (D == Direction::Upstream)
    {
        std::unique_ptr<XY> &result{*std::next(osiql::begin<D>(longitudinalAxes), std::distance(begin<D>(), it))};
        if (!result)
        {
            if (detail::HasTAxisYaw(*it))
            {
                result = std::make_unique<XY>(XY{std::cos(detail::GetTAxisYaw(*it)), std::sin(detail::GetTAxisYaw(*it))});
            }
            else // Set t-axis to the normalized counter-clockwise normal of the s-axis
            {
                result = std::make_unique<XY>(GetPerpendicular<Winding::CounterClockwise>(Norm((std::next(it) == end<D>()) ? (*std::prev(it) - *it) : (*it - *std::next(it)))));
            }
        }
        return *result;
    }
    else
    {
        static_assert(always_false<decltype(D)>, "Direction must be Downstream or Upstream");
    }
}

template <Direction D>
ConstIterator<Container<typename ReferenceLine::Point>, D> ReferenceLine::begin(const XY &point) const
{
    if constexpr (GetOSIVersion() >= Version{3, 6, 0}) // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    {
        auto it{Base::begin<D>(point)};
        if (GetType(Type::PolylineWithTAxis) == Type::Polyline)
        {
            return it;
        }
        while ((it != begin<D>()) && (point.GetSide(*it, *it + GetLongitudinalAxis<D>(it)) == !GetSide(D)))
        {
            --it;
        }
        if (point.GetSide(*it, *it + GetLongitudinalAxis<D>(it)) == !GetSide(D))
        {
            return it;
        }
        ++it;
        while ((std::next(it) != end<D>()) && (point.GetSide(*it, *it + GetLongitudinalAxis<D>(it)) != !GetSide(D)))
        {
            ++it;
        }
        return --it;
    }
    else
    {
        return Base::begin<D>(point);
    }
}

template <Direction D>
ConstIterator<Container<typename ReferenceLine::Point>, D> ReferenceLine::end(const XY &point) const
{
    if constexpr (GetOSIVersion() >= Version{3, 6, 0}) // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    {
        auto it{Base::end<D>(point)};
        if (GetType(Type::PolylineWithTAxis) == Type::Polyline)
        {
            return it;
        }
        while ((std::next(it) != end<D>()) && (point.GetSide(*it, *it + GetLongitudinalAxis<D>(it)) != !GetSide(D)))
        {
            ++it;
        }
        if (point.GetSide(*it, *it + GetLongitudinalAxis<D>(it)) != !GetSide(D))
        {
            return it;
        }
        --it;
        while ((it != begin<D>()) && (point.GetSide(*it, *it + GetLongitudinalAxis<D>(it)) == !GetSide(D)))
        {
            --it;
        }
        return std::next(it);
    }
    else
    {
        return Base::end<D>(point);
    }
}
} // namespace osiql
