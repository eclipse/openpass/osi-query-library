/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Wrapper of an OSI logical lane

#include "OsiQueryLibrary/Street/Lane.h"

#include "OsiQueryLibrary/Component/Overlapable.tpp"
#include "OsiQueryLibrary/Object/MovingObject.h"
#include "OsiQueryLibrary/Street/Lane/BoundaryEnclosure.tpp"
#include "OsiQueryLibrary/Street/Road.h"
#include "OsiQueryLibrary/Types/Enum.tpp"
#include "OsiQueryLibrary/Types/Overlap.tpp"

namespace osiql {
struct Vehicle;

template <typename Object, typename Pred, typename Transform>
constexpr decltype(auto) Lane::GetAll(Pred &&pred, Transform &&transform) const
{
    if constexpr (std::is_same_v<Pred, osiql::Return<true>>)
    {
        if constexpr (std::is_same_v<Transform, osiql::Get<Placement<Object> *>>)
        {
            if constexpr (std::is_same_v<Object, Vehicle>)
            {
                return GetAll<MovingObject>(Is{MovingObject::Type::Vehicle}, std::forward<Transform>(transform));
            }
            else if constexpr (std::is_same_v<Object, MovingObject>)
            {
                return (GetOverlapping<MovingObject>());
            }
            else if constexpr (std::is_same_v<Object, RoadMarking>)
            {
                return (markings);
            }
            else if constexpr (std::is_same_v<Object, StationaryObject>)
            {
                return (GetOverlapping<StationaryObject>());
            }
            else if constexpr (std::is_same_v<Object, TrafficLight>)
            {
                return (lights);
            }
            else if constexpr (std::is_same_v<Object, TrafficSign>)
            {
                return (signs);
            }
            else
            {
                static_assert(always_false<Object>, "Lane::GetAll does not store objects of the given type. Only MovingObject, RoadMarking, StationaryObject, TrafficLight and TrafficSign are supported.");
            }
        }
        else
        {
            std::vector<std::invoke_result_t<Transform, const Placement<Object> &>> result;
            InsertAll<Object>(std::back_inserter(result), {}, std::forward<Transform>(transform));
            return result;
        }
    }
    else
    {
        std::vector<std::invoke_result_t<Transform, const Placement<Object> &>> result;
        InsertAll<Object>(std::back_inserter(result), std::forward<Pred>(pred), std::forward<Transform>(transform));
        return result;
    }
}

template <typename Object>
constexpr decltype(auto) Lane::GetAll()
{
    static_assert(!std::is_same_v<Object, Vehicle>);
    return const_cast< // clang-format off
        std::add_lvalue_reference_t<
            std::remove_const_t<
                std::remove_reference_t<
                    decltype(std::declval<const Lane>().GetAll<Object>())
                >
            >
        >
    >(std::as_const(*this).GetAll<Object>()); // clang-format on
}

template <typename Object, typename Pred, typename Transform, typename OutputIt>
OutputIt Lane::InsertAll(OutputIt output, Pred &&pred, Transform &&transform) const
{
    for (const auto &item : GetAll<Object>())
    {
        if constexpr (std::is_same_v<Pred, osiql::Return<true>>)
        {
            (output++) = transform(get<Placement<Object>>(item));
        }
        else
        {
            if (pred(get<Placement<Object>>(item)))
            {
                (output++) = transform(get<Placement<Object>>(item));
            }
        }
    }
    return output;
}

template <typename Type, typename Predicate, typename Comp, typename Transform>
constexpr Transformations<Transform, Placement<Type> &&> Lane::Find(
    size_t n,
    Predicate &&pred,
    Comp &&comp,
    Transform &&transform
) const
{
    Transformations<Transform, Placement<Type> &&> result;
    Insert<Type>(std::back_inserter(result), n, std::forward<Predicate>(pred), std::forward<Comp>(comp), std::forward<Transform>(transform));
    return result;
}

template <typename Type, typename Predicate, typename Comp, typename Transform, typename OutputIt>
constexpr OutputIt Lane::Insert(OutputIt output, size_t n, Predicate &&pred, Comp &&comp, Transform &&transform) const
{
    decltype(auto) all{GetAll<Type>(std::forward<Predicate>(pred))};
    if (all.size() > n)
    {
        std::partial_sort(all.begin(), std::next(all.begin(), static_cast<std::ptrdiff_t>(n)), all.end());
        const auto pastLast{std::next(all.begin(), static_cast<std::ptrdiff_t>(n))};
        for (auto item{all.begin()}; item != pastLast; ++item)
        {
            *(output++) = transform(get<Placement<Type>>(*item));
        }
    }
    else
    {
        std::sort(all.begin(), all.end(), std::forward<Comp>(comp));
        for (auto item{all.begin()}; item != all.end(); ++item)
        {
            *(output++) = transform(get<Placement<Type>>(*item));
        }
    }
    return output;
}

namespace detail {
constexpr std::array<std::string_view, Size<Lane::Type>> laneTypeToString{
    "Undefined",
    "Other",
    "Normal",
    "Biking",
    "Sidewalk",
    "Parking",
    "Stop",
    "Restricted",
    "Border",
    "Shoulder",
    "Exit",
    "Entry",
    "On Ramp",
    "Off Ramp",
    "Connecting Ramp",
    "Median",
    "Curb",
    "Rail",
    "Tram",
};

constexpr std::string_view undefinedStreetName{"UNDEFINED_STREET_NAME"};

template <typename Lane>
inline std::string_view GetStreetName(const Lane *lane)
{
    if constexpr (GetOSIVersion() >= Version{3, 6, 0}) // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    {
        return lane->GetHandle().has_street_name() ? lane->GetHandle().street_name() : undefinedStreetName;
    }
    else
    {
        return undefinedStreetName;
    }
}
} // namespace detail
} // namespace osiql
