/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Adds the function objects ConnectedNodes which returns the connected nodes of the node it is
//! invoked on, and NextNode, which is used to tell a node graph in what order it should be traversed

#include <set>

#include "OsiQueryLibrary/Types/Enum.h" // Traversal
#include "OsiQueryLibrary/Utility/Compare.h"
#include "OsiQueryLibrary/Utility/Get.tpp"

namespace osiql {
template <Traversal, typename>
struct Node;

struct Depth
{
};
OSIQL_SET_RETURN_TYPE(Depth, size_t)
OSIQL_GET(Depth, depth, GetDepth());

//! Function object returning the connected nodes of a given node when invoked.
//!
//! \tparam Traversal The direction of the connection
template <Traversal, typename Predicate = Return<true>>
struct ConnectedNodes
{
    //! Returns the connected nodes of the given node that satisfy this class's predicates
    //!
    //! \tparam T Traversal direction of the give node
    //! \tparam Scope Lane or Road. The scope of the given node
    //! \return Vector of shared pointers of constant nodes. Reference if this class has no predicates
    template <Traversal T, typename Scope>
    constexpr decltype(auto) operator()(const Node<T, Scope> &) const;

    Predicate predicate;
};

//! Function object that can be passed to Node methods
//! to determine in what order its successor nodes are traversed.
//!
//! It maintains a set of nodes and pops the first element when invoked.
//! The nodes returned by the given selector are added to the queue.
//! If there are no elements left in the queue, a nullptr is returned.
//!
//! \tparam Comparator How the upcoming nodes are ordered. Receives two Node pointers
//! and returns whether the first compares as less than the second.
//! \tparam NodeType A node with a certain traversal direction and scope type
template <typename Comparator, typename NodeType>
struct NextNode
{
    const NodeType *operator()();

    template <typename Nodes>
    const NodeType *operator()(Nodes &&);

private:
    std::set<const NodeType *, Comparator> queue;
};
} // namespace osiql
