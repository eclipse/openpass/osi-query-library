/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <deque>
#include <vector>

#include "Node.h"
#include "OsiQueryLibrary/Component/Iterable.h"
#include "OsiQueryLibrary/Point/Point.h"
#include "OsiQueryLibrary/Trait/Localization.h"
#include "OsiQueryLibrary/Types/Distance.h"
#include "OsiQueryLibrary/Types/Enum.h" // Traversal & Selection
#include "OsiQueryLibrary/Types/Interval.h"

namespace osiql {
struct Lane;
template <typename>
struct Pose;
template <size_t>
struct Vector;

template <Traversal T, typename Scope>
using StoredNode = std::shared_ptr<const Node<T, Scope>>;

//! \brief A local origin point, destination point and
//! a chain of nodes, one for each traversed road
template <Traversal T = Traversal::Forward, typename Scope = Road>
struct Route : public Iterable<std::vector<StoredNode<T, Scope>>, Route<T>, Traversal>
{
    using Base = Iterable<std::vector<StoredNode<T, Scope>>, Route<T>, Traversal>;

    static constexpr Traversal orientation = T;

    //! Creates a route from its components.
    //!
    //! \param Origin The point at which the route begins. t-coordinate is ignored
    //! \param Destination The point at which the route ends. t-coordinate is ignored
    //! \param path Chain of nodes from the origin to the destination
    template <typename Origin, typename Destination>
    Route(Origin &&origin, Destination &&destination, std::vector<StoredNode<T, Scope>> &&path);

    //! Creates a route of length zero.
    //!
    //! \param Point Both the origin and destination of the route.
    Route(const Point<> &point);

    //! Creates a route that follows the shortest path from the given
    //! origin to the given destination.
    //!
    //! \tparam OriginLaneType Either const Lane or Lane
    //! \tparam DestinationLaneType Either const Lane or Lane
    //! \param origin Starting point of the route. t-coordinate is ignored
    //! \param destination End point of the route. t-coordinate is ignored
    template <typename OriginLaneType, typename DestinationLaneType>
    Route(const Point<OriginLaneType> &, const Point<DestinationLaneType> &);

    //! Creates a route passing through the given points
    //!
    //! \tparam It Iterator pointing to a local point
    //! \param first
    //! \param pastLast
    template <typename It, OSIQL_REQUIRES(std::is_same_v<typename std::iterator_traits<It>::iterator_category, std::random_access_iterator_tag>)>
    Route(It first, It pastLast) :
        origin{*first}, destination{*std::prev(pastLast)}
    {
        assert(first != pastLast);
        const auto root{Node<T, Scope>::Create(origin)};
        const Node<T, Scope> *checkpoint{root.get()};
        for (auto it{std::next(first)}; it != pastLast; ++it)
        {
            const Node<T, Scope> *nextCheckpoint{checkpoint->FindClosestNode(*it)};
            if (!nextCheckpoint)
            {
                break;
            }
            checkpoint = nextCheckpoint;
        }
        path = checkpoint->GetPathFromDepth(0);
    }

    using Base::begin;
    ConstIterator<std::vector<StoredNode<T, Scope>>> begin() const;
    Iterator<std::vector<StoredNode<T, Scope>>> begin();

    using Base::end;
    ConstIterator<std::vector<StoredNode<T, Scope>>> end() const;
    Iterator<std::vector<StoredNode<T, Scope>>> end();

    //! Returns the total longitudinal distance between this route's origin and destination.
    //!
    //! \return double [0, ∞)
    double GetLength() const;

    //! Returns the s-coordinate distance from the start of this route to the given point. If the point does not lie
    //! on any road of this route, returns NaN instead.
    //!
    //! \tparam Type MovingObject, StationaryObject or Point
    //! \return double if a single point, Interval<double> if the input is a moving or static object
    template <typename Type>
    decltype(auto) GetDistance(const Type &) const;

    //! Returns the shortest s-coordinate difference between the two given objects/points.
    //!
    //! \tparam From Type supported by GetDistance
    //! \tparam To Type supported by GetDistance
    //! \param From The origin from which the distance is measured.
    //! \param To The destination to which the distance is measured.
    //! \return Negative value if the origin is further along the route than the destination.
    //! Zero if the objects partially overlap in their s-coordinate.
    //! NaN if origin or destination are not on this route.
    template <typename From, typename To>
    double GetDistanceBetween(const From &, const To &) const;

    //! Returns an iterator to the closest node of this route
    //! to the given distance from the start of this route.
    //!
    //! \param distance
    //! \return std::vector<Node<T, Scope>*>::const_iterator
    ConstIterator<std::vector<StoredNode<T, Scope>>> FindClosestNode(double distance) const;

    //! Returns an iterator pointing to the road point of this
    //! route on the road closest to the given global point.
    //!
    //! \param globalPoint
    //! \return std::vector<Node<T, Scope>*>::const_iterator
    ConstIterator<std::vector<StoredNode<T, Scope>>> FindClosestNode(const XY &globalPoint) const;

    //! Returns a local point at the given distance from this route's starting point.
    //! The t-coordinate of the returned point will be NaN.
    //!
    //! \param distance
    //! \return Local point with NaN as a t-coordinate
    Point<> Localize(double distance) const;

    //! Returns a local representation to the closest road on this route of input.
    //!
    //! \tparam Global xy-coordinates, optionally with a global yaw.
    //! \return Localization of the input
    template <typename GlobalPoint>
    Localization<GlobalPoint> Localize(const GlobalPoint &) const;

    //! Returns all node relations to object placements of the specified type along this route
    //! that satisfy the given predicate sorted by their distance.
    //!
    //! \tparam Type Type of the objects whose placements will be returned
    //! \tparam Traversal The traversal direction relative to the direction of this route (Forward or Backward)
    //! \tparam Selection Which placement will be returned per object if it lies on multiple roads
    //! of the route. (Each, First or Last).
    //! \tparam Predicate Unary predicate with the signature bool(const Location<Type, Node<T, Scope>>&)
    //! \tparam Comparator Invocable used to sort results within each node, but not between nodes
    //! \return Sorted transformed object placement relations along this route satisfying the given predicate
    template <typename Type,                                                                                                     //
              Traversal Toward = Traversal::Forward,                                                                             //
              Selection = Selection::Each,                                                                                       //
              typename Predicate = Return<true>,                                                                                 //
              typename Comparator = typename std::conditional_t<IsInverse(Toward), Greater<Max<Distance>>, Less<Min<Distance>>>, //
              typename Transform = Get<Location<Type, Node<T, Scope>>>>
    Transformations<Transform, Location<Type, Node<T, Scope>> &&> FindAll(Predicate && = {}, Comparator && = {}, Transform && = {}) const;

    template <typename Type,                                                                                                     //
              Traversal Toward = Traversal::Forward,                                                                             //
              Selection = Selection::Each,                                                                                       //
              typename Predicate = Return<true>,                                                                                 //
              typename Comparator = typename std::conditional_t<IsInverse(Toward), Greater<Max<Distance>>, Less<Min<Distance>>>, //
              typename Transform = Get<Location<Type, Node<T, Scope>>>,                                                          //
              typename OutputIt = void>
    OutputIt InsertAll(OutputIt, Predicate && = {}, Comparator && = {}, Transform && = {}) const;

    template <typename Type,                                                                                                     //
              Traversal Toward = Traversal::Forward,                                                                             //
              Selection = Selection::Each,                                                                                       //
              typename Predicate = Return<true>,                                                                                 //
              typename Comparator = typename std::conditional_t<IsInverse(Toward), Greater<Max<Distance>>, Less<Min<Distance>>>, //
              typename Transform = Get<Location<Type, Node<T, Scope>>>>
    Transformations<Transform, Location<Type, Node<T, Scope>> &&> FindAllBetween(Interval<double> range, Predicate && = {}, Comparator && = {}, Transform && = {}) const;

    template <typename Type,                                                                                                     //
              Traversal Toward = Traversal::Forward,                                                                             //
              Selection = Selection::Each,                                                                                       //
              typename Predicate = Return<true>,                                                                                 //
              typename Comparator = typename std::conditional_t<IsInverse(Toward), Greater<Max<Distance>>, Less<Min<Distance>>>, //
              typename Transform = Get<Location<Type, Node<T, Scope>>>,                                                          //
              typename OutputIt = void>
    OutputIt InsertAllBetween(
        OutputIt, Interval<double> range, Predicate && = {}, Comparator && = {}, Transform && = {}
    ) const;

    //! Returns the placement of the first object of the specified type along this route that satisfies
    //! the given predicates.
    //!
    //! \tparam Type Type of the objects whose placements will be returned
    //! \tparam Traversal The traversal direction relative to the direction of this route (Forward or Backward)
    //! \tparam Selection Which placement of the found object should be returned if multiple satisfy all predicates
    //! due to the object overlapping with more than one road of the route. (First or Last).
    //! (Selection::Each will be treated as Selection::First)
    //! \tparam Predicate Invocable with the signature bool(const Location<Type, Node<T, Scope>>&).
    //! \return Object placements sorted by ascending distance in the order they were traversed
    template <typename Type,                                                                                                     //
              Traversal Toward = Traversal::Forward,                                                                             //
              Selection = Selection::Each,                                                                                       //
              typename Predicate = Return<true>,                                                                                 //
              typename Comparator = typename std::conditional_t<IsInverse(Toward), Greater<Max<Distance>>, Less<Min<Distance>>>, //
              typename Transform = Get<Location<Type, Node<T, Scope>>>>
    Transformations<Transform, Location<Type, Node<T, Scope>> &&> Find(size_t n, Predicate && = {}, Comparator && = {}, Transform && = {}) const;

    template <typename Type,                                                                                                     //
              Traversal Toward = Traversal::Forward,                                                                             //
              Selection = Selection::Each,                                                                                       //
              typename Predicate = Return<true>,                                                                                 //
              typename Comparator = typename std::conditional_t<IsInverse(Toward), Greater<Max<Distance>>, Less<Min<Distance>>>, //
              typename Transform = Get<Location<Type, Node<T, Scope>>>,                                                          //
              typename OutputIt = void>
    OutputIt Insert(
        OutputIt, size_t n, Predicate && = {}, Comparator && = {}, Transform && = {}
    ) const;

    template <typename Type,                                                                                                     //
              Traversal Toward = Traversal::Forward,                                                                             //
              Selection = Selection::Each,                                                                                       //
              typename Predicate = Return<true>,                                                                                 //
              typename Comparator = typename std::conditional_t<IsInverse(Toward), Greater<Max<Distance>>, Less<Min<Distance>>>, //
              typename Transform = Get<Location<Type, Node<T, Scope>>>>
    Transformations<Transform, Location<Type, Node<T, Scope>> &&> FindBetween(size_t n,               //
                                                                              Interval<double> range, //
                                                                              Predicate && = {},      //
                                                                              Comparator && = {},     //
                                                                              Transform && = {}) const;

    template <typename Type,                                                                                                     //
              Traversal Toward = Traversal::Forward,                                                                             //
              Selection = Selection::Each,                                                                                       //
              typename Predicate = Return<true>,                                                                                 //
              typename Comparator = typename std::conditional_t<IsInverse(Toward), Greater<Max<Distance>>, Less<Min<Distance>>>, //
              typename Transform = Get<Location<Type, Node<T, Scope>>>,                                                          //
              typename OutputIt = void>
    OutputIt InsertBetween(OutputIt,               //
                           size_t n,               //
                           Interval<double> range, //
                           Predicate && = {},      //
                           Comparator && = {},     //
                           Transform && = {}) const;

    //! Returns the chain of successor and predecessor lanes from the given lane along this route.
    //! Returns an empty container if the given lane does not lie on the route.
    //!
    //! \return std::deque<const Lane*>
    std::deque<const Lane *> GetLaneChain(const Lane &) const;

    //! Returns the range of nodes that overlap with the given interval
    //! of distances.
    //!
    //! \tparam Toward The direction of the returned iterators
    //! \return Interval consisting of an iterator to the first overlapping node
    //! and an iterator pointing past the last overlapping node.
    template <Traversal Toward = Traversal::Forward>
    Interval<ConstIterator<std::vector<StoredNode<T, Scope>>, Toward>> GetNodesBetween(Interval<double> range) const
    { // NOTE: Due to a cryptic MinGW compiler bug, this implementation can not be separated from its declaration
        using Beyond = std::conditional_t<IsInverse(Toward), Less<Min<Distance>>::Than<>, Greater<Max<Distance>>::Than<>>;
        using Behind = std::conditional_t<IsInverse(Toward), Greater<Max<Distance>>::Than<>, Less<Min<Distance>>::Than<>>;
        return this->template GetMatchingRange<Toward>(Beyond{extract<Toward>(range)}, Behind{extract<!Toward>(range)});
    }

    //! Returns the given point projected by the give distance along its assigned lane's centerline.
    //! If the distance extends past the point's lane, the projection continues on the lane's connected
    //! lane which is part of this route. If such a projection is possible, the returned point will
    //! have the same lateral distance from its assigned lane's centerline as the given point.
    //!
    //! \tparam LaneType Lane or const Lane
    //! \param origin The point to be projected the given distance along the route.
    //! \param distance Positive or negative distance the given point will be projected along the route
    //! \return std::nullopt if a lane change is required or the given point does not lie on the route.
    template <typename LaneType = const Lane>
    std::optional<Point<>> ProjectPoint(const Point<LaneType> &origin, double distance) const;

    // TODO: GetLaneNodes in opposite direction

    template <Traversal Toward = Traversal::Any>
    std::deque<std::shared_ptr<const Node<T, Lane>>> GetLaneNodes(const Lane &) const;

    template <Traversal Toward = Traversal::Any>
    std::deque<std::shared_ptr<const Node<T, Lane>>> GetLaneNodes(const Lane &, ConstIterator<Route<T>>) const;

    Point<> origin;
    Point<> destination;
    std::vector<StoredNode<T, Scope>> path;
};

template <Traversal T, typename Scope>
bool operator<(const Route<T, Scope> &, const Route<T, Scope> &);

// TODO: Support any global point, not just XY

template <typename RouteIterator>
RouteIterator FindClosestNode(RouteIterator first, RouteIterator pastLast, const XY &globalPoint);

template <typename RouteIterator>
RouteIterator FindClosestNode(RouteIterator first, RouteIterator pastLast, double distance);

template <typename SearchNodeIterator, typename Type>
decltype(auto) GetDistance(SearchNodeIterator first, SearchNodeIterator pastLast, const Type &);

template <typename RouteIterator, typename PointType>
Localization<PointType> Localize(RouteIterator first, RouteIterator pastLast, const PointType &globalPoint);

template <Traversal T, typename Scope>
std::ostream &operator<<(std::ostream &, const Route<T, Scope> &);

std::ostream &operator<<(std::ostream &, Selection);
} // namespace osiql
