/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Utilities.h"

#include "Node.h"

namespace osiql {
template <Traversal X, typename Predicate>
template <Traversal T, typename Scope>
constexpr decltype(auto) ConnectedNodes<X, Predicate>::operator()(const Node<T, Scope> &node) const
{
    return node.template GetConnectedNodes<X>(predicate);
}

template <typename Comparator, typename NodeType>
const NodeType *NextNode<Comparator, NodeType>::operator()()
{
    if (queue.empty())
    {
        return nullptr;
    }
    const auto iterator{queue.begin()};
    const NodeType *node{*iterator};
    queue.erase(iterator);
    return node;
}

template <typename Comparator, typename NodeType>
template <typename Nodes>
const NodeType *NextNode<Comparator, NodeType>::operator()(Nodes &&nodes)
{
    for (const auto &node : nodes)
    {
        queue.emplace(&get<NodeType>(node));
    }
    return this->operator()();
}
} // namespace osiql
