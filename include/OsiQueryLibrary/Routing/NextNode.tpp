/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "NextNode.h"

#include "Node.h"

namespace osiql {
template <typename Comparator, Traversal T, typename Scope>
constexpr NextNode<Comparator, T, Scope>::NextNode(const Node<T, Scope> &root) :
    queue{&root}
{
}

template <typename Comparator, Traversal T, typename Scope>
template <typename Selector>
const Node<T, Scope> *NextNode<Comparator, T, Scope>::operator()(Selector &&selector)
{
    if (queue.empty())
    {
        return nullptr;
    }
    const auto iterator{queue.begin()};
    const Node<T, Scope> *node{*iterator};
    queue.erase(iterator);
    for (const auto &child : selector(*node))
    {
        queue.emplace(child.get());
    }
    return node;
}

template <typename Comparator>
template <Traversal T, typename Scope>
constexpr bool CompareDepth<Comparator>::operator()(const Node<T, Scope> *a, const Node<T, Scope> *b) const
{
    if (a->depth == b->depth)
    {
        return a < b;
    }
    return Comparator{}(a->depth, b->depth);
}

template <typename Comparator>
template <Traversal T, typename Scope>
constexpr bool CompareEndDistance<Comparator>::operator()(const Node<T, Scope> *a, const Node<T, Scope> *b) const
{
    const double endDistanceA{a->distance + a->GetLength()};
    const double endDistanceB{b->distance + b->GetLength()};
    if (endDistanceA == endDistanceB)
    {
        return a < b;
    }
    return Comparator{}(endDistanceA, endDistanceB);
}
} // namespace osiql
