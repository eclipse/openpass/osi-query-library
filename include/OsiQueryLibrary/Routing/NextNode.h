/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Function object used to tell a search node graph in what order it should be traversed

#include <set>

#include "Node.h"
#include "OsiQueryLibrary/Types/Enum.h" // Traversal
#include "OsiQueryLibrary/Utility/Compare.h"

namespace osiql {
//! Function object that can be passed to Node methods
//! to determine in what order its successor nodes are traversed.
//!
//! It maintains a set of nodes and pops the first element when invoked.
//! The nodes returned by the given selector are added to the queue.
//! If there are no elements left in the queue, a nullptr is returned.
//!
//! \tparam Comparator How the upcoming nodes are ordered
//! \tparam T Traversal direction of the traversed nodes
//! \tparam Scope Lane or Road. Scope of the traversed nodes
template <typename Comparator, Traversal T = Traversal::Forward, typename Scope = Road>
struct NextNode
{
    constexpr NextNode(const Node<T, Scope> &root);

    template <typename Selector = ConnectedNodes<T>>
    const Node<T, Scope> *operator()(Selector && = {});

private:
    std::set<const Node<T, Scope> *, Comparator> queue;
};

//! Function object that compares the depth of two nodes when invoked.
//! In the case of equality, their pointer addresses are compared.
//!
//! \tparam Comparator Binary functor such as std::less<> or std::greater<>
template <typename Comparator>
struct CompareDepth
{
    template <Traversal T, typename Scope>
    constexpr bool operator()(const Node<T, Scope> *, const Node<T, Scope> *) const;
};

//! Function object that compares the end distance of two nodes when invoked.
//! In the case of equality, their pointer addresses are compared.
//!
//! \tparam Comparator Binary functor such as std::less<> or std::greater<>
template <typename Comparator>
struct CompareEndDistance
{
    template <Traversal T, typename Scope>
    constexpr bool operator()(const Node<T, Scope> *, const Node<T, Scope> *) const;
};

namespace detail {
template <Order>
struct NextNodeComparator;

template <>
struct NextNodeComparator<Order::BreadthFirst>
{
    using type = CompareDepth<std::less<>>;
};

template <>
struct NextNodeComparator<Order::DepthFirst>
{
    using type = CompareDepth<std::greater<>>;
};

template <>
struct NextNodeComparator<Order::MinDistance>
{
    using type = CompareEndDistance<std::less<>>;
};

template <>
struct NextNodeComparator<Order::MaxDistance>
{
    using type = CompareEndDistance<std::greater<>>;
};
} // namespace detail

//! The corresponding comparator type to the given search node graph traversal value
//!
//! \tparam Order The type of traversal
template <Order O>
using NextNodeComparator = typename detail::NextNodeComparator<O>::type;
} // namespace osiql
