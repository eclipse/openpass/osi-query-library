/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Node.h"

#include <algorithm>
#include <numeric>
#include <queue>

#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/Lane/Connectable.tpp"
#include "OsiQueryLibrary/Street/Road.h"
#include "OsiQueryLibrary/Types/Enum.tpp"
#include "OsiQueryLibrary/Types/Location.tpp"
#include "OsiQueryLibrary/Utility/Compare.h"
#include "OsiQueryLibrary/Utility/Pick.tpp"

namespace osiql {
template <Traversal T, typename Scope>
Node<T, Scope>::Node(const Lane &lane, double distance) :
    Has<const Lane *>{&lane}, distance{distance}
{
}

template <Traversal T, typename Scope>
template <typename Origin>
Node<T, Scope>::Node(const Origin &origin, const Lane &lane) :
    Has<const Lane *>{&lane}
{
    if constexpr (Are<Overlaps<Road>>::in<Origin>)
    {
        const auto &overlaps{get<Overlaps<Road>>(origin)};
        const auto overlap{std::find_if(overlaps.begin(), overlaps.end(), Matches<Road>(lane))};
        if (overlap == overlaps.end())
        {
            throw std::runtime_error("Node(const Lane&, const Origin&): Origin does not lie on the same Road as the given Lane. Unable to determine node distance from origin.");
        }
        InitializeDistance(*overlap);
    }
    else
    {
        InitializeDistance(origin);
    }
}

template <Traversal T, typename Scope>
Node<T, Scope>::Node(const Lane &lane, double distance, const Node<T, Scope> &parent) :
    Has<const Lane *>{&lane}, distance{distance}, parent{&parent}, depth{parent.depth + 1}
{
}

template <Traversal T, typename Scope>
Direction Node<T, Scope>::GetDirection() const
{
    return this->GetLane().GetDirection(T);
}

template <Traversal T, typename Scope>
template <typename Type>
decltype(auto) Node<T, Scope>::GetDistance(const Type &t) const
{
    if constexpr (Are<Overlaps<Road>>::in<Type>)
    {
        const auto &overlaps{get<Overlaps<Road>>(t)};
        const auto overlap{std::find_if(overlaps.begin(), overlaps.end(), Matches<Road>{*this})};
        return overlap != overlaps.end() ? GetDistance(*overlap) : Interval{std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};
    }
    else if constexpr (OSIQL_HAS_MEMBER(Type, s))
    {
        return GetDistance(t.s);
    }
    else if constexpr (OSIQL_HAS_MEMBER(Type, min) && OSIQL_HAS_MEMBER(Type, max))
    {
        if (GetDirection() == Direction::Upstream)
        {
            const double base{distance + extract<Direction::Upstream>(this->GetLane())};
            return Interval{base - t.max, base - t.min};
        }
        const double base{distance + extract<Direction::Downstream>(this->GetLane())};
        return Interval{base + t.min, base + t.max};
    }
    else
    {
        if (GetDirection() == Direction::Upstream)
        {
            return distance + extract<Direction::Upstream>(this->GetLane()) - extract<Direction::Upstream>(t);
        }
        return distance + extract<Direction::Downstream>(t) - extract<Direction::Downstream>(this->GetLane());
    }
}

template <Traversal T, typename Scope>
template <typename Input>
double Node<T, Scope>::GetStartDistance(const Input &input) const
{
    if constexpr (Are<Overlaps<Road>>::in<Input>)
    {
        const auto &overlaps{get<Overlaps<Road>>(input)};
        const auto overlap{std::find_if(overlaps.begin(), overlaps.end(), Matches<Road>{*this})};
        return overlap != overlaps.end() ? GetStartDistance(*overlap) : std::numeric_limits<double>::quiet_NaN();
    }
    else if constexpr (Is<S>::in<Input>)
    {
        return GetDistance(get<S>(input));
    }
    else
    {
        if (GetDirection() == Direction::Upstream)
        {
            return distance + extract<Direction::Upstream>(this->GetLane()) - extract<Direction::Upstream>(input);
        }
        return distance + extract<Direction::Downstream>(input) - extract<Direction::Downstream>(this->GetLane());
    }
}

template <Traversal T, typename Scope>
template <typename Input>
double Node<T, Scope>::GetEndDistance(const Input &input) const
{
    if constexpr (Are<Overlaps<Road>>::in<Input>)
    {
        const auto &overlaps{get<Overlaps<Road>>(input)};
        const auto overlap{std::find_if(overlaps.begin(), overlaps.end(), Matches<Road>{*this})};
        return overlap != overlaps.end() ? GetEndDistance(*overlap) : std::numeric_limits<double>::quiet_NaN();
    }
    else if constexpr (Is<S>::in<Input>)
    {
        return GetDistance(get<S>(input));
    }
    else
    {
        if (GetDirection() == Direction::Upstream)
        {
            return distance + extract<Direction::Upstream>(this->GetLane()) - extract<Direction::Downstream>(input);
        }
        return distance + extract<Direction::Upstream>(input) - extract<Direction::Downstream>(this->GetLane());
    }
}

template <Traversal T, typename Scope>
double Node<T, Scope>::GetS(double targetDistance) const
{
    if (GetDirection() == Direction::Upstream)
    {
        return Increase<Direction::Upstream>(extract<Direction::Upstream>(this->GetLane()), targetDistance - distance);
    }
    return Increase<Direction::Downstream>(extract<Direction::Downstream>(this->GetLane()), targetDistance - distance);
}

template <Traversal T, typename Scope>
Interval<double> Node<T, Scope>::GetS(Interval<double> targetDistances) const
{
    if (GetDirection() == Direction::Upstream)
    {
        return {
            Increase<Direction::Upstream>(extract<Direction::Upstream>(this->GetLane()), targetDistances.min - distance),
            Increase<Direction::Upstream>(extract<Direction::Upstream>(this->GetLane()), targetDistances.max - distance),
        };
    }
    return {
        Increase<Direction::Downstream>(extract<Direction::Downstream>(this->GetLane()), targetDistances.min - distance),
        Increase<Direction::Downstream>(extract<Direction::Downstream>(this->GetLane()), targetDistances.max - distance),
    };
}

template <Traversal T, typename Scope>
double Node<T, Scope>::GetClampedS(double distance) const
{
    return std::clamp(GetS(distance), extract<Direction::Downstream>(this->GetLane()), extract<Direction::Upstream>(this->GetLane()));
}

template <Traversal T, typename Scope>
Interval<double> Node<T, Scope>::GetClampedS(Interval<double> distances) const
{
    return {
        std::clamp(GetS(distances.min), extract<Direction::Downstream>(this->GetLane()), extract<Direction::Upstream>(this->GetLane())),
        std::clamp(GetS(distances.max), extract<Direction::Downstream>(this->GetLane()), extract<Direction::Upstream>(this->GetLane())),
    };
}

template <Traversal T, typename Scope>
double Node<T, Scope>::GetLength() const
{
    return this->GetLane().GetLength();
}

template <Traversal T, typename Scope>
template <typename Type, typename Predicate>
decltype(auto) Node<T, Scope>::GetPlacements(Predicate &&pred) const
{
    if constexpr (std::is_same_v<Predicate, Return<true>>)
    {
        if constexpr (std::is_same_v<Scope, Road>)
        {
            if (this->GetLane().GetSideOfRoad() == Side::Left)
            {
                return this->GetRoad().template GetAll<Type, Side::Left>();
            }
            return this->GetRoad().template GetAll<Type, Side::Right>();
        }
        else // std::is_same_v<Scope, Lane>
        {
            return (this->GetLane().template GetAll<Type>());
        }
    }
    else
    {
        std::vector<const Placement<Type> *> result;
        const auto &placements{GetPlacements<Type>()};
        for (const auto &entry : placements)
        {
            if (pred(get<Placement<Type>>(entry)))
            {
                result.push_back(get<Placement<Type> *>(entry));
            }
        }
        return result;
    }
}

template <Traversal T, typename Scope>
template <typename Type, typename Predicate>
const Placement<Type> *Node<T, Scope>::GetPlacement(Predicate &&pred) const
{
    for (const auto &entry : GetPlacements<Type>())
    {
        if (pred(get<Placement<Type>>(entry)))
        {
            return &get<Placement<Type>>(entry);
        }
    }
    return nullptr;
}

template <Traversal T, typename Scope>
double Node<T, Scope>::GetStartDistance() const
{
    return distance;
}

template <Traversal T, typename Scope>
double Node<T, Scope>::GetEndDistance() const
{
    return distance + GetLength();
}

template <Traversal T, typename Scope>
template <typename Type,             //
          typename FutureNodeFinder, //
          typename NextNodePicker,   //
          typename NodeFilter,       //
          typename PlacementTester,  //
          typename OutputTransformer>
Transformations<OutputTransformer, Location<Type, Node<T, Scope>> &&> Node<T, Scope>::FindAll(
    bool includeThisNode,
    FutureNodeFinder &&finder,
    NextNodePicker &&picker,
    NodeFilter &&filter,
    PlacementTester &&tester,
    OutputTransformer &&transformer
) const
{
    Transformations<OutputTransformer, Location<Type> &&> result;
    InsertAll<Type>(
        std::back_inserter(result),
        includeThisNode,
        std::forward<FutureNodeFinder>(finder),
        std::forward<NextNodePicker>(picker),
        std::forward<NodeFilter>(filter),
        std::forward<PlacementTester>(tester),
        std::forward<OutputTransformer>(transformer)
    );
    return result;
}

template <Traversal T, typename Scope>
template <typename Type, typename Predicate, typename Transform>
Transformations<Transform, Location<Type, Node<T, Scope>> &&> Node<T, Scope>::FindAllWithin(Predicate &&pred, Transform &&transform) const
{
    Transformations<Transform, Location<Type> &&> result;
    InsertAllWithin<Type>(std::back_inserter(result), std::forward<Predicate>(pred), std::forward<Transform>(transform));
    return result;
}

template <Traversal T, typename Scope>
template <typename Type,              //
          typename FutureNodeFinder,  //
          typename NextNodePicker,    //
          typename NodeFilter,        //
          typename PlacementTester,   //
          typename OutputTransformer, //
          typename OutputIt>
OutputIt Node<T, Scope>::InsertAll(OutputIt output,                   //
                                   bool includeThisNode,              //
                                   FutureNodeFinder &&nodeFinder,     //
                                   NextNodePicker &&nodePicker,       //
                                   NodeFilter &&nodeFilter,           //
                                   PlacementTester &&placementTester, //
                                   OutputTransformer &&transform) const
{
    for (const auto *node{includeThisNode ? this : nodePicker(nodeFinder(*this))}; node != nullptr; node = nodePicker(nodeFinder(*node)))
    {
        if (nodeFilter(*node))
        {
            output = node->template InsertAllWithin<Type>(output, std::forward<PlacementTester>(placementTester), std::forward<OutputTransformer>(transform));
        }
    }
    return output;
}

template <Traversal T, typename Scope>
template <typename Type, typename Predicate, typename Transform, typename OutputIt>
OutputIt Node<T, Scope>::InsertAllWithin(OutputIt output, Predicate &&pred, Transform &&transform) const
{
    decltype(auto) placements{GetPlacements<Type>()};
    if constexpr (std::is_same_v<Predicate, Return<true>>)
    {
        return std::transform(placements.begin(), placements.end(), output, [&](const auto &placement) {
            return transform(Location<Type>{
                GetStartDistance(get<Placement<Type>>(placement)),
                &get<Placement<Type>>(placement),
                this //
            });
        });
    }
    else
    {
        for (const auto &placement : placements)
        {
            Location<Type> candidate{
                GetStartDistance(get<Placement<Type>>(placement)),
                &get<Placement<Type>>(placement),
                this //
            };
            if (pred(candidate))
            {
                *(output++) = transform(std::move(candidate));
            }
        }
        return output;
    }
}

template <Traversal T, typename Scope>
template <typename Type,
          typename FutureNodeFinder, //
          typename NextNodePicker,   //
          typename NodeTester,       //
          typename PlacementTester,  //
          typename Comparator,       //
          typename OutputTransformer>
Transformations<OutputTransformer, Location<Type, Node<T, Scope>> &&> Node<T, Scope>::Find(
    size_t n,                          //
    bool includeThisNode,              //
    FutureNodeFinder &&nodeFinder,     //
    NextNodePicker &&nodePicker,       //
    NodeTester &&nodeTester,           //
    PlacementTester &&placementTester, //
    Comparator &&comp,                 //
    OutputTransformer &&transform
) const
{
    Transformations<OutputTransformer, Location<Type> &&> result;
    result.reserve(n);
    Insert<Type>(std::back_inserter(result),                     //
                 n,                                              //
                 includeThisNode,                                //
                 std::forward<FutureNodeFinder>(nodeFinder),     //
                 std::forward<NextNodePicker>(nodePicker),       //
                 std::forward<NodeTester>(nodeTester),           //
                 std::forward<PlacementTester>(placementTester), //
                 std::forward<Comparator>(comp),                 //
                 std::forward<OutputTransformer>(transform));
    return result;
}

template <Traversal T, typename Scope>
template <typename Type, typename Predicate, typename Comparator, typename Transform>
Transformations<Transform, Location<Type, Node<T, Scope>> &&> Node<T, Scope>::FindWithin(size_t n, Predicate &&pred, Comparator &&comp, Transform &&transform) const
{
    Transformations<Transform, Location<Type> &&> output;
    output.reserve(n);
    InsertWithin<Type>(std::back_inserter(output), n, std::forward<Predicate>(pred), std::forward<Comparator>(comp), std::forward<Transform>(transform));
    return output;
}

template <Traversal T, typename Scope>
template <typename Type,              //
          typename FutureNodeFinder,  //
          typename NextNodePicker,    //
          typename NodeTester,        //
          typename PlacementTester,   //
          typename Comparator,        //
          typename OutputTransformer, //
          typename OutputIt>
OutputIt Node<T, Scope>::Insert(OutputIt output,                   //
                                size_t n,                          //
                                bool includeThisNode,              //
                                FutureNodeFinder &&nodeFinder,     //
                                NextNodePicker &&nodePicker,       //
                                NodeTester &&nodeTester,           //
                                PlacementTester &&placementTester, //
                                Comparator &&comp,                 //
                                OutputTransformer &&transform) const
{
    std::vector<Location<Type>> candidates;
    double threshold{std::numeric_limits<double>::max()};
    for (auto node{includeThisNode ? this : nodePicker(nodeFinder(*this))}; node != nullptr; node = nodePicker(nodeFinder(*node)))
    {
        if (node->GetStartDistance() >= threshold)
        {
            if (candidates.size() >= n)
            {
                const auto pastLast{std::next(candidates.begin(), n)};
                return std::transform(std::make_move_iterator(candidates.begin()), //
                                      std::make_move_iterator(pastLast),           //
                                      output,                                      //
                                      std::forward<OutputTransformer>(transform));
            }
            if (!candidates.empty())
            {
                output = std::transform(std::make_move_iterator(candidates.begin()), //
                                        std::make_move_iterator(candidates.end()),   //
                                        output,                                      //
                                        std::forward<OutputTransformer>(transform));
                n -= candidates.size();
                candidates.clear();
            }
        }
        else if (nodeTester(*node))
        {
            size_t numberOfPriorCandidates{candidates.size()};
            node->template InsertWithin<Type>(std::back_inserter(candidates), n, std::forward<PlacementTester>(placementTester), std::forward<Comparator>(comp));
            if (candidates.size() > numberOfPriorCandidates)
            {
                std::sort(std::next(candidates.begin(), numberOfPriorCandidates), candidates.end(), comp);
                std::inplace_merge(candidates.begin(), std::next(candidates.begin(), numberOfPriorCandidates), candidates.end());
                if (candidates.size() >= n)
                {
                    threshold = get<Min<Distance>>(candidates[n - 1]);
                }
            }
        }
    }
    const auto pastLast{std::next(candidates.begin(), std::min(candidates.size(), n))};
    for (auto item{candidates.begin()}; item != pastLast; ++item)
    {
        *(output++) = transform(get<Location<Type>>(*item));
    }
    return output;
}

template <Traversal T, typename Scope>
template <typename Type, typename Predicate, typename Comparator, typename Transform, typename OutputIt>
OutputIt Node<T, Scope>::InsertWithin(OutputIt output, size_t n, Predicate &&pred, Comparator &&comp, Transform &&transform) const
{
    decltype(auto) all{FindAllWithin<Type>(std::forward<Predicate>(pred))};
    auto end{std::next(all.begin(), std::min(all.size(), n))};
    std::partial_sort(all.begin(), end, all.end(), std::forward<Comparator>(comp));
    return std::transform(std::make_move_iterator(all.begin()), std::make_move_iterator(end), output, std::forward<Transform>(transform));
}

template <Traversal T, typename Scope>
template <typename Origin>
void Node<T, Scope>::InitializeDistance(const Origin &origin)
{
    if (GetDirection() == Direction::Upstream)
    {
        distance = extract<Direction::Upstream>(origin) - extract<Direction::Upstream>(GetLane());
    }
    else
    {
        distance = extract<Direction::Downstream>(GetLane()) - extract<Direction::Downstream>(origin);
    }
}

template <Traversal T, typename Scope>
std::shared_ptr<const Node<T, Scope>> Node<T, Scope>::Create(const Lane &lane, double distance, const Node<T, Scope> &parent)
{
    return std::make_shared<Node<T, Scope>>(Node<T, Scope>{lane, distance, parent});
}

template <Traversal T, typename Scope>
std::shared_ptr<const Node<T, Scope>> Node<T, Scope>::Create(const Lane &lane, double distance)
{
    return std::make_shared<Node<T, Scope>>(Node<T, Scope>{lane, distance});
}

template <Traversal T, typename Scope>
template <typename Origin>
std::shared_ptr<const Node<T, Scope>> Node<T, Scope>::Create(const Origin &origin, const Lane &lane)
{
    return std::make_shared<Node<T, Scope>>(Node<T, Scope>{origin, lane});
}

template <Traversal T, typename Scope>
template <typename Origin>
std::shared_ptr<const Node<T, Scope>> Node<T, Scope>::Create(const Origin &origin)
{
    return std::make_shared<Node<T, Scope>>(Node<T, Scope>{origin});
}

template <Traversal T, typename Scope>
std::shared_ptr<const Node<T, Scope>> Node<T, Scope>::GetShared() const
{
    return this->shared_from_this();
}

template <Traversal T, typename Scope>
std::shared_ptr<Node<T, Scope>> Node<T, Scope>::GetShared()
{
    return this->shared_from_this();
}

template <Traversal T, typename Scope>
Node<T, Scope>::operator std::shared_ptr<const Node<T, Scope>>() const
{
    return this->shared_from_this();
}

template <Traversal T, typename Scope>
Node<T, Scope>::operator std::shared_ptr<Node<T, Scope>>()
{
    return this->shared_from_this();
}

template <Traversal T, typename Scope>
bool Node<T, Scope>::HasAncestor(const Node<T, Scope> &ancestor) const
{
    if (ancestor.depth >= depth)
    {
        return false;
    }
    const auto *candidate{this};
    for (size_t distance{depth - ancestor.depth}; distance > 0; --distance)
    {
        candidate = candidate->parent;
    }
    return *parent == ancestor;
}

template <Traversal T, typename Scope>
const Node<T, Scope> *Node<T, Scope>::GetAncestor(size_t distance) const
{
    const Node<T, Scope> *node{this};
    while (node && distance)
    {
        node = node->parent;
        --distance;
    }
    return node;
}

template <Traversal T, typename Scope>
bool Node<T, Scope>::IsPeripheral() const
{
    return parent && parent->distance > this->distance;
}

template <Traversal T, typename Scope>
template <Traversal Toward>
decltype(auto) Node<T, Scope>::GetConnectedLanes() const
{
    if constexpr (std::is_same_v<Scope, Road>)
    {
        std::set<const Lane *, Less<Road>> pool;
        return this->GetRoad().template GetConnectedLanes<Toward>(this->GetSideOfRoad(), [&pool](const Lane *lane) {
            return pool.emplace(lane).second;
        });
    }
    else // if constexpr(std::is_same_v<Scope, Lane>)
    {
        return (this->GetLane().template GetConnectedLanes<Toward>());
    }
}

template <Traversal T, typename Scope>
template <auto Toward, typename Predicate>
decltype(auto) Node<T, Scope>::GetConnectedNodes(Predicate &&pred) const
{
    if constexpr (std::is_same_v<decltype(Toward), Traversal>)
    {
        if constexpr (std::is_same_v<Predicate, Return<true>>)
        {
            if (!front<Toward>(connectedNodes).has_value())
            {
                // NOTE: It may be necessary to filter lanes by unique roads
                // if a lane can split into two adjacent lanes
                decltype(auto) lanes{GetConnectedLanes<Toward>()};
                front<Toward>(connectedNodes) = std::vector<std::shared_ptr<const Node<T, Scope>>>{};
                front<Toward>(connectedNodes).value().reserve(lanes.size());
                std::transform(lanes.begin(), lanes.end(), std::back_inserter(front<Toward>(connectedNodes).value()), [&](const Lane *connectedLane) {
                    if constexpr (T == Toward)
                    {
                        return Node<T, Scope>::Create(*connectedLane, this->distance + this->GetLength(), *this);
                    }
                    else
                    {
                        return Node<T, Scope>::Create(*connectedLane, this->distance + connectedLane->GetLength(), *this);
                    }
                });
            }
            return (front<Toward>(connectedNodes).value());
        }
        else
        {
            std::vector<std::shared_ptr<const Node<T, Scope>>> result;
            const auto &nodes{GetConnectedNodes<Toward>()};
            result.reserve(nodes.size());
            for (const auto &node : nodes)
            {
                if (pred(get<Node<T, Scope>>(node)))
                {
                    result.push_back(node);
                }
            }
            return result;
        }
    }
    else
    {
        static_assert(std::is_same_v<decltype(Toward), Direction>);
        return GetConnectedNodes(this->GetDirection() * Toward, std::forward<Predicate>(pred));
    }
}

template <Traversal T, typename Scope>
template <typename Toward, typename Predicate>
decltype(auto) Node<T, Scope>::GetConnectedNodes(Toward toward, Predicate &&pred) const
{
    if constexpr (std::is_same_v<Toward, Direction>)
    {
        assert(toward != Direction::Bidirectional);
        return GetConnectedNodes(toward * this->GetDirection(), std::forward<Predicate>(pred));
    }
    else
    {
        static_assert(std::is_same_v<Toward, Traversal>);
        assert(toward != Traversal::Any);
        return toward == Traversal::Backward ? GetConnectedNodes<Traversal::Backward>(std::forward<Predicate>(pred)) : GetConnectedNodes<Traversal::Forward>(std::forward<Predicate>(pred));
    }
}

template <Traversal T, typename Scope>
template <auto Toward, typename Predicate>
const Node<T, Scope> *Node<T, Scope>::GetConnectedNode(Predicate &&pred) const
{
    for (const auto &node : GetConnectedNodes<Toward>())
    {
        if (pred(*node))
        {
            return node.get();
        }
    }
    return nullptr;
}

template <Traversal T, typename Scope>
template <typename Toward, typename Predicate>
const Node<T, Scope> *Node<T, Scope>::GetConnectedNode(Toward toward, Predicate &&pred) const
{
    if constexpr (std::is_same_v<Toward, Direction>)
    {
        return GetConnectedNode(toward * this->GetDirection(), std::forward<Predicate>(pred));
    }
    else
    {
        static_assert(std::is_same_v<Toward, Traversal>);
        assert(toward != Traversal::Any);
        return toward == Traversal::Backward ? GetConnectedNode<Traversal::Backward>(std::forward<Predicate>(pred)) : GetConnectedNode<Traversal::Forward>(std::forward<Predicate>(pred));
    }
}

template <Traversal T, typename Scope>
template <typename OutputIt, typename FutureNodeFinder, typename NextNodePicker, typename NodeFilter, typename NodeTransformer>
OutputIt Node<T, Scope>::InsertNodes(OutputIt output,           //
                                     bool includeThisNode,      //
                                     FutureNodeFinder &&finder, //
                                     NextNodePicker &&picker,   //
                                     NodeFilter &&filter,       //
                                     NodeTransformer &&transformer) const
{
    for (const auto *node{includeThisNode ? this : picker(finder(*this))}; node != nullptr; node = picker(finder(*node)))
    {
        if constexpr (std::is_same_v<NodeFilter, Return<true>>)
        {
            *(output++) = transformer(*node);
        }
        else
        {
            if (filter(*node))
            {
                *(output++) = transformer(*node);
            }
        }
    }
    return output;
}

template <Traversal T, typename Scope>
template <typename FutureNodeFinder, typename NextNodePicker, typename Predicate>
const Node<T, Scope> *Node<T, Scope>::FindNode(bool includeThisNode, FutureNodeFinder &&finder, NextNodePicker &&picker, Predicate &&pred) const
{
    for (const auto *node{includeThisNode ? this : picker(finder(*this))}; node != nullptr; node = picker(finder(*node)))
    {
        if constexpr (std::is_same_v<Predicate, Return<true>>)
        {
            return node;
        }
        else
        {
            if (pred(*node))
            {
                return node;
            }
        }
    }
    return nullptr;
}

namespace detail {
template <typename Type>
struct DistanceHeuristic
{
    explicit DistanceHeuristic(const Type &t) :
        t{t}
    {
    }

    template <Traversal T, typename Scope>
    double operator()(const Node<T, Scope> &node) const
    {
        if constexpr (OSIQL_HAS_MEMBER(Type, GetXY()))
        {
            return node.GetLength() * node.GetLength() + (t.GetXY() - node.GetRoad().GetReferenceLine().back(node.GetDirection())).SquaredLength();
        }
        else if constexpr (Is<Bounds>::in<Type>)
        {
            return node.GetLength() * node.GetLength() + GetDistanceBetween(get<Bounds>(t), node.GetRoad().GetReferenceLine().back(node.GetDirection())).SquaredLength();
        }
        else
        {
            static_assert(always_false<Type>, "Not supported");
        }
    }

    const Type &t; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)
};
} // namespace detail

template <Traversal T, typename Scope>
template <typename Destination>
const Node<T, Scope> *Node<T, Scope>::FindClosestNode(const Destination &goal, double maxDistance) const
{
    if constexpr (Are<Overlaps<Road>>::in<Destination>)
    {
        const auto &overlaps{get<Overlaps<Road>>(goal)};
        return std::transform_reduce(
                   overlaps.begin(),
                   overlaps.end(),
                   std::pair<double, const Node<T, Scope> *>{std::numeric_limits<double>::max(), nullptr},
                   [](const auto &a, const auto &b) { return std::min(a, b, [](const auto &a, const auto &b) { return a.first < b.first; }); },
                   [&](const Overlap<Road> &overlap) {
                       const Node<T, Scope> *node{FindClosestNode(overlap, maxDistance)};
                       return node ? std::pair{extract<Direction::Downstream>(node->GetDistance(overlap)), node}
                                   : std::pair<double, const Node<T, Scope> *>{std::numeric_limits<double>::max(), nullptr};
                   }
        ).second;
    }
    else
    {
        const detail::DistanceHeuristic heuristic{goal};
        using Data = typename std::pair<const Node<T, Scope> *, double>;
        const auto comp = [](double a, const Data &b) { return a > b.second; };
        std::vector<Data> queue{std::make_pair(this, this->distance + heuristic(*this))};
        std::set<Id> closedNodes;

        while (!queue.empty())
        {
            const Node<T, Scope> *current{queue.back().first};
            if (current->GetRoad() == get<Road>(goal))
            {
                return current;
            }
            queue.pop_back();
            for (const auto &next : current->GetConnectedNodes())
            {
                if (closedNodes.find(next->GetRoad().GetId()) != closedNodes.end())
                {
                    continue;
                }
                const auto otherPath{std::find_if(queue.begin(), queue.end(), Matches<Road>{*next})};
                if (otherPath != queue.end() && otherPath->first->distance > next->distance)
                {
                    const double newDistance{(otherPath->second - otherPath->first->distance) + next->distance};
                    auto destination{std::upper_bound(otherPath, queue.end(), newDistance, comp)};
                    std::rotate(otherPath, std::next(otherPath), destination)->second = newDistance;
                    continue;
                }
                const double nextDistance{next->distance + heuristic(*next)};
                const auto position{std::upper_bound(queue.begin(), queue.end(), nextDistance, comp)};
                queue.emplace(position, next.get(), nextDistance);
            }
            closedNodes.emplace(current->GetRoad().GetId());
        }
        return nullptr;
    }
}

template <Traversal T, typename Scope>
template <typename FutureNodeFinder, typename NextNodePicker, typename NodeFilter, typename NodeTransformer>
std::vector<std::invoke_result_t<NodeTransformer, const Node<T, Scope> &>> Node<T, Scope>::GetNodes(
    bool includeThisNode, FutureNodeFinder &&finder, NextNodePicker &&picker, NodeFilter &&filter, NodeTransformer &&transformer
) const
{
    std::vector<std::invoke_result_t<NodeTransformer, const Node<T, Scope> &>> result;
    InsertNodes(std::back_inserter(result), includeThisNode, std::forward<FutureNodeFinder>(finder), std::forward<NextNodePicker>(picker), std::forward<NodeFilter>(filter), std::forward<NodeTransformer>(transformer));
    return result;
}

template <Traversal T, typename Scope>
template <typename FutureNodeFinder>
std::vector<std::shared_ptr<const Node<T, Scope>>> Node<T, Scope>::GetPath(FutureNodeFinder &&selector) const
{
    std::vector<std::shared_ptr<const Node<T, Scope>>> result{this->GetShared()};
    const auto *node{this};
    while ((node = selector(*node)))
    {
        result.push_back(node->GetShared());
    }
    return result;
}

template <Traversal T, typename Scope>
template <typename RNG>
std::vector<std::shared_ptr<const Node<T, Scope>>> Node<T, Scope>::GetRandomPath(RNG &&rng, double maxDistance) const
{
    return GetPath([&](const Node<T, Scope> &node) -> const Node<T, Scope> * {
        if (node.distance + node.GetLength() >= maxDistance)
        {
            return nullptr;
        }
        const auto &candidates{node.GetConnectedNodes()};
        if (candidates.size() < 2)
        {
            return candidates.empty() ? nullptr : candidates[0].get();
        }
        const double value{rng()};
        assert(value >= 0.0 && value < 1.0);
        const size_t index{static_cast<size_t>(value * candidates.size())};
        return candidates[index].get();
    });
}

template <Traversal T, typename Scope>
std::vector<std::shared_ptr<const Node<T, Scope>>> Node<T, Scope>::GetPathFromDepth(size_t targetDepth) const
{
    if (depth >= targetDepth)
    {
        const size_t size{1 + depth - targetDepth};
        std::vector<std::shared_ptr<const Node<T, Scope>>> result(size, this->GetShared());
        for (auto node{std::next(result.rbegin())}; node != result.rend(); ++node)
        {
            *node = (*std::prev(node))->parent->GetShared();
        }
        return result;
    }
    return {};
}

//

template <Traversal T, typename Scope>
bool operator<(const Node<T, Scope> &a, const Node<T, Scope> &b)
{
    return std::make_pair(a.distance, a.GetRoad().GetId()) < std::make_pair(b.distance, b.GetRoad().GetId());
}
template <Traversal T, typename Scope>
bool operator==(const Node<T, Scope> &a, const Node<T, Scope> &b)
{
    return std::make_pair(a.distance, a.GetRoad().GetId()) == std::make_pair(b.distance, b.GetRoad().GetId());
}
template <Traversal T, typename Scope>
bool operator>(const Node<T, Scope> &a, const Node<T, Scope> &b)
{
    return std::make_pair(a.distance, a.GetRoad().GetId()) > std::make_pair(b.distance, b.GetRoad().GetId());
}

template <Traversal T, typename Scope, typename It>
std::vector<std::shared_ptr<const Node<T, Scope>>> CreateNodes(It first, It pastLast)
{
    assert(first != pastLast);
    std::vector<std::shared_ptr<const Node<T, Scope>>> nodes;
    nodes.reserve(static_cast<size_t>(std::distance(first, pastLast)));
    std::transform(first, pastLast, std::back_inserter(nodes), [](const auto &point) {
        return Node<T, Scope>::Create(point);
    });
    return nodes;
}

template <Traversal T, typename Scope>
std::ostream &operator<<(std::ostream &os, const Node<T, Scope> &node)
{
    return os << "Node<" << T << "> {Lane: " << node.GetLane().GetId()
              << " (Road: " << node.GetRoad().GetId()
              << "), Distance: " << node.distance
              << ", Length: " << node.GetLength() << '}';
}
} // namespace osiql
