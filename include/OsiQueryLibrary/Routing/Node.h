/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Node of a road network search graph

#include <iostream>
#include <memory>
#include <optional>
#include <vector>

#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Trait/Placement.h"
#include "OsiQueryLibrary/Types/Common.h"
#include "OsiQueryLibrary/Types/Distance.h"
#include "OsiQueryLibrary/Types/Enum.h"
#include "OsiQueryLibrary/Types/Interval.h"
#include "OsiQueryLibrary/Types/Location.h"
#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Has.h"
#include "OsiQueryLibrary/Utility/Is.tpp"
#include "OsiQueryLibrary/Utility/Pick.tpp"
#include "Utilities.h"

namespace osiql {
//! A Node is an extension of a Node that holds pointers to its connected nodes.
//! It can be used to traverse these nodes and find the shortest path to a destination.
//! To avoid wasteful usage of this class, it can only be constructed as a shared_ptr.
//!
//! \tparam T Forward or Backward
//! \tparam Scope of the node. Either Lane or Road. If the scope is Road, the successors
//! are all roads connected to any lane of this node's side of the road
template <Traversal T = Traversal::Forward, typename Scope = Road>
struct Node : Has<const Lane *>, std::enable_shared_from_this<Node<T, Scope>>
{
    static_assert(std::is_same_v<Scope, Lane> || std::is_same_v<Scope, Road>);

    static constexpr Traversal traversal = T;

    template <typename Object>
    using Location = osiql::Location<Object, Node<T, Scope>>;

    //! Creates a node from its components
    //!
    //! \param const Lane&
    //! \param distance
    //! \param parent
    //! \return Shared pointer to the created search node
    static std::shared_ptr<const Node<T, Scope>> Create(const Lane &, double distance, const Node<T, Scope> &parent);

    //! Creates a node
    //!
    //! \param const Lane&
    //! \param distance
    //! \param parent
    //! \return Shared pointer to the created search node
    static std::shared_ptr<const Node<T, Scope>> Create(const Lane &, double distance);

    //! Creates a root node from a given origin
    //!
    //! \tparam Origin
    //! \return Shared pointer to the created search node
    template <typename Origin>
    static std::shared_ptr<const Node<T, Scope>> Create(const Origin &);

    //! Creates a root node from a given lane and origin
    //!
    //! \tparam Origin
    //! \param Lane The lane associated with this search node
    //! \return Shared pointer to the created search node
    template <typename Origin>
    static std::shared_ptr<const Node<T, Scope>> Create(const Origin &, const Lane &);

    //! Returns the shared pointer that holds this object
    //!
    //! \return Shared pointer holding this object
    std::shared_ptr<const Node<T, Scope>> GetShared() const;

    //! Returns the shared pointer that holds this object
    //!
    //! \return Shared pointer holding this object
    std::shared_ptr<Node<T, Scope>> GetShared();

    //! \brief Implicit cast to a std::shared_ptr
    operator std::shared_ptr<const Node<T, Scope>>() const;

    //! \brief Implicit cast to a std::shared_ptr
    operator std::shared_ptr<Node<T, Scope>>();

    //! Returns the traversal direction of this node along its lane.
    //!
    //! \return Direction Forward or Backward
    Direction GetDirection() const;

    //! Returns the start distance of this node
    //!
    //! \return Minimum signed distance along the node network from the node origin in traversal direction
    double GetStartDistance() const;

    //! Returns the end distance of this node
    //!
    //! \return Maximum signed distance along the node network from the node origin in traversal direction
    double GetEndDistance() const;

    //! Returns the length of the lane or road this node represents
    //!
    //! \return double
    double GetLength() const;

    //! Returns the distance of the given object or s-coordinate
    //! on this node's road from the origin of the network.
    //!
    //! \tparam Type Object or s-coordinate
    //! \return double or Interval<double> if the object can span a length
    template <typename Type>
    decltype(auto) GetDistance(const Type &) const;

    //! Returns the earliest distance the given object
    //! is encountered on this node in the node's travel direction.
    //!
    //! \tparam Type Object or s-coordinate
    //! \return Distance from the root node's origin
    template <typename Type>
    double GetStartDistance(const Type &) const;

    //! Returns the last distance the given object
    //! is encountered on this node in the node's travel direction.
    //!
    //! \tparam Type Object or s-coordinate
    //! \return Distance from the root node's origin
    template <typename Type>
    double GetEndDistance(const Type &) const;

    //! Returns the s-coordinate at the given distance from the network origin
    //! relative to this node's lane.
    //!
    //! \param distance Distance from the origin of the road network
    //! \return s-coordinate a point at the given distance would have
    // on this node's lane.
    double GetS(double distance) const;

    //! Returns the s-coordinates at the given distances from the
    //! network origin relative to this node's lane.
    //!
    //! \param distances Pair of distances from the origin of the road network
    //! \return s-coordinates the points at the given distances would have
    // on this node's lane.
    Interval<double> GetS(Interval<double> distances) const;

    //! Returns the s-coordinate on this node's road closest to the given distance.
    //!
    //! \param distance Distance from the origin of the road network
    //! \return double
    double GetClampedS(double distance) const;

    //! Returns the s-coordinates on this node's road closest to the given distances.
    //!
    //! \param distances Distances from the origin of the road network
    //! \return double
    Interval<double> GetClampedS(Interval<double> distances) const;

    //! Return whether the given node is an ancestor of this node
    //!
    //! \return Whether the given node is an ancestor of this node
    bool HasAncestor(const Node<T, Scope> &) const;

    //! Returns the ancestor with the given difference in node depth from this node.
    //!
    //! \param distance A distance of 0 would return this node, 1 would return this node's parent node, etc.
    //! \return The n-th ancestor or nullptr if n is greater than this node's depth
    const Node<T, Scope> *GetAncestor(size_t distance) const;

    //! Returns whether this node is a peripheral node.
    //!
    //! \return Whether this node's parent has a greater distance than this node.
    bool IsPeripheral() const;

    //! Returns the connected nodes in the given direction that satisfy the given predicate.
    //! Does not include this node's parent.
    //!
    //! \tparam Toward    The direction (of type Direction or Traversal) from this node to the returned connected nodes.
    //! \tparam Predicate Invocable with the signature bool(const Node<T, Scope>&)
    //! \return Nodes connected to this node in the given direction.
    //! The returned container is a const reference if no predicate is passed.
    template <auto Toward = T, typename Predicate = Return<true>>
    decltype(auto) GetConnectedNodes(Predicate && = {}) const;

    //! Returns the connected nodes in the given direction that satisfy the given predicate.
    //! Excludes this node's parent if the orientation is backwards.
    //!
    //! \tparam Toward    Direction or Traversal
    //! \tparam Predicate Invocable with the signature bool(const Node<T, Scope>&)
    //! \param Toward     The direction at the end of which the connected nodes are returned.
    //! \return Nodes (other than this node's parent) connected to this node in the given direction.
    //! The returned container is a const reference if no predicate is passed.
    template <typename Toward, typename Predicate = Return<true>>
    decltype(auto) GetConnectedNodes(Toward, Predicate && = {}) const;

    //! Returns the first connected node at the end of this node in the given direction
    //! that satisfies the given predicate. Does not include this node's parent.
    //!
    //! \tparam Toward    The direction (of type Direction or Traversal) from this node to the returned connected nodes.
    //! \tparam Predicate Invocable with signature bool(const Node<T, Scope>&).
    //! \return nullptr if no such node exists
    template <auto Toward = T, typename Predicate>
    const Node<T, Scope> *GetConnectedNode(Predicate &&) const;

    //! Returns the first connected node at the end of this node in the given traversal direction
    //! relative to this node's traversal direction that satisfies the given predicate.
    //! Excludes this node's parent if the traversal direction is backwards.
    //!
    //! \tparam Toward    The direction (of type Direction or Traversal) from this node
    //!                   to the returned connected nodes.
    //! \tparam Predicate Invocable with signature bool(const Node<T, Scope>&).
    //! \return nullptr if no such node exists
    template <typename Toward, typename Predicate>
    const Node<T, Scope> *GetConnectedNode(Toward, Predicate && = {}) const;

    //! Returns all placements on this node of objects of the given type
    //!
    //! \tparam Type Type of the object placed on this node
    //! \return Container of object placements
    template <typename Type, typename Predicate = Return<true>>
    decltype(auto) GetPlacements(Predicate && = {}) const;

    //! Returns a placement of the given object type that satisfies the given predicate.
    //!
    //! \tparam Type      Type of the placed object
    //! \tparam Predicate Invocable with the signature bool(const Placement<Type>&).
    //! \return An object placement satisfying the predicate or nullptr if no such placement exists
    template <typename Type, typename Predicate>
    const Placement<Type> *GetPlacement(Predicate &&) const;

    //! Returns all transformed locations of the given object type that satisfy the given predicate
    //!
    //! \tparam Type              Type of the object whose transformed locations will be returned
    //! \tparam FutureNodeFinder  Invocable with the signature std::vector<std::shared_ptr<const Node<T, Scope>>>(const Node<T, Scope>&).
    //!                           Receives the last visited node and returns a number of nodes that should be visited in the future.
    //! \tparam NextNodePicker    Invocable with the signature const Node<T, Scope>&(std::vector<std::shared_ptr<const Node<T, Scope>>>&&).
    //!                           Receives the output of the future node finder and returns which node should be visited next.
    //! \tparam NodeTester        Invocable with the signature bool(const Node<T, Scope>&)
    //!                           Receives any node returned by the next node picker. If the tester returns true,
    //!                           the placements of that node will be passed to the placement tester.
    //! \tparam PlacementTester   Invocable with the signature bool(const Location<Type>&)
    //!                           Receives the placements of any node that satisfied the node tester. Any placement for which
    //!                           the placement tester returns true will be passed to the output transformer.
    //! \tparam OutputTransformer Invocable receiving a Location<Type>&& and returning the type to be inserted in the output
    //! \param includeThisNode    Whether to pass this node to the node tester
    //! \return Container of transformed object locations
    template <typename Type,                                                                 //
              typename FutureNodeFinder = ConnectedNodes<T>,                                 //
              typename NextNodePicker = NextNode<StableLess<Max<Distance>>, Node<T, Scope>>, //
              typename NodeTester = Return<true>,                                            //
              typename PlacementTester = Return<true>,                                       //
              typename OutputTransformer = osiql::Get<Location<Type>>>
    Transformations<OutputTransformer, Location<Type> &&> FindAll( //
        bool includeThisNode = true,                               //
        FutureNodeFinder && = {},                                  //
        NextNodePicker && = {},                                    //
        NodeTester && = {},                                        //
        PlacementTester && = {},                                   //
        OutputTransformer && = {}
    ) const;

    //! Returns the first/nearest n transformed locations that satisfy the given predicate.
    //!
    //! \tparam Type              The type of object to search for on this node
    //! \tparam FutureNodeFinder  Invocable with the signature std::vector<std::shared_ptr<const Node<T, Scope>>>(const Node<T, Scope>&).
    //!                           Receives the last visited node and returns a number of nodes that should be visited in the future.
    //! \tparam NextNodePicker    Invocable with the signature const Node<T, Scope>&(std::vector<std::shared_ptr<const Node<T, Scope>>>&&).
    //!                           Receives the output of the future node finder and returns which node should be visited next.
    //! \tparam NodeTester        Invocable with the signature bool(const Node<T, Scope>&)
    //!                           Receives any node returned by the next node picker. If the tester returns true,
    //!                           the placements of that node will be passed to the placement tester.
    //! \tparam PlacementTester   Invocable with signature bool(const Location<Type>&).
    //!                           The predicate is called on all objects associated with this node in unsorted order. If the computation is costly,
    //!                           it is more efficient to perform a std::find_if on FindAll()
    //! \tparam Comparator        Invocable with signature bool(const Location<Type>*, const Location<Type>*) or with references
    //!                           instead of pointers if the predicate is of type Return<true>. Used to sort the visited placements.
    //! \tparam OutputTransformer Invocable receiving a const Location<Type>& and returning a value that will be inserted into the returned container.
    //! \param n                  Maximum number of transformed locations to return
    //! \param includeThisNode    Whether to pass this node to the placement tester. If false, this node will be skipped
    //! \return Sorted collection of locations that satisfy the given predicate
    template <typename Type,
              typename FutureNodeFinder = ConnectedNodes<T>,                                 //
              typename NextNodePicker = NextNode<StableLess<Max<Distance>>, Node<T, Scope>>, //
              typename NodeTester = Return<true>,                                            //
              typename PlacementTester = Return<true>,                                       //
              typename Comparator = Less<Min<Distance>>,                                     //
              typename OutputTransformer = osiql::Get<Location<Type>>>
    Transformations<OutputTransformer, Location<Type> &&> Find(size_t n = 1,                //
                                                               bool includeThisNode = true, //
                                                               FutureNodeFinder && = {},    //
                                                               NextNodePicker && = {},      //
                                                               NodeTester && = {},          //
                                                               PlacementTester && = {},     //
                                                               Comparator && = {},          //
                                                               OutputTransformer && = {}) const;

    //! Returns all object locations of the given type on this node that satisfy the given predicates.
    //!
    //! \tparam Type       Object type such as MovingObject, StationaryObject, etc.
    //! \tparam Predicate  Any number of invocables with signature bool(const Location<Type>&)
    //! \tparam Transform  Invocable taking a Location<Type>&& and returning a value
    //!                    to be inserted to the returned container.
    //! \return Container of locations of the given object type on this node.
    template <typename Type, typename Predicate = Return<true>, typename Transform = osiql::Get<Location<Type>>>
    Transformations<Transform, Location<Type> &&> FindAllWithin(Predicate && = {}, Transform && = {}) const;

    //! Returns up to the first n transformed locations of the given type on this node that satisfy the given predicate sorted by the given comparator.
    //!
    //! \tparam Type       Object type such as MovingObject, StationaryObject, etc.
    //! \tparam Predicate  An invocable with signature bool(const Location<Type>&)
    //! \tparam Comparator Invocable taking two Locations and returning bool, determining the order in which the elements are returned.
    //! \tparam Transform  Invocable taking a Location<Type>&& and returning a value to be inserted to the returned container.
    //! \return            Container of transformed node locations of the given type
    template <typename Type, typename Predicate = Return<true>, typename Comparator = Less<Min<Distance>>, typename Transform = osiql::Get<Location<Type>>>
    Transformations<Transform, Location<Type> &&> FindWithin(size_t n, Predicate && = {}, Comparator && = {}, Transform && = {}) const;

    //! Searches the node network starting on this node and inserts all locations
    //! of the given type that satisfy the given predicates.
    //!
    //! \tparam Type              Type of the object whose transformed locations will be returned
    //! \tparam FutureNodeFinder  Invocable with the signature std::vector<std::shared_ptr<const Node<T, Scope>>>(const Node<T, Scope>&).
    //!                           Receives the last visited node and returns a number of nodes that should be visited in the future.
    //! \tparam NextNodePicker    Invocable with the signature const Node<T, Scope>&(std::vector<std::shared_ptr<const Node<T, Scope>>>&&).
    //!                           Receives the output of the future node finder and returns which node should be visited next.
    //! \tparam NodeTester        Invocable with the signature bool(const Node<T, Scope>&)
    //!                           Receives any node returned by the next node picker. If the tester returns true,
    //!                           the placements of that node will be passed to the placement tester.
    //! \tparam PlacementTester   Invocable with the signature bool(const Location<Type>&)
    //!                           Receives the placements of any node that satisfied the node tester. Any placement for which
    //!                           the placement tester returns true will be passed to the output transformer.
    //! \tparam OutputTransformer Invocable receiving a Location<Type>&&
    //!                           and returning the type to be inserted in the output
    //! \tparam OutputIt          Iterator pointing to the next place of insertion. The iterator is incremented after insertion.
    //!                           It is responsible for ensuring insertability at the address.
    //! \param includeThisNode    Whether to pass this node to the node tester
    //! \return Iterator pointing past the last inserter element
    template <typename Type,                                                                 //
              typename FutureNodeFinder = ConnectedNodes<T>,                                 //
              typename NextNodePicker = NextNode<StableLess<Max<Distance>>, Node<T, Scope>>, //
              typename NodeTester = Return<true>,                                            //
              typename PlacementTester = Return<true>,                                       //
              typename OutputTransformer = osiql::Get<Location<Type>>,                       //
              typename OutputIt = void>
    OutputIt InsertAll(OutputIt,                    //
                       bool includeThisNode = true, //
                       FutureNodeFinder && = {},    //
                       NextNodePicker && = {},      //
                       NodeTester && = {},          //
                       PlacementTester && = {},     //
                       OutputTransformer && = {}) const;

    //! Inserts the first/nearrest n transformed locations that satisfy the given predicate to the given output iterator.
    //!
    //! \tparam Type              The type of object whose transformed locations shall be inserted
    //! \tparam FutureNodeFinder  Invocable with the signature std::vector<std::shared_ptr<const Node<T, Scope>>>(const Node<T, Scope>&).
    //!                           Receives the last visited node and returns a number of nodes that should be visited in the future.
    //! \tparam NextNodePicker    Invocable with the signature const Node<T, Scope>&(std::vector<std::shared_ptr<const Node<T, Scope>>>&&).
    //!                           Receives the output of the future node finder and returns which node should be visited next.
    //! \tparam NodeTester        Invocable with the signature bool(const Node<T, Scope>&). Receives any node returned by the next node
    //!                           picker. If the tester returns true, the placements of that node will be passed to the placement tester.
    //! \tparam PlacementTester   Invocable with signature bool(const Location<Type>&).
    //!                           The predicate is called on all objects associated with this node in unsorted order. If the computation
    //!                           is costly, it is more efficient to perform a std::find_if on FindAll()
    //! \tparam Comparator        Invocable with signature bool(const Location<Type>*, const Location<Type>*)
    //!                           or with references instead of pointers if the predicate is of type Return<true>. Used to sort the visited placements.
    //! \tparam OutputTransformer Invocable receiving a Location<Type>&& and returning a value that will be inserted into the returned container.
    //! \tparam OutputIt          Iterator pointing to the next place of insertion. The iterator is incremented after insertion.
    //!                           It is responsible for ensuring insertability at the address,
    //!                           akin to a std::back_insert_iterator (std::back_inserter(std::vector))
    //! \param n                  Maximum number of transformed locations to insert
    //! \param includeThisNode    Whether to pass this node to the placement tester. If false, this node will be skipped
    //! \return Iterator pointing past the last place of insertion
    template <typename Type,
              typename FutureNodeFinder = ConnectedNodes<T>,                                 //
              typename NextNodePicker = NextNode<StableLess<Max<Distance>>, Node<T, Scope>>, //
              typename NodeTester = Return<true>,                                            //
              typename PlacementTester = Return<true>,                                       //
              typename Comparator = Less<Min<Distance>>,                                     //
              typename OutputTransformer = osiql::Get<Location<Type>>,                       //
              typename OutputIt = void>
    OutputIt Insert(
        OutputIt,                    //
        size_t n = 1,                //
        bool includeThisNode = true, //
        FutureNodeFinder && = {},    //
        NextNodePicker && = {},      //
        NodeTester && = {},          //
        PlacementTester && = {},     //
        Comparator && = {},          //
        OutputTransformer && = {}
    ) const;

    //! Inserts all locations on this node of given object type
    //! that satisfy the given predicate to the given output iterator
    //!
    //! \tparam Type      Object type such as MovingObject, StationaryObject, etc.
    //! \tparam Predicate Any number of invocables with signature bool(const Location<Type>&)
    //! \tparam Transform Invocable receiving a Location<Type>&& and returning a value
    //!                   that will be inserted to where the output iterator points
    //! \tparam OutputIt  Incrementable iterator to a container of transformed locations
    //! \return Iterator pointing past the last inserted element
    template <typename Type,                                   //
              typename Predicate = Return<true>,               //
              typename Transform = osiql::Get<Location<Type>>, //
              typename OutputIt = void>
    OutputIt InsertAllWithin(OutputIt, Predicate && = {}, Transform && = {}) const;

    //! Inserts the first n locations on this node of the given object type that satisfy the given predicate to the given output iterator
    //!
    //! \tparam Type       Object type such as MovingObject, StationaryObject, etc.
    //! \tparam Predicate  Invocable with signature bool(const Location<Type>&) determining whether to include a location in the output.
    //! \tparam Comparator Invocable with signature bool(const Location<Type>&, const Location<Type>&) that sorts locations such that the
    //!                    first n may be inserted.
    //! \tparam Transform  Invocable receiving a Location<Type>&& and returning a value that will be inserted to where the output iterator points.
    //! \tparam OutputIt   Incrementable iterator to a container of transformed locations.
    //! \param n           Maximum number of locations to return.
    //! \return Iterator pointing past the last inserted element
    template <typename Type,                                   //
              typename Predicate = Return<true>,               //
              typename Comparator = Less<Min<Distance>>,       //
              typename Transform = osiql::Get<Location<Type>>, //
              typename OutputIt = void>
    OutputIt InsertWithin(OutputIt output, size_t n = 1, Predicate && = {}, Comparator && = {}, Transform && = {}) const;

    //! Inserts all nodes returned by the NextNodePicker that satify the NodeTester to the given output iterator.
    //!
    //! \tparam FutureNodeFinder Invocable with the signature std::vector<const std::shared<Node<T, Scope>>>(const Node<T, Scope>&).
    //!                          The passed node is the last node selected by the node picker. The returned nodes are ones that
    //!                          should be visited at some point in the future. If this node is included, the first invocation of
    //!                          the FutureNodeFinder will receive this node.
    //! \tparam NextNodePicker   Invocable with the signature const Node<T, Scope>*(std::vector<const std::shared<Node<T, Scope>>>&&).
    //!                          The passed nodes are nodes that should be returned in the future. The returned node is the one to be
    //!                          visited next. See NextNode in Routing/Utilities for an example of an implementation.
    //! \tparam NodeTester       Invocable with the signature bool(const Node<T, Scope>&). If it returns, the given node will be inserted.
    //! \tparam NodeTransformer  Invocable taking a const Node<T, Scope>& and returning a type matching the value_type of the output iterator.
    //! \param includeThisNode   Whether this node should be tested for insertion instead of being skipped
    //! \return Collection of transformed nodes
    template <typename FutureNodeFinder = ConnectedNodes<T>,                                 //
              typename NextNodePicker = NextNode<StableLess<Max<Distance>>, Node<T, Scope>>, //
              typename NodeTester = Return<true>,                                            //
              typename NodeTransformer = osiql::Get<Node<T, Scope> *>>
    std::vector<std::invoke_result_t<NodeTransformer, const Node<T, Scope> &>> GetNodes(bool includeThisNode = true, //
                                                                                        FutureNodeFinder && = {},    //
                                                                                        NextNodePicker && = {},      //
                                                                                        NodeTester && = {},          //
                                                                                        NodeTransformer && = {}) const;

    //! Inserts all nodes returned by the NextNodePicker that satify the NodeTester to the given output iterator.
    //!
    //! \tparam OutputIt         Iterator pointing to the next address to which a node should be inserted
    //! \tparam FutureNodeFinder Invocable with the signature std::vector<const std::shared<Node<T, Scope>>>(const Node<T, Scope>&).
    //!                          The given node is the last node selected by the node picker. The returned nodes are new ones that
    //!                          should be visited at some point in the future. If this node is included, the first invocation of
    //!                          the FutureNodeFinder will receive this node.
    //! \tparam NextNodePicker   Invocable with the signature const Node<T, Scope>*(std::vector<const std::shared<Node<T, Scope>>>&&).
    //!                          The given nodes are nodes that should be returned in the future. The returned node is the one to be
    //!                          visited next. See NextNode in Routing/Utilities for an example of an implementation.
    //! \tparam NodeTester       Invocable with the signature bool(const Node<T, Scope>&). If it returns, the given node will be inserted.
    //! \tparam NodeTransformer  Invocable taking a const Node<T, Scope>& and returning a type matching the value_type of the output iterator.
    //! \param includeThisNode   Whether this node should be tested for insertion instead of being skipped
    //! \return Iterator pointing past the last inserted node
    template <typename OutputIt,                                                             //
              typename FutureNodeFinder = ConnectedNodes<T>,                                 //
              typename NextNodePicker = NextNode<StableLess<Max<Distance>>, Node<T, Scope>>, //
              typename NodeTester = Return<true>,                                            //
              typename NodeTransformer = osiql::Get<Node<T, Scope> *>>
    OutputIt InsertNodes(OutputIt,                    //
                         bool includeThisNode = true, //
                         FutureNodeFinder && = {},    //
                         NextNodePicker && = {},      //
                         NodeTester && = {},          //
                         NodeTransformer && = {}) const;

    //! Returns the first node that satisfies the given predicate among those offered by the FutureNodeFinder,
    //! visited in the order determined by the NextNodePicker.
    //!
    //! \tparam FutureNodeFinder Invocable with the signature std::vector<const std::shared<Node<T, Scope>>>(const Node<T, Scope>&).
    //!                          The passed node is the last node selected by the node picker. The returned nodes are ones that
    //!                          should be visited at some point in the future. If this node is included, the first invocation
    //!                          of the FutureNodeFinder will receive this node.
    //! \tparam NextNodePicker   Invocable with the signature const Node<T, Scope>*(std::vector<const std::shared<Node<T, Scope>>>&&).
    //!                          The passed nodes are new additions that should be returned in the future. The returned node is
    //!                          the one to be visited next. See NextNode in Routing/Utilities for an example of an implementation.
    //! \tparam Predicate        Invocable with the signature bool(const Node<T, Scope>&).
    //! \param includeThisNode   Whether to test this node instead of skipping it
    //! \return Node satisfying the predicate or a nullptr of none was found
    template <typename FutureNodeFinder = ConnectedNodes<T>,                                 //
              typename NextNodePicker = NextNode<StableLess<Max<Distance>>, Node<T, Scope>>, //
              typename Predicate = Return<true>>
    const Node<T, Scope> *FindNode(bool includeThisNode = false, //
                                   FutureNodeFinder && = {},     //
                                   NextNodePicker && = {},       //
                                   Predicate && = {}) const;

    //! Finds and returns the nearest (successor) node containing the given destination.
    //! \note The search is performed using the A* algorithm with Euclidean distance to the endpoint
    //! of the node's reference line as a cost heuristic. Use FindNode if you need a different heuristic.
    //!
    //! \tparam Destination Point or object.
    //! \param Destination  Destination lying on the returned node
    //! \param maxDistance  If the shortest path is longer than the maximum search distance, nullptr is returned.
    //! \return Nearest node containing the destination or nullptr if no such node exists.
    template <typename Destination>
    const Node<T, Scope> *FindClosestNode(const Destination &, double maxDistance = MAX_SEARCH_DISTANCE) const;

    //! Returns a custom path starting with this node based on the given successor selector.
    //!
    //! \tparam FutureNodeFinder Invocable with signature const Node<T, Scope>*(Node<T, Scope>&).
    //!                          If nullptr is returned, the path from this node is returned.
    //! \param FutureNodeFinder  A functor that receives the current node and returns the next node.
    //! \return Chain of nodes starting with this node
    template <typename FutureNodeFinder>
    std::vector<std::shared_ptr<const Node<T, Scope>>> GetPath(FutureNodeFinder &&) const;

    //! Returns a random path from this node through its successors, selecting a successor
    //! at each step using the given random number generator.
    //!
    //! \tparam RNG        Nullary invocable return a value between [0, 1).
    //!                    Only invoked when a node has more than one successor.
    //! \param maxDistance The maximum distance of any traversed node. Search is aborted if surpassed.
    //! \return Chain of nodes starting with this node up until the maximum distance is reached or no further nodes are found
    template <typename RNG>
    std::vector<std::shared_ptr<const Node<T, Scope>>> GetRandomPath(RNG &&, double maxDistance = MAX_SEARCH_DISTANCE) const;

    //! Returns a chain of nodes where each successor is a direct child of its predecessor.
    //!
    //! \param depth The depth of the first node within the returned range, ergo its number of parent nodes.
    //! \return Chain of nodes. Empty if this node's depth is smaller than the given depth.
    std::vector<std::shared_ptr<const Node<T, Scope>>> GetPathFromDepth(size_t depth) const;

    //! \brief distance from the node network's origin to the start (in traversal direction) of this node.
    //! If this node is a peripheral node or the root node, this might be negative.
    double distance{0.0};

    //! \brief This node's preceding node.
    //! If this node is peripheral, the parent is a successor
    const Node<T, Scope> *parent{nullptr};

    //! The number of parents this node and its ancestors have.
    size_t depth{0};

private:
    Node(const Lane &, double distance);
    Node(const Lane &, double distance, const Node<T, Scope> &parent);

    template <typename Origin>
    Node(const Origin &, const Lane &);

    template <typename Origin, OSIQL_REQUIRES(Is<Lane>::in<Origin>)>
    Node(const Origin &origin) :
        Node<T, Scope>{origin, get<Lane>(origin)}
    {
    }

    template <Traversal Toward>
    decltype(auto) GetConnectedLanes() const;

    template <typename Origin>
    void InitializeDistance(const Origin &);

    mutable std::array<std::optional<std::vector<std::shared_ptr<const Node<T, Scope>>>>, 2> connectedNodes;
};
OSIQL_HAS(Node<Traversal::Forward OSIQL_COMMA Road>, GetNode);
OSIQL_HAS(Node<Traversal::Backward OSIQL_COMMA Road>, GetNode);
OSIQL_HAS(Node<Traversal::Forward OSIQL_COMMA Lane>, GetNode);
OSIQL_HAS(Node<Traversal::Backward OSIQL_COMMA Lane>, GetNode);

template <Traversal T = Traversal::Forward>
using LaneNode = Node<T, Lane>;

template <Traversal T, typename Scope>
bool operator<(const Node<T, Scope> &, const Node<T, Scope> &);
template <Traversal T, typename Scope>
bool operator==(const Node<T, Scope> &, const Node<T, Scope> &);
template <Traversal T, typename Scope>
bool operator>(const Node<T, Scope> &, const Node<T, Scope> &);

template <Traversal T, typename Scope>
std::ostream &operator<<(std::ostream &, const Node<T, Scope> &);

//! Creates a list of separate node root from a range of points or lanes.
//!
//! \tparam Scope What kind of node to create. Either Lane or Road
//! \tparam It Iterator pointing to an element from which a node can be created, such as a lane or a local point
//! \tparam T Traversal direction of the created nodes. Either Forward or Backward.
//! \param first Iterator to the first element in the range
//! \param pastLast Iterator pointing past the last element in the range
//! \return Separate node for each element, even if two elements share the same lane
template <Traversal T = Traversal::Forward, typename Scope = Road, typename It = void>
std::vector<std::shared_ptr<const Node<T, Scope>>> CreateNodes(It first, It pastLast);
} // namespace osiql
