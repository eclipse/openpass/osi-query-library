/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Route.h"

#include "Node.tpp"
#include "OsiQueryLibrary/Component/Iterable.tpp"
#include "OsiQueryLibrary/Point/Point.tpp"
#include "OsiQueryLibrary/Trait/Localization.h"
#include "OsiQueryLibrary/Types/Enum.tpp" // Traversal & Selection
#include "OsiQueryLibrary/Types/Interval.tpp"

namespace osiql {
template <Traversal T, typename Scope>
template <typename Origin, typename Destination>
Route<T, Scope>::Route(Origin &&origin, Destination &&destination, std::vector<StoredNode<T, Scope>> &&path) :
    origin{std::forward<Origin>(origin)},
    destination{std::forward<Destination>(destination)},
    path{std::forward<std::vector<StoredNode<T, Scope>>>(path)}
{
}

template <Traversal T, typename Scope>
Route<T, Scope>::Route(const Point<const Lane> &point) :
    origin{point}, destination{point}, path{Node<T, Scope>::Create(point)}
{
}

template <Traversal T, typename Scope>
template <typename OriginLaneType, typename DestinationLaneType>
Route<T, Scope>::Route(const Point<OriginLaneType> &origin, const Point<DestinationLaneType> &destination) :
    origin{origin}, destination{destination}
{
    const auto root{Node<T, Scope>::Create(origin)};
    if (const auto *leaf{root->FindClosestNode(destination)}; leaf)
    {
        path = leaf->GetPathFromDepth(0);
    }
    else
    {
        path = {root};
    }
}

template <Traversal T, typename Scope>
ConstIterator<std::vector<StoredNode<T, Scope>>> Route<T, Scope>::begin() const
{
    return path.begin();
}

template <Traversal T, typename Scope>
Iterator<std::vector<StoredNode<T, Scope>>> Route<T, Scope>::begin()
{
    return path.begin();
}

template <Traversal T, typename Scope>
ConstIterator<std::vector<StoredNode<T, Scope>>> Route<T, Scope>::end() const
{
    return path.end();
}

template <Traversal T, typename Scope>
Iterator<std::vector<StoredNode<T, Scope>>> Route<T, Scope>::end()
{
    return path.end();
}

template <Traversal T, typename Scope>
double Route<T, Scope>::GetLength() const
{
    assert(!this->empty());
    return path.back()->GetDistance(destination);
}

template <typename RouteIterator, typename Type>
decltype(auto) GetDistance(RouteIterator first, RouteIterator pastLast, const Type &t)
{
    if constexpr (Are<Overlaps<Road>>::in<Type>)
    {
        const auto &overlaps{get<Overlaps<Road>>(t)};
        auto firstOverlap{overlaps.end()};
        const auto firstNode{std::find_if(first, pastLast, [&overlaps, &firstOverlap](const auto &node) {
            firstOverlap = std::find_if(overlaps.begin(), overlaps.end(), Matches<Road>{node});
            return firstOverlap != overlaps.end();
        })};
        if (firstNode == pastLast)
        {
            return Interval{std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};
        }
        auto lastOverlap{firstOverlap};
        const auto lastNode{std::prev(std::find_if(std::next(firstNode), pastLast, [&overlaps, &lastOverlap](const auto &node) {
            auto overlap{std::find_if(overlaps.begin(), overlaps.end(), Matches<Road>{node})};
            if (overlap != overlaps.end())
            {
                lastOverlap = overlap;
                return false;
            }
            return true;
        }))};
        return Interval{
            (*firstNode)->GetDistance(extract(*firstOverlap, (*firstNode)->GetDirection())),
            (*lastNode)->GetDistance(extract(*lastOverlap, !(*lastNode)->GetDirection())) //
        };
    }
    else if constexpr (std::is_base_of_v<Point<const Lane>, Type>)
    {
        const auto node{std::find_if(first, pastLast, Matches<Road>{t})};
        if (node == pastLast)
        {
            return std::numeric_limits<double>::quiet_NaN();
        }
        return (*node)->GetDistance(t);
    }
    else
    {
        static_assert(always_false<Type>, "Not supported");
    }
}

template <Traversal T, typename Scope>
template <typename Type>
decltype(auto) Route<T, Scope>::GetDistance(const Type &t) const
{
    return osiql::GetDistance(begin(), end(), t);
}

template <Traversal T, typename Scope>
template <typename From, typename To>
double Route<T, Scope>::GetDistanceBetween(const From &from, const To &to) const
{
    const auto origin{GetDistance(from)};
    const auto destination(GetDistance(to));
    if (std::isnan(extract<Direction::Downstream>(origin)) || std::isnan(extract<Direction::Downstream>(destination)))
    {
        return std::numeric_limits<double>::quiet_NaN();
    }
    if (extract<Direction::Upstream>(origin) < extract<Direction::Downstream>(destination))
    {
        return extract<Direction::Downstream>(destination) - extract<Direction::Upstream>(origin);
    }
    if (extract<Direction::Downstream>(origin) > extract<Direction::Upstream>(destination))
    {
        return extract<Direction::Upstream>(destination) - extract<Direction::Downstream>(origin);
    }
    return 0.0;
}

template <Traversal T, typename Scope>
ConstIterator<std::vector<StoredNode<T, Scope>>> Route<T, Scope>::FindClosestNode(double distance) const
{
    return std::prev(std::upper_bound(std::next(begin()), end(), distance, Less<Distance>{}));
}

template <typename RouteIterator>
RouteIterator FindClosestNode(RouteIterator first, RouteIterator pastLast, const XY &globalPoint)
{
    if (first == pastLast)
    {
        return first;
    }
    return TernaryIteratorSearch(first, std::prev(pastLast), [&globalPoint](RouteIterator it) {
        const auto &node{*it};
        const auto coords{node->GetRoad().GetReferenceLine().Localize(globalPoint)};
        if (coords.s > extract<Direction::Upstream>(node->GetLane()))
        {
            const double sDifference{coords.s - extract<Direction::Upstream>(node->GetLane())};
            return sDifference * sDifference + coords.t * coords.t;
        }
        if (coords.s < extract<Direction::Downstream>(node->GetLane()))
        {
            const double sDifference{extract<Direction::Upstream>(node->GetLane()) - coords.s};
            return sDifference * sDifference + coords.t * coords.t;
        }
        return coords.t * coords.t;
    });
}

template <Traversal T, typename Scope>
ConstIterator<std::vector<StoredNode<T, Scope>>> Route<T, Scope>::FindClosestNode(const XY &globalPoint) const
{
    return osiql::FindClosestNode(begin(), end(), globalPoint);
}

template <Traversal T, typename Scope>
Point<const Lane> Route<T, Scope>::Localize(double targetDistance) const
{
    const auto node{FindClosestNode(targetDistance)};
    // TODO: Introduce CrossSection class to avoid supplying a useless t-coordinate
    return {
        ST{(*node)->GetS(targetDistance), std::numeric_limits<double>::quiet_NaN()},
        (*node)->GetLane() //
    };
}

template <Traversal T, typename Scope>
template <typename GlobalPoint>
Localization<GlobalPoint> Route<T, Scope>::Localize(const GlobalPoint &globalPoint) const
{
    return osiql::Localize(begin(), end(), globalPoint);
}

template <Traversal T, typename Scope>
template <typename Type,       //
          Traversal Toward,    //
          Selection S,         //
          typename Predicate,  //
          typename Comparator, //
          typename Transform>
Transformations<Transform, Location<Type, Node<T, Scope>> &&> Route<T, Scope>::FindAll(
    Predicate &&pred, Comparator &&comp, Transform &&transform
) const
{
    static_assert(!std::is_same_v<Lane, Type> && !std::is_same_v<Road, Type>);

    Transformations<Transform, Location<Type, Node<T, Scope>> &&> result;
    InsertAll<Type, Toward, S>(std::back_inserter(result), std::forward<Predicate>(pred), std::forward<Comparator>(comp), std::forward<Transform>(transform));
    return result;
}

template <Traversal T, typename Scope>
template <typename Type, Traversal Toward, Selection S, typename Predicate, typename Comparator, typename Transform, typename OutputIt>
OutputIt Route<T, Scope>::InsertAll(OutputIt output, Predicate &&pred, Comparator &&comp, Transform &&transform) const
{
    static_assert(!std::is_same_v<Lane, Type> && !std::is_same_v<Road, Type>);

    if constexpr (S == Selection::First)
    {
        std::set<Id> pool;
        return InsertAll<Type, Toward, Selection::Each>(
            output, [&](const auto &relation) {
                return pred(relation) && pool.emplace(get<Id>(get<Type>(relation))).second;
            },
            std::forward<Comparator>(comp), std::forward<Transform>(transform)
        );
    }
    else if constexpr (S == Selection::Last)
    {
        static_assert(always_false<decltype(S)>, "Not supported");
    }
    else // if constexpr (S == Selection::Each)
    {
        std::vector<Location<Type, Node<T, Scope>>> result;
        for (auto node{osiql::begin<Toward>(path)}; node != osiql::end<Toward>(path); ++node)
        {
            auto i{static_cast<std::ptrdiff_t>(result.size())};
            (*node)->template InsertAllWithin<Type>(std::back_inserter(result), std::forward<Predicate>(pred));
            std::sort(std::next(result.begin(), i), result.end(), comp);
        }
        return std::transform(std::make_move_iterator(result.begin()), //
                              std::make_move_iterator(result.end()),   //
                              output,                                  //
                              std::forward<Transform>(transform));
    }
}

template <Traversal T, typename Scope>
template <typename Type,       //
          Traversal Toward,    //
          Selection S,         //
          typename Predicate,  //
          typename Comparator, //
          typename Transform>
Transformations<Transform, Location<Type, Node<T, Scope>> &&> Route<T, Scope>::FindAllBetween(
    Interval<double> range, Predicate &&pred, Comparator &&comp, Transform &&transform
) const
{
    Transformations<Transform, Location<Type, Node<T, Scope>> &&> result;
    InsertAllBetween<Type, Toward, S>(std::back_inserter(result),     //
                                      range,                          //
                                      std::forward<Predicate>(pred),  //
                                      std::forward<Comparator>(comp), //
                                      std::forward<Transform>(transform));
    if constexpr (S == Selection::Last)
    {
        static_assert(always_false<decltype(S)>, "Not supported");
    }
    return result;
}

namespace detail {
template <typename Comparator, typename Transformer, typename Container, typename InsertIterator>
InsertIterator SortAndMove(Container &input, InsertIterator output, Comparator &&comp, Transformer &&transform)
{
    std::sort(input.begin(), input.end(), std::forward<Comparator>(comp));
    output = std::transform(std::make_move_iterator(input.begin()), //
                            std::make_move_iterator(input.end()),   //
                            output,                                 //
                            std::forward<Transformer>(transform));
    input.clear();
    return output;
}
} // namespace detail

template <Traversal T, typename Scope>
template <typename Type,       //
          Traversal Toward,    //
          Selection S,         //
          typename Predicate,  //
          typename Comparator, //
          typename Transform,  //
          typename OutputIt>
OutputIt Route<T, Scope>::InsertAllBetween(
    OutputIt output, Interval<double> range, Predicate &&pred, Comparator &&comp, Transform &&transform
) const
{
    if constexpr (S == Selection::First)
    {
        std::set<Id> pool;
        return InsertAllBetween<Type, Toward, Selection::Each>(
            output, range, [&](const auto &relation) {
                return pred(relation) && pool.insert(get<Id>(get<Type>(relation))).second;
            },
            std::forward<Comparator>(comp), std::forward<Transform>(transform)
        );
    }
    else if constexpr (S == Selection::Last)
    {
        static_assert(always_false<decltype(S)>, "Not supported");
    }
    else
    {
        std::vector<Location<Type, Node<T, Scope>>> tmp;
        const auto nodes{GetNodesBetween<Toward>(range)};
        if (size(nodes) > 2)
        {
            (*nodes.min)->template InsertAllWithin<Type>(std::back_inserter(tmp), [&](const auto &relation) {
                return Greater<Max<Distance>>{}(relation, range.min) && pred(relation);
            });
            output = detail::SortAndMove(tmp, output, std::forward<Comparator>(comp), std::forward<Transform>(transform));

            for (auto node{std::next(nodes.min)}; node != std::prev(nodes.max); ++node)
            {
                (*node)->template InsertAllWithin<Type>(std::back_inserter(tmp), std::forward<Predicate>(pred));
                output = detail::SortAndMove(tmp, output, std::forward<Comparator>(comp), std::forward<Transform>(transform));
            }

            (*std::prev(nodes.max))->template InsertAllWithin<Type>(output, [&](const auto &relation) {
                return Less<Min<Distance>>{}(relation, range.max) && pred(relation);
            });
            output = detail::SortAndMove(tmp, output, std::forward<Comparator>(comp), std::forward<Transform>(transform));
        }
        else if (size(nodes) == 2)
        {
            (*nodes.min)->template InsertAllWithin<Type>(std::back_inserter(tmp), [&](const auto &relation) {
                return Greater<Max<Distance>>{}(relation, range.min) && pred(relation);
            });
            output = detail::SortAndMove(tmp, output, std::forward<Comparator>(comp), std::forward<Transform>(transform));

            (*std::prev(nodes.max))->template InsertAllWithin<Type>(std::back_inserter(tmp), [&](const auto &relation) {
                return Less<Min<Distance>>{}(relation, range.max) && pred(relation);
            });
            output = detail::SortAndMove(tmp, output, std::forward<Comparator>(comp), std::forward<Transform>(transform));
        }
        else if (size(nodes) == 1)
        {
            (*nodes.min)->template InsertAllWithin<Type>(std::back_inserter(tmp), [&](const auto &relation) {
                return Greater<Max<Distance>>{}(relation, range.min) && Less<Min<Distance>>{}(relation, range.max) && pred(relation);
            });
            output = detail::SortAndMove(tmp, output, std::forward<Comparator>(comp), std::forward<Transform>(transform));
        }
        return output;
    }
}

template <Traversal T, typename Scope>
template <typename Type,       //
          Traversal Toward,    //
          Selection S,         //
          typename Predicate,  //
          typename Comparator, //
          typename Transform>
Transformations<Transform, Location<Type, Node<T, Scope>> &&> Route<T, Scope>::Find(
    size_t n, Predicate &&pred, Comparator &&comp, Transform &&transform
) const
{
    Transformations<Transform, Location<Type, Node<T, Scope>> &&> result;
    Insert<Type, Toward, S>(std::back_inserter(result),     //
                            n,                              //
                            std::forward<Predicate>(pred),  //
                            std::forward<Comparator>(comp), //
                            std::forward<Transform>(transform));
    return result;
}

template <Traversal T, typename Scope>
template <typename Type,       //
          Traversal Toward,    //
          Selection S,         //
          typename Predicate,  //
          typename Comparator, //
          typename Transform,  //
          typename OutputIt>
OutputIt Route<T, Scope>::Insert(
    OutputIt output, size_t n, Predicate &&pred, Comparator &&comp, Transform &&transform
) const
{
    if constexpr (S == Selection::First)
    {
        std::set<Id> pool;
        return Insert<Type, Toward, Selection::Each>(
            output, n, [&](const auto &relation) {
                pred(relation) && pool.emplace(get<Id>(get<Type>(relation))).second;
            },
            std::forward<Comparator>(comp), std::forward<Transform>(transform)
        );
    }
    else if constexpr (S == Selection::Last)
    {
        static_assert(always_false<decltype(S)>, "Not supported");
    }
    else
    {
        for (auto node{Base::template begin<Toward>()}; node != Base::template end<Toward>(); ++node)
        {
            // NOTE: back_insert_iterator::difference_type is void until C++20...
            const auto before{&*output}; // const auto before{output};
            output = (*node)->template InsertWithin<Type>(output, n, std::forward<Predicate>(pred), std::forward<Comparator>(comp), std::forward<Transform>(transform));
            n -= static_cast<size_t>(&*output - before); // n -= std::distance(before, output);
            if (n == 0) { break; }
        }
        return output;
    }
}

template <Traversal T, typename Scope>
template <typename Type,       //
          Traversal Toward,    //
          Selection S,         //
          typename Predicate,  //
          typename Comparator, //
          typename Transform>
Transformations<Transform, Location<Type, Node<T, Scope>> &&> Route<T, Scope>::FindBetween(
    size_t n,               //
    Interval<double> range, //
    Predicate &&pred,       //
    Comparator &&comp,      //
    Transform &&transform
) const
{
    Transformations<Transform, Location<Type, Node<T, Scope>> &&> result;
    result.reserve(n);
    InsertBetween<Type, Toward, S>(std::back_inserter(result), n, range, std::forward<Predicate>(pred), std::forward<Comparator>(comp), std::forward<Transform>(transform));
    return result;
}

namespace detail {
template <typename Type,       //
          Traversal Toward,    //
          typename Predicate,  //
          typename Comparator, //
          typename Transform,  //
          typename OutputIt>
auto GetInserter(OutputIt &output,       //
                 size_t &n,              //
                 Interval<double> range, //
                 Predicate &&pred,       //
                 Comparator &&comp,      //
                 Transform &&transform)
{
    if constexpr (Toward == Traversal::Forward)
    {
#if __cplusplus >= 202002L
        return [&](auto &node, auto &&condition) {
            auto prior{output};
            output = node->template InsertWithin<Type, Toward>( //
                output,                                         //
                n,                                              //
                [&](const auto &item) { return condition(item) && pred(item); },
                comp, //
                transform
            );
            n -= std::distance(prior, output); // Requires C++20 (insert_iterators have difference_type void in C++17)
        };
#else
        return [&](auto &node, auto &&condition) {
            auto entries{node->template FindWithin<Type>(                        //
                n,                                                               //
                [&](const auto &item) { return condition(item) && pred(item); }, //
                comp
            )};
            n -= entries.size();
            output = std::transform(std::make_move_iterator(entries.begin()), std::make_move_iterator(entries.end()), output, transform);
        };
#endif
    }
    else
    {
        return GetInserter<Type, Traversal::Forward>(output, n, range, pred, Not{comp}, transform);
    }
}

template <typename Inserter, typename Nodes>
void InsertInRangeAcrossMultipleNodes(Inserter &&inserter, size_t &n, Interval<double> range, Nodes &&nodes)
{
    inserter(*nodes.begin(), Greater<Max<Distance>>::Than<>{range.min});
    if (n)
    {
        const auto lastNode{std::prev(nodes.end())};
        for (auto node{std::next(nodes.begin())}; node != lastNode; ++node)
        {
            inserter(*lastNode, Return<true>{});
            if (!n)
            {
                return;
            }
        }
        inserter(*lastNode, Less<Min<Distance>>::Than<>{range.max});
    }
}
} // namespace detail

template <Traversal T, typename Scope>
template <typename Type,       //
          Traversal Toward,    //
          Selection S,         //
          typename Predicate,  //
          typename Comparator, //
          typename Transform,  //
          typename OutputIt>
OutputIt Route<T, Scope>::InsertBetween(OutputIt output,        //
                                        size_t n,               //
                                        Interval<double> range, //
                                        Predicate &&pred,       //
                                        Comparator &&comp,      //
                                        Transform &&transform) const
{
    if constexpr (S == Selection::First)
    {
        std::set<Id> pool;
        return InsertBetween<Type, Toward, Selection::Each>(
            output, //
            n,      //
            range,  //
            [&](const auto &location) {
                return pred(location) && pool.emplace(get<Id>(get<Type>(location))).second;
            },
            std::forward<Comparator>(comp), //
            std::forward<Transform>(transform)
        );
    }
    else if constexpr (S == Selection::Last)
    {
        static_assert(always_false<decltype(S)>, "Not supported");
    }
    else
    {
        auto nodes{GetNodesBetween<Toward>(range)};
        auto inserter{detail::GetInserter<Type, Toward>(output, n, range, pred, comp, transform)};
        if (size(nodes) >= 2)
        {
            detail::InsertInRangeAcrossMultipleNodes(inserter, n, range, nodes);
        }
        else if (size(nodes) == 1)
        {
            inserter(*nodes.begin(), Between<Distance>{range.min, range.max});
        }
        return output;
    }
}

template <Traversal T, typename Scope>
template <typename LaneType>
std::optional<Point<const Lane>> Route<T, Scope>::ProjectPoint(const Point<LaneType> &origin, double distance) const
{
    // TODO: Performance - Restrict lane chain to specific direction based on sign of distance
    const auto chain{origin.GetLane().GetChain(*this)};
    if (chain.empty())
    {
        return std::nullopt;
    }
    const double offset{GetDistance(origin)};
    distance += offset;
    if (distance < offset)
    {
        if (distance < this->front()->distance)
        {
            return std::nullopt;
        }
    }
    else if (distance > this->back()->distance + this->back()->GetLength())
    {
        return std::nullopt;
    }
    Point<const Lane> target{Localize(distance)};
    const auto lane{std::find_if(chain.begin(), chain.end(), Matches<Road>{target})};
    if (lane == chain.end())
    {
        return std::nullopt;
    }
    const UV uv{target.GetU(), origin.GetV()};
    return Point<const Lane>{(*lane)->GetST(uv), target.GetLane()};
}

template <Traversal T, typename Scope>
template <Traversal Toward>
std::deque<std::shared_ptr<const Node<T, Lane>>> Route<T, Scope>::GetLaneNodes(const Lane &lane) const
{
    if (auto initialNode{this->TraverseUntil(Matches<Road>{lane})}; initialNode != end())
    {
        return GetLaneNodes<Toward>(lane, initialNode);
    }
    return {};
}

template <Traversal T, typename Scope>
template <Traversal Toward>
std::deque<std::shared_ptr<const Node<T, Lane>>> Route<T, Scope>::GetLaneNodes(const Lane &lane, ConstIterator<Route<T, Road>> initialNode) const
{
    // TODO: Use SFINAE or inheritance to remove Route<T, Lane>::GetLaneNodes
    static_assert(std::is_same_v<Scope, Road>);

    std::deque<std::shared_ptr<const Node<T, Lane>>> nodes;
    nodes.emplace_back(Node<T, Lane>::Create(lane, (*initialNode)->distance));
    if constexpr (Toward != Traversal::Forward)
    {
        for (auto prevNode{std::make_reverse_iterator(initialNode)}; prevNode != this->rend(); ++prevNode)
        {
            const auto *prevLaneNode{nodes.front()->GetConnectedNode(Matches<Road>{*prevNode})};
            if (!prevLaneNode)
            {
                break;
            }
            nodes.emplace_front(Node<T, Lane>::Create(prevLaneNode->GetLane(), (*prevNode)->distance));
        }
    }
    if constexpr (Toward != Traversal::Backward)
    {
        for (auto nextNode{std::next(initialNode)}; nextNode != this->end(); ++nextNode)
        {
            const auto *nextLaneNode{nodes.back()->GetConnectedNode(Matches<Road>{*nextNode})};
            if (!nextLaneNode)
            {
                break;
            }
            nodes.emplace_back(Node<T, Lane>::Create(nextLaneNode->GetLane(), (*nextNode)->distance));
        }
    }
}

template <Traversal T, typename Scope>
bool operator<(const Route<T, Scope> &a, const Route<T, Scope> &b)
{
    return a.GetLength() < b.GetLength();
}

template <typename RouteIterator, typename GlobalPoint>
Localization<GlobalPoint> Localize(RouteIterator first, RouteIterator pastLast, const GlobalPoint &globalPoint)
{
    const auto &node{*FindClosestNode(first, pastLast, globalPoint)};
    return node->GetRoad().Localize(globalPoint, get<Side>(*node));
}

template <Traversal T, typename Scope>
std::ostream &operator<<(std::ostream &os, const Route<T, Scope> &route)
{
    os << "Route (" << T << ") [" << route.origin << " - " << route.destination << "] {";
    if (!route.empty())
    {
        os << '\n';
        for (const auto &node : route)
        {
            os << "    " << *node << " Road " << node->GetLane().GetRoadId() << " (" << node->GetRoad().GetId() << ")\n";
        }
    }
    return os << '}';
}
} // namespace osiql
