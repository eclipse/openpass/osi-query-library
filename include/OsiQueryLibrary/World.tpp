/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "World.h"

#include <boost/iterator/function_output_iterator.hpp>

#include "OsiQueryLibrary/GroundTruth.tpp"
#include "OsiQueryLibrary/Street/Lane.tpp"
#include "OsiQueryLibrary/Street/Road.tpp"
#include "OsiQueryLibrary/Trait/BoostGeometry.h"
#include "OsiQueryLibrary/Types/Distance.h"
#include "OsiQueryLibrary/Types/Overlap.tpp"

namespace osiql {
template <typename ObjectType>
void World::UpdateObject(ObjectType &object)
{
    object.UpdateShapeAndBounds();
    object.template SetOverlaps<Road>(InternalFindOverlapping<Road>(object.GetShape(), object.GetBounds()));
    // TODO: Performance - Stop computing object centers
    object.positions.clear();
    for (auto &overlap : get<Overlaps<Road>>(object))
    {
        object.positions.push_back(overlap.GetRoad().Localize(Pose<XY>{object.GetXY(), object.GetYaw()}));
    }
    // TODO: Performance - Get lane overlaps from road overlaps
    object.template SetOverlaps<Lane>(InternalFindOverlapping<Lane>(object.GetShape(), object.GetBounds()));
}

template <typename GlobalPoint>
std::vector<Localization<GlobalPoint>> World::Localize(const GlobalPoint &point) const
{
    std::vector<Localization<GlobalPoint>> result;
    const auto inserter = [&](const Road *road) {
        auto localization{road->GetReferenceLine().Localize(point)};
        for (const Lane *closestLane : road->GetLanesContaining(localization))
        {
            result.emplace_back(localization, *closestLane);
        }
    };
    roadRTree.query(boost::geometry::index::intersects(point), boost::make_function_output_iterator(inserter));
    return result;
}

template <Traversal T, typename Scope, typename Origin, typename Destination>
std::optional<Route<T, Scope>> World::GetRouteBetweenGlobalPoints(const Origin &origin, const Destination &destination) const
{
    const auto origins{Localize(origin)};
    if (origins.empty())
    {
        return std::nullopt;
    }
    const auto destinations{Localize(destination)};

    std::vector<std::shared_ptr<const Node<T, Scope>>> roots;
    std::transform(origins.begin(), origins.end(), std::back_inserter(roots), [](const Point<const Lane> &point) {
        return Node<T, Scope>::Create(point);
    });
    struct Data
    {
        const Point<const Lane> *origin;
        const Point<const Lane> *destination;
        const Node<T, Scope> *goal;
        double distance;
    };
    std::vector<Data> paths;
    paths.reserve(origins.size() * destinations.size());
    for (const auto &destination : destinations)
    {
        auto root{roots.begin()};
        for (auto origin{origins.begin()}; root != roots.end(); ++root, ++origin)
        {
            if (const Node<T, Scope> *goal{(*root)->FindClosestNode(destination)}; goal != nullptr)
            {
                if (const double distance{goal->GetDistance(destination)}; distance >= -EPSILON)
                {
                    paths.emplace_back(Data{&*origin, &destination, goal, distance});
                }
            }
        }
    }
    if (paths.empty())
    {
        return std::nullopt;
    }
    const auto &path{*std::min_element(paths.begin(), paths.end(), Less<Distance>{})};
    return Route<T, Scope>{*path.origin, *path.destination, path.goal->GetPathFromDepth(0)};
}

template <Traversal T, typename Scope, typename Origin, typename Destination>
std::optional<Route<T, Scope>> World::GetRouteFromGlobalPointToLocalPoint(const Origin &origin, const Destination &destination) const
{
    const auto origins{Localize(origin)};
    if (origins.empty())
    {
        return std::nullopt;
    }
    std::vector<std::optional<Route<T, Scope>>> routes;
    routes.reserve(origins.size());
    std::transform(origins.begin(), origins.end(), std::back_inserter(routes), [&destination](const auto &origin) {
        return Route<T, Scope>{origin, destination};
    });
    const auto route{std::min_element(routes.begin(), routes.end(), [](const auto &a, const auto &b) {
        return !b.has_value() || (a.has_value() && a.value().GetLength() < b.value().GetLength());
    })};
    if (!route->has_value())
    {
        return std::nullopt;
    }
    return std::move(*route);
}

template <Traversal T, typename Scope, typename Origin, typename Destination>
std::optional<Route<T, Scope>> World::GetRouteFromLocalPointToGlobalPoint(const Origin &origin, const Destination &destination) const
{
    const auto destinations{Localize(destination)};
    if (destinations.empty())
    {
        std::cout << "GetRoute: Destination does not lie on any road. Returning the starting point\n";
        return {origin};
    }
    Node<T, Scope> root{origin};
    double shortestDistance{std::numeric_limits<double>::max()};
    const Node<T, Scope> *closestGoal{nullptr};
    const auto *preferredDestination{&destinations.front()};
    for (const auto &localDestination : destinations)
    {
        if (const auto *goal{root.FindClosestNode(localDestination)}; goal)
        {
            const double distance{goal->GetDistance(localDestination)};
            if (distance < shortestDistance)
            {
                shortestDistance = distance;
                closestGoal = goal;
                preferredDestination = &localDestination;
            }
        }
    }
    if (!closestGoal)
    {
        return std::nullopt;
    }
    return Route<T, Scope>{origin, *preferredDestination, std::move(root), closestGoal->GetPathFromDepth(0)};
}

template <Traversal T, typename Scope, typename Origin, typename Destination>
std::optional<Route<T, Scope>> World::GetRouteFromLocalPointToLocalPoint(const Origin &origin, const Destination &destination) const
{
    Route<T, Scope> result{origin, destination};
    if (result.destination != destination)
    {
        return std::nullopt;
    }
    return result;
}

template <Traversal T, typename Scope, typename PointIterator>
std::optional<Route<T, Scope>> World::GetRouteFromRangeOfGlobalPoints(PointIterator first, PointIterator pastLast) const
{
    if (first == pastLast)
    {
        return std::nullopt;
    }
    const auto origins{Localize(*first)};
    if (origins.empty())
    {
        return std::nullopt;
    }
    struct NodeData
    {
        std::shared_ptr<const Node<T, Scope>> root{nullptr};
        const Node<T, Scope> *leaf{nullptr};
        double distance{std::numeric_limits<double>::max()};
        const Point<const Lane> *origin{nullptr};
        const Point<const Lane> *destination{nullptr};
    };
    std::vector<NodeData> nodes;
    nodes.reserve(origins.size());
    std::transform(origins.begin(), origins.end(), std::back_inserter(nodes), [](const Point<const Lane> &point) {
        std::shared_ptr<const Node<T, Scope>> root{Node<T, Scope>::Create(point)};
        return NodeData{root, root.get(), 0.0, &point, &point};
    });

    std::vector<NodeData> nextNodes;
    for (auto checkpoint{std::next(first)}; checkpoint != pastLast; ++checkpoint)
    {
        const auto destinations{Localize(*checkpoint)};
        if (destinations.empty())
        {
            return std::nullopt;
        }
        nextNodes.reserve(destinations.size());
        std::transform(destinations.begin(), destinations.end(), std::back_inserter(nextNodes), [&](const Point<const Lane> &goal) {
            return std::transform_reduce(
                nodes.begin(), nodes.end(), NodeData{},
                [](const auto &a, const auto &b) { return std::min(a, b, Less<Distance>{}); },
                [&goal](const auto &data) {
                    if (const Node<T, Scope> *nextNode{data.leaf->FindClosestNode(goal)}; nextNode != nullptr)
                    {
                        const double distance{nextNode->GetDistance(goal)};
                        return data.distance <= distance ? NodeData{data.root, nextNode, distance, data.origin, data.destination} : NodeData{};
                    }
                    return NodeData{};
                }
            );
        });
        nextNodes.erase(
            std::remove_if(nextNodes.begin(), nextNodes.end(), [](const auto &node) { return node.root == nullptr; }),
            nextNodes.end()
        );
        if (nextNodes.empty())
        {
            return std::nullopt;
        }
        std::swap(nodes, nextNodes);
        nextNodes.clear();
    }
    const auto &path{*std::min_element(nodes.begin(), nodes.end(), Less<Distance>{})};
    return Route<T, Scope>{*path.origin, *path.destination, path.leaf->GetPathFromDepth(0)};
}

template <Traversal T, typename Scope, typename PointIterator>
std::optional<Route<T, Scope>> World::GetRouteFromRangeOfLocalPoints(PointIterator first, PointIterator pastLast) const
{
    if (first == pastLast)
    {
        return std::nullopt;
    }
    const auto root{Node<T, Scope>::Create(*first)};
    const Node<T, Scope> *pathEnd{root.get()};
    for (auto waypoint{std::next(first)}; waypoint != pastLast; ++waypoint)
    {
        const Node<T, Scope> *newEnd{pathEnd->FindClosestNode(*waypoint)};
        if (!newEnd)
        {
            std::cout << "GetRoute<" << T << ">: Skipping waypoint " << *waypoint << ", as no route to it exists\n";
            continue;
        }
        pathEnd = newEnd;
    }
    return Route<T, Scope>{*first, *std::prev(pastLast), *root, pathEnd->GetPathFromDepth(0)};
}

template <Traversal T, typename Scope, typename Origin, typename Destination>
std::optional<Route<T, Scope>> World::GetRoute(const Origin &origin, const Destination &destination) const
{
    static_assert(T != Traversal::Any);
    if constexpr (Are<X, Y>::in<Origin, Destination>)
    {
        return GetRouteBetweenGlobalPoints<T, Scope>(origin, destination);
    }
    else if constexpr (Are<X, Y>::in<Origin>)
    {
        return GetRouteFromGlobalPointToLocalPoint<T, Scope>(origin, destination);
    }
    else if constexpr (Are<X, Y>::in<Destination>)
    {
        return GetRouteFromLocalPointToGlobalPoint<T, Scope>(origin, destination);
    }
    else if constexpr (Are<S, Lane>::in<Origin, Destination>)
    {
        return GetRouteFromLocalPointToLocalPoint<T, Scope>(origin, destination);
    }
    else if constexpr (std::is_same_v<Origin, Destination> && Are<X, Y>::in<decltype(*std::declval<Origin>())>)
    {
        return GetRouteFromRangeOfGlobalPoints<T, Scope>(origin, destination);
    }
    else if constexpr (std::is_same_v<Origin, Destination> && Are<S, Lane>::in<decltype(*std::declval<Origin>)>())
    {
        return GetRouteFromRangeOfLocalPoints<T, Scope>(origin, destination);
    }
    else
    {
        static_assert(always_false<Origin, Destination>, "Not supported");
    }
}

template <Traversal T, typename Scope, typename Origin, typename RNG>
std::optional<Route<T, Scope>> World::GetRandomRoute(const Origin &origin, RNG &&rng, double maxDistance) const
{
    if constexpr (Are<X, Y>::in<Origin>)
    {
        const auto localizations{Localize(origin)};
        if (localizations.empty())
        {
            std::cout << "GetRandomRoute<" << T << ">: Origin " << origin << " does not lie on any road\n";
            return std::nullopt;
        }
        return localizations.front().template GetRandomRoute<T, Scope>(std::forward<RNG>(rng), maxDistance);
    }
    else if constexpr (OSIQL_HAS_MEMBER(Origin, template GetRandomRoute<T, Scope>(rng, maxDistance)))
    {
        return origin.template GetRandomRoute<T, Scope>(std::forward<RNG>(rng), maxDistance);
    }
    else
    {
        static_assert(always_false<Origin>, "Origin type not supported");
    }
}

template <Traversal T, typename Scope, typename Origin, typename Selector>
std::optional<Route<T, Scope>> World::GetCustomRoute(const Origin &origin, Selector &&selector) const
{
    if constexpr (Are<X, Y>::in<Origin>)
    {
        const auto localizations{Localize(origin)};
        if (localizations.empty())
        {
            std::cout << "GetCustomRoute<" << T << ">: Origin " << origin << " does not lie on any road\n";
            return std::nullopt;
        }
        return localizations.front().template GetCustomRoute<T, Scope>(std::forward<Selector>(selector));
    }
    else if constexpr (OSIQL_HAS_MEMBER(Origin, template GetCustomRoute<T, Scope>(std::forward<Selector>(selector))))
    {
        return origin.template GetCustomRoute<T, Scope>(std::forward<Selector>(selector));
    }
    else
    {
        static_assert(always_false<Origin>, "Origin type not supported");
    }
}

template <Traversal T, typename Scope>
std::optional<Route<T, Scope>> World::GetRoute(const osi3::TrafficAction::FollowPathAction &action) const
{
    static_assert(T != Traversal::Any);
    if (action.path_point().empty())
    {
        return std::nullopt;
    };
    if (std::any_of(action.path_point().begin(), action.path_point().end(), [](const osi3::StatePoint &point) {
            return !point.has_position();
        }))
    {
        std::cerr << "osiql::GetRoute: Not all points in the FollowPathAction have positions. Returning an empty route.\n";
        return std::nullopt;
    }
    return GetRoute<T, Scope>(action.path_point().begin(), action.path_point().end());
}

template <typename Type>
std::vector<Overlap<const Type>> World::FindOverlapping(const Shape &shape, const Bounds &bounds) const
{
    std::vector<Overlap<const Type>> result;
    const auto inserter = [&result, &shape](const Type *object) {
        Shape intersections{detail::GetIntersections(get<Shape>(object), shape)};
        if (intersections.size() > 2)
        {
            if constexpr (std::is_base_of_v<Lane, Type> || std::is_base_of_v<Road, Type>)
            {
                result.emplace_back(std::move(intersections), *object, get<Road>(object).GetReferenceLine().Localize(intersections.begin(), intersections.end()));
            }
            else
            {
                result.emplace_back(std::move(intersections), *object);
            }
        }
    };
    GetRTree<Type>().query(boost::geometry::index::intersects(bounds), boost::make_function_output_iterator(inserter));
    return result;
}

template <typename Type>
std::vector<Overlap<Type>> World::InternalFindOverlapping(const Shape &shape, const Bounds &bounds)
{
    std::vector<Overlap<Type>> result;
    const auto inserter = [&result, &shape](Type *object) {
        Shape intersections{detail::GetIntersections(get<Shape>(object), shape)};
        if (intersections.size() > 2)
        {
            if constexpr (std::is_base_of_v<Overlapable<Type>, Type>)
            {
                result.emplace_back(std::move(intersections), *object, get<Road>(object).GetReferenceLine().Localize(intersections.begin(), intersections.end()));
            }
            else
            {
                result.emplace_back(std::move(intersections), *object);
            }
        }
    };
    GetRTree<Type>().query(boost::geometry::index::intersects(bounds), boost::make_function_output_iterator(inserter));
    return result;
}

template <typename Type>
constexpr const RTree<Type> &World::GetRTree() const
{
    if constexpr (std::is_same_v<Type, Lane>)
    {
        return laneRTree;
    }
    else if constexpr (std::is_same_v<Type, Road>)
    {
        return roadRTree;
    }
    else if constexpr (std::is_same_v<Type, MovingObject>)
    {
        return movingObjectRTree;
    }
    else if constexpr (std::is_same_v<Type, StationaryObject>)
    {
        return stationaryObjectRTree;
    }
    else if constexpr (std::is_same_v<Type, TrafficLight>)
    {
        return trafficLightRTree;
    }
    else if constexpr (std::is_same_v<Type, TrafficSign>)
    {
        return trafficSignRTree;
    }
    else
    {
        static_assert(always_false<Type>, "World maintains no RTree of the given type");
    }
}

template <typename Type>
constexpr RTree<Type> &World::GetRTree()
{
    return const_cast<RTree<Type> &>(std::as_const(*this).GetRTree<Type>());
}

namespace detail {
template <typename A, typename B>
std::vector<XY> GetIntersections(const A &polygonA, const B &polygonB)
{
    std::vector<XY> intersections{};

    std::vector<XY> edgesA(polygonA.size());
    for (size_t i{1}; i < polygonA.size(); ++i)
    {
        edgesA[i - 1] = polygonA[i] - polygonA[i - 1];
    }
    edgesA.back() = polygonA.front() - polygonA.back();

    std::vector<XY> edgesB(polygonB.size());
    for (size_t i{1}; i < polygonB.size(); ++i)
    {
        edgesB[i - 1] = polygonB[i] - polygonB[i - 1];
    }
    edgesB.back() = polygonB.front() - polygonB.back();

    for (size_t i{0}; i < edgesB.size(); ++i)
    {
        for (size_t k{0}; k < edgesA.size(); ++k)
        {
            const double det{edgesB[i].Cross(edgesA[k])};
            const double lambda{(edgesA[k].Cross(polygonB[i]) + polygonA[k].Cross(edgesA[k])) / det};
            const double kappa{(edgesB[i].Cross(polygonB[i]) + polygonA[k].Cross(edgesB[i])) / det};
            if (lambda >= 0.0 && lambda <= 1.0 && kappa >= 0.0 && kappa <= 1.0)
            {
                const double x{polygonB[i].x + lambda * edgesB[i].x};
                const double y{polygonB[i].y + lambda * edgesB[i].y};
                if (intersections.empty() || intersections.back().x != x || intersections.back().y != y)
                {
                    intersections.emplace_back(x, y);
                }
            }
        }
    }

    std::copy_if(polygonA.begin(), polygonA.end(), std::back_inserter(intersections), [&](const XY &a) {
        return (std::find(intersections.begin(), intersections.end(), a) == intersections.end()) && a.IsWithin(polygonB);
    });
    std::copy_if(polygonB.begin(), polygonB.end(), std::back_inserter(intersections), [&](const XY &b) {
        return (std::find(intersections.begin(), intersections.end(), b) == intersections.end()) && b.IsWithin(polygonA);
    });
    return intersections;
}

template <typename A, typename B>
bool HasIntersections(const A &polygonA, const B &polygonB)
{
    std::vector<XY> edgesA(polygonA.size());
    for (size_t i{1}; i < polygonA.size(); ++i)
    {
        edgesA[i - 1] = polygonA[i] - polygonA[i - 1];
    }
    edgesA.back() = polygonA.front() - polygonA.back();

    std::vector<XY> edgesB(polygonB.size());
    for (size_t i{1}; i < polygonB.size(); ++i)
    {
        edgesB[i - 1] = polygonB[i] - polygonB[i - 1];
    }
    edgesB.back() = polygonB.front() - polygonB.back();

    for (size_t i{0}; i < edgesB.size(); ++i)
    {
        for (size_t k{0}; k < edgesA.size(); ++k)
        {
            const double det{edgesB[i].Cross(edgesA[k])};
            const double lambda{(edgesA[k].Cross(polygonB[i]) + polygonA[k].Cross(edgesA[k])) / det};
            const double kappa{(edgesB[i].Cross(polygonB[i]) + polygonA[k].Cross(edgesB[i])) / det};
            if (lambda >= 0.0 && lambda <= 1.0 && kappa >= 0.0 && kappa <= 1.0)
            {
                return true;
            }
        }
    }

    return std::any_of(polygonA.begin(), polygonA.end(), [&](const XY &a) { return a.IsWithin(polygonB); }) // clang-format off
        || std::any_of(polygonB.begin(), polygonB.end(), [&](const XY &b) { return b.IsWithin(polygonA); }); // clang-format on
}
} // namespace detail
} // namespace osiql
