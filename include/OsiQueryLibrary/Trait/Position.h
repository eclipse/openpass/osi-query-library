/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Type trait for how an object's position on a lane is stored as part of that object based on that object's type

#include <osi3/osi_object.pb.h>

namespace osiql {
template <typename Type>
struct Assignment;
struct ST;
struct Lane;
template <typename Type>
struct Locatable;
template <typename, typename>
struct Point;
template <typename>
struct Pose;
struct Road;

namespace trait {
//! The type in which an object stores its positions.
//! For static objects this is an Assignment<Lane>, which is a wrapper of an osi3::LogicalLaneAssignment.
//!
//! \tparam Type
template <typename Type>
struct Position
{
    //! \brief How an object stores its positions
    using type = Assignment<Lane>;
};

template <>
struct Position<Locatable<osi3::MovingObject>>
{
    using type = Pose<Point<Lane, ST>>;
};

template <>
struct Position<Locatable<osi3::StationaryObject>>
{
    using type = Pose<Point<Lane, ST>>;
};
} // namespace trait

//! Pose or Assignment<Lane>
//!
//! \tparam Type Wrapper of an OSI object
template <typename Type>
using Position = typename trait::Position<Type>::type;

template <typename Type>
struct Locatable;
} // namespace osiql
