/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Collection.h"

#include "OsiQueryLibrary/Utility/Common.h" // always_false<Type>

namespace osiql {
template <typename Type>
Collection<Type> From(const Container<typename Type::Handle> &container)
{
    if constexpr (std::is_same_v<Collection<Type>, Set<Item<Type>>>)
    {
        Set<Type> result;
        for (const typename Type::Handle &entity : container)
        {
            result.emplace(entity);
        }
        return result;
    }
    else if constexpr (std::is_same_v<Collection<Type>, std::vector<Item<Type>>>)
    {
        std::vector<Item<Type>> result;
        result.reserve(static_cast<size_t>(container.size()));
        for (const typename Type::Handle &entity : container)
        {
            result.emplace_back(entity);
        }
        std::sort(result.begin(), result.end());
        return result;
    }
    else
    {
        static_assert(always_false<Type>, "The specialized collection can not be constructed directly from an OSI object container");
    }
}

template <typename Type, typename... Args>
Type &Emplace(Collection<Type> &collection, Args &&...args)
{
    if constexpr (std::is_same_v<Collection<Type>, std::vector<Item<Type>>>)
    {
        return collection.emplace_back(std::forward<Args>(args)...);
    }
    else if constexpr (std::is_same_v<Item<Type>, std::pair<const Id, std::unique_ptr<Type>>> && sizeof...(Args) == 1)
    {
        return *collection.emplace(get<Id>(args...), std::make_unique<Type>(args...)).first->second;
    }
    else if constexpr (std::is_same_v<Item<Type>, std::pair<const Id, Type>> && sizeof...(Args) == 1)
    {
        return collection.emplace(get<Id>(args...), std::forward<Args>(args)...);
    }
    else if constexpr (std::is_same_v<Item<Type>, std::pair<const Id, Type>>)
    {
        return collection.emplace(std::forward<Args>(args)...).first->second;
    }
    else
    {
        return get<Type>(*collection.emplace(std::forward<Args>(args)...).first);
    }
}

template <typename Type>
Type *Find(Collection<Type> &collection, Id id)
{
    const auto match{osiql::Search<Type>(collection, id)};
    return (match == collection.end()) ? nullptr : &get<Type>(*match);
}

template <typename Type>
const Type *Find(const Collection<Type> &collection, Id id)
{
    const auto match{osiql::Search<Type>(collection, id)};
    return (match == collection.end()) ? nullptr : &get<Type>(*match);
}

template <typename Type>
typename Collection<Type>::const_iterator Search(const Collection<Type> &collection, Id id)
{
    if constexpr (std::is_same_v<Collection<Type>, std::vector<Item<Type>>>)
    {
        return std::find(collection.begin(), collection.end(), id);
    }
    else
    {
        return collection.find(id);
    }
}

template <typename Type>
typename Collection<Type>::iterator Search(Collection<Type> &collection, Id id)
{
    if constexpr (std::is_same_v<Collection<Type>, std::vector<Item<Type>>>)
    {
        return std::find(collection.begin(), collection.end(), id);
    }
    else
    {
        return collection.find(id);
    }
}
} // namespace osiql
