/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Type trait for the type of an object's id based on the type of that object

#include <cstdint>
#include <limits>

#include "OsiQueryLibrary/Utility/Get.tpp"
#include "OsiQueryLibrary/Utility/Is.tpp"

namespace osiql {
using Id = std::uint64_t;

constexpr Id UNDEFINED_ID{std::numeric_limits<Id>::max()}; // NOLINT(readability-identifier-naming)

OSIQL_GET(Id, GetId(), id(), id, other_lane_id());

constexpr std::string_view UNDEFINED_ROAD_ID{"UNDEFINED_ROAD_ID"}; // NOLINT(readability-identifier-naming)

template <typename A, typename B, OSIQL_REQUIRES(Is<Id>::in<A, B>)>
constexpr bool operator<(const A &a, const B &b)
{
    return get<Id>(a) < get<Id>(b);
}

template <typename A, typename B, OSIQL_REQUIRES(Is<Id>::in<A, B>)>
constexpr bool operator>(const A &a, const B &b)
{
    return get<Id>(a) > get<Id>(b);
}

template <typename A, typename B, OSIQL_REQUIRES(Is<Id>::in<A, B>)>
constexpr bool operator==(const A &a, const B &b)
{
    return get<Id>(a) == get<Id>(b);
}

template <typename A, typename B, OSIQL_REQUIRES(Is<Id>::in<A, B>)>
constexpr bool operator!=(const A &a, const B &b)
{
    return get<Id>(a) != get<Id>(b);
}
} // namespace osiql
