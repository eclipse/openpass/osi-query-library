/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Type trait to disambiguate osi3::BaseStationary and osi3::BaseMoving based on the template object type

#include <osi3/osi_common.pb.h>
#include <osi3/osi_object.pb.h>

namespace osiql {
namespace trait {
//! Generic base of an OSI object. The base holds an object's global position, orientation,
//! bounding box dimensions and shape (without transformation).
//!
//! \tparam Type
template <typename Type>
struct Base
{
    //! \brief Class holding an object's global position, orientation,
    //! bounding box dimensions and shape (without transformation)
    using type = osi3::BaseStationary;
};

//! A base that also holds an object's positional and rotational velocity and acceleration on
//! top of its global position, orientation, bounding box dimensions and shape (without transformation).
//!
//! \tparam osi3::MovingObject
template <>
struct Base<osi3::MovingObject>
{
    //! \brief Class holding an object's positional and rotational velocity and acceleration on
    //! top of its global position, orientation, bounding box dimensions and shape (without transformation).
    using type = osi3::BaseMoving;
};
} // namespace trait

template <typename Type>
using Base = typename trait::Base<Type>::type;
} // namespace osiql
