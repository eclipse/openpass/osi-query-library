/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief This file defines template specializations of boost::geometry::traits::access for
//! osiql::XY, osiql::Pose<osiql::XY>, osi3::TrafficAction_AcquireGlobalPositionAction
//! & osi3::StatePoint. This allows these types to be passed directly to boost rTree-queries, which
//! are used to find what roads in the world a given global point touches.

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/register/point.hpp>
#include <osi3/osi_common.pb.h>         // StatePoint
#include <osi3/osi_trafficcommand.pb.h> // AcquireGlobalPositionAction

#include "OsiQueryLibrary/Point/Pose.h"
#include "OsiQueryLibrary/Point/XY.h"

// Makes XY usable as a boost point type
BOOST_GEOMETRY_REGISTER_POINT_2D(osiql::XY, double, cs::cartesian, x, y)

BOOST_GEOMETRY_REGISTER_POINT_2D(osiql::Pose<osiql::XY>, double, cs::cartesian, x, y)

namespace boost::geometry::traits {
BOOST_GEOMETRY_DETAIL_SPECIALIZE_POINT_TRAITS(osi3::TrafficAction_AcquireGlobalPositionAction, 2, double, cs::cartesian)
template <>
struct access<osi3::TrafficAction_AcquireGlobalPositionAction, 0>
{
    static inline double get(const osi3::TrafficAction_AcquireGlobalPositionAction &point)
    {
        return point.position().x();
    }
    static inline void set(osi3::TrafficAction_AcquireGlobalPositionAction &point, const double &value)
    {
        point.mutable_position()->set_x(value);
    }
};
template <>
struct access<osi3::TrafficAction_AcquireGlobalPositionAction, 1>
{
    static inline double get(const osi3::TrafficAction_AcquireGlobalPositionAction &point)
    {
        return point.position().y();
    }
    static inline void set(osi3::TrafficAction_AcquireGlobalPositionAction &point, const double &value)
    {
        point.mutable_position()->set_y(value);
    }
};

BOOST_GEOMETRY_DETAIL_SPECIALIZE_POINT_TRAITS(osi3::StatePoint, 2, double, cs::cartesian)
template <>
struct access<osi3::StatePoint, 0>
{
    static inline double get(const osi3::StatePoint &point)
    {
        return point.position().x();
    }
    static inline void set(osi3::StatePoint &point, const double &value)
    {
        point.mutable_position()->set_x(value);
    }
};
template <>
struct access<osi3::StatePoint, 1>
{
    static inline double get(const osi3::StatePoint &point)
    {
        return point.position().y();
    }
    static inline void set(osi3::StatePoint &point, const double &value)
    {
        point.mutable_position()->set_y(value);
    }
};
} // namespace boost::geometry::traits
