/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Type trait for how an object's position on a lane is stored as part of that lane based on that object's type

#include "OsiQueryLibrary/Object/MovingObject.hpp"
#include "OsiQueryLibrary/Object/StationaryObject.hpp"
#include "OsiQueryLibrary/Point/Assignment.h"
#include "OsiQueryLibrary/Utility/Aggregate.h"
#include "OsiQueryLibrary/Utility/Has.h"

namespace osiql {
struct Lane;
struct LocalBounds;
template <typename>
struct Overlap;

namespace trait {
//! Type trait for lane-relative object placements
//!
//! \tparam Type Type of the object
template <typename Type>
struct Placement
{
    //! \brief lane-relative object placement
    using type = Assignment<std::remove_const_t<Type>>;
};

template <>
struct Placement<MovingObject>
{
    using type = Aggregate<Overlap<Lane> *, MovingObject *>;
};

template <>
struct Placement<const MovingObject>
{
    using type = Aggregate<Overlap<Lane> *, const MovingObject *>;
};

template <>
struct Placement<StationaryObject>
{
    using type = Aggregate<Overlap<Lane> *, StationaryObject *>;
};

template <>
struct Placement<const StationaryObject>
{
    using type = Aggregate<Overlap<Lane> *, const StationaryObject *>;
};
} // namespace trait

template <typename Type>
using Placement = typename trait::Placement<Type>::type;

template <typename Type>
struct Has<Placement<MovingObject>, Type> : detail::Has<Placement<MovingObject>, Type>
{
    using detail::Has<Placement<MovingObject>, Type>::Has;

    constexpr const MovingObject &GetMovingObject() const
    {
        return detail::Has<Placement<MovingObject>, Type>::template Get<MovingObject>();
    }

    template <typename _ = Type, OSIQL_REQUIRES(!is_const<_>)>
    constexpr MovingObject &GetMovingObject()
    {
        return detail::Has<Placement<MovingObject>, Type>::template Get<MovingObject>();
    }
};

template <typename Type>
struct Has<Placement<StationaryObject>, Type> : detail::Has<Placement<StationaryObject>, Type>
{
    using detail::Has<Placement<StationaryObject>, Type>::Has;

    constexpr const StationaryObject &GetStationaryObject() const
    {
        return detail::Has<Placement<StationaryObject>, Type>::template Get<StationaryObject>();
    }

    template <typename _ = Type, OSIQL_REQUIRES(!is_const<_>)>
    constexpr StationaryObject &GetStationaryObject()
    {
        return detail::Has<Placement<StationaryObject>, Type>::template Get<StationaryObject>();
    }
};

template <typename Type>
std::ostream &operator<<(std::ostream &os, const Placement<Type> &placement)
{
    return os << '{' << get<Type>(placement) << " on Lane " << placement.GetLane().GetId() << '}';
}
} // namespace osiql
