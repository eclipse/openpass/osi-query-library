/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Type trait to specify different containers for different OSI objects

#include <map>
#include <vector>

#include "Id.h"
#include "OsiQueryLibrary/Component/Iterable.h"
#include "OsiQueryLibrary/Object/MovingObject.hpp"
#include "OsiQueryLibrary/Types/Container.h"

namespace osiql {
struct Lane;
struct LightBulb;
struct LocalBounds;
struct Road;
struct TrafficLight;

namespace trait {
//! Type trait determining how objects are stored in osiql::GroundTruth
//!
//! \tparam Type
template <typename Type>
struct Collection
{
    //! \brief How objects are stored in osiql::GroundTruth
    using type = std::vector<Type>;
};

//! \brief Type trait specifying how lanes are stored in osiql::GroundTruth
template <>
struct Collection<Lane>
{
    //! \brief How lanes are stored in osiql::GroundTruth
    using type = HashMap<Lane>;
};

//! \brief Type trait specifying how moving objects are stored in osiql::GroundTruth
template <>
struct Collection<MovingObject>
{
    //! \brief How moving objects are stored in osiql::GroundTruth
    using type = HashMap<std::unique_ptr<MovingObject>>;
};

//! \brief Type trait specifying how light bulbs are stored in osiql::GroundTruth
template <>
struct Collection<LightBulb>
{
    //! \brief How light bulbs are stored in osiql::GroundTruth
    using type = std::map<Id, TrafficLight *>;
};

//! \brief Type trait specifying how roads are stored in osiql::GroundTruth
template <>
struct Collection<Road>
{
    //! \brief How roads are stored in osiql::GroundTruth
    using type = HashMap<Road>;
};
} // namespace trait

//! How objects are stored in osiql::GroundTruth
//!
//! \tparam Type
template <typename Type>
using Collection = typename trait::Collection<Type>::type;

//! The value type of a collection
//!
//! \tparam Type
template <typename Type>
using Item = typename Collection<Type>::value_type;

//! Creates and returns a collection of wrapper objects from the given container of OSI objects
//!
//! \tparam Type
//! \param container
//! \return Collection<Type>
template <typename Type>
Collection<Type> Collect(const Container<typename Type::Handle> &container);

//! Returns a pointer to the item with the given id in the collection or nullptr if there is none.
//!
//! \tparam Type
//! \return Type*
template <typename Type>
Type *Find(Collection<Type> &, Id);

//! Returns a pointer to the item with the given id in the collection or nullptr if there is none.
//!
//! \tparam Type
//! \return const Type*
template <typename Type>
const Type *Find(const Collection<Type> &, Id);

//! Emplaces an object into the given collection
//!
//! \tparam Type
//! \tparam Args
//! \param collection
//! \param args
template <typename Type, typename... Args>
Type &Emplace(Collection<Type> &collection, Args &&...args);

//! Searches for an object within the collection and returns it if found, otherwise returns nullptr.
//!
//! \tparam Type
//! \return const Type*
template <typename Type>
typename Collection<Type>::const_iterator Search(const Collection<Type> &, Id);

//! Searches for an object within the collection and returns it if found, otherwise returns nullptr.
//!
//! \tparam Type
//! \return Collection<Type>::iterator
template <typename Type>
typename Collection<Type>::iterator Search(Collection<Type> &, Id);
} // namespace osiql
