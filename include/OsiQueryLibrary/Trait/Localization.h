/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <cstddef>
#include <vector>

namespace osiql {
struct ST;
template <typename, typename>
struct Point;
template <typename>
struct Pose;
template <size_t>
struct Vector;
struct Lane;

namespace trait {
template <typename>
struct Localization
{
    using type = Point<Lane, ST>;
    using const_type = Point<const Lane, ST>;
};

template <typename Type>
struct Localization<Pose<Type>>
{
    using type = Pose<typename Localization<Type>::type>;
    using const_type = Pose<typename Localization<Type>::const_type>;
};

template <typename Type>
struct Localization<std::vector<Type>>
{
    using type = std::vector<typename Localization<Type>::type>;
    using const_type = std::vector<typename Localization<Type>::const_type>;
};
} // namespace trait

//! A mutable localization of the given template type
//!
//! \tparam Type Localizable type (such as a global point, global pose, or vector thereof)
template <typename Type>
using Localized = typename trait::Localization<Type>::type;

//! An immutable localization of the given template type
//!
//! \tparam Type Localizable type (such as a global point, global pose, or vector thereof)
template <typename Type>
using Localization = typename trait::Localization<Type>::const_type;
} // namespace osiql
