/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <utility> // std::declval

namespace osiql {
namespace trait {
template <typename A, typename B, typename F>
constexpr auto is_comparable(F &&func) -> decltype(func(std::declval<A>(), std::declval<B>()), true) // NOLINT(readability-identifier-naming)
{
    return true;
};

template <typename>
constexpr bool is_comparable(...) // NOLINT(readability-identifier-naming)
{
    return false;
}
} // namespace trait
#define OSIQL_IS_COMPARABLE(OPERATOR, A, B) trait::is_comparable<A, B>([](auto &&a, auto &&b) -> decltype(a OPERATOR b) {})

// Lambdas can not be used directly for SFINAE in template declarations, so define precomputed aliases instead:

template <typename A, typename B>
constexpr bool is_equal_comparable = OSIQL_IS_COMPARABLE(==, A, B); // NOLINT(readability-identifier-naming)

template <typename A, typename B>
constexpr bool is_not_equal_comparable = OSIQL_IS_COMPARABLE(!=, A, B); // NOLINT(readability-identifier-naming)

template <typename A, typename B>
constexpr bool is_less_comparable = OSIQL_IS_COMPARABLE(<, A, B); // NOLINT(readability-identifier-naming)

template <typename A, typename B>
constexpr bool is_less_or_equal_comparable = OSIQL_IS_COMPARABLE(<=, A, B); // NOLINT(readability-identifier-naming)

template <typename A, typename B>
constexpr bool is_greater_comparable = OSIQL_IS_COMPARABLE(>, A, B); // NOLINT(readability-identifier-naming)

template <typename A, typename B>
constexpr bool is_greater_or_equal_comparable = OSIQL_IS_COMPARABLE(>=, A, B); // NOLINT(readability-identifier-naming)
} // namespace osiql
