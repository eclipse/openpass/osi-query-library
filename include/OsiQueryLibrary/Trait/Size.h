/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Type trait that can be specialized for an enum type to specify how many valid values it contains.

#include <cstddef>

namespace osiql {
namespace trait {
template <typename Enum>
struct Size
{
    static constexpr size_t value = 0;
};
} // namespace trait
template <typename Enum>
constexpr size_t Size = trait::Size<Enum>::value;
} // namespace osiql

#define OSIQL_SET_SIZE(ENUM, VALUE)            \
    namespace osiql::trait {                   \
    template <>                                \
    struct Size<ENUM>                          \
    {                                          \
        static constexpr size_t value = VALUE; \
    };                                         \
    } // namespace osiql::trait
