/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Type trait for how an OSI object handle is stored depending on the OSI object type

#include <osi3/osi_object.pb.h>

namespace osiql {
namespace trait {
//! Most wrapper objects store their handles as const references
//!
//! \tparam UnderlyingType
template <typename UnderlyingType>
struct Handle
{
    //! \brief The full non-decayed type of the wrapper object's handle
    using type = std::reference_wrapper<const UnderlyingType>;
};

template <>
struct Handle<osi3::MovingObject>
{
    //! \brief The full non-decayed type of the wrapper object's handle
    using type = const osi3::MovingObject *;
};
} // namespace trait

//! OSI object handle. Usually a reference_wrapper
//!
//! \tparam Type
template <typename Type>
using Handle = typename trait::Handle<Type>::type;
} // namespace osiql
