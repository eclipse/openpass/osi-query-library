/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Wrapper of a osi3::SensorView

#include <osi3/osi_sensorview.pb.h>

#include "Object/VehicleData.h"
#include "World.h"

namespace osiql {
template <typename VehicleDataType = HostVehicleData>
struct SensorView : Wrapper<osi3::SensorView>
{
    //! Constructs a sensor view from a OSI sensor view handle and optional additional vehicle data arguments
    //!
    //! \tparam ...AdditionalArguments The vehicle data of this sensor view is constructed using
    //! a osi3::HostVehicleData, osiql::Vehicle, and these additional vehicle data arguments.
    //! \param osi3::SensorView Underlying osi handle of this sensor view
    //! \param AdditionalArguments... Can remain empty unless the vehicle data type requires additional arguments
    template <typename... AdditionalArguments>
    SensorView(const osi3::SensorView &, AdditionalArguments &&...);

    bool HasVehicleData() const;

    const VehicleDataType &GetVehicleData() const;

    VehicleDataType &GetVehicleData();

    const Vehicle &GetVehicle() const;

    bool HasRoute() const;

    const Route<> &GetRoute() const;

    std::shared_ptr<World> world;

private:
    std::unique_ptr<VehicleDataType> vehicleData;
};
} // namespace osiql

namespace osiql {
template <typename VehicleDataType>
template <typename... Args>
SensorView<VehicleDataType>::SensorView(const osi3::SensorView &handle, Args &&...args) :
    Wrapper<osi3::SensorView>{handle},
    world{GetSharedWorld(handle.global_ground_truth())}
{
    if (!world)
    {
        world = std::make_shared<World>(handle.global_ground_truth());
        std::cout << "Warning: The osi3::SensorView used to construct this osiql::SensorView does not " //
                     "contain an existing World in the source reference of the environmental "          //
                     "conditions of its ground truth. An unintentional copy may have been created.\n";
    }
    if (GetHandle().has_host_vehicle_data())
    {
        const osi3::HostVehicleData &data{GetHandle().host_vehicle_data()};
        const Vehicle &vehicle{world->HasHostVehicle() && (!data.has_host_vehicle_id() || world->GetHostVehicle().GetId() != data.host_vehicle_id().value()) //
                                   ? world->GetHostVehicle()
                                   : *world->GetVehicle(data.host_vehicle_id().value())};
        vehicleData = std::make_unique<VehicleDataType>(data, vehicle, std::forward<Args>(args)...);
    }
}

template <typename VehicleDataType>
bool SensorView<VehicleDataType>::HasVehicleData() const
{
    return vehicleData.get();
}

template <typename VehicleDataType>
const VehicleDataType &SensorView<VehicleDataType>::GetVehicleData() const
{
    assert(HasVehicleData());
    return *vehicleData;
}

template <typename VehicleDataType>
VehicleDataType &SensorView<VehicleDataType>::GetVehicleData()
{
    assert(HasVehicleData());
    return *vehicleData;
}

template <typename VehicleDataType>
const Vehicle &SensorView<VehicleDataType>::GetVehicle() const
{
    return GetVehicleData().vehicle;
}

template <typename VehicleDataType>
bool SensorView<VehicleDataType>::HasRoute() const
{
    return GetVehicleData().HasRoute();
}

template <typename VehicleDataType>
const Route<> &SensorView<VehicleDataType>::GetRoute() const
{
    return GetVehicleData().GetRoute();
}
} // namespace osiql
