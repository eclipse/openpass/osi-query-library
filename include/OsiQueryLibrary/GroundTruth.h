/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Representation of an osi3::GroundTruth

#include <iostream>
#include <map>
#include <set>
#include <vector>

#include <osi3/osi_groundtruth.pb.h>

#include "OsiQueryLibrary/Object/MovingObject.h"
#include "OsiQueryLibrary/Object/RoadMarking.h"
#include "OsiQueryLibrary/Object/StationaryObject.h"
#include "OsiQueryLibrary/Object/TrafficLight.h"
#include "OsiQueryLibrary/Object/TrafficSign.h"
#include "OsiQueryLibrary/Object/Vehicle.h"
#include "OsiQueryLibrary/Street/Boundary.h"
#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/ReferenceLine.h"
#include "OsiQueryLibrary/Street/Road.h"
#include "OsiQueryLibrary/Trait/Collection.h"
#include "OsiQueryLibrary/Trait/Id.h"
#include "OsiQueryLibrary/Version.h"

namespace osiql {
//! \brief Encapsulates osi3::GroundTruth which provides information about the objects in the simulated world.
//! This class is public to allow for full access of OSI, but should be used interfacing through an osiql::World,
//! as it is needed to update the world's moving objects between timesteps.
struct GroundTruth : Wrapper<osi3::GroundTruth>
{
    //! Constructs a GroundTruth from the given osi3::GroundTruth
    //!
    //! \param osi3::GroundTruth Handle of the world
    GroundTruth(const osi3::GroundTruth &);

    //! Custom destructor to deallocate objects more efficiently
    ~GroundTruth();

    //! Returns all objects of the given type.
    //!
    //! \tparam Type Type of returned objects
    //! \return Collection of objects of the given type
    template <typename Type>
    constexpr const Collection<Type> &GetAll() const;

    //! Returns all objects of the given type.
    //!
    //! \tparam Type
    //! \return Collection<Type>&
    template <typename Type>
    constexpr Collection<Type> &GetAll();

    //! Returns the object of the given type with the given id
    //! or nullptr if no such object exists.
    //!
    //! \tparam Type Type of the returned object
    //! \param id Unique identifier of the object to be returned
    //! \return Pointer to the object with the given id or nullptr if it does not exist
    template <typename Type>
    const Type *Get(Id) const;

    //! Returns the object of the given type with the given id.
    //! Does not check whether the object exists.
    //!
    //! \tparam Type
    //! \return Type&
    template <typename Type>
    Type &Find(Id);

    //! Returns whether the handle of this world has a valid assigned host vehicle id
    //!
    //! \return Whether a valid host vehicle id is stored in the world's handle
    bool HasHostVehicle() const;

    //! Returns the host vehicle of this world
    //!
    //! \return const Vehicle&
    const Vehicle &GetHostVehicle() const;

    //! \brief The interface version of this world's ground truth.
    const Version version;

    Collection<Boundary> laneBoundaries;
    Collection<LaneMarking> laneMarkings;
    Collection<Lane> lanes;
    Collection<ReferenceLine> referenceLines;
    Collection<Road> roads;

    Collection<MovingObject> movingObjects;
    Collection<RoadMarking> roadMarkings;
    Collection<StationaryObject> stationaryObjects;
    Collection<LightBulb> lightBulbs;
    Collection<TrafficLight> trafficLights;
    Collection<TrafficSign> trafficSigns;

private:
    template <typename Type>
    Collection<Type> FetchAll(const Container<typename Type::Handle> &);

    void CreateLanes();
    void ConnectLanes();
    void CreateRoads();
    static void AddConnectedLanesToRoad(Road &road);

    void LinkBoundariesAndMarkings();
    void CreateAndLinkTrafficLights();

    template <typename Type>
    void LinkToLanes(std::vector<Type> &entities);

    void LinkBoundaries(Road &);
    void LinkPredecessorLanes(Lane &);
    void LinkSuccessorLanes(Lane &);
};

std::ostream &operator<<(std::ostream &, const GroundTruth &);
} // namespace osiql
