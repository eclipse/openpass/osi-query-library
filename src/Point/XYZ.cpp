/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Point/XYZ.tpp"

namespace osiql {
double XYZ::Length() const
{
    return osiql::Length{}(*this);
}

void PrintTo(const XYZ &point, std::ostream *os)
{
    *os << point;
}
} // namespace osiql
