/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Point/XY.tpp"

#include <iostream>

#include "OsiQueryLibrary/Street/Boundary.h"
#include "OsiQueryLibrary/Street/LaneMarking.h"
#include "OsiQueryLibrary/Street/ReferenceLine.h"
#include "OsiQueryLibrary/Types/Common.h"
#include "OsiQueryLibrary/Types/Geometry.h"

namespace osiql {
XY XY::Rotate(double angle) const noexcept
{
    const double cos{std::cos(angle)};
    const double sin{std::sin(angle)};
    return {x * cos - y * sin, x * sin + y * cos};
}

[[nodiscard]] double XY::Angle() const noexcept
{
    return std::atan2(y, x);
}

double XY::Length() const
{
    // Borges, Carlos F. "An Improved Algorithm for hypot (a, b)." arXiv preprint arXiv:1904.09481 (2019).
    if (x == 0.0)
    {
        return std::abs(y);
    }

    if (y == 0.0)
    {
        return std::abs(x);
    }
    const auto hypot{std::sqrt(std::fma(x, x, y * y))};
    const auto hypotSquared{hypot * hypot};
    const auto xSquared{x * x};
    const auto correctionTerm{std::fma(-y, y, hypotSquared - xSquared) + std::fma(hypot, hypot, -hypotSquared) - std::fma(x, x, -xSquared)};
    return hypot - correctionTerm / (2 * hypot);
}

XY XY::Projection(double angle) const
{
    const XY line{std::cos(angle), std::sin(angle)};
    return line.Dot(*this) * line / line.SquaredLength();
}

double XY::ProjectionLength(double angle) const
{
    return x * std::cos(angle) + y * std::sin(angle);
}

// TODO: Generalize a & b
double XY::GetSignedDistanceTo(const XY &a, const XY &b) const
{
    return GetSide(a, b) == Side::Right ? -GetPathTo(a, b).Length() : GetPathTo(a, b).Length();
}

void PrintTo(const XY &point, std::ostream *os)
{
    *os << point;
}

std::ostream &operator<<(std::ostream &os, Winding winding)
{
    return os << detail::windingToString.at(static_cast<size_t>(winding));
}
} // namespace osiql
