/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Point/Coordinates.tpp"

#include "OsiQueryLibrary/Types/Common.h"

namespace osiql {
std::ostream &operator<<(std::ostream &os, const ST &rhs)
{
    return os << '(' << rhs.s << ", " << rhs.t << ')';
}

std::ostream &operator<<(std::ostream &os, const UV &uv)
{
    return os << '(' << uv.u << ", " << uv.v << ')';
}
} // namespace osiql
