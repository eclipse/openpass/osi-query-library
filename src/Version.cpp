/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Version.h"

#include <utility>

namespace osiql {
Version GetVersion(const osi3::InterfaceVersion &version)
{
    return {version.has_version_major() ? version.version_major() : 0, version.has_version_minor() ? version.version_minor() : 0, version.has_version_patch() ? version.version_patch() : 0};
}
Version GetVersion(const osi3::GroundTruth &groundTruth)
{
    return groundTruth.has_version() ? GetVersion(groundTruth.version()) : Version{0, 0, 0};
}
} // namespace osiql
