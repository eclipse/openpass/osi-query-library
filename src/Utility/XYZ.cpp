/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Utility/XYZ.h"

#include <limits>

#include "OsiQueryLibrary/Point/XY.tpp"
#include "OsiQueryLibrary/Street/ReferenceLine.tpp"

namespace osiql {
std::ostream &operator<<(std::ostream &os, const osi3::ReferenceLine::ReferenceLinePoint &point)
{
    if constexpr (GetOSIVersion() >= Version{3, 6, 0}) // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    {
        return os << '[' << XY{point} << ", s: " << get<S>(point) << ", t-yaw: " << (detail::GetTAxisYaw(point)) << ']';
    }
    else
    {
        return os << '[' << XY{point} << ", s: " << get<S>(point) << ']';
    }
}
} // namespace osiql
