/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/Lane/Orientable.tpp"

#include "OsiQueryLibrary/Types/Enum.tpp"
#include "OsiQueryLibrary/Utility/Extract.tpp"

namespace osiql::detail {
Direction Orientable::GetDirection(Traversal traversal) const
{
    assert(GetHandle().has_move_direction());
    return static_cast<Direction>(GetHandle().move_direction()) * traversal;
}

Traversal Orientable::GetOrientation(Direction direction) const
{
    return GetDirection() == direction ? Traversal::Forward : Traversal::Backward;
}

Side Orientable::GetSideOfRoad() const
{
    return GetDirection() == Default<Direction> ? !Default<Side> : Default<Side>;
}

double Orientable::GetS(const UV &coordinates) const
{
    return Increase(*this, coordinates.u, GetDirection());
}

double Orientable::GetLength() const
{
    return GetHandle().end_s() - GetHandle().start_s();
}
} // namespace osiql::detail
