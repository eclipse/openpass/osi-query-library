/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/Boundary.tpp"

#include "OsiQueryLibrary/Point/Coordinates.tpp"
#include "OsiQueryLibrary/Point/XY.tpp"

namespace osiql {
Container<Boundary::Point>::const_iterator Boundary::begin() const
{
    return GetHandle().boundary_line().begin();
}

Container<Boundary::Point>::const_iterator Boundary::end() const
{
    return GetHandle().boundary_line().end();
}

Side Boundary::GetPassingRule(Direction direction) const
{
    return direction == Direction::Upstream ? GetPassingRule<Direction::Upstream>() : GetPassingRule<Direction::Downstream>();
}

double Boundary::GetLateralDistance(Direction direction, const ST &coords) const
{
    return IsInverse(direction) ? GetLateralDistance<!Default<Direction>>(coords) : GetLateralDistance<Default<Direction>>(coords);
}

std::ostream &operator<<(std::ostream &os, const Boundary::Point &point)
{
    return os << "[XY" << XY(point) << ", ST" << ST(point) << ']';
}

std::ostream &operator<<(std::ostream &os, const Boundary::Points &points)
{
    return os << '{' << points.size() << " points | " << points.front() << " - " << points.back() << '}';
}

std::ostream &operator<<(std::ostream &os, const Boundary &boundary)
{
    return os << '{' << boundary.GetPassingRule<Direction::Downstream>() << " | " << boundary.front() << " - " << boundary.back() << '}';
}
} // namespace osiql
