/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/ReferenceLine.tpp"

#include <utility>

#include "OsiQueryLibrary/Point/Coordinates.tpp"
#include "OsiQueryLibrary/Point/Pose.tpp"

namespace osiql {
ReferenceLine::ReferenceLine(const osi3::ReferenceLine &handle) :
    Identifiable<osi3::ReferenceLine>::Identifiable(handle), longitudinalAxes(size()), intersections(size() - 1)
{
    assert(size() > 1);
}

ConstIterator<Container<ReferenceLine::Point>> ReferenceLine::begin() const
{
    return GetHandle().poly_line().begin();
}

ConstIterator<Container<ReferenceLine::Point>> ReferenceLine::end() const
{
    return GetHandle().poly_line().end();
}

const std::optional<XY> &ReferenceLine::GetLongitudinalAxisIntersection(size_t i) const
{
    if (!intersections[i])
    {
        auto vertex{std::next(begin(), static_cast<std::ptrdiff_t>(i))};
        const Line startingLateralAxis{*vertex, *vertex + GetLongitudinalAxis(vertex)};
        const Line endingLateralAxis{*std::next(vertex), *std::next(vertex) + GetLongitudinalAxis(std::next(vertex))};
        intersections[i] = std::make_unique<std::optional<XY>>(startingLateralAxis.GetIntersection(endingLateralAxis));
    }
    return *intersections[i];
}

XY ReferenceLine::GetXY(const ST &point) const
{
    if constexpr (GetOSIVersion() >= Version{3, 6, 0}) // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    {
        if (GetType(Type::PolylineWithTAxis) == Type::Polyline)
        {
            return Base::GetXY(point);
        }
        auto it{end(point.s)};
        if (it == begin())
        {
            ++it;
        }
        else if (it == end())
        {
            --it;
        }
        const double ratio{(point.s - std::prev(it)->s_position()) / (it->s_position() - std::prev(it)->s_position())};
        const XY pointOnEdge{XY{std::prev(it)->world_position()} * (1.0 - ratio) + XY{it->world_position()} * ratio};
        const size_t edgeIndex{static_cast<size_t>(std::distance(begin(), it) - 1)};
        const std::optional<XY> &vanishingPoint{GetLongitudinalAxisIntersection(edgeIndex)};
        if (vanishingPoint.has_value())
        {
            const XY tAxis{Norm(vanishingPoint.value() - pointOnEdge) * point.t};
            if (vanishingPoint.value().GetSide(*std::prev(it), *it) == Side::Left)
            {
                return pointOnEdge + tAxis;
            }
            return pointOnEdge - tAxis;
        }
        return pointOnEdge + GetLongitudinalAxis(it) * point.t;
    }
    else // OSI 3.5
    {
        return Base::GetXY(point);
    }
}

ST ReferenceLine::LocalizeUsingTAxes(const XY &input) const
{
    auto it{end(input)};
    if (it == begin())
    {
        ++it;
    }
    const size_t i{static_cast<size_t>(std::distance(begin(), it))};
    const std::optional<XY> &vanishingPoint{GetLongitudinalAxisIntersection(i - 1)};
    if (vanishingPoint.has_value())
    {
        const Line edge{*std::prev(it), *it};
        const XY intersection{Line{vanishingPoint.value(), input}.GetIntersection(edge).value()};
        const double ratio{edge.GetRatio(intersection)};
        return ST{
            extract<Direction::Downstream>(*std::prev(it)) * (1.0 - ratio) + extract<Direction::Downstream>(*it) * ratio,
            input.GetSide(edge.start, edge.end) == Side::Right ? -(input - intersection).Length() : (input - intersection).Length() //
        };
    }
    const XY axis{GetLongitudinalAxis(it)};
    const Line edge{*std::prev(it), *it};
    const XY path{edge.end - edge.start};
    if ((path.GetPerpendicular<Winding::CounterClockwise>() - axis).SquaredLength() > EPSILON)
    {
        const XY intersection{edge.GetIntersection(Line{input, input + axis}).value()};
        const double ratio{edge.GetRatio(intersection)};
        return ST{
            extract<Direction::Downstream>(*std::prev(it)) * (1.0 - ratio) + extract<Direction::Downstream>(*it) * ratio,
            input.GetSide(edge.start, edge.end) == Side::Right ? -(input - intersection).Length() : (input - intersection).Length() //
        };
    }
    return input.GetST(*std::prev(it), *it);
}

std::ostream &operator<<(std::ostream &os, const ReferenceLine &line)
{
    return os << "Reference Line " << line.GetId() << ": " << static_cast<const ReferenceLine::Base &>(line);
}
} // namespace osiql
