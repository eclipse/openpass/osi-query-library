/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/LaneMarking.tpp"

#include "OsiQueryLibrary/Component/Identifiable.tpp"
#include "OsiQueryLibrary/Component/Iterable.tpp"
#include "OsiQueryLibrary/Point/XY.tpp"

namespace osiql {
// LaneMarking::Vertex

double LaneMarking::Vertex::GetWidth() const
{
    return handle.has_width() ? handle.width() : .0;
}

double LaneMarking::Vertex::GetHeight() const
{
    return handle.has_height() ? handle.height() : .0;
}

double LaneMarking::Vertex::x() const
{
    return handle.position().has_x() ? handle.position().x() : .0;
}

double LaneMarking::Vertex::y() const
{
    return handle.position().has_y() ? handle.position().y() : .0;
}

LaneMarking::Vertex::Dash LaneMarking::Vertex::GetDash() const
{
    return handle.has_dash() ? static_cast<Dash>(handle.dash()) : Dash::Unknown;
}

// LaneMarking

LaneMarking::Type LaneMarking::GetType() const
{
    return (GetHandle().has_classification() && GetHandle().classification().has_type()) ? static_cast<Type>(GetHandle().classification().type()) : Type::Unknown;
}

LaneMarking::Color LaneMarking::GetColor() const
{
    return (GetHandle().has_classification() && GetHandle().classification().has_color()) ? static_cast<Color>(GetHandle().classification().color()) : Color::Unknown;
}

Container<LaneMarking::Point>::const_iterator LaneMarking::begin() const
{
    return GetHandle().boundary_line().begin();
}
Container<LaneMarking::Point>::const_iterator LaneMarking::end() const
{
    return GetHandle().boundary_line().end();
}

// Debug methods

std::ostream &operator<<(std::ostream &os, LaneMarking::Vertex::Dash dash)
{
    return os << detail::laneMarkingDashToString.at(static_cast<size_t>(dash));
}
std::ostream &operator<<(std::ostream &os, const LaneMarking::Vertex &point)
{
    return os << '[' << point.GetDash() << XY(point) << ", " << point.GetWidth() << ", " << point.GetHeight() << ']';
}

std::ostream &operator<<(std::ostream &os, LaneMarking::Type type)
{
    return os << detail::laneMarkingTypeToString.at(static_cast<size_t>(type));
}
std::ostream &operator<<(std::ostream &os, LaneMarking::Color color)
{
    return os << detail::laneMarkingColorToString.at(static_cast<size_t>(color));
}

std::ostream &operator<<(std::ostream &os, const LaneMarking::Point &point)
{
    return os << "[(" << point.position().x() << ", " << point.position().y() << "), " << static_cast<LaneMarking::Vertex::Dash>(point.dash()) << ", " << point.width() << ']';
}

std::ostream &operator<<(std::ostream &os, const LaneMarking &marking)
{
    os << "Type: " << marking.GetType() << ", Color: " << marking.GetColor() << " | ";
    if (marking.GetHandle().boundary_line_size() == 1)
    {
        os << "1 point: " << marking.front();
    }
    else
    {
        os << marking.GetHandle().boundary_line_size() << " points";
    }
    return os;
}
} // namespace osiql
