/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/Road.tpp"

#include <algorithm>
#include <numeric>

#include "OsiQueryLibrary/Street/Lane.tpp"
#include "OsiQueryLibrary/Utility/Length.tpp"

namespace osiql {
Road::Road(const ReferenceLine &referenceLine) :
    RoadBase{&referenceLine, Shape{}, Bounds{}}
{
}

Id Road::GetId() const
{
    return lanes.front()->GetId();
}

std::string_view Road::GetOpenDriveId() const
{
    return lanes.front()->GetRoadId();
}

double Road::GetDistanceTo(const XY &globalPoint) const
{
    const ST coordinates{GetReferenceLine().Localize(globalPoint)};
    assert(!lanes.empty());
    const Lane *lane{GetClosestLane(coordinates)};
    return lane->GetDistanceTo(globalPoint);
}

double Road::GetDistanceTo(const XY &point, Side side) const
{
    return IsInverse(side) ? GetDistanceTo<!Default<Side>>(point) : GetDistanceTo<Default<Side>>(point);
}

double Road::GetLength() const
{
    return osiql::Length{}(lanes.front());
}

void Road::UpdateShapeAndBounds()
{
    std::vector<XY> rightPoints{boundaries.front().GetPoints()};
    std::vector<XY> leftPoints{boundaries.back().GetPoints()};
    // Drop a point if the boundaries merge together at the start or end of the road:
    auto beginRight{leftPoints.front() == rightPoints.front() ? std::next(rightPoints.begin()) : rightPoints.begin()};
    auto beginLeft{leftPoints.back() == rightPoints.back() ? std::next(leftPoints.rbegin()) : leftPoints.rbegin()};
    this->GetShape().reserve(static_cast<size_t>(std::distance(beginRight, rightPoints.end()) + std::distance(beginLeft, leftPoints.rend())));
    this->GetShape().insert(this->GetShape().end(), std::make_move_iterator(beginRight), std::make_move_iterator(rightPoints.end()));
    this->GetShape().insert(this->GetShape().end(), std::make_move_iterator(beginLeft), std::make_move_iterator(leftPoints.rend()));
    boost::geometry::envelope(this->GetShape(), this->GetBounds());
}

std::ostream &operator<<(std::ostream &os, const Road &road)
{
    os << "Road \"" << road.GetOpenDriveId() << "\" (" << road.GetId() << ", ";
    if (road.lanes.size() == 1)
    {
        os << "1 lane, ";
    }
    else
    {
        os << road.lanes.size() << " lanes, ";
    }
    os << road.lanes.size<Side::Right>() << " Right/" << road.lanes.size<Side::Left>() << " Left)";
    if (const auto roads = Road::GetRoads(road.GetConnectedLanes<Side::Right, Traversal::Forward>()); !roads.empty())
    {
        os << "\n    Outgoing downstream roads: ";
        os << roads.front()->GetOpenDriveId();
        for (auto nextRoad{std::next(roads.begin())}; nextRoad != roads.end(); ++nextRoad)
        {
            os << ", " << (*nextRoad)->GetOpenDriveId();
        }
    }
    if (const auto roads = Road::GetRoads(road.GetConnectedLanes<Side::Left, Traversal::Forward>()); !roads.empty())
    {
        os << "\n    Outgoing upstream roads: ";
        os << roads.front()->GetOpenDriveId();
        for (auto nextRoad{std::next(roads.begin())}; nextRoad != roads.end(); ++nextRoad)
        {
            os << ", " << (*nextRoad)->GetOpenDriveId();
        }
    }
    if (const auto roads = Road::GetRoads(road.GetConnectedLanes<Side::Right, Traversal::Backward>()); !roads.empty())
    {
        os << "\n    Incoming downstream roads: ";
        os << roads.front()->GetOpenDriveId();
        for (auto previousRoad{std::next(roads.begin())}; previousRoad != roads.end(); ++previousRoad)
        {
            os << ", " << (*previousRoad)->GetOpenDriveId();
        }
    }
    if (const auto roads = Road::GetRoads(road.GetConnectedLanes<Side::Left, Traversal::Backward>()); !roads.empty())
    {
        os << "\n    Incoming upstream roads: ";
        os << roads.front()->GetOpenDriveId();
        for (auto previousRoad{std::next(roads.begin())}; previousRoad != roads.end(); ++previousRoad)
        {
            os << ", " << (*previousRoad)->GetOpenDriveId();
        }
    }
    return os;
}
} // namespace osiql
