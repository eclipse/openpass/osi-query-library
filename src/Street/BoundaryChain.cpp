/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/BoundaryChain.tpp"

#include <numeric>

#include "OsiQueryLibrary/Component/Iterable.tpp"

namespace osiql {
ConstIterator<std::vector<Boundary *>> BoundaryChain::begin() const
{
    return container.begin();
}
Iterator<std::vector<Boundary *>> BoundaryChain::begin()
{
    return container.begin();
}

ConstIterator<std::vector<Boundary *>> BoundaryChain::end() const
{
    return container.end();
}
Iterator<std::vector<Boundary *>> BoundaryChain::end()
{
    return container.end();
}

std::vector<XY> BoundaryChain::GetPoints() const
{
    std::vector<XY> points;
    if (!empty())
    {
        const size_t sizeN{std::transform_reduce(
            begin(), end(), size_t{0}, std::plus{}, [](const Boundary *b) { return b->size(); }
        )};
        points.reserve(sizeN - (size() - 1));
        std::transform(front()->begin(), front()->end(), std::back_inserter(points), [](const auto &point) { return XY{point}; });
        for (auto it{std::next(begin())}; it != end(); ++it)
        {
            std::transform(std::next((*it)->begin()), (*it)->end(), std::back_inserter(points), [](const auto &point) { return XY{point}; });
        }
        return points;
    }
    return points;
}

const Boundary &BoundaryChain::GetBoundary(Direction direction, double s) const
{
    return IsInverse(direction) ? GetBoundary<Direction::Upstream>(s) : GetBoundary<Direction::Downstream>(s);
}

Boundary &BoundaryChain::GetBoundary(Direction direction, double s)
{
    return const_cast<Boundary &>(std::as_const(*this).GetBoundary(direction, s));
}

double BoundaryChain::GetLateralDistance(Direction direction, const ST &coords) const
{
    return IsInverse(direction) ? GetLateralDistance<!Default<Direction>>(coords) : GetLateralDistance<Default<Direction>>(coords);
}

std::ostream &operator<<(std::ostream &os, const BoundaryChain &boundaries)
{
    os << '[' << boundaries.size() << ' ' << (boundaries.size() == 1 ? "Boundary" : "Boundaries") << '\n';
    for (const Boundary *boundary : boundaries)
    {
        os << "    " << *boundary << '\n';
    }
    return os << ']';
}
} // namespace osiql
