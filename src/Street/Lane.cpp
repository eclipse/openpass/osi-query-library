/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/Lane.tpp"

#include "OsiQueryLibrary/Point/Pose.tpp"
#include "OsiQueryLibrary/Point/XY.tpp"
#include "OsiQueryLibrary/Street/ReferenceLine.tpp"
#include "OsiQueryLibrary/Street/Road.tpp"

namespace osiql {
Lane::Type Lane::GetType() const
{
    if (GetHandle().has_type())
    {
        return static_cast<Lane::Type>(GetHandle().type());
    }
    std::cerr << "Lane " << GetId() << " has no assigned type.\n";
    return Type::Undefined;
}

int Lane::GetOpenDriveId() const
{
    return GetSideOfRoad() == Side::Left ? static_cast<int>(GetIndex<Side::Left>() + 1) : -static_cast<int>(GetIndex<Side::Right>() + 1);
}

std::string_view Lane::GetStreetName() const
{
    return detail::GetStreetName(this);
}

namespace detail {
double GetWidth(const Lane *lane, Traversal toward, double offset) // NOLINT(misc-no-recursion)
{
    for (const Lane *nextLane : lane->GetConnectedLanes(toward))
    {
        const double s{Increase(nextLane, -offset, nextLane->GetDirection(toward))};
        if (const double width{nextLane->GetWidth(s)}; width > 0.0)
        {
            return width;
        }
    }
    return 0.0;
}
} // namespace detail

double Lane::GetWidth(double s) const // NOLINT(misc-no-recursion)
{
    if (const double fromStartOfLane{s - extract<Direction::Downstream>(this)}; fromStartOfLane < 0.0)
    {
        return detail::GetWidth(this, GetOrientation(Direction::Upstream), fromStartOfLane);
    }
    if (const double toEndOfLane{extract<Direction::Upstream>(this) - s}; toEndOfLane < 0.0)
    {
        return detail::GetWidth(this, GetOrientation(Direction::Downstream), toEndOfLane);
    }
    const double right{GetBoundary<Side::Right>(s).GetT(s)};
    const double left{GetBoundary<Side::Left>(s).GetT(s)};
    return std::abs(left - right);
}

double Lane::GetDistanceTo(const XY &globalPoint) const
{
    const ST coords{GetRoad().GetReferenceLine().Localize(globalPoint)};
    if (coords.s < extract<Direction::Downstream>(this))
    {
        const auto &left{GetBoundaries<Side::Left, Direction::Downstream>().front()->front()};
        const auto &right{GetBoundaries<Side::Right, Direction::Downstream>().front()->front()};
        return globalPoint.GetPathTo(left, right).Length();
    }
    if (coords.s > extract<Direction::Upstream>(this))
    {
        const auto &left{GetBoundaries<Side::Left, Direction::Downstream>().back()->back()};
        const auto &right{GetBoundaries<Side::Right, Direction::Downstream>().back()->back()};
        return globalPoint.GetPathTo(left, right).Length();
    }
    const double left{GetBoundary<Side::Left, Direction::Downstream>(coords.s).GetT(coords.s)};
    if (coords.t > left)
    {
        return coords.t - left;
    }
    const double right{GetBoundary<Side::Right, Direction::Downstream>(coords.s).GetT(coords.s)};
    if (coords.t < right)
    {
        return right - coords.t;
    }
    return 0.0;
}

Point<const Lane> Lane::Localize(const XY &point) const
{
    return Point<const Lane>{GetRoad().GetReferenceLine().Localize(point), *this};
}

Pose<Point<const Lane>> Lane::Localize(const Pose<XY> &globalPose) const
{
    return Pose<Point<const Lane>>{GetRoad().GetReferenceLine().Localize(globalPose), *this};
}

std::pair<Side, size_t> Lane::GetLaneChangesTo(const Lane &target, Traversal toward) const
{
    const std::ptrdiff_t ownIndex{GetIndex(GetSideOfRoad())};
    const std::ptrdiff_t targetIndex{target.GetIndex(target.GetSideOfRoad())};
    if (GetDirection() == target.GetDirection())
    {
        return ownIndex > targetIndex                                                        //
                   ? std::make_pair(Side::Left, static_cast<size_t>(ownIndex - targetIndex)) //
                   : std::make_pair(Side::Right, static_cast<size_t>(targetIndex - ownIndex));
    }
    return std::make_pair(GetDirection(toward) == Direction::Downstream ? Side::Left : Side::Right, static_cast<size_t>(ownIndex + targetIndex + 1));
}

std::ostream &operator<<(std::ostream &os, Lane::Type type)
{
    return os << detail::laneTypeToString.at(static_cast<size_t>(type));
}

std::ostream &operator<<(std::ostream &os, const Lane &lane)
{
    os << "Lane " << lane.GetId() << " (" << lane.index << ", " << lane.GetDirection()
       << ", Road \"" << lane.GetRoadId() << "\" (" << lane.GetRoad().GetId() << ")): ";
    os << "s[" << extract<Direction::Downstream>(lane) << ", " << extract<Direction::Upstream>(lane) << "], left[";
    {
        if (lane.GetHandle().left_boundary_id().empty())
        {
            os << "NONE";
        }
        else
        {
            auto it{lane.GetHandle().left_boundary_id().begin()};
            os << (it++)->value();
            for (; it != lane.GetHandle().left_boundary_id().end(); ++it)
            {
                os << ", " << it->value();
            }
        }
    }
    os << "], right[";
    {
        if (lane.GetHandle().right_boundary_id().empty())
        {
            os << "NONE";
        }
        else
        {
            auto it{lane.GetHandle().right_boundary_id().begin()};
            os << (it++)->value();
            for (; it != lane.GetHandle().right_boundary_id().end(); ++it)
            {
                os << ", " << it->value();
            }
        }
    }
    os << "], ref " << lane.GetRoad().GetReferenceLine().GetId()
       << ", " << lane.GetOverlapping<MovingObject>().size() << " objs, out[";
    {
        const auto &lanes{lane.GetConnectedLanes<Traversal::Forward>()};
        if (lanes.empty())
        {
            os << "None";
        }
        else
        {
            auto it{lanes.begin()};
            os << (*it)->GetId() << " (" << (*it)->GetRoadId() << ", " << (*it)->GetRoad().GetId() << ')';
            for (++it; it != lanes.end(); ++it)
            {
                os << ", " << (*it)->GetId() << " (" << (*it)->GetRoadId() << ", " << (*it)->GetRoad().GetId() << ')';
            }
        }
    }
    os << "], in[";
    {
        const auto &lanes{lane.GetConnectedLanes<Traversal::Backward>()};
        if (lanes.empty())
        {
            os << "None";
        }
        else
        {
            auto it{lanes.begin()};
            os << (*it)->GetId() << " (" << (*it)->GetRoadId() << ", " << (*it)->GetRoad().GetId() << ')';
            for (++it; it != lanes.end(); ++it)
            {
                os << ", " << (*it)->GetId() << " (" << (*it)->GetRoadId() << ", " << (*it)->GetRoad().GetId() << ')';
            }
        }
    }
    return os << ']';
}
} // namespace osiql
