/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/Road/Lanes.tpp"

#include "OsiQueryLibrary/Street/Lane.tpp"

namespace osiql {
ConstIterator<std::vector<Lane *>> Lanes::begin() const
{
    return container.begin();
}

Iterator<std::vector<Lane *>> Lanes::begin()
{
    return container.begin();
}

ConstIterator<std::vector<Lane *>> Lanes::end() const
{
    return container.end();
}

Iterator<std::vector<Lane *>> Lanes::end()
{
    return container.end();
}

const Lane *Lanes::on(Side side, size_t i) const
{
    return IsInverse(side) ? on<!Default<Side>>(i) : on<Default<Side>>(i);
}

Lane *Lanes::on(Side side, size_t i)
{
    return IsInverse(side) ? on<!Default<Side>>(i) : on<Default<Side>>(i);
}

size_t Lanes::size(Side side) const
{
    return IsInverse(side) ? size<!Default<Side>>() : size<Default<Side>>();
}
} // namespace osiql
