/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/Road/BoundaryChains.tpp"

#include "OsiQueryLibrary/Component/Iterable.tpp"

namespace osiql {
ConstIterator<std::vector<BoundaryChain>> BoundaryChains::begin() const
{
    return container.begin();
}
Iterator<std::vector<BoundaryChain>> BoundaryChains::begin()
{
    return container.begin();
}

ConstIterator<std::vector<BoundaryChain>> BoundaryChains::end() const
{
    return container.end();
}
Iterator<std::vector<BoundaryChain>> BoundaryChains::end()
{
    return container.end();
}

const BoundaryChain &BoundaryChains::on(Side side, std::ptrdiff_t i) const
{
    return IsInverse(side) ? on<!Default<Side>>(i) : on<Default<Side>>(i);
}

BoundaryChain &BoundaryChains::on(Side side, std::ptrdiff_t i)
{
    return const_cast<BoundaryChain &>(std::as_const(*this).on(side, i));
}
} // namespace osiql
