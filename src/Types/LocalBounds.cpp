/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Types/LocalBounds.tpp"

#include "OsiQueryLibrary/Types/Enum/Direction.tpp"
#include "OsiQueryLibrary/Types/Enum/Side.tpp"

namespace osiql {
template <typename T>
void LocalBounds::Add(const T &object)
{
    s.min = std::min(s.min, extract<Default<Direction>>(object));
    s.max = std::max(s.max, extract<!Default<Direction>>(object));
    t.min = std::min(t.min, extract<Default<Side>>(object));
    t.max = std::max(t.max, extract<!Default<Side>>(object));
}

std::ostream &operator<<(std::ostream &os, const LocalBounds &bounds)
{
    return os << "[s: " << bounds.s << ", t: " << bounds.t << ']';
}
} // namespace osiql
