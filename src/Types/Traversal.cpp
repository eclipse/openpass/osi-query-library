/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Types/Enum/Traversal.tpp"

namespace osiql {
std::ostream &operator<<(std::ostream &os, Traversal orientation)
{
    return os << detail::orientationToString.at(static_cast<size_t>(orientation));
}
} // namespace osiql
