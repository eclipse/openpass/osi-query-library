/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Types/Matrix.h"

namespace osiql {
Matrix<2> RotationMatrix(double angle)
{
    const double cos{std::cos(angle)};
    const double sin{std::sin(angle)};
    return std::array<Row<2>, 2>{
        Row<2>{cos, -sin},
        Row<2>{sin, cos} //
    };
}
} // namespace osiql
