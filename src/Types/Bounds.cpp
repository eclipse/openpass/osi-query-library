/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Types/Bounds.h"

#include "OsiQueryLibrary/Point/XY.tpp"

namespace osiql {
XY GetDistanceBetween(const Bounds &a, const Bounds &b)
{
    return {
        std::max({a.min_corner().x - b.max_corner().x, b.min_corner().x - a.max_corner().x, 0.0}),
        std::max({a.min_corner().y - b.max_corner().y, b.min_corner().y - a.max_corner().y, 0.0})};
}

XY GetDistanceBetween(const Bounds &a, const XY &b)
{
    return {
        std::max({a.min_corner().x - b.x, b.x - a.max_corner().x, 0.0}),
        std::max({a.min_corner().y - b.y, b.y - a.max_corner().y, 0.0})};
}
XY GetDistanceBetween(const XY &a, const Bounds &b)
{
    return {
        std::max({a.x - b.max_corner().x, b.min_corner().x - a.x, 0.0}),
        std::max({a.y - b.max_corner().y, b.min_corner().y - a.y, 0.0})};
}

std::ostream &operator<<(std::ostream &os, const Bounds &b)
{
    return os << "[X(" << b.min_corner().x << " - " << b.max_corner().x << ')'
              << ", Y(" << b.min_corner().y << " - " << b.max_corner().y << ")]";
}
} // namespace osiql
