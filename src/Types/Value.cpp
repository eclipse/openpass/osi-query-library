/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Types/Value.h"

namespace osiql {
const std::map<osi3::TrafficSignValue_Unit, std::pair<double, Value::Unit>> Value::units = {
    {osi3::TrafficSignValue_Unit_UNIT_KILOMETER_PER_HOUR, {0.27778, Unit::MeterPerSecond}},
    {osi3::TrafficSignValue_Unit_UNIT_MILE_PER_HOUR, {0.44704, Unit::MeterPerSecond}},
    {osi3::TrafficSignValue_Unit_UNIT_KILOMETER, {1000.0, Unit::Meter}},
    {osi3::TrafficSignValue_Unit_UNIT_METER, {1.0, Unit::Meter}},
    {osi3::TrafficSignValue_Unit_UNIT_MILE, {1609.34, Unit::Meter}},
    {osi3::TrafficSignValue_Unit_UNIT_FEET, {0.3048, Unit::Meter}},
    {osi3::TrafficSignValue_Unit_UNIT_PERCENTAGE, {1.0, Unit::Percentage}},
    {osi3::TrafficSignValue_Unit_UNIT_METRIC_TON, {1000.0, Unit::Kilogram}},
    {osi3::TrafficSignValue_Unit_UNIT_SHORT_TON, {907.185, Unit::Kilogram}},
    {osi3::TrafficSignValue_Unit_UNIT_LONG_TON, {1016.05, Unit::Kilogram}},
    {osi3::TrafficSignValue_Unit_UNIT_MINUTES, {60.0, Unit::Second}} //
};

Value::Value(const osi3::TrafficSignValue &value)
{
    if (value.has_value() && value.has_value_unit())
    {
        const auto it{units.find(value.value_unit())};
        if (it != units.end())
        {
            unit = it->second.second;
            this->value = it->second.first * value.value();
        }
        else
        {
            this->value = value.value();
        }
    }
}

Value::operator double() const
{
    return value;
}

std::ostream &operator<<(std::ostream &os, Value::Unit unit)
{
    return os << detail::unitToString.at(static_cast<size_t>(unit));
}
} // namespace osiql
