/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Types/Geometry.h"

#include "OsiQueryLibrary/Point/XY.tpp"

namespace osiql {
std::optional<XY> Line::GetIntersection(const Line &other) const
{
    const XY edgeA{this->start - this->end};
    const XY edgeB{other.start - other.end};
    const double area{edgeA.Cross(edgeB)};
    return area == 0.0 ? std::nullopt : std::optional<XY>{(edgeB * this->start.Cross(this->end) - edgeA * other.start.Cross(other.end)) / area};
}

double Line::GetRatio(const XY &xy) const
{
    return start.x != end.x ? (xy.x - start.x) / (end.x - start.x)
                            : (xy.y - start.y) / (end.y - start.y);
}

std::ostream &operator<<(std::ostream &os, const Line &line)
{
    return os << '[' << line.start << " - " << line.end << ']';
}
} // namespace osiql
