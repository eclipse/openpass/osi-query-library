/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Object/SupplementarySign.tpp"

namespace osiql {
SupplementarySign::Type SupplementarySign::GetType() const
{
    if (!GetHandle().has_classification() || !GetHandle().classification().has_type())
    {
        return SupplementarySign::Type::Unknown;
    }
    return static_cast<Type>(GetHandle().classification().type());
}

std::ostream &operator<<(std::ostream &os, SupplementarySign::Actor actor)
{
    return os << detail::supplementarySignActorToString.at(static_cast<size_t>(actor));
}
std::ostream &operator<<(std::ostream &os, SupplementarySign::Type type)
{
    return os << detail::supplementarySignTypeToString.at(static_cast<size_t>(type));
}
std::ostream &operator<<(std::ostream &os, const SupplementarySign &sign)
{
    os << sign.GetType();
    if (sign.GetHandle().has_classification())
    {
        const auto &laneAssignments{sign.GetHandle().classification().logical_lane_assignment()};
        if (!laneAssignments.empty())
        {
            os << '[';
            auto it{laneAssignments.begin()};
            os << it->assigned_lane_id().value();
            for (; it != laneAssignments.end(); ++it)
            {
                os << ", " << it->assigned_lane_id().value();
            }
            os << ']';
        }
    }
    return os;
}
} // namespace osiql
