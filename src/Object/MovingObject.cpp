/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Object/MovingObject.tpp"

#include "OsiQueryLibrary/Routing/Node.tpp"

namespace osiql {
MovingObject &MovingObject::operator=(const osi3::MovingObject &handle)
{
    this->handle = &handle;
    OutdateTranslation();
    OutdateRotation();
    OutdateSpin();
    OutdateVelocity();
    UpdateShapeAndBounds();
    return *this;
}

void MovingObject::OutdateRotation()
{
    Locatable<osi3::MovingObject>::OutdateRotation();
    spinAndRotationMatrix = std::nullopt;
}

void MovingObject::OutdateSpin()
{
    spinMatrix = std::nullopt;
    spinAndRotationMatrix = std::nullopt;
    localMovementMatrix = std::nullopt;
    globalMovementMatrix = std::nullopt;
}

void MovingObject::OutdateVelocity()
{
    velocityMatrix = std::nullopt;
    localMovementMatrix = std::nullopt;
    globalMovementMatrix = std::nullopt;
}

Rotation MovingObject::GetAngularVelocity() const
{
    if (GetBase().has_orientation_rate())
    {
        return {
            GetBase().orientation_rate().has_roll() ? GetBase().orientation_rate().roll() : 0.0,
            GetBase().orientation_rate().has_pitch() ? GetBase().orientation_rate().pitch() : 0.0,
            GetBase().orientation_rate().has_yaw() ? GetBase().orientation_rate().yaw() : 0.0,
        };
    }
    std::cerr << "Error: GetAngularVelocity() - Moving object " << GetId() << " has no assigned angular velocity.\n";
    return {};
}

double MovingObject::GetRollVelocity() const
{
    if (GetBase().has_orientation_rate())
    {
        if (GetBase().orientation_rate().has_roll())
        {
            return GetBase().orientation_rate().roll();
        }
        std::cerr << "Error: GetRollVelocity() - Angular velocity of moving object " << GetId() << " has no assigned roll.\n";
        return 0.0;
    }
    std::cerr << "Error: GetRollVelocity() - Moving object " << GetId() << " has no assigned rotational velocity.\n";
    return 0.0;
}

double MovingObject::GetPitchVelocity() const
{
    if (GetBase().has_orientation_rate())
    {
        if (GetBase().orientation_rate().has_pitch())
        {
            return GetBase().orientation_rate().pitch();
        }
        std::cerr << "Error: GetPitchVelocity() - Angular velocity of moving object " << GetId() << " has no assigned pitch.\n";
        return 0.0;
    }
    std::cerr << "Error: GetPitchVelocity() - Moving object " << GetId() << " has no assigned rotational velocity.\n";
    return 0.0;
}

double MovingObject::GetYawVelocity() const
{
    if (GetBase().has_orientation_rate())
    {
        if (GetBase().orientation_rate().has_yaw())
        {
            return GetBase().orientation_rate().yaw();
        }
        std::cerr << "Error: GetYawVelocity() - Angular velocity of moving object " << GetId() << " has no assigned yaw.\n";
        return 0.0;
    }
    std::cerr << "Error: GetYawVelocity() - Moving object " << GetId() << " has no assigned rotational velocity.\n";
    return 0.0;
}

Rotation MovingObject::GetAngularAcceleration() const
{
    if (GetBase().has_orientation_acceleration())
    {
        return {
            GetBase().orientation_acceleration().has_roll() ? GetBase().orientation_acceleration().roll() : 0.0,
            GetBase().orientation_acceleration().has_pitch() ? GetBase().orientation_acceleration().pitch() : 0.0,
            GetBase().orientation_acceleration().has_yaw() ? GetBase().orientation_acceleration().yaw() : 0.0,
        };
    }
    std::cerr << "Error: GetAngularAcceleration() - Moving object " << GetId() << " has no assigned angular acceleration.\n";
    return {};
}

double MovingObject::GetRollAcceleration() const
{
    if (GetBase().has_orientation_acceleration())
    {
        if (GetBase().orientation_acceleration().has_roll())
        {
            return GetBase().orientation_acceleration().roll();
        }
        std::cerr << "Error: GetRollAcceleration() - Angular acceleration of moving object " << GetId() << " has no assigned roll.\n";
        return 0.0;
    }
    std::cerr << "Error: GetRollAcceleration() - Moving object " << GetId() << " has no assigned rotational acceleration.\n";
    return 0.0;
}

double MovingObject::GetPitchAcceleration() const
{
    if (GetBase().has_orientation_acceleration())
    {
        if (GetBase().orientation_acceleration().has_pitch())
        {
            return GetBase().orientation_acceleration().pitch();
        }
        std::cerr << "Error: GetPitchAcceleration() - Angular acceleration of moving object " << GetId() << " has no assigned pitch.\n";
        return 0.0;
    }
    std::cerr << "Error: GetPitchAcceleration() - Moving object " << GetId() << " has no assigned rotational acceleration.\n";
    return 0.0;
}

double MovingObject::GetYawAcceleration() const
{
    if (GetBase().has_orientation_acceleration())
    {
        if (GetBase().orientation_acceleration().has_yaw())
        {
            return GetBase().orientation_acceleration().yaw();
        }
        std::cerr << "Error: GetYawAcceleration() - Angular acceleration of moving object " << GetId() << " has no assigned yaw.\n";
        return 0.0;
    }
    std::cerr << "Error: GetYawAcceleration() - Moving object " << GetId() << " has no assigned rotational acceleration.\n";
    return 0.0;
}

MovingObject::Type MovingObject::GetType() const
{
    return GetHandle().has_type() ? static_cast<Type>(GetHandle().type()) : Type::Unknown;
}

const Matrix<3> &MovingObject::GetSpinMatrix() const
{
    if (!spinMatrix.has_value())
    {
        spinMatrix = AntimetricMatrix(GetAngularVelocity());
    }
    return spinMatrix.value();
}

const Matrix<4> &MovingObject::GetVelocityMatrix() const
{
    if (!velocityMatrix.has_value())
    {
        velocityMatrix = TranslationMatrix(GetVelocity());
    }
    return velocityMatrix.value();
}

const Matrix<4> &MovingObject::GetLocalMovementMatrix() const
{
    if (!localMovementMatrix.has_value())
    {
        localMovementMatrix = GetVelocityMatrix() * GetSpinMatrix();
    }
    return localMovementMatrix.value();
}

const Matrix<4> &MovingObject::GetGlobalMovementMatrix() const
{
    if (!globalMovementMatrix.has_value())
    {
        globalMovementMatrix = GetVelocityMatrix() * GetSpinAndRotationMatrix();
    }
    return globalMovementMatrix.value();
}

const Matrix<3> &MovingObject::GetSpinAndRotationMatrix() const
{
    if (!spinAndRotationMatrix.has_value())
    {
        spinAndRotationMatrix = GetRotationMatrix() * GetSpinMatrix();
    }
    return spinAndRotationMatrix.value();
}

const Matrix<4> &MovingObject::GetAccelerationMatrix() const
{
    if (!accelerationMatrix.has_value())
    {
        accelerationMatrix = TranslationMatrix(GetAcceleration());
    }
    return accelerationMatrix.value();
}

const Matrix<3> &MovingObject::GetAngularAccelerationMatrix() const
{
    if (!angularAccelerationMatrix.has_value())
    {
        angularAccelerationMatrix = AntimetricMatrix(GetAngularAcceleration());
    }
    return angularAccelerationMatrix.value();
}

const Matrix<4> &MovingObject::GetFullAccelerationMatrix() const
{
    if (!fullAccelerationMatrix.has_value())
    {
        fullAccelerationMatrix = GetAccelerationMatrix() * GetAngularAccelerationMatrix();
    }
    return fullAccelerationMatrix.value();
}

Distances MovingObject::GetApproximateBoundsDistancesToBoundaries(const Lane &lane, Traversal traversal) const
{
    // TODO: Performance - Search overlaps before computing bounds
    const auto localShape{lane.GetRoad().GetReferenceLine().Localize(GetShape().begin(), GetShape().end())};
    const LocalBounds bounds{
        {extract<Direction::Downstream>(*std::min_element(localShape.begin(), localShape.end(), Extract<Direction::Downstream>::less)),
         extract<Direction::Downstream>(*std::max_element(localShape.begin(), localShape.end(), Extract<Direction::Downstream>::less))},
        {extract<Side::Left>(*std::min_element(localShape.begin(), localShape.end(), Extract<Side::Left>::less)),
         extract<Side::Left>(*std::max_element(localShape.begin(), localShape.end(), Extract<Side::Left>::less))},
    };

    if (lane.GetDirection(traversal) == Direction::Upstream)
    {
        const auto &left{lane.GetBoundary<Side::Left, Direction::Upstream>(bounds.s.min)};
        const double leftT{std::max(left.GetT(bounds.s.min), left.GetT(bounds.s.max))};

        const auto &right{lane.GetBoundary<Side::Right, Direction::Upstream>(bounds.s.min)};
        const double rightT{std::min(right.GetT(bounds.s.min), right.GetT(bounds.s.max))};

        return {rightT - bounds.t.min, bounds.t.max - leftT};
    }
    const auto &left{lane.GetBoundary<Side::Left, Direction::Downstream>(bounds.s.max)};
    const double leftT{std::min(left.GetT(bounds.s.min), left.GetT(bounds.s.max))};

    const auto &right{lane.GetBoundary<Side::Right, Direction::Downstream>(bounds.s.max)};
    const double rightT{std::max(right.GetT(bounds.s.min), right.GetT(bounds.s.max))};

    return {leftT - bounds.t.max, bounds.t.min - rightT};
}

std::ostream &operator<<(std::ostream &os, const MovingObject &object)
{
    return os << "{Moving Object - Type: " << object.GetType() << ", Id: " << object.GetId() << ", XY: " << object.GetXY() << ", Size: " << object.GetSize() << '}';
}
std::ostream &operator<<(std::ostream &os, MovingObject::Type type)
{
    return os << detail::movingObjectTypeToString.at(static_cast<size_t>(type));
}

std::ostream &operator<<(std::ostream &os, const Distances &distances)
{
    return os << "[left: " << distances.left << ", right: " << distances.right << ']';
}

void SetDimensions(osi3::Dimension3d &output, double length, double width, double height) // NOLINT(bugprone-easily-swappable-parameters)
{
    output.set_length(length);
    output.set_width(width);
    output.set_height(height);
}

void SetDimensions(osi3::BaseMoving &output, double length, double width, double height) // NOLINT(bugprone-easily-swappable-parameters)
{
    SetDimensions(*output.mutable_dimension(), length, width, height);
}

void SetDimensions(osi3::MovingObject &output, double length, double width, double height) // NOLINT(bugprone-easily-swappable-parameters)
{
    SetDimensions(*output.mutable_base(), length, width, height);
}
} // namespace osiql
