/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Object/VehicleData.tpp"

namespace osiql {
VehicleData::VehicleData(const Vehicle &vehicle) :
    vehicle{vehicle}
{
}

VehicleData::VehicleData(const Vehicle &vehicle, std::shared_ptr<Route<>> &route) :
    vehicle{vehicle}, route{route}
{
    UpdateOnRoute();
}

void VehicleData::UpdateOnRoute()
{
    assert(HasRoute());
    const auto &allOverlaps{vehicle.GetOverlaps<Road>()};
    Interval<ConstIterator<std::vector<osiql::Overlap<osiql::Road>>>> overlaps{allOverlaps.end()};
    auto startNode = route->TraverseUntil([&allOverlaps, &overlaps](const auto &node) {
        overlaps.min = std::find_if(allOverlaps.begin(), allOverlaps.end(), Matches<Road>{*node});
        return overlaps.min != allOverlaps.end();
    });
    nodes = std::distance(route->cbegin(), startNode);
    if (startNode != route->end())
    {
        distance = (*startNode)->GetDistance(*overlaps.min);
        auto endNode = std::prev(TraverseWhile(std::next(startNode), route->cend(), [&allOverlaps, &overlaps](const auto &node) {
            overlaps.max = std::find_if(allOverlaps.begin(), allOverlaps.end(), Matches<Road>{*node});
            return overlaps.max != allOverlaps.end();
        }));
        if (startNode != endNode)
        {
            distance.max = (*endNode)->GetDistance(*overlaps.max).max;
            nodes.max = std::distance(route->cbegin(), endNode);
        }
    }
}

bool VehicleData::HasRoute() const
{
    return route != nullptr;
}

bool VehicleData::IsOnRoute() const
{
    return route && static_cast<size_t>(nodes.max) < route->size();
}

const Route<> &VehicleData::GetRoute() const
{
    return *route;
}

void VehicleData::SetRoute(std::shared_ptr<Route<>> route)
{
    this->route = std::move(route);
    if (this->route)
    {
        UpdateOnRoute();
    }
}

Interval<ConstIterator<Route<>>> VehicleData::GetTouchedNodes() const
{
    assert(HasRoute());
    return {GetFirstNodeIterator(), std::min(route->cend(), std::next(GetLastNodeIterator()))};
}

ConstIterator<Route<>> VehicleData::GetFirstNodeIterator() const
{
    return std::next(route->begin(), nodes.min);
}

ConstIterator<Route<>> VehicleData::GetLastNodeIterator() const
{
    return std::next(route->begin(), nodes.max);
}

const Node<> &VehicleData::GetFirstTouchedNode() const
{
    return **GetFirstNodeIterator();
}

const Node<> &VehicleData::GetLastTouchedNode() const
{
    return **GetLastNodeIterator();
}

double SteeringWheel::GetAngle() const
{
    return GetHandle().has_angle() ? GetHandle().angle() : .0;
}

double SteeringWheel::GetAngularSpeed() const
{
    return GetHandle().has_angular_speed() ? GetHandle().angular_speed() : .0;
}

double SteeringWheel::GetTorque() const
{
    return GetHandle().has_torque() ? GetHandle().torque() : .0;
}

HostVehicleData::HostVehicleData(const osi3::HostVehicleData &data, const Vehicle &vehicle) :
    VehicleData{vehicle}, Identifiable<osi3::HostVehicleData>{data}
{
}

HostVehicleData::HostVehicleData(const osi3::HostVehicleData &data, const Vehicle &vehicle, std::shared_ptr<Route<>> &route) :
    VehicleData{vehicle, route}, Identifiable<osi3::HostVehicleData>{data}
{
    UpdateOnRoute();
}

double HostVehicleData::GetAccelerationPedalPosition() const
{
    if (GetHandle().has_vehicle_powertrain() && GetHandle().vehicle_powertrain().has_pedal_position_acceleration())
    {
        return GetHandle().vehicle_powertrain().pedal_position_acceleration();
    }
    return std::numeric_limits<double>::quiet_NaN();
}

double HostVehicleData::GetBrakePedalPosition() const
{
    if (GetHandle().has_vehicle_brake_system() && GetHandle().vehicle_brake_system().has_pedal_position_brake())
    {
        return GetHandle().vehicle_brake_system().pedal_position_brake();
    }
    return std::numeric_limits<double>::quiet_NaN();
}

double HostVehicleData::GetClutchPedalPosition() const
{
    if (GetHandle().has_vehicle_powertrain() && GetHandle().vehicle_powertrain().has_pedal_position_clutch())
    {
        return GetHandle().vehicle_powertrain().pedal_position_clutch();
    }
    return std::numeric_limits<double>::quiet_NaN();
}

SteeringWheel HostVehicleData::GetSteeringWheel() const
{
    assert(GetHandle().has_vehicle_steering());
    assert(GetHandle().vehicle_steering().has_vehicle_steering_wheel());
    return SteeringWheel{GetHandle().vehicle_steering().vehicle_steering_wheel()};
}

double HostVehicleData::GetVehicleWeight() const
{
    if (GetHandle().has_vehicle_basics() && GetHandle().vehicle_basics().has_curb_weight())
    {
        return GetHandle().vehicle_basics().curb_weight();
    }
    return std::numeric_limits<double>::quiet_NaN();
}

OperatingState HostVehicleData::GetVehicleState() const
{
    if (GetHandle().has_vehicle_basics() && GetHandle().vehicle_basics().has_operating_state())
    {
        return static_cast<OperatingState>(GetHandle().vehicle_basics().operating_state());
    }
    return OperatingState::Unknown;
}
} // namespace osiql
