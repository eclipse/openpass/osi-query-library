/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Query.tpp"

#include "OsiQueryLibrary/Object/MovingObject.tpp"
#include "OsiQueryLibrary/Point/Point.tpp"
#include "OsiQueryLibrary/Point/XY.tpp"
#include "OsiQueryLibrary/Street/Lane.tpp"
#include "OsiQueryLibrary/Utility/Get.tpp"

namespace osiql {
Query::Query(const osi3::GroundTruth &groundTruth) :
    world{std::make_unique<World>(groundTruth)}
{
    for (auto &[id, lane] : world->lanes)
    {
        lanes.insert(&lane);
    }
    for (auto &[id, road] : world->roads)
    {
        roads.insert(&road);
    }

    const auto &allObjects{groundTruth.moving_object()};
    priorMovingObjectIds.reserve(static_cast<size_t>(allObjects.size()));
    std::transform(allObjects.begin(), allObjects.end(), std::back_inserter(priorMovingObjectIds), Get<Id>{});

    for (auto &item : world->GetAll<MovingObject>())
    {
        UpdateObject(get<MovingObject>(item));
        movingObjects.insert(&get<MovingObject>(item));
    }

    for (auto &object : world->GetAll<StaticObject>())
    {
        UpdateObject(object);
        staticObjects.insert(&object);
    }
}

const World &Query::GetWorld() const
{
    return *world;
}

#define DEFINE_QUERY_GET(T)             \
    const T *Query::Get##T(Id id) const \
    {                                   \
        return world->Get<T>(id);       \
    }
DEFINE_QUERY_GET(Boundary)
DEFINE_QUERY_GET(Lane)
DEFINE_QUERY_GET(MovingObject)
DEFINE_QUERY_GET(ReferenceLine)
DEFINE_QUERY_GET(RoadMarking)
DEFINE_QUERY_GET(StaticObject)
DEFINE_QUERY_GET(TrafficLight)
DEFINE_QUERY_GET(TrafficSign)

const Vehicle &Query::GetHostVehicle() const
{
    return world->GetHostVehicle();
}

void Query::Update(const Container<osi3::MovingObject> &objects)
{
    // Remove all overlaps

    for (auto &[id, lane] : world->lanes)
    {
        lane.template GetOverlapping<MovingObject>().clear();
    }
    for (auto &[id, road] : world->roads)
    {
        road.template GetOverlapping<MovingObject>().clear();
    }

    // Spawn/Despawn objects

    assert(!priorMovingObjectIds.empty());
    assert(!objects.empty());
    auto priorObjectId{priorMovingObjectIds.begin()};
    auto object{objects.begin()};
    while (priorObjectId != priorMovingObjectIds.end() && object != objects.end())
    {
        if (*priorObjectId == get<Id>(*object))
        {
            auto &wrapper{*world->GetAll<MovingObject>().find(get<Id>(*object))->second};
            wrapper = *object;
            UpdateObject(wrapper);
            ++priorObjectId;
            ++object;
        }
        else if (*priorObjectId < get<Id>(*object))
        {
            world->GetAll<MovingObject>().erase(*priorObjectId);
            ++priorObjectId;
        }
        else
        {
            UpdateObject(Emplace<MovingObject>(world->GetAll<MovingObject>(), *object));
            ++object;
        }
    }
    for (; priorObjectId != priorMovingObjectIds.end(); ++priorObjectId)
    {
        world->GetAll<MovingObject>().erase(*priorObjectId);
    }
    for (; object != objects.end(); ++object)
    {
        UpdateObject(Emplace<MovingObject>(world->GetAll<MovingObject>(), *object));
    }
    // TODO: Performance - Track if there has been a change and refresh prior Ids only from the first change
    priorMovingObjectIds.clear();
    priorMovingObjectIds.reserve(static_cast<size_t>(objects.size()));
    std::transform(objects.begin(), objects.end(), std::back_inserter(priorMovingObjectIds), Get<Id>{});

    // Refresh RTree

    GetRTree<MovingObject>().clear();
    for (auto &item : world->GetAll<MovingObject>())
    {
        GetRTree<MovingObject>().insert(&get<MovingObject>(item));
    }
}

std::ostream &operator<<(std::ostream &os, const Query &query)
{
    return os << query.GetWorld();
}
} // namespace osiql
