/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/World.tpp"

#include "OsiQueryLibrary/Object/MovingObject.tpp"
#include "OsiQueryLibrary/Point/Point.tpp"
#include "OsiQueryLibrary/Point/XY.tpp"
#include "OsiQueryLibrary/Street/Lane.tpp"
#include "OsiQueryLibrary/Utility/Get.tpp"

namespace osiql {
World::World(const osi3::GroundTruth &groundTruth) :
    GroundTruth{groundTruth}
{
    for (auto &[id, lane] : lanes)
    {
        laneRTree.insert(&lane);
    }
    for (auto &[id, road] : roads)
    {
        roadRTree.insert(&road);
    }

    const auto &allObjects{groundTruth.moving_object()};
    priorMovingObjectIds.reserve(static_cast<size_t>(allObjects.size()));
    std::transform(allObjects.begin(), allObjects.end(), std::back_inserter(priorMovingObjectIds), osiql::Get<Id>{});

    for (auto &item : GetAll<MovingObject>())
    {
        UpdateObject(get<MovingObject>(item));
        movingObjectRTree.insert(&get<MovingObject>(item));
    }

    for (auto &object : GetAll<StationaryObject>())
    {
        UpdateObject(object);
        stationaryObjectRTree.insert(&object);
    }
}

#define OSIQL_DEFINE_QUERY_GET(T)       \
    const T *World::Get##T(Id id) const \
    {                                   \
        return Get<T>(id);              \
    }
OSIQL_DEFINE_QUERY_GET(Boundary)
OSIQL_DEFINE_QUERY_GET(Lane)
OSIQL_DEFINE_QUERY_GET(MovingObject)
OSIQL_DEFINE_QUERY_GET(ReferenceLine)
OSIQL_DEFINE_QUERY_GET(RoadMarking)
OSIQL_DEFINE_QUERY_GET(StationaryObject)
OSIQL_DEFINE_QUERY_GET(TrafficLight)
OSIQL_DEFINE_QUERY_GET(TrafficSign)

const Vehicle *World::GetVehicle(Id id) const
{
    return static_cast<const Vehicle *>(GetMovingObject(id)); // NOLINT(cppcoreguidelines-pro-type-static-cast-downcast)
}

void World::Update(const Container<osi3::MovingObject> &objects)
{
    // Remove all overlaps

    for (auto &[id, lane] : lanes)
    {
        lane.template GetOverlapping<MovingObject>().clear();
    }
    for (auto &[id, road] : roads)
    {
        road.template GetOverlapping<MovingObject>().clear();
    }

    // Spawn/Despawn objects

    auto priorObjectId{priorMovingObjectIds.begin()};
    auto object{objects.begin()};
    while (priorObjectId != priorMovingObjectIds.end() && object != objects.end())
    {
        if (*priorObjectId == get<Id>(*object))
        {
            auto &wrapper{*GetAll<MovingObject>().find(get<Id>(*object))->second};
            wrapper = *object;
            UpdateObject(wrapper);
            ++priorObjectId;
            ++object;
        }
        else if (*priorObjectId < get<Id>(*object))
        {
            GetAll<MovingObject>().erase(*priorObjectId);
            ++priorObjectId;
        }
        else
        {
            UpdateObject(Emplace<MovingObject>(GetAll<MovingObject>(), *object));
            ++object;
        }
    }
    for (; priorObjectId != priorMovingObjectIds.end(); ++priorObjectId)
    {
        GetAll<MovingObject>().erase(*priorObjectId);
    }
    for (; object != objects.end(); ++object)
    {
        UpdateObject(Emplace<MovingObject>(GetAll<MovingObject>(), *object));
    }
    // TODO: Performance - Track if there has been a change and refresh prior Ids only from the first change
    priorMovingObjectIds.clear();
    priorMovingObjectIds.reserve(static_cast<size_t>(objects.size()));
    std::transform(objects.begin(), objects.end(), std::back_inserter(priorMovingObjectIds), osiql::Get<Id>{});

    // Refresh RTree

    GetRTree<MovingObject>().clear();
    for (auto &item : GetAll<MovingObject>())
    {
        GetRTree<MovingObject>().insert(&get<MovingObject>(item));
    }
}

World *GetWorld(const osi3::GroundTruth &groundTruth)
{
    if (!groundTruth.has_environmental_conditions())
    {
        return nullptr;
    }
    const auto &data{groundTruth.environmental_conditions().source_reference()};
    auto entry{std::find_if(data.begin(), data.end(), [](const osi3::ExternalReference &data) {
        return data.has_type() && data.type() == "osiql::World";
    })};
    if ((entry == data.end()) || !entry->has_reference())
    {
        return nullptr;
    }
    const std::uintptr_t address{StringToAddress(entry->reference())};
    return reinterpret_cast<World *>(address); // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast, performance-no-int-to-ptr)
}

std::shared_ptr<World> GetSharedWorld(const osi3::GroundTruth &groundTruth)
{
    if (!groundTruth.has_environmental_conditions())
    {
        return nullptr;
    }
    const auto &data{groundTruth.environmental_conditions().source_reference()};
    auto entry{std::find_if(data.begin(), data.end(), [](const osi3::ExternalReference &data) {
        return data.has_type() && data.type() == "std::shared_ptr<osiql::World>";
    })};
    if ((entry == data.end()) || !entry->has_reference())
    {
        return nullptr;
    }
    const std::uintptr_t address{StringToAddress(entry->reference())};
    return *reinterpret_cast<std::shared_ptr<World> *>(address); // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast, performance-no-int-to-ptr)
}
} // namespace osiql
