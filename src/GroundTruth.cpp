/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/GroundTruth.tpp"

#include "OsiQueryLibrary/Object/Vehicle.tpp"
#include "OsiQueryLibrary/Point/Assignment.tpp"
#include "OsiQueryLibrary/Point/XY.tpp"
#include "OsiQueryLibrary/Street/Lane.tpp"

namespace osiql {
GroundTruth::GroundTruth(const osi3::GroundTruth &world) :
    Wrapper<osi3::GroundTruth>{world},
    version(GetVersion(world)),
    laneBoundaries(FetchAll<Boundary>(world.logical_lane_boundary())),
    laneMarkings(FetchAll<LaneMarking>(world.lane_boundary())),
    referenceLines(FetchAll<ReferenceLine>(world.reference_line())),
    movingObjects(FetchAll<MovingObject>(world.moving_object())),
    roadMarkings(FetchAll<RoadMarking>(world.road_marking())),
    stationaryObjects(FetchAll<StationaryObject>(world.stationary_object())),
    // trafficLights are defined in the body because they are a special case
    trafficSigns(FetchAll<TrafficSign>(world.traffic_sign()))
{
    CreateLanes();
    ConnectLanes();
    CreateRoads();

    LinkBoundariesAndMarkings();

    LinkToLanes(roadMarkings);
    LinkToLanes(trafficSigns);
    CreateAndLinkTrafficLights();
}

GroundTruth::~GroundTruth()
{
    // When a moving object is destroyed, it detaches itself from its touched lanes. Lanes shouldn't
    // be modified directly prior to destruction and object destruction fails if its connected lanes
    // no longer exist, so the best option is to prevent objects from detaching themselves in advance:
    for (auto &[id, object] : movingObjects)
    {
        object->positions.clear();
    }
    for (auto &object : stationaryObjects)
    {
        object.positions.clear();
    }
}

void GroundTruth::CreateLanes()
{
    lanes.reserve(static_cast<size_t>(GetHandle().logical_lane().size()));
    for (const osi3::LogicalLane &lane : GetHandle().logical_lane())
    {
        lanes.emplace(get<Id>(lane), lane);
    }
}

void GroundTruth::ConnectLanes()
{
    for (auto &[id, lane] : lanes)
    {
        LinkSuccessorLanes(lane);
        LinkPredecessorLanes(lane);
    }
}

void GroundTruth::AddConnectedLanesToRoad(Road &road) // NOLINT(modernize-concat-nested-namespaces)
{
    {
        Set<Lane *> lanes;
        for (auto lane{road.lanes.first<Side::Right>()}; lane != road.lanes.end<Side::Right>(); ++lane)
        {
            for (Lane *successor : (*lane)->GetConnectedLanes<Traversal::Forward>())
            {
                lanes.insert(successor);
            }
        }
        std::vector<Lane *> successorLanes{lanes.begin(), lanes.end()};
        road.GetConnectedLanes<Side::Right, Traversal::Forward>() = std::move(successorLanes);
    }
    {
        Set<Lane *> lanes;
        for (auto lane{road.lanes.first<Side::Right>()}; lane != road.lanes.end<Side::Right>(); ++lane)
        {
            for (Lane *predecessor : (*lane)->GetConnectedLanes<Traversal::Backward>())
            {
                lanes.insert(predecessor);
            }
        }
        std::vector<Lane *> predecessorLanes{lanes.begin(), lanes.end()};
        road.GetConnectedLanes<Side::Right, Traversal::Backward>() = std::move(predecessorLanes);
    }
    {
        Set<Lane *> lanes;
        for (auto lane{road.lanes.first<Side::Left>()}; lane != road.lanes.end<Side::Left>(); ++lane)
        {
            for (Lane *successor : (*lane)->GetConnectedLanes<Traversal::Backward>())
            {
                lanes.insert(successor);
            }
        }
        std::vector<Lane *> successorLanes{lanes.begin(), lanes.end()};
        road.GetConnectedLanes<Side::Left, Traversal::Backward>() = std::move(successorLanes);
    }
    {
        Set<Lane *> lanes;
        for (auto lane{road.lanes.first<Side::Left>()}; lane != road.lanes.end<Side::Left>(); ++lane)
        {
            for (Lane *predecessor : (*lane)->GetConnectedLanes<Traversal::Forward>())
            {
                lanes.insert(predecessor);
            }
        }
        std::vector<Lane *> predecessorLanes{lanes.begin(), lanes.end()};
        road.GetConnectedLanes<Side::Left, Traversal::Forward>() = std::move(predecessorLanes);
    }
}

void GroundTruth::CreateRoads()
{
    // Copy all lanes into a separate hash map.
    // Each traversed lane will be removed from the map until it is empty
    HashMap<Lane *> allLanes;
    allLanes.reserve(lanes.size());
    for (auto &[id, lane] : lanes)
    {
        allLanes.emplace_hint(allLanes.end(), id, &lane);
    }
    while (!allLanes.empty())
    {
        // Get any lane and all its right neighboring lanes
        std::vector<Lane *> adjacentLanes{allLanes.begin()->second};
        allLanes.erase(get<Id>(adjacentLanes.back()));
        while (!adjacentLanes.back()->GetHandle().right_adjacent_lane().empty())
        {
            const auto &handle{adjacentLanes.back()->GetHandle()};
            assert(handle.right_adjacent_lane().size() == 1);
            assert(handle.right_adjacent_lane(0).has_other_lane_id());
            Lane *emplacement{adjacentLanes.emplace_back(&Find<Lane>(get<Id>(handle.right_adjacent_lane(0))))};
            allLanes.erase(get<Id>(emplacement));
        }

        // Create a road with all found lanes
        assert(adjacentLanes.back()->GetHandle().has_reference_line_id());
        assert(adjacentLanes.back()->GetHandle().reference_line_id().has_value());
        const ReferenceLine &referenceLine{Find<ReferenceLine>(adjacentLanes.back()->GetHandle().reference_line_id().value())};
        Road &road{Emplace<Road>(roads, get<Id>(adjacentLanes.back()), referenceLine)};
        road.lanes.container = std::vector<Lane *>{adjacentLanes.rbegin(), adjacentLanes.rend()};
        // Add all the left neighboring lanes to the road
        while (!road.lanes.container.back()->GetHandle().left_adjacent_lane().empty())
        {
            Lane *lane{&Find<Lane>(get<Id>(road.lanes.container.back()->GetHandle().left_adjacent_lane(0)))};
            road.lanes.container.push_back(lane);
            allLanes.erase(get<Id>(lane));
        }
        // Assign the road and index of each lane
        for (size_t i{0}; i < road.lanes.size(); ++i)
        {
            Lane *lane{road.lanes.container[i]};
            lane->SetRoad(road);
            lane->index = i;
        }

        LinkBoundaries(road);
        road.UpdateShapeAndBounds();
        // Compute the global shape and bounds of each of the road's lanes
        for (Lane *lane : road.lanes)
        {
            lane->UpdateShapeAndBounds();
        }

        auto it{std::upper_bound(road.lanes.begin(), road.lanes.end(), Side::Left, [](Side side, const Lane *lane) {
            return lane->GetSideOfRoad() == side;
        })};
        road.lanes.center = std::distance(road.lanes.begin(), it);
        road.boundaries.center = road.lanes.center;

        AddConnectedLanesToRoad(road);
    }
}

void GroundTruth::LinkSuccessorLanes(Lane &lane)
{
    auto &container{lane.GetConnectedLanes(Direction::Downstream)};
    const auto &successors{lane.GetHandle().successor_lane()};
    std::transform(successors.begin(), successors.end(), std::back_inserter(container), [&](const osi3::LogicalLane_LaneConnection &connection) {
        return &Find<Lane>(get<Id>(connection));
    });
}

void GroundTruth::LinkPredecessorLanes(Lane &lane)
{
    std::vector<Lane *> &container{lane.GetConnectedLanes(Direction::Upstream)};
    for (const osi3::LogicalLane_LaneConnection &connection : lane.GetHandle().predecessor_lane())
    {
        Lane &prevLane{Find<Lane>(connection.other_lane_id().value())};
        container.push_back(&prevLane);
    }
}

void GroundTruth::LinkBoundariesAndMarkings()
{
    for (Boundary &boundary : laneBoundaries)
    {
        for (const osi3::Identifier &id : boundary.GetHandle().physical_boundary_id())
        {
            const LaneMarking &marking{Find<LaneMarking>(id.value())};
            boundary.markings.push_back(&marking);
        }
    }
}

void GroundTruth::LinkBoundaries(Road &road)
{
    road.boundaries.container.reserve(road.lanes.size() + 1);
    assert(!road.lanes.front()->GetHandle().right_boundary_id().empty());
    road.boundaries.container.emplace_back(BoundaryChain{});
    road.boundaries.front().container.reserve(static_cast<size_t>(road.lanes.front()->GetHandle().right_boundary_id_size()));
    for (const auto &identifier : road.lanes.front()->GetHandle().right_boundary_id())
    {
        road.boundaries.front().container.emplace_back(&Find<Boundary>(identifier.value()));
    }
    for (const Lane *lane : road.lanes)
    {
        const auto &leftBoundaries{lane->GetHandle().left_boundary_id()};
        assert(!leftBoundaries.empty());
        road.boundaries.container.emplace_back(BoundaryChain{});
        road.boundaries.back().container.reserve(static_cast<size_t>(leftBoundaries.size()));
        std::transform(leftBoundaries.begin(), leftBoundaries.end(), std::back_inserter(road.boundaries.back().container), [this](const auto &identifier) {
            return &Find<Boundary>(identifier.value());
        });
    }
}

bool GroundTruth::HasHostVehicle() const
{
    return GetHandle().has_host_vehicle_id() && GetHandle().host_vehicle_id().has_value();
}

const Vehicle &GroundTruth::GetHostVehicle() const
{
    assert(HasHostVehicle());
    return *static_cast<const Vehicle *>(Get<MovingObject>(GetHandle().host_vehicle_id().value())); // NOLINT(cppcoreguidelines-pro-type-static-cast-downcast)
}

void GroundTruth::CreateAndLinkTrafficLights()
{
    // An osi3::TrafficLight is an individual bulb. For convenience, we group bulbs into one traffic light entity.
    // We can't group by xy-position because horizontal or tilted traffic lights exist.
    // We can't group by lane assignment s-coordinate because different lights can be assigned to the same lane's s coordinate.
    // Instead, bulbs are part of the same traffic light if their set of lane assignments is identical, however, there is no guarantee
    // that assignments will be in the same order. Therefore, we need to create a sorted intermediary that uses a custom comparator.

    struct CompareAssignmentSets
    {
        bool operator()(const std::set<Assignment<Lane>> &lhs, const std::set<Assignment<Lane>> &rhs) const
        {
            if (lhs.size() != rhs.size())
            {
                return lhs.size() < rhs.size();
            }
            for (auto lhsIter{lhs.begin()}, rhsIter{rhs.begin()}; lhsIter != lhs.end(); std::advance(lhsIter, 1), std::advance(rhsIter, 1))
            {
                if (extract<Direction::Downstream>(*lhsIter) != extract<Direction::Downstream>(*rhsIter))
                {
                    return extract<Direction::Downstream>(*lhsIter) < extract<Direction::Downstream>(*rhsIter);
                }
                if (lhsIter->GetLane().GetId() != rhsIter->GetLane().GetId())
                {
                    return lhsIter->GetLane().GetId() < rhsIter->GetLane().GetId();
                }
                if (extract<Side::Left>(*lhsIter) != extract<Side::Left>(*rhsIter))
                {
                    return extract<Side::Left>(*lhsIter) < extract<Side::Left>(*rhsIter);
                }
            }
            return false;
        }
    };
    // Keys are the sets of lane assignments of each light bulb
    // Values are the light bulbs that have their key as lane assignments.
    // That way, bulbs are grouped by their lane assignments:

    std::map<std::set<Assignment<Lane>>, std::vector<const osi3::TrafficLight *>, CompareAssignmentSets> protoLights;
    for (const osi3::TrafficLight &bulb : GetHandle().traffic_light())
    {
        std::set<Assignment<Lane>> assignments;
        for (const osi3::LogicalLaneAssignment &assignment : bulb.classification().logical_lane_assignment())
        {
            assignments.emplace(Find<Lane>(assignment.assigned_lane_id().value()), assignment);
        }
        protoLights[std::move(assignments)].emplace_back(&bulb);
    }
    // Now create a new traffic light from each grouping of bulbs
    for (auto &[laneAssignments, trafficBulbs] : protoLights)
    {
        // Define the traffic light
        trafficLights.emplace_back(std::move(trafficBulbs));
        // Link the traffic light to its lanes
        for (const Assignment<Lane> &assignment : laneAssignments)
        {
            trafficLights.back().positions.emplace_back(assignment);
        }
        // Map each bulb to the created traffic light
        for (const osi3::TrafficLight *bulb : trafficLights.back().bulbs)
        {
            lightBulbs.emplace(bulb->id().value(), &trafficLights.back());
        }
    }
    // Link the lanes back to the created traffic lights
    for (TrafficLight &light : trafficLights)
    {
        for (auto &assignment : light.positions)
        {
            assignment.GetLane().GetAll<TrafficLight>().emplace_back(light, assignment.GetHandle());
        }
    }
}

std::ostream &operator<<(std::ostream &os, const GroundTruth &world)
{
    if (world.GetHandle().has_host_vehicle_id() && world.GetHandle().host_vehicle_id().has_value())
    {
        os << "GroundTruth of host vehicle " << world.GetHandle().host_vehicle_id().value() << ":";
    }
    else
    {
        os << "GroundTruth without host vehicle:";
    }
    os << "\nReference Lines: [(x, y), s]\n";
    for (const ReferenceLine &reference : world.GetAll<ReferenceLine>())
    {
        os << reference << '\n';
    }
    os << "\nBoundaries: {PassingRule | (x, y), (s, t)}\n";
    for (const Boundary &boundary : world.GetAll<Boundary>())
    {
        os << boundary << '\n';
    }
    os << "\nLanes (" << world.GetAll<Lane>().size() << "):\n";
    for (const auto &[id, lane] : world.GetAll<Lane>())
    {
        os << lane << '\n';
    }
    os << "\nRoads (" << world.GetAll<Lane>().size() << "):\n";
    for (const auto &[id, road] : world.GetAll<Road>())
    {
        os << road << '\n';
    }
    // os << "\nMoving Objects:\n";
    // for (const auto &[id, object] : world.movingObjects)
    // {
    //     os << *object << '\n';
    // }
    // os << "\nRoad Markings:\n";
    // for (const auto &[id, marking] : world.roadMarkings)
    // {
    //     os << marking << '\n';
    // }
    // os << "\nStationary Objects:\n";
    // for (const auto &[id, object] : world.stationaryObjects)
    // {
    //     os << object << '\n';
    // }
    // os << "\nTraffic Lights:\n";
    // for (const auto &[id, light] : world.trafficLights)
    // {
    //     os << light << '\n';
    // }
    // os << "\nTraffic Signs:\n";
    // for (const auto &[id, sign] : world.trafficSigns)
    // {
    //     os << sign << '\n';
    // }
    return os << '\n';
}
} // namespace osiql
