#!/bin/bash

################################################################################
# Copyright (c) 2021 in-tech GmbH
#               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script builds and executes unit tests
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../../../build" || exit 1

if hash nproc 2>/dev/null; then
  # calculation is kept for reference
  MAKE_JOB_COUNT=$(($(nproc)/2))
else
  # fallback, if nproc doesn't exist
  MAKE_JOB_COUNT=1
fi

export MAKEFLAGS=-j${MAKE_JOB_COUNT}

ctest -j2 --output-on-failure

