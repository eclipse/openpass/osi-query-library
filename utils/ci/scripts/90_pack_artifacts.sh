#!/bin/bash

################################################################################
# Copyright (c) 2021 in-tech GmbH
#               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script packs the artifacts
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../../../dist" || exit 1

ARTIFACT_NAME=OsiQL

mkdir -p ../artifacts || exit 1

if [[ "${OSTYPE}" = "msys" ]]; then
  $MYDIR/util_zip.sh ../artifacts/${ARTIFACT_NAME}.zip osiql
else
  $MYDIR/util_tar.sh ../artifacts/${ARTIFACT_NAME}.tar.gz osiql
fi

