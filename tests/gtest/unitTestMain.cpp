/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gtest/gtest.h>

#include <string>

/*!
 * \brief Sets a default XML test result filename for gtest
 *
 * If the command line parameters to the test executable contain
 * the element `--default-xml`, the gtest `output` parameter is set
 * to write an XML file. The filename will be based on the test
 * executable's name.
 *
 * The argument `--default-xml` is removed from the argument list
 * before returning.
 *
 * \param[inout] argc   Command line arguments count
 * \param[inout] argv   Command line arguments values
 */
void SetDefaultXmlOutput(int *argc, char **argv)
{
    for (int i = 1; i < *argc; ++i)
    {
        // NOLINTBEGIN(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        if (std::string(argv[i]) == "--default-xml")
        {
#ifdef WIN32
            std::string basename(argv[0]);
            basename = basename.substr(0, basename.length() - 4); // strip extension
#else
            const std::string basename(argv[0]);
#endif
            ::testing::GTEST_FLAG(output) = std::string("xml:") + basename + ".xml";

            // remove argument from list
            for (int j = i; j != *argc; ++j)
            {
                argv[j] = argv[j + 1]; // NOLINTEND(cppcoreguidelines-pro-bounds-pointer-arithmetic)
            }

            // decrement argument count
            *argc -= 1;

            break;
        }
    }
}

int main(int argc, char **argv)
{
    SetDefaultXmlOutput(&argc, argv);
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
