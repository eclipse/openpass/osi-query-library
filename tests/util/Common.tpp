/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "util/Common.h"

namespace osiql {
namespace test::detail {
template <Traversal T, typename It>
osi3::LogicalLane_LaneConnection *AddConnectedLane(It lane)
{
    if constexpr (is_reverse_iterator<It> == (T == Traversal::Forward))
    {
        return (*lane)->add_predecessor_lane();
    }
    else
    {
        return (*lane)->add_successor_lane();
    }
}
} // namespace test::detail

template <typename It1, typename It2>
void test::ConnectRoads(It1 lanes1, It1 lanes1end, It2 lanes2, It2 lanes2end) // NOLINT(bugprone-easily-swappable-parameters, misc-unused-parameters)
{
    assert(std::distance(lanes1, lanes1end) == std::distance(lanes2, lanes2end));
    for (; lanes1 != lanes1end; ++lanes1, ++lanes2)
    {
        osi3::LogicalLane_LaneConnection *connection{detail::AddConnectedLane<Traversal::Forward>(lanes1)};
        connection->set_at_begin_of_other_lane(!is_reverse_iterator<It2>);
        connection->mutable_other_lane_id()->set_value((*lanes2)->id().value());
        connection = detail::AddConnectedLane<Traversal::Backward>(lanes2);
        connection->set_at_begin_of_other_lane(is_reverse_iterator<It1>);
        connection->mutable_other_lane_id()->set_value((*lanes1)->id().value());
    }
}
} // namespace osiql
