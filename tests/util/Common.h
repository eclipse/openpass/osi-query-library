/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "GroundTruth.h"
#include "OsiQueryLibrary/osiql.h"

using ::testing::_;
using ::testing::DoubleEq;
using ::testing::DoubleNear;
using ::testing::ElementsAre;
using ::testing::Eq;
using ::testing::IsEmpty;
using ::testing::Lt;
using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::SizeIs;
using ::testing::UnorderedElementsAre;

using osiql::XY;
using osiql::XYZ;
using osiql::operator<<;

namespace osiql {
Shape CreatePolygon(const Bounds &);
namespace test {
template <typename It1, typename It2>
void ConnectRoads(It1 lanes1begin, It1 lanes1end, It2 lanes2begin, It2 lanes2end);
} // namespace test
} // namespace osiql
