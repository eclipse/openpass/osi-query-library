/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/osiql.h"

namespace osiql {
namespace test {
struct GroundTruth
{
    GroundTruth();

    template <typename... Point>
    osi3::ReferenceLine &AddReferenceLine(Point &&...);

    template <typename... Point>
    osi3::LogicalLaneBoundary &AddBoundary(Point &&...);

    osi3::LogicalLane &AddLane(osi3::ReferenceLine &,                   //
                               osi3::LogicalLaneBoundary &leftBoundary, //
                               osi3::LogicalLaneBoundary &rightBoundary);

    std::vector<osi3::LogicalLane *> AddRoad(osi3::ReferenceLine &, //
                                             std::vector<osi3::LogicalLaneBoundary *> boundaries);

    template <typename Object, typename Position>
    typename Object::Handle &Add(Position &&, double length, double width);

    template <typename Object, typename Position>
    typename Object::Handle &Add(Position &&, double length, double width, Id laneId);

    osi3::GroundTruth handle;

private:
    Id id{0};

    template <typename Object, typename Position>
    void DerivePrimaryLaneAssignment(Object &, Position &&, Id laneId) const;

    template <typename Object>
    typename Object::Handle *Create();
};
} // namespace test

namespace detail {
template <typename Input>
void SetType(Input &, ReferenceLine::Type);

template <typename Input>
void SetTAxisYaw(Input &, double yaw);
} // namespace detail
} // namespace osiql

namespace osiql {
namespace test {
template <typename... Point>
osi3::ReferenceLine &GroundTruth::AddReferenceLine(Point &&...point)
{
    osi3::ReferenceLine *referenceLine{handle.add_reference_line()};
    referenceLine->mutable_id()->set_value(++id);
    if constexpr ((std::is_same_v<Point, Pose<XY>> && ...))
    {
        detail::SetType(*referenceLine, ReferenceLine::Type::PolylineWithTAxis);
        const auto addPoint = [referenceLine](const auto &pose) {
            osi3::ReferenceLine_ReferenceLinePoint *point{referenceLine->add_poly_line()};
            point->mutable_world_position()->set_x(pose.x);
            point->mutable_world_position()->set_y(pose.y);
            detail::SetTAxisYaw(*point, pose.angle);
        };
        (addPoint(point), ...);
    }
    else
    {
        detail::SetType(*referenceLine, ReferenceLine::Type::Polyline);
        const auto addPoint = [referenceLine](const auto &xy) {
            osi3::ReferenceLine_ReferenceLinePoint *point{referenceLine->add_poly_line()};
            point->mutable_world_position()->set_x(X{}(xy));
            point->mutable_world_position()->set_y(Y{}(xy));
        };
        (addPoint(point), ...);
    }
    referenceLine->mutable_poly_line(0)->set_s_position(0.0);
    for (auto next{std::next(referenceLine->mutable_poly_line()->begin())}; next != referenceLine->mutable_poly_line()->end(); ++next)
    {
        next->set_s_position(std::prev(next)->s_position() + (*std::prev(next) - *next).Length());
    }
    return *referenceLine;
}

template <typename... Point>
osi3::LogicalLaneBoundary &GroundTruth::AddBoundary(Point &&...point)
{
    osi3::LogicalLaneBoundary *boundary{handle.add_logical_lane_boundary()};
    boundary->mutable_id()->set_value(++id);
    const auto addPoint = [boundary](const auto &xy) {
        osi3::LogicalLaneBoundary_LogicalBoundaryPoint *point{boundary->add_boundary_line()};
        point->mutable_position()->set_x(X{}(xy));
        point->mutable_position()->set_y(Y{}(xy));
    };
    (addPoint(point), ...);
    return *boundary;
}

template <typename Object, typename Position>
void PlaceObject(Object &object, Position &&position, double length, double width)
{
    if constexpr (std::is_same_v<Object, osi3::TrafficSign>)
    {
        PlaceObject(*object.mutable_main_sign(), std::forward<Position>(position), length, width);
    }
    else
    {
        object.mutable_base()->mutable_position()->set_x(X{}(position));
        object.mutable_base()->mutable_position()->set_y(Y{}(position));
        object.mutable_base()->mutable_position()->set_z(Z{}(position));
        if constexpr (std::is_same_v<Position, Pose<XY>>)
        {
            object.mutable_base()->mutable_orientation()->set_yaw(position.angle);
        }
        object.mutable_base()->mutable_dimension()->set_length(length);
        object.mutable_base()->mutable_dimension()->set_width(width);
    }
}

template <typename Object>
typename Object::Handle *GroundTruth::Create()
{
    if constexpr (std::is_same_v<Object, MovingObject>)
    {
        return handle.add_moving_object();
    }
    else if constexpr (std::is_same_v<Object, StationaryObject>)
    {
        return handle.add_stationary_object();
    }
    else if constexpr (std::is_same_v<Object, RoadMarking>)
    {
        return handle.add_road_marking();
    }
    else if constexpr (std::is_same_v<Object, TrafficLight>)
    {
        return handle.add_traffic_light();
    }
    else if constexpr (std::is_same_v<Object, TrafficSign>)
    {
        return handle.add_traffic_sign();
    }
}

template <typename Object, typename Position>
typename Object::Handle &GroundTruth::Add(Position &&position, double length, double width)
{
    typename Object::Handle *object{Create<Object>()};
    object->mutable_id()->set_value(++id);
    PlaceObject(*object, std::forward<Position>(position), length, width);
    return *object;
}

template <typename Object, typename Position>
void GroundTruth::DerivePrimaryLaneAssignment(Object &object, Position &&position, Id laneId) const
{
    if constexpr (std::is_same_v<Object, osi3::TrafficSign>)
    {
        DerivePrimaryLaneAssignment(*object.mutable_main_sign(), std::forward<Position>(position), laneId);
    }
    else
    {
        osi3::LogicalLaneAssignment *assignment{object.mutable_classification()->add_logical_lane_assignment()};
        assignment->mutable_assigned_lane_id()->set_value(laneId);
        World world{handle};
        ST localization{world.GetLane(laneId)->GetRoad().GetReferenceLine().Localize(position)};
        assignment->set_s_position(localization.s);
        assignment->set_t_position(localization.t);
    }
}

template <typename Object, typename Position>
typename Object::Handle &GroundTruth::Add(Position &&position, double length, double width, Id laneId)
{
    typename Object::Handle *object{Create<Object>()};
    object->mutable_id()->set_value(++id);
    PlaceObject(*object, std::forward<Position>(position), length, width);
    DerivePrimaryLaneAssignment(*object, std::forward<Position>(position), laneId);
    return *object;
}
} // namespace test

namespace detail {
template <typename Input>
void SetType(Input &referenceLine, ReferenceLine::Type type)
{
    if constexpr (GetOSIVersion() >= Version{3, 6, 0})
    {
        referenceLine.set_type(static_cast<typename Input::Type>(type));
    }
}

template <typename Input>
void SetTAxisYaw(Input &point, double yaw)
{
    if constexpr (GetOSIVersion() >= Version{3, 6, 0})
    {
        point.set_t_axis_yaw(yaw);
    }
}
} // namespace detail
} // namespace osiql
