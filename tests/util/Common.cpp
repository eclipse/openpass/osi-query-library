/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "util/Common.tpp"

namespace osiql {
Shape CreatePolygon(const Bounds &box)
{
    return Shape{
        box.min_corner(),
        {box.max_corner().x, box.min_corner().y},
        box.max_corner(),
        {box.min_corner().x, box.max_corner().y} //
    };
}
} // namespace osiql
