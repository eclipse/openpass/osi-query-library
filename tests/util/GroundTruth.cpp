/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "GroundTruth.h"

namespace osiql::test {
GroundTruth::GroundTruth()
{
    const auto [major, minor, patch]{GetOSIVersion()};
    handle.mutable_version()->set_version_major(major);
    handle.mutable_version()->set_version_minor(minor);
    handle.mutable_version()->set_version_patch(patch);
}

osi3::LogicalLane &GroundTruth::AddLane(osi3::ReferenceLine &referenceLine,      //
                                        osi3::LogicalLaneBoundary &leftBoundary, //
                                        osi3::LogicalLaneBoundary &rightBoundary)
{
    {
        const osiql::ReferenceLine localizer{referenceLine};
        for (osi3::LogicalLaneBoundary_LogicalBoundaryPoint &point : *leftBoundary.mutable_boundary_line())
        {
            const auto coordinates{localizer.Localize(point.position())};
            point.set_s_position(coordinates.s);
            point.set_t_position(coordinates.t);
        }
        for (osi3::LogicalLaneBoundary_LogicalBoundaryPoint &point : *rightBoundary.mutable_boundary_line())
        {
            const auto coordinates{localizer.Localize(point.position())};
            point.set_s_position(coordinates.s);
            point.set_t_position(coordinates.t);
        }
    }
    osi3::LogicalLane *lane{handle.add_logical_lane()};
    lane->mutable_id()->set_value(++id);
    lane->mutable_reference_line_id()->set_value(referenceLine.id().value());
    lane->set_start_s(referenceLine.poly_line().begin()->s_position());
    lane->set_end_s(referenceLine.poly_line().rbegin()->s_position());
    lane->add_left_boundary_id()->set_value(leftBoundary.id().value());
    lane->add_right_boundary_id()->set_value(rightBoundary.id().value());
    lane->set_move_direction(osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S);
    return *lane;
}

std::vector<osi3::LogicalLane *> GroundTruth::AddRoad(osi3::ReferenceLine &referenceLine, //
                                                      std::vector<osi3::LogicalLaneBoundary *> boundaries)
{
    std::vector<osi3::LogicalLane *> lanes;
    for (auto rightBoundary{std::next(boundaries.begin())}; rightBoundary != boundaries.end(); ++rightBoundary)
    {
        auto leftBoundary{std::prev(rightBoundary)};
        lanes.push_back(&AddLane(referenceLine, **leftBoundary, **rightBoundary));
    }
    for (auto rightLane{std::next(lanes.begin())}; rightLane != lanes.end(); ++rightLane)
    {
        auto leftLane{std::prev(rightLane)};
        {
            auto *relation{(*leftLane)->add_right_adjacent_lane()};
            relation->mutable_other_lane_id()->set_value((*rightLane)->id().value());
            relation->set_start_s((*leftLane)->start_s());
            relation->set_start_s_other((*leftLane)->start_s());
            relation->set_end_s((*leftLane)->end_s());
            relation->set_end_s_other((*leftLane)->end_s());
        }
        {
            auto *relation{(*rightLane)->add_left_adjacent_lane()};
            relation->mutable_other_lane_id()->set_value((*leftLane)->id().value());
            relation->set_start_s((*rightLane)->start_s());
            relation->set_start_s_other((*rightLane)->start_s());
            relation->set_end_s((*rightLane)->end_s());
            relation->set_end_s_other((*rightLane)->end_s());
        }
    }
    const auto numLanesWithinEpsilon{static_cast<size_t>(std::distance(boundaries.begin(), std::find_if( // clang-format off
        boundaries.begin(),
        boundaries.end(),
        [](const osi3::LogicalLaneBoundary *boundary) {
            const auto& points{boundary->boundary_line()};
            return std::all_of(points.begin(), points.end(), [](const osi3::LogicalLaneBoundary_LogicalBoundaryPoint& point) {
                return point.t_position() <= EPSILON;
            });
        }
    )))}; // clang-format on
    for (size_t i{0}; i < numLanesWithinEpsilon; ++i)
    {
        lanes[i]->set_move_direction(osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_DECREASING_S);
    }
    return lanes;
}
} // namespace osiql::test
