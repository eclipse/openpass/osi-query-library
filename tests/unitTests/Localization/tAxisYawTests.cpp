/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "util/Common.tpp"

using namespace osiql;

class TAxisYawTests : public ::testing::Test
{
public:
    TAxisYawTests()
    {
        std::vector<osi3::LogicalLane *> road1{groundTruth.AddRoad(
            groundTruth.AddReferenceLine(Pose<XY>{{0, -4}, halfPi}, Pose<XY>{{4, 0}, pi}),
            {
                &groundTruth.AddBoundary(XY{0, -2}, XY{2, 0}),
                &groundTruth.AddBoundary(XY{0, -4}, XY{4, 0}),
                &groundTruth.AddBoundary(XY{0, -6}, XY{6, 0}),
            }
        )};

        std::vector<osi3::LogicalLane *> road2{groundTruth.AddRoad(
            groundTruth.AddReferenceLine(Pose<XY>{{4, 0}, pi}, Pose<XY>{{2, 2}, -threeQuarterPi}),
            {
                &groundTruth.AddBoundary(XY{2, 0}, XY{1, 1}),
                &groundTruth.AddBoundary(XY{4, 0}, XY{2, 2}),
                &groundTruth.AddBoundary(XY{6, 0}, XY{3, 3}),
            }
        )};

        std::vector<osi3::LogicalLane *> road3{groundTruth.AddRoad(
            groundTruth.AddReferenceLine(Pose<XY>{{2, 2}, -threeQuarterPi}, Pose<XY>{{0, 4}, -halfPi}, Pose<XY>{{-4, 0}, 0}),
            {
                &groundTruth.AddBoundary(XY{1, 1}, XY{0, 2}, XY{-2, 0}),
                &groundTruth.AddBoundary(XY{2, 2}, XY{0, 4}, XY{-4, 0}),
                &groundTruth.AddBoundary(XY{3, 3}, XY{0, 6}, XY{-6, 0}),
            }
        )};

        std::vector<osi3::LogicalLane *> road4{groundTruth.AddRoad(
            groundTruth.AddReferenceLine(Pose<XY>{{-2, -1}, pi}, Pose<XY>{{-1, -3}, pi}),
            {
                &groundTruth.AddBoundary(XY{-2, -1}, XY{-1, -3}),
                &groundTruth.AddBoundary(XY{-3, -1}, XY{-3, -3}),
            }
        )};

        auto &referenceLine{groundTruth.AddReferenceLine(XY{11, -3}, XY{7, -3}, XY{7, 3}, XY{11, 3})};
        detail::SetType(referenceLine, ReferenceLine::Type::PolylineWithTAxis);
        std::vector<osi3::LogicalLane *> road5{groundTruth.AddRoad(
            referenceLine,
            {
                &groundTruth.AddBoundary(XY{11, -3}, XY{7, -3}, XY{7, 3}, XY{11, 3}),
                &groundTruth.AddBoundary(XY{11, -1}, XY{9, -1}, XY{9, 1}, XY{11, 1}),
            }
        )};

        world = std::make_unique<World>(groundTruth.handle);
    }

    std::unique_ptr<World> world;
    test::GroundTruth groundTruth;
};

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(TAxisYawTests, Localizations)
{
    using namespace osiql;
    { // 1
        const XY xy{0, -4};
        auto points1{world->Localize(xy)};
        ASSERT_THAT(points1, SizeIs(2));
        const ST st1{0, 0};
        EXPECT_EQ(points1[0], st1);
        EXPECT_EQ(points1[0].GetXY(), xy);
    }
    { // 2
        const XY xy{1, -1};
        auto points2{world->Localize(xy)};
        const Road &road{*world->Get<Road>(6)};
        ASSERT_THAT(points2, SizeIs(1));
        ST st2{2 * sqrt(2), sqrt(2)};
        EXPECT_DOUBLE_EQ(points2[0].s, st2.s);
        EXPECT_DOUBLE_EQ(points2[0].t, st2.t);
        EXPECT_EQ(points2[0].GetXY(), xy);
    }
    { // 3
        const XY xy{4, 0};
        auto points3{world->Localize(xy)};
        std::sort(points3.begin(), points3.end(), Less<Lane>{});
        ASSERT_THAT(points3, SizeIs(4));
        ST st3{4 * sqrt(2), 0};
        EXPECT_EQ(points3[0], st3);
        EXPECT_EQ(points3[0].GetXY(), xy);
    }
    { // 4
        const XY xy{4, 4};
        auto points4{world->Localize(xy)};
        EXPECT_THAT(points4, IsEmpty());
        Point<const Lane> st4{ST{2 * sqrt(2), -2 * sqrt(2)}, *world->GetLane(12)};
        EXPECT_EQ(st4.GetXY(), xy);
    }
    { // 5
        const XY xy{0, 5};
        auto points5{world->Localize(xy)};
        ASSERT_THAT(points5, SizeIs(1));
        ST st5{2 * sqrt(2), -1};
        EXPECT_EQ(points5[0], st5);
        EXPECT_EQ(points5[0].GetXY(), xy);
    }
    // { // 6
    //     const XY xy{-5, -4};
    //     auto points{world->Localize(xy)};
    //     ASSERT_THAT(points, SizeIs(1));
    //     std::cout << "Mystery xy: " << points[0] << '\n';
    // }
    { // 7
        const XY xy{-2.5, -2};
        auto points7{world->Localize(xy)};
        ASSERT_THAT(points7, SizeIs(1));
        ST st7{0.5 * sqrt(5), -1};
        EXPECT_EQ(points7[0], st7);
        EXPECT_EQ(points7[0].GetXY(), xy);
    }
    { // 8
        const XY xy{-6, 0};
        auto points8{world->Localize(xy)};
        ASSERT_THAT(points8, SizeIs(1));
        ST st8{6 * sqrt(2), -2};
        EXPECT_EQ(points8[0], st8);
        EXPECT_EQ(points8[0].GetXY(), xy);
    }
    { // 9
        const XY xy{9.5, -2};
        auto points9{world->Localize(xy)};
        ASSERT_THAT(points9, SizeIs(1));
        ST st9{2, -sqrt(5) * 0.5};
        EXPECT_DOUBLE_EQ(points9[0].s, st9.s);
        EXPECT_DOUBLE_EQ(points9[0].t, st9.t);
        EXPECT_EQ(points9[0].GetXY(), xy);
    }
    { // 10
        const XY xy{8, -2};
        auto points10{world->Localize(xy)};
        ASSERT_THAT(points10, SizeIs(1));
        ST st10{4, -sqrt(2)};
        EXPECT_DOUBLE_EQ(points10[0].s, st10.s);
        EXPECT_NEAR(points10[0].t, st10.t, osiql::EPSILON);
        EXPECT_EQ(points10[0].GetXY(), xy);
    }
    { // 11
        const XY xy{8, 0};
        auto points11{world->Localize(xy)};
        ASSERT_THAT(points11, SizeIs(1));
        ST st11{7, -1};
        EXPECT_EQ(points11[0], st11);
        EXPECT_EQ(points11[0].GetXY(), xy);
    }
    { // 12
        const XY xy{9, -1};
        auto points12{world->Localize(xy)};
        ASSERT_THAT(points12, SizeIs(1));
        ST st12{4, -2 * sqrt(2)};
        EXPECT_NEAR(points12[0].s, st12.s, osiql::EPSILON);
        EXPECT_NEAR(points12[0].t, st12.t, osiql::EPSILON);
        EXPECT_EQ(points12[0].GetXY(), xy);
    }
    { // 13
        const XY xy{-1, -5};
        const Road &road{get<Road>(*world->Get<Road>(6))};
        const ST st13{road.GetReferenceLine().Localize(xy)};
        EXPECT_NEAR(st13.s, -sqrt(2), osiql::EPSILON);
        EXPECT_NEAR(st13.t, 0, osiql::EPSILON);
        EXPECT_EQ(road.GetReferenceLine().GetXY(st13), xy);
    }
    { // 14
        const XY xy{-7, -1};
        const Road &road{get<Road>(*world->GetAll<Road>().find(18))};
        const ST st14{road.GetReferenceLine().Localize(xy)};
        const XY intersection{Line{xy, {0, 0}}.GetIntersection({{0, 4}, {-4, 0}}).value()};
        const double expectedS{6 * sqrt(2) + (intersection - XY{-4, 0}).Length()};
        const double expectedT{-(xy - intersection).Length()};
        EXPECT_NEAR(st14.s, expectedS, osiql::EPSILON);
        EXPECT_NEAR(st14.t, expectedT, osiql::EPSILON);
        EXPECT_EQ(road.GetReferenceLine().GetXY(st14), xy);
    }
}

TEST(TAxisYaw, GivenPoseWithCoordinatesAndYaw_WhenProjectedOntoStraightVerticalReferenceLine_ThenReturnsCorrectCoordinates)
{
    test::GroundTruth groundTruth;
    osiql::ReferenceLine referenceLine{
        groundTruth.AddReferenceLine(
            Pose<XY>{{-1, -2}, constants::pi},
            Pose<XY>{{-1, +2}, constants::pi}
        )};

    auto point = referenceLine.Localize(Pose<XY>{XY{1, -1}, constants::halfPi});
    EXPECT_NEAR(point.s, 1.0, osiql::EPSILON);
    EXPECT_NEAR(point.t, -2.0, osiql::EPSILON);
    EXPECT_NEAR(point.angle, 0.0, osiql::EPSILON);
}
