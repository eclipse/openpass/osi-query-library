/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "util/Common.tpp"

using namespace osiql;

struct CoordinateTest : ::testing::Test
{
    CoordinateTest()
    {
        groundTruth.AddRoad( // ROAD 0
            groundTruth.AddReferenceLine(Pose<XY>{{3, 3}, -threeQuarterPi}, Pose<XY>{{-3, 3}, -quarterPi}),
            {
                &groundTruth.AddBoundary(XY{1, 1}, XY{-1, 1}),
                &groundTruth.AddBoundary(XY{2, 2}, XY{-2, 2}),
                &groundTruth.AddBoundary(XY{3, 3}, XY{-3, 3}),
                &groundTruth.AddBoundary(XY{4, 4}, XY{-4, 4}),
                &groundTruth.AddBoundary(XY{5, 5}, XY{1, 5}, XY{1, 6}, XY{-1, 6}, XY{-1, 5}, XY{-5, 5}),
            }
        );
        groundTruth.AddRoad( // ROAD 1
            groundTruth.AddReferenceLine(Pose<XY>{{-5, 2}, halfPi}, Pose<XY>{{-1, -2}, halfPi}),
            {
                &groundTruth.AddBoundary(XY{-5, 2}, XY{-1, -2}),
                &groundTruth.AddBoundary(XY{-5, 0}, XY{-1, -4}),
            }
        );
        groundTruth.AddRoad( // ROAD 2
            groundTruth.AddReferenceLine(XY{-5, -2}, XY{-2, 1}),
            {
                &groundTruth.AddBoundary(XY{-5, -2}, XY{-2, 1}),
                &groundTruth.AddBoundary(XY{-5, -3}, XY{-1, 0}),
            }
        );
        groundTruth.AddRoad( // ROAD 3
            groundTruth.AddReferenceLine(Pose<XY>{{2, -4}, pi}, Pose<XY>{{0, -2}, pi}, Pose<XY>{{0, -1}, pi}, Pose<XY>{{2, 1}, pi}),
            {
                &groundTruth.AddBoundary(XY{2, -4}, XY{0, -2}, XY{0, -1}, XY{2, 1}),
                &groundTruth.AddBoundary(XY{2, -4}, XY{2, 1}),
            }
        );
        groundTruth.AddRoad( // ROAD 4
            groundTruth.AddReferenceLine(Pose<XY>{{3, -4}, pi}, Pose<XY>{{5, -2}, pi}),
            {
                &groundTruth.AddBoundary(XY{3, -4}, XY{5, -2}),
                &groundTruth.AddBoundary(XY{5, -4}, XY{5, -2}),
            }
        );
        groundTruth.AddRoad( // ROAD 5
            groundTruth.AddReferenceLine(Pose<XY>{{5, -1}, pi}, Pose<XY>{{3, 1}, pi}),
            {
                &groundTruth.AddBoundary(XY{5, -1}, XY{3, 1}),
                &groundTruth.AddBoundary(XY{5, -1}, XY{5, 1}),
            }
        );
        world = std::make_unique<osiql::World>(groundTruth.handle);
    }

    std::unique_ptr<osiql::World> world;
    test::GroundTruth groundTruth;
};

class CoordinateTestP : public CoordinateTest, public ::testing::WithParamInterface<std::tuple<std::string, Bounds, size_t>>
{
};

TEST_P(CoordinateTestP, FindOverlappingLane)
{
    auto [test_case, bounds, expected_size] = GetParam();
    Shape polygon{CreatePolygon(bounds)};
    auto result{world->FindOverlapping<Lane>(polygon, bounds)};
    EXPECT_EQ(result.size(), expected_size) << test_case;
}

INSTANTIATE_TEST_SUITE_P(
    CoordinateTests,
    CoordinateTestP,
    ::testing::Values(
        // Test cases for PolygonOutside_Returns_EmptyResult
        //  [test_case, bounds, expected_size]
        std::make_tuple("Touching no point on a lane", Bounds{{4.0, 2.0}, {5.0, 3.0}}, 0),
        std::make_tuple("Polygon corner touching lane corner", Bounds{{5.0, 5.0}, {6.0, 6.0}}, 0),
        std::make_tuple("Polygon corner touching lane edge", Bounds{{-3.5, 0.5}, {-2.5, 1.5}}, 0),
        std::make_tuple("Polygon edge touching lane corner", Bounds{{4.5, -2.0}, {5.5, -1.0}}, 0),
        std::make_tuple("Polygon corner touching multiple lanes", Bounds{{-4.0, 2.0}, {-3.0, 3.0}}, 0),
        std::make_tuple("Polygon edges touching multiple lanes", Bounds{{2.0, -3.0}, {4.0, -1.0}}, 0),

        // Test cases for PolygonFullyInside_Returns_NonEmptyResult
        //  [test_case, bounds, expected_size]
        std::make_tuple("Touching no border (0 intersection points)", Bounds{{-0.5, 4.5}, {0.5, 5.5}}, 1),
        std::make_tuple("Touching border corner with corner (1 intersection point)", Bounds{{0.5, 4.5}, {1.0, 5.0}}, 1),
        std::make_tuple("Touching border edge with corner (1 intersection point)", Bounds{{1.0, -0.5}, {1.5, 0.0}}, 1),
        std::make_tuple("Touching two border edges with corners (2 intersection points)", Bounds{{-3.5, -1.5}, {-2.5, -0.5}}, 2),
        std::make_tuple("Touching border edge with edge (2 intersection points) (case 1)", Bounds{{-1.0, 4.0}, {1.0, 6.0}}, 1),
        std::make_tuple("Touching border edge with edge (2 intersection points) (case 2)", Bounds{{0.0, 3.0}, {1.0, 4.0}}, 1),
        std::make_tuple("Touching border edge with edge (2 intersection points) (case 3)", Bounds{{1.0, 4.0}, {2.0, 5.0}}, 1),

        // Test cases for PolygonPartiallyInside_Returns_NonEmptyResult
        //  [test_case, bounds, expected_size]
        std::make_tuple("Intersecting with start of lane", Bounds{{3.0, 3.0}, {4.0, 4.0}}, 1),
        std::make_tuple("Intersecting with end of lane", Bounds{{-4.0, 3.0}, {-3.0, 4.0}}, 1),
        std::make_tuple("Intersecting with left side of lane (case 1)", Bounds{{-1.0, -2.0}, {0.0, -1.0}}, 1),
        std::make_tuple("Intersecting with left side of lane (case 2)", Bounds{{0.0, -2.0}, {1.0, -1.0}}, 1),
        std::make_tuple("Intersecting with left side of lane (case 3)", Bounds{{0.0, -1.0}, {1.0, 0.0}}, 1),
        std::make_tuple("Intersecting with left side of lane (case 4)", Bounds{{0.0, -3.0}, {1.0, -2.0}}, 1),
        std::make_tuple("Intersecting with right side of lane (case 1)", Bounds{{-1.5, 5.0}, {-0.5, 6.0}}, 1),
        std::make_tuple("Intersecting with right side of lane (case 2)", Bounds{{-1.5, 4.5}, {-0.5, 6.5}}, 1),
        std::make_tuple("Intersecting with right side of lane (case 3)", Bounds{{-1.0, 5.0}, {0.0, 6.0}}, 1),
        std::make_tuple("Intersecting with right side of lane (case 4)", Bounds{{0.0, 5.0}, {1.0, 6.0}}, 1),
        std::make_tuple("Intersecting with right side of lane (case 5)", Bounds{{0.5, 5.0}, {1.5, 6.0}}, 1),
        std::make_tuple("Intersecting with right side of lane (case 6)", Bounds{{0.5, 4.5}, {1.5, 6.5}}, 1),
        std::make_tuple("Fully covering entire lane", Bounds{{3.0, -4.0}, {5.0, -2.0}}, 1)
    )
);

TEST_F(CoordinateTest, FindOverlappingLane_PolygonInsideEdgeTouchesCorner_ReturnsEmptyResult)
{
    // Touching border corner with edge (1 intersection point)
    Shape polygon{{0.5, 4.5}, {1.5, 4.5}, {0.5, 5.5}};
    Bounds bounds;
    boost::geometry::envelope(polygon, bounds);
    auto result{world->FindOverlapping<Lane>(polygon, bounds)};
    EXPECT_EQ(result.size(), 1);
}
