/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <array>
#include <numeric>

#include "OsiQueryLibrary/osiql.h"

// Lane tests are intended to verify the behavior of lanes with multiple boundaries and successive
// lanes with the same reference line. The groundTruth is laid out to test these scenarios:
// - Retrieve the s & t coordinates & angle/curvature on a curved lane with multiple boundaries
// - Retrieve the traversed lanes of a route, once for an ambiguous successor lane and once for an ambiguous adjacent lane

using XY = osiql::XY;

// Returns the point along a given Bézier curve interpolated for a given t in [0.0, 1.0].
template <size_t N>
XY GetPoint(double t, const std::array<XY, N> &curve)
{
    if constexpr (N == 4)
    {
        const double tInv{1 - t};
        return tInv * tInv * tInv * curve[0] + 3.0 * tInv * tInv * t * curve[1] + 3.0 * tInv * t * t * curve[2] + t * t * t * curve[3];
    }
    else if constexpr (N == 3)
    {
        const double tInv{1 - t};
        return tInv * tInv * curve[0] + 2.0 * tInv * t * curve[1] + t * t * curve[2];
    }
    else if constexpr (N == 2)
    {
        return (1.0 - t) * curve[0] + t * curve[1];
    }
    else
    {
        std::cerr << "Interpolating the point of a bezier curve of degree n=" << N << " is not supported. Returning coordinate origin.";
        return {};
    }
}

class LocalizationTest : public ::testing::Test
{
public:
    osi3::GroundTruth groundTruth{};
    std::unique_ptr<osiql::World> world{nullptr};

    int laneId{100};
    int boundaryId{200};
    int referenceLineId{300};

    // void CreateStartingRoad()
    // {
    //     const std::array<const std::array<XY, 3>, 2> points{
    //         std::array<XY, 3>{XY{0.0, -30.0}, {-100.0, -30.0}, {-100.0, 0.0}},
    //         std::array<XY, 3>{XY{0.0, -20.0}, {-090.0, -20.0}, {-090.0, 0.0}} //
    //     };
    //     // the start and end of boundaries on the curved entry lane
    //     const std::array<const std::vector<double>, 2> breaks{
    //         std::vector<double>{0.0, 0.5, 1.0},
    //         std::vector<double>{0.0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1.0} //
    //     };
    // }

    static void ConnectSuccessors(size_t i, std::array<std::vector<osi3::LogicalLane *>, 4> &lanes)
    { // NOLINTBEGIN(cppcoreguidelines-pro-bounds-constant-array-index)
        for (size_t j{1}; j < lanes[i].size(); ++j)
        {
            osi3::LogicalLane_LaneConnection *connection{lanes[i][j - 1]->add_successor_lane()};
            connection->set_at_begin_of_other_lane(true);
            connection->mutable_other_lane_id()->set_value(lanes[i][j]->id().value());
            connection = lanes[i][j]->add_predecessor_lane();
            connection->set_at_begin_of_other_lane(false);
            connection->mutable_other_lane_id()->set_value(lanes[i][j - 1]->id().value());
        } // NOLINTEND(cppcoreguidelines-pro-bounds-constant-array-index)
    }

    void CreateLanes(osi3::ReferenceLine *referenceLine, std::vector<std::vector<osi3::LogicalLaneBoundary *>> &boundaries, const std::array<const std::vector<osiql::Lane::Type>, 4> &laneTypes, const std::array<const std::vector<double>, 5> &breaks, std::array<std::vector<osi3::LogicalLane *>, 4> &lanes)
    {
        for (size_t i{0}; i < lanes.size(); ++i) // NOLINTBEGIN(cppcoreguidelines-pro-bounds-constant-array-index)
        {
            for (size_t j{0}; j < breaks[i].size() - 1; ++j)
            {
                osi3::LogicalLane *lane{groundTruth.add_logical_lane()};
                lanes[i].push_back(lane);
                lane->mutable_id()->set_value(laneId++);
                lane->set_move_direction(osi3::LogicalLane_MoveDirection::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S);
                lane->set_type(static_cast<osi3::LogicalLane_Type>(laneTypes[i][j]));
                lane->add_source_reference()->add_identifier("Road 2");
                lane->mutable_reference_line_id()->set_value(referenceLine->id().value());
                const auto id{boundaries[i][j]->id().value()};
                const double startS{boundaries[i][j]->boundary_line(0).s_position()};
                lane->set_start_s(startS);
                const double endS{boundaries[i][j]->boundary_line().rbegin()->s_position()};
                lane->set_end_s(endS);
                lane->add_left_boundary_id()->set_value(boundaries[i][j]->id().value());
                for (osi3::LogicalLaneBoundary *boundary : boundaries[i + 1])
                {
                    if (boundary->boundary_line().rbegin()->s_position() <= lane->start_s())
                    {
                        continue;
                    }
                    if (boundary->boundary_line(0).s_position() >= lane->end_s())
                    {
                        break;
                    }
                    lane->add_right_boundary_id()->set_value(boundary->id().value());
                }
            }
            // Connect successors
            ConnectSuccessors(i, lanes);
        } // NOLINTEND(cppcoreguidelines-pro-bounds-constant-array-index)
    }

    static void ConnectAdjacentLanesHelper(size_t i, std::array<std::vector<osi3::LogicalLane *>, 4> &lanes, std::vector<osi3::LogicalLane *>::iterator leftLane)
    {
        for (auto *rightLane : lanes[i]) // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
        {
            if ((*leftLane)->end_s() <= rightLane->start_s())
            {
                ++leftLane;
            }
            else if ((*leftLane)->start_s() >= rightLane->end_s())
            {
                continue;
            }
            else
            {
                osi3::LogicalLane_LaneRelation *relation{(*leftLane)->add_right_adjacent_lane()};
                relation->set_start_s((*leftLane)->start_s());
                relation->set_start_s_other(rightLane->start_s());
                relation->set_end_s((*leftLane)->end_s());
                relation->set_end_s_other(rightLane->end_s());
                relation->mutable_other_lane_id()->set_value(rightLane->id().value());
                relation = rightLane->add_left_adjacent_lane();
                relation->set_start_s(rightLane->start_s());
                relation->set_start_s_other((*leftLane)->start_s());
                relation->set_end_s(rightLane->end_s());
                relation->set_end_s_other((*leftLane)->end_s());
                relation->mutable_other_lane_id()->set_value((*leftLane)->id().value());
            }
        }
    }

    static void ConnectAdjacentLanes(std::array<std::vector<osi3::LogicalLane *>, 4> &lanes)
    {
        for (size_t i{1}; i < lanes.size(); ++i)
        {
            auto leftLane{lanes[i - 1].begin()}; // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
            ConnectAdjacentLanesHelper(i, lanes, leftLane);
        }
    }

    void SetBoundariesAndPoints(size_t i, google::protobuf::internal::RepeatedPtrIterator<const osi3::ReferenceLine_ReferenceLinePoint> pointIt, const std::array<const std::array<XY, 4>, 5> &points, std::vector<double>::const_iterator breakIt, const std::array<const std::vector<double>, 5> &breaks, std::vector<std::vector<osi3::LogicalLaneBoundary *>> &boundaries, osi3::ReferenceLine *referenceLine, std::vector<double> &tValues)
    {
        auto t{tValues.begin()};
        for (; breakIt != breaks[i].end(); ++breakIt) // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
        {
            osi3::LogicalLaneBoundary *boundary{groundTruth.add_logical_lane_boundary()};
            boundaries[i].push_back(boundary);
            boundary->mutable_id()->set_value(boundaryId++);
            boundary->mutable_reference_line_id()->set_value(referenceLine->id().value());

            while ((pointIt != referenceLine->poly_line().end()) && (*t <= *breakIt))
            {
                osi3::LogicalLaneBoundary_LogicalBoundaryPoint *point{boundary->add_boundary_line()};
                const XY values{GetPoint(*t, points[i])}; // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
                point->mutable_position()->set_x(values.x);
                point->mutable_position()->set_y(values.y);
                point->set_s_position(pointIt->s_position());
                point->set_t_position(static_cast<double>(-10 * i));
                ++pointIt;
                ++t;
            }
            --pointIt;
            --t;
            // TODO: Assign t_axis_yaw of reference line points
        }
    }

    void CreateMiddleRoad()
    {
        // Anchor points of a quadratic bézier curve describing the boundaries of this road
        const std::array<const std::array<XY, 4>, 5> points{
            std::array<XY, 4>{XY{-100.0, 0.0}, {-100.0, 100.0}, {100.0, 100.0}, {100.0, 0.0}},
            std::array<XY, 4>{XY{-090.0, 0.0}, {-090.0, 090.0}, {090.0, 090.0}, {090.0, 0.0}},
            std::array<XY, 4>{XY{-080.0, 0.0}, {-080.0, 080.0}, {080.0, 080.0}, {080.0, 0.0}},
            std::array<XY, 4>{XY{-070.0, 0.0}, {-070.0, 070.0}, {070.0, 070.0}, {070.0, 0.0}},
            std::array<XY, 4>{XY{-060.0, 0.0}, {-060.0, 060.0}, {060.0, 060.0}, {060.0, 0.0}} //
        };
        const std::array<const std::vector<osiql::Lane::Type>, 4> laneTypes{
            std::vector<osiql::Lane::Type>{osiql::Lane::Type::Normal, osiql::Lane::Type::Normal},
            std::vector<osiql::Lane::Type>{osiql::Lane::Type::Normal, osiql::Lane::Type::Other, osiql::Lane::Type::Normal, osiql::Lane::Type::Other, osiql::Lane::Type::Normal, osiql::Lane::Type::Other},
            std::vector<osiql::Lane::Type>{osiql::Lane::Type::Other, osiql::Lane::Type::Normal, osiql::Lane::Type::Other, osiql::Lane::Type::Normal, osiql::Lane::Type::Normal, osiql::Lane::Type::Other},
            std::vector<osiql::Lane::Type>{osiql::Lane::Type::Normal} //
        };
        // Starts and ends of lanes in t values in [0.0, 1.0] to be interpolated along the corresponding bezier curve
        const std::array<const std::vector<double>, 5> breaks{
            std::vector<double>{0.0, 0.1, 1.0},
            std::vector<double>{0.0, 0.2, 0.35, 0.6, 0.8, 1.0},
            std::vector<double>{0.0, 0.1, 0.5, 0.7, 0.75, 0.9, 1.0},
            std::vector<double>{0.0, 1.0},
            std::vector<double>{0.0, 1.0} //
        };

        std::vector<double> tValues{
            0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45,     //
            0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0 //
        };

        std::array<std::vector<osi3::LogicalLane *>, 4> lanes;
        std::vector<std::vector<osi3::LogicalLaneBoundary *>> boundaries(breaks.size());

        // The first boundary chain is special. We can't create a lane yet and we need to create a reference line:
        osi3::ReferenceLine *referenceLine{groundTruth.add_reference_line()};
        referenceLine->mutable_id()->set_value(referenceLineId++);

        osi3::ReferenceLine_ReferenceLinePoint *referenceLinePoint{referenceLine->add_poly_line()};
        referenceLinePoint->mutable_world_position()->set_x(points[0][0].x);
        referenceLinePoint->mutable_world_position()->set_y(points[0][0].y);
        referenceLinePoint->set_s_position(0.0);
        for (auto tIter{std::next(tValues.begin())}; tIter != tValues.end(); ++tIter)
        {
            const XY point{GetPoint(*tIter, points[0])};
            const double s{referenceLine->poly_line().rbegin()->s_position()};
            const XY prev{*referenceLine->poly_line().rbegin()};
            osi3::ReferenceLine_ReferenceLinePoint *referenceLinePoint{referenceLine->add_poly_line()};
            referenceLinePoint->mutable_world_position()->set_x(point.x);
            referenceLinePoint->mutable_world_position()->set_y(point.y);
            referenceLinePoint->set_s_position(s + (point - prev).Length());
        }

        for (size_t i{0}; i < points.size(); ++i)
        {
            auto breakIt{std::next(breaks[i].begin())}; // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
            auto pointIt{referenceLine->poly_line().begin()};
            SetBoundariesAndPoints(i, pointIt, points, breakIt, breaks, boundaries, referenceLine, tValues);
        }
        // Create lanes
        CreateLanes(referenceLine, boundaries, laneTypes, breaks, lanes);

        // TODO: connect adjacent lanes
        ConnectAdjacentLanes(lanes);
    }

    // void CreateEndingRoad()
    // {
    //     const std::array<const std::array<XY, 2>, 2> points{
    //         std::array<XY, 2>{XY{70.0, 0.0}, {70.0, -20.0}},
    //         std::array<XY, 2>{XY{60.0, 0.0}, {60.0, -20.0}} //
    //     };
    // }

    LocalizationTest()
    {
        // CreateStartingRoad(); // TODO: Implement
        CreateMiddleRoad();
        // CreateEndingRoad(); // TODO: Implement

        world = std::make_unique<osiql::World>(groundTruth);
    }
};
