/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "util/Common.tpp"

using namespace osiql;

struct PathfindingTest : ::testing::Test
{
    PathfindingTest() :
        connectedRoads{
            groundTruth.AddRoad(
                groundTruth.AddReferenceLine(XY{0, 0}, XY{10, 0}),
                {
                    &groundTruth.AddBoundary(XY{0, 3}, XY{10, 3}),
                    &groundTruth.AddBoundary(XY{0, 0}, XY{10, 0}),
                    &groundTruth.AddBoundary(XY{0, -3}, XY{10, -3}),
                }
            ),
            groundTruth.AddRoad(
                groundTruth.AddReferenceLine(XY{10, 0}, XY{20, 0}),
                {
                    &groundTruth.AddBoundary(XY{10, 3}, XY{20, 3}),
                    &groundTruth.AddBoundary(XY{10, 0}, XY{20, 0}),
                    &groundTruth.AddBoundary(XY{10, -3}, XY{20, -3}),
                }
            ),
            groundTruth.AddRoad(
                groundTruth.AddReferenceLine(XY{20, 0}, XY{30, 0}),
                {
                    &groundTruth.AddBoundary(XY{20, 3}, XY{30, 3}),
                    &groundTruth.AddBoundary(XY{20, 0}, XY{30, 0}),
                    &groundTruth.AddBoundary(XY{20, -3}, XY{30, -3}),
                }
            )},
        disconnectedRoad{groundTruth.AddRoad(
            groundTruth.AddReferenceLine(XY{-10, 0}, XY{0, 0}),
            {
                &groundTruth.AddBoundary(XY{-10, 3}, XY{0, 3}),
                &groundTruth.AddBoundary(XY{-10, 0}, XY{0, 0}),
                &groundTruth.AddBoundary(XY{-10, -3}, XY{0, -3}),
            }
        )}
    {
        test::ConnectRoads(connectedRoads[0].begin(), connectedRoads[0].end(), connectedRoads[1].begin(), connectedRoads[1].end());
        test::ConnectRoads(connectedRoads[1].begin(), connectedRoads[1].end(), connectedRoads[2].begin(), connectedRoads[2].end());

        world = std::make_unique<World>(groundTruth.handle);
    }

    test::GroundTruth groundTruth;
    std::vector<std::vector<osi3::LogicalLane *>> connectedRoads;
    std::vector<osi3::LogicalLane *> disconnectedRoad;
    std::unique_ptr<World> world;
};

TEST_F(PathfindingTest, Point_GetRoute_Reachability_Test)
{
    // Downstream lanes
    {
        const auto A{world->Localize(XY{5.0, -1.5})};
        ASSERT_THAT(A, SizeIs(1));
        const Point<> &a{A[0]};

        const auto B{world->Localize(XY{30.0, -1.5})};
        ASSERT_THAT(B, SizeIs(1));
        const Point<> &b{B[0]};

        EXPECT_TRUE(a.GetRoute(b).has_value());
        EXPECT_FALSE(b.GetRoute(a).has_value());
        EXPECT_FALSE(a.GetRoute<Traversal::Backward>(b).has_value());
        EXPECT_TRUE(b.GetRoute<Traversal::Backward>(a).has_value());
    }
    // Upstream lanes
    {
        const auto A{world->Localize(XY{5.0, 1.5})};
        ASSERT_THAT(A, SizeIs(1));
        const Point<> &a{A[0]};

        const auto B{world->Localize(XY{30.0, 1.5})};
        ASSERT_THAT(B, SizeIs(1));
        const Point<> &b{B[0]};

        EXPECT_FALSE(a.GetRoute(b).has_value());
        EXPECT_TRUE(b.GetRoute(a).has_value());
        EXPECT_TRUE(a.GetRoute<Traversal::Backward>(b).has_value());
        EXPECT_FALSE(b.GetRoute<Traversal::Backward>(a).has_value());
    }
}

TEST_F(PathfindingTest, Point_GetRoute_Reachability_Disconnected_Test)
{
    const auto A{world->Localize(XY{5.0, -1.5})};
    ASSERT_THAT(A, SizeIs(1));
    const Point<> &a{A[0]};

    const auto B{world->Localize(XY{-5.0, -1.5})};
    ASSERT_THAT(B, SizeIs(1));
    const Point<> &b{B[0]};

    EXPECT_FALSE(a.GetRoute(b).has_value());
    EXPECT_FALSE(b.GetRoute(a).has_value());
    EXPECT_FALSE(a.GetRoute<Traversal::Backward>(b).has_value());
    EXPECT_FALSE(b.GetRoute<Traversal::Backward>(a).has_value());
}

TEST_F(PathfindingTest, World_GetRoute_Reachability_Test)
{
    EXPECT_TRUE(world->GetRoute(XY{0, -1.5}, XY{30, -1.5}).has_value());
    EXPECT_FALSE(world->GetRoute(XY{30, -1.5}, XY{0, -1.5}).has_value());

    EXPECT_FALSE(world->GetRoute(XY{0, 1.5}, XY{30, 1.5}).has_value());
    EXPECT_TRUE(world->GetRoute(XY{30, 1.5}, XY{0, 1.5}).has_value());

    EXPECT_FALSE(world->GetRoute<Traversal::Backward>(XY{0, -1.5}, XY{30, -1.5}).has_value());
    EXPECT_TRUE(world->GetRoute<Traversal::Backward>(XY{30, -1.5}, XY{0, -1.5}).has_value());

    EXPECT_TRUE(world->GetRoute<Traversal::Backward>(XY{0, 1.5}, XY{30, 1.5}).has_value());
    EXPECT_FALSE(world->GetRoute<Traversal::Backward>(XY{30, 1.5}, XY{0, 1.5}).has_value());
}

TEST_F(PathfindingTest, World_GetRoute_Reachability_Disconnected_Test)
{
    EXPECT_FALSE(world->GetRoute(XY{0, -1.5}, XY{-30, -1.5}).has_value());
    EXPECT_FALSE(world->GetRoute(XY{-30, -1.5}, XY{0, -1.5}).has_value());

    EXPECT_FALSE(world->GetRoute(XY{0, 1.5}, XY{-30, 1.5}).has_value());
    EXPECT_FALSE(world->GetRoute(XY{-30, 1.5}, XY{0, 1.5}).has_value());

    EXPECT_FALSE(world->GetRoute<Traversal::Backward>(XY{0, -1.5}, XY{-30, -1.5}).has_value());
    EXPECT_FALSE(world->GetRoute<Traversal::Backward>(XY{-30, -1.5}, XY{0, -1.5}).has_value());

    EXPECT_FALSE(world->GetRoute<Traversal::Backward>(XY{0, 1.5}, XY{-30, 1.5}).has_value());
    EXPECT_FALSE(world->GetRoute<Traversal::Backward>(XY{-30, 1.5}, XY{0, 1.5}).has_value());
}
