/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "util/Common.tpp"

using namespace osiql;

TEST(Node, Construction_From_Point)
{
    test::GroundTruth groundTruth;
    auto &logicalLane{groundTruth.AddLane(
        groundTruth.AddReferenceLine(XY{0, 0}, XY{10, 0}),
        groundTruth.AddBoundary(XY{0, 0}, XY{10, 0}),
        groundTruth.AddBoundary(XY{0, -10}, XY{10, -10})
    )};
    const Id laneId{get<Id>(logicalLane)};

    World world{groundTruth.handle};
    const Lane *lane{world.GetLane(laneId)};
    ASSERT_NE(lane, nullptr);

    { // Origin before start of lane:
        const Point<> origin{{-5, -5}, *lane};
        EXPECT_DOUBLE_EQ(Node<>::Create(origin)->distance, 5.0);
        EXPECT_DOUBLE_EQ(LaneNode<>::Create(origin)->distance, 5.0);
        EXPECT_DOUBLE_EQ(Node<Traversal::Backward>::Create(origin)->distance, -15.0);
        EXPECT_DOUBLE_EQ(LaneNode<Traversal::Backward>::Create(origin)->distance, -15.0);
    }
    { // Origin at start of lane:
        const Point<> origin{{0, -5}, *lane};
        EXPECT_DOUBLE_EQ(Node<>::Create(origin)->distance, 0.0);
        EXPECT_DOUBLE_EQ(LaneNode<>::Create(origin)->distance, 0.0);
        EXPECT_DOUBLE_EQ(Node<Traversal::Backward>::Create(origin)->distance, -10.0);
        EXPECT_DOUBLE_EQ(LaneNode<Traversal::Backward>::Create(origin)->distance, -10.0);
    }
    { // Origin between start and end of lane:
        const Point<> origin{{2, -5}, *lane};
        EXPECT_DOUBLE_EQ(Node<>::Create(origin)->distance, -2.0);
        EXPECT_DOUBLE_EQ(LaneNode<>::Create(origin)->distance, -2.0);
        EXPECT_DOUBLE_EQ(Node<Traversal::Backward>::Create(origin)->distance, -8.0);
        EXPECT_DOUBLE_EQ(LaneNode<Traversal::Backward>::Create(origin)->distance, -8.0);
    }
    { // Origin after end of lane:
        const Point<> origin{{15, -5}, *lane};
        EXPECT_DOUBLE_EQ(Node<>::Create(origin)->distance, -15.0);
        EXPECT_DOUBLE_EQ(LaneNode<>::Create(origin)->distance, -15.0);
        EXPECT_DOUBLE_EQ(Node<Traversal::Backward>::Create(origin)->distance, 5.0);
        EXPECT_DOUBLE_EQ(LaneNode<Traversal::Backward>::Create(origin)->distance, 5.0);
    }
}

// This test can not fail without compilation failing as well. It guarantees
// that the template constructor of a node taking a single argument supports
// a lane as an argument as well
TEST(Node, Construction_From_Lane)
{
    test::GroundTruth groundTruth;
    auto &logicalLane{groundTruth.AddLane(
        groundTruth.AddReferenceLine(XY{0, 0}, XY{10, 0}),
        groundTruth.AddBoundary(XY{0, 0}, XY{10, 0}),
        groundTruth.AddBoundary(XY{0, -10}, XY{10, -10})
    )};
    World world{groundTruth.handle};
    const Lane *lane{world.GetLane(get<Id>(logicalLane))};
    Node<>::Create(*lane);
    LaneNode<>::Create(*lane);
    Node<Traversal::Backward>::Create(*lane);
    LaneNode<Traversal::Backward>::Create(*lane);
}

struct SingleLaneWith3MovingObjects : ::testing::Test
{
    SingleLaneWith3MovingObjects() :
        logicalLane{groundTruth.AddLane(
            groundTruth.AddReferenceLine(XY{0, 0}, XY{10, 0}),
            groundTruth.AddBoundary(XY{0, 0}, XY{10, 0}),
            groundTruth.AddBoundary(XY{0, -10}, XY{10, -10})
        )},
        objects{
            &groundTruth.Add<MovingObject>(XY{0, -5}, 2, 1),
            &groundTruth.Add<MovingObject>(XY{5, -5}, 2, 1),
            &groundTruth.Add<MovingObject>(XY{10, -5}, 2, 1) //
        }
    {
    }

    test::GroundTruth groundTruth;
    osi3::LogicalLane &logicalLane; // The handle of the lane of the node
    std::array<const osi3::MovingObject *, 3> objects;
};

// Construct nodes from the overlaps of these three objects on a lane:
//    0m                     10m
//    .-----------------------.
//    |                       |
// .-----.     .-----.     .-----.
// |  1  |     |  2  |     |  3  | <- Moving objects
// '-----'     '-----'     '-----'
//    |                       |
//    '-----------------------'
// Since overlaps don't include parts of the object that don't lie on the lane, the
// respective s-coordinate intervals of the objects should be 0 to 1, 4 to 6 and 9 to 10.
TEST_F(SingleLaneWith3MovingObjects, NodeConstruction_From_LaneOverlap)
{
    World world{groundTruth.handle};
    {
        const Overlap<Lane> &overlap{world.GetMovingObject(get<Id>(objects[0]))->GetOverlaps<Lane>()[0]};
        EXPECT_DOUBLE_EQ(Node<>::Create(overlap)->distance, 0.0);
        EXPECT_DOUBLE_EQ(LaneNode<>::Create(overlap)->distance, 0.0);
        EXPECT_DOUBLE_EQ(Node<Traversal::Backward>::Create(overlap)->distance, -9.0);
        EXPECT_DOUBLE_EQ(LaneNode<Traversal::Backward>::Create(overlap)->distance, -9.0);
    }
    {
        const Overlap<Lane> &overlap{world.GetMovingObject(get<Id>(objects[1]))->GetOverlaps<Lane>()[0]};
        EXPECT_DOUBLE_EQ(Node<>::Create(overlap)->distance, -4.0);
        EXPECT_DOUBLE_EQ(LaneNode<>::Create(overlap)->distance, -4.0);
        EXPECT_DOUBLE_EQ(Node<Traversal::Backward>::Create(overlap)->distance, -4.0);
        EXPECT_DOUBLE_EQ(LaneNode<Traversal::Backward>::Create(overlap)->distance, -4.0);
    }
    {
        const Overlap<Lane> &overlap{world.GetMovingObject(get<Id>(objects[2]))->GetOverlaps<Lane>()[0]};
        EXPECT_DOUBLE_EQ(Node<>::Create(overlap)->distance, -9.0);
        EXPECT_DOUBLE_EQ(LaneNode<>::Create(overlap)->distance, -9.0);
        EXPECT_DOUBLE_EQ(Node<Traversal::Backward>::Create(overlap)->distance, 0.0);
        EXPECT_DOUBLE_EQ(LaneNode<Traversal::Backward>::Create(overlap)->distance, 0.0);
    }
}

TEST(Node, GetDirection)
{
    test::GroundTruth groundTruth;
    std::vector<osi3::LogicalLane *> road{groundTruth.AddRoad(
        groundTruth.AddReferenceLine(XY{0, 0}, XY{10, 0}),
        {
            &groundTruth.AddBoundary(XY{0, 5}, XY{10, 5}),
            &groundTruth.AddBoundary(XY{0, 0}, XY{10, 0}),
            &groundTruth.AddBoundary(XY{0, -5}, XY{10, -5}),
        }
    )};
    World world{groundTruth.handle};
    const Lane *downstreamLane{world.GetLane(get<Id>(road[1]))};
    const Lane *upstreamLane{world.GetLane(get<Id>(road[0]))};

    EXPECT_EQ(Node<>::Create(*downstreamLane)->GetDirection(), Direction::Downstream);
    EXPECT_EQ(Node<>::Create(*upstreamLane)->GetDirection(), Direction::Upstream);
    EXPECT_EQ(Node<Traversal::Backward>::Create(*downstreamLane)->GetDirection(), Direction::Upstream);
    EXPECT_EQ(Node<Traversal::Backward>::Create(*upstreamLane)->GetDirection(), Direction::Downstream);
}

TEST(Node, GetDistance_OfPoint)
{
    test::GroundTruth groundTruth;
    std::vector<osi3::LogicalLane *> road{groundTruth.AddRoad(
        groundTruth.AddReferenceLine(XY{0, 0}, XY{10, 0}),
        {
            &groundTruth.AddBoundary(XY{0, 5}, XY{10, 5}),
            &groundTruth.AddBoundary(XY{0, 0}, XY{10, 0}),
            &groundTruth.AddBoundary(XY{0, -5}, XY{10, -5}),
        }
    )};
    World world{groundTruth.handle};
    const Lane *downstreamLane{world.GetLane(get<Id>(road[1]))};
    const Lane *upstreamLane{world.GetLane(get<Id>(road[0]))};

    const Point<> point{*downstreamLane, {3, 0}};
    EXPECT_EQ(Node<>::Create(*downstreamLane)->GetDistance(point), 3.0);
    EXPECT_EQ(Node<>::Create(*upstreamLane)->GetDistance(point), 7.0);
    EXPECT_EQ(Node<Traversal::Backward>::Create(*downstreamLane)->GetDistance(point), 7.0);
    EXPECT_EQ(Node<Traversal::Backward>::Create(*upstreamLane)->GetDistance(point), 3.0);
}

TEST_F(SingleLaneWith3MovingObjects, GetDistance_OfObject_FromNodeOrigin_AtStartOfLane_InNodeDirection)
{
    World world{groundTruth.handle};
    const Lane &lane{*world.GetLane(get<Id>(logicalLane))};
    {
        const MovingObject &object{*world.GetMovingObject(get<Id>(objects[0]))};
        EXPECT_EQ(Node<>::Create(lane)->GetDistance(object), Interval<double>(0, 1));
        EXPECT_EQ(Node<Traversal::Backward>::Create(lane)->GetDistance(object), Interval<double>(9, 10));
    }
    {
        const MovingObject &object{*world.GetMovingObject(get<Id>(objects[1]))};
        EXPECT_EQ(Node<>::Create(lane)->GetDistance(object), Interval<double>(4, 6));
        EXPECT_EQ(Node<Traversal::Backward>::Create(lane)->GetDistance(object), Interval<double>(4, 6));
    }
    {
        const MovingObject &object{*world.GetMovingObject(get<Id>(objects[2]))};
        EXPECT_EQ(Node<>::Create(lane)->GetDistance(object), Interval<double>(9, 10));
        EXPECT_EQ(Node<Traversal::Backward>::Create(lane)->GetDistance(object), Interval<double>(0, 1));
    }
}

struct RoadNetwork : ::testing::Test
{
    RoadNetwork() :
        roads{
            groundTruth.AddRoad( // 0
                groundTruth.AddReferenceLine(XY{-70, 0}, XY{-50, 0}),
                {
                    &groundTruth.AddBoundary(XY{-70, 5}, XY{-50, 5}),
                    &groundTruth.AddBoundary(XY{-70, 0}, XY{-50, 0}),
                    &groundTruth.AddBoundary(XY{-70, -5}, XY{-50, -5}),
                }
            ),
            groundTruth.AddRoad( // 1
                groundTruth.AddReferenceLine(Pose<XY>{{-30, 15}, -M_PI_2}, Pose<XY>{{-50, 30}, -M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{-30, 10}, XY{-50, 25}),
                    &groundTruth.AddBoundary(XY{-30, 15}, XY{-50, 30}),
                    &groundTruth.AddBoundary(XY{-30, 20}, XY{-50, 35}),
                }
            ),
            groundTruth.AddRoad( // 2
                groundTruth.AddReferenceLine(Pose<XY>{{-50, 0}, M_PI_2}, Pose<XY>{{-30, 15}, M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{-50, 5}, XY{-30, 20}),
                    &groundTruth.AddBoundary(XY{-50, 0}, XY{-30, 15}),
                    &groundTruth.AddBoundary(XY{-50, -5}, XY{-30, 10}),
                }
            ),
            groundTruth.AddRoad( // 3
                groundTruth.AddReferenceLine(Pose<XY>{{-30, -15}, -M_PI_2}, Pose<XY>{{-50, 0}, -M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{-30, -20}, XY{-50, -5}),
                    &groundTruth.AddBoundary(XY{-30, -15}, XY{-50, 0}),
                    &groundTruth.AddBoundary(XY{-30, -10}, XY{-50, 5}),
                }
            ),
            groundTruth.AddRoad( // 4
                groundTruth.AddReferenceLine(Pose<XY>{{-50, -30}, M_PI_2}, Pose<XY>{{-30, -15}, M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{-50, -35}, XY{-30, -20}),
                    &groundTruth.AddBoundary(XY{-50, -30}, XY{-30, -15}),
                    &groundTruth.AddBoundary(XY{-50, -25}, XY{-30, -10}),
                }
            ),
            groundTruth.AddRoad( // 5
                groundTruth.AddReferenceLine(Pose<XY>{{-30, 15}, M_PI_2}, Pose<XY>{{-10, 30}, M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{-30, 20}, XY{-10, 35}),
                    &groundTruth.AddBoundary(XY{-30, 15}, XY{-10, 30}),
                    &groundTruth.AddBoundary(XY{-30, 10}, XY{-10, 25}),
                }
            ),
            groundTruth.AddRoad( // 6
                groundTruth.AddReferenceLine(Pose<XY>{{-10, 0}, -M_PI_2}, Pose<XY>{{-30, 15}, -M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{-10, -5}, XY{-30, 10}),
                    &groundTruth.AddBoundary(XY{-10, 0}, XY{-30, 15}),
                    &groundTruth.AddBoundary(XY{-10, 5}, XY{-30, 20}),
                }
            ),
            groundTruth.AddRoad( // 7
                groundTruth.AddReferenceLine(Pose<XY>{{-30, -15}, M_PI_2}, Pose<XY>{{-10, 0}, M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{-30, -10}, XY{-10, 5}),
                    &groundTruth.AddBoundary(XY{-30, -15}, XY{-10, 0}),
                    &groundTruth.AddBoundary(XY{-30, -20}, XY{-10, -5}),
                }
            ),
            groundTruth.AddRoad( // 8
                groundTruth.AddReferenceLine(Pose<XY>{{-10, -30}, -M_PI_2}, Pose<XY>{{-30, -15}, -M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{-10, -35}, XY{-30, -20}),
                    &groundTruth.AddBoundary(XY{-10, -30}, XY{-30, -15}),
                    &groundTruth.AddBoundary(XY{-10, -25}, XY{-30, -10}),
                }
            ),
            groundTruth.AddRoad( // 9
                groundTruth.AddReferenceLine(XY{-10, 30}, XY{10, 30}),
                {
                    &groundTruth.AddBoundary(XY{-10, 35}, XY{10, 35}),
                    &groundTruth.AddBoundary(XY{-10, 30}, XY{10, 30}),
                    &groundTruth.AddBoundary(XY{-10, -25}, XY{10, -25}),
                }
            ),
            groundTruth.AddRoad( // 10
                groundTruth.AddReferenceLine(XY{-10, 0}, XY{10, 0}),
                {
                    &groundTruth.AddBoundary(XY{-10, 5}, XY{10, 5}),
                    &groundTruth.AddBoundary(XY{-10, 0}, XY{10, 0}),
                    &groundTruth.AddBoundary(XY{-10, -5}, XY{10, -5}),
                }
            ),
            groundTruth.AddRoad( // 11
                groundTruth.AddReferenceLine(XY{-10, -30}, XY{10, -30}),
                {
                    &groundTruth.AddBoundary(XY{-10, -25}, XY{10, -25}),
                    &groundTruth.AddBoundary(XY{-10, -30}, XY{10, -30}),
                    &groundTruth.AddBoundary(XY{-10, -35}, XY{10, -35}),
                }
            ),
            groundTruth.AddRoad( // 12
                groundTruth.AddReferenceLine(Pose<XY>{{30, 15}, -M_PI_2}, Pose<XY>{{10, 30}, -M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{30, 10}, XY{10, 25}),
                    &groundTruth.AddBoundary(XY{30, 15}, XY{10, 30}),
                    &groundTruth.AddBoundary(XY{30, 20}, XY{10, 35}),
                }
            ),
            groundTruth.AddRoad( // 13
                groundTruth.AddReferenceLine(Pose<XY>{{10, 0}, M_PI_2}, Pose<XY>{{30, 15}, M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{10, 5}, XY{30, 20}),
                    &groundTruth.AddBoundary(XY{10, 0}, XY{30, 15}),
                    &groundTruth.AddBoundary(XY{10, -5}, XY{30, 10}),
                }
            ),
            groundTruth.AddRoad( // 14
                groundTruth.AddReferenceLine(Pose<XY>{{30, -15}, -M_PI_2}, Pose<XY>{{10, 0}, -M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{30, -20}, XY{10, -5}),
                    &groundTruth.AddBoundary(XY{30, -15}, XY{10, 0}),
                    &groundTruth.AddBoundary(XY{30, -10}, XY{10, 5}),
                }
            ),
            groundTruth.AddRoad( // 15
                groundTruth.AddReferenceLine(Pose<XY>{{10, -30}, M_PI_2}, Pose<XY>{{30, -15}, M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{10, -25}, XY{30, -10}),
                    &groundTruth.AddBoundary(XY{10, -30}, XY{30, -15}),
                    &groundTruth.AddBoundary(XY{10, -35}, XY{30, -20}),
                }
            ),
            groundTruth.AddRoad( // 16
                groundTruth.AddReferenceLine(Pose<XY>{{30, 15}, M_PI_2}, Pose<XY>{{50, 30}, M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{30, 20}, XY{50, 35}),
                    &groundTruth.AddBoundary(XY{30, 15}, XY{50, 30}),
                    &groundTruth.AddBoundary(XY{30, 10}, XY{50, 25}),
                }
            ),
            groundTruth.AddRoad( // 17
                groundTruth.AddReferenceLine(Pose<XY>{{50, 0}, -M_PI_2}, Pose<XY>{{30, 15}, -M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{50, -5}, XY{30, 10}),
                    &groundTruth.AddBoundary(XY{50, 0}, XY{30, 15}),
                    &groundTruth.AddBoundary(XY{50, 5}, XY{30, 20}),
                }
            ),
            groundTruth.AddRoad( // 18
                groundTruth.AddReferenceLine(Pose<XY>{{30, -15}, M_PI_2}, Pose<XY>{{50, 0}, M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{30, -10}, XY{50, 5}),
                    &groundTruth.AddBoundary(XY{30, -15}, XY{50, 0}),
                    &groundTruth.AddBoundary(XY{30, -20}, XY{50, -5}),
                }
            ),
            groundTruth.AddRoad( // 19
                groundTruth.AddReferenceLine(Pose<XY>{{50, -30}, -M_PI_2}, Pose<XY>{{30, -15}, -M_PI_2}),
                {
                    &groundTruth.AddBoundary(XY{50, -35}, XY{30, -20}),
                    &groundTruth.AddBoundary(XY{50, -30}, XY{30, -15}),
                    &groundTruth.AddBoundary(XY{50, -25}, XY{30, -10}),
                }
            ),
            groundTruth.AddRoad( // 20
                groundTruth.AddReferenceLine(XY{50, 0}, XY{70, 0}),
                {
                    &groundTruth.AddBoundary(XY{50, 5}, XY{70, 5}),
                    &groundTruth.AddBoundary(XY{50, 0}, XY{70, 0}),
                    &groundTruth.AddBoundary(XY{50, -5}, XY{70, -5}),
                }
            ),
        }
    {
        for (size_t i{0}; i < roads.size(); ++i)
        {
            for (osi3::LogicalLane *logicalLane : roads[i])
            {
                logicalLane->mutable_source_reference()->Add()->add_identifier("Road " + std::to_string(i));
            }
        }
        test::ConnectRoads(roads[0].begin(), roads[0].end(), roads[2].begin(), roads[2].end());
        test::ConnectRoads(roads[0].begin(), roads[0].end(), roads[3].rbegin(), roads[3].rend());
        test::ConnectRoads(roads[1].rbegin(), roads[1].rend(), roads[5].begin(), roads[5].end());
        test::ConnectRoads(roads[1].rbegin(), roads[1].rend(), roads[6].rbegin(), roads[6].rend());
        test::ConnectRoads(roads[2].begin(), roads[2].end(), roads[5].begin(), roads[5].end());
        test::ConnectRoads(roads[2].begin(), roads[2].end(), roads[6].rbegin(), roads[6].rend());
        test::ConnectRoads(roads[2].begin(), roads[2].end(), roads[7].begin(), roads[7].end());
        test::ConnectRoads(roads[2].begin(), roads[2].end(), roads[8].rbegin(), roads[8].rend());
        test::ConnectRoads(roads[3].rbegin(), roads[3].rend(), roads[7].begin(), roads[7].end());
        test::ConnectRoads(roads[3].rbegin(), roads[3].rend(), roads[8].rbegin(), roads[8].rend());
        test::ConnectRoads(roads[5].begin(), roads[5].end(), roads[9].begin(), roads[9].end());
        test::ConnectRoads(roads[6].rbegin(), roads[6].rend(), roads[10].begin(), roads[10].end());
        test::ConnectRoads(roads[7].begin(), roads[7].end(), roads[10].begin(), roads[10].end());
        test::ConnectRoads(roads[8].begin(), roads[8].end(), roads[11].begin(), roads[11].end());
        test::ConnectRoads(roads[9].begin(), roads[9].end(), roads[12].rbegin(), roads[12].rend());
        test::ConnectRoads(roads[10].begin(), roads[10].end(), roads[13].begin(), roads[13].end());
        test::ConnectRoads(roads[10].begin(), roads[10].end(), roads[14].rbegin(), roads[14].rend());
        test::ConnectRoads(roads[11].begin(), roads[11].end(), roads[15].begin(), roads[15].end());
        test::ConnectRoads(roads[12].rbegin(), roads[12].rend(), roads[16].begin(), roads[16].end());
        test::ConnectRoads(roads[12].rbegin(), roads[12].rend(), roads[17].rbegin(), roads[17].rend());
        test::ConnectRoads(roads[13].begin(), roads[13].end(), roads[16].begin(), roads[16].end());
        test::ConnectRoads(roads[13].begin(), roads[13].end(), roads[17].rbegin(), roads[17].rend());
        test::ConnectRoads(roads[13].begin(), roads[13].end(), roads[18].begin(), roads[18].end());
        test::ConnectRoads(roads[13].begin(), roads[13].end(), roads[19].rbegin(), roads[19].rend());
        test::ConnectRoads(roads[14].rbegin(), roads[14].rend(), roads[18].begin(), roads[18].end());
        test::ConnectRoads(roads[14].rbegin(), roads[14].rend(), roads[19].rbegin(), roads[19].rend());
        test::ConnectRoads(roads[17].rbegin(), roads[17].rend(), roads[20].begin(), roads[20].end());
        test::ConnectRoads(roads[18].begin(), roads[18].end(), roads[20].begin(), roads[20].end());
    }

    test::GroundTruth groundTruth;
    std::vector<std::vector<osi3::LogicalLane *>> roads;
};

TEST_F(RoadNetwork, FindNode)
{
    World world{groundTruth.handle};
    { // Downstream - Forward
        const Lane &initialLane{*world.GetLane(get<Id>(roads[10].back()))};
        std::shared_ptr<const Node<>> root{Node<>::Create(initialLane)};
        const Node<> *goal{root->FindClosestNode(Point<>{{10, -1}, *world.GetLane(get<Id>(roads[20].back()))})};
        ASSERT_NE(goal, nullptr);
        EXPECT_EQ(goal->GetRoad().GetOpenDriveId(), "Road 20");
    }
    { // Downstream - Backward
        const Lane &initialLane{*world.GetLane(get<Id>(roads[10].back()))};
        auto root{Node<Traversal::Backward>::Create(initialLane)};
        const auto *goal{root->FindClosestNode(Point<>{{10, -1}, *world.GetLane(get<Id>(roads[0].back()))})};
        ASSERT_NE(goal, nullptr);
        EXPECT_EQ(goal->GetRoad().GetOpenDriveId(), "Road 0");
    }
    { // Upstream - Forward
        const Lane &initialLane{*world.GetLane(get<Id>(roads[10].front()))};
        std::shared_ptr<const Node<>> root{Node<>::Create(initialLane)};
        const Node<> *goal{root->FindClosestNode(Point<>{{10, -1}, *world.GetLane(get<Id>(roads[0].back()))})};
        ASSERT_NE(goal, nullptr);
        EXPECT_EQ(goal->GetRoad().GetOpenDriveId(), "Road 0");
    }
    { // Upstream - Backward
        const Lane &initialLane{*world.GetLane(get<Id>(roads[10].front()))};
        auto root{Node<Traversal::Backward>::Create(initialLane)};
        const auto *goal{root->FindClosestNode(Point<>{{10, -1}, *world.GetLane(get<Id>(roads[20].back()))})};
        ASSERT_NE(goal, nullptr);
        EXPECT_EQ(goal->GetRoad().GetOpenDriveId(), "Road 20");
    }
}

TEST_F(RoadNetwork, Find_MovingObject)
{
    std::vector<osi3::MovingObject *> objects{
        &groundTruth.Add<MovingObject>(XY{0, 0}, 4, 1),
        &groundTruth.Add<MovingObject>(XY{0, 0}, 3, 1),
        &groundTruth.Add<MovingObject>(XY{0, 0}, 2, 1),
    };
    World world{groundTruth.handle};
    { // Downstream - Forward
        const Lane &initialLane{*world.GetLane(get<Id>(roads[10].back()))};
        std::shared_ptr<const Node<>> root{Node<>::Create(initialLane)};
        std::vector<Location<MovingObject, Node<>>> relation{root->Find<MovingObject>()};
        ASSERT_FALSE(relation.empty());
        EXPECT_DOUBLE_EQ(relation[0].Get<MovingObject>().GetLength(), 4.0);
    }
}
