/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "util/Common.tpp"

using namespace osiql;

TEST(MathTest, Angle_Wrapping)
{
    std::array<double, 4> results{0.0, halfPi, pi, -halfPi};
    EXPECT_NEAR(osiql::WrapAngle(-12 * halfPi), 0.0, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(-8 * halfPi), 0.0, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(-4 * halfPi), 0.0, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(0.0), 0.0, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(4 * halfPi), 0.0, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(8 * halfPi), 0.0, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(12 * halfPi), 0.0, 1e-5);

    EXPECT_NEAR(osiql::WrapAngle(-11 * halfPi), halfPi, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(-7 * halfPi), halfPi, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(-3 * halfPi), halfPi, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(1 * halfPi), halfPi, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(5 * halfPi), halfPi, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(9 * halfPi), halfPi, 1e-5);

    EXPECT_NEAR(osiql::WrapAngle(-10 * halfPi), pi, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(-6 * halfPi), pi, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(-2 * halfPi), pi, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(2 * halfPi), pi, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(6 * halfPi), pi, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(10 * halfPi), pi, 1e-5);

    EXPECT_NEAR(osiql::WrapAngle(-9 * halfPi), -halfPi, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(-5 * halfPi), -halfPi, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(-1 * halfPi), -halfPi, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(3 * halfPi), -halfPi, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(7 * halfPi), -halfPi, 1e-5);
    EXPECT_NEAR(osiql::WrapAngle(11 * halfPi), -halfPi, 1e-5);
}

TEST(MathTest, Matrix_Default_Initialization_Creates_Identity_Matrix)
{
    EXPECT_EQ(Matrix<2>(), Matrix<2>({Row<2>{1.0, 0.0}, Row<2>{0.0, 1.0}}));
    EXPECT_EQ(Matrix<3>(), Matrix<3>({Row<3>{1.0, 0.0, 0.0}, Row<3>{0.0, 1.0, 0.0}, Row<3>{0.0, 0.0, 1.0}}));
    EXPECT_EQ(Matrix<4>(), Matrix<4>({Row<4>{1.0, 0.0, 0.0, 0.0}, Row<4>{0.0, 1.0, 0.0, 0.0}, Row<4>{0.0, 0.0, 1.0, 0.0}, Row<4>{0.0, 0.0, 0.0, 1.0}}));
}

TEST(MathTest, Matrix_Addition_And_Subtraction)
{
    Matrix<3> A({Row<3>{10.0, 20.0, 10.0}, Row<3>{4.0, 5.0, 6.0}, Row<3>{2.0, 3.0, 5.0}});
    Matrix<3> B({Row<3>{3.0, 2.0, 4.0}, Row<3>{3.0, 3.0, 9.0}, Row<3>{4.0, 4.0, 2.0}});
    Matrix<3> C({Row<3>{13.0, 22.0, 14.0}, Row<3>{7.0, 8.0, 15.0}, Row<3>{6.0, 7.0, 7.0}});
    EXPECT_EQ(A + B, C);
    EXPECT_EQ(C - B, A);
}

TEST(MathTest, Matrix_Multiplication)
{
    Matrix<3> A({Row<3>{10.0, 20.0, 10.0}, Row<3>{4.0, 5.0, 6.0}, Row<3>{2.0, 3.0, 5.0}});
    Matrix<3> B({Row<3>{3.0, 2.0, 4.0}, Row<3>{3.0, 3.0, 9.0}, Row<3>{4.0, 4.0, 2.0}});
    Matrix<3> C({Row<3>{130.0, 120.0, 240.0}, Row<3>{51.0, 47.0, 73.0}, Row<3>{35.0, 33.0, 45.0}});
    EXPECT_EQ(A * B, C);
}

TEST(MathTest, Matrix_Scalar_Multiplication_And_Division)
{
    Matrix<3> A({Row<3>{10.0, 20.0, 10.0}, Row<3>{4.0, 5.0, 6.0}, Row<3>{2.0, 3.0, 5.0}});
    EXPECT_EQ(A * 2.0, Matrix<3>({Row<3>{20.0, 40.0, 20.0}, Row<3>{8.0, 10.0, 12.0}, Row<3>{4.0, 6.0, 10.0}}));
    EXPECT_EQ(A / 2.0, Matrix<3>({Row<3>{5.0, 10.0, 5.0}, Row<3>{2.0, 2.5, 3.0}, Row<3>{1.0, 1.5, 2.5}}));
}

TEST(MathTest, Matrix_Vector_Multiplication)
{
    Matrix<4> A;
    XY v{1.0, 2.0};
    EXPECT_EQ(A * v, XY(1.0, 2.0));
}

TEST(MathTest, Matrix_Submatrices)
{
    Matrix<3> A({Row<3>{6.0, 1.0, 1.0}, Row<3>{4.0, -2.0, 5.0}, Row<3>{2.0, 8.0, 7.0}});

    // gtest can't handle functions with more than one template parameter inside EXPECT_EQ, so we assign them outside:
    Matrix<2> subA11{Submatrix<1, 1>(A)};
    Matrix<2> subA12{Submatrix<1, 2>(A)};
    Matrix<2> subA13{Submatrix<1, 3>(A)};
    Matrix<2> subA21{Submatrix<2, 1>(A)};
    Matrix<2> subA22{Submatrix<2, 2>(A)};
    Matrix<2> subA23{Submatrix<2, 3>(A)};
    Matrix<2> subA31{Submatrix<3, 1>(A)};
    Matrix<2> subA32{Submatrix<3, 2>(A)};
    Matrix<2> subA33{Submatrix<3, 3>(A)};

    EXPECT_EQ(subA11, Matrix<2>({Row<2>{-2.0, 5.0}, Row<2>{8.0, 7.0}}));
    EXPECT_EQ(subA12, Matrix<2>({Row<2>{4.0, 5.0}, Row<2>{2.0, 7.0}}));
    EXPECT_EQ(subA13, Matrix<2>({Row<2>{4.0, -2.0}, Row<2>{2.0, 8.0}}));
    EXPECT_EQ(subA21, Matrix<2>({Row<2>{1.0, 1.0}, Row<2>{8.0, 7.0}}));
    EXPECT_EQ(subA22, Matrix<2>({Row<2>{6.0, 1.0}, Row<2>{2.0, 7.0}}));
    EXPECT_EQ(subA23, Matrix<2>({Row<2>{6.0, 1.0}, Row<2>{2.0, 8.0}}));
    EXPECT_EQ(subA31, Matrix<2>({Row<2>{1.0, 1.0}, Row<2>{-2.0, 5.0}}));
    EXPECT_EQ(subA32, Matrix<2>({Row<2>{6.0, 1.0}, Row<2>{4.0, 5.0}}));
    EXPECT_EQ(subA33, Matrix<2>({Row<2>{6.0, 1.0}, Row<2>{4.0, -2.0}}));
}

TEST(MathTest, Matrix_Determinant)
{
    Matrix<3> A({Row<3>{6.0, 1.0, 1.0}, Row<3>{4.0, -2.0, 5.0}, Row<3>{2.0, 8.0, 7.0}});
    EXPECT_EQ(Det(A), -306.0);
}

TEST(MathTest, Matrix_Cofactor_Matrix)
{
    Matrix<3> A({Row<3>{1.0, 2.0, 3.0}, Row<3>{0.0, 4.0, 5.0}, Row<3>{1.0, 0.0, 6.0}});

    EXPECT_EQ(Cof(A), Matrix<3>({Row<3>{24.0, 5.0, -4.0}, Row<3>{-12.0, 3.0, 2.0}, Row<3>{-2.0, -5.0, 4.0}}));
}

TEST(MathTest, Matrix_Transposed_Matrix)
{
    Matrix<3> A({Row<3>{1.0, 2.0, 3.0}, Row<3>{0.0, 4.0, 5.0}, Row<3>{1.0, 0.0, 6.0}});

    EXPECT_EQ(Transpose(A), Matrix<3>({Row<3>{1.0, 0.0, 1.0}, Row<3>{2.0, 4.0, 0.0}, Row<3>{3.0, 5.0, 6.0}}));
}

TEST(MathTest, Matrix_Inverted_Matrix)
{
    Matrix<3> A({Row<3>{1.0, 2.0, -1.0}, Row<3>{2.0, 1.0, 2.0}, Row<3>{-1.0, 2.0, 1.0}});

    EXPECT_EQ(Inv(A), Matrix<3>({Row<3>{3.0 / 16.0, 1.0 / 4.0, -5.0 / 16.0}, Row<3>{1.0 / 4.0, 0.0, 1.0 / 4.0}, Row<3>{-5.0 / 16.0, 1.0 / 4.0, 3.0 / 16.0}}));
}

TEST(MathTest, Vector_Translation_With_Matrix)
{
    Matrix<3> A{TranslationMatrix(XY{5.0, 3.0})};
    Matrix<3> B{};
    B.Translate({5.0, 3.0});

    EXPECT_EQ(A, B);
    EXPECT_EQ(A * XY(0.0, 0.0), XY(5.0, 3.0));
    EXPECT_EQ(A * XY(5.0, 3.0), XY(10.0, 6.0));
}

TEST(MathTest, Vector_Rotation2D_With_Matrix)
{
    // Rotate around origin
    Matrix<3> A{RotationMatrix(halfPi)};
    EXPECT_EQ(A * XY(1.0, 0.0), XY(0.0, 1.0));
    // Rotate around given point
    Matrix<3> B{RotationMatrix(halfPi, XY{1.0, 0.0})};
    EXPECT_EQ(B * XY(0.0, 0.0), XY(1.0, -1.0));
}

TEST(MathTest, Vector_Rotation3D_With_Matrix)
{
    // Rotate around x-axis
    Matrix<4> X{RotationMatrix(halfPi, XYZ{1.0, 0.0, 0.0})};

    EXPECT_EQ(X * XYZ(1.0, 0.0, 0.0), XYZ(1.0, 0.0, 0.0));
    EXPECT_EQ(X * XYZ(1.0, 1.0, 0.0), XYZ(1.0, 0.0, 1.0));

    // Rotate around y-axis
    Matrix<4> Y{RotationMatrix(halfPi, XYZ{0.0, 1.0, 0.0})};

    EXPECT_EQ(Y * XYZ(0.0, 1.0, 0.0), XYZ(0.0, 1.0, 0.0));
    EXPECT_EQ(Y * XYZ(1.0, 1.0, 0.0), XYZ(0.0, 1.0, -1.0));

    // Rotate around z-axis
    Matrix<4> Z{RotationMatrix(halfPi, XYZ{0.0, 0.0, 1.0})};

    EXPECT_EQ(Z * XYZ(0.0, 0.0, 1.0), XYZ(0.0, 0.0, 1.0));
    EXPECT_EQ(Z * XYZ(1.0, 1.0, 1.0), XYZ(-1.0, 1.0, 1.0));

    // Rotate around arbitrary axis (through origin)
    Matrix<4> A{RotationMatrix(pi, XYZ{1.0, 1.0, 1.0})};

    EXPECT_EQ(A * XYZ(1.0, 1.0, 1.0), XYZ(1.0, 1.0, 1.0));

    // Combined rotations
    const XYZ point{1.0, 0.0, 0.0};
    const Rotation rotation{0.0, 0.0, pi};

    EXPECT_EQ(RotationMatrix(rotation) * point, XYZ(-1.0, 0.0, 0.0));
    EXPECT_EQ(RotationMatrix(rotation * 0.5) * point, XYZ(0.0, 1.0, 0.0));
    EXPECT_EQ(RotationMatrix(rotation * 0.5) * point, XYZ(0.0, 1.0, 0.0));

    // FIXME: If a test fails, the print output of XYZ is incorrect. Why?
}

TEST(MathTest, Vector_Transformation_On_Flat_Surface_With_Matrix)
{
    const Matrix<4> velocity{TranslationMatrix(XYZ(4, 0, 0))};        // Moving forwards 5m/s
    const Matrix<3> rotation{RotationMatrix(Rotation{0, 0, halfPi})}; // facing y-axis horizon
    const Matrix<3> spin{RotationMatrix(Rotation{0, 0, halfPi})};     // Spinning to the left at 0.25 rps
    const Matrix<4> transformation{Matrix<4>(rotation) * velocity * Matrix<4>(spin)};
    // Object center:
    EXPECT_EQ(transformation * XYZ(0, 0, 0), XYZ(0, 4, 0));
    // Object corner:
    //  spin*(1, 1) = (-1, 1)
    //  velocity*(-1, 1) = (3, 1)
    //  rotation*(3, 1) = (-1, 3)
    EXPECT_EQ(transformation * XYZ(1, 1, 0), XYZ(-1, 3, 0));
}

TEST(MathTest, Vector_LineIntersections)
{
    // Overlapping lines have no intersection
    {
        auto overlappingIntersection{Line{{0.0, 0.0}, {0.0, 1.0}}.GetIntersection(Line{{0.0, 0.0}, {0.0, 1.0}})};
        EXPECT_FALSE(overlappingIntersection.has_value());
        overlappingIntersection = Line{{0.0, 0.0}, {0.0, -1.0}}.GetIntersection(Line{{0.0, 0.0}, {0.0, -1.0}});
        EXPECT_FALSE(overlappingIntersection.has_value());
        overlappingIntersection = Line{{0.0, 0.0}, {0.0, 1.0}}.GetIntersection(Line{{0.0, 1.0}, {0.0, 0.0}});
        EXPECT_FALSE(overlappingIntersection.has_value());
        overlappingIntersection = Line{{0.0, 0.0}, {0.0, -1.0}}.GetIntersection(Line{{0.0, -1.0}, {0.0, 0.0}});
        EXPECT_FALSE(overlappingIntersection.has_value());
    }
    // Parallel lines have no intersection
    {
        auto parallelIntersection{Line{{0.0, 0.0}, {0.0, 1.0}}.GetIntersection({{1.0, 0.0}, {1.0, 1.0}})};
        EXPECT_FALSE(parallelIntersection.has_value());
    }
    // Normal intersection:
    {
        auto intersection{Line{XY{-4.0, 0.0}, XY{0.0, 3.0}}.GetIntersection(Line{XY{2.0, 0.0}, XY{2.0, 1.0}})};
        ASSERT_TRUE(intersection.has_value());
        const XY expectedResult{2.0, 4.5};
        EXPECT_EQ(intersection.value(), expectedResult);
    }
}

TEST(MathTest, Vector_HasIntersection_NotIntersecting_Returns_False)
{
    const Shape a{XY{1.0, 1.0}, {1.0, 2.0}, {0.0, 2.0}, {0.0, 1.0}};
    const Shape b{XY{1.0, -1.0}, {1.0, 0.0}, {0.0, 0.0}, {0.0, -1.0}};
    EXPECT_FALSE(osiql::detail::HasIntersections(a, b));
    EXPECT_FALSE(osiql::detail::HasIntersections(b, a));
}

TEST(MathTest, Vector_IsWithin_Octagon_PolygonVertex_Returns_True)
{
    const std::array<osiql::XY, 8> octagon{
        osiql::XY{0, 1},
        osiql::XY{0, 2},
        osiql::XY{1, 3},
        osiql::XY{2, 3},
        osiql::XY{3, 2},
        osiql::XY{3, 1},
        osiql::XY{2, 0},
        osiql::XY{1, 0},
    };

    EXPECT_TRUE(octagon[0].IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE(octagon[1].IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE(octagon[2].IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE(octagon[3].IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE(octagon[4].IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE(octagon[5].IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE(octagon[6].IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE(octagon[7].IsWithin(octagon.begin(), octagon.end()));

    EXPECT_TRUE(octagon[0].IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE(octagon[1].IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE(octagon[2].IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE(octagon[3].IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE(octagon[4].IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE(octagon[5].IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE(octagon[6].IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE(octagon[7].IsWithin(octagon.rbegin(), octagon.rend()));
}

TEST(MathTest, Vector_IsWithin_Octagon_OnEdge_Returns_True)
{
    const std::array<osiql::XY, 8> octagon{
        osiql::XY{0, 1},
        osiql::XY{0, 2},
        osiql::XY{1, 3},
        osiql::XY{2, 3},
        osiql::XY{3, 2},
        osiql::XY{3, 1},
        osiql::XY{2, 0},
        osiql::XY{1, 0},
    };

    EXPECT_TRUE((octagon[0] * 0.5 + octagon[1] * 0.5).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE((octagon[1] * 0.5 + octagon[2] * 0.5).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE((octagon[2] * 0.5 + octagon[3] * 0.5).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE((octagon[3] * 0.5 + octagon[4] * 0.5).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE((octagon[4] * 0.5 + octagon[5] * 0.5).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE((octagon[5] * 0.5 + octagon[6] * 0.5).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE((octagon[6] * 0.5 + octagon[7] * 0.5).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE((octagon[7] * 0.5 + octagon[0] * 0.5).IsWithin(octagon.begin(), octagon.end()));

    EXPECT_TRUE((octagon[0] * 0.5 + octagon[1] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE((octagon[1] * 0.5 + octagon[2] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE((octagon[2] * 0.5 + octagon[3] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE((octagon[3] * 0.5 + octagon[4] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE((octagon[4] * 0.5 + octagon[5] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE((octagon[5] * 0.5 + octagon[6] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE((octagon[6] * 0.5 + octagon[7] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE((octagon[7] * 0.5 + octagon[0] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
}

TEST(MathTest, Vector_IsWithin_Octagon_Inside_Returns_True)
{
    const std::array<osiql::XY, 8> octagon{
        osiql::XY{0, 1},
        osiql::XY{0, 2},
        osiql::XY{1, 3},
        osiql::XY{2, 3},
        osiql::XY{3, 2},
        osiql::XY{3, 1},
        osiql::XY{2, 0},
        osiql::XY{1, 0},
    };

    EXPECT_TRUE((octagon[0] * 0.5 + octagon[2] * 0.5).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE((octagon[1] * 0.5 + octagon[3] * 0.5).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE((octagon[2] * 0.5 + octagon[4] * 0.5).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE((octagon[3] * 0.5 + octagon[5] * 0.5).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE((octagon[4] * 0.5 + octagon[6] * 0.5).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE((octagon[5] * 0.5 + octagon[7] * 0.5).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE((octagon[6] * 0.5 + octagon[0] * 0.5).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_TRUE((octagon[7] * 0.5 + octagon[1] * 0.5).IsWithin(octagon.begin(), octagon.end()));

    EXPECT_TRUE((octagon[0] * 0.5 + octagon[2] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE((octagon[1] * 0.5 + octagon[3] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE((octagon[2] * 0.5 + octagon[4] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE((octagon[3] * 0.5 + octagon[5] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE((octagon[4] * 0.5 + octagon[6] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE((octagon[5] * 0.5 + octagon[7] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE((octagon[6] * 0.5 + octagon[0] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_TRUE((octagon[7] * 0.5 + octagon[1] * 0.5).IsWithin(octagon.rbegin(), octagon.rend()));
}

TEST(MathTest, Vector_IsWithin_Octagon_Outside_Returns_False)
{
    const std::array<osiql::XY, 8> octagon{
        osiql::XY{0, 1},
        osiql::XY{0, 2},
        osiql::XY{1, 3},
        osiql::XY{2, 3},
        osiql::XY{3, 2},
        osiql::XY{3, 1},
        osiql::XY{2, 0},
        osiql::XY{1, 0},
    };

    EXPECT_FALSE(osiql::XY(0, 0).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_FALSE(osiql::XY(3, 0).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_FALSE(osiql::XY(3, 3).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_FALSE(osiql::XY(0, 3).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_FALSE((octagon[0] + osiql::XY(4, 0)).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_FALSE((octagon[1] + osiql::XY(4, 0)).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_FALSE((octagon[2] + osiql::XY(4, 0)).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_FALSE((octagon[3] + osiql::XY(4, 0)).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_FALSE((octagon[4] + osiql::XY(4, 0)).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_FALSE((octagon[5] + osiql::XY(4, 0)).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_FALSE((octagon[6] + osiql::XY(4, 0)).IsWithin(octagon.begin(), octagon.end()));
    EXPECT_FALSE((octagon[7] + osiql::XY(4, 0)).IsWithin(octagon.begin(), octagon.end()));

    EXPECT_FALSE(osiql::XY(0, 0).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_FALSE(osiql::XY(3, 0).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_FALSE(osiql::XY(3, 3).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_FALSE(osiql::XY(0, 3).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_FALSE((octagon[0] + osiql::XY(4, 0)).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_FALSE((octagon[1] + osiql::XY(4, 0)).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_FALSE((octagon[2] + osiql::XY(4, 0)).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_FALSE((octagon[3] + osiql::XY(4, 0)).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_FALSE((octagon[4] + osiql::XY(4, 0)).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_FALSE((octagon[5] + osiql::XY(4, 0)).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_FALSE((octagon[6] + osiql::XY(4, 0)).IsWithin(octagon.rbegin(), octagon.rend()));
    EXPECT_FALSE((octagon[7] + osiql::XY(4, 0)).IsWithin(octagon.rbegin(), octagon.rend()));
}
