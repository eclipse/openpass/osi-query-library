/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "util/Common.tpp"

using namespace osiql;

struct Roads : ::testing::Test
{
    Roads()
    {
        roads.push_back(groundTruth.AddRoad(
            groundTruth.AddReferenceLine(XY{0, 0}, XY{10, 0}),
            {
                &groundTruth.AddBoundary(XY{0, 3}, XY{10, 3}),
                &groundTruth.AddBoundary(XY{0, 0}, XY{10, 0}),
                &groundTruth.AddBoundary(XY{0, -3}, XY{10, -3}),
            }
        ));
        roads.push_back(groundTruth.AddRoad(
            groundTruth.AddReferenceLine(XY{10, 0}, XY{20, 0}),
            {
                &groundTruth.AddBoundary(XY{10, 3}, XY{20, 3}),
                &groundTruth.AddBoundary(XY{10, 0}, XY{20, 0}),
                &groundTruth.AddBoundary(XY{10, -3}, XY{20, -3}),
            }
        ));
        test::ConnectRoads(roads[0].begin(), roads[0].end(), roads[1].begin(), roads[1].end());
        roads.push_back(groundTruth.AddRoad(
            groundTruth.AddReferenceLine(XY{20, 0}, XY{30, 0}),
            {
                &groundTruth.AddBoundary(XY{20, 3}, XY{30, 3}),
                &groundTruth.AddBoundary(XY{20, 0}, XY{30, 0}),
                &groundTruth.AddBoundary(XY{20, -3}, XY{30, -3}),
            }
        ));
        test::ConnectRoads(roads[1].begin(), roads[1].end(), roads[2].begin(), roads[2].end());
    }

    test::GroundTruth groundTruth;
    std::vector<std::vector<osi3::LogicalLane *>> roads;
};

// This test creates a route over a chain of three lanes
//   -5m     0m     5m           15m    20m    25m
//    .-----------------------------------------.
//    |             |             |             |
// .-----.       .-----.       .-----.       .-----.
// |     |   °   |     |       |     |   °   |     | <- Moving objects
// '-----'       '-----'       '-----'       '-----'
//    |             |             |             |
//    '-----------------------------------------'
//           ^                           ^
//          origin/destination of the route
struct StraightRoute : Roads
{
    StraightRoute()
    {
        // Object starting behind the first road, overlapping with it partially
        objects.push_back(&groundTruth.Add<MovingObject>(XY{0, -1.5}, 2, 1));
        // Object overlapping with the first and second road
        objects.push_back(&groundTruth.Add<MovingObject>(XY{10, -1.5}, 2, 1));
        // Object overlapping with the second and last road
        objects.push_back(&groundTruth.Add<MovingObject>(XY{20, -1.5}, 2, 1));
        // Object ending behind the last road, overlapping with it partially
        objects.push_back(&groundTruth.Add<MovingObject>(XY{30, -1.5}, 2, 1));

        world = std::make_unique<World>(groundTruth.handle);
    }

    std::vector<osi3::MovingObject *> objects;
    std::unique_ptr<World> world;
};

TEST_F(StraightRoute, FindBetween)
{
    const auto route{world->GetRoute(XY{5, -1.5}, XY{25, -1.5})};
    ASSERT_TRUE(route.has_value());
    ASSERT_THAT(*route, SizeIs(3));
    const auto &object{*world->GetMovingObject(get<Id>(objects[0]))};
    {
        const auto location{route->template FindBetween<MovingObject, Traversal::Forward>(1, {-10, 20})};
        EXPECT_THAT(location, SizeIs(1));
    }
    {
        const auto location{route->template FindBetween<MovingObject, Traversal::Forward, Selection::First>(
            1, {-10, 20}, NotEqual<MovingObject>::To<const MovingObject &>{object}
        )};
        EXPECT_THAT(location, SizeIs(1));
    }
}

TEST_F(StraightRoute, FindAll_MovingObjects_EachOverlap_SearchingForward_OnForwardRoute)
{
    auto routeOptional{world->GetRoute(XY{5, -1.5}, XY{25, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings{route.FindAll<MovingObject, Traversal::Forward, Selection::Each>()};
    EXPECT_THAT(findings, SizeIs(6));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Each>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(5));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Forward, Selection::Each>({0, 20});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));
}

TEST_F(StraightRoute, FindAll_MovingObjects_FirstOverlap_SearchingForward_OnForwardRoute)
{
    auto routeOptional{world->GetRoute(XY{5, -1.5}, XY{25, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings = route.FindAll<MovingObject, Traversal::Forward, Selection::First>();
    EXPECT_THAT(findings, SizeIs(4));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Forward, Selection::First>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Forward, Selection::First>({0, 20});
    EXPECT_THAT(findings, SizeIs(2));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));
}

TEST_F(StraightRoute, DISABLED_FindAll_MovingObjects_LastOverlap_SearchingForward_OnForwardRoute)
{
    auto routeOptional{world->GetRoute(XY{5, -1.5}, XY{25, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    // auto findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Last>();
    // EXPECT_THAT(findings, SizeIs(4));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    // findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Last>(Greater<Distance>::Than<>{0});
    // EXPECT_THAT(findings, SizeIs(3));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    // findings = route.FindAllBetween<MovingObject, Traversal::Forward, Selection::Last>({0, 20});
    // EXPECT_THAT(findings, SizeIs(2));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));
}

TEST_F(StraightRoute, FindAll_MovingObjects_EachOverlap_SearchingForward_OnBackwardRoute)
{
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{25, -1.5}, XY{5, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings{route.FindAll<MovingObject, Traversal::Forward, Selection::Each>()};
    EXPECT_THAT(findings, SizeIs(6));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Each>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(5));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Forward, Selection::Each>({0, 20});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));
}

TEST_F(StraightRoute, FindAll_MovingObjects_FirstOverlap_SearchingForward_OnBackwardRoute)
{
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{25, -1.5}, XY{5, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings = route.FindAll<MovingObject, Traversal::Forward, Selection::First>();
    EXPECT_THAT(findings, SizeIs(4));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Forward, Selection::First>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Forward, Selection::First>({0, 20});
    EXPECT_THAT(findings, SizeIs(2));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));
}

TEST_F(StraightRoute, DISABLED_FindAll_MovingObjects_LastOverlap_SearchingForward_OnBackwardRoute)
{
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{25, -1.5}, XY{5, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    // auto findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Last>();
    // EXPECT_THAT(findings, SizeIs(4));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    // findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Last>(Greater<Distance>::Than<>{0});
    // EXPECT_THAT(findings, SizeIs(3));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    // findings = route.FindAllBetween<MovingObject, Traversal::Forward, Selection::Last>({0, 20});
    // EXPECT_THAT(findings, SizeIs(2));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));
}

// Searching backwards

TEST_F(StraightRoute, FindAll_MovingObjects_EachOverlap_SearchingBackward_OnForwardRoute)
{
    auto routeOptional{world->GetRoute(XY{5, -1.5}, XY{25, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings{route.FindAll<MovingObject, Traversal::Backward, Selection::Each>()};
    EXPECT_THAT(findings, SizeIs(6));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Backward, Selection::Each>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(5));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Backward, Selection::Each>({0, 20});
    EXPECT_THAT(findings, SizeIs(4));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));
}

TEST_F(StraightRoute, FindAll_MovingObjects_FirstOverlap_SearchingBackward_OnForwardRoute)
{
    auto routeOptional{world->GetRoute(XY{5, -1.5}, XY{25, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings = route.FindAll<MovingObject, Traversal::Backward, Selection::First>();
    EXPECT_THAT(findings, SizeIs(4));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Backward, Selection::First>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Backward, Selection::First>({0, 20});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));
}

TEST_F(StraightRoute, DISABLED_FindAll_MovingObjects_LastOverlap_SearchingBackward_OnForwardRoute)
{
    auto routeOptional{world->GetRoute(XY{5, -1.5}, XY{25, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    // auto findings = route.FindAll<MovingObject, Traversal::Backward, Selection::Last>();
    // EXPECT_THAT(findings, SizeIs(4));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    // findings = route.FindAll<MovingObject, Traversal::Backward, Selection::Last>(Greater<Distance>::Than<>{0});
    // EXPECT_THAT(findings, SizeIs(3));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    // findings = route.FindAllBetween<MovingObject, Traversal::Backward, Selection::Last>({0, 20});
    // EXPECT_THAT(findings, SizeIs(2));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));
}

TEST_F(StraightRoute, FindAll_MovingObjects_EachOverlap_SearchingBackward_OnBackwardRoute)
{
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{25, -1.5}, XY{5, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings{route.FindAll<MovingObject, Traversal::Backward, Selection::Each>()};
    EXPECT_THAT(findings, SizeIs(6));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Backward, Selection::Each>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(5));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Backward, Selection::Each>({0, 20});
    EXPECT_THAT(findings, SizeIs(4));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));
}

TEST_F(StraightRoute, FindAll_MovingObjects_FirstOverlap_SearchingBackward_OnBackwardRoute)
{
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{25, -1.5}, XY{5, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings = route.FindAll<MovingObject, Traversal::Backward, Selection::First>();
    EXPECT_THAT(findings, SizeIs(4));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Backward, Selection::First>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Backward, Selection::First>({0, 20});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));
}

TEST_F(StraightRoute, DISABLED_FindAll_MovingObjects_LastOverlap_SearchingBackward_OnBackwardRoute)
{
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{25, -1.5}, XY{5, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    // auto findings = route.FindAll<MovingObject, Traversal::Backward, Selection::Last>();
    // EXPECT_THAT(findings, SizeIs(4));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    // findings = route.FindAll<MovingObject, Traversal::Backward, Selection::Last>(Greater<Distance>::Than<>{0});
    // EXPECT_THAT(findings, SizeIs(3));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    // findings = route.FindAllBetween<MovingObject, Traversal::Backward, Selection::Last>({0, 20});
    // EXPECT_THAT(findings, SizeIs(2));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));
}

struct StraightRouteUpstream : Roads
{
    StraightRouteUpstream()
    {
        // Object starting behind the first road, overlapping with it partially
        objects.push_back(&groundTruth.Add<MovingObject>(XY{0, 1.5}, 2, 1));
        // Object overlapping with the first and second road
        objects.push_back(&groundTruth.Add<MovingObject>(XY{10, 1.5}, 2, 1));
        // Object overlapping with the second and last road
        objects.push_back(&groundTruth.Add<MovingObject>(XY{20, 1.5}, 2, 1));
        // Object ending behind the last road, overlapping with it partially
        objects.push_back(&groundTruth.Add<MovingObject>(XY{30, 1.5}, 2, 1));

        world = std::make_unique<World>(groundTruth.handle);
    }

    std::vector<osi3::MovingObject *> objects;
    std::unique_ptr<World> world;
};

TEST_F(StraightRouteUpstream, FindAll_MovingObjects_EachOverlap_SearchingForward_OnForwardRoute)
{
    auto routeOptional{world->GetRoute(XY{25, 1.5}, XY{5, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings{route.FindAll<MovingObject, Traversal::Forward, Selection::Each>()};
    EXPECT_THAT(findings, SizeIs(6));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Each>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(5));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Forward, Selection::Each>({0, 20});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));
}

TEST_F(StraightRouteUpstream, FindAll_MovingObjects_FirstOverlap_SearchingForward_OnForwardRoute)
{
    auto routeOptional{world->GetRoute(XY{25, 1.5}, XY{5, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings = route.FindAll<MovingObject, Traversal::Forward, Selection::First>();
    EXPECT_THAT(findings, SizeIs(4));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Forward, Selection::First>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Forward, Selection::First>({0, 20});
    EXPECT_THAT(findings, SizeIs(2));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));
}

TEST_F(StraightRouteUpstream, DISABLED_FindAll_MovingObjects_LastOverlap_SearchingForward_OnForwardRoute)
{
    auto routeOptional{world->GetRoute(XY{25, 1.5}, XY{5, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    // auto findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Last>();
    // EXPECT_THAT(findings, SizeIs(4));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    // findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Last>(Greater<Distance>::Than<>{0});
    // EXPECT_THAT(findings, SizeIs(3));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    // findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Last>({0, 20});
    // EXPECT_THAT(findings, SizeIs(2));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));
}

TEST_F(StraightRouteUpstream, FindAll_MovingObjects_EachOverlap_SearchingForward_OnBackwardRoute)
{
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{5, 1.5}, XY{25, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings{route.FindAll<MovingObject, Traversal::Forward, Selection::Each>()};
    EXPECT_THAT(findings, SizeIs(6));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Each>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(5));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Forward, Selection::Each>({0, 20});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));
}

TEST_F(StraightRouteUpstream, FindAll_MovingObjects_FirstOverlap_SearchingForward_OnBackwardRoute)
{
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{5, 1.5}, XY{25, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings = route.FindAll<MovingObject, Traversal::Forward, Selection::First>();
    EXPECT_THAT(findings, SizeIs(4));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Forward, Selection::First>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Forward, Selection::First>({0, 20});
    EXPECT_THAT(findings, SizeIs(2));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));
}

TEST_F(StraightRouteUpstream, DISABLED_FindAll_MovingObjects_LastOverlap_SearchingForward_OnBackwardRoute)
{
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{5, 1.5}, XY{25, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    // auto findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Last>();
    // EXPECT_THAT(findings, SizeIs(4));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    // findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Last>(Greater<Distance>::Than<>{0});
    // EXPECT_THAT(findings, SizeIs(3));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    // findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Last>({0, 20});
    // EXPECT_THAT(findings, SizeIs(2));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));
}

// Searching backwards

TEST_F(StraightRouteUpstream, FindAll_MovingObjects_EachOverlap_SearchingBackward_OnForwardRoute)
{
    auto routeOptional{world->GetRoute(XY{25, 1.5}, XY{5, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings{route.FindAll<MovingObject, Traversal::Backward, Selection::Each>()};
    EXPECT_THAT(findings, SizeIs(6));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Backward, Selection::Each>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(5));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Backward, Selection::Each>({0, 20});
    EXPECT_THAT(findings, SizeIs(4));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));
}

TEST_F(StraightRouteUpstream, FindAll_MovingObjects_FirstOverlap_SearchingBackward_OnForwardRoute)
{
    auto routeOptional{world->GetRoute(XY{25, 1.5}, XY{5, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings = route.FindAll<MovingObject, Traversal::Backward, Selection::First>();
    EXPECT_THAT(findings, SizeIs(4));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Backward, Selection::First>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Backward, Selection::First>({0, 20});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));
}

TEST_F(StraightRouteUpstream, DISABLED_FindAll_MovingObjects_LastOverlap_SearchingBackward_OnForwardRoute)
{
    auto routeOptional{world->GetRoute(XY{25, 1.5}, XY{5, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    // auto findings = route.FindAll<MovingObject, Traversal::Backward, Selection::Last>();
    // EXPECT_THAT(findings, SizeIs(4));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    // findings = route.FindAll<MovingObject, Traversal::Backward, Selection::Last>(Greater<Distance>::Than<>{0});
    // EXPECT_THAT(findings, SizeIs(3));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    // auto findings = route.FindAllBetween<MovingObject, Traversal::Backward, Selection::Last>({0, 20});
    // EXPECT_THAT(findings, SizeIs(2));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));
}

TEST_F(StraightRouteUpstream, FindAll_MovingObjects_EachOverlap_SearchingBackward_OnBackwardRoute)
{
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{5, 1.5}, XY{25, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings{route.FindAll<MovingObject, Traversal::Backward, Selection::Each>()};
    EXPECT_THAT(findings, SizeIs(6));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Backward, Selection::Each>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(5));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Backward, Selection::Each>({0, 20});
    EXPECT_THAT(findings, SizeIs(4));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));
}

TEST_F(StraightRouteUpstream, FindAll_MovingObjects_FirstOverlap_SearchingBackward_OnBackwardRoute)
{
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{5, 1.5}, XY{25, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings = route.FindAll<MovingObject, Traversal::Backward, Selection::First>();
    EXPECT_THAT(findings, SizeIs(4));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Backward, Selection::First>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Backward, Selection::First>({0, 20});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));
}

TEST_F(StraightRouteUpstream, DISABLED_FindAll_MovingObjects_LastOverlap_SearchingBackward_OnBackwardRoute)
{
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{5, 1.5}, XY{25, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    // auto findings = route.FindAll<MovingObject, Traversal::Backward, Selection::Last>();
    // EXPECT_THAT(findings, SizeIs(4));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));
    // findings = route.FindAll<MovingObject, Traversal::Backward, Selection::Last>(Greater<Distance>::Than<>{0});
    // EXPECT_THAT(findings, SizeIs(3));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));

    // findings = route.FindAllBetween<MovingObject, Traversal::Backward, Selection::Last>({0, 20});
    // EXPECT_THAT(findings, SizeIs(2));
    // EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Greater<Distance>{}));
}

struct StraightRouteCornerCases : Roads
{
    StraightRouteCornerCases()
    {
        // Object center starting behind the first road
        objects.push_back(&groundTruth.Add<MovingObject>(XY{-0.75, -1}, 2, 1));
        // Object is in the corner of the first road
        objects.push_back(&groundTruth.Add<MovingObject>(XY{0, -3.45}, 2, 1));
        // Object touches the boundary from outside
        objects.push_back(&groundTruth.Add<MovingObject>(XY{10, -3.5}, 2, 1));
        // Object center is slightly outside the boundary
        objects.push_back(&groundTruth.Add<MovingObject>(XY{15, -3.1}, 2, 1));
        // Object is on the boundary
        objects.push_back(&groundTruth.Add<MovingObject>(XY{20, -3}, 2, 1));
        // Object touches the road corner with object's corner
        objects.push_back(&groundTruth.Add<MovingObject>(XY{30.9, -3.4}, 2, 1));

        world = std::make_unique<World>(groundTruth.handle);
    }

    std::vector<osi3::MovingObject *> objects;
    std::unique_ptr<World> world;
};

TEST_F(StraightRouteCornerCases, FindAll_MovingObjects_EachOverlap_SearchingForward_OnForwardRoute)
{
    auto routeOptional{world->GetRoute(XY{5, -1.5}, XY{25, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings{route.FindAll<MovingObject, Traversal::Forward, Selection::Each>()};
    EXPECT_THAT(findings, SizeIs(6));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Each>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(4));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Forward, Selection::Each>({0, 20});
    EXPECT_THAT(findings, SizeIs(3));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));
}

struct StraightRouteCornerCasesLeft : Roads
{
    StraightRouteCornerCasesLeft()
    {
        // Object center starting behind the first road
        objects.push_back(&groundTruth.Add<MovingObject>(XY{-0.75, 1}, 2, 1));
        // Object is in the corner of the first road
        objects.push_back(&groundTruth.Add<MovingObject>(XY{0, 3.45}, 2, 1));
        // Object touches the boundary from outside
        objects.push_back(&groundTruth.Add<MovingObject>(XY{10, 3.5}, 2, 1));
        // Object center is slightly outside the boundary
        objects.push_back(&groundTruth.Add<MovingObject>(XY{15, 3.1}, 2, 1));
        // Object is on the boundary
        objects.push_back(&groundTruth.Add<MovingObject>(XY{20, 3}, 2, 1));
        // Object touches the road corner with object's corner
        objects.push_back(&groundTruth.Add<MovingObject>(XY{30.9, 3.4}, 2, 1));

        world = std::make_unique<World>(groundTruth.handle);
    }

    std::vector<osi3::MovingObject *> objects;
    std::unique_ptr<World> world;
};

TEST_F(StraightRouteCornerCasesLeft, FindAll_MovingObjects_EachOverlap_SearchingForward_OnForwardRoute)
{
    auto routeOptional{world->GetRoute(XY{25, 1.5}, XY{5, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto findings{route.FindAll<MovingObject, Traversal::Forward, Selection::Each>()};
    EXPECT_THAT(findings, SizeIs(6));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAll<MovingObject, Traversal::Forward, Selection::Each>(Greater<Distance>::Than<>{0});
    EXPECT_THAT(findings, SizeIs(5));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));

    findings = route.FindAllBetween<MovingObject, Traversal::Forward, Selection::Each>({0, 20});
    EXPECT_THAT(findings, SizeIs(2));
    EXPECT_TRUE(std::is_sorted(findings.begin(), findings.end(), Less<Distance>{}));
}

struct StraightRouteFindClosestNode : Roads
{
    StraightRouteFindClosestNode()
    {
        world = std::make_unique<World>(groundTruth.handle);
    }

    std::unique_ptr<World> world;
};

TEST_F(StraightRouteFindClosestNode, FindClosestNode_OnForwarRoute_CompareClosestToActualNodes)
{
    // Distances of each start of the road part: -5, 5, 10, 15
    auto routeOptional{world->GetRoute(XY{5, -1.5}, XY{25, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    // Compare the pointers
    EXPECT_EQ(*route.FindClosestNode(-6.0), route[0]);
    EXPECT_EQ(*route.FindClosestNode(-5.0), route[0]);
    EXPECT_EQ(*route.FindClosestNode(0.0), route[0]);
    EXPECT_EQ(*route.FindClosestNode(4.9), route[0]);
    // test between first and second nodes
    EXPECT_EQ(*route.FindClosestNode(5.0), route[1]);
    EXPECT_EQ(*route.FindClosestNode(14.9), route[1]);
    // third and behind
    EXPECT_EQ(*route.FindClosestNode(15.0), route[2]);
    EXPECT_EQ(*route.FindClosestNode(20), route[2]);
    // behind the route
    EXPECT_EQ(*route.FindClosestNode(28), route[2]);
    // behind the end of the road
    EXPECT_EQ(*route.FindClosestNode(35), route[2]);
}

TEST_F(StraightRouteFindClosestNode, FindClosestNode_OnForwarRoute_CompareClosestToActualNodesLeft)
{
    // Reverse of the route
    auto routeOptional{world->GetRoute(XY{25, 1.5}, XY{5, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    // Same tests as previous
    EXPECT_EQ(*route.FindClosestNode(-6.0), route[0]);
    EXPECT_EQ(*route.FindClosestNode(-5.0), route[0]);
    EXPECT_EQ(*route.FindClosestNode(0.0), route[0]);
    EXPECT_EQ(*route.FindClosestNode(4.9), route[0]);
    EXPECT_EQ(*route.FindClosestNode(35), route[2]);
}

TEST_F(StraightRouteFindClosestNode, FindClosestNode_OnForwarRoute_CompareClosestToActualNodes_OnBackwardRoute)
{
    // Backward traversal of the route
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{5, 1.5}, XY{25, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    // Same tests as previous
    EXPECT_EQ(*route.FindClosestNode(-6.0), route[0]);
    EXPECT_EQ(*route.FindClosestNode(-5.0), route[0]);
    EXPECT_EQ(*route.FindClosestNode(0.0), route[0]);
    EXPECT_EQ(*route.FindClosestNode(4.9), route[0]);
    EXPECT_EQ(*route.FindClosestNode(35), route[2]);
}

class StraightRouteFindClosestNodeP : public Roads, public ::testing::WithParamInterface<std::tuple<std::string, XY, size_t>>
{
};

TEST_P(StraightRouteFindClosestNodeP, OnForwardRoute_CompareClosestToActualNodes_GlobalPoint)
{
    auto [test_case, globalPoint, index] = GetParam();
    auto world = std::make_unique<World>(groundTruth.handle);
    auto routeOptional{world->GetRoute(XY{5, -1.5}, XY{25, -1.5})};

    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    EXPECT_EQ(*route.FindClosestNode(globalPoint), route[index]);
}

TEST_P(StraightRouteFindClosestNodeP, OnForwardRoute_CompareClosestToActualNodes_GlobalPoint_Backward)
{
    auto [test_case, globalPoint, index] = GetParam();
    auto world = std::make_unique<World>(groundTruth.handle);
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{5, 1.5}, XY{25, 1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    EXPECT_EQ(*route.FindClosestNode(globalPoint), route[index]);
}

INSTANTIATE_TEST_SUITE_P(
    StraightRouteTests,
    StraightRouteFindClosestNodeP,
    ::testing::Values(
        // Test bottom, middle and top positions
        std::make_tuple("Left from the route, bottom", XY{-10, -10}, 0),
        std::make_tuple("Left from the route, middle", XY{-10, 0}, 0),
        std::make_tuple("Left from the route, top", XY{-10, 10}, 0),

        std::make_tuple("On the road and left from the first node, bottom", XY{9, -2}, 0),
        std::make_tuple("On the road and left from the first node, bottom", XY{9, 0}, 0),
        std::make_tuple("On the road and left from the first node, top", XY{9, 2}, 0),
        std::make_tuple("Outside the road and left from the first node, bottom", XY{9, -10}, 0),
        std::make_tuple("Outside the road and left from the first node, top", XY{9, 10}, 0),

        std::make_tuple("On the first node, bottom", XY{10, -2}, 1),
        std::make_tuple("On the first node, middle", XY{10, 0}, 1),
        std::make_tuple("On the first node, top", XY{10, 2}, 1),
        std::make_tuple("On the first node, bottom", XY{10, -10}, 1),
        std::make_tuple("On the first node, top", XY{10, 10}, 1),

        std::make_tuple("Near the end of the second, bottom", XY{19, -2}, 1),
        std::make_tuple("Near the end of the second, middle", XY{19, 0}, 1),
        std::make_tuple("Near the end of the second, top", XY{19, 2}, 1),
        std::make_tuple("Near the end of the second, bottom", XY{19, -10}, 1),
        std::make_tuple("Near the end of the second, top", XY{19, 10}, 1),

        std::make_tuple("Behind the second node, bottom", XY{23, -2}, 2),
        std::make_tuple("Behind the second node, middle", XY{23, 0}, 2),
        std::make_tuple("Behind the second node, top", XY{23, 2}, 2),
        std::make_tuple("Behind the second node, bottom", XY{23, -10}, 2),
        std::make_tuple("Behind the second node, top", XY{23, 10}, 2),

        std::make_tuple("End of the route, bottom", XY{25, -2}, 2),
        std::make_tuple("End of the route, middle", XY{25, 0}, 2),
        std::make_tuple("End of the route, top", XY{25, 2}, 2),
        std::make_tuple("End of the route, bottom", XY{25, -10}, 2),
        std::make_tuple("End of the route, top", XY{25, 10}, 2),

        std::make_tuple("Behind of the route, bottom", XY{25, -2}, 2),
        std::make_tuple("Behind of the route, middle", XY{25, 0}, 2),
        std::make_tuple("Behind of the route, top", XY{25, 2}, 2),
        std::make_tuple("Behind of the route, bottom", XY{25, -10}, 2),
        std::make_tuple("Behind of the route, top", XY{25, 10}, 2)
    )
);

struct StraightRouteGetDistance : Roads
{
    StraightRouteGetDistance()
    {
        // Object starting behind the first road, overlapping with it partially
        objects.push_back(&groundTruth.Add<MovingObject>(XY{0, -1.5}, 2, 1));
        // Object overlapping with the first and second road
        objects.push_back(&groundTruth.Add<MovingObject>(XY{10, -1.5}, 2, 1));
        // Object overlapping with the last road
        objects.push_back(&groundTruth.Add<MovingObject>(XY{21, -3}, 2, 1));
        // Object ending behind the last road, overlapping with it partially
        objects.push_back(&groundTruth.Add<MovingObject>(XY{30, 3}, 2, 1));
        // Objects outside of the road
        objects.push_back(&groundTruth.Add<MovingObject>(XY{-1.1, -1.5}, 2, 1));
        objects.push_back(&groundTruth.Add<MovingObject>(XY{31.1, -1.5}, 2, 1));
        objects.push_back(&groundTruth.Add<MovingObject>(XY{10, 3.6}, 2, 1));
        objects.push_back(&groundTruth.Add<MovingObject>(XY{10, -3.6}, 2, 1));

        world = std::make_unique<World>(groundTruth.handle);
    }

    std::vector<osi3::MovingObject *> objects;
    std::unique_ptr<World> world;
};

TEST_F(StraightRouteGetDistance, GetDistance_Point)
{
    auto routeOptional{world->GetRoute(XY{5, -1.5}, XY{25, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    // Point coordinates are relative to a specific road

    // Inside roads
    EXPECT_DOUBLE_EQ(route.GetDistance(Point<>{{0, 0}, *world->GetLane(get<Id>(roads[0].front()))}), -5);
    EXPECT_DOUBLE_EQ(route.GetDistance(Point<>{{5, 0}, *world->GetLane(get<Id>(roads[0].front()))}), 0);
    EXPECT_DOUBLE_EQ(route.GetDistance(Point<>{{10, 0}, *world->GetLane(get<Id>(roads[0].front()))}), 5);

    EXPECT_DOUBLE_EQ(route.GetDistance(Point<>{{0, 0}, *world->GetLane(get<Id>(roads[1].front()))}), 5);
    EXPECT_DOUBLE_EQ(route.GetDistance(Point<>{{5, 0}, *world->GetLane(get<Id>(roads[1].front()))}), 10);
    EXPECT_DOUBLE_EQ(route.GetDistance(Point<>{{10, 0}, *world->GetLane(get<Id>(roads[1].front()))}), 15);

    EXPECT_DOUBLE_EQ(route.GetDistance(Point<>{{0, 0}, *world->GetLane(get<Id>(roads[2].front()))}), 15);
    EXPECT_DOUBLE_EQ(route.GetDistance(Point<>{{5, 0}, *world->GetLane(get<Id>(roads[2].front()))}), 20);
    EXPECT_DOUBLE_EQ(route.GetDistance(Point<>{{10, 0}, *world->GetLane(get<Id>(roads[2].front()))}), 25);

    auto routeOptionalShort{world->GetRoute(XY{5, -1.5}, XY{15, -1.5})};
    ASSERT_TRUE(routeOptionalShort.has_value());
    auto &routeShort{routeOptionalShort.value()};
    ASSERT_THAT(routeShort, SizeIs(2));

    // Test on the road that is not the part the path
    EXPECT_TRUE(isnan(routeShort.GetDistance(Point<>{{0, 0}, *world->GetLane(get<Id>(roads[2].front()))})));
}

bool IntervalIsNaN(Interval<double> interval)
{
    return isnan(interval.begin()) && isnan(interval.end());
}

TEST_F(StraightRouteGetDistance, GetDistance_MovingObjects)
{
    auto routeOptional{world->GetRoute(XY{5, -1.5}, XY{25, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    EXPECT_EQ(route.GetDistance(*world->GetMovingObject(get<Id>(objects[0]))), Interval<double>(-5, -4));
    EXPECT_EQ(route.GetDistance(*world->GetMovingObject(get<Id>(objects[1]))), Interval<double>(4, 6));
    EXPECT_EQ(route.GetDistance(*world->GetMovingObject(get<Id>(objects[2]))), Interval<double>(15, 17));
    EXPECT_EQ(route.GetDistance(*world->GetMovingObject(get<Id>(objects[3]))), Interval<double>(24, 25));

    EXPECT_TRUE(IntervalIsNaN(route.GetDistance(*world->GetMovingObject(get<Id>(objects[4])))));
    EXPECT_TRUE(IntervalIsNaN(route.GetDistance(*world->GetMovingObject(get<Id>(objects[5])))));
    EXPECT_TRUE(IntervalIsNaN(route.GetDistance(*world->GetMovingObject(get<Id>(objects[6])))));
    EXPECT_TRUE(IntervalIsNaN(route.GetDistance(*world->GetMovingObject(get<Id>(objects[7])))));
}

TEST_F(StraightRouteGetDistance, GetDistance_MovingObjects_Backward)
{
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{25, -1.5}, XY{5, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    EXPECT_EQ(route.GetDistance(*world->GetMovingObject(get<Id>(objects[0]))), Interval<double>(24, 25));
    EXPECT_EQ(route.GetDistance(*world->GetMovingObject(get<Id>(objects[1]))), Interval<double>(14, 16));
    EXPECT_EQ(route.GetDistance(*world->GetMovingObject(get<Id>(objects[2]))), Interval<double>(3, 5));
    EXPECT_EQ(route.GetDistance(*world->GetMovingObject(get<Id>(objects[3]))), Interval<double>(-5, -4));

    EXPECT_TRUE(IntervalIsNaN(route.GetDistance(*world->GetMovingObject(get<Id>(objects[4])))));
    EXPECT_TRUE(IntervalIsNaN(route.GetDistance(*world->GetMovingObject(get<Id>(objects[5])))));
    EXPECT_TRUE(IntervalIsNaN(route.GetDistance(*world->GetMovingObject(get<Id>(objects[6])))));
    EXPECT_TRUE(IntervalIsNaN(route.GetDistance(*world->GetMovingObject(get<Id>(objects[7])))));
}

TEST_F(StraightRouteGetDistance, GetDistanceBetween_Points)
{
    auto routeOptional{world->GetRoute(XY{5, -1.5}, XY{25, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    auto routeOptionalShort{world->GetRoute(XY{5, -1.5}, XY{15, -1.5})};
    ASSERT_TRUE(routeOptionalShort.has_value());
    auto &routeShort{routeOptionalShort.value()};
    ASSERT_THAT(routeShort, SizeIs(2));

    const Point<> point1{{0, 0}, *world->GetLane(get<Id>(roads[0].front()))};
    const Point<> point2{{5, 3}, *world->GetLane(get<Id>(roads[0].front()))};
    const Point<> point3{{0, 0}, *world->GetLane(get<Id>(roads[1].front()))};
    const Point<> point4{{5, -3}, *world->GetLane(get<Id>(roads[1].front()))};
    const Point<> point5{{0, 0}, *world->GetLane(get<Id>(roads[2].front()))};
    const Point<> point6{{5, -3}, *world->GetLane(get<Id>(roads[2].front()))};

    // Test both directions
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(point1, point1), 0);

    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(point1, point2), 5);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(point2, point1), -5);

    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(point1, point3), 10);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(point3, point1), -10);

    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(point1, point4), 15);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(point4, point1), -15);

    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(point1, point5), 20);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(point5, point1), -20);

    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(point1, point6), 25);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(point6, point1), -25);

    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(point3, point6), 15);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(point6, point3), -15);

    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(point4, point5), 5);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(point5, point4), -5);

    // Test outsiders
    EXPECT_TRUE(isnan(routeShort.GetDistanceBetween(point1, point5)));
    EXPECT_TRUE(isnan(routeShort.GetDistanceBetween(point2, point5)));
    EXPECT_TRUE(isnan(routeShort.GetDistanceBetween(point3, point5)));
    EXPECT_TRUE(isnan(routeShort.GetDistanceBetween(point4, point5)));
    EXPECT_TRUE(isnan(routeShort.GetDistanceBetween(point5, point6)));

    EXPECT_TRUE(isnan(routeShort.GetDistanceBetween(point5, point1)));
    EXPECT_TRUE(isnan(routeShort.GetDistanceBetween(point5, point2)));
    EXPECT_TRUE(isnan(routeShort.GetDistanceBetween(point5, point3)));
    EXPECT_TRUE(isnan(routeShort.GetDistanceBetween(point5, point4)));
    EXPECT_TRUE(isnan(routeShort.GetDistanceBetween(point6, point5)));
}

TEST_F(StraightRouteGetDistance, GetDistanceBetween_MovingObjects)
{
    auto routeOptional{world->GetRoute(XY{5, -1.5}, XY{25, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    std::vector<const MovingObject *> movingObjects{
        world->GetMovingObject(get<Id>(objects[0])),
        world->GetMovingObject(get<Id>(objects[1])),
        world->GetMovingObject(get<Id>(objects[2])),
        world->GetMovingObject(get<Id>(objects[3])),
        world->GetMovingObject(get<Id>(objects[4])),
        world->GetMovingObject(get<Id>(objects[5])),
        world->GetMovingObject(get<Id>(objects[6])),
        world->GetMovingObject(get<Id>(objects[7])) //
    };

    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[0], *movingObjects[0]), 0);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[0], *movingObjects[1]), 8);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[0], *movingObjects[2]), 19);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[0], *movingObjects[3]), 28);

    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[1], *movingObjects[2]), 9);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[1], *movingObjects[3]), 18);

    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[2], *movingObjects[3]), 7);

    // Test outsiders
    EXPECT_TRUE(isnan(route.GetDistanceBetween(*movingObjects[0], *movingObjects[4])));
    EXPECT_TRUE(isnan(route.GetDistanceBetween(*movingObjects[1], *movingObjects[5])));
    EXPECT_TRUE(isnan(route.GetDistanceBetween(*movingObjects[2], *movingObjects[6])));
    EXPECT_TRUE(isnan(route.GetDistanceBetween(*movingObjects[3], *movingObjects[7])));

    // Test reverse order
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[1], *movingObjects[0]), -8);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[2], *movingObjects[1]), -9);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[3], *movingObjects[2]), -7);
    EXPECT_TRUE(isnan(route.GetDistanceBetween(*movingObjects[4], *movingObjects[2])));
}

TEST_F(StraightRouteGetDistance, GetDistanceBetween_MovingObjects_Backward)
{
    auto routeOptional{world->GetRoute<Traversal::Backward>(XY{25, -1.5}, XY{5, -1.5})};
    ASSERT_TRUE(routeOptional.has_value());
    auto &route{routeOptional.value()};
    ASSERT_THAT(route, SizeIs(3));

    std::vector<const MovingObject *> movingObjects{
        world->GetMovingObject(get<Id>(objects[0])),
        world->GetMovingObject(get<Id>(objects[1])),
        world->GetMovingObject(get<Id>(objects[2])),
        world->GetMovingObject(get<Id>(objects[3])),
        world->GetMovingObject(get<Id>(objects[4])),
        world->GetMovingObject(get<Id>(objects[5])),
        world->GetMovingObject(get<Id>(objects[6])),
        world->GetMovingObject(get<Id>(objects[7])) //
    };

    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[0], *movingObjects[0]), 0);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[0], *movingObjects[1]), -8);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[0], *movingObjects[2]), -19);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[0], *movingObjects[3]), -28);

    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[1], *movingObjects[2]), -9);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[1], *movingObjects[3]), -18);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[2], *movingObjects[3]), -7);

    // Test outsiders
    EXPECT_TRUE(isnan(route.GetDistanceBetween(*movingObjects[0], *movingObjects[4])));
    EXPECT_TRUE(isnan(route.GetDistanceBetween(*movingObjects[1], *movingObjects[5])));
    EXPECT_TRUE(isnan(route.GetDistanceBetween(*movingObjects[2], *movingObjects[6])));
    EXPECT_TRUE(isnan(route.GetDistanceBetween(*movingObjects[3], *movingObjects[7])));

    // Test reverse order
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[1], *movingObjects[0]), 8);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[2], *movingObjects[1]), 9);
    EXPECT_DOUBLE_EQ(route.GetDistanceBetween(*movingObjects[3], *movingObjects[2]), 7);
    EXPECT_TRUE(isnan(route.GetDistanceBetween(*movingObjects[4], *movingObjects[2])));
}

// Copied from /RouteTests/Tests.cpp to have all route tests in a single file
struct PointProjectionOnStraightRoads : ::testing::Test
{
    PointProjectionOnStraightRoads()
    {
        auto road1 = groundTruth.AddRoad(
            groundTruth.AddReferenceLine(XY{0, 3}, XY{30, 3}),
            {
                &groundTruth.AddBoundary(XY{0, 6}, XY{30, 6}),
                &groundTruth.AddBoundary(XY{0, 3}, XY{30, 3}),
                &groundTruth.AddBoundary(XY{0, 0}, XY{30, 0}),
            }
        );
        auto road2 = groundTruth.AddRoad(
            groundTruth.AddReferenceLine(XY{30, 3}, XY{60, 3}),
            {
                &groundTruth.AddBoundary(XY{30, 6}, XY{60, 6}),
                &groundTruth.AddBoundary(XY{30, 3}, XY{60, 3}),
                &groundTruth.AddBoundary(XY{30, 0}, XY{60, 0}),
            }
        );
        test::ConnectRoads(road1.begin(), road1.end(), road2.begin(), road2.end());
        world = std::make_unique<World>(groundTruth.handle);
        route = std::make_unique<Route<>>(world->GetRoute( // clang-format off
            Point<const Lane>{ST{1.0, 0}, *world->GetLane(road1.back()->id().value())},
            Point<const Lane>{ST{3.5, 0}, *world->GetLane(road2.back()->id().value())}
        ).value()); // clang-format on
    }

    test::GroundTruth groundTruth;
    std::unique_ptr<World> world;
    std::unique_ptr<Route<>> route;
};

TEST_F(PointProjectionOnStraightRoads, FromStartingRoad_ToItself)
{
    const Point<const Lane> origin{ST{5, -1}, route->origin.GetLane()};
    auto projection{route->ProjectPoint(origin, 15.0)};
    ASSERT_TRUE(projection.has_value());
    EXPECT_THAT(projection.value().GetXY().x, DoubleNear(20.0, EPSILON));
    EXPECT_THAT(projection.value().GetXY().y, DoubleNear(2.0, EPSILON));
}

TEST_F(PointProjectionOnStraightRoads, FromStartingRoad_ToAnotherRoad)
{
    const Point<const Lane> origin{ST{5, -1}, route->origin.GetLane()};
    auto projection{route->ProjectPoint(origin, 35.0)};
    ASSERT_TRUE(projection.has_value());
    EXPECT_THAT(projection.value().GetXY().x, DoubleNear(40.0, EPSILON));
    EXPECT_THAT(projection.value().GetXY().y, DoubleNear(2.0, EPSILON));
}

TEST_F(PointProjectionOnStraightRoads, FromStartingRoad_BeyondEndOfRoute)
{
    const Point<const Lane> origin{ST{5.0, -1.0}, route->origin.GetLane()};
    auto projection{route->ProjectPoint(origin, 65.0)};
    EXPECT_FALSE(projection.has_value());
}

TEST_F(PointProjectionOnStraightRoads, FromStartingRoad_OffRoad_ToLastRoad)
{
    const Point<const Lane> origin{ST{35.0, -1.0}, route->origin.GetLane()};
    auto projection{route->ProjectPoint(origin, 15.0)};
    ASSERT_TRUE(projection.has_value());
    EXPECT_THAT(projection.value().GetXY().x, DoubleNear(50.0, EPSILON));
    EXPECT_THAT(projection.value().GetXY().y, DoubleNear(2.0, EPSILON));
}

TEST_F(PointProjectionOnStraightRoads, FromStartingRoad_OffRoad_Backward)
{
    const Point<const Lane> origin{ST{35.0, -1.0}, route->origin.GetLane()};
    auto projection{route->ProjectPoint(origin, -30.0)};
    ASSERT_TRUE(projection.has_value());
    EXPECT_THAT(projection.value().GetXY().x, DoubleNear(5.0, EPSILON));
    EXPECT_THAT(projection.value().GetXY().y, DoubleNear(2.0, EPSILON));
}

struct PointProjection_OnRoadsWithChangingWidth : ::testing::Test
{
    PointProjection_OnRoadsWithChangingWidth()
    {
        auto road1 = groundTruth.AddRoad(
            groundTruth.AddReferenceLine(Pose<XY>{{0, 0}, M_PI_2}, Pose<XY>{{30, 0}, M_PI_2}),
            {
                &groundTruth.AddBoundary(XY{0, 6}, XY{30, 3.0}),
                &groundTruth.AddBoundary(XY{0, 3}, XY{30, 1.5}),
                &groundTruth.AddBoundary(XY{0, 0}, XY{30, 0.0}),
            }
        );
        auto road2 = groundTruth.AddRoad(
            groundTruth.AddReferenceLine(Pose<XY>{{30, 0}, M_PI_2}, Pose<XY>{{60, 0}, M_PI_2}),
            {
                &groundTruth.AddBoundary(XY{30, 3.0}, XY{60, 6}),
                &groundTruth.AddBoundary(XY{30, 1.5}, XY{60, 3}),
                &groundTruth.AddBoundary(XY{30, 0.0}, XY{60, 0}),
            }
        );
        test::ConnectRoads(road1.begin(), road1.end(), road2.begin(), road2.end());
        world = std::make_unique<World>(groundTruth.handle);
        route = std::make_unique<Route<Traversal::Backward>>(
            world->GetRoute<Traversal::Backward>(XY{25, 0}, XY{35, 0}).value()
        );
    }

    test::GroundTruth groundTruth;
    std::unique_ptr<World> world;
    std::unique_ptr<Route<Traversal::Backward>> route;
};

TEST_F(PointProjection_OnRoadsWithChangingWidth, FromStartingRoad_ToSameRoad)
{
    const Point<const Lane> origin{ST{10.0, 0.5}, route->origin.GetLane()};
    ASSERT_THAT(origin.GetLane().GetCenterlineT(origin.s), DoubleNear(1.25, EPSILON));
    const auto projection{route->ProjectPoint(origin, 10.0)};
    ASSERT_TRUE(projection.has_value());
    EXPECT_THAT(projection.value().GetV(), DoubleNear(0.75, EPSILON));
    EXPECT_THAT(projection.value().GetXY().x, DoubleNear(20.0, EPSILON));
    EXPECT_THAT(projection.value().GetXY().y, DoubleNear(0.25, EPSILON));
}

TEST_F(PointProjection_OnRoadsWithChangingWidth, FromStartingRoad_ToNextRoad)
{
    const auto origin{route->Localize(XY{10.0, 0.5})};
    const auto projection{route->ProjectPoint(origin, 40.0)};
    ASSERT_TRUE(projection.has_value());
    EXPECT_THAT(projection.value().GetXY().x, DoubleNear(50.0, EPSILON));
    EXPECT_THAT(projection.value().GetXY().y, DoubleNear(0.5, EPSILON));
}

struct PointProjectionOnBranchingRoads : ::testing::Test
{
    PointProjectionOnBranchingRoads()
    {
        auto road1 = groundTruth.AddRoad(
            groundTruth.AddReferenceLine(Pose<XY>{{0, 0}, M_PI_2}, Pose<XY>{{30, 0}, M_PI_2}),
            {
                &groundTruth.AddBoundary(XY{0, 4}, XY{30, 4}),
                &groundTruth.AddBoundary(XY{0, 0}, XY{30, 0}),
                &groundTruth.AddBoundary(XY{0, -4}, XY{30, -4}),
            }
        );
        auto road2 = groundTruth.AddRoad(
            groundTruth.AddReferenceLine(Pose<XY>{{30, 0}, M_PI_2}, Pose<XY>{{70, 30}, M_PI_2}),
            {
                &groundTruth.AddBoundary(XY{30, 4}, XY{70, 40}),
                &groundTruth.AddBoundary(XY{30, 0}, XY{70, 30}),
                &groundTruth.AddBoundary(XY{30, -4}, XY{70, 20}),
            }
        );
        test::ConnectRoads(road1.begin(), road1.end(), road2.begin(), road2.end());
        auto road3 = groundTruth.AddRoad(
            groundTruth.AddReferenceLine(Pose<XY>{{30, 0}, M_PI_2}, Pose<XY>{{70, -30}, M_PI_2}),
            {
                &groundTruth.AddBoundary(XY{30, 4}, XY{70, -20}),
                &groundTruth.AddBoundary(XY{30, 0}, XY{70, -30}),
                &groundTruth.AddBoundary(XY{30, -4}, XY{70, -40}),
            }
        );
        test::ConnectRoads(road1.begin(), road1.end(), road3.begin(), road3.end());
        world = std::make_unique<World>(groundTruth.handle);
        route = std::make_unique<Route<>>(world->GetRoute<>(XY{0, 0}, XY{70, -30}).value());
    }

    test::GroundTruth groundTruth;
    std::unique_ptr<World> world;
    std::unique_ptr<Route<>> route;
};

TEST_F(PointProjectionOnBranchingRoads, StaysOnStartingRoad)
{
    const Point<const Lane> origin{ST{5.0, -1.0}, route->origin.GetLane()};
    const auto projection{route->ProjectPoint(origin, 15.0)};
    ASSERT_TRUE(projection.has_value());
    EXPECT_THAT(projection.value().GetXY().x, DoubleNear(20.0, EPSILON));
    EXPECT_THAT(projection.value().GetXY().y, DoubleNear(-1.0, EPSILON));
}

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(PointProjectionOnBranchingRoads, FromStartingRoad_ToNextRoad)
{
    const auto origin{world->Localize(XY{0.0, -1.0}).front()}; // 1 unit above the centerline
    {
        const auto projection{route->ProjectPoint(origin, 30).value().GetXY()};
        EXPECT_DOUBLE_EQ(projection.x, 30);
        EXPECT_DOUBLE_EQ(projection.y, -1);
    }
    {
        const auto projection{route->ProjectPoint(origin, 40).value().GetXY()};
        EXPECT_DOUBLE_EQ(projection.x, 38);
        EXPECT_DOUBLE_EQ(projection.y, -33 * 0.2 - 1);
    }
    {
        const auto projection{route->ProjectPoint(origin, 50).value().GetXY()};
        EXPECT_DOUBLE_EQ(projection.x, 46);
        EXPECT_DOUBLE_EQ(projection.y, -33 * 0.4 - 1);
    }
    {
        const auto projection{route->ProjectPoint(origin, 60).value().GetXY()};
        EXPECT_DOUBLE_EQ(projection.x, 54);
        EXPECT_DOUBLE_EQ(projection.y, -33 * 0.6 - 1);
    }
    {
        const auto projection{route->ProjectPoint(origin, 70).value().GetXY()};
        EXPECT_DOUBLE_EQ(projection.x, 62);
        EXPECT_DOUBLE_EQ(projection.y, -33 * 0.8 - 1);
    }
    {
        const auto projection{route->ProjectPoint(origin, 80).value().GetXY()};
        EXPECT_DOUBLE_EQ(projection.x, 70);
        EXPECT_DOUBLE_EQ(projection.y, -34);
    }
    {
        const auto projection{route->ProjectPoint(origin, 85)};
        EXPECT_FALSE(projection.has_value());
    }
}
