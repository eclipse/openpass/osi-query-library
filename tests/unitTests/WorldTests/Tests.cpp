/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "util/Common.tpp"

using namespace osiql;

class QueryTest : public ::testing::Test
{
public:
    QueryTest()
    {
        roads.push_back(groundTruth.AddRoad( // ROAD 0
            groundTruth.AddReferenceLine(Pose<XY>{{0, 0}, halfPi}, Pose<XY>{{2, 0}, halfPi}),
            {
                &groundTruth.AddBoundary(XY{0, 2}, XY{2, 2}),
                &groundTruth.AddBoundary(XY{0, 1}, XY{2, 2}),
                &groundTruth.AddBoundary(XY{0, 0}, XY{2, 0}),
                &groundTruth.AddBoundary(XY{0, -1}, XY{2, -2}),
                &groundTruth.AddBoundary(XY{0, -2}, XY{2, -2}),
            }
        ));
        roads.push_back(groundTruth.AddRoad( // ROAD 1
            groundTruth.AddReferenceLine(Pose<XY>{{4, 2}, -halfPi}, Pose<XY>{{2, 0}, -halfPi}),
            {
                &groundTruth.AddBoundary(XY{4, 1}, XY{2, -2}),
                &groundTruth.AddBoundary(XY{4, 2}, XY{2, 0}),
                &groundTruth.AddBoundary(XY{4, 3}, XY{2, 2}),
            }
        ));
        test::ConnectRoads(std::next(roads[0].begin()), std::prev(roads[0].end()), roads[1].rbegin(), roads[1].rend());

        roads.push_back(groundTruth.AddRoad(
            groundTruth.AddReferenceLine(Pose<XY>{{4, 2}, halfPi}, Pose<XY>{{6, 1}, halfPi}),
            {
                &groundTruth.AddBoundary(XY{4, 3}, XY{6, 1}),
                &groundTruth.AddBoundary(XY{4, 2}, XY{6, 1}),
                &groundTruth.AddBoundary(XY{4, 1}, XY{6, 1}),
            }
        ));
        test::ConnectRoads(roads[1].rbegin(), roads[1].rend(), roads[2].begin(), roads[2].end());

        roads.push_back(groundTruth.AddRoad(
            groundTruth.AddReferenceLine(Pose<XY>{{8, -2}, -halfPi}, Pose<XY>{{6, -2}, -halfPi}, Pose<XY>{{2, 0}, -halfPi}),
            {
                &groundTruth.AddBoundary(XY{8, -4}, XY{6, -3}, XY{2, -2}),
                &groundTruth.AddBoundary(XY{8, -2}, XY{6, -2}, XY{2, 0}),
                &groundTruth.AddBoundary(XY{8, 0}, XY{6, -1}, XY{2, 2}),
            }
        ));
        test::ConnectRoads(std::next(roads[0].begin()), std::prev(roads[0].end()), roads[3].rbegin(), roads[3].rend());

        groundTruth.handle.mutable_host_vehicle_id()->set_value(groundTruth.Add<MovingObject>(XY{1.0, -1.0}, 1.0, 0.5).id().value());
        world = std::make_unique<World>(groundTruth.handle);
    }
    test::GroundTruth groundTruth;
    std::unique_ptr<World> world;
    std::vector<std::vector<osi3::LogicalLane *>> roads;
};

TEST_F(QueryTest, LaneIndices)
{
    // On a road with 2 downstream and 2 upstream lanes:

    const Lane *rightmostLane{world->GetLane(get<Id>(roads[0][3]))};
    ASSERT_NE(rightmostLane, nullptr);
    const Lane *rightLane{world->GetLane(get<Id>(roads[0][2]))};
    ASSERT_NE(rightLane, nullptr);
    const Lane *leftLane{world->GetLane(get<Id>(roads[0][1]))};
    ASSERT_NE(leftLane, nullptr);
    const Lane *leftmostLane{world->GetLane(get<Id>(roads[0][0]))};
    ASSERT_NE(leftmostLane, nullptr);

    EXPECT_EQ(rightmostLane->GetIndex(Side::Right), 1);
    EXPECT_EQ(rightLane->GetIndex(Side::Right), 0);
    EXPECT_EQ(leftLane->GetIndex(Side::Right), -1);
    EXPECT_EQ(leftmostLane->GetIndex(Side::Right), -2);

    EXPECT_EQ(rightmostLane->GetIndex(Side::Left), -2);
    EXPECT_EQ(rightLane->GetIndex(Side::Left), -1);
    EXPECT_EQ(leftLane->GetIndex(Side::Left), 0);
    EXPECT_EQ(leftmostLane->GetIndex(Side::Left), 1);
}

TEST_F(QueryTest, AdjacentLanes)
{
    const Lane *rightmostLane{world->GetLane(get<Id>(roads[0][3]))};
    ASSERT_NE(rightmostLane, nullptr);
    const Lane *rightLane{world->GetLane(get<Id>(roads[0][2]))};
    ASSERT_NE(rightLane, nullptr);
    const Lane *leftLane{world->GetLane(get<Id>(roads[0][1]))};
    ASSERT_NE(leftLane, nullptr);
    const Lane *leftmostLane{world->GetLane(get<Id>(roads[0][0]))};
    ASSERT_NE(leftmostLane, nullptr);

    const Lane *lane{rightmostLane->GetAdjacentLane<Side::Right, Traversal::Forward>(0)};
    EXPECT_EQ(lane, rightmostLane);
    lane = rightmostLane->GetAdjacentLane<Side::Left, Traversal::Backward>(0);
    EXPECT_EQ(lane, rightmostLane);

    lane = rightmostLane->GetAdjacentLane<Side::Right, Traversal::Forward>(1);
    EXPECT_EQ(lane, nullptr);
    lane = rightmostLane->GetAdjacentLane<Side::Right, Traversal::Forward>(2);
    EXPECT_EQ(lane, nullptr);
    lane = rightLane->GetAdjacentLane<Side::Right, Traversal::Forward>(2);
    EXPECT_EQ(lane, nullptr);
    lane = leftLane->GetAdjacentLane<Side::Left, Traversal::Forward>(3);
    EXPECT_EQ(lane, nullptr);
    lane = leftmostLane->GetAdjacentLane<Side::Left, Traversal::Forward>(4);
    EXPECT_EQ(lane, nullptr);

    lane = rightmostLane->GetAdjacentLane<Side::Left, Traversal::Backward>(1);
    EXPECT_EQ(lane, nullptr);
    lane = rightmostLane->GetAdjacentLane<Side::Left, Traversal::Backward>(2);
    EXPECT_EQ(lane, nullptr);
    lane = rightLane->GetAdjacentLane<Side::Left, Traversal::Backward>(2);
    EXPECT_EQ(lane, nullptr);
    lane = leftLane->GetAdjacentLane<Side::Right, Traversal::Backward>(3);
    EXPECT_EQ(lane, nullptr);
    lane = leftmostLane->GetAdjacentLane<Side::Right, Traversal::Backward>(4);
    EXPECT_EQ(lane, nullptr);

    EXPECT_EQ(rightmostLane->GetAdjacentLane<Side::Left>(1), rightLane);
    EXPECT_EQ(rightLane->GetAdjacentLane<Side::Right>(1), rightmostLane);
    EXPECT_EQ(rightmostLane->GetAdjacentLane<Side::Left>(2), leftLane);
    EXPECT_EQ(leftLane->GetAdjacentLane<Side::Left>(2), rightmostLane);
}

TEST_F(QueryTest, AdjacentBoundaries)
{
    const Lane *rightmostLane{world->GetLane(get<Id>(roads[0][3]))};
    ASSERT_NE(rightmostLane, nullptr);
    const Lane *rightLane{world->GetLane(get<Id>(roads[0][2]))};
    ASSERT_NE(rightLane, nullptr);
    const Lane *leftLane{world->GetLane(get<Id>(roads[0][1]))};
    ASSERT_NE(leftLane, nullptr);
    const Lane *leftmostLane{world->GetLane(get<Id>(roads[0][0]))};
    ASSERT_NE(leftmostLane, nullptr);
    EXPECT_EQ(rightmostLane->GetBoundaries<Side::Left>(Traversal::Forward)[0], rightmostLane->GetBoundaries<Side::Right>(Traversal::Backward)[0]);
    EXPECT_EQ(rightmostLane->GetBoundaries<Side::Left>(Traversal::Forward)[0], rightLane->GetBoundaries<Side::Right>(Traversal::Forward)[0]);
    EXPECT_EQ(rightLane->GetBoundaries<Side::Left>(Traversal::Forward)[0], leftLane->GetBoundaries<Side::Left>(Traversal::Forward)[0]);
    EXPECT_EQ(leftLane->GetBoundaries<Side::Right>(Traversal::Forward)[0], leftmostLane->GetBoundaries<Side::Left>(Traversal::Forward)[0]);
}

// Lane Width Tests

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(QueryTest, GetLaneWidth_Forward)
{
    const Lane *lane{world->GetLane(get<Id>(roads[0][2]))};
    ASSERT_NE(lane, nullptr);
    // Before the start of the road
    EXPECT_DOUBLE_EQ(lane->GetWidth(std::numeric_limits<double>::lowest()), 0.0);
    // On the current road
    const double lengthOfRoad0{2.0};
    EXPECT_DOUBLE_EQ(lane->GetWidth(0.0), 1.0);
    EXPECT_DOUBLE_EQ(lane->GetWidth(0.5 * lengthOfRoad0), 1.5);
    EXPECT_DOUBLE_EQ(lane->GetWidth(lengthOfRoad0), 2.0);
    // On next road (has two connected roads, always selects the first)
    const double lengthOfRoad1{sqrt(8.0)};
    EXPECT_DOUBLE_EQ(lane->GetWidth(lengthOfRoad0 + 0.5 * lengthOfRoad1), 1.5);
    EXPECT_DOUBLE_EQ(lane->GetWidth(lengthOfRoad0 + lengthOfRoad1), 1.0);
    // Two roads further
    const double lengthOfRoad2{sqrt(5.0)};
    EXPECT_DOUBLE_EQ(lane->GetWidth(lengthOfRoad0 + lengthOfRoad1 + 0.5 * lengthOfRoad2), 0.5);
    EXPECT_THAT(lane->GetWidth(lengthOfRoad0 + lengthOfRoad1 + lengthOfRoad2), DoubleNear(0.0, EPSILON));
    // Beyond one path (roads[0] -> roads[1] -> roads[2]), but on the other (roads[0] -> roads[3])
    EXPECT_DOUBLE_EQ(lane->GetWidth(lengthOfRoad0 + sqrt(20.0) + 1.0), 1.5);
    EXPECT_DOUBLE_EQ(lane->GetWidth(lengthOfRoad0 + sqrt(20.0) + 2.0), 2.0);
    // Beyond the end of the road
    EXPECT_DOUBLE_EQ(lane->GetWidth(std::numeric_limits<double>::max()), 0.0);
}

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(QueryTest, GetLaneWidth_Backward)
{
    const Lane *lane{world->GetLane(get<Id>(roads[2][1]))};
    ASSERT_NE(lane, nullptr);
    const double lengthOfRoad1{sqrt(8.0)};
    // On roads[1]
    EXPECT_DOUBLE_EQ(lane->GetWidth(-0.5 * lengthOfRoad1), 1.5);
    EXPECT_DOUBLE_EQ(lane->GetWidth(-lengthOfRoad1), 2.0);
    // On roads[0]
    EXPECT_DOUBLE_EQ(lane->GetWidth(-lengthOfRoad1 - 1.0), 1.5);
    EXPECT_DOUBLE_EQ(lane->GetWidth(-lengthOfRoad1 - 2.0), 1.0);
}

// Distance from Object to Polyline Tests

TEST_F(QueryTest, GetDistanceTo_LaneBoundaries_Returns_CorrectValue)
{
    const auto &object{world->GetHostVehicle()};
    const Lane *lane{world->GetLane(get<Id>(roads[0][2]))};
    ASSERT_NE(lane, nullptr);
    const auto [left, right]{object.GetBoundaryDistances(*lane)};
    EXPECT_DOUBLE_EQ(left, 0.75);
    EXPECT_DOUBLE_EQ(right, 0.0);
}

TEST_F(QueryTest, GetDistanceTo_AdjacentLanePolyline_Returns_CorrectValue)
{
    const auto &object{world->GetHostVehicle()};
    const auto &rightmostBoundary{*world->GetLane(get<Id>(roads[0][3]))->GetBoundaries<Side::Right>()[0]};
    const auto &centerBoundaryFromRight{*world->GetLane(get<Id>(roads[0][2]))->GetBoundaries<Side::Left>()[0]};
    const auto &centerBoundaryFromLeft{*world->GetLane(get<Id>(roads[0][1]))->GetBoundaries<Side::Left>()[0]};
    const auto &leftmostBoundary{*world->GetLane(get<Id>(roads[0][0]))->GetBoundaries<Side::Right>()[0]};
    EXPECT_DOUBLE_EQ(object.GetSignedDistancesTo(leftmostBoundary.begin(), leftmostBoundary.end()).min, 2.75);

    const auto distanceToCenterFromLeft{object.GetSignedDistancesTo(centerBoundaryFromLeft.begin(), centerBoundaryFromLeft.end())};
    const auto distanceToCenterFromRight{object.GetSignedDistancesTo(centerBoundaryFromRight.begin(), centerBoundaryFromRight.end())};
    EXPECT_EQ(distanceToCenterFromLeft, distanceToCenterFromRight);
    EXPECT_DOUBLE_EQ(object.GetSignedDistancesTo(rightmostBoundary.begin(), rightmostBoundary.end()).max, -0.75);
}

TEST_F(QueryTest, GetDistanceTo_IntersectingPolyline_Returns_Positive_And_Negative_Distance)
{
    // Make the object wider so that it overlaps the boundary
    groundTruth.handle.mutable_moving_object(0)->mutable_base()->mutable_dimension()->set_width(2.5);
    world->Update(groundTruth.handle.moving_object());
    const auto &object{world->GetHostVehicle()};
    const Lane *lane{world->GetLane(get<Id>(roads[0][2]))};
    ASSERT_NE(lane, nullptr);
    const auto &centerBoundary{*lane->GetBoundaries<Side::Left>()[0]};
    EXPECT_LE(object.GetSignedDistancesTo(centerBoundary.begin(), centerBoundary.end()).min, 0.0);
    EXPECT_GE(object.GetSignedDistancesTo(centerBoundary.begin(), centerBoundary.end()).max, 0.0);
}

// Lane Curvature Tests

TEST_F(QueryTest, GetCurvature_StraightReferenceLine_Returns_Zero)
{
    const ReferenceLine *referenceLine{world->GetReferenceLine(roads[0][0]->reference_line_id().value())};
    ASSERT_NE(referenceLine, nullptr);
    EXPECT_DOUBLE_EQ(referenceLine->GetCurvature(0.0), 0.0);
}

TEST_F(QueryTest, GetCurvature_Forward_Equals_BackwardInverted)
{
    const ReferenceLine *referenceLine{world->GetReferenceLine(roads[3][0]->reference_line_id().value())};
    ASSERT_NE(referenceLine, nullptr);
    EXPECT_DOUBLE_EQ(referenceLine->GetCurvature(0.0, Direction::Downstream), -referenceLine->GetCurvature(0.0, Direction::Upstream));
    EXPECT_DOUBLE_EQ(referenceLine->GetCurvature(1.0, Direction::Downstream), -referenceLine->GetCurvature(1.0, Direction::Upstream));
    EXPECT_DOUBLE_EQ(referenceLine->GetCurvature(2.0, Direction::Downstream), -referenceLine->GetCurvature(2.0, Direction::Upstream));
    EXPECT_DOUBLE_EQ(referenceLine->GetCurvature(3.0, Direction::Downstream), -referenceLine->GetCurvature(3.0, Direction::Upstream));
    EXPECT_DOUBLE_EQ(referenceLine->GetCurvature(4.0, Direction::Downstream), -referenceLine->GetCurvature(4.0, Direction::Upstream));
    EXPECT_DOUBLE_EQ(referenceLine->GetCurvature(5.0, Direction::Downstream), -referenceLine->GetCurvature(5.0, Direction::Upstream));
    EXPECT_DOUBLE_EQ(referenceLine->GetCurvature(6.0, Direction::Downstream), -referenceLine->GetCurvature(6.0, Direction::Upstream));
}

TEST_F(QueryTest, GetCurvature_OfLane_IsDifferentFromReferenceLine_OnCurvedRoad)
{
    const ReferenceLine *referenceLine{world->GetReferenceLine(roads[3][0]->reference_line_id().value())};
    ASSERT_NE(referenceLine, nullptr);
    const Lane *lane0{world->GetLane(get<Id>(roads[3][1]))};
    ASSERT_NE(lane0, nullptr);
    const Lane *lane1{world->GetLane(get<Id>(roads[3][0]))};
    ASSERT_NE(lane1, nullptr);

    EXPECT_DOUBLE_EQ(lane0->GetCurvature<Traversal::Forward>(0.0), -lane0->GetCurvature<Traversal::Backward>(0.0));
    EXPECT_DOUBLE_EQ(lane0->GetCurvature<Traversal::Forward>(1.0), -lane0->GetCurvature<Traversal::Backward>(1.0));
    EXPECT_DOUBLE_EQ(lane0->GetCurvature<Traversal::Forward>(2.0), -lane0->GetCurvature<Traversal::Backward>(2.0));
    EXPECT_DOUBLE_EQ(lane0->GetCurvature<Traversal::Forward>(3.0), -lane0->GetCurvature<Traversal::Backward>(3.0));

    EXPECT_DOUBLE_EQ(lane1->GetCurvature<Traversal::Forward>(0.0), -lane1->GetCurvature<Traversal::Backward>(0.0));
    EXPECT_DOUBLE_EQ(lane1->GetCurvature<Traversal::Forward>(1.0), -lane1->GetCurvature<Traversal::Backward>(1.0));
    EXPECT_DOUBLE_EQ(lane1->GetCurvature<Traversal::Forward>(2.0), -lane1->GetCurvature<Traversal::Backward>(2.0));
    EXPECT_DOUBLE_EQ(lane1->GetCurvature<Traversal::Forward>(3.0), -lane1->GetCurvature<Traversal::Backward>(3.0));

    EXPECT_LT(lane0->GetCurvature<Direction::Downstream>(1.0), referenceLine->GetCurvature<Direction::Downstream>(1.0));
    EXPECT_LT(lane0->GetCurvature<Direction::Downstream>(2.0), referenceLine->GetCurvature<Direction::Downstream>(2.0));
    EXPECT_LT(lane0->GetCurvature<Direction::Downstream>(3.0), referenceLine->GetCurvature<Direction::Downstream>(3.0));

    EXPECT_GT(lane1->GetCurvature<Direction::Downstream>(1.0), referenceLine->GetCurvature<Direction::Downstream>(1.0));
    EXPECT_GT(lane1->GetCurvature<Direction::Downstream>(2.0), referenceLine->GetCurvature<Direction::Downstream>(2.0));
    EXPECT_GT(lane1->GetCurvature<Direction::Downstream>(3.0), referenceLine->GetCurvature<Direction::Downstream>(3.0));
}

TEST_F(QueryTest, GetObstruction_OfVehicleBy_Itself)
{
    const Vehicle &ego{world->GetHostVehicle()};
    ASSERT_THAT(ego.GetOverlaps<Road>(), ::testing::Not(IsEmpty()));
    ASSERT_THAT(ego.GetOverlaps<Lane>(), ::testing::Not(IsEmpty()));
    const auto route{world->GetRoute(XY{1.0, -1.0}, XY{7.0, -3.0})};
    ASSERT_TRUE(route.has_value());
    const auto laneChain{ego.GetLaneChain(route.value())};

    const auto [min, max]{ego.GetObstruction(ego, laneChain)};
    EXPECT_DOUBLE_EQ(-min, max);
    EXPECT_GT(max, ego.GetWidth());

    const auto origin{world->Localize(ego.GetXY()).front()};
    const auto [centerMin, centerMax]{ego.GetObstruction(origin, laneChain)};
    EXPECT_DOUBLE_EQ(max, 2.0 * centerMax);
}

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(QueryTest, GetObstruction_OfVehicleBy_IndividualPoints)
{
    const Vehicle &ego{world->GetHostVehicle()};
    ASSERT_THAT(ego.GetOverlaps<Road>(), ::testing::Not(IsEmpty()));
    ASSERT_THAT(ego.GetOverlaps<Lane>(), ::testing::Not(IsEmpty()));

    const auto route{world->GetRoute(XY{1.0, -1.0}, XY{7.0, -3.0})};
    ASSERT_TRUE(route.has_value());
    const auto origin{world->Localize(ego.GetXY()).front()};
    const auto laneChain{route.value().origin.GetLane().GetChain(route.value())};

    const auto [min1, max1]{ego.GetObstruction(origin, laneChain)};
    EXPECT_THAT(min1, DoubleEq(-3.0 / 8.0));
    EXPECT_THAT(max1, DoubleEq(3.0 / 8.0));
    const auto [min2, max2]{ego.GetObstruction(route.value().destination, laneChain)};
    EXPECT_THAT(min2, DoubleEq(-3.0 / 8.0));
    EXPECT_THAT(max2, DoubleEq(3.0 / 8.0));

    const auto [min3, max3]{ego.GetObstruction(
        Point<const Lane>{ST{0.0, 0.0}, route.value().destination.GetLane()}, laneChain
    )};
    EXPECT_THAT(min3, DoubleEq(7.0 / 8.0));
    EXPECT_THAT(max3, DoubleEq(13.0 / 8.0));
}

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(QueryTest, GetObstruction_OfVehicleBy_ObjectOneRoadApart)
{
    // Object on road 2
    groundTruth.Add<MovingObject>(XY{7.5, -2.5}, 1.0, 1.0);
    world->Update(groundTruth.handle.moving_object());

    const auto &startObject{world->GetHostVehicle()};
    const auto *endObject{world->GetMovingObject(startObject.GetId() + 1)};
    ASSERT_NE(endObject, nullptr);

    const auto route{world->GetRoute(XY{1.0, -1.0}, XY{7.0, -3.0})};
    ASSERT_TRUE(route.has_value());
    const auto laneChain{route.value().origin.GetLane().GetChain<Traversal::Forward>(route.value())};
    const auto [min1, max1]{startObject.GetObstruction(*endObject, laneChain)};
    EXPECT_DOUBLE_EQ(min1, -0.375);
    EXPECT_DOUBLE_EQ(max1, 1.625);
    const auto [min2, max2]{endObject->GetObstruction(startObject, laneChain)};
    EXPECT_DOUBLE_EQ(min1, -max2);
    EXPECT_DOUBLE_EQ(max1, -min2);
}

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(QueryTest, GetObstruction_OfVehicleBy_ObjectTwoRoadsApart)
{
    // Object on road 2
    groundTruth.Add<MovingObject>(XY{5.5, 1.5}, 1.0, 1.0);
    world->Update(groundTruth.handle.moving_object());

    const auto &startObject{world->GetHostVehicle()};
    const auto *endObject{world->GetMovingObject(startObject.GetId() + 1)};
    ASSERT_NE(endObject, nullptr);

    // From road 0 over road 1 to road 2
    const auto route{world->GetRoute(XY{1.0, -1.0}, XY{5.0, 1.25})};
    ASSERT_TRUE(route.has_value());
    const auto laneChain{route.value().origin.GetLane().GetChain<Traversal::Forward>(route.value())};
    const auto [min1, max1]{startObject.GetObstruction(*endObject, laneChain)};
    EXPECT_DOUBLE_EQ(min1, -0.375);
    EXPECT_DOUBLE_EQ(max1, 1.375);
    const auto [min2, max2]{endObject->GetObstruction(startObject, laneChain)};
    EXPECT_DOUBLE_EQ(min1, -max2);
    EXPECT_DOUBLE_EQ(max1, -min2);
}

TEST_F(QueryTest, GetObstruction_OfVehicleBy_ObjectNotOnRoute)
{
    // Object on road 2
    groundTruth.Add<MovingObject>(XY{5.5, 1.5}, 1.0, 1.0);
    world->Update(groundTruth.handle.moving_object());

    const auto &startObject{world->GetHostVehicle()};
    const auto *endObject{world->GetMovingObject(startObject.GetId() + 1)};
    ASSERT_NE(endObject, nullptr);

    // From road 0 to road 3
    const auto route{world->GetRoute(XY{1.0, -1.0}, XY{7.0, -3.0})};
    ASSERT_TRUE(route.has_value());
    const auto laneChain{route.value().origin.GetLane().GetChain<Traversal::Forward>(route.value())};
    const auto [min1, max1]{startObject.GetObstruction(*endObject, laneChain)};
    EXPECT_DOUBLE_EQ(min1, std::numeric_limits<double>::max());
    EXPECT_DOUBLE_EQ(max1, std::numeric_limits<double>::lowest());
    const auto [min2, max2]{endObject->GetObstruction(startObject, laneChain)};
    EXPECT_DOUBLE_EQ(min2, std::numeric_limits<double>::max());
    EXPECT_DOUBLE_EQ(max2, std::numeric_limits<double>::lowest());
}

TEST_F(QueryTest, GetObstruction_OfVehicleBy_ObjectNotOnAnyRoad)
{
    auto id{groundTruth.Add<MovingObject>(XY{100.0, 100.0}, 1.0, 1.0).id().value()};
    world->Update(groundTruth.handle.moving_object());

    const auto *objectOnRoad{world->GetMovingObject(id - 1)};
    ASSERT_NE(objectOnRoad, nullptr);
    const auto *objectOffRoad{world->GetMovingObject(id)};
    ASSERT_NE(objectOffRoad, nullptr);

    const auto route{world->GetRoute(XY{1.0, -1.0}, XY{5.0, 1.25})};
    ASSERT_TRUE(route.has_value());
    const auto laneChain{route.value().origin.GetLane().GetChain<Traversal::Forward>(route.value())};
    const auto [min1, max1]{objectOnRoad->GetObstruction(*objectOffRoad, laneChain)};
    EXPECT_DOUBLE_EQ(min1, std::numeric_limits<double>::max());
    EXPECT_DOUBLE_EQ(max1, std::numeric_limits<double>::lowest());
    const auto [min2, max2]{objectOffRoad->GetObstruction(*objectOnRoad, laneChain)};
    EXPECT_DOUBLE_EQ(min2, std::numeric_limits<double>::max());
    EXPECT_DOUBLE_EQ(max2, std::numeric_limits<double>::lowest());
}

TEST_F(QueryTest, Update_Objects)
{
    // One object
    world->Update(groundTruth.handle.moving_object());
    EXPECT_THAT(world->GetAll<MovingObject>(), SizeIs(1));
    // No objects anymore
    groundTruth.handle.mutable_moving_object()->Clear();
    world->Update(groundTruth.handle.moving_object());
    EXPECT_THAT(world->GetAll<MovingObject>(), IsEmpty());
    // Two objects
    groundTruth.Add<MovingObject>(XY{0, 0}, 2, 1);
    groundTruth.Add<MovingObject>(XY{1, 0}, 2, 1);
    world->Update(groundTruth.handle.moving_object());
    EXPECT_THAT(world->GetAll<MovingObject>(), SizeIs(2));
    // Replace one of the objects by first deleting then adding
    {
        auto *objects{groundTruth.handle.mutable_moving_object()};
        objects->erase(std::prev(objects->end()));
        groundTruth.Add<MovingObject>(XY{2, 0}, 2, 1);
        world->Update(groundTruth.handle.moving_object());
        EXPECT_THAT(world->GetAll<MovingObject>(), SizeIs(2));
    }
    // Replace one of the objects by first adding then deleting
    {
        groundTruth.Add<MovingObject>(XY{3, 0}, 2, 1);
        auto *objects{groundTruth.handle.mutable_moving_object()};
        objects->erase(objects->begin());
        world->Update(groundTruth.handle.moving_object());
        EXPECT_THAT(world->GetAll<MovingObject>(), SizeIs(2));
    }
}
