/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "OsiQueryLibrary/Point/Point.h"
#include "util/Common.tpp"
#include "util/GroundTruth.h"

using namespace osiql;

struct PointProjection_OnStraightRoads : ::testing::Test
{
    PointProjection_OnStraightRoads()
    {
        auto road1 = world.AddRoad(
            world.AddReferenceLine(XY{0, 3}, XY{30, 3}),
            {
                &world.AddBoundary(XY{0, 6}, XY{30, 6}),
                &world.AddBoundary(XY{0, 3}, XY{30, 3}),
                &world.AddBoundary(XY{0, 0}, XY{30, 0}),
            }
        );
        auto road2 = world.AddRoad(
            world.AddReferenceLine(XY{30, 3}, XY{60, 3}),
            {
                &world.AddBoundary(XY{30, 6}, XY{60, 6}),
                &world.AddBoundary(XY{30, 3}, XY{60, 3}),
                &world.AddBoundary(XY{30, 0}, XY{60, 0}),
            }
        );
        test::ConnectRoads(road1.begin(), road1.end(), road2.begin(), road2.end());
        query = std::make_unique<Query>(world.handle);
        route = std::make_unique<Route<>>(query->GetRoute( // clang-format off
            Point<const Lane>{ST{1.0, 0}, *query->GetLane(road1.back()->id().value())},
            Point<const Lane>{ST{3.5, 0}, *query->GetLane(road2.back()->id().value())}
        ).value()); // clang-format on
    }

    test::GroundTruth world;
    std::unique_ptr<Query> query;
    std::unique_ptr<Route<>> route;
};

TEST_F(PointProjection_OnStraightRoads, FromStartingRoad_ToItself)
{
    const Point<const Lane> origin{ST{5, -1}, route->origin.GetLane()};
    auto projection{route->ProjectPoint(origin, 15.0)};
    ASSERT_TRUE(projection.has_value());
    EXPECT_THAT(projection.value().GetXY().x, DoubleNear(20.0, EPSILON));
    EXPECT_THAT(projection.value().GetXY().y, DoubleNear(2.0, EPSILON));
}

TEST_F(PointProjection_OnStraightRoads, FromStartingRoad_ToAnotherRoad)
{
    const Point<const Lane> origin{ST{5, -1}, route->origin.GetLane()};
    auto projection{route->ProjectPoint(origin, 35.0)};
    ASSERT_TRUE(projection.has_value());
    EXPECT_THAT(projection.value().GetXY().x, DoubleNear(40.0, EPSILON));
    EXPECT_THAT(projection.value().GetXY().y, DoubleNear(2.0, EPSILON));
}

TEST_F(PointProjection_OnStraightRoads, FromStartingRoad_BeyondEndOfRoute)
{
    const Point<const Lane> origin{ST{5.0, -1.0}, route->origin.GetLane()};
    auto projection{route->ProjectPoint(origin, 65.0)};
    EXPECT_FALSE(projection.has_value());
}

TEST_F(PointProjection_OnStraightRoads, FromStartingRoad_OffRoad_ToLastRoad)
{
    const Point<const Lane> origin{ST{35.0, -1.0}, route->origin.GetLane()};
    auto projection{route->ProjectPoint(origin, 15.0)};
    ASSERT_TRUE(projection.has_value());
    EXPECT_THAT(projection.value().GetXY().x, DoubleNear(50.0, EPSILON));
    EXPECT_THAT(projection.value().GetXY().y, DoubleNear(2.0, EPSILON));
}

TEST_F(PointProjection_OnStraightRoads, FromStartingRoad_OffRoad_Backward)
{
    const Point<const Lane> origin{ST{35.0, -1.0}, route->origin.GetLane()};
    auto projection{route->ProjectPoint(origin, -30.0)};
    ASSERT_TRUE(projection.has_value());
    EXPECT_THAT(projection.value().GetXY().x, DoubleNear(5.0, EPSILON));
    EXPECT_THAT(projection.value().GetXY().y, DoubleNear(2.0, EPSILON));
}

struct PointProjection_OnRoadsWithChangingWidth : ::testing::Test
{
    PointProjection_OnRoadsWithChangingWidth()
    {
        auto road1 = world.AddRoad(
            world.AddReferenceLine(Pose<XY>{{0, 0}, M_PI_2}, Pose<XY>{{30, 0}, M_PI_2}),
            {
                &world.AddBoundary(XY{0, 6}, XY{30, 3.0}),
                &world.AddBoundary(XY{0, 3}, XY{30, 1.5}),
                &world.AddBoundary(XY{0, 0}, XY{30, 0.0}),
            }
        );
        auto road2 = world.AddRoad(
            world.AddReferenceLine(Pose<XY>{{30, 0}, M_PI_2}, Pose<XY>{{60, 0}, M_PI_2}),
            {
                &world.AddBoundary(XY{30, 3.0}, XY{60, 6}),
                &world.AddBoundary(XY{30, 1.5}, XY{60, 3}),
                &world.AddBoundary(XY{30, 0.0}, XY{60, 0}),
            }
        );
        test::ConnectRoads(road1.begin(), road1.end(), road2.begin(), road2.end());
        query = std::make_unique<Query>(world.handle);
        route = std::make_unique<Route<Traversal::Backward>>(query->GetRoute<Traversal::Backward>( // clang-format off
            XY{25.0, 0.0}, XY{35.0, 0.0}
        ).value()); // clang-format on
    }

    test::GroundTruth world;
    std::unique_ptr<Query> query;
    std::unique_ptr<Route<Traversal::Backward>> route;
};

TEST_F(PointProjection_OnRoadsWithChangingWidth, FromStartingRoad_ToSameRoad)
{
    const Point<const Lane> origin{ST{10.0, 0.5}, route->origin.GetLane()};
    ASSERT_THAT(origin.GetLane().GetV(origin.s), DoubleNear(1.25, EPSILON));
    const auto projection{route->ProjectPoint(origin, 10.0)};
    ASSERT_TRUE(projection.has_value());
    EXPECT_THAT(projection.value().GetV(), DoubleNear(0.75, EPSILON));
    EXPECT_THAT(projection.value().GetXY().x, DoubleNear(20.0, EPSILON));
    EXPECT_THAT(projection.value().GetXY().y, DoubleNear(0.25, EPSILON));
}

TEST_F(PointProjection_OnRoadsWithChangingWidth, FromStartingRoad_ToNextRoad)
{
    const auto origin{route->Localize(XY{10.0, 0.5})};
    const auto projection{route->ProjectPoint(origin, 40.0)};
    ASSERT_TRUE(projection.has_value());
    EXPECT_THAT(projection.value().GetXY().x, DoubleNear(50.0, EPSILON));
    EXPECT_THAT(projection.value().GetXY().y, DoubleNear(0.5, EPSILON));
}

struct PointProjection_OnBranchingRoads : ::testing::Test
{
    PointProjection_OnBranchingRoads()
    {
        auto road1 = world.AddRoad(
            world.AddReferenceLine(Pose<XY>{{0, 0}, M_PI_2}, Pose<XY>{{30, 0}, M_PI_2}),
            {
                &world.AddBoundary(XY{0, 4}, XY{30, 4}),
                &world.AddBoundary(XY{0, 0}, XY{30, 0}),
                &world.AddBoundary(XY{0, -4}, XY{30, -4}),
            }
        );
        auto road2 = world.AddRoad(
            world.AddReferenceLine(Pose<XY>{{30, 0}, M_PI_2}, Pose<XY>{{70, 30}, M_PI_2}),
            {
                &world.AddBoundary(XY{30, 4}, XY{70, 40}),
                &world.AddBoundary(XY{30, 0}, XY{70, 30}),
                &world.AddBoundary(XY{30, -4}, XY{70, 20}),
            }
        );
        test::ConnectRoads(road1.begin(), road1.end(), road2.begin(), road2.end());
        auto road3 = world.AddRoad(
            world.AddReferenceLine(Pose<XY>{{30, 0}, M_PI_2}, Pose<XY>{{70, -30}, M_PI_2}),
            {
                &world.AddBoundary(XY{30, 4}, XY{70, -20}),
                &world.AddBoundary(XY{30, 0}, XY{70, -30}),
                &world.AddBoundary(XY{30, -4}, XY{70, -40}),
            }
        );
        test::ConnectRoads(road1.begin(), road1.end(), road3.begin(), road3.end());
        query = std::make_unique<Query>(world.handle);
        route = std::make_unique<Route<>>(query->GetRoute<>(XY{0, 0}, XY{70, -30}).value());
    }

    test::GroundTruth world;
    std::unique_ptr<Query> query;
    std::unique_ptr<Route<>> route;
};

TEST_F(PointProjection_OnBranchingRoads, StaysOnStartingRoad)
{
    const Point<const Lane> origin{ST{5.0, -1.0}, route->origin.GetLane()};
    const auto projection{route->ProjectPoint(origin, 15.0)};
    ASSERT_TRUE(projection.has_value());
    EXPECT_THAT(projection.value().GetXY().x, DoubleNear(20.0, EPSILON));
    EXPECT_THAT(projection.value().GetXY().y, DoubleNear(-1.0, EPSILON));
}

TEST_F(PointProjection_OnBranchingRoads, FromStartingRoad_ToNextRoad)
{
    const auto origin{query->Localize(XY{0.0, -1.0}).front()}; // 1 unit above the centerline
    {
        const auto projection{route->ProjectPoint(origin, 30).value().GetXY()};
        EXPECT_DOUBLE_EQ(projection.x, 30);
        EXPECT_DOUBLE_EQ(projection.y, -1);
    }
    {
        const auto projection{route->ProjectPoint(origin, 40).value().GetXY()};
        EXPECT_DOUBLE_EQ(projection.x, 38);
        EXPECT_DOUBLE_EQ(projection.y, -33 * 0.2 - 1);
    }
    {
        const auto projection{route->ProjectPoint(origin, 50).value().GetXY()};
        EXPECT_DOUBLE_EQ(projection.x, 46);
        EXPECT_DOUBLE_EQ(projection.y, -33 * 0.4 - 1);
    }
    {
        const auto projection{route->ProjectPoint(origin, 60).value().GetXY()};
        EXPECT_DOUBLE_EQ(projection.x, 54);
        EXPECT_DOUBLE_EQ(projection.y, -33 * 0.6 - 1);
    }
    {
        const auto projection{route->ProjectPoint(origin, 70).value().GetXY()};
        EXPECT_DOUBLE_EQ(projection.x, 62);
        EXPECT_DOUBLE_EQ(projection.y, -33 * 0.8 - 1);
    }
    {
        const auto projection{route->ProjectPoint(origin, 80).value().GetXY()};
        EXPECT_DOUBLE_EQ(projection.x, 70);
        EXPECT_DOUBLE_EQ(projection.y, -34);
    }
    {
        const auto projection{route->ProjectPoint(origin, 85)};
        EXPECT_FALSE(projection.has_value());
    }
}
