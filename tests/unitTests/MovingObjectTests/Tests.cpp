/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "util/Common.tpp"

using namespace osiql;

class MovingObjectTest : public ::testing::Test
{
public:
    MovingObjectTest()
    {
        handle.mutable_base()->mutable_dimension()->set_length(10.0);
        handle.mutable_base()->mutable_dimension()->set_width(10.0);
        object = std::make_unique<MovingObject>(handle);
        SetSpin(0.0);
    }

    void SetPosition(const XYZ &position)
    {
        handle.mutable_base()->mutable_position()->set_x(position.x);
        handle.mutable_base()->mutable_position()->set_y(position.y);
        handle.mutable_base()->mutable_position()->set_z(position.z);
        *object = handle;
    }

    void SetVelocity(const XYZ &velocity)
    {
        handle.mutable_base()->mutable_velocity()->set_x(velocity.x);
        handle.mutable_base()->mutable_velocity()->set_y(velocity.y);
        handle.mutable_base()->mutable_velocity()->set_z(velocity.z);
        *object = handle;
    }

    void SetRotation(const XYZ &rotation)
    {
        handle.mutable_base()->mutable_orientation()->set_roll(rotation.x);
        handle.mutable_base()->mutable_orientation()->set_pitch(rotation.y);
        handle.mutable_base()->mutable_orientation()->set_yaw(rotation.z);
        *object = handle;
    }

    void SetRotation(double yaw)
    {
        handle.mutable_base()->mutable_orientation()->set_roll(0.0);
        handle.mutable_base()->mutable_orientation()->set_pitch(0.0);
        handle.mutable_base()->mutable_orientation()->set_yaw(yaw);
        *object = handle;
    }

    void SetSpin(const XYZ &spin)
    {
        handle.mutable_base()->mutable_orientation_rate()->set_roll(spin.x);
        handle.mutable_base()->mutable_orientation_rate()->set_pitch(spin.y);
        handle.mutable_base()->mutable_orientation_rate()->set_yaw(spin.z);
        *object = handle;
    }

    void SetSpin(double spin)
    {
        handle.mutable_base()->mutable_orientation_rate()->set_roll(0.0);
        handle.mutable_base()->mutable_orientation_rate()->set_pitch(0.0);
        handle.mutable_base()->mutable_orientation_rate()->set_yaw(spin);
        *object = handle;
    }

    osi3::MovingObject handle;
    std::unique_ptr<MovingObject> object;
};

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints01)
{
    SetVelocity({3, 4, 0});
    SetRotation(halfPi);
    SetSpin(0.0);
    EXPECT_EQ(object->GetVelocity(XYZ(1, 0, 0)), XYZ(3, 4, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints02)
{
    SetVelocity({3, 4, 0});
    SetRotation(halfPi);
    SetSpin(0.0);
    EXPECT_EQ(object->GetVelocity(XYZ(1, -1, 1)), XYZ(3, 4, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints03)
{
    SetVelocity({3, 4, 0});
    SetRotation(halfPi);
    SetSpin(0.5);
    EXPECT_EQ(object->GetVelocity(XYZ(0, 0, 0)), XYZ(3, 4, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints04)
{
    SetVelocity({3, 4, 0});
    SetRotation(0.0);
    SetSpin(0.5);
    EXPECT_EQ(object->GetVelocity(XYZ(1, -2, 3)), XYZ(4, 4.5, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints05)
{
    SetVelocity({3, 4, 0});
    SetRotation(halfPi);
    SetSpin(0.5);
    std::cout << "Spin: " << object->GetSpinMatrix() * XYZ(1, -2, 3) << '\n';
    EXPECT_EQ(object->GetVelocity(XYZ(1, -2, 3)), XYZ(2.5, 5, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints06)
{
    SetVelocity({3, 4, 0});
    SetRotation({0.3, 0.2, 0.1});
    SetSpin(0.0);
    EXPECT_EQ(object->GetVelocity(XYZ(1, -1, 1)), XYZ(3, 4, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints07)
{
    SetVelocity({3, 4, 0});
    SetRotation(halfPi);
    SetSpin({0, -0.5, 0});
    EXPECT_EQ(object->GetVelocity(XYZ(0, 0, 0)), XYZ(3, 4, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints08)
{
    SetVelocity({3, 4, 0});
    SetRotation(halfPi);
    SetSpin({0, -0.5, 0});
    EXPECT_EQ(object->GetVelocity(XYZ(1, -2, 3)), XYZ(3, 2.5, 0.5));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints09)
{
    SetVelocity({3, 4, 0});
    SetRotation({0, -halfPi, halfPi});
    SetSpin({0, -0.5, 0});
    EXPECT_EQ(object->GetVelocity(XYZ(1, -2, 3)), XYZ(3, 3.5, -1.5));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints10)
{
    SetVelocity({3, 4, 0});
    SetRotation(halfPi);
    SetSpin({0.5, 0, 0});
    EXPECT_EQ(object->GetVelocity(XYZ(0, 0, 0)), XYZ(3, 4, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints11)
{
    SetVelocity({3, 4, 0});
    SetRotation(halfPi);
    SetSpin({0.5, 0, 0});
    EXPECT_EQ(object->GetVelocity(XYZ(1, -2, 3)), XYZ(4.5, 4, -1));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints12)
{
    SetVelocity({3, 4, 0});
    SetRotation({-halfPi, 0, halfPi});
    SetSpin({0.5, 0, 0});
    EXPECT_EQ(object->GetVelocity(XYZ(1, -2, 3)), XYZ(4, 4, 1.5));
}
