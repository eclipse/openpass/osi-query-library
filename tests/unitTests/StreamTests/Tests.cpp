/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "util/Common.tpp"

using namespace osiql;
class StreamTest : public ::testing::Test
{
public:
    StreamTest()
    {
        roads.push_back(groundTruth.AddRoad( // 0
            groundTruth.AddReferenceLine(Pose<XY>{{0, 2}, halfPi}, Pose<XY>{{8, 6}, halfPi}),
            {
                &groundTruth.AddBoundary(XY{0, 2}, XY{8, 6}),
                &groundTruth.AddBoundary(XY{0, 0}, XY{8, 4}),
            }
        ));
        roads.push_back(groundTruth.AddRoad( // 1
            groundTruth.AddReferenceLine(Pose<XY>{{8, 6}, -halfPi}, Pose<XY>{{2, 8}, -halfPi}),
            {
                &groundTruth.AddBoundary(XY{8, 4}, XY{2, 6}),
                &groundTruth.AddBoundary(XY{8, 6}, XY{2, 8}),
            }
        ));
        roads.push_back(groundTruth.AddRoad( // 2
            groundTruth.AddReferenceLine(Pose<XY>{{8, 6}, halfPi}, Pose<XY>{{16, 4}, halfPi}),
            {
                &groundTruth.AddBoundary(XY{8, 6}, XY{16, 4}),
                &groundTruth.AddBoundary(XY{8, 4}, XY{16, 2}),
            }
        ));
        test::ConnectRoads(roads[0].begin(), roads[0].end(), roads[2].begin(), roads[2].end());
        test::ConnectRoads(roads[1].rbegin(), roads[1].rend(), roads[2].begin(), roads[2].end());
        roads.push_back(groundTruth.AddRoad( // 3
            groundTruth.AddReferenceLine(Pose<XY>{{14, 9}, -halfPi}, Pose<XY>{{8, 6}, -halfPi}),
            {
                &groundTruth.AddBoundary(XY{14, 7}, XY{8, 4}),
                &groundTruth.AddBoundary(XY{14, 9}, XY{8, 6}),
            }
        ));
        test::ConnectRoads(roads[0].begin(), roads[0].end(), roads[3].rbegin(), roads[3].rend());
        test::ConnectRoads(roads[1].rbegin(), roads[1].rend(), roads[3].rbegin(), roads[3].rend());
        roads.push_back(groundTruth.AddRoad( // 4
            groundTruth.AddReferenceLine(Pose<XY>{{14, 9}, -halfPi}, Pose<XY>{{8, 12}, -halfPi}),
            {
                &groundTruth.AddBoundary(XY{14, 7}, XY{8, 10}),
                &groundTruth.AddBoundary(XY{14, 9}, XY{8, 12}),
            }
        ));
        roads.push_back(groundTruth.AddRoad( // 5
            groundTruth.AddReferenceLine(Pose<XY>{{24, 4}, -halfPi}, Pose<XY>{{16, 4}, -halfPi}),
            {
                &groundTruth.AddBoundary(XY{24, 2}, XY{16, 2}),
                &groundTruth.AddBoundary(XY{24, 4}, XY{16, 4}),
            }
        ));
        test::ConnectRoads(roads[2].begin(), roads[2].end(), roads[5].rbegin(), roads[5].rend());
        roads.push_back(groundTruth.AddRoad( // 6
            groundTruth.AddReferenceLine(Pose<XY>{{22, 10}, -halfPi}, Pose<XY>{{16, 4}, -halfPi}),
            {
                &groundTruth.AddBoundary(XY{22, 8}, XY{16, 2}),
                &groundTruth.AddBoundary(XY{22, 10}, XY{16, 4}),
            }
        ));
        test::ConnectRoads(roads[2].begin(), roads[2].end(), roads[6].rbegin(), roads[6].rend());
        roads.push_back(groundTruth.AddRoad( // 7
            groundTruth.AddReferenceLine(Pose<XY>{{14, 9}, halfPi}, Pose<XY>{{22, 10}, halfPi}),
            {
                &groundTruth.AddBoundary(XY{14, 9}, XY{22, 10}),
                &groundTruth.AddBoundary(XY{14, 7}, XY{22, 8}),
            }
        ));
        test::ConnectRoads(roads[3].rbegin(), roads[3].rend(), roads[7].begin(), roads[7].end());
        test::ConnectRoads(roads[4].rbegin(), roads[4].rend(), roads[7].begin(), roads[7].end());
        roads.push_back(groundTruth.AddRoad( // 8
            groundTruth.AddReferenceLine(Pose<XY>{{22, 10}, halfPi}, Pose<XY>{{26, 10}, halfPi}),
            {
                &groundTruth.AddBoundary(XY{22, 10}, XY{26, 10}),
                &groundTruth.AddBoundary(XY{22, 8}, XY{26, 8}),
            }
        ));
        test::ConnectRoads(roads[6].rbegin(), roads[6].rend(), roads[8].begin(), roads[8].end());
        test::ConnectRoads(roads[7].begin(), roads[7].end(), roads[8].begin(), roads[8].end());

        objects.push_back(&groundTruth.Add<MovingObject>(XY{4, 3}, 1.0, 1.0));
        objects.push_back(&groundTruth.Add<MovingObject>(XY{0, 1}, 1.0, 1.0));
        objects.push_back(&groundTruth.Add<MovingObject>(XY{12, 4}, 1.0, 1.0));
        objects.push_back(&groundTruth.Add<MovingObject>(XY{11, 7}, 1.0, 1.0));

        world = std::make_unique<World>(groundTruth.handle);

        auto root{Node<>::Create(Point<>{{0.0, 0.0}, *world->GetLane(roads[0][0]->id().value())})};
    }

    test::GroundTruth groundTruth;
    std::unique_ptr<World> world;
    std::vector<std::vector<osi3::LogicalLane *>> roads;
    std::vector<osi3::MovingObject *> objects;
};

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(StreamTest, GetRandomRoute_DistanceZeroReturnsSameStartAndEnd)
{
    NumberGenerator rng;
    const auto route{world->GetRandomRoute(XY{0.0, 1.0}, rng, 0.0)};
    ASSERT_TRUE(route.has_value());
    EXPECT_DOUBLE_EQ(route.value().GetLength(), 0.0);
    EXPECT_EQ(route.value().front(), route.value().back());
}

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(StreamTest, GetRandomRoute_Forward)
{
    NumberGenerator rng;
    const auto randomRoute{world->GetRandomRoute(XY{0.0, 1.0}, rng, 100.0)};
    ASSERT_TRUE(randomRoute.has_value());
}

TEST_F(StreamTest, GetRandomRoute_Backward)
{
    NumberGenerator rng;
    const auto route{world->GetRandomRoute<Traversal::Backward>(XY{26.0, 9.0}, rng, 100.0)};
    ASSERT_TRUE(route.has_value());
}

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(StreamTest, GetRoute_Forward_MultiplePaths_SelectsShortestPath)
{
    const auto route{world->GetRoute(XY{0.0, 1.0}, XY{26.0, 9.0})};
    // There are two paths
    // The shorter path: 0 -> 3 -> 7 -> 8
    // The longer path: 0 -> 2 -> 6 -> 8
    ASSERT_TRUE(route.has_value());
    ASSERT_THAT(route.value(), SizeIs(4));
    EXPECT_THAT(route.value()[1]->GetRoad().GetId(), get<Id>(roads[3][0]));
    EXPECT_THAT(route.value()[2]->GetRoad().GetId(), get<Id>(roads[7][0]));
}

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(StreamTest, GetRoute_Backward_MultiplePaths_SelectsShortestPath)
{
    const auto route{world->GetRoute<Traversal::Backward>(XY{26.0, 9.0}, XY{0.0, 1.0})};
    // There are two paths
    // The shorter path: 8 -> 7 -> 3 -> 0
    const double expectedLength{world->GetLane(get<Id>(roads[8][0]))->GetLength() + world->GetLane(get<Id>(roads[7][0]))->GetLength() + world->GetLane(get<Id>(roads[3][0]))->GetLength() + world->GetLane(get<Id>(roads[0][0]))->GetLength()};
    // The longer path: 8 -> 6 -> 2 -> 0
    ASSERT_TRUE(route.has_value());
    ASSERT_THAT(route.value(), SizeIs(4));
    EXPECT_THAT(route.value()[1]->GetRoad().GetId(), get<Id>(roads[7][0]));
    EXPECT_THAT(route.value()[2]->GetRoad().GetId(), get<Id>(roads[3][0]));
    std::cout << "Expected length: " << expectedLength << '\n';
    std::cout << "Backward Route length: " << route.value().GetLength() << '\n';
}

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(StreamTest, GetRoute_OriginIsDestination_ReturnsOnePoint)
{
    const auto routeForward{world->GetRoute<Traversal::Forward>(XY{0.0, 1.0}, XY{0.0, 1.0})};
    ASSERT_TRUE(routeForward.has_value());
    EXPECT_THAT(routeForward.value().GetLength(), DoubleNear(0.0, EPSILON));
    EXPECT_EQ(routeForward.value().origin, routeForward.value().destination);

    const auto routeBackward{world->GetRoute<Traversal::Backward>(XY{0.0, 1.0}, XY{0.0, 1.0})};
    ASSERT_TRUE(routeBackward.has_value());
    EXPECT_THAT(routeBackward.value().GetLength(), DoubleNear(0.0, EPSILON));
    EXPECT_EQ(routeBackward.value().origin, routeBackward.value().destination);
}

TEST_F(StreamTest, GetRoute_DestinationBehindOrigin_Returns_NoRoute)
{
    { // On a road with Downstream driving direction
        const auto routeForward{world->GetRoute<Traversal::Forward>(XY{6.0, 4.0}, XY{0.0, 1.0})};
        EXPECT_FALSE(routeForward.has_value());

        const auto routeBackward{world->GetRoute<Traversal::Backward>(XY{0.0, 1.0}, XY{6.0, 4.0})};
        EXPECT_FALSE(routeBackward.has_value());
    }
    { // On a road with Upstream driving direction
        const auto routeForward{world->GetRoute<Traversal::Forward>(XY{5.0, 6.0}, XY{2.0, 7.0})};
        EXPECT_FALSE(routeForward.has_value());

        const auto routeBackward{world->GetRoute<Traversal::Backward>(XY{2.0, 7.0}, XY{5.0, 6.0})};
        EXPECT_FALSE(routeBackward.has_value());
    }
}

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(StreamTest, GetRoute_DestinationOnSameRoad)
{
    { // On a road with Downstream driving direction
        const auto routeForward{world->GetRoute<Traversal::Forward>(XY{0.0, 1.0}, XY{6.0, 4.0})};
        EXPECT_TRUE(routeForward.has_value());

        const auto routeBackward{world->GetRoute<Traversal::Backward>(XY{6.0, 4.0}, XY{0.0, 1.0})};
        EXPECT_TRUE(routeBackward.has_value());
    }
    { // On a road with Upstream driving direction
        const auto routeForward{world->GetRoute<Traversal::Forward>(XY{2.0, 7.0}, XY{5.0, 6.0})};
        EXPECT_TRUE(routeForward.has_value());

        const auto routeBackward{world->GetRoute<Traversal::Backward>(XY{5.0, 6.0}, XY{2.0, 7.0})};
        EXPECT_TRUE(routeBackward.has_value());
    }
}

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(StreamTest, GetRoute_DestinationOnNextRoad)
{
    { // From a downstream lane to a downstream lane
        const auto routeForward{world->GetRoute<Traversal::Forward>(XY{0.0, 1.0}, XY{12.0, 4.0})};
        EXPECT_TRUE(routeForward.has_value());

        const auto routeBackward{world->GetRoute<Traversal::Backward>(XY{12.0, 4.0}, XY{0.0, 1.0})};
        EXPECT_TRUE(routeBackward.has_value());
    }
    { // From a downstream lane to an upstream lane
        const auto routeForward{world->GetRoute<Traversal::Forward>(XY{0.0, 1.0}, XY{11.0, 7.0})};
        EXPECT_TRUE(routeForward.has_value());

        const auto routeBackward{world->GetRoute<Traversal::Backward>(XY{12.0, 4.0}, XY{2.0, 7.0})};
        EXPECT_TRUE(routeBackward.has_value());
    }
    { // From an upstream lane to a downstream lane
        const auto routeForward{world->GetRoute<Traversal::Forward>(XY{2.0, 7.0}, XY{12.0, 4.0})};
        EXPECT_TRUE(routeForward.has_value());

        const auto routeBackward{world->GetRoute<Traversal::Backward>(XY{11.0, 7.0}, XY{0.0, 1.0})};
        EXPECT_TRUE(routeBackward.has_value());
    }
    { // From an upstream lane to an upstream lane
        const auto routeForward{world->GetRoute<Traversal::Forward>(XY{2.0, 7.0}, XY{11.0, 7.0})};
        EXPECT_TRUE(routeForward.has_value());

        const auto routeBackward{world->GetRoute<Traversal::Backward>(XY{11.0, 7.0}, XY{2.0, 7.0})};
        EXPECT_TRUE(routeBackward.has_value());
    }
}

TEST_F(StreamTest, GetRoute_InvalidDestination_Returns_NoRoute)
{
    { // Point not on any road
        const auto route{world->GetRoute(XY{0.0, 1.0}, XY{1000.0, 1000.0})};
        EXPECT_FALSE(route.has_value());
    }
    { // Point with NaN components
        const auto route{world->GetRoute(XY{0.0, 1.0}, XY{std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()})};
        EXPECT_FALSE(route.has_value());
    }
}

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(StreamTest, GetRoute_AcquireGlobalPositionAction)
{
    osi3::TrafficAction::AcquireGlobalPositionAction action;
    action.mutable_position()->set_x(26.0);
    action.mutable_position()->set_y(9.0);
    const auto route{world->GetRoute(XY{0.0, 1.0}, action)};
    EXPECT_TRUE(route.has_value());
}

TEST_F(StreamTest, GetRoute_EmptyFollowPathAction_ReturnsNoRoute)
{
    const auto route{world->GetRoute(osi3::TrafficAction::FollowPathAction{})};
    EXPECT_FALSE(route.has_value());
}

void TestAddPoint(osi3::TrafficAction::FollowPathAction &action, const XY &xyPoint)
{
    auto *point{action.add_path_point()};
    point->mutable_position()->set_x(xyPoint.x);
    point->mutable_position()->set_y(xyPoint.y);
}

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(StreamTest, GetRoute_FollowPathAction_ContinuousPointsOnSingleRoad_Returns_FullRoute)
{
    osi3::TrafficAction::FollowPathAction action;
    TestAddPoint(action, XY{0.0, 1.0});
    TestAddPoint(action, XY{4.0, 3.0});
    TestAddPoint(action, XY{6.0, 4.0});
    const auto route{world->GetRoute(action)};
    ASSERT_TRUE(route.has_value());
    EXPECT_THAT(route.value(), SizeIs(1));
}

TEST_F(StreamTest, GetRoute_FollowPathAction_OscillatingPointsOnSingleRoad_Returns_NoRoute)
{
    osi3::TrafficAction::FollowPathAction action;
    TestAddPoint(action, XY{0.0, 1.0});
    TestAddPoint(action, XY{8.0, 5.0});
    TestAddPoint(action, XY{4.0, 3.0});
    const auto route{world->GetRoute(action)};
    ASSERT_FALSE(route.has_value());
}

TEST_F(StreamTest, GetCustomPath)
{
    const Point<const Lane> startPoint{ST{0.0, 0.0}, *world->GetLane(get<Id>(roads[0][0]))};
    // const double range{lengths[0] + lengths[3] + lengths[7] + lengths[8]};
    const auto route{world->GetRandomRoute(startPoint, []() { return 0.99; })};
    ASSERT_TRUE(route.has_value());
    ASSERT_THAT(route.value(), SizeIs(4));
}

TEST_F(StreamTest, GetDistancesToLaneEnds)
{
    const Lane *lane{world->GetLane(get<Id>(roads[0][0]))};
    ASSERT_NE(lane, nullptr);
    const auto root{Node<Traversal::Forward>::Create(*lane)};
    auto nodes{root->GetNodes(true, {}, {}, [](const auto &node) { return node.GetConnectedNodes().empty(); })};
    EXPECT_THAT(nodes, SizeIs(3));

    const double threshold{world->GetLane(get<Id>(roads[0][0]))->GetLength() + world->GetLane(get<Id>(roads[2][0]))->GetLength() + 1.0};
    nodes = root->GetNodes(
        true,
        ConnectedNodes<Traversal::Forward, Less<Min<Distance>>::Than<>>{threshold},
        {},
        [](const Node<Traversal::Forward> &node) { return node.GetConnectedNodes().empty(); }
    );
    EXPECT_THAT(nodes, SizeIs(1));
}

TEST_F(StreamTest, GetDistanceBetweenPoints_ForwardBackward_Returns_SameResult)
{
    const Point<const Lane> startPoint{ST{2.0, 0.0}, *world->GetLane(get<Id>(roads[0][0]))};
    const Point<const Lane> endPoint{ST{3.0, 0.0}, *world->GetLane(get<Id>(roads[8][0]))};
    EXPECT_DOUBLE_EQ(startPoint.GetDistanceTo(endPoint), endPoint.GetDistanceTo<Traversal::Backward>(startPoint));
    EXPECT_DOUBLE_EQ(startPoint.GetDistanceTo(startPoint), 0.0);
    EXPECT_DOUBLE_EQ(startPoint.GetDistanceTo<Traversal::Backward>(startPoint), 0.0);
}

// NOTE: Fails prior to OSI 3.6 due to lack of t_axis_yaw support
TEST_F(StreamTest, Find)
{
    const auto localizations{world->Localize(XY{0.0, 1.0})};
    ASSERT_THAT(localizations, SizeIs(1));
    const auto searchNode{Node<>::Create(localizations.front())};
    auto nodes{searchNode->GetNodes()};
    std::vector<Location<MovingObject, Node<>>> objects;
    for (const auto &node : nodes)
    {
        node->InsertAllWithin<MovingObject>(std::back_inserter(objects));
    }
    EXPECT_THAT(objects, SizeIs(4));
}

// Get Distance Between Objects

TEST_F(StreamTest, GetDistanceTo_FromObjectToSelf_Returns_Zero)
{
    const MovingObject *object{world->GetMovingObject(get<Id>(objects[0]))};
    ASSERT_NE(object, nullptr);
    EXPECT_THAT(object->GetDistanceTo(*object), DoubleEq(0.0));
}

TEST_F(StreamTest, GetDistanceTo_FromObjectToOtherObjectOnSameRoad_Returns_CorrectDistance)
{
    const MovingObject *first{world->GetMovingObject(get<Id>(objects[0]))};
    const MovingObject *second{world->GetMovingObject(get<Id>(objects[1]))};
    const double firstToSecond{first->GetDistanceTo<Traversal::Forward>(*second)};
    const double secondToFirst{second->GetDistanceTo<Traversal::Backward>(*first)};
    ASSERT_DOUBLE_EQ(firstToSecond, secondToFirst);
}
