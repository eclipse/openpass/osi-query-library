/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "util/Common.tpp"

using namespace osiql;

struct RoadTest : public ::testing::Test
{
    RoadTest()
    {
        lanes.push_back(groundTruth.AddRoad(
            groundTruth.AddReferenceLine(XY{0, 0}, XY{2, 0}),
            {
                &groundTruth.AddBoundary(XY{0, 1}, XY{2, 1}),
                &groundTruth.AddBoundary(XY{0, 0}, XY{2, 0}),
                &groundTruth.AddBoundary(XY{0, -1}, XY{2, -1}),
            }
        ));
        lanes.push_back(groundTruth.AddRoad(
            groundTruth.AddReferenceLine(XY{2, 0}, XY{4, 0}),
            {
                &groundTruth.AddBoundary(XY{2, 1}, XY{4, 1}),
                &groundTruth.AddBoundary(XY{2, 0}, XY{4, 0}),
                &groundTruth.AddBoundary(XY{2, -1}, XY{4, -1}),
            }
        ));
        test::ConnectRoads(lanes[0].begin(), lanes[0].end(), lanes[1].begin(), lanes[1].end());

        const std::array<XY, 11> positions{
            XY{0.5, -1.0},
            XY{1.25, -0.5},
            XY{2.0, -1.0},
            XY{4.25, -1.25},
            XY{4.5, -0.5},
            XY{4.25, 0.5},
            XY{3.0, 1.5},
            XY{2.0, 1.25},
            XY{1.0, 1.25},
            XY{1.75, 0.5},
            XY{3.0, 0.0},
        };
        for (size_t i{0}; i < objects.size(); ++i)
        {
            objects[i] = &groundTruth.Add<MovingObject>(positions[i], 0.5, 0.5); // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
        }
        objects[7]->mutable_base()->mutable_orientation()->set_yaw(halfPi);
        world = std::make_unique<World>(groundTruth.handle);
    }

    test::GroundTruth groundTruth;
    std::unique_ptr<World> world;
    std::array<osi3::MovingObject *, 11> objects{};
    std::vector<std::vector<osi3::LogicalLane *>> lanes;
};

TEST_F(RoadTest, GetAll_MovingObjects)
{
    const auto &roads{world->GetAll<Road>()};
    {
        const auto rightRelations{roads.at(get<Id>(lanes[0][1])).GetAll<MovingObject, Side::Right>()};
        ASSERT_THAT(rightRelations.size(), Eq(3));
        EXPECT_THAT(rightRelations[0]->GetMovingObject().GetId(), Eq(get<Id>(objects[0])));
        EXPECT_THAT(rightRelations[1]->GetMovingObject().GetId(), Eq(get<Id>(objects[1])));
        EXPECT_THAT(rightRelations[2]->GetMovingObject().GetId(), Eq(get<Id>(objects[2])));
    }
    {
        const auto leftRelations{roads.at(get<Id>(lanes[0][1])).GetAll<MovingObject, Side::Left>()};
        // FIXME: Boundary has collinear points, thus treating touching objects as intersecting
        ASSERT_THAT(leftRelations.size(), Eq(1));
        EXPECT_THAT(leftRelations[0]->GetMovingObject().GetId(), Eq(get<Id>(objects[9])));
    }
    {
        const auto rightRelations{roads.at(get<Id>(lanes[1][1])).GetAll<MovingObject, Side::Right>()};
        ASSERT_THAT(rightRelations.size(), Eq(2));
        EXPECT_THAT(rightRelations[0]->GetMovingObject().GetId(), Eq(get<Id>(objects[2])));
        EXPECT_THAT(rightRelations[1]->GetMovingObject().GetId(), Eq(get<Id>(objects[10])));
    }
    {
        const auto leftRelations{roads.at(get<Id>(lanes[1][1])).GetAll<MovingObject, Side::Left>()};
        ASSERT_THAT(leftRelations.size(), Eq(1));
        EXPECT_THAT(leftRelations[0]->GetMovingObject().GetId(), Eq(get<Id>(objects[10])));
    }
}

TEST_F(RoadTest, GetLanesContaining)
{
    const auto &roads{world->GetAll<Road>()};
    auto road1 = roads.at(get<Id>(lanes[0][1]));
    auto lanes1 = road1.GetLanesContaining(ST{0.0, 0.0});
    EXPECT_FALSE(lanes1.begin() == road1.lanes.end());
    EXPECT_TRUE(lanes1.end() == road1.lanes.end());
    auto lanes2 = road1.GetLanesContaining(ST{1.0, 0.0});
    EXPECT_FALSE(lanes2.begin() == road1.lanes.end());
    EXPECT_TRUE(lanes2.end() == road1.lanes.end());
    auto lanes3 = road1.GetLanesContaining(ST{2.0, 0.0});
    EXPECT_FALSE(lanes3.begin() == road1.lanes.end());
    EXPECT_TRUE(lanes3.end() == road1.lanes.end());
    auto lanes4 = road1.GetLanesContaining(ST{3.0, 0.0});
    EXPECT_TRUE(lanes4.begin() == road1.lanes.end());
    EXPECT_TRUE(lanes4.end() == road1.lanes.end());
    auto lanes5 = road1.GetLanesContaining(ST{-1.0, 0.0});
    EXPECT_TRUE(lanes5.begin() == road1.lanes.end());
    EXPECT_TRUE(lanes5.end() == road1.lanes.end());

    auto road2 = roads.at(get<Id>(lanes[1][1]));
    auto lanes6 = road2.GetLanesContaining(ST{0.0, 0.0});
    EXPECT_FALSE(lanes6.begin() == road2.lanes.end());
    EXPECT_TRUE(lanes6.end() == road2.lanes.end());
    auto lanes7 = road2.GetLanesContaining(ST{1.0, 0.0});
    EXPECT_FALSE(lanes7.begin() == road2.lanes.end());
    EXPECT_TRUE(lanes7.end() == road2.lanes.end());
    auto lanes8 = road2.GetLanesContaining(ST{2.0, 0.0});
    EXPECT_FALSE(lanes8.begin() == road2.lanes.end());
    EXPECT_TRUE(lanes8.end() == road2.lanes.end());
    auto lanes9 = road2.GetLanesContaining(ST{3.0, 0.0});
    EXPECT_TRUE(lanes9.begin() == road2.lanes.end());
    EXPECT_TRUE(lanes9.end() == road2.lanes.end());
    auto lanes10 = road2.GetLanesContaining(ST{-1.0, 0.0});
    EXPECT_TRUE(lanes10.begin() == road2.lanes.end());
    EXPECT_TRUE(lanes10.end() == road2.lanes.end());
}

TEST_F(RoadTest, GetClosetLane)
{
    const auto &roads{world->GetAll<Road>()};
    auto road1 = roads.at(get<Id>(lanes[0][1]));

    // Check which lane is chosen for a point in the center of the road
    auto *closestLane1 = road1.GetClosestLane<Side::Both>(ST{0.0, 0.0});
    EXPECT_EQ(closestLane1->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane2 = road1.GetClosestLane<Side::Left>(ST{0.0, 0.0});
    EXPECT_EQ(closestLane2->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane3 = road1.GetClosestLane<Side::Right>(ST{0.0, 0.0});
    EXPECT_EQ(closestLane3->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][1]))->GetOpenDriveId());

    auto *closestLane4 = road1.GetClosestLane<Side::Both>(ST{1.0, 0.0});
    EXPECT_EQ(closestLane4->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane5 = road1.GetClosestLane<Side::Left>(ST{1.0, 0.0});
    EXPECT_EQ(closestLane5->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane6 = road1.GetClosestLane<Side::Right>(ST{1.0, 0.0});
    EXPECT_EQ(closestLane6->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][1]))->GetOpenDriveId());

    auto *closestLane7 = road1.GetClosestLane<Side::Both>(ST{2.0, 0.0});
    EXPECT_EQ(closestLane7->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane8 = road1.GetClosestLane<Side::Left>(ST{2.0, 0.0});
    EXPECT_EQ(closestLane8->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane9 = road1.GetClosestLane<Side::Right>(ST{2.0, 0.0});
    EXPECT_EQ(closestLane9->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][1]))->GetOpenDriveId());

    // Check which lane is chosen for point at the top of the road
    auto *closestLane10 = road1.GetClosestLane<Side::Both>(ST{0.0, 1.0});
    EXPECT_EQ(closestLane10->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane11 = road1.GetClosestLane<Side::Left>(ST{0.0, 1.0});
    EXPECT_EQ(closestLane11->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane12 = road1.GetClosestLane<Side::Right>(ST{0.0, 1.0});
    EXPECT_EQ(closestLane12->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][1]))->GetOpenDriveId());

    auto *closestLane13 = road1.GetClosestLane<Side::Both>(ST{1.0, 1.0});
    EXPECT_EQ(closestLane13->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane14 = road1.GetClosestLane<Side::Left>(ST{1.0, 1.0});
    EXPECT_EQ(closestLane14->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane15 = road1.GetClosestLane<Side::Right>(ST{1.0, 1.0});
    EXPECT_EQ(closestLane15->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][1]))->GetOpenDriveId());

    auto *closestLane16 = road1.GetClosestLane<Side::Both>(ST{2.0, 1.0});
    EXPECT_EQ(closestLane16->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane17 = road1.GetClosestLane<Side::Left>(ST{2.0, 1.0});
    EXPECT_EQ(closestLane17->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane18 = road1.GetClosestLane<Side::Right>(ST{2.0, 1.0});
    EXPECT_EQ(closestLane18->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][1]))->GetOpenDriveId());

    // Check which lane is chosen for point at the bottom of the road
    auto *closestLane19 = road1.GetClosestLane<Side::Both>(ST{0.0, -1.0});
    EXPECT_EQ(closestLane19->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][1]))->GetOpenDriveId());
    auto *closestLane20 = road1.GetClosestLane<Side::Left>(ST{0.0, -1.0});
    EXPECT_EQ(closestLane20->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane21 = road1.GetClosestLane<Side::Right>(ST{0.0, -1.0});
    EXPECT_EQ(closestLane21->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][1]))->GetOpenDriveId());

    auto *closestLane22 = road1.GetClosestLane<Side::Both>(ST{1.0, -1.0});
    EXPECT_EQ(closestLane22->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][1]))->GetOpenDriveId());
    auto *closestLane23 = road1.GetClosestLane<Side::Left>(ST{1.0, -1.0});
    EXPECT_EQ(closestLane23->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane24 = road1.GetClosestLane<Side::Right>(ST{1.0, -1.0});
    EXPECT_EQ(closestLane24->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][1]))->GetOpenDriveId());

    auto *closestLane25 = road1.GetClosestLane<Side::Both>(ST{2.0, -1.0});
    EXPECT_EQ(closestLane25->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][1]))->GetOpenDriveId());
    auto *closestLane26 = road1.GetClosestLane<Side::Left>(ST{2.0, -1.0});
    EXPECT_EQ(closestLane26->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane27 = road1.GetClosestLane<Side::Right>(ST{2.0, -1.0});
    EXPECT_EQ(closestLane27->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][1]))->GetOpenDriveId());

    // Check which lane is chosen for point outside of the road
    auto *closestLane28 = road1.GetClosestLane<Side::Both>(ST{-1.0, 0.0});
    EXPECT_EQ(closestLane28->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane29 = road1.GetClosestLane<Side::Left>(ST{1.0, -2.0});
    EXPECT_EQ(closestLane29->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][0]))->GetOpenDriveId());
    auto *closestLane30 = road1.GetClosestLane<Side::Right>(ST{3.0, 0.0});
    EXPECT_EQ(closestLane30->GetOpenDriveId(), world->GetLane(get<Id>(lanes[0][1]))->GetOpenDriveId());

    auto road2 = roads.at(get<Id>(lanes[1][1]));

    // Check which lane is chosen for a point in the center of the road
    auto *closestLane31 = road2.GetClosestLane<Side::Both>(ST{0.0, 0.0});
    EXPECT_EQ(closestLane31->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane32 = road2.GetClosestLane<Side::Left>(ST{0.0, 0.0});
    EXPECT_EQ(closestLane32->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane33 = road2.GetClosestLane<Side::Right>(ST{0.0, 0.0});
    EXPECT_EQ(closestLane33->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][1]))->GetOpenDriveId());

    auto *closestLane34 = road2.GetClosestLane<Side::Both>(ST{1.0, 0.0});
    EXPECT_EQ(closestLane34->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane35 = road2.GetClosestLane<Side::Left>(ST{1.0, 0.0});
    EXPECT_EQ(closestLane35->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane36 = road2.GetClosestLane<Side::Right>(ST{1.0, 0.0});
    EXPECT_EQ(closestLane36->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][1]))->GetOpenDriveId());

    auto *closestLane37 = road2.GetClosestLane<Side::Both>(ST{2.0, 0.0});
    EXPECT_EQ(closestLane37->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane38 = road2.GetClosestLane<Side::Left>(ST{2.0, 0.0});
    EXPECT_EQ(closestLane38->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane39 = road2.GetClosestLane<Side::Right>(ST{2.0, 0.0});
    EXPECT_EQ(closestLane39->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][1]))->GetOpenDriveId());

    // Check which lane is chosen for point at the top of the road
    auto *closestLane40 = road2.GetClosestLane<Side::Both>(ST{0.0, 1.0});
    EXPECT_EQ(closestLane40->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane41 = road2.GetClosestLane<Side::Left>(ST{0.0, 1.0});
    EXPECT_EQ(closestLane41->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane42 = road2.GetClosestLane<Side::Right>(ST{0.0, 1.0});
    EXPECT_EQ(closestLane42->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][1]))->GetOpenDriveId());

    auto *closestLane43 = road2.GetClosestLane<Side::Both>(ST{1.0, 1.0});
    EXPECT_EQ(closestLane43->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane44 = road2.GetClosestLane<Side::Left>(ST{1.0, 1.0});
    EXPECT_EQ(closestLane44->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane45 = road2.GetClosestLane<Side::Right>(ST{1.0, 1.0});
    EXPECT_EQ(closestLane45->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][1]))->GetOpenDriveId());

    auto *closestLane46 = road2.GetClosestLane<Side::Both>(ST{2.0, 1.0});
    EXPECT_EQ(closestLane46->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane47 = road2.GetClosestLane<Side::Left>(ST{2.0, 1.0});
    EXPECT_EQ(closestLane47->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane48 = road2.GetClosestLane<Side::Right>(ST{2.0, 1.0});
    EXPECT_EQ(closestLane48->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][1]))->GetOpenDriveId());

    // Check which lane is chosen for point at the bottom of the road
    auto *closestLane49 = road2.GetClosestLane<Side::Both>(ST{0.0, -1.0});
    EXPECT_EQ(closestLane49->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][1]))->GetOpenDriveId());
    auto *closestLane50 = road2.GetClosestLane<Side::Left>(ST{0.0, -1.0});
    EXPECT_EQ(closestLane50->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane51 = road2.GetClosestLane<Side::Right>(ST{0.0, -1.0});
    EXPECT_EQ(closestLane51->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][1]))->GetOpenDriveId());

    auto *closestLane52 = road2.GetClosestLane<Side::Both>(ST{1.0, -1.0});
    EXPECT_EQ(closestLane52->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][1]))->GetOpenDriveId());
    auto *closestLane53 = road2.GetClosestLane<Side::Left>(ST{1.0, -1.0});
    EXPECT_EQ(closestLane53->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane54 = road2.GetClosestLane<Side::Right>(ST{1.0, -1.0});
    EXPECT_EQ(closestLane54->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][1]))->GetOpenDriveId());

    auto *closestLane55 = road2.GetClosestLane<Side::Both>(ST{2.0, -1.0});
    EXPECT_EQ(closestLane55->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][1]))->GetOpenDriveId());
    auto *closestLane56 = road2.GetClosestLane<Side::Left>(ST{2.0, -1.0});
    EXPECT_EQ(closestLane56->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane57 = road2.GetClosestLane<Side::Right>(ST{2.0, -1.0});
    EXPECT_EQ(closestLane57->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][1]))->GetOpenDriveId());

    // Check which lane is chosen for point outside of the road
    auto *closestLane58 = road2.GetClosestLane<Side::Both>(ST{-1.0, 0.0});
    EXPECT_EQ(closestLane58->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane59 = road2.GetClosestLane<Side::Left>(ST{1.0, -2.0});
    EXPECT_EQ(closestLane59->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][0]))->GetOpenDriveId());
    auto *closestLane60 = road2.GetClosestLane<Side::Right>(ST{3.0, 0.0});
    EXPECT_EQ(closestLane60->GetOpenDriveId(), world->GetLane(get<Id>(lanes[1][1]))->GetOpenDriveId());
}

TEST_F(RoadTest, GetRoadsFromLanes)
{
    const auto &roads{world->GetAll<Road>()};
    const auto collectedRoads{Road::GetRoads(world->GetAll<Lane>())};
    const Road &road1 = roads.at(get<Id>(lanes[0][1]));
    const Road &road0 = roads.at(get<Id>(lanes[1][1]));
    ASSERT_THAT(collectedRoads, SizeIs(2));
    EXPECT_EQ(*collectedRoads[0], road0);
    EXPECT_EQ(*collectedRoads[1], road1);
}

TEST_F(RoadTest, GetConnectedLanes)
{
    const auto &roads{world->GetAll<Road>()};

    auto road1 = roads.at(get<Id>(lanes[0][1]));
    const auto &connectedLanes1 = road1.GetConnectedLanes(Side::Left, Traversal::Backward);
    EXPECT_EQ(connectedLanes1.size(), 1);
    EXPECT_EQ(connectedLanes1[0]->GetId(), get<Id>(lanes[1][0]));
    const auto &connectedLanes2 = road1.GetConnectedLanes(Side::Right, Traversal::Forward);
    EXPECT_EQ(connectedLanes2.size(), 1);
    EXPECT_EQ(connectedLanes2[0]->GetId(), get<Id>(lanes[1][1]));
    const auto &connectedLanes3 = road1.GetConnectedLanes(Side::Left, Traversal::Forward);
    EXPECT_EQ(connectedLanes3.size(), 0);
    const auto &connectedLanes4 = road1.GetConnectedLanes(Side::Right, Traversal::Backward);
    EXPECT_EQ(connectedLanes4.size(), 0);

    auto road2 = roads.at(get<Id>(lanes[1][1]));
    const auto &connectedLanes5 = road2.GetConnectedLanes(Side::Right, Traversal::Backward);
    EXPECT_EQ(connectedLanes5.size(), 1);
    EXPECT_EQ(connectedLanes5[0]->GetId(), get<Id>(lanes[0][1]));
    const auto &connectedLanes6 = road2.GetConnectedLanes(Side::Left, Traversal::Forward);
    EXPECT_EQ(connectedLanes6.size(), 1);
    EXPECT_EQ(connectedLanes6[0]->GetId(), get<Id>(lanes[0][0]));
    const auto &connectedLanes7 = road2.GetConnectedLanes(Side::Right, Traversal::Forward);
    EXPECT_EQ(connectedLanes7.size(), 0);
    const auto &connectedLanes8 = road2.GetConnectedLanes(Side::Left, Traversal::Backward);
    EXPECT_EQ(connectedLanes8.size(), 0);
}

TEST_F(RoadTest, GetDistanceTo)
{
    const auto &roads{world->GetAll<Road>()};

    constexpr const double rootOF2 = boost::math::constants::root_two<double>();
    const double rootOf5 = sqrt(5.0);

    // Road 1
    const auto &road1 = roads.at(get<Id>(lanes[0][1]));

    // On road
    // On road both sides
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{0, 0}), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{1, 0}), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{2, 0}), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{0, 1}), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{0, -1}), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{1, 1}), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{1, -1}), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{2, 1}), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{2, -1}), 0.0);

    // On road left
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{0, 0}, Side::Left), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{1, 0}, Side::Left), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{2, 0}, Side::Left), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{0, 1}, Side::Left), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{0, -1}, Side::Left), 1.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{1, 1}, Side::Left), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{1, -1}, Side::Left), 1.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{2, 1}, Side::Left), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{2, -1}, Side::Left), 1.0);

    // On road right
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{0, 0}, Side::Right), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{1, 0}, Side::Right), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{2, 0}, Side::Right), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{0, 1}, Side::Right), 1.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{0, -1}, Side::Right), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{1, 1}, Side::Right), 1.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{1, -1}, Side::Right), 0.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{2, 1}, Side::Right), 1.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{2, -1}, Side::Right), 0.0);

    // Off road
    // Off road both sides
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{-1, 0}), 1.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{3, 0}), 1.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{-1, 2}), rootOF2);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{1, 2}), 1.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{3, 2}), rootOF2);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{-1, -2}), rootOF2);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{1, -2}), 1.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{3, -2}), rootOF2);

    // Off road left
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{-1, 0}, Side::Left), 1.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{3, 0}, Side::Left), 1.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{-1, 2}, Side::Left), rootOF2);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{1, 2}, Side::Left), 1.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{3, 2}, Side::Left), rootOF2);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{-1, -2}, Side::Left), rootOf5);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{1, -2}, Side::Left), 2.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{3, -2}, Side::Left), rootOf5);

    // Off road right
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{-1, 0}, Side::Right), 1.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{3, 0}, Side::Right), 1.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{-1, 2}, Side::Right), rootOf5);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{1, 2}, Side::Right), 2.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{3, 2}, Side::Right), rootOf5);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{-1, -2}, Side::Right), rootOF2);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{1, -2}, Side::Right), 1.0);
    EXPECT_DOUBLE_EQ(road1.GetDistanceTo(XY{3, -2}, Side::Right), rootOF2);
}
