/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "util/Common.tpp"

using namespace osiql;

using ::testing::Contains;
using ::testing::Key;

TEST(Lane, osiqlLaneTypes_Match_osiLogicalLaneTypes)
{
    osi3::LogicalLane handle;
    const Lane lane{handle};
    handle.set_type(osi3::LogicalLane_Type_TYPE_UNKNOWN);
    EXPECT_THAT(lane.GetType(), Lane::Type::Undefined);

    handle.set_type(osi3::LogicalLane_Type_TYPE_OTHER);
    EXPECT_THAT(lane.GetType(), Lane::Type::Other);

    handle.set_type(osi3::LogicalLane_Type_TYPE_NORMAL);
    EXPECT_THAT(lane.GetType(), Lane::Type::Normal);

    handle.set_type(osi3::LogicalLane_Type_TYPE_BIKING);
    EXPECT_THAT(lane.GetType(), Lane::Type::Biking);

    handle.set_type(osi3::LogicalLane_Type_TYPE_SIDEWALK);
    EXPECT_THAT(lane.GetType(), Lane::Type::Sidewalk);

    handle.set_type(osi3::LogicalLane_Type_TYPE_PARKING);
    EXPECT_THAT(lane.GetType(), Lane::Type::Parking);

    handle.set_type(osi3::LogicalLane_Type_TYPE_STOP);
    EXPECT_THAT(lane.GetType(), Lane::Type::Stop);

    handle.set_type(osi3::LogicalLane_Type_TYPE_RESTRICTED);
    EXPECT_THAT(lane.GetType(), Lane::Type::Restricted);

    handle.set_type(osi3::LogicalLane_Type_TYPE_BORDER);
    EXPECT_THAT(lane.GetType(), Lane::Type::Border);

    handle.set_type(osi3::LogicalLane_Type_TYPE_SHOULDER);
    EXPECT_THAT(lane.GetType(), Lane::Type::Shoulder);

    handle.set_type(osi3::LogicalLane_Type_TYPE_EXIT);
    EXPECT_THAT(lane.GetType(), Lane::Type::Exit);

    handle.set_type(osi3::LogicalLane_Type_TYPE_ENTRY);
    EXPECT_THAT(lane.GetType(), Lane::Type::Entry);

    handle.set_type(osi3::LogicalLane_Type_TYPE_ONRAMP);
    EXPECT_THAT(lane.GetType(), Lane::Type::OnRamp);

    handle.set_type(osi3::LogicalLane_Type_TYPE_OFFRAMP);
    EXPECT_THAT(lane.GetType(), Lane::Type::OffRamp);

    handle.set_type(osi3::LogicalLane_Type_TYPE_CONNECTINGRAMP);
    EXPECT_THAT(lane.GetType(), Lane::Type::ConnectingRamp);

    handle.set_type(osi3::LogicalLane_Type_TYPE_MEDIAN);
    EXPECT_THAT(lane.GetType(), Lane::Type::Median);

    handle.set_type(osi3::LogicalLane_Type_TYPE_CURB);
    EXPECT_THAT(lane.GetType(), Lane::Type::Curb);

    handle.set_type(osi3::LogicalLane_Type_TYPE_RAIL);
    EXPECT_THAT(lane.GetType(), Lane::Type::Rail);

    handle.set_type(osi3::LogicalLane_Type_TYPE_TRAM);
    EXPECT_THAT(lane.GetType(), Lane::Type::Tram);
}

struct LaneGetAll : ::testing::Test
{
    LaneGetAll()
    {
        // Add single lane
        lanes.push_back(&groundTruth.AddLane(
            groundTruth.AddReferenceLine(XY{0, 0}, XY{11, 0}),
            groundTruth.AddBoundary(XY{0, 3}, XY{11, 3}),
            groundTruth.AddBoundary(XY{0, -3}, XY{11, -3})
        ));
    }

    test::GroundTruth groundTruth;
    std::vector<osi3::LogicalLane *> lanes;
};

TEST_F(LaneGetAll, LaneGetAll_SingleLane)
{
    const Id laneId{get<Id>(lanes[0])};
    const World world{groundTruth.handle};
    const Lane *lane{world.GetLane(laneId)};
    ASSERT_TRUE(lane != nullptr);

    // Check that no objects exist
    EXPECT_THAT(lane->GetAll<MovingObject>(), IsEmpty());
    EXPECT_THAT(lane->GetAll<RoadMarking>(), IsEmpty());
    EXPECT_THAT(lane->GetAll<StationaryObject>(), IsEmpty());
    EXPECT_THAT(lane->GetAll<TrafficLight>(), IsEmpty());
    EXPECT_THAT(lane->GetAll<TrafficSign>(), IsEmpty());
}

struct LaneGetAllObjects : LaneGetAll
{
    LaneGetAllObjects()
    {
        // Add single object of each type and check whether it exists
        movingObjects.push_back(&groundTruth.Add<MovingObject>(XY{1, 0}, 2, 1));
        roadMarkings.push_back(&groundTruth.Add<RoadMarking>(XY{2, 0}, 1, 1, 4));
        stationaryObjects.push_back(&groundTruth.Add<StationaryObject>(XY{3, 0}, 1, 2));
        trafficLights.push_back(&groundTruth.Add<TrafficLight>(XY{4, 0}, 0.5, 1, 4));
        trafficSigns.push_back(&groundTruth.Add<TrafficSign>(XY{5, 0}, 1, 1, 4));
    }

    void AddObjectAndCheck(const Id &laneId, size_t expectedSize) // NOLINT(bugprone-easily-swappable-parameters)
    {
        const World world{groundTruth.handle};
        const Lane *lane{world.GetLane(laneId)};
        EXPECT_THAT(lane->GetAll<MovingObject>(), SizeIs(expectedSize));
        EXPECT_THAT(lane->GetAll<RoadMarking>(), SizeIs(expectedSize));
        EXPECT_THAT(lane->GetAll<StationaryObject>(), SizeIs(expectedSize));
        EXPECT_THAT(lane->GetAll<TrafficLight>(), SizeIs(expectedSize));
        EXPECT_THAT(lane->GetAll<TrafficSign>(), SizeIs(expectedSize));

        for (size_t i = 0; i < expectedSize; ++i)
        {
            CheckObject(lane, i);
        }
    }

    void CheckObject(const Lane *lane, size_t index)
    {
        EXPECT_THAT(lane->GetAll<MovingObject>(), Contains(Key(get<Id>(movingObjects[index]))));
        EXPECT_EQ(lane->GetAll<RoadMarking>()[index].Get<RoadMarking>().GetId(), get<Id>(roadMarkings[index]));
        EXPECT_THAT(lane->GetAll<StationaryObject>(), Contains(Key(get<Id>(stationaryObjects[index]))));
        EXPECT_EQ(lane->GetAll<TrafficLight>()[index].Get<TrafficLight>().GetId(), get<Id>(trafficLights[index]));
        EXPECT_EQ(lane->GetAll<TrafficSign>()[index].Get<TrafficSign>().GetId(), get<Id>(trafficSigns[index]));
    }

    std::vector<osi3::MovingObject *> movingObjects;
    std::vector<osi3::RoadMarking *> roadMarkings;
    std::vector<osi3::StationaryObject *> stationaryObjects;
    std::vector<osi3::TrafficLight *> trafficLights;
    std::vector<osi3::TrafficSign *> trafficSigns;
};

TEST_F(LaneGetAllObjects, LaneGetAll_SingleLane_SingleObject)
{
    const Id laneId{get<Id>(lanes[0])};
    AddObjectAndCheck(laneId, 1);
}

TEST_F(LaneGetAllObjects, LaneGetAll_SingleLane_AdditionalObject)
{
    const Id laneId{get<Id>(lanes[0])};
    AddObjectAndCheck(laneId, 1); // Check the initial state with one object per type

    // Add another object of each type
    movingObjects.push_back(&groundTruth.Add<MovingObject>(XY{6, 0}, 1, 1));
    roadMarkings.push_back(&groundTruth.Add<RoadMarking>(XY{7, 0}, 1, 1, 4));
    stationaryObjects.push_back(&groundTruth.Add<StationaryObject>(XY{8, 0}, 1, 1));
    trafficLights.push_back(&groundTruth.Add<TrafficLight>(XY{9, 0}, 1, 1, 4));
    trafficSigns.push_back(&groundTruth.Add<TrafficSign>(XY{10, 0}, 1, 1, 4));

    AddObjectAndCheck(laneId, 2); // Check the state after adding the second set of objects
}

TEST(Lane, GetWidth)
{
    test::GroundTruth groundTruth;
    std::vector<osi3::LogicalLane *> lanes{&groundTruth.AddLane(
        groundTruth.AddReferenceLine(XY{0, 0}, XY{10, 0}),
        groundTruth.AddBoundary(XY{0, 3}, XY{10, 6}),
        groundTruth.AddBoundary(XY{0, -3}, XY{10, -6})
    )};

    // Check whether lane exists
    const World world{groundTruth.handle};
    const Lane *lane{world.GetLane(4)};
    ASSERT_TRUE(lane != nullptr);

    EXPECT_DOUBLE_EQ(lane->GetWidth(-5), 0.0);
    EXPECT_DOUBLE_EQ(lane->GetWidth(0), 6.0);
    EXPECT_DOUBLE_EQ(lane->GetWidth(5), 9.0);
    EXPECT_DOUBLE_EQ(lane->GetWidth(10), 12.0);
    EXPECT_DOUBLE_EQ(lane->GetWidth(20), 0.0);
}

TEST(Lane, GetDistanceTo)
{
    test::GroundTruth groundTruth;
    const std::vector<osi3::LogicalLane *> lanes{&groundTruth.AddLane(
        groundTruth.AddReferenceLine(XY{0, 0}, XY{10, 0}),
        groundTruth.AddBoundary(XY{0, 3}, XY{10, 3}),
        groundTruth.AddBoundary(XY{0, -3}, XY{10, -3})
    )};

    // Check whether lane exists
    const World world{groundTruth.handle};
    const Lane *lane{world.GetLane(4)};
    ASSERT_TRUE(lane != nullptr);

    // Check distance measurements
    EXPECT_DOUBLE_EQ(lane->GetDistanceTo(XY{0, 0}), 0.0);
    EXPECT_DOUBLE_EQ(lane->GetDistanceTo(XY{5, 0}), 0.0);
    EXPECT_DOUBLE_EQ(lane->GetDistanceTo(XY{10, 0}), 0.0);
    EXPECT_DOUBLE_EQ(lane->GetDistanceTo(XY{20, 0}), 10.0);
    EXPECT_DOUBLE_EQ(lane->GetDistanceTo(XY{10, 3}), 0.0);
    EXPECT_DOUBLE_EQ(lane->GetDistanceTo(XY{15, 3}), 5.0);
}

TEST(Lane, LocalizePoint)
{
    test::GroundTruth groundTruth;
    std::vector<osi3::LogicalLane *> lanes{&groundTruth.AddLane(
        groundTruth.AddReferenceLine(XY{0, 0}, XY{10, 0}),
        groundTruth.AddBoundary(XY{0, 3}, XY{10, 3}),
        groundTruth.AddBoundary(XY{0, -3}, XY{10, -3})
    )};

    // Check whether lane exists
    const World world{groundTruth.handle};
    const Lane *lane{world.GetLane(get<Id>(lanes[0]))};
    ASSERT_TRUE(lane != nullptr);

    // Check relative positions
    const Point<const Lane> point1 = lane->Localize(XY{0, 0});
    EXPECT_DOUBLE_EQ(point1.s, 0.0);
    EXPECT_DOUBLE_EQ(point1.t, 0.0);
    const Point<const Lane> point2 = lane->Localize(XY{5, 0});
    EXPECT_DOUBLE_EQ(point2.s, 5.0);
    EXPECT_DOUBLE_EQ(point2.t, 0.0);
    const Point<const Lane> point3 = lane->Localize(XY{10, 0});
    EXPECT_DOUBLE_EQ(point3.s, 10.0);
    EXPECT_DOUBLE_EQ(point3.t, 0.0);
    const Point<const Lane> point4 = lane->Localize(XY{0, 1});
    EXPECT_DOUBLE_EQ(point4.s, 0.0);
    EXPECT_DOUBLE_EQ(point4.t, 1.0);
    const Point<const Lane> point5 = lane->Localize(XY{5, -1});
    EXPECT_DOUBLE_EQ(point5.s, 5.0);
    EXPECT_DOUBLE_EQ(point5.t, -1.0);
    const Point<const Lane> point6 = lane->Localize(XY{10, 2});
    EXPECT_DOUBLE_EQ(point6.s, 10.0);
    EXPECT_DOUBLE_EQ(point6.t, 2.0);
    const Point<const Lane> point7 = lane->Localize(XY{11, 2});
    EXPECT_DOUBLE_EQ(point7.s, 11.0);
    EXPECT_DOUBLE_EQ(point7.t, 2.0);
    const Point<const Lane> point8 = lane->Localize(XY{-20, -6});
    EXPECT_DOUBLE_EQ(point8.s, -20.0);
    EXPECT_DOUBLE_EQ(point8.t, -6.0);
}

TEST(Lane, LocalizePose)
{
    test::GroundTruth groundTruth;
    std::vector<osi3::LogicalLane *> lanes;

    // Add single lane
    lanes.push_back(&groundTruth.AddLane(groundTruth.AddReferenceLine(XY{0, 0}, XY{10, 0}), groundTruth.AddBoundary(XY{0, 3}, XY{10, 3}), groundTruth.AddBoundary(XY{0, -3}, XY{10, -3})));

    // Check whether lane exists
    const World world{groundTruth.handle};
    const Lane *lane{world.GetLane(4)};
    ASSERT_TRUE(lane != nullptr);

    // Check relative positions
    const Pose<Point<const Lane>> point1 = lane->Localize(Pose<XY>{XY{0, 0}, 0.0});
    EXPECT_DOUBLE_EQ(point1.s, 0.0);
    EXPECT_DOUBLE_EQ(point1.t, 0.0);
    EXPECT_DOUBLE_EQ(point1.angle, 0.0);
    const Pose<Point<const Lane>> point2 = lane->Localize(Pose<XY>{XY{5, 0}, 0.0});
    EXPECT_DOUBLE_EQ(point2.s, 5.0);
    EXPECT_DOUBLE_EQ(point2.t, 0.0);
    EXPECT_DOUBLE_EQ(point2.angle, 0.0);
    const Pose<Point<const Lane>> point3 = lane->Localize(Pose<XY>{XY{10, 0}, 0.0});
    EXPECT_DOUBLE_EQ(point3.s, 10.0);
    EXPECT_DOUBLE_EQ(point3.t, 0.0);
    EXPECT_DOUBLE_EQ(point3.angle, 0.0);
    const Pose<Point<const Lane>> point4 = lane->Localize(Pose<XY>{XY{0, 1}, 0.0});
    EXPECT_DOUBLE_EQ(point4.s, 0.0);
    EXPECT_DOUBLE_EQ(point4.t, 1.0);
    EXPECT_DOUBLE_EQ(point4.angle, 0.0);
    const Pose<Point<const Lane>> point5 = lane->Localize(Pose<XY>{XY{5, -1}, 0.0});
    EXPECT_DOUBLE_EQ(point5.s, 5.0);
    EXPECT_DOUBLE_EQ(point5.t, -1.0);
    EXPECT_DOUBLE_EQ(point5.angle, 0.0);
    const Pose<Point<const Lane>> point6 = lane->Localize(Pose<XY>{XY{10, 2}, 0.0});
    EXPECT_DOUBLE_EQ(point6.s, 10.0);
    EXPECT_DOUBLE_EQ(point6.t, 2.0);
    EXPECT_DOUBLE_EQ(point6.angle, 0.0);
    const Pose<Point<const Lane>> point7 = lane->Localize(Pose<XY>{XY{11, 2}, 0.0});
    EXPECT_DOUBLE_EQ(point7.s, 11.0);
    EXPECT_DOUBLE_EQ(point7.t, 2.0);
    EXPECT_DOUBLE_EQ(point7.angle, 0.0);
    const Pose<Point<const Lane>> point8 = lane->Localize(Pose<XY>{XY{-20, -6}, 0.0});
    EXPECT_DOUBLE_EQ(point8.s, -20.0);
    EXPECT_DOUBLE_EQ(point8.t, -6.0);
    EXPECT_DOUBLE_EQ(point8.angle, 0.0);
    const Pose<Point<const Lane>> point9 = lane->Localize(Pose<XY>{XY{0, 0}, 0.123});
    EXPECT_DOUBLE_EQ(point9.s, 0.0);
    EXPECT_DOUBLE_EQ(point9.t, 0.0);
    EXPECT_DOUBLE_EQ(point9.angle, 0.123);
    const Pose<Point<const Lane>> point10 = lane->Localize(Pose<XY>{XY{5, 0}, -0.123});
    EXPECT_DOUBLE_EQ(point10.s, 5.0);
    EXPECT_DOUBLE_EQ(point10.t, 0.0);
    EXPECT_DOUBLE_EQ(point10.angle, -0.123);
    const Pose<Point<const Lane>> point11 = lane->Localize(Pose<XY>{XY{10, 0}, 0.23});
    EXPECT_DOUBLE_EQ(point11.s, 10.0);
    EXPECT_DOUBLE_EQ(point11.t, 0.0);
    EXPECT_DOUBLE_EQ(point11.angle, 0.23);
}

TEST(Lane, GetLaneChangesTo)
{
    test::GroundTruth groundTruth;
    std::vector<std::vector<osi3::LogicalLane *>> roads{groundTruth.AddRoad(
        groundTruth.AddReferenceLine(XY{0, 0}, XY{10, 0}),
        {
            &groundTruth.AddBoundary(XY{0, 6}, XY{10, 6}),
            &groundTruth.AddBoundary(XY{0, 3}, XY{10, 3}),
            &groundTruth.AddBoundary(XY{0, 0}, XY{10, 0}),
            &groundTruth.AddBoundary(XY{0, -3}, XY{10, -3}),
            &groundTruth.AddBoundary(XY{0, -6}, XY{10, -6}),
        }
    )};

    // Check whether lanes exists
    const World world{groundTruth.handle};
    const Lane *lane1{world.GetLane(get<Id>(roads[0][0]))};
    ASSERT_TRUE(lane1 != nullptr);
    const Lane *lane2{world.GetLane(get<Id>(roads[0][1]))};
    ASSERT_TRUE(lane2 != nullptr);
    const Lane *lane3{world.GetLane(get<Id>(roads[0][2]))};
    ASSERT_TRUE(lane3 != nullptr);
    const Lane *lane4{world.GetLane(get<Id>(roads[0][3]))};
    ASSERT_TRUE(lane4 != nullptr);

    // Check functions outputs
    auto [side1, changes1] = lane1->GetLaneChangesTo(*lane1);
    EXPECT_EQ(side1, Side::Right);
    EXPECT_EQ(changes1, 0);
    auto [side2, changes2] = lane1->GetLaneChangesTo(*lane2);
    EXPECT_EQ(side2, Side::Left);
    EXPECT_EQ(changes2, 1);
    auto [side3, changes3] = lane1->GetLaneChangesTo(*lane3);
    EXPECT_EQ(side3, Side::Right);
    EXPECT_EQ(changes3, 2);
    auto [side4, changes4] = lane1->GetLaneChangesTo(*lane4);
    EXPECT_EQ(side4, Side::Right);
    EXPECT_EQ(changes4, 3);
    auto [side5, changes5] = lane4->GetLaneChangesTo(*lane1);
    EXPECT_EQ(side5, Side::Left);
    EXPECT_EQ(changes5, 3);
    auto [side6, changes6] = lane4->GetLaneChangesTo(*lane2);
    EXPECT_EQ(side6, Side::Left);
    EXPECT_EQ(changes6, 2);
    auto [side7, changes7] = lane4->GetLaneChangesTo(*lane3);
    EXPECT_EQ(side7, Side::Left);
    EXPECT_EQ(changes7, 1);
    auto [side8, changes8] = lane2->GetLaneChangesTo(*lane3);
    EXPECT_EQ(side8, Side::Right);
    EXPECT_EQ(changes8, 1);
}

TEST(Lane, GetOpenDriveId)
{
    test::GroundTruth groundTruth;
    std::vector<std::vector<osi3::LogicalLane *>> roads;
    roads.push_back(groundTruth.AddRoad(
        groundTruth.AddReferenceLine(XY{0, 0}, XY{10, 0}),
        {
            &groundTruth.AddBoundary(XY{0, 6}, XY{10, 6}),
            &groundTruth.AddBoundary(XY{0, 3}, XY{10, 3}),
            &groundTruth.AddBoundary(XY{0, 0}, XY{10, 0}),
            &groundTruth.AddBoundary(XY{0, -3}, XY{10, -3}),
            &groundTruth.AddBoundary(XY{0, -6}, XY{10, -6}),
        }
    ));

    // Check whether lanes exists
    const World world{groundTruth.handle};
    const Lane *lane1{world.GetLane(get<Id>(roads[0][0]))};
    ASSERT_TRUE(lane1 != nullptr);
    const Lane *lane2{world.GetLane(get<Id>(roads[0][1]))};
    ASSERT_TRUE(lane2 != nullptr);
    const Lane *lane3{world.GetLane(get<Id>(roads[0][2]))};
    ASSERT_TRUE(lane3 != nullptr);
    const Lane *lane4{world.GetLane(get<Id>(roads[0][3]))};
    ASSERT_TRUE(lane4 != nullptr);

    // Check functions outputs
    EXPECT_EQ(lane1->GetOpenDriveId(), 2);
    EXPECT_EQ(lane2->GetOpenDriveId(), 1);
    EXPECT_EQ(lane3->GetOpenDriveId(), -1);
    EXPECT_EQ(lane4->GetOpenDriveId(), -2);
}
